
## 说明  

- "CE"是北条司作品"Cat's Eye"的缩写，该作品的其他名称有: キャッツアイ / 貓眼三姐妹(大陸) / 貓之眼(港) / 貓眼(台)  
- "CH"是北条司作品"City Hunter"的缩写，该作品的其他名称有: シティーハンター / 城市猎人(大陸) / 城市獵人(港/台) / 侠探寒羽良(大陆 盗版)  
- "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭(大陆) / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  
- 《こもれ陽の下で･･･》是北条司的作品，该作品的其他名称有: Komorebi No Moto De... / Under the Dapple Shade / Beneath the Dappled Shade / 阳光少女(大陆) / 艷陽少女(港)  
- "AH"是北条司作品"Angel Heart"的缩写，该作品的其他名称有: エンジェルハート / 天使之心(港) / 天使心(台)  
- 以下这些文章发布在[北条司中文网论坛](http://www.hojocn.com/bbs)，欢迎您来该论坛讨论。  
- 本项目数据同步至以下两个代码库：  
    - [myblogs - Gitlab](https://gitlab.com/city4cat/myblogs)  
    - [myblogs - Gitea](https://gitea.com/city4cat/myblogsgl)  


## 目录  

## CE:  
[关于「Cat's Eye 40周年纪念原画展」的一些信息](./hojocn/ce_40th/readme.md)  
[猫眼咖啡屋的建筑风格](./hojocn/ce_catseye_cafe/readme.md)  
[CE的一些细节](./hojocn/ce_details/readme.md)  
[Margaret Atwood的小说《Cat's Eye》 ](./hojocn/zz_ce_margaret/catseye_margaret.md)  
[CE的一些信息](./hojocn/ce_info/readme.md)  


## CH:  
[CH结尾处的致谢](./hojocn/ch_acknowledgement/readme.md)  
[CH的绘画 - 关于"《城市猎人》里角色相貌相似](./hojocn/ch_drawing/similar_faces.md)  
[CH的绘画 - 衣服/布料/褶皱](./hojocn/ch_drawing__cloth/readme.md)  
[CH的绘画 - 动作/姿态](./hojocn/ch_drawing__gesture)  
[CH的绘画 - 化妆](./hojocn/ch_make-up/img)  
[CH的一些信息](./hojocn/ch_info/readme.md)  
[CH的一些细节](./hojocn/ch_details/readme.md)  


## 短篇:  
[《Parrot-幸福的人》 ~ 神谷明](./hojocn/others_kamiya_akira/airman_parrot_kamiya_akira.md)  
[解读《蔚蓝长空》](./hojocn/ss_blue-sky/readme.md)  
[解读《少年们的夏天--珍妮的乐曲》](./hojocn/ss_melody-of-jenny/readme.md)  
[解读《American Dream》](./hojocn/ss_american-dream/readme.md)  
[解读《白猫少女》](./hojocn/ss_cat_lady/readme.md)  


## FC:  
[Family Compo Asset Library](https://gitlab.com/family-compo-asset), ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96741))    
[FC同人-无题](https://gitlab.com/family-compo-asset/family-compo-mods/-/tree/master/02_memory)， 
    ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96877))  
[FC同人-你是我的女王](https://gitlab.com/family-compo-asset/family-compo-mods/-/tree/master/04_you_are_my_queen)， 
    ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96759))   
[FC动图](./hojocn/fc_animation/readme.md)  
[可能影响FC创作的（日本）社会背景](./hojocn/fc_background/readme.md)  
[FC的绘画-修图](./hojocn/fc_bugfix/readme.md)  
[如何化解FC里堂表兄妹恋在中国文化里的尴尬](./hojocn/fc_cousin_love_in_chinese_culture/readme.md)  
[FC的一些细节](./hojocn/fc_details/readme.md)  
[雅彦日记](./hojocn/fc_diary/fc_diary_masahiko.md)  

[FC的绘画特点(汇总)](./hojocn/fc_drawing/readme.md)  
    - [FC的绘画-对话框](./hojocn/fc_dialogue/readme.md)  
    - [FC的画风-面部-年龄变化](./hojocn/fc_drawing_style__aging/readme.md)  
    - [FC的画风-人物身材比例](./hojocn/fc_drawing_style__body_proportion/readme.md)  
    - [FC的画风-衣服/布料/褶皱](./hojocn/fc_drawing_style__cloth/readme.md)  
    - [FC的绘画-面部](./hojocn/fc_drawing_style__face/readme.md)  
    - [FC的绘画-面部2](./hojocn/fc_drawing_style__face/readme2.md)  
    - [FC的画风-面部(侧面)](./hojocn/fc_drawing_style__face/fc_face_side.md)  
    - [FC的画风-面部-眉](./hojocn/fc_drawing_style__face_brow/readme.md)  
    - [FC的画风-面部-眉2](./hojocn/fc_drawing_style__face_brow2/readme.md)  
    - [FC的画风-面部-五官组合(正面)](./hojocn/fc_drawing_style__face_composition/readme.md)  
    - [FC的画风-面部-耳(侧面)](./hojocn/fc_drawing_style__face_ear_side/readme.md)  
    - [FC的画风-面部-眼部](./hojocn/fc_drawing_style__face_eye/readme.md)  
    - [FC的画风-面部-眼睛2](./hojocn/fc_drawing_style__face_eye2/readme.md)  
    - [FC的画风-面部-眼睛(侧面)](./hojocn/fc_drawing_style__face_eye_side/readme.md)  
    - [FC的画风-面部-嘴](./hojocn/fc_drawing_style__face_mouth/readme.md)  
    - [FC的画风-面部-嘴(正面)](./hojocn/fc_drawing_style__face_mouth2/readme.md)  
    - [FC的画风-面部-鼻](./hojocn/fc_drawing_style__face_nose/readme.md)  
    - [FC的画风-面部-鼻(正面)](./hojocn/fc_drawing_style__face_nose2/readme.md)  
    - [FC的画风-面部比例](./hojocn/fc_drawing_style__face_proportion/readme.md)  
    - [FC的画风-面部2(侧面)](./hojocn/fc_drawing_style__face_side2/readme.md)  
    - [FC的画风-特效](./hojocn/fc_drawing_style__fx/readme.md)  
    - [FC的画风-动作/姿态](./hojocn/fc_drawing_style__gesture/readme.md)  
    - [FC的画风-头发](./hojocn/fc_drawing_style__hair/readme.md)  
    - [FC的画风-面部-头发2(正面)](./hojocn/fc_drawing_style__hair2/readme.md)  
    - [FC的画风-手](./hojocn/fc_drawing_style__hand/readme.md)  
    - [FC的画风-腿](./hojocn/fc_drawing_style__leg/readme.md)  
    - [FC的画风-化妆](./hojocn/fc_drawing_style__make-up/readme.md)  
    - [FC的画风-颈部](./hojocn/fc_drawing_style__neck/readme.md)  
    - [FC的绘画-透视](./hojocn/fc_drawing_style__perspective/readme.md)  
    - [FC的绘画-写实风格](./hojocn/fc_drawing_style__realistic/readme.md)  
    - [FC的画风-场景](./hojocn/fc_drawing_style__scene/readme.md)  
    - [FC的画风-着色](./hojocn/fc_drawing_style__shading/readme.md)  
    - [FC的服饰](./hojocn/fc_dress/readme.md)  

[FC表情包](./hojocn/fc_expression) 
( 
    [表情-雅彦](./hojocn/fc_expression/fc_exp_masahiko.md)， 
    [表情-紫苑](./hojocn/fc_expression/fc_exp_shion.md)， 
    [表情-其他人](./hojocn/fc_expression/fc_exp_others.md)， 
)  
[FC里某些角色的颜值](./hojocn/fc_face_score/fc_face_score.md)  
[FC的画风-食物](./hojocn/fc_food/readme.md)  
[FC里的幽默](./hojocn/fc_humour/readme.md)  
[FC同人文--我是一只流浪猫](./hojocn/fc_im_a_stray_cat/readme.md)  
[整理FC的一些信息](./hojocn/fc_information/readme.md)， 
    [非常家庭 - wikipedia](./hojocn/fc_information/fc_zh_wiki.md)， 
    [考究一下FC漫画名的意思](./hojocn/fc_information/fc_title.md)， 
    [FC时间线](./hojocn/fc_information/fc_timeline.md)， 

[FC玉皇朝版封底彩图收录](./hojocn/fc_jade_dynasty_back_color), ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96851))    
[[Todo]FC的画风-风景](./hojocn/fc_landscapes/readme.md)  
[FC的分镜](./hojocn/fc_sequence/readme.md)  
[FC里的人物姓名与日本姓氏](./hojocn/fc_surname_vs_jap_surname/readme.md)  
[[zz]关于"紫苑"这个名字](./hojocn/fc_surname_vs_jap_surname/zz_shion_jianshu/zz_shion_jianshu.md)  
[FC实物集](./hojocn/fc_things_in_real_world/readme.md)  
[把FC故事板化所遇到的问题](./hojocn/fc_to_storyboard/fc_to_storyboard.md)  
[若苗家的户型和布局](./hojocn/fc_wakanae_house_layout/readme.md)  
[FC的编剧](./hojocn/fc_writing/readme.md)， 
    [FC的编剧 - FC每话情节概括](./hojocn/fc_writing/fc_chapters_summary.md)， 
    [FC每话剧情与"Pixar的故事讲述规则"的对照](./hojocn/fc_writing/fc_vs_pixar_storytelling_in_chapters.md)， 
    ["Pixar的故事讲述规则"对照FC的剧情](./hojocn/fc_writing/fc_vs_pixar_storytelling_overall.md)，  
    [FC后续剧情的推测](./hojocn/fc_writing/fc_future_stories.md)  


## AH:  
[AH里最受欢迎的10个故事(访谈)](./hojocn/ah2_vol10_best_EP_10/readme.md)  
[AH的一些细节](./hojocn/ah_details/ah_details.md)  
[AH的故事原型](./hojocn/ah_details/ah_prototype.md)  
[AH的绘画 - 脚](./hojocn/ah_drawing__foot/readmd.md)  
[AH的分镜](./hojocn/ah_sequence/readme.md)  


## Interviews:  
[北条司漫画家20周年纪念(访谈)](./hojocn/zz_anniversary_20th/anni_20th_interview.md)， 
    [评论](./hojocn/zz_anniversary_20th/reviews.md)  
[大美术 20周年纪念](./hojocn/zz_anniversary_20th/dameishu.md)  
[神谷明 特别采访(2004) |CityHunter 完全版 Vol X](./hojocn/zz_ch_vol-x/ch5_interview_2004.md)  
[Angel Heart Official Guide Book 中的专访 (2008)](./hojocn/zz_ahg_interview/readme.md))  
[北条司 拾遗集 画业35周年纪念](./hojocn/zz_anniversary_35th/readme.md)  
[Message from Tsukasa Hojo(2013)](./hojocn/zz_message/README.md)  
[AH里最受欢迎的10个故事(访谈)(2014～2015)](./hojocn/ah2_vol10_best_EP_10/readme.md)  
[北条司访谈: "Until the very last moment"(2015)](./hojocn/zz_until-the-very-last-moment/README.md)  
[北条司访谈: "Music to my Eyes"(2015)](./hojocn/zz_music_to_my_eyes/README.md)  
[井上武彦 & 北条司：漫画师徒 - 独家报道！(2017)](./hojocn/zz_takehiko_hojo/readme.md)  
[北条司和井上雄彦谈到了那部杰作的另一面!　一场极好的师徒对话已经实现了!(2017)](./hojocn/zz_ddnavi.com/2017-02-06.md)  
[一个无与伦比的好色之徒! 传说中的清道夫冴羽獠的32年! 从《城市猎人》到《天使心》!(2017)](./hojocn/zz_ddnavi.com/2017-09-11.md)  
[成为漫画家 之二 北条司(2018)](./hojocn/zz_making_a_mangaka/readme.md)  
[“剧场版城市猎人<新宿私家侦探>”专访  北条司（原作者）x神谷明(2019)](./hojocn/ch_movie01/readme.md)  
[从《城市猎人》到《天使心》的结束! 冴羽獠相伴的32年【采访】(2019)](./hojocn/zz_ddnavi.com/2019-03-01.md)  
[北条司于60岁当导演！ 无对白的《天使印记》的吸引力是什么？(2019)](./hojocn/zz_moviewalker.jp/2019-11-16(2).md)  
[鲁邦三世VSCat'sEye - 週刊文春(2022)](./hojocn/zz_weeklyBunShun/zz_interview_2022.md)  


## 其它:  
[关于北条司最喜欢的5本书](./hojocn/hojo_best5books/readme.md)  
[北条司作品中的重要日期](./hojocn/hojo_big_days_in_the_works/hojo_big_days_in_the_works.md)  
[北条司作品研究纲领](./hojocn/hojo_program/hojo_program.md)  
[北条司作品里某些人物的相貌原型(猜测)](./hojocn/hojo_prototype/hojo_prototype.md)  
[连载漫画作品写实程度的一种衡量方法](./hojocn/hojo_realistic/readme.md)  
[关于北条司的签名](./hojocn/hojo_signature/readme.md)  

[“日本福冈县的北条氏“ 与 “日本历史上的北条氏“](./hojocn/fc_surname_vs_jap_surname/surname_hojo.md)  
[北条司签售会(2015.07.25)](./hojocn/zz_AutographSessionWithHojoSensei/readme.md)  
[与北条司相关的论文](./hojocn/zz_papers/zz_papers.md)  
[北条司作品卷首语](./hojocn/zz_preambles/readme.md)  


## 转载、翻译:  
[CH同人 - 飛ばない鳥は(by たくさんの大好きを。)](./hojocn/zz_9393279339.amebaownd.com/the_flightless_bird.md)  
[[zz]animenewsnetwork.com上与北条司相关的新闻](./hojocn/zz_animenewsnetwork.com/readme.md)    
[[zz]geminight.com上和北条司及其作品相关的信息](./hojocn/zz_ce_geminight/img_geminight.md)  
[Margaret Atwood的小说《Cat's Eye》 ](./hojocn/zz_ce_margaret/catseye_margaret.md)  
[[zz]www.paffio.it上和北条司及其作品相关的信息](./hojocn/zz_ce_paffio/paffio.md)  
[[zz]《CityHunter 新宿 Private Eyes》剧场版相关照片](./hojocn/zz_ch_private_eyes_roadshow/zz_ch_private_eyes_roadshow.md)  
[[zz]cityhunter-movie.com上和北条司及其作品相关的信息](./hojocn/zz_cityhunter-movie.com/readme.md)  

[[zz]ddnavi.com上和北条司及其作品相关的信息](./hojocn/zz_ddnavi.com/):  
[在《城市猎人》的"天使心 "平行世界中，充满爱的生活的秘密是什么？](./hojocn/zz_ddnavi.com/2014-09-24.md)  
["漫画Gacha"，一个可以让你免费阅读最新一期"Comic Zenon "的Gacha应用程序，现在可以使用了!](./hojocn/zz_ddnavi.com/2014-11-12.md)  
[美纪的眼睛有什么神秘的力量？ 《天使之心第二季》新篇章《ミキビジョン編》不容错过!](./hojocn/zz_ddnavi.com/2015-03-07.md)  
[庆祝《城市猎人》诞生30周年! 熟悉而华丽的演员们再次聚首!](./hojocn/zz_ddnavi.com/2015-06-26.md)  
[北条司："我很惊讶还有人支持我们" 30周年"城市猎人"×35周年"吉祥寺公园"特别活动!](./hojocn/zz_ddnavi.com/2015-07-16.2.md)  
[冴羽獠出现在真人秀节目中! 热门漫画《天使心》将被改编为电视剧!](./hojocn/zz_ddnavi.com/2015-07-16.md)  
[纪念电视剧改编的《天使心》特别版! 包括上川隆也和北条司的对话](./hojocn/zz_ddnavi.com/2015-10-15.md)  
[原作粉丝必看的作品《城市猎人》30周年纪念 北条司原画展](./hojocn/zz_ddnavi.com/2015-10-17.md)  
[电视剧《天使心》即将迎来它的最后一集! 上川隆也和北条司也出现在粉丝答谢会上!](./hojocn/zz_ddnavi.com/2015-12-05.md)  
[东京吉祥寺最好的布丁! CAFE ZENON的特色产品 "YAMANAKA布丁"现在以漫画家北条司绘制的包装在有限的时间内出售!](./hojocn/zz_ddnavi.com/2016-09-18.md)  
[北条司和井上雄彦谈到了那部杰作的另一面!　一场极好的师徒对话已经实现了!](./hojocn/zz_ddnavi.com/2017-02-06.md)  
[《天使之心》第二季：三大家庭爱情场景](./hojocn/zz_ddnavi.com/2017-04-24.md)  
[小室哲也："'GET WILD'是独自行走的最亲切的歌" 庆祝《天使心》的完结! ](./hojocn/zz_ddnavi.com/2017-08-25.md)  
[一个无与伦比的好色之徒! 传说中的清道夫冴羽獠的32年! 从《城市猎人》到《天使心》!](./hojocn/zz_ddnavi.com/2017-09-11.md)  
["在北条司先生面前流泪是一段美好的回忆"，这是绘制《城市猎人》的作者意想不到的真面目](./hojocn/zz_ddnavi.com/2017-10-01.md)  
[新《城市猎人》剧场版动画获得巨大反响! "我太高兴了，我哭了"。](./hojocn/zz_ddnavi.com/2018-03-25.md)  
[新《城市猎人》电影动画专题报道--冴羽獠的魅力在本书中得到充分展现!](./hojocn/zz_ddnavi.com/2018-03-29.md)  
[《城市猎人》现在有了可视化的顺口溜! 流行的警察小说《警視庁公安J》系列](./hojocn/zz_ddnavi.com/2018-04-20.md)  
[如果你在《城市猎人》的世界里转世，你希望看到什么？](./hojocn/zz_ddnavi.com/2018-07-25.md)  
["想起以前的日子，我就哭了......" 粉丝们对新版电影"城市猎人 "的特别报道视频感到兴奋](./hojocn/zz_ddnavi.com/2018-08-08.md)  
["当年的獠回来了！"。很多人对电影"城市猎人"的预告片感到满意](./hojocn/zz_ddnavi.com/2018-12-26.md)  
[你可以阅读《城市猎人》的全部32卷! 劇場版门票和毛毯赠送!](./hojocn/zz_ddnavi.com/2019-02-01.md)  
[从《城市猎人》到《天使心》的结束! 冴羽獠相伴的32年【采访】](./hojocn/zz_ddnavi.com/2019-03-01.md)  
[《城市猎人外传》海坊主的旋风来了! 捕捉不良顾客的"XYZ"!](./hojocn/zz_ddnavi.com/2019-03-09.2.md)  
[剧场版《城市猎人》中没有描写的情节也被添加进来。 粉丝们期待已久的小说版现在开始销售了!](./hojocn/zz_ddnavi.com/2019-03-09.md)  
[是什么让《劇場版城市猎人》在粉丝中引起如此大的反响？](./hojocn/zz_ddnavi.com/2019-03-10.md)  
[挑战自己，用冴羽獠解开一个个谜团! 《城市猎人：目标天使的安魂曲》已经开始。 让我们在吉祥寺的街道上奔跑吧!](./hojocn/zz_ddnavi.com/2020-01-26.md)  
[和北条司作品相关的书](./hojocn/zz_ddnavi.com/books.md)  

[[zz]edition-88.com上和北条司及其作品相关的信息](./hojocn/zz_edition-88.com/readme.md)  

[[zz]fanfiction上的同人作品](./hojocn/zz_fanfiction/):  
- [[FC]"我也想道歉"](./hojocn/zz_fanfiction/fc_I_too_have_to_apologize.md)  

[[zz]Family Compo 里的转义词语（tvtropes版）](./hojocn/zz_fc_tropes/tv_tropes.md)  
[[zz]Family Compo 里的转义词语（allthetropes版）](./hojocn/zz_fc_tropes/all_the_tropes.md)  
[[zz]《Forever Tsukasa Hojo(永远的北条司)》](./hojocn/zz_forever_tsukasa_hojo/readme.md)  
[[zz]第23届Golden Romics之北条司](./hojocn/zz_golden_romics_xxiii/README.md)  

[[zz]HFC上的同人作品](./hojocn/zz_hfc)：  
- [[FC]Une journée de folie!](./hojocn/zz_hfc/fanficition_fc_a_day_of_madness.md)  
- [[FC]Papaaa](./hojocn/zz_hfc/fanficition_fc_papaaa.md)  
- [[FC]FAMILY COMPO: LE STAGIAIRE](./hojocn/zz_hfc/fanficition_fc_trainee.md)  

[[zz]北条司笔下的王祖贤 & 北条司照片](./hojocn/zz_hojo_wangzx/readme.md)    
[[zz]japan-expo-paris.com上和北条司及其作品相关的信息](./hojocn/zz_japan_expo_paris/readme.md)  
[[zz]日本Jump粉丝排名了那些想继续看到的80年代漫画](./hojocn/zz_jump_reboot/readme.md)  
[2019熊本国际漫画营的报道](./hojocn/zz_kimc2019/readme.md)  

[[zz]manga-audition.com上和北条司及其作品相关的信息](), ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96828))  :    
- [成为漫画家 之二 北条司(2018)](./hojocn/zz_making_a_mangaka/readme.md)  
- [Message from Tsukasa Hojo(2013)](./hojocn/zz_message/README.md)  
- [北条司访谈: "Music to my Eyes"(2015)](./hojocn/zz_music_to_my_eyes/README.md)  
- [SMA’s Italian Job](./hojocn/zz_sma_italian_job/readme.md)  
- [井上武彦 & 北条司：漫画师徒 - 独家报道！(2017)](./hojocn/zz_takehiko_hojo/readme.md)  
- [“双页胜过单页” / JapaneseManga101 ＃002 双页展开](./hojocn/zz_two_is_better_than_one/readme.md)  
- [北条司访谈: "Until the very last moment"(2015)](./hojocn/zz_until-the-very-last-moment/README.md)  

[[zz]store.line.me上的表情包](./hojocn/zz_store.line.me/ch_ah_faces.md)  
[[zz]漫画杂志ALLMan的封面](./hojocn/zz_manga-allman_covers/readme.md)  
[[zz]少年Jump的一些封面](./hojocn/zz_sjump_covers/)  

[[zz]moviewalker.jp上和北条司及其作品相关的信息](./hojocn/zz_moviewalker.jp/):  
[あのスゴ腕スイーパーが帰ってくる！「シティーハンター」長編アニメーション映画として2019年に復活！](./hojocn/zz_moviewalker.jp/2018-03-19.md)  
[法国“城市猎人”的真人版非常受欢迎！ 北条司的粉丝们涌向巴黎动漫展](./hojocn/zz_moviewalker.jp/2018-11-05.md)  
[劇場版『シティーハンター』エンディングは「Get Wild」飯豊まりえとチュート徳井も参加決定](./hojocn/zz_moviewalker.jp/2018-12-13.md)  
[キャッツアイ3姉妹が『劇場版シティーハンター 』に登場！ 衝撃の最新予告映像](./hojocn/zz_moviewalker.jp/2019-01-11.md)  
[神谷明「シティーハンター」の“ルパン化”を希望！キャッツアイ・戸田恵子と揃い踏み](./hojocn/zz_moviewalker.jp/2019-02-09.md)  
[“City Hunter”北条司首次挑战真人电影“总导演”！ “天使标志”制作开始](./hojocn/zz_moviewalker.jp/2019-03-20.md)  
[松下奈緒和藤冈靛对北条司的"用故事板代替剧本"印象深刻! 潜入《天使标志》的拍摄现场](./hojocn/zz_moviewalker.jp/2019-07-01.md)  
[伝説の始末屋こと冴羽獠がフランスから逆上陸！実写版『シティーハンター』特報映像＆ポスターが到着](./hojocn/zz_moviewalker.jp/2019-08-17.md)  
[冴羽リョウの吹替えは山寺宏一！実写版『シティーハンター』豪華声優キャスト発表「リョウは山ちゃんに任せた！」](./hojocn/zz_moviewalker.jp/2019-09-19.md)  
[山寺宏一＆沢城みゆきの“リョウと香”がお披露目！フランス版『シティーハンター』予告が完成](./hojocn/zz_moviewalker.jp/2019-10-08.md)  
[監督業にも挑戦！『シティーハンター』漫画家の北条司が映画界でもアツい！](./hojocn/zz_moviewalker.jp/2019-11-15.md)  
[ディーン・フジオカ、松下奈緒のチェロの腕前に感嘆！「僕もいまバイオリンをやっているので…」](./hojocn/zz_moviewalker.jp/2019-11-16.md)  
[北条司于60岁当导演！ 无对白的《天使印记》的吸引力是什么？](./hojocn/zz_moviewalker.jp/2019-11-16(2).md)  

[[zz]FC的两个法语版(Panini版、Tonkam版)的比较](./hojocn/zz_panini_vs_tonkam/README.md)  
[[zz]ramenparados.com上和北条司及其作品相关的信息](./hojocn/zz_ramenparados):  
- [Arechi漫画获得《城市猎人》漫画的授权](./hojocn/zz_ramenparados/2020-10-27.md)  

[[zz]4月，猫眼与城市猎人之父 北条司将会出席Romics大会](./hojocn/zz_romics23/README.md)  

[[zz]sina.com.cn上和北条司及其作品相关的信息](./hojocn/zz_sina.com.cn):  
- [[zz]日本写实画风漫画家总结](./hojocn/zz_sina.com.cn/artists_with_realistic_drawing_style.md)  

[[zz]weibo.cn上和北条司及其作品相关的信息](./hojocn/zz_weibo.cn/readme.md)  

[times.abema.tv上和北条司及其作品相关的信息](./hojocn/zz_times.abema.tv):  
- [佐藤蓝子20年来首次和北条司重逢，并为不可思议的联系而兴奋](./hojocn/zz_times.abema.tv/readme.md)  

[Twitter上和北条司及其作品相关的信息](./hojocn/zz_twitter/readme.md)  
- [pound_poundy的同人作品《つなぐ》 ](./hojocn/zz_twitter.com__pound_poundy/)  

[[zz][Todo]natalie.mu上和北条司及其作品相关的信息](./hojocn/ch_movie01/links.txt)  
[The History Of Japan](./hojocn/history_japan/readme.md)  
[[zz]Category 東京都港区の歴史 - Wikipedia](./hojocn/history_japan/Category%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%AF%E5%8C%BA%E3%81%AE%E6%AD%B4%E5%8F%B2%20-%20Wikipedia.html)  
[[zz]Hipster Manga: Angel Heart](./hojocn/zz_womenwriteaboutcomics.com/hipster_manga_ah.md)  

[[zz]F.COMPO応援隊(备份+中英文翻译)](./hojocn/zz_www.biglobe.ne.jp_julianus/readme.md)， 
[[zz]北条司応援隊(备份+中英文翻译)](./hojocn/zz_www.biglobe.ne.jp_julianus/support.md)  
[[zz]北条司研究序说(中文翻译)](./hojocn/zz_htkj_cn/readme.md)  
[[zz]历代《少年Jump》周刊的信息，以及其中对北条司的介绍](./hojocn/zz_www.biwa.ne.jp/readme.md)  
[[zz]官方网站的一些中文翻译](./hojocn/zz_www.hojo-tsukasa.com/readme.md)  
[[zz]海坊主闭眼识世界的能力是真实存在的](./hojocn/zz_blind/readme.md)  



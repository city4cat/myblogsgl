
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](fc_face_side.md)  



# FC的画风-面部(侧面)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96880))  


- 面部素描参考图[1]：  
    - 青年：  
        ![](side_img/Drawing.The.Head.hands.p127.female.teenage.jpg) 
        ![](side_img/Drawing.The.Head.hands.p126.male.teenage.jpg)  

    - 成年：  
        ![](side_img/Drawing.The.Head.hands.p077.female.scaled.jpg) 
        ![](side_img/Drawing.The.Head.hands.p043.male.jpg)  


- 美容整形外科(Aesthetic Plastic Surgery)的侧面mark点[2]：  
![](side_img/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig4.Asian.jpg) 
![](side_img/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig4.Caucasian.jpg)  
(FIGURE4 in [2], 左图为亚洲人种(Asian)，右图为高加索人种(Caucasian)(欧美)。可以看出，高加索人种眼窝相对较深。)   

图2中的mark点的注解和中文翻译:  
    - Tragion (t) 耳屏点: the most anterior portion of the supratragal notch),  
    - glabella (g) 眉间点: the most prominent or anterior point of the forehead between the eyebrows,  
    - sellion (se) 鼻梁点: the most concave point in the tissue overlying the area of the frontonasal suture,  
    - pronasale (prn) 鼻尖: the most prominent or anterior projection point of the nose,  
    - columella breakpoint (c) (鼻)小柱断点 the highest point of the columella or breakpoint of Daniel,  
    - subnasale (sn) 鼻下点: the junctional point of the columella and the upper cutaneous lip, alar curvature point or alar  
    crest point (ac) 波峰点: the most lateral point in the curved base line of each ala, indicating the facial insertion of the nasal wing base, 鼻翼根与面部连接处的曲线的最外侧点  
    - labiale superius (ls) 上唇中点: the mucocutaneous junction and vermilion border of the upper lip,  
    - labiale inferius (li) 下唇中点: the mucocutaneous junction and vermilion border of the lower lip,  
    - cheilion (ch), 口角点  
    - pogonion (pg) 颏前点: the most anterior point of the soft-­tissue chin,  
    - distant chin (dc): the farthest point from the fiducial t. 下巴上距离t点最远的点。  
    - menton (m): the most inferior portion of the anterior chin. Points m1, m2, m3, and dc were not usually defined in articles; only I defined the landmarks.  颏下点/颔下点 (gnathion (gn)[3])。  
        The arbitrary points help us to understand the actual differences in the profile faces of Caucasians and Asians.  
        - m1: the point where the mandibular下颌骨 contour line meets the horizontal line that extends from the fiducial ls,  
        - m2: the intersecting line of the mandibular contour with the horizontally extended line from the fiducial li;  
        - m3: the intersecting line of the mandibular contour with the horizontally extended line from the fiducial pg;  
    

- FC里的角色（比如雅彦）的大多数侧面的眼睛位置更类似于亚洲人(角色的眼窝没有高加索人种那么深)；所以，使用上图里的亚洲人种的mark点位置为基准。  
![](side_img/Asian.mark.jpg)  

- FC里角色有左侧脸和右侧脸。这里为了方便比较，统一使用某一侧的脸进行比较。暂时选用左侧脸(并没有特别的原因。当然可以统一使用右侧脸)，所以，将FC里右侧脸图片水平反转为左侧脸(反转后的图片添加后缀'r')。  


- **紫苑(Shion)-侧面**  
FC里该角色的侧面镜头约有471张。由于图片太多，为了减少工作量，只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 14_277_6_, 14_272_0, 14_123_3, 14_063_4, 14_048_5__r, 14_003_0, 14_003_0, 11_197_3, 10_064_4_, 06_054_6, 02_100_2, 
        - 特点： 所选图片里，属于这组里的数量最多，所以选定该组为基准。叠加后的图片，面部轮廓清晰，说明线条一致性高。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_shion_basis.png) 
        ![](side_img/face_side_shion_basis_mark.jpg)  
        - 紫苑basis(黑色) vs 雅彦basis(红色)：  
            - 紫苑basis的下巴(点pg、dc)没有雅彦的凸；  
        ![](side_img/face_side_cmp_masahiko_basis_vs_shion_basis.jpg)  

    - low-bridge:  
        - 图片： 14_290_3_, 14_063_6。
        - 特点： 比basis组的鼻梁低。  
        - 这些图片叠加后的效果、和basis(红色)比较：  
        ![](side_img/face_side_shion_low-bridge.png) 
        ![](side_img/face_side_cmp_shion_basis_vs_shion_low-bridge.jpg)    

    - kiss:  
        - 图片： 14_161_3_, 08_001_0。
        - 特点： 亲吻时撅嘴(和basis的点ls、li有差异)。  
        - 这些图片叠加后的效果、和basis(红色)比较：  
        ![](side_img/face_side_shion_kiss.png) 
        ![](side_img/face_side_cmp_shion_basis_vs_shion_kiss.jpg)    

    - short-nose:  
        - 图片： 14_146_1__r, 14_132_0__r, 14_131_4__r, 14_285_4__r, 14_284_0__r, 14_282_5_r, 12_048_6__r, 11_040_1__r, 08_005_5__r, 03_141_0__r, 
        - 特点： 鼻尖略短(相比basis的点prn)；嘴部、下巴有差异(肯可能嘴部、下巴有动作)。  
        - 这些图片叠加后的效果、和basis(红色)比较：  
        ![](side_img/face_side_shion_short-nose.png) 
        ![](side_img/face_side_cmp_shion_basis_vs_shion_short-nose.jpg)    

    - long-nose:  
        - 图片： 07_077_6_，05_112_2_，02_019_5，
        - 特点： 鼻尖略长(相比basis的点prn)。  
        - 这些图片叠加后的效果、和basis(红色)比较：  
        ![](side_img/face_side_shion_long-nose.png) 
        ![](side_img/face_side_cmp_shion_basis_vs_shion_long-nose.jpg)    




- **塩谷/盐谷(Shionoya)(紫苑男装)-侧面**  
FC里该角色的侧面镜头约有96张。由于图片太多，为了减少工作量，只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片：10_125_2__r, 13_044_1__r(排除相机视角偏下的镜头：14_240_0__r,14_181_0__r,12_192_5__r,11_003_0).    
        - 特点：.      
        - 这些图片叠加后的效果、附加参考比例、附加亚洲人种侧面mark点：  
        ![](side_img/face_side_shya_basis.png) 
        ![](side_img/face_side_shya_basis_mark.jpg)  

    - curve-bridge:  
        - 图片：14_102_3, 14_055_4, 13_190_2(排除角色眼睛偏上的镜头：14_119_6).    
        - 特点：鼻梁弯曲。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_shya_curve-bridge.png) 
        ![](side_img/face_side_shya_basis_vs_curve-bridge.jpg)  

    - -se:  
        - 图片：14_007_5，12_116_1.    
        - 特点：se点凹。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_shya_-se.png) 
        ![](side_img/face_side_shya_basis_vs_-se.jpg)  


- **雅彦(Masahiko)-侧面**  
FC里该角色的侧面镜头约有228张，其中大部分可以分为以下几组：  
    - basis:  
        - 图片：14_222_4__r, 11_184_5__r, 08_109_0__r, 07_082_6__, 07_022_6__r, 05_153_4__r, 05_152_1__r, 05_048_7__r, 03_129_2__r, 01_129_1, 14_086_1__r, 12_112_5__r, 11_132_7__r, 07_022_1__r, 08_100_2__r, 10_176_4__r, 02_048_6__r, 01_156_1__r, 06_014_4__r, 05_139_0__r.    
        - 特点：这组图最接近上述亚洲人种的参考图(所以该组被选作雅彦侧脸的基准)。有些图(14_222_4__r,11_184_5__r)里眼睛的位置偏上。    
        - 这些图片叠加后的效果、附加参考比例、附加亚洲人种侧面mark点：  
        ![](side_img/face_side_masahiko_basis.jpg) 
        ![](side_img/face_side_masahiko_basis_mark.jpg)  

    - +ls_+li:  
        - 图片：08_107_0_, 05_133_3.    
        - 特点：比basis组的嘴唇凸(ls点和li点凸)。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_masahiko_+ls_+li.png) 
        ![](side_img/face_side_cmp_masahiko_basis_vs_+ls_+li.jpg)  

    - lower-nose-tip:  
        - 图片：07_099_0__r, 07_082_7__r.    
        - 特点：鼻尖稍低。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_masahiko_lower-nose-tip.png) 
        ![](side_img/face_side_cmp_masahiko_basis_vs_lower-nose-tip.jpg)  

    - lower-nose-tip_+ls_+li:  
        - 图片：12_133_0__, 06_024_2_, 06_015_5__r.    
        - 特点：比lower-nose-tip组的嘴唇凸(ls点和li点凸)。    
        - 这些图片叠加后的效果，以及和lower-nose-tip(红色)比较：  
        ![](side_img/face_side_masahiko_lower-nose-tip_+ls_+li.png) 
        ![](side_img/face_side_cmp_masahiko_lower-nose-tip_vs_lower-nose-tip_+ls_+li.jpg)  

    - long-nose:  
        - 图片：05_137_5__r, 05_127_0__, 03_129_6__r.    
        - 特点：鼻子(尖)长(prn点凸出)。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_masahiko_long-nose.png) 
        ![](side_img/face_side_cmp_masahiko_basis_vs_long-nose.jpg)  

    - curved-bridge:  
        - 图片：07_096_1__, 07_054_1__, 07_039_1__r, 05_139_4__, 02_167_0__r.    
        - 特点：鼻梁塌、弯曲，se点凹。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_masahiko_curved-bridge.png) 
        ![](side_img/face_side_cmp_masahiko_basis_vs_curved-bridge.jpg)  

    - caucasian:  
        - 图片：04_191_4__, 03_124_6__r.    
        - 特点：眼睛后移(凹)，类似高加索人种(Caucasian)。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_masahiko_caucasian.png) 
        ![](side_img/face_side_cmp_masahiko_basis_vs_caucasian.jpg)  

    - +ls_+li_+pg_+dc:  
        - 图片：06_005_6__, 06_005_0__r, 05_152_6_, 03_125_1__r.    
        - 特点：嘴(点ls、li、pg、dc)凸。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_masahiko_+ls_+li_+pg_+dc.png) 
        ![](side_img/face_side_cmp_masahiko_basis_vs_+ls_+li_+pg_+dc.jpg)  

    - long-nose-bridge:  
        - 图片：08_107_1__, 07_038_5_, 05_106_3.    
        - 特点：长鼻梁(点g凹)。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_masahiko_long-nose-bridge.png) 
        ![](side_img/face_side_cmp_masahiko_basis_vs_long-nose-bridge.jpg)  

    - +se:  
        - 图片：13_190_7__r, 13_190_1__r, 06_186_1__r.    
        - 特点：点se凸。    
        - 这些图片叠加后的效果，以及和basis(红色)比较：  
        ![](side_img/face_side_masahiko_+se.png) 
        ![](side_img/face_side_cmp_masahiko_basis_vs_+se.jpg)  

    - 总结：  
        - 眼睛的水平位置没有严格遵循面部比例。  
        - 雅彦的侧面里，有很多是画了喉结的。  



- **雅美(Masami)-侧面**  
FC里该角色的侧面镜头约有144张。由于图片太多，为了减少工作量，只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片：14_259_2__r, 14_235_0, 12_005_3__r. (排除相机视角偏下的镜头：10_140_3__r，排除相机视角偏下的镜头：14_257_4)  
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_masami_basis.png) 
        ![](side_img/face_side_masami_basis_mark.jpg)  
        - 雅彦basis(红色) vs 雅美basis:  
        ![](side_img/face_side_masahiko_basis_vs_masami_basis.jpg)  

    - -li_-pg_-dc:  
        - 图片：03_033_3__r (排除相机视角偏下的镜头：10_131_6, 04_124_3__r, 11_055_1__r)    
        - 特点：li、pg、dc点凹。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_masami_-li_-pg_-dc.png) 
        ![](side_img/face_side_masami_basis_vs_-li_-pg_-dc.jpg)  

    - -g_-li_-pg_-dc:  
        - 图片：14_179_3 (排除相机视角偏下的镜头：14_241_2)    
        - 特点：g、li、pg、dc点凹。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_masami_-g_-li_-pg_-dc.png) 
        ![](side_img/face_side_masami_basis_vs_-g_-li_-pg_-dc.jpg)  

    - +ls_+li:  
        - 图片：14_231_4，12_072_5__r，11_159_3__r    
        - 特点：ls、li点凸（撅嘴）。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_masami_+ls_+li.png) 
        ![](side_img/face_side_masami_basis_vs_+ls_+li.jpg)  

    - +g_-se:  
        - 图片：14_261_0__r，14_256_2   
        - 特点：g点凸、se点凹（皱眉）。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_masami_+g_-se.png) 
        ![](side_img/face_side_masami_basis_vs_+g_-se.jpg)  

    - -g_-se:  
        - 图片：11_180_2__r (排除相机视角偏下的镜头：12_014_2_)   
        - 特点：g点凹、se点凹（鼻梁长）。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_masami_-g_-se.png) 
        ![](side_img/face_side_masami_basis_vs_-g_-se.jpg)  


- **若苗空(Sora)-侧面**  
FC里该角色的侧面镜头约有189张。由于图片太多，为了减少工作量，只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片：03_164_1, 04_074_5, 07_015_1, 04_057_0    
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_sora_basis.png) 
        ![](side_img/face_side_sora_basis_mark.jpg)  
        - 雅彦basis(红色) vs 若苗空basis(se点凸):  
        ![](side_img/face_side_cmp_masahiko_basis_vs_sora_basis.jpg)  

    - anger:  
        - 图片：07_042_4, 06_120_2, 01_190_2   
        - 特点：发怒。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_sora_anger.png) 
        ![](side_img/face_side_cmp_sora_basis_vs_anger.jpg)  

    - flat:  
        - 图片：03_182_2, 07_014_1，   
        - 特点：prn点凹，g、pg、dc点凸。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_sora_flat.png) 
        ![](side_img/face_side_cmp_sora_basis_vs_flat.jpg)  

    - long-bridge:  
        - 图片：09_151_6_, 09_035_5, 08_085_0    
        - 特点：鼻梁长。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_sora_long-bridge.png) 
        ![](side_img/face_side_cmp_sora_basis_vs_long-bridge.jpg)  


- **若苗紫(Yukari)-侧面**  
FC里该角色的侧面镜头约有306张。由于图片太多，为了减少工作量，只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片：12_094_3__r, 05_073_1__r, 05_028_4__r, 01_080_1__r, 01_030_1__r,    
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_yukari_basis.png) 
        ![](side_img/face_side_yukari_basis_mark.jpg)  
        - 雅彦basis(红色) vs 若苗紫basis(se点凸):  
        ![](side_img/face_side_cmp_masahiko_basis_vs_yukari_basis.jpg)  
        - 紫苑basis(红色) vs 若苗紫basis(se点、pg点、dc点凸):  
        ![](side_img/face_side_cmp_shion_basis_vs_yukari_basis.jpg)  

    - low-bridge:  
        - 图片：05_179_6__r, 02_191_0, 02_171_1__r,    
        - 特点：鼻梁比basis组低。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yukari_low-bridge.png) 
        ![](side_img/face_side_cmp_yukari_basis_vs_low-bridge.jpg)  

    - low-bridge2:  
        - 图片：05_179_6__r, 02_191_0, 02_171_1__r,    
        - 特点：鼻梁比low-bridge组更低。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yukari_low-bridge2.png) 
        ![](side_img/face_side_cmp_yukari_basis_vs_low-bridge2.jpg)  


- **浅冈叶子(Yoko)-侧面**  
FC里该角色的侧面镜头约有210张。由于图片太多，为了减少工作量，只处理特写镜头、大画面、重要情节的镜头。特写镜头很多。可以分为以下几组：  
    - basis:  
        - 图片：06_011_0__r, 07_122_4__r, 11_071_1__r, 11_074_4，13_098_2，13_160_3， 14_254_5， 
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_yoko_basis.png) 
        ![](side_img/face_side_yoko_basis_mark.jpg)  

    - +g_-ls_-li_-pg_-dc:  
        - 图片： 02_063_6，02_093_6，03_057_4__r，11_019_5，11_022_0，14_255_2
        - 特点：g点凸，ls、li、pg、dc点凹。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_+g_-ls_-li_-pg_-dc.png) 
        ![](side_img/face_side_yoko_basis_vs_+g_-ls_-li_-pg_-dc.jpg)  

    - +g_-li_-pg2_-dc2:  
        - 图片： 06_017_1__r，13_113_2
        - 特点：g点凸，li、pg、dc点凹；相比+g_-ls_-li_-pg_-dc组，在pg、dc点更凹。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_+g_-li_-pg2_-dc2.png) 
        ![](side_img/face_side_yoko_basis_vs_+g_-li_-pg2_-dc2.jpg)  

    - +g_+c_-ls_-li_-pg_-dc:  
        - 图片： 13_176_1，09_010_0
        - 特点：g、c点凸，ls、li、pg、dc点凹。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_+g_+c_-ls_-li_-pg_-dc.png) 
        ![](side_img/face_side_yoko_basis_vs_+g_+c_-ls_-li_-pg_-dc.jpg)  

    - +g_-ls_-li_-pg2_-dc2:  
        - 图片： 04_132_1__r，05_099_7__r，05_148_3__r，05_150_4__r，13_087_5，
        - 特点：g点凸，ls、li、pg、dc点凹。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_+g_-ls_-li_-pg2_-dc2.png) 
        ![](side_img/face_side_yoko_basis_vs_+g_-ls_-li_-pg2_-dc2.jpg)  

    - -se_-sn_-ls_-li_-pg_-dc:  
        - 图片： 14_259_4，13_187_5，08_147_0
        - 特点：se、sn、ls、li、pg、dc点凹。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_-se_-sn_-ls_-li_-pg_-dc.png) 
        ![](side_img/face_side_yoko_basis_vs_-se_-sn_-ls_-li_-pg_-dc.jpg)  

    - -se_-sn_-ls_-li_-pg_-dc_long-nose:  
        - 图片： 06_017_6，04_191_6，03_067_2_
        - 特点：se、sn、ls、li、pg、dc点凹，鼻梁长。    
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_-se_-sn_-ls_-li_-pg_-dc_long-nose.png) 
        ![](side_img/face_side_yoko_basis_vs_-se_-sn_-ls_-li_-pg_-dc_long-nose.jpg)  

    - -se_-sn_-ls_-li_-pg_-dc_long-nose2:  
        - 图片： 06_100_2，06_006_4，05_153_4，
        - 特点：se、sn、ls、li、pg、dc点凹，鼻梁长。相比-se_-sn_-ls_-li_-pg_-dc_long-nose，g点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_-se_-sn_-ls_-li_-pg_-dc_long-nose2.png) 
        ![](side_img/face_side_yoko_basis_vs_-se_-sn_-ls_-li_-pg_-dc_long-nose2.jpg)  

    - -g_-ls_-li_-pg_-dc:  
        - 图片： 13_174_0，13_169_0，13_174_0，05_157_1，05_161_5，
        - 特点：g、ls、li、pg、dc点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_-g_-ls_-li_-pg_-dc.png) 
        ![](side_img/face_side_yoko_basis_vs_-g_-ls_-li_-pg_-dc.jpg)  

    - +g_-sn_-ls_-li_-pg_-dc_curve-bridge:  
        - 图片： 11_015_7，06_015_5，
        - 特点：g点凸，sn、ls、li、pg、dc点凹，鼻梁弯曲。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoko_+g_-sn_-ls_-li_-pg_-dc_curve-bridge.png) 
        ![](side_img/face_side_yoko_basis_vs_+g_-sn_-ls_-li_-pg_-dc_curve-bridge.jpg)  


- **浩美(Hiromi)-侧面**
FC里该角色的侧面镜头约有28张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片：09_051_3__r， 01_184_1__r
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_hiromi_basis.png) 
        ![](side_img/face_side_hiromi_basis_mark.jpg)  

    - -se_-dc:  
        - 图片： 09_049_3，05_067_2_
        - 特点： se、dc点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_hiromi_-se_-dc.png) 
        ![](side_img/face_side_hiromi_basis_vs_-se_-dc.jpg)  

    - -pg_-dc:  
        - 图片： 14_029_3, 05_129_6
        - 特点： pg、dc点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_hiromi_-pg_-dc.png) 
        ![](side_img/face_side_hiromi_basis_vs_-pg_-dc.jpg)  


- **熏(Kaoru)-侧面**
FC里该角色的侧面镜头约有142张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片：09_157_0, 08_075_2, 07_138_3__r
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_kaoru_basis.png) 
        ![](side_img/face_side_kaoru_basis_mark.jpg)  
        - 雅彦basis(红色) vs 熏basis(se点凸):  
        ![](side_img/face_side_kaoru_basis_vs_masahiko_basis.jpg)  

    - -g_-se:  
        - 图片： 10_007_5，07_168_0
        - 特点： g、se点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kaoru_-g_-se.png) 
        ![](side_img/face_side_kaoru_basis_vs_-g_-se.jpg)  

    - -li_-pg_-dc:  
        - 图片： 10_007_5，07_168_0
        - 特点： li、pg、se点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kaoru_-li_-pg_-dc.png) 
        ![](side_img/face_side_kaoru_basis_vs_-li_-pg_-dc.jpg)  

    - +prn_+c:  
        - 图片： 09_188_0，09_151_1
        - 特点： prn、c点凸      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kaoru_+prn_+c.png) 
        ![](side_img/face_side_kaoru_basis_vs_+prn_+c.jpg)  

    - +se_-pg_-dc:  
        - 图片： 09_117_6，09_117_3_
        - 特点： se点凸；pg、dc点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kaoru_+se_-pg_-dc.png) 
        ![](side_img/face_side_kaoru_basis_vs_+se_-pg_-dc.jpg)  

    - -pg2_-dc2:  
        - 图片： 09_117_6，09_117_3_
        - 特点： pg、dc点凹得程度更大。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kaoru_-pg2_-dc2.png) 
        ![](side_img/face_side_kaoru_basis_vs_-pg2_-dc2.jpg)  

    - 13_043_6，13_047_2，13_047_6，13_062_6，鼻尖上翘：  
        ![](side_img/13_043_6__.jpg) 
        ![](side_img/13_047_2__.jpg) 
        ![](side_img/13_047_6__.jpg) 
        ![](side_img/13_062_6__.jpg)  


- **江岛(Ejima)-侧面**
FC里该角色的侧面镜头约有152张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片：03_073_1, 06_163_6
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_ejima_basis.png) 
        ![](side_img/face_side_ejima_basis_mark.jpg)  
        - 雅彦basis(红色) vs 江岛basis:  
        ![](side_img/face_side_ejima_basis_vs_masahiko_basis.jpg)  

    - +se_-m:  
        - 图片： 09_165_5，06_040_2__r
        - 特点： se点凸、m点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_ejima_+se_-m.png) 
        ![](side_img/face_side_ejima_basis_vs_+se_-m.jpg)  

    - +g:  
        - 图片： 07_098_7__r，12_148_5
        - 特点： se点凸、m点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_ejima_+g.png) 
        ![](side_img/face_side_ejima_basis_vs_+g.jpg)  

    - -se:  
        - 图片： 07_098_7__r，12_148_5
        - 特点： se点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_ejima_-se.png) 
        ![](side_img/face_side_ejima_basis_vs_-se.jpg)  


- **辰巳(Tatsumi)-侧面**
FC里该角色的侧面镜头约有99张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片：09_127_1__r, 09_103_5__r, 07_180_2, 03_125_0
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_tatsumi_basis.png) 
        ![](side_img/face_side_tatsumi_basis_mark.jpg)  
        - 雅彦basis(红色) vs 辰巳basis(下巴大):  
        ![](side_img/face_side_tatsumi_basis_vs_masahiko_basis.jpg)  

    - -m:  
        - 图片： 09_165_5，06_040_2__r
        - 特点： m点下移。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_tatsumi_-m.png) 
        ![](side_img/face_side_tatsumi_basis_vs_-m.jpg)  

    - -prn_open:  
        - 图片： 11_115_4, 11_094_3, 09_167_0__r
        - 特点： prn点凹，张嘴。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_tatsumi_-prn_open.png) 
        ![](side_img/face_side_tatsumi_basis_vs_-prn_open.jpg)  


- **导演-侧面**
FC里该角色的侧面镜头约有28张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 14_113_4__r, 03_099_3__r, 03_092_2, 
        - 特点：。    
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_director_basis.png) 
        ![](side_img/face_side_director_basis_mark.jpg)  
        - 雅彦basis(红色) vs 导演basis(鼻尖凹):  
        ![](side_img/face_side_director_basis_vs_masahiko_basis.jpg)  

    - 12_161_6:  
        - 图片： 12_161_6
        - 特点： prn点凹. 该镜头是特写,但没能和其他图片匹配成组。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_director_12_161_6.png) 
        ![](side_img/face_side_director_basis_vs_12_161_6.jpg)  


- **早纪(Saki)-侧面**  
FC里该角色的侧面镜头约有49张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 10_049_1__r, 10_020_1__r, 09_164_5__r
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_saki_basis.png) 
        ![](side_img/face_side_saki_basis_mark.jpg)  
        - 雅彦basis(红色) vs 早纪basis():  
        ![](side_img/face_side_saki_basis_vs_masahiko_basis.jpg)  

    - -se_-pg_-dc:  
        - 图片： 09_175_4, 09_161_5, 
        - 特点： se、pg、dc点凹。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_saki_-se_-pg_-dc.png) 
        ![](side_img/face_side_saki_basis_vs_-se_-pg_-dc.jpg)  

    - +c_-pg_-dc:  
        - 图片： 10_009_4，09_177_0
        - 特点： prn点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_saki_+c_-pg_-dc.png) 
        ![](side_img/face_side_saki_basis_vs_+c_-pg_-dc.jpg)  

    - +prn_-sn_-ls_-li_-pg_-dc:  
        - 图片： 11_110_0__r，11_107_4
        - 特点： prn点凹。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_saki_+prn_-sn_-ls_-li_-pg_-dc.png) 
        ![](side_img/face_side_saki_basis_vs_+prn_-sn_-ls_-li_-pg_-dc.jpg)  


- **真琴(Makoto)-侧面**  
    - basis:  
        - 图片： 03_008_3
        - 特点：。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_makoto_basis.png) 
        ![](side_img/face_side_makoto_basis_mark.jpg)  
        - 雅彦basis(红色) vs 真琴basis():  
        ![](side_img/face_side_makoto_basis_vs_masahiko_basis.jpg)  

    - +se_+ls_+li_+pg_+dc_+m:  
        - 图片： 01_181_4,09_033_5__r
        - 特点： se、ls、li、pg、dc、m点凸，撅嘴。      
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_makoto_+se_+ls_+li_+pg_+dc_+m.png) 
        ![](side_img/face_side_makoto_basis_vs_+se_+ls_+li_+pg_+dc_+m.jpg)  


- **摄像师-侧面**  
FC里该角色的侧面镜头约有28张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 07_105_1,07_126_3__r,07_128_3,07_131_4,07_188_4__r
        - 特点：成组的图数量最多。鹰钩鼻。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_cameraman_basis.png) 
        ![](side_img/face_side_cameraman_basis_mark.jpg)  
        - 雅彦basis(红色) vs 摄像师basis():  
        ![](side_img/face_side_cameraman_basis_vs_masahiko_basis.jpg)  

    - -g_-c_-li_-pg_-dc_-m:  
        - 图片： 12_117_6__r，12_147_0，12_149_2
        - 特点： 鼻尖上翘。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_cameraman_-g_-c_-li_-pg_-dc_-m.png) 
        ![](side_img/face_side_cameraman_basis_vs_-g_-c_-li_-pg_-dc_-m.jpg)  


- **浅葱(Asagi)-侧面**  
FC里该角色的侧面镜头约有42张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 14_125_0, 14_097_0__r, 14_168_5, 14_098_1
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_asagi_basis.png) 
        ![](side_img/face_side_asagi_basis_mark.jpg)  
        - 雅彦basis(红色) vs 浅葱basis():  
        ![](side_img/face_side_asagi_basis_vs_masahiko_basis.jpg)  

    - -prn_+pg_+dc_+m:  
        - 图片： 14_236_4__r, 14_123_3__r,14_079_2__r,14_044_0__r
        - 特点： prn点凹,pg、dc、m点凸。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_asagi_-prn_+pg_+dc_+m.png) 
        ![](side_img/face_side_asagi_basis_vs_-prn_+pg_+dc_+m.jpg)  

    - +se_open:  
        - 图片： 14_131_6，14_234_6
        - 特点： se点凸，张嘴。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_asagi_+se_open.png) 
        ![](side_img/face_side_asagi_basis_vs_+se_open.jpg)  


- **和子(Kazuko)-侧面**  
FC里该角色的侧面镜头约有25张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 08_008_0__r,08_124_1__r,08_091_4__r
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_kazuko_basis.png) 
        ![](side_img/face_side_kazuko_basis_mark.jpg)  
        - 雅彦basis(红色) vs 和子basis(下巴大):  
        ![](side_img/face_side_kazuko_basis_vs_masahiko_basis.jpg)  

    - long-bridge_-sn_-ls_-li_-pg_-dc:  
        - 图片： 08_107_0, 08_104_6
        - 特点： 长鼻梁，sn、ls、li、pg、dc点凹。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kazuko_long-bridge_-sn_-ls_-li_-pg_-dc.png) 
        ![](side_img/face_side_kazuko_basis_vs_long-bridge_-sn_-ls_-li_-pg_-dc.jpg)  


- **爷爷-侧面**  



- **横田进(Susumu)-侧面**  
FC里该角色的侧面镜头约有24张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 05_065_3__r,03_093_4__r
        - 特点：特写。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_susumu_basis.png) 
        ![](side_img/face_side_susumu_basis_mark.jpg)  
        - 雅彦basis(红色) vs 和子basis():  
        ![](side_img/face_side_susumu_basis_vs_masahiko_basis.jpg)  

    - long-bridge_-se:  
        - 图片： 08_107_0, 08_104_6
        - 特点： 长鼻梁，se点凹。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_susumu_long-bridge_-se.png) 
        ![](side_img/face_side_susumu_basis_vs_long-bridge_-se.jpg)  


- **宪司(Kenji)-侧面**  
FC里该角色的侧面镜头约有36张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 04_056_0,04_040_2
        - 特点：这两个镜头几乎完全一致(包括面部的细节)  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_kenji_basis.png) 
        ![](side_img/face_side_kenji_basis_mark.jpg)  
        - 雅彦basis(红色) vs 宪司basis(), 和子basis(红色) vs 宪司basis() :  
        ![](side_img/face_side_kenji_basis_vs_masahiko_basis.jpg) 
        ![](side_img/face_side_kenji_basis_vs_kazuko_basis.jpg)  

    - +g_+dc_+m:  
        - 图片： 04_009_4__r，04_009_5__r
        - 特点： g、dc、m点凸。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kenji_+g_+dc_+m.png) 
        ![](side_img/face_side_kenji_basis_vs_+g_+dc_+m.jpg)  

    - +prn_+c_-m:  
        - 图片： 04_017_7，04_011_5_
        - 特点： g、dc、m点凸。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kenji_+prn_+c_-m.png) 
        ![](side_img/face_side_kenji_basis_vs_+prn_+c_-m.jpg)  

    - +g_+prn_-li_-pg_-dc_-m:  
        - 图片： 07_024_4，07_035_4
        - 特点： g、prn点凸，li、pg、dc、m点凹。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_kenji_+g_+prn_-li_-pg_-dc_-m.png) 
        ![](side_img/face_side_kenji_basis_vs_+g_+prn_-li_-pg_-dc_-m.jpg)  


- **顺子(Yoriko)-侧面**  
FC里该角色的侧面镜头约有36张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  
    - basis:  
        - 图片： 07_058_4__r,04_040_3__r,03_156_3,04_016_5__r,03_177_6__r,03_153_0__r,07_057_3,03_147_5
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_yoriko_basis.png) 
        ![](side_img/face_side_yoriko_basis_mark.jpg)  
        - 雅彦basis(红色) vs 顺子basis():  
        ![](side_img/face_side_yoriko_basis_vs_masahiko_basis.jpg)  

    - +se_+g:  
        - 图片： 07_046_3__r,03_173_2__r
        - 特点： se、g点凸。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_yoriko_+se_+g.png) 
        ![](side_img/face_side_yoriko_basis_vs_+se_+g.jpg)  


- **森(Mori)-侧面**  
FC里该角色的侧面镜头约有15张。可以分为以下几组：  
    - basis:  
        - 图片： 02_072_3__r,10_095_3
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_mori_basis.png) 
        ![](side_img/face_side_mori_basis_mark.jpg)  
        - 雅彦basis(红色) vs 森basis(几乎一致):  
        ![](side_img/face_side_mori_basis_vs_masahiko_basis.jpg)  

    - -prn_open:  
        - 图片： 01_181_3__r,10_113_4
        - 特点： prn点稍微凹,张嘴。  
        - 这些图片叠加后的效果、和basis组(红色)比较：  
        ![](side_img/face_side_mori_-prn_open.png) 
        ![](side_img/face_side_mori_basis_vs_-prn_open.jpg)  


- **叶子母-侧面**  
FC里该角色的侧面镜头约有18张。可以分为以下几组：  
    - basis:  
        - 图片： 13_175_1,13_170_0__r,13_108_3
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_ykma_basis.png) 
        ![](side_img/face_side_ykma_basis_mark.jpg)  
        - 雅彦basis(红色) vs 森basis(几乎一致):  
        ![](side_img/face_side_ykma_basis_vs_masahiko_basis.jpg)  


- **理沙(Risa)-侧面**  
FC里该角色的侧面镜头约有25张。可以分为以下几组：  
    - basis:  
        - 图片： 12_053_4__r,11_179_0,11_168_4__r
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_risa_basis.png) 
        ![](side_img/face_side_risa_basis_mark.jpg)  
        - 雅彦basis(红色) vs 理沙basis():  
        ![](side_img/face_side_risa_basis_vs_masahiko_basis.jpg)  


- **美菜(Mika)-侧面**  
没有合适的图(侧面图太小,或张嘴).  


- **齐藤茜(Akane)-侧面**  
FC里该角色的侧面镜头约有13张。可以分为以下几组：  
    - basis:  
        - 图片： 06_157_0__r, 06_156_3__r, 06_145_6__r
        - 特点：成组的图数量最多. se点凸。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_akane_basis.png) 
        ![](side_img/face_side_akane_basis_mark.jpg)  
        - 雅彦basis(红色) vs 齐藤茜basis():  
        ![](side_img/face_side_akane_basis_vs_masahiko_basis.jpg)  



- **仁科(Nishina)-侧面**  
FC里该角色的侧面镜头约有15张。可以分为以下几组：  
    - basis:  
        - 图片： 10_169_3,14_006_0,10_169_2__r
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_nishina_basis.png) 
        ![](side_img/face_side_nishina_basis_mark.jpg)  
        - 雅彦basis(红色) vs 仁科basis():  
        ![](side_img/face_side_nishina_basis_vs_masahiko_basis.jpg)  




- **葵(Aoi)-侧面**  
FC里该角色的侧面镜头约有12张。两张特写(06_083_0__r,06_039_2)差异大,无法成组. 可以分为以下几组:    
    - basis:  
        - 图片： 06_047_2,06_037_2,
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_aoi_basis.png) 
        ![](side_img/face_side_aoi_basis_mark.jpg)  
        - 雅彦basis(红色) vs 葵basis():  
        ![](side_img/face_side_aoi_basis_vs_masahiko_basis.jpg)  


 
- **奶奶-侧面**  
 


- **文哉(Fumiya)-侧面**  
FC里该角色的侧面镜头约有10张。可以分为以下几组:    
    - basis:  
        - 图片： 12_040_0,12_034_3__r
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_fumiya_basis.png) 
        ![](side_img/face_side_fumiya_basis_mark.jpg)  
        - 雅彦basis(红色) vs 葵basis():  
        ![](side_img/face_side_fumiya_basis_vs_masahiko_basis.jpg)  



- **齐藤玲子(Reiko)-侧面**  
FC里该角色的侧面镜头约有8张。3张特写(01_177_0__r,01_171_2,01_169_2)差异大,无法成组.  


- **松下敏史(Toshifumi)-侧面**  
FC里该角色的侧面镜头约有22张。可以分为以下组:  
    - basis:  
        - 图片： 09_069_7,09_069_6,09_069_3,09_068_5__r
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_matsu_basis.png) 
        ![](side_img/face_side_matsu_basis_mark.jpg)  
        - 雅彦basis(红色) vs 松下敏史basis():  
        ![](side_img/face_side_matsu_basis_vs_masahiko_basis.jpg)  



- **章子(Shoko)-侧面**  



- **阿透(Tooru)-侧面**  
FC里该角色的侧面镜头约有3张：12_014_2，12_015_2，12_042_0。差异大,无法成组.  



- **京子(Kyoko)-侧面**  
        

- **一马(Kazuma)-侧面**  



- **一树(Kazuki)-侧面**  


- **千夏(Chinatsu)-侧面**  


- **绫子(Aya)-侧面**  


- **典子(Tenko)-侧面**  


- **麻衣(Mai)-侧面**  


- **古屋公弘(Kimihiro)-侧面**  


- **古屋朋美(Tomomi)-侧面**  
FC里该角色的侧面镜头约有5张。可以分为以下组:  
    - basis:  
        - 图片： 09_048_1,09_043_3
        - 特点：成组的图数量最多。  
        - 这些图片叠加后的效果、参考比例、亚洲人种侧面mark点：  
        ![](side_img/face_side_tomomi_basis.png) 
        ![](side_img/face_side_tomomi_basis_mark.jpg)  
        - 雅彦basis(红色) vs 古屋朋美basis():  
        ![](side_img/face_side_tomomi_basis_vs_masahiko_basis.jpg)  



- **八不(Hanzu)-侧面**  


- **哲(Te)-侧面**  
 


- **总结**：  


- **其他**：  
    - 07_014_1, 角色这个侧面的角度，鼻翼处画出了对侧的脸颊：  
      ![](side_img/07_014_1__mod.jpg)  
      类似的还有12_115_0：  
      ![](side_img/12_115_0__.jpg)  

    - 11_015_7，下巴和脖子连接处成锐角(可能有助于表现角色的紧张)：   
      ![](side_img/11_015_7__.jpg)  



- **参考资料**：  
    1. Drawing The Head & Hands, Andrew Loomis.  
    2. [Differences between Caucasian and Asian attractive faces](https://www.researchgate.net/publication/318548459), Seung Chul Rhee, 2017.  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处




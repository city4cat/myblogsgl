
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
    - 如有错误，请指出，非常感谢！  


# FC的画风-面部-五官组合(正面)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96889))  

### **0. 一个问题**  
五官组合后的结果似乎没有原图漂亮。以紫苑为例，五官组合后的结果：  
![](img/face_compose_char_shion.jpg) ![](img/face_compose_char_shion_mark.jpg)  
FC里原图叠加后([5]里的紫苑group3)：  
![](img/face_front_shion_psduo-front.jpg) ![](img/face_front_shion_psduo-front_mark.jpg)  

两者各自五官的面部比例是一致的。但为什么组合的结果没有原图漂亮呢？究竟是哪些因素导致的？  


### **1. FC里"某些次要角色的五官"可由"主要角色的五官"组合而成  **  

之前写了关于FC角色五官的文章：  

- [眉毛](../fc_drawing_style__face_brow/readme.md)  
- [眼睛](../fc_drawing_style__face_eye2/readme.md)  
- [鼻子](../fc_drawing_style__face_nose2/readme.md)  
- [嘴](../fc_drawing_style__face_mouth2/readme.md)  
- [脸形](../fc_drawing_style__face/readme.md)  

相比上述每篇文章里单独地看五官，(通过本文可以发现)五官组合在一起后，差异更明显。  

在上述每篇文章最后，将某一器官(例如眉、眼、鼻、嘴、脸形)相近的角色划分为同一类。这里按角色总结如下。可以发现，某些次要角色(后出场的角色)是由主要角色(先出场的角色)的某些五官组合而成的。例如：  

- 熏(Kaoru)的相貌 = 江岛(Ejima)的眼睛 + 若苗紫(Yukari)的鼻子 + 雅彦(Masahiko)的嘴 + 浩美(Hiromi)的脸形  
- 浅葱(Asagi)的相貌 = 早纪(Saki)的眉毛 + 叶子(Yoko)的眼睛 + 雅彦(Masahiko)的鼻子 + 叶子(Yoko)的嘴 + 若苗紫(Yukari)的脸形  
- 古屋朋美(Tomomi)之所以很像真琴，是因为：  
  古屋朋美(Tomomi)的相貌 = 真琴(Makoto)的眼睛 + 真琴(Makoto)的鼻子 + 真琴(Makoto)的脸形。        


|                     |                  |                 |               |                 |                 |  
|             ------: | :----------------|   :-------------| :-------------|  :--------------| :---------------|  
|           **角色 =** |           **眉** |         **+眼** |      **+鼻子** |         **+嘴** |        **+脸形** |  
|     雅彦(Masahiko) = |                  |                |                |                 |                 |  
|        紫苑(Shion) = |   雅彦(Masahiko) |  雅彦(Masahiko) |                |                 |                 |  
|       若苗空(Sora) = |                  |                |                |   雅彦(Masahiko) |                 |  
|     若苗紫(Yukari) = |    雅彦(Masahiko) |                |                |      紫苑(Shion) |                 |  
|                     |                  |                |                |                 |                 |  
|      浩美(Hiromi) = |                  |                |  若苗紫(Yukari) |                 |                 |  
|      真琴(Makoto) = |                  |                |    真琴(Makoto) |                 |                 |  
|      和子(Kazuko) = |                  |                |                |                 |                 |  
|                    |                  |                |                |                 |                 |  
|   齐藤玲子(Reiko) = |                  |    真琴(Makoto) |                |                 |                 |  
|                    |                  |                |                |                 |                 |  
|         森(Mori) = |      浩美(Hiromi) |                |    真琴(Makoto) |                 |                 |  
|                    |                  |                |                |                  |                 |  
|      雅美(Masami) = |                  |                |  雅彦(Masahiko) |   雅彦(Masahiko) |  真琴(Makoto)[5] |  
|      盐谷(Shya)   = |    雅彦(Masahiko) |   顺子(Yoriko) |  雅彦(Masahiko) |   雅彦(Masahiko) |      紫苑(Shion) |  
|                    |                  |                |                 |                 |                 |  
|       江岛(Ejima) = |                  |                |  雅彦(Masahiko) |   雅彦(Masahiko) |                 |  
|    导演(Director) = |                  |                |                |                 |                 |  
| 摄像师(Cameraman) = |                  |                 | 导演(Director) |                 |                 |  
|                    |                  |                 |               |                 |                 |  
|        叶子(Yoko) = |                  |                 |   真琴(Makoto) |                |     浩美(Hiromi) |  
|      叶子母(ykma) = |        叶子(Yoko) |                 |               |      叶子(Yoko) |     浩美(Hiromi) |  
|                    |                  |                 |               |                 |                 |  
|      京子(Kyoko) = |                   |                 |               |      叶子(Yoko) |                 |  
|      八不(Hanzu) = |                   |                 |               |                |                 |  
|        绫子(Aya) = |                   |                 |               |                |                 |  
|      典子(Tenko) = |                   |                 |      绫子(Aya) |                |                 |  
|                    |                  |                  |              |                 |                 |  
|          **角色 =** |           **眉** |          **+眼** |     **+鼻子** |         **+嘴** |        **+脸形** |  
|      顺子(Yoriko) = |                  |                 |               |      叶子(Yoko) |     真琴(Makoto) |  
|      宪司(Kenji) =  |                  |                 |  和子(Kazuko) |     和子(Kazuko) |                 |  
|                    |                  |                 |               |                 |                 |  
|    横田进(Susumu) = |     顺子(Yoriko)  |                |  雅彦(Masahiko) | 江岛(Ejima)+口红 |                 |  
|                    |                  |                 |                |                |                 |  
|     齐藤茜(Akane) = |                   |      叶子(Yoko) | 齐藤玲子(Reiko) |      叶子(Yoko) |     京子(Kyoko) |  
|                    |                   |                |               |                 |                 |  
|         熏(Kaoru) = |                  |     江岛(Ejima) | 若苗紫(Yukari) |   雅彦(Masahiko) |     浩美(Hiromi) |  
|     辰巳(Tatsumi) = |                  |     江岛(Ejima) |               |         (+胡子茬) |     若苗空(Sora) |  
|        早纪(Saki) = |                  |                 |  真琴(Makoto) |        叶子(Yoko) |     紫苑(Shion) |  
|                    |                  |                 |               |                  |                 |  
|       理沙(Risa) =  |                  |      叶子(Yoko) |    紫苑(Shion) |        叶子(Yoko) |                 |  
|       美菜(Mika) =  |                  |    真琴(Makoto) |   顺子(Yoriko) |        叶子(Yoko) |                 |  
|                    |                  |                 |               |                  |                 |  
|     仁科(Nishina) = |     和子(Kazuko) |   雅彦(Masahiko) | 雅彦(Masahiko) |                  |                 |  
|                    |                  |                 |               |                  |                 |  
|           葵(Aoi) = |    若苗空(Sora)  |   横田进(Susumu) | 雅彦(Masahiko) |  江岛(Ejima)+口红 |                 |  
|                    |                  |                 |               |                  |                 |  
|      文哉(Fumiya) = | 摄像师(Cameraman) |                | 雅彦(Masahiko) |       江岛(Ejima) |   横田进(Susumu) |  
|       阿透(Tooru) = |                  |     江岛(Ejima) | 雅彦(Masahiko) |                  |                 |  
|      一树(Kazuki) = |                  |                | 雅彦(Masahiko) |       江岛(Ejima) |   雅彦(Masahiko) |  
|                    |                  |                |                |                  |                 |  
|    松下敏史(matsu) = |                 |                 | 雅彦(Masahiko) |     辰巳(Tatsumi) |   横田进(Susumu) |  
|                    |                  |                |                |                  |                 |  
|    千夏(Chinatsu) = |     浩美(Hiromi) |      叶子(Yoko) |    顺子(Yoriko) |                  |     紫苑(Shion) |  
|         麻衣(Mai) = |                  |   真琴(Makoto) |    顺子(Yoriko) |                  |      紫苑(Shion) |  
|                    |                  |                |                |       紫苑(Shion) |                 |  
| 古屋公弘(Kimihiro) = |                  |               |  雅彦(Masahiko) |                  |    浩美(Hiromi)[5] |  
|   古屋朋美(Tomomi) = |                  |   真琴(Makoto) |    真琴(Makoto) |                  |    真琴(Makoto) |  
|            哲(Te) = |                  |               |                 |                 |                 |  
|                    |                  |                |                 |                 |                 |  
|       浅葱(Asagi) = |       早纪(Saki) |      叶子(Yoko) |  雅彦(Masahiko) |       叶子(Yoko) |    若苗紫(Yukari) |  
|                    |                  |                |                |                 |                 |  
|    爷爷(Grandpa) =  |                  |                |                |                 |                 |  
|     奶奶(Grandma) = |                  |                |                |                 |                 |  
|     章子(Shoko) =   |                  |                |                |                 |                 |  
|     一马(Kazuma) =  |                  |                |                |                 |                 |  
|                    |                  |                |                |                 |                 |  
|          **角色 =** |           **眉** |          **+眼** |     **+鼻子** |         **+嘴** |        **+脸形** |  



### **2. 组合更多角色的五官和发形**  

按照以上的思路，可以组合更多FC角色的五官。组合的结果里有很多是FC里没有出现过的相貌，其中有五官搭配合适的、也有五官搭配不合适的。对于五官搭配合适的那些组合结果，我猜，如果FC继续连载的话，新出场的角色的相貌可能会类似这些组合结果（头发例外，因为几乎每个角色的头发都不相同，所以新出场的角色应该有自己独特的头发）。  

为了让组合的五官结果看起来舒服，我决定为五官组合结果加上头发。  

为了能检索每一个组合的结果，给每一个组合结果指定一个"组合码"。方法如下：  

- 为每一个角色赋值一个编号，如下表格所示。  
- 为每一个组合结果关联一个6位"组合码"（例如：012345）。组合码从左至右的每一位分别表示：某角色的头发、某角色的眉、某角色的眼、某角色的鼻、某角色的嘴、某角色的脸庞。例如：  
    - 组合码"2025EE"表示组合结果：紫苑的头发 + 雅彦的眉 + 紫苑的眼 + 紫的鼻 + 浅葱的嘴 + 浅葱的脸庞；  
    - 组合码"7D6E52"表示组合结果：浩美的头发 + 早纪的眉 + 叶子的眼 + 浅葱的鼻 + 紫的嘴 + 紫苑的脸庞；  

|             |                     |                         |  
| ----------: | :-------------------|   :---------------------|  
| **角色编号** |             **名字** |                 **说明** |  
|           - | 空白                |  该五官处为空白            |  
|           0 | Masahiko/雅彦       |                          |  
|           1 | Masami/雅美         |                          |  
|           2 | Shion/紫苑          |                          |  
|           3 | Shya/盐谷           | "Shya"是"Shionoya"的缩写  |  
|           4 | Sora/空             |                          |  
|           5 | Yukari/紫           |                          |  
|           6 | Yoko/叶子           |                          |  
|           7 | Hiromi/浩美         |                          |  
|           8 | Kaoru/熏            |                         |  
|           9 | Ejima/江岛          |                          |  
|           A | Tatsumi/辰巳        |                          |  
|           B | Makoto/真琴         |                          |  
|           C | Director/导演       |                          |  
|           D | Saki/早纪           |                          |  
|           E | Asagi/浅葱          |                          |  
|           F | Cameraman/摄像师     |                         |  
|           G | Yoriko/顺子         |                          |  
|           H | Mori/森             |                         |  
|           I | Kazuko/和子         |                          |  
|           J | Grandpa/爷爷        | 该角色的五官尚未制作        |  
|           K | Susumu/横田进       |                         |  
|           L | Kenji/宪司          |                         |  
|           M | Yoko's Mother/叶子母 | 缩写为ykma              |  
|           N | Risa/理沙            |                        |  
|           O | Mika/美菜            |                        |  
|           P | Akane/齐藤茜         |                        |  
|           Q | Nishina/仁科         |                        |  
|           R | Aoi/葵              |                         |  
|           S | Grandma/奶奶        | 该角色的五官尚未制作        |  
|           T | Fumiya/文哉         |                         |  
|           U | Reiko/齐藤玲子       |                          |  
|           V | Matsu/松下敏史       | "Matsu"是"Matsushita"的缩写 |  
|           W | Shoko/章子          | 该角色的五官尚未制作        |  
|           X | Tooru/阿透          |                         |  
|           Y | Kyoko/京子          |                         |  
|           Z | Kazuma/一马         | 该角色的五官尚未制作       |  
|           a | Kazuki/一树         |                         |  
|           b | Chinatsu/千夏       |                         |  
|           c | Mai/麻衣            |                         |  
|           d | Aya/绫子            |                         |  
|           e | Tenko/典子          |                         |  
|           f | Kimihiro/古屋公弘    |                         |  
|           g | Tomomi/古屋朋美      |                         |  
|           h | Hanzu/八不          |                         |  
|           i | Te/哲               | 该角色的五官没有合适的图   |  
|           j | Kazuto/和人         | Kazuko/和子的本名(男装)   |  


这将会有47^6个组合结果。  

如果再加上化妆元素：眼镜、胡子、疤痕(比如辰巳)、皱纹等，组合结果会更多。    


### **3. 手动组合角色的五官和发形**  
因为以上的组合结果数量巨大，不可能把所有组合结果罗列在此。尽管如此，这里提供两种方法，可以查看所有组合结果。  

##### **3.1  使用网页**  
- 访问[https://city4cat.gitlab.io/myblogs/](https://city4cat.gitlab.io/myblogs/)。如图：  
![](img/face_compo_webpage.jpg)  
6个下拉菜单对应"头发"和五官，每个下拉菜单里可选择不同的角色。  
图中所示的五官组合结果为： 紫苑(Shion)的头发 + 雅彦(Masahiko)的眉 + 紫苑(Shion)的眼睛 + 紫(Yukari)的鼻子 + 浅葱(Asagi)的嘴 + 浅葱(Asagi)的脸庞。  
- 优点：无需安装软件。  
- 缺点：无法修改图片。  

##### **3.2  使用Gimp**  
- 安装软件Gimp。它类似于Photoshop。    
- 下载[project/face_compose.xcf.7z](project/face_compose.xcf.7z)至本地电脑，并解压缩出文件face_compose.xcf。  
- 使用软件Gimp打开本地文件face_compose.xcf。如图：   
![](img/face_compo_gimp.jpg)  
6个layer group对应"头发"和五官。每个layer group下有每个角色的图层。  
图中所示的五官组合结果为： 浩美(Hiromi)的头发 + 早纪(Saki)的眉 + 叶子(Yoko)的眼睛 + 浅葱(Asagi)的鼻子 + 紫(Yukari)的嘴 + 紫苑(Shion)的脸庞。  
- 优点：可修改所有图片，包括图层的透明度。  
- 缺点：需安装软件Gimp。  


### **4. 如何减少五官组合结果的数量**  
以上的组合结果数量巨大，导致无法系统地分析组合结果(之所以想系统地分析这些组合结果，是想从中发现一些有趣的东西)。所以要尝试减少组合结果。考虑如下几个策略：  
    - 策略1：考虑所有角色，合并类似的五官。  
    - 策略2：依某些特征将角色分类(男1组、女1组、男2组、女2组、...)，只组合同一组里的角色的五官。  
        - 策略2 + 每组只用1个发形  
    - 策略3：只考虑少数主要角色，组合他们的五官。  
        - 策略2 + 策略3  
        - 策略2 + 策略3 + 每组只用1个发形
    - 策略4：策略2 + 一个组合结果里只包含2个角色的五官。   
  

##### **4.1 策略1：考虑所有角色，合并类似的五官**  
- 头发。因为角色的头发几乎都不一样，所以无法减少分类。所以有46(不包含角色编号‘-’)个分类。  

- 在文章[眉毛](../fc_drawing_style__face_brow/readme.md)里：  
    - 把类似的眉毛分组后，有19个分类。  
    - 合并眉毛粗细度(合并2,3,4)，从而减少分组后，有15个分类。  
    - 进一步合并较为类似的眉毛，从而减少分组后，有9个分类。  

- 在文章[眼睛](../fc_drawing_style__face_eye2/readme.md)里：  
    - 合并分类(合并非常类似的眼睛)后，有10+13(未能合并)=23个分类。  
    - 进一步合并分类2(合并类似的眼睛)后，有6+9(未能合并)=15个分类。  

- 在文章[鼻子](../fc_drawing_style__face_nose2/readme.md)里：  
    - 把类似的鼻子分组后，有14个分类。  
    - 进一步减少分组后，有4个分类。  

- 在文章[嘴](../fc_drawing_style__face_mouth2/readme.md)里：  
    - 把类似的嘴分组后，有8(按大小分类)+2(按口红分类)+4(未能合并)=14个分类。  
    - 进一步(粗粒化)分组后，有4+1=5个分类。  
    
- 在文章[脸形](../fc_drawing_style__face/readme.md)里：  
    - 把很类似的脸形分类后，有9个分类。  

分类后最多有46x19x23x14x14x9(=约3545万)个五官组合结果，最少有46x9x15x4x5x9(=约111万)个五官组合结果。如果排除未能合并的分类，则有46x9x6x4x4x9(=约35万)个五官组合结果。这些组合数目太多，无法系统地分析。  

（注：眼睛最终能合并为15个分类，鼻子最终合并为4个分类。这说明"不同角色的眼睛差异明显、鼻子差异不明显"。我猜，作者更注重设计角色的眼睛。应该也能间接说明：(漫画里)眼睛在面部五官中更具表现力。）  


##### **4.2 策略2：依某些特征将角色分类，只组合同一组里的角色的五官**  
假设：
- 排除"男性五官"搭配"女性五官"；  
- 排除"粗犷的五官"搭配"颜值高的五官"；例如，排除"宪司的五官"搭配"雅彦的五官";  
- 排除"胖子的五官"搭配"颜值高的五官"；例如，排除"导演的五官"搭配"雅彦的五官";  
- 排除"瘦子的五官"搭配"颜值高的五官"；例如，排除"摄像师的五官"搭配"雅彦的五官";  

依相貌特征将角色分类：  
- 男1组：  
    - 特征：颜值高或普通  
    - 角色：雅彦，盐谷，空，熏，江岛，辰巳；仁科，文哉，松下敏史，阿透，一树，古屋公弘，  
    
- 女1组：  
    - 特征：颜值高或普通  
    - 角色：雅美，紫苑，紫，叶子，浩美，真琴，早纪，浅葱，顺子；森，叶子母，理沙，美菜，齐藤茜，齐藤玲子，京子，千夏，麻衣，古屋朋美，  
    
- 男2组：  
    - 特征：粗犷  
    - 角色：和子，宪司，和人，   
    
- 女2组：  
    - 特征：粗犷  
    - 角色：横田进，葵,  
    
- 男3组：  
    - 特征：胖、瘦。  
    - 角色：导演，摄像师,；  
    
- 女3组：  
    - 特征：胖、瘦。  
    - 角色：绫子，典子，  

- 其他组(未处理)：
    - 角色及特征：爷爷(老年男性)，奶奶(老年女性)，一马(男童)，章子(女童)，  

按最粗粒度分组：  

|         |                  |                        |                      |               |                     |                     |  
| ------: | :----------------|       :----------------|        :-------------| :-------------|      :--------------|     :---------------|  
| **分组** |         **头发** |                  **眉** |               **眼** |       **鼻子** |              **嘴** |             **脸形** |  
|    男1组 |       12类(12人) | \{Masahiko\} \{Sora\}  | \{Masahiko\} \{Shya\} \{Sora\} \{Ejima\} | \{Masahiko\} \{Tatsumi\} |                \{Masahiko\} | \{Masahiko\} \{Sora\} \{Ejima\} |  
|    女1组 |       19类(19人) | \{Shion\} \{Reiko\} \{Risa\} \{Asagi\} | \{Shion\} \{Yukari\} \{Yoko\} \{Yoriko\} \{Saki\} \{ykma\} | \{Masami\} | \{Reiko\} \{Shion\} \{Saki\} | \{Shion\} \{Masami\} \{Yukari\} \{Yoko\} \{Akane\} |  
|    男2组  |        3类(3人) | \{Kazuko\}  |  \{Kazuko\} | \{Kazuko\} | \{Kazuko\} | \{Kazuko\} \{Kenji\} |  
|    女2组  |        2类(2人) |  \{Susumu\} \{Aoi\} | \{Susumu\} |  \{Susumu\} |  \{Susumu\} |  \{Susumu\} \{Aoi\} |  
|    男3组  |        2类(2人) | \{Director\} \{Cameraman\} | \{Director\} \{Cameraman\} \{Hanzu\} | \{Director\} \{Hanzu\} | \{Director\} \{Cameraman\} |                \{Director\} \{Cameraman\} |  
|    女3组  |        2类(2人) | \{Aya\}                | \{Aya\}               | \{Aya\}      | \{Aya\}             | \{Aya\}              |  

这样的组合结果数目：  
= 男1组组合结果数 + 女1组组合结果数 + 男2组组合结果数 + 女2组组合结果数 + 男3组组合结果数 + 女3组组合结果数  
= 12x2x4x2x1x3 +19x4x6x1x3x5 +3x1x1x1x1x2 + 2x2x1x1x1x2 +2x2x3x2x2x2 +2x1x1x1x1x1  
= 576 +6840 +6 +8 +160 +2  
= 7592  

##### **4.2.1 策略2 + 每组只用1个发形**
因为男女组分开，所以不会出现"男生搭配女性发形"或"女生搭配男性发形"的情况，所以可以考虑每组只用1个发形:  

|         |                  |                        |                      |               |                     |                     |  
| ------: | :----------------|       :----------------|        :-------------| :-------------|      :--------------|     :---------------|  
| **分组** |         **头发** |                  **眉** |               **眼** |       **鼻子** |              **嘴** |             **脸形** |  
|    男1组 |       1类 | \{Masahiko\} \{Sora\}  | \{Masahiko\} \{Shya\} \{Sora\} \{Ejima\} | \{Masahiko\} \{Tatsumi\} |                \{Masahiko\} | \{Masahiko\} \{Sora\} \{Ejima\} |  
|    女1组 |       1类 | \{Shion\} \{Reiko\} \{Risa\} \{Asagi\} | \{Shion\} \{Yukari\} \{Yoko\} \{Yoriko\} \{Saki\} \{ykma\} | \{Masami\} | \{Reiko\} \{Shion\} \{Saki\} | \{Shion\} \{Masami\} \{Yukari\} \{Yoko\} \{Akane\} |  
|    男2组  |        1类 | \{Kazuko\}  |  \{Kazuko\} | \{Kazuko\} | \{Kazuko\} | \{Kazuko\} \{Kenji\} |  
|    女2组  |        1类 |  \{Susumu\} \{Aoi\} | \{Susumu\} |  \{Susumu\} |  \{Susumu\} |  \{Susumu\} \{Aoi\} |  
|    男3组  |        1类 | \{Director\} \{Cameraman\} | \{Director\} \{Cameraman\} \{Hanzu\} | \{Director\} \{Hanzu\} | \{Director\} \{Cameraman\} |                \{Director\} \{Cameraman\} |  
|    女3组  |        1类 | \{Aya\}                | \{Aya\}               | \{Aya\}      | \{Aya\}             | \{Aya\}              |  

这样的组合结果数目：  
= 男1组组合结果数 + 女1组组合结果数 + 男2组组合结果数 + 女2组组合结果数 + 男3组组合结果数 + 女3组组合结果数  
= 1x2x4x2x1x3 +1x4x6x1x3x5 +1x1x1x1x1x2 +1x2x1x1x1x2 +1x2x3x2x2x2 +1x1x1x1x1x1  
= 48 +360 +2 +4 +48 +1  
= 463  


##### **4.3 策略3：只考虑少数主要角色，组合他们的五官**  
若选n个主要角色，则他们的五官组合结果为n^5个；若搭配每个角色的发形，则搭配结果为n^6个。  

|        |                   |                       |  
| ------:| :-----------------| :---------------------|  
| 角色数n | 五官组合结果数(n^5) | 五官+发形组合结果数(n^6) |  
|      2 |                32 |                    64 |  
|      3 |               243 |                   729 |  
|      4 |              1024 |                  4096 |  

可以看出，选4个角色的搭配结果数目就已经很大了。需要想办法减少组合数目。    

##### **4.3.1 策略2 + 策略3**  
如果考虑策略2里的4个假设，并只考虑如下主要角色：  

- 男1组：  
    - 特征：颜值高或普通  
    - 角色：雅彦，空，熏，江岛，辰巳；  
    
- 女1组：  
    - 特征：颜值高或普通  
    - 角色：紫苑，紫，叶子，浩美，真琴，早纪，浅葱，顺子；  
    
- 男2组：  
    - 特征：粗犷  
    - 角色：和子，宪司，   
    
- 女2组：  
    - 特征：粗犷  
    - 角色：横田进  
    
- 男3组：  
    - 特征：胖、瘦。  
    - 角色：导演，摄像师；  
    
- 女3组：  
    - 特征：胖、瘦。  
    - 角色：(无)  

- 其他组(未处理)：
    - 角色及特征：(无)，  

按最粗粒度分组：  

|         |                  |                        |                      |               |                     |                     |  
| ------: | :----------------|       :----------------|        :-------------| :-------------|      :--------------|     :---------------|  
| **分组** |         **头发** |                  **眉** |               **眼** |       **鼻子** |              **嘴** |             **脸形** |  
|    男1组 |       5类(5人) | \{Masahiko\} \{Sora\}  | \{Masahiko\} \{Sora\} \{Ejima\} | \{Masahiko\} \{Tatsumi\} |                \{Masahiko\} | \{Masahiko\} \{Sora\} \{Ejima\} |  
|    女1组 |       8类(8人) | \{Shion\} \{Asagi\} | \{Shion\} \{Yukari\} \{Yoko\} \{Yoriko\} \{Saki\} | \{Shion\} | \{Shion\} \{Saki\} | \{Shion\} \{Yukari\} \{Yoko\} |  
|    男2组  |      3类(3人) |  \{Kazuko\}  |  \{Kazuko\} | \{Kazuko\} |  \{Kazuko\} | \{Kazuko\} \{Kenji\} |  
|    女2组  |      1类(1人) | \{Susumu\} | \{Susumu\}  |  \{Susumu\} |                    \{Susumu\} |     \{Susumu\} |  
|    男3组  |      2类(2人) | \{Director\} \{Cameraman\} | \{Director\} \{Cameraman\} | \{Director\} | \{Director\} \{Cameraman\} | \{Director\} \{Cameraman\} |  
|    女3组  |      0类(0人) |                        |                     |                |                     |                      |  

组合结果数目：  
= 男1组组合结果数 + 女1组组合结果数 + 男2组组合结果数 + 女2组组合结果数 + 男3组组合结果数 + 女3组组合结果数  
= 5x2x3x2x1x3 +8x2x5x1x2x3 +3x1x1x1x1x2 +1 +2x2x2x1x2x2 +0  
= 180 +480 +6 +1 +32 +0  
= 699

##### **4.3.2 策略2 + 策略3 + 每组只用1个发形**  
因为男女组分开，所以不会出现"男生搭配女性发形"或"女生搭配男性发形"的情况，所以可以考虑每组只用1个发形:  

|         |                  |                        |                      |               |                     |                     |  
| ------: | :----------------|       :----------------|        :-------------| :-------------|      :--------------|     :---------------|  
| **分组** |         **头发** |                  **眉** |               **眼** |       **鼻子** |              **嘴** |             **脸形** |  
|    男1组 |       1类 | \{Masahiko\} \{Sora\}  | \{Masahiko\} \{Sora\} \{Ejima\} | \{Masahiko\} \{Tatsumi\} |                \{Masahiko\} | \{Masahiko\} \{Sora\} \{Ejima\} |  
|    女1组 |       1类 | \{Shion\} \{Asagi\} | \{Shion\} \{Yukari\} \{Yoko\} \{Saki\} \{Yoriko\} | \{Shion\} | \{Shion\} \{Saki\} | \{Shion\} \{Yukari\} \{Yoko\} |  
|    男2组  |      1类 |  \{Kazuko\}  |  \{Kazuko\} | \{Kazuko\} |  \{Kazuko\} | \{Kazuko\} \{Kenji\} |  
|    女2组  |      1类 | \{Susumu\} | \{Susumu\}  |  \{Susumu\} |                    \{Susumu\} |     \{Susumu\} |  
|    男3组  |      1类 | \{Director\} \{Cameraman\} | \{Director\} \{Cameraman\} | \{Director\} | \{Director\} \{Cameraman\} | \{Director\} \{Cameraman\} |  
|    女3组  |      1类 |                        |                     |                |                     |                      |  

这样的组合结果数目：  
= 男1组组合结果数 + 女1组组合结果数 + 男2组组合结果数 + 女2组组合结果数 + 男3组组合结果数 + 女3组组合结果数  
= 1x2x3x2x1x3 +1x2x5x1x2x3 +1x1x1x1x1x2 +1 +1x2x2x1x2x2 +0  
= 36 +60 +2 +1 +16 +0  
= 115  
这是一个我可以接受的数目。组合结果如下, 点击图片看大图。  

- 男1组：  
![](project/s2+s3+1hair/male1group/000000.jpg){.css} ![](project/s2+s3+1hair/male1group/000004.jpg){.css} ![](project/s2+s3+1hair/male1group/000009.jpg){.css} ![](project/s2+s3+1hair/male1group/000A00.jpg){.css} ![](project/s2+s3+1hair/male1group/000A04.jpg){.css} ![](project/s2+s3+1hair/male1group/000A09.jpg){.css}  
![](project/s2+s3+1hair/male1group/004000.jpg){.css} ![](project/s2+s3+1hair/male1group/004004.jpg){.css} ![](project/s2+s3+1hair/male1group/004009.jpg){.css} ![](project/s2+s3+1hair/male1group/004A00.jpg){.css} ![](project/s2+s3+1hair/male1group/004A04.jpg){.css} ![](project/s2+s3+1hair/male1group/004A09.jpg){.css}  
![](project/s2+s3+1hair/male1group/009000.jpg){.css} ![](project/s2+s3+1hair/male1group/009004.jpg){.css} ![](project/s2+s3+1hair/male1group/009009.jpg){.css} ![](project/s2+s3+1hair/male1group/009A00.jpg){.css} ![](project/s2+s3+1hair/male1group/009A04.jpg){.css} ![](project/s2+s3+1hair/male1group/009A09.jpg){.css}  
![](project/s2+s3+1hair/male1group/040000.jpg){.css} ![](project/s2+s3+1hair/male1group/040004.jpg){.css} ![](project/s2+s3+1hair/male1group/040009.jpg){.css} ![](project/s2+s3+1hair/male1group/040A00.jpg){.css} ![](project/s2+s3+1hair/male1group/040A04.jpg){.css} ![](project/s2+s3+1hair/male1group/040A09.jpg){.css}  
![](project/s2+s3+1hair/male1group/044000.jpg){.css} ![](project/s2+s3+1hair/male1group/044004.jpg){.css} ![](project/s2+s3+1hair/male1group/044009.jpg){.css} ![](project/s2+s3+1hair/male1group/044A00.jpg){.css} ![](project/s2+s3+1hair/male1group/044A04.jpg){.css} ![](project/s2+s3+1hair/male1group/044A09.jpg){.css}  
![](project/s2+s3+1hair/male1group/049000.jpg){.css} ![](project/s2+s3+1hair/male1group/049004.jpg){.css} ![](project/s2+s3+1hair/male1group/049009.jpg){.css} ![](project/s2+s3+1hair/male1group/049A00.jpg){.css} ![](project/s2+s3+1hair/male1group/049A04.jpg){.css} ![](project/s2+s3+1hair/male1group/049A09.jpg){.css}  

- 女1组：  
![](project/s2+s3+1hair/female1group/222222.jpg){.css} ![](project/s2+s3+1hair/female1group/222225.jpg){.css} ![](project/s2+s3+1hair/female1group/222226.jpg){.css} ![](project/s2+s3+1hair/female1group/2222D2.jpg){.css} ![](project/s2+s3+1hair/female1group/2222D5.jpg){.css} ![](project/s2+s3+1hair/female1group/2222D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/225222.jpg){.css} ![](project/s2+s3+1hair/female1group/225225.jpg){.css} ![](project/s2+s3+1hair/female1group/225226.jpg){.css} ![](project/s2+s3+1hair/female1group/2252D2.jpg){.css} ![](project/s2+s3+1hair/female1group/2252D5.jpg){.css} ![](project/s2+s3+1hair/female1group/2252D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/226222.jpg){.css} ![](project/s2+s3+1hair/female1group/226225.jpg){.css} ![](project/s2+s3+1hair/female1group/226226.jpg){.css} ![](project/s2+s3+1hair/female1group/2262D2.jpg){.css} ![](project/s2+s3+1hair/female1group/2262D5.jpg){.css} ![](project/s2+s3+1hair/female1group/2262D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/22D222.jpg){.css} ![](project/s2+s3+1hair/female1group/22D225.jpg){.css} ![](project/s2+s3+1hair/female1group/22D226.jpg){.css} ![](project/s2+s3+1hair/female1group/22D2D2.jpg){.css} ![](project/s2+s3+1hair/female1group/22D2D5.jpg){.css} ![](project/s2+s3+1hair/female1group/22D2D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/22G222.jpg){.css} ![](project/s2+s3+1hair/female1group/22G225.jpg){.css} ![](project/s2+s3+1hair/female1group/22G226.jpg){.css} ![](project/s2+s3+1hair/female1group/22G2D2.jpg){.css} ![](project/s2+s3+1hair/female1group/22G2D5.jpg){.css} ![](project/s2+s3+1hair/female1group/22G2D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/2E2222.jpg){.css} ![](project/s2+s3+1hair/female1group/2E2225.jpg){.css} ![](project/s2+s3+1hair/female1group/2E2226.jpg){.css} ![](project/s2+s3+1hair/female1group/2E22D2.jpg){.css} ![](project/s2+s3+1hair/female1group/2E22D5.jpg){.css} ![](project/s2+s3+1hair/female1group/2E22D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/2E5222.jpg){.css} ![](project/s2+s3+1hair/female1group/2E5225.jpg){.css} ![](project/s2+s3+1hair/female1group/2E5226.jpg){.css} ![](project/s2+s3+1hair/female1group/2E52D2.jpg){.css} ![](project/s2+s3+1hair/female1group/2E52D5.jpg){.css} ![](project/s2+s3+1hair/female1group/2E52D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/2E6222.jpg){.css} ![](project/s2+s3+1hair/female1group/2E6225.jpg){.css} ![](project/s2+s3+1hair/female1group/2E6226.jpg){.css} ![](project/s2+s3+1hair/female1group/2E62D2.jpg){.css} ![](project/s2+s3+1hair/female1group/2E62D5.jpg){.css} ![](project/s2+s3+1hair/female1group/2E62D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/2ED222.jpg){.css} ![](project/s2+s3+1hair/female1group/2ED225.jpg){.css} ![](project/s2+s3+1hair/female1group/2ED226.jpg){.css} ![](project/s2+s3+1hair/female1group/2ED2D2.jpg){.css} ![](project/s2+s3+1hair/female1group/2ED2D5.jpg){.css} ![](project/s2+s3+1hair/female1group/2ED2D6.jpg){.css}  
![](project/s2+s3+1hair/female1group/2EG222.jpg){.css} ![](project/s2+s3+1hair/female1group/2EG225.jpg){.css} ![](project/s2+s3+1hair/female1group/2EG226.jpg){.css} ![](project/s2+s3+1hair/female1group/2EG2D2.jpg){.css} ![](project/s2+s3+1hair/female1group/2EG2D5.jpg){.css} ![](project/s2+s3+1hair/female1group/2EG2D6.jpg){.css}  

- 男2组：  
![](project/s2+s3+1hair/male2group/IIIIII.jpg){.css} ![](project/s2+s3+1hair/male2group/IIIIIL.jpg){.css} 

- 女2组：横田进1人。    

- 男3组：  
![](project/s2+s3+1hair/male3group/CCCCCC.jpg){.css} ![](project/s2+s3+1hair/male3group/CCCCCF.jpg){.css} ![](project/s2+s3+1hair/male3group/CCCCFC.jpg){.css} ![](project/s2+s3+1hair/male3group/CCCCFF.jpg){.css} ![](project/s2+s3+1hair/male3group/CCFCCC.jpg){.css} ![](project/s2+s3+1hair/male3group/CCFCCF.jpg){.css} ![](project/s2+s3+1hair/male3group/CCFCFC.jpg){.css} ![](project/s2+s3+1hair/male3group/CCFCFF.jpg){.css}  
![](project/s2+s3+1hair/male3group/CFCCCC.jpg){.css} ![](project/s2+s3+1hair/male3group/CFCCCF.jpg){.css} ![](project/s2+s3+1hair/male3group/CFCCFC.jpg){.css} ![](project/s2+s3+1hair/male3group/CFCCFF.jpg){.css} ![](project/s2+s3+1hair/male3group/CFFCCC.jpg){.css} ![](project/s2+s3+1hair/male3group/CFFCCF.jpg){.css} ![](project/s2+s3+1hair/male3group/CFFCFC.jpg){.css} ![](project/s2+s3+1hair/male3group/CFFCFF.jpg){.css}  
- 女3组. 无。  


##### 4.4 **策略4：策略2 + 一个组合结果里只包含2个角色的五官**  
使用4.2里的角色和分组。组合结果罗列如下, 点击图片看大图。    

- 男1组：  
    - 雅彦的五官被(其他角色的五官)替换。阴影列表示该角色本身，所以可忽略该列：    
    ![](img/face_compose_char_flat_male1group_vs_masahiko.jpg)  
    - 盐谷的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_shya.jpg)  
    - 空的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_sora.jpg)  
    - 熏的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_kaoru.jpg)  
    - 江岛的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_ejima.jpg)  
    - 辰巳的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_tatsumi.jpg)  
    - 仁科的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_nishina.jpg)  
    - 文哉的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_fumiya.jpg)  
    - 松下敏史的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_matsu.jpg)  
    - 阿透的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_tooru.jpg)  
    - 一树的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_kazuki.jpg)  
    - 古屋公弘的五官被替换：  
    ![](img/face_compose_char_flat_male1group_vs_kimihiro.jpg)  

- 女1组：  
    - 紫苑的五官被(其他角色的五官)替换。阴影列表示该角色本身，所以可忽略该列：    
    ![](img/face_compose_char_flat_female1group_vs_shion.jpg)  
    - 雅美的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_masami.jpg)  
    - 紫的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_yukari.jpg)  
    - 叶子的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_yoko.jpg)  
    - 浩美的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_hiromi.jpg)  
    - 真琴的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_makoto.jpg)  
    - 早纪的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_saki.jpg)  
    - 浅葱的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_asagi.jpg)  
    - 顺子的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_yoriko.jpg)  
    - 森的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_mori.jpg)  
    - 叶子母的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_ykma.jpg)  
    - 理沙的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_risa.jpg)  
    - 美菜的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_mika.jpg)  
    - 齐藤茜的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_akane.jpg)  
    - 齐藤玲子的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_reiko.jpg)  
    - 京子的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_kyoko.jpg)  
    - 千夏的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_chinatsu.jpg)  
    - 麻衣的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_mai.jpg)  
    - 古屋朋美的五官被替换：  
    ![](img/face_compose_char_flat_female1group_vs_tomomi.jpg)  

- 男2组：  
    - 宪司的五官被替换：  
    ![](img/face_compose_char_flat_male2group_vs_kenji.jpg)  
    - 和人的五官被替换：  
    ![](img/face_compose_char_flat_male2group_vs_kazuto.jpg)  
    - 和子的五官被替换：  
    ![](img/face_compose_char_flat_male2group_vs_kazuko.jpg)  

- 女2组，只有横田进1人，无法被替换。  

- 男3组：  
    - 摄像师的五官被替换（这两个角色的五官组合有点滑稽）：  
    ![](img/face_compose_char_flat_male3group_vs_cameraman.jpg)  
    - 导演的五官被替换（这两个角色的五官组合有点滑稽）：  
    ![](img/face_compose_char_flat_male3group_vs_director.jpg)  

- 女3组：  
    - 绫子的五官被替换：  
    ![](img/face_compose_char_flat_female3group_vs_aya.jpg)  
    - 典子的五官被替换：  
    ![](img/face_compose_char_flat_female3group_vs_tenko.jpg)  

- 附带：  
    - 横田进的五官被替换：  
    ![](img/face_compose_char_flat_male2group_vs_susumu.jpg)  
    - 葵的五官被替换：  
    ![](img/face_compose_char_flat_male2group_vs_aoi.jpg)  


- **总结**  
以上只是显示了部分组合结果。

未显示的那些组合结果可能会有一些有趣的东西。以后可以继续来做这些事。      


**参考资料**：  
1. [FC的画风-面部-眉毛](../fc_drawing_style__face_brow/readme.md)  
2. [FC的画风-面部-眼睛](../fc_drawing_style__face_eye2/readme.md)  
3. [FC的画风-面部-鼻子](../fc_drawing_style__face_nose2/readme.md)  
4. [FC的画风-面部-嘴](../fc_drawing_style__face_mouth2/readme.md)  
5. [FC的画风-面部-脸形](../fc_drawing_style__face/readme.md)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

<style type="text/css">
.css{  
    zoom: 50%;
    
}
</style>
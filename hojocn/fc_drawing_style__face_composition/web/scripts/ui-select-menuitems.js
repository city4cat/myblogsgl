
const t_menuitems = document.createElement('template');
t_menuitems.innerHTML =`
<option value="-_">-</option>
<option value="0_masahiko">Masahiko/雅彦</option>
<option value="1_masami">Masami/雅美</option>
<option value="2_shion">Shion/紫苑</option>
<option value="3_shya">Shya/盐谷</option>
<option value="4_sora">Sora/空</option>
<option value="5_yukari">Yukari/紫</option>
<option value="6_yoko">Yoko/叶子</option>
<option value="7_hiromi">Hiromi/浩美</option>
<option value="8_kaoru">Kaoru/熏</option>
<option value="9_ejima">Ejima/江岛</option>
<option value="A_tatsumi">Tatsumi/辰巳</option>
<option value="B_makoto">Makoto/真琴</option>
<option value="C_director">Director/导演</option>
<option value="D_saki">Saki/早纪</option>
<option value="E_asagi">Asagi/浅葱</option>
<option value="F_cameraman">Cameraman/摄像师</option>
<option value="G_yoriko">Yoriko/顺子</option>
<option value="H_mori">Mori/森</option>
<option value="I_kazuko">Kazuko/和子</option>
<option value="J_grandpa">Grandpa/爷爷</option>
<option value="K_susumu">Susumu/</option>
<option value="L_kenji">Kenji/宪司</option>
<option value="M_ykma">Yoko's Mother/叶子母</option>
<option value="N_risa">Risa/理沙</option>
<option value="O_mika">Mika/美菜</option>
<option value="P_akane">Akane/齐藤茜</option>
<option value="Q_nishina">Nishina/仁科</option>
<option value="R_aoi">Aoi/葵</option>
<option value="S_grandma">Grandma/奶奶</option>
<option value="T_fumiya">Fumiya/文哉</option>
<option value="U_reiko">Reiko/齐藤玲子</option>
<option value="V_matsu">Matsu/松下敏史</option>
<option value="W_shoko">Shoko/章子</option>
<option value="X_tooru">Tooru/阿透</option>
<option value="Y_kyoko">Kyoko/京子</option>
<option value="Z_kazuma">Kazuma/一马</option>
<option value="a_kazuki">Kazuki/一树</option>
<option value="b_chinatsu">Chinatsu/千夏</option>
<option value="c_mai">Mai/麻衣</option>
<option value="d_aya">Aya/绫子</option>
<option value="e_tenko">Tenko/典子</option>
<option value="f_kimihiro">Kimihiro/古屋公弘</option>
<option value="g_tomomi">Tomomi/古屋朋美</option>
<option value="h_hanzu">Hanzu/八不</option>
<option value="i_te">Te/哲</option>
<option value="j_kazuto">Kazuto/和人</option>
`;

document.getElementById('select_hair').appendChild(t_menuitems.content.cloneNode(true));
document.getElementById('select_brow').appendChild(t_menuitems.content.cloneNode(true));
document.getElementById('select_eye').appendChild(t_menuitems.content.cloneNode(true));
document.getElementById('select_nose').appendChild(t_menuitems.content.cloneNode(true));
document.getElementById('select_mouth').appendChild(t_menuitems.content.cloneNode(true));
document.getElementById('select_face').appendChild(t_menuitems.content.cloneNode(true));

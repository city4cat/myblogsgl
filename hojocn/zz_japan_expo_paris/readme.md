
source:  
https://www.japan-expo-paris.com/en/invites/tsukasa-hojo_1741.htm  
([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96824))  

## Tsukasa HÔJÔ

From City Hunter to Cat’s Eye, F. Compo and many more, Tsukasa HÔJÔ’s work is part of the History of manga and he is still to this day one of the most iconic mangaka for whole generations. The year 2020 is the 40th anniversary of his career and he will be celebrating this at Japan Expo with his fans on Thursday, Friday, and Sunday! On this occasion, he is the Manga Guest of Honor of the 21st Impact!  
从《城市猎人》到《猫眼》、F.Compo等等，北条司的作品是漫画史的一部分，他至今仍是整个世代最具代表性的漫画家之一。 2020年是他职业生涯的40周年，他将在周四、周五和周日在Japan Expo(日本博览会)上与他的粉丝们一起庆祝！ 在这个场合，他是《21st Impact》的漫画嘉宾！  

Posted on December 20, 2019  
发表于2019年12月20日

## Tsukasa HÔJÔ's debut  

Tsukasa HÔJÔ was born on March 5, 1959 in Fukuoka Prefecture. He showed no special interest in manga as a kid, until discovering Ultra Q in primary school. Even then though, if he liked drawing, he still preferred watching TV to reading manga. In college, he turned to cinema, hoping to work in this field. At the time, he often studied in a café where he also read a lot of manga. If becoming a mangaka was still not part of the plan, **he drew a few one shots while still in college**, to enter contests and make some money.  
北条司于1959年3月5日出生于福冈县。 他小时候对漫画没有特别的兴趣，直到在小学时发现Ultra Q。 即使那样，即使他喜欢绘画，他仍然还是喜欢看电视而不是看漫画。 在大学里，他转向电影，希望能在这一领域工作。 那时，他经常在一家咖啡馆读书，在那里他还读了很多漫画。 如果成为漫画家还不是计划的一部分，**他还在上大学时就画了一些尝试性的作品**，参加比赛并赚了一些钱。

In 1979, he was **among the finalists of the 18th Tezuka Award** organized by Shônen Jump magazine with Space Angel. That’s when he met Nobuhiko HORIE who was to become his editor at Shônen Jump and drove him to keep writing. He then created **Ore wa Otoko Da!, the short story that marked the beginning of his mangaka career in 1980**. Ore wa Otoko Da! was later published again in a collection of short stories.   
1979年，他凭借《太空天使》入围由《少年Jump》杂志举办的“第18届手冢奖决赛”。 那时他遇见堀江信彦（Nobuhiko HORIE），他将成为《少年Jump》的编辑，并驱使他继续创作。 然后，他创作了**短篇故事《我是男子汉！》，这是他1980年开始从事漫画家生涯的故事**。 《我是男子汉！》 后来又以短篇小说集再次出版。
 
 
### Cat's Eye, the first success  
猫眼，第一个成功  

With the success of **Cat’s Eye, another one shot at first**, Shônen Jump urged Tsukasa HÔJÔ to write more of it. It became a long series that made the artist move to Tokyo. He worked on Cat’s Eye between 1981 and 1985.   
随着《猫眼》（Cat's Eye）的成功，**最初只是又一次尝试**，《少年Jump》敦促北条司创作更多故事。 经过漫长的创作，这位漫画家搬到了东京。 他在1981年至1985年之间连载《猫眼》。

In 1983, Tokyo Movie Shinsha made **an animated adaptation** of the series. That’s how the French public happily discovered on TV the three Kisugi sisters, cunning and lovable burglars who steal works of art that used to belong to their missing father, in the hope of finding him. 
1983年，东京Shinsha电影改编了该系列的动画片。 法国民众就是这样在电视上愉快地发现了来生三姐妹，她们是狡猾而可爱的窃贼，偷走了曾属于父亲的艺术品，并希望找到她们失踪的父亲。


### the recognition with City Hunter  
城市猎人的认可
 

When Cat’s Eye ended, Tsukasa HÔJÔ’s editor invited him to launch a new manga. When he was still working on Cat’s Eye, he had drafted several short stories, including XYZ, which was to give birth to **City Hunter**, a long series started in 1985 and which remains the author’s most iconic work. Yet success did not come at once. City Hunter started as a dark story which only appealed to the readers when Tsukasa HÔJÔ added some humor in it, transforming his hero, Ryo Saeba, into a complex character, both a failed womanizer and a charismatic sweeper and private detective, who evolves beside Kaori and many more colorful characters.   
当《猫眼》结束时，北条司的编辑邀请他创作新的漫画。 当他仍在连载《猫眼》时，他粗略地画了一些短篇故事，其中包括XYZ，这产生了一个诞生于1985年的长篇连载《城市猎人》，它至今仍是作者最具标志性的作品。 然而，成功并没有立即出现。 City Hunter最初是一个黑暗的故事，当北条司在其中添加了一些幽默情节后，才吸引了读者，将他的英雄冴羽獠转变成一个复杂的角色，既是失败的女性主义者，又是超凡脱俗的清道夫和私人侦探，在他身边有香，还有其他丰富多彩的角色。

**Studio Sunrise adapted City Hunter as an anime** from 1987 and that’s how it first came to France, with instant success. Since then, City Hunter has had **many more adaptations**, with several animation films in the 90s or recently **Shinjuku Private Eyes**, approved by Tsukasa HÔJÔ and released in 2019. **Live action films** have also been realized, one by Jing WONG starring Jackie CHAN in 2002, and a French one by director and actor Philippe LACHEAU, Nicky Larson et le parfum de Cupidon, in 2019 and which made its way to Japan where it has been released as City Hunter the movie on November 29.   
**Sunrise工作室于1987年改编了《城市猎人》作为动画**，法国最初看的就是动画版，而且立刻火遍全国。 从那时起，《城市猎人》有了“更多改编”，其中包括90年代的几部动画电影，或者最近由北条司批准并于2019年发行的**《新宿私家侦探》 **。 **真人动作电影**是由王晶（Jing Wong）在2002年由成龙（Jackie CHAN）主演；法国版的真人动作电影是由导演兼演员Philippe LACHEAU拍摄的《Nicky Larson之丘比特香水》，于2019年进入日本，并以《城市猎人》（City Hunter）的形式发行。 电影于11月29日上映。

**Tsukasa HÔJÔ has also gone further in the world of City Hunter with Angel Heart**, from 2001 to 2010, a manga he describes not as a sequel but as an alternative story. Much darker, Angel Heart is about Glass Heart, a 14-year old hitwoman who is transplanted with Kaori's heart, who died in an accident. With her heart, Kaori's memories have survived, especially her feelings for Ryô. Glass Heart sets to find him. Angel Heart is one of Tsukasa HÔJÔ's longest series with **a second season** published from 2010 to 2017.  
** 在《城市猎人》的世界里继续创作，北条司带来了《天使之心》**，从2001年到2010年，他这部漫画不是续集，而是另一个故事。 更阴暗的是，天使之心（Angel Heart）是关于玻璃心（Glass Heart）的，这是一个14岁的杀手，被移植了死于车祸的香的心脏。 凭着香的心脏，香的记忆得以保存，尤其是她对獠的感情。 玻璃心开始寻找獠。 《天使心》是北条司发行时间最长的连载之一，《第二季》于2010年至2017年出版。

### From mangaka to film director  
从漫画家到电影导演

After City Hunter came to an end in 1992, Tsukasa HÔJÔ worked on **Rash!!** (1994 - 1995), a two-volume series about Yûki Asaka, a doctor who succeeds her grandmother in a prison and who has a strong tendency to get into trouble. A year later, he started **F. Compo** (**Family Compo**, 1996 - 2000), a manga about family and genre.  
在1992年《城市猎人》（City Hunter）结束后，北条司创作了**《 Rash !! 》**（1994年至1995年），漫画分两卷，讲述了勇希的故事。。她是一位医生，继承了祖母在一个监狱的工作，但她容易陷入麻烦。 一年后，北条司开始了** F. Compo **（** Family Compo **，1996-2000），关于家庭和性别的漫画。

Now, Tsukasa HÔJÔ helps to educate young mangaka. He has also come back to his first love, cinema, and is **working as a film director for the first time on Angel Sign**, an anthology of five silent manga from all over the world. These works have competed at Silent Manga Audition, the biggest silent manga contest in the world and for which Tsukasa HÔJÔ is a member of the jury. He is also in charge of the prologue and epilogue of the film. Have a look at the trailer:   
映画『エンジェルサイン』予告編 （77秒）/ Angel Sign - Teaser trailer (77sec ver.) 

现在，北条司在帮助培养年轻的漫画家。 他回到了自己的最初喜欢的电影世界，并**在《 Angel Sign》里首次担任电影导演**，《 Angel Sign》是一个漫画集，收录了来自世界各地的五个无对白漫画。 这些作品曾参加世界上最大的无对白漫画竞赛“默白漫画大赛”，而北条司则是评审团的成员。 他还负责《 Angel Sign》电影的序幕和结语。 看看预告片：  
电影“天使印记”预告片（77秒）/ Angel Sign - 前导预告片（77秒版）

As he will be celebrating **the 40th anniversary of his career** in 2020, there is no doubt Tsukasa HÔJÔ is **one of the most renowned mangaka in the world**, mainly due to his famous series Cat’s Eye, City Hunter, and Angel Heart. He got known as soon as his career began and **his work is still alive to this day**, in his own creations, in new editions of his manga, and in the works of other artists he inspired. In 2017, to mention one of them, the manga City Hunter Rebirth was released, a spin-off by Sokura NISHIKI who worked on the script with Tsukasa HÔJÔ himself.  
由于他将在2020年庆祝其**出道40周年**，毫无疑问北条司是**世界上最著名的漫画家之一**，这主要归功于他著名的系列《猫眼》，《城市猎人》 和Angel Heart。 他在职业生涯的一开始就广为人知，而且**他的作品现在也还有生命力**，这体现在他的创造的角色和故事里、漫画的新版本以及受他启发的其他漫画作品里。 例如，其中一个，在2017年发行了漫画《城市猎人重生》，这是由Sokura NISHIKI衍生出来的，Sokura NISHIKI与北条司亲自合作编写了剧本。

Japan Expo is honored to celebrate the 40th anniversary of Tsukasa HÔJÔ’s career and is inviting you to meet this legendary mangaka at the festival **on Thursday, Friday, and Sunday**. He is the **Manga Guest of Honor** of the festival and will be holding a **live drawing, signings, and a masterclass**. His schedule will be updated regularly below and [on this page](https://www.japan-expo-paris.com/en/programme).  
Japan Expo很荣幸能庆祝北条司出道40周年，并邀请您**在周四、周五和周日**的节日上与这位传奇漫画家见面。 他是节日的“**荣誉漫画嘉宾**”，并将举行“**现场绘画、签名和大师班**”。 他的日程安排将定期更新在下面和[在此页面上](https://www.japan-expo-paris.com/en/programme)。

### Bibliography  
参考书目

    - Tenshi no Okurimono (short stories, 1980 - 1988)  
    - Cat's Eye (series, 1981 - 1985)  
    - City Hunter (series, 1985 - 1991)  
    - Sakura no Hana Sakukoro (short stories, 1989 - 1993)  
    - Komorebi no Moto De... (short stories, 1993 - 1994)  
    - Rash!! (series, 1994 - 1995)  
    - Melody of Jenny (short stories, 1995)  
    - F. Compo (series, 1996 - 2000)  
    - Angel Heart (series, 2001 - 2010)  
    - Angel Heart Saison 2 (series, 2010 - 2017)  

Many thanks to [Ki-oon](http://www.ki-oon.com/) and [Panini Manga](http://www.paninicomics.fr/web/guest/home) publishing for allowing us to use illustrations from Trésors de Tsukasa Hôjô
非常感谢[Ki-oon]（http://www.ki-oon.com/）和[Panini Manga]（http://www.paninicomics.fr/web/guest/home）的发布，使我们能够使用 Trésorsde 北条司的插图

## Tsukasa HÔJÔ

### Date of birth  
March 05, 1959

### Country  
Japan  

### Occupation  
Director  
Mangaka  

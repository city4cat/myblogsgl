([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96823))  

法国粉丝出了一本书：  
Forever Tsukasa Hojo  
（译：永远的北条司）

以下是法译中，google机翻，有点惨不忍睹。

------------------------

------------------------

## 出版社的页面： 
 
https://www.editionspixnlove.com/pop-n-love/900-forever-tsukasa-hojo-magnum-375-edition.html

![](https://www.editionspixnlove.com/3091-thickbox_default/forever-tsukasa-hojo-magnum-375-edition.webp) 

![](https://www.editionspixnlove.com/3086-thickbox_default/forever-tsukasa-hojo-magnum-375-edition.webp) 

![](https://www.editionspixnlove.com/3087-thickbox_default/forever-tsukasa-hojo-magnum-375-edition.webp) 

![](https://www.editionspixnlove.com/3088-thickbox_default/forever-tsukasa-hojo-magnum-375-edition.webp) 


### 图书信息：  

Forever Tsukasa Hojo - Magnum 357 Edition

De Cat’s Eye en passant par City Hunter, Family Compo ou Angel Heart, découvrez l’œuvre et l’histoire du célèbre mangaka Tsukasa Hojo.   
从猫眼到城市猎人、非常家庭和天使心，探索著名的漫画家北条司的作品和历史。

---

EDITION COLLECTOR MAGNUM 357 EDITION


Cette édition limitée et numérotée comprend :

      Un fourreau cartonné exclusif avec découpe.
      Un certificat d’authenticité numéroté à 1000 ex.
      Le livre Forever Tsukasa Hojo en couverture cartonnée rigide avec tranchefile.  
此限量编号版本包括：   
       独家镂空纸板套。  
       真实性证书编号为1000份。  
       《Forever Tsukasa Hojo》这本精装书，带边带。  
       
------------------------

XYZ. Trois lettres qui ont frappé l’imaginaire de toute une génération, et qui ramènent directement devant le tableau de la gare de Shinjuku. Ce dernier n’a pas changé de place ; il reste à l’endroit de vos souvenirs. Des lettres synonymes de dernier espoir, qui permettent de faire appel à City Hunter, une entité composée de Ryō Saeba et Kaori Makimura. Du roman noir à la comédie policière débridée, l’histoire de cet improbable duo va toucher le cœur des lecteurs, puis des spectateurs. Dans l’hexagone, il sera Nicky Larson, un compagnon de route d’une enfance passée devant le Club Dorothée.  
XYZ，三个字母吸引了整整一代人的想象力，并直接通向新宿站的留言板。 这块黑板没有改变地方； 它仍然留在你的记忆中。 代表最后希望的这三个字母，是用来呼唤由伢羽獠和木村香组成的城市猎人。 从黑色小说到无拘无束的侦探喜剧，这个不可思议的二人组合先是打动了读者，然后又打动了观众。 在法国，他是Nicky Larson，那个曾在多萝西俱乐部（Club Dorothée）前伴你度过童年时光的旅伴。

Si l’on associe volontiers City Hunter à Tsukasa Hōjō, l’artiste japonais a façonné bien plus que cette unique œuvre. Des courses effrénées sur les toits des agiles Cat’s Eye à l’intimité d’une famille un peu particulière dans F.Compo, en passant par l’écoute de l’âme des plantes de Sous un rayon de soleil, l’homme est le père d’une galerie de personnages hauts en couleur, sensibles et touchants.  
如果说一提到北条司就很容易想到《城市猎人》，那么这位日本漫画家塑造的可不仅这一部作品。 从在屋顶上的疯狂追逐的敏捷《猫眼》，到一个有亲密关系的特殊家庭的《非常家庭》，再到聆听植物心声的《艳阳少女》，作者塑造了一个又一个丰富多彩、敏感且感人的角色。

Si Nicky Larson ne craint personne, Tsukasa Hōjō non plus. Le natif de Kyushu a connu une ascension fulgurante, qui lui a permis de s’évader du carcan du Weekly Shōnen Jumppour aborder des sujets de plus en plus graves. Avec, toujours, le souci de réunir les êtres au travers de l’art, de composer une nouvelle famille qui ne se réduit pas aux liens du sang.  
如果Nicky Larson不惧怕任何人，那么北条司也不会怕任何人。 这位九州本地人迅速成名，这使他得以摆脱《周刊少年》连载的束缚，可以创作日益严肃的话题。作者的漫画始终在讲述一个主题：将(没有血缘关系的)人们集合在一起组建成一个家庭，家的温暖归根结底不在于血缘关系。  

Ce livre est le récit de sa quête d’émancipation. Et de liberté.  
这本书是他追求解放和自由的故事。

Au sommaire :  
Préface de Nico Prat (auteur et réalisateur).  
1. Né sous les mêmes étoiles
2. Venu de nulle part, c’est Tsukasa
3. Mysterious Girls
4. L’héritage des Cat’s Eye
5. Le samouraï
6. It’s your dream or my dream or somebody’s dream
7. Nicky Larson ne craint personne
8. Dans l’intimité du Maître
9. Transgressions
10. Angel Heart ou la mécanique du cœur
11. Forever City Hunter

内容简介：  
Nico Prat（作者兼导演）的序言。  
1.生在同一个星空下  
2.初出茅庐，北条司  
3.神秘女孩  
4.猫眼的遗产  
5.武士  
6.这是你的梦想，我的梦想或某人的梦想  
7.Nicky Larson谁都不怕  
8.在主人的隐私下  
9.性别转换  
10.天使之心或心脏的机理  
11.永远的城市猎人

À propos du livre :  
Titre : Forever Tsukasa Hojo – Portrait du père de City Hunter  
Auteur : Pierre-William Fregonese  
Date de parution : début août 2020  
Nombre de pages : 240  
Format : 16 x 24 cm  
Couverture : cartonnée avec vernis sélectif  
Papier : mat  
Impression : noir & blanc sans visuel  
Inclus dans cette édition :  
    - Un fourreau cartonné exclusif avec découpe.  
    - Un certificat d'authenticité numéroté à 1000 ex.  
    - Le livre « Forever Tsukasa Hojo » en couverture cartonnée rigide avec tranchefile.  

关于本书：  
标题：Forever Tsukasa Hojo – 城市猎人之父的点点滴滴  
作者：Pierre-William Fregonese  
出版日期：2020年8月初  
页数：240  
大小：16 x 24 cm  
封面：选择性清漆纸板  
纸：垫子  
印刷：黑白无视觉  
此版本中包括：  
     - 独家镂空纸板套。  
     - 真实性证书编号为1000份。  
     - 这本书“ Forever Tsukasa Hojo”在带有封边带的精装书中。  


Pierre-William Fregonese  
Pierre-William Fregonese est docteur en science politique de l'Université Panthéon-Assas, chercheur associé au Centre Thucydide et enseigne à Sciences Po Lille et à l'EMIC. Ancien chercheur invité à l'Université de Kobe, ses recherches portent notamment sur les influences culturelles de la pop et de la geek culture en Europe et en Asie de l'est (Chine, Japon, Corée du sud).  

关于作者Pierre-William Fregonese：  
Pierre-William Fregonese拥有Panthéon-Assas大学的政治科学博士学位，是Thucydide中心的副研究员，并在波里尔科学学院和EMIC任教。 前神户大学访问学者，他的研究特别关注流行和极客文化在欧洲和东亚（中国，日本，韩国）的文化影响。  

------------------------

------------------------

## 亚马逊的页面：    
https://www.amazon.com/Forever-Tsukasa-Hojo-Portrait-Hunter/dp/2371880833/

![](https://images-na.ssl-images-amazon.com/images/I/81P-249aUJL.jpg) 


### 图书信息：    
Publisher : PIX N LOVE (September 4, 2020)
Language: : French
ISBN-10 : 2371880833
ISBN-13 : 978-2371880832
Item Weight : 1.58 pounds
Dimensions : 6.3 x 1.06 x 9.45 inches
出版社：PIX N LOVE（2020年9月4日）
语言：法文
书号ISBN-10：2371880833
书号ISBN-13：978-2371880832
产品重量：1.58磅
尺寸：6.3 x 1.06 x 9.45英寸

### Reviews:    
Beaucoup d'anecdotes inutiles et sans intérêt pour gonfler le nombre de page du livre. Livre dispensable.   
(英译：A lot of useless and uninteresting anecdotes to inflate the number of pages of the book. Dispensable book.)

从评论可以看出，书里有一些关于北条司的轶事。


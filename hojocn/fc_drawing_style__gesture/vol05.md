# FC的画风-动作、姿态  

## 目录  
[Readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

按动作、姿态分类：  
[吃](./gesture_eat.md), 
[其他](./gesture_else.md), 
[跳](./gesture_jump.md), 
[跑](./gesture_run.md), 
[坐](./gesture_sit.md), 
[蹲](./gesture_squat.md), 
[站](./gesture_stand.md), 
[走](./gesture_walk.md), 


## Vol05

- 05_011_3，  
![](img/05_011_3__mod.jpg) 
![](img/05_011_3__mod_force-line.jpg) 


- 05_012_6，  
![](img/05_012_6__mod.jpg) 
![](img/05_012_6__mod_force-line.jpg) 

- 05_014_4，  
![](img/05_014_4__mod.jpg) 
![](img/05_014_4__mod_force-line.jpg) 


- 05_015_5，  
![](img/05_015_5__mod.jpg) 
![](img/05_015_5__mod_force-line.jpg) 

- 05_018_0，  
![](img/05_018_0__mod.jpg) 
![](img/05_018_0__mod_force-line.jpg) 

- 05_019_4，  
![](img/05_019_4__mod.jpg) 
![](img/05_019_4__mod_force-line.jpg) 

- 05_019_5，  
![](img/05_019_5__mod.jpg) 
![](img/05_019_5__mod_force-line.jpg) 


- 05_021_2，  
![](img/05_021_2__mod.jpg) 
![](img/05_021_2__mod_force-line.jpg) 

- 05_021_6，  
![](img/05_021_6__mod.jpg) 
![](img/05_021_6__mod_force-line.jpg) 

- 05_022_0，  
![](img/05_022_0__mod_0.jpg) 
![](img/05_022_0__mod_0_force-line.jpg) 

- 05_022_0，  
![](img/05_022_0__mod_1.jpg) 
![](img/05_022_0__mod_1_force-line.jpg) 

- 05_022_1，  
![](img/05_022_1__mod.jpg) 
![](img/05_022_1__mod_force-line.jpg) 

- 05_023_1，  
![](img/05_023_1__mod.jpg) 
![](img/05_023_1__mod_force-line.jpg) 


- 05_023_4，  
![](img/05_023_4__mod.jpg) 
![](img/05_023_4__mod_force-line.jpg) 

- 05_024_1，  
![](img/05_024_1__mod.jpg) 
![](img/05_024_1__mod_force-line.jpg) 

- 05_031_0，  
![](img/05_031_0__mod.jpg) 
![](img/05_031_0__mod_force-line.jpg) 

- 05_033_0，  
![](img/05_033_0__mod.jpg) 
![](img/05_033_0__mod_force-line.jpg) 

- 05_037_4，  
![](img/05_037_4__mod.jpg) 
![](img/05_037_4__mod_force-line.jpg) 


- 05_038_3，  
![](img/05_038_3__mod.jpg) 
![](img/05_038_3__mod_force-line.jpg) 


- 05_038_5，  
![](img/05_038_5__mod.jpg) 
![](img/05_038_5__mod_force-line.jpg) 


- 05_040_2，  
![](img/05_040_2__mod.jpg) 
![](img/05_040_2__mod_force-line.jpg) 

- 05_041_0，  
![](img/05_041_0__mod.jpg) 
![](img/05_041_0__mod_force-line.jpg) 

- 05_041_2，  
![](img/05_041_2__mod.jpg) 
![](img/05_041_2__mod_force-line.jpg) 


- 05_051. 紫驾车逆行。右边的路人虽然只画了轮廓，但能看得出很惊讶。  
![](./img/05_051_0__.jpg)


- 05_055_3，  
![](img/05_055_3__mod.jpg) 
![](img/05_055_3__mod_force-line.jpg) 


- 05_060_3，  
![](img/05_060_3__mod.jpg) 
![](img/05_060_3__mod_force-line.jpg) 


- 05_060_4，  
![](img/05_060_4__mod.jpg) 
![](img/05_060_4__mod_force-line.jpg) 

- 05_063_3，  
![](img/05_063_3__mod.jpg) 
![](img/05_063_3__mod_force-line.jpg) 

- 05_063_4，  
![](img/05_063_4__mod.jpg) 
![](img/05_063_4__mod_force-line.jpg) 


![](img/05_075_2__sora_side_surprise_fear.jpg) 

- 05_083_0，  
![](img/05_083_0__mod.jpg) 
![](img/05_083_0__mod_force-line.jpg) 

- 05_098_1，  
![](img/05_098_1__mod.jpg) 
![](img/05_098_1__mod_fl.jpg) 


- 05_099_3，  
![](img/05_099_3__mod.jpg) 
![](img/05_099_3__mod_fl.jpg) 

- 05_111_0，  
![](img/05_111_0__mod.jpg) 
![](img/05_111_0__mod_fl.jpg) 


- 05_117_4，  
![](img/05_117_4__mod.jpg) 
![](img/05_117_4__mod_fl.jpg) 

- 05_118_2，  
![](img/05_118_2___mod.jpg) 
![](img/05_118_2___mod_fl.jpg) 

- 05_119_2，  
![](img/05_119_2__.jpg) 
![](img/05_119_2__fl.jpg) 

- 05_126_5，  
![](img/05_126_5__mod.jpg) 
![](img/05_126_5__mod_fl.jpg) 

- 05_129_6，  
![](img/05_129_6__mod.jpg) 
![](img/05_129_6__mod_fl.jpg) 

- 05_133_6，  
![](img/05_133_6__mod.jpg) 
![](img/05_133_6__mod_fl.jpg) 

- 05_134_2，  
![](img/05_134_2__mod.jpg) 
![](img/05_134_2__mod_fl.jpg) 

- 05_138_0，  
![](img/05_138_0__mod.jpg) 
![](img/05_138_0__mod_fl.jpg) 

- 05_142_0，  
![](img/05_142_0__mod.jpg) 
![](img/05_142_0__mod_fl.jpg) 

- 05_155_0，  
![](img/05_155_0__mod.jpg) 
![](img/05_155_0__mod_fl.jpg) 

- 05_167_0，  
![](img/05_167_0__mod.jpg) 
![](img/05_167_0__mod_fl.jpg) 

- 05_193_3，  
![](img/05_193_3__mod.jpg) 
![](img/05_193_3__mod_fl.jpg) 

- 05_194_1，  
![](img/05_194_1__mod.jpg) 
![](img/05_194_1__mod_fl.jpg) 


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
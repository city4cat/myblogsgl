# FC的画风-动作、姿态  

## 目录  
[Readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

按动作、姿态分类：  
[吃](./gesture_eat.md), 
[其他](./gesture_else.md), 
[跳](./gesture_jump.md), 
[跑](./gesture_run.md), 
[坐](./gesture_sit.md), 
[蹲](./gesture_squat.md), 
[站](./gesture_stand.md), 
[走](./gesture_walk.md), 


## Vol02  



- 02_016_2, 看到雅彦珍爱的mark杯掉落，众人慌张。动作很生动。    
![](img/02_016_2__.jpg)  


- 02_018_4，02_018_7，一边生气、一边吃饭。  
![](img/02_018_4__mod.jpg) 
![](img/02_018_7__mod.jpg)  


- 02_040_5，紫苑一边避开紫和空、一边打算和雅彦说悄悄话。  
![](img/02_040_5__mod.jpg)  


- 02_050_1， 紫苑的坐姿很美（紫苑翘二郎腿）。雅彦搭在腿上的右手的手指很生动。  
![](img/02_050_1__.jpg)  


- 02_051_2，02_054_0，紫苑的坐姿很美。  
![](img/02_051_2__mod.jpg) 
![](img/02_054_0__mod.jpg)  


- 02_057_3，雅彦翘二郎腿。  
![](img/02_057_3__.jpg)  


- 02_064_4，02_064_6，动作很生动。  
![](img/02_064_4__mod.jpg) 
![](img/02_064_6__mod.jpg)  


- 02_082_3，紫苑翘二郎腿，空翘二郎腿，两人动作很相似。  
![](img/02_082_3__sora.jpg) 
![](img/02_082_3__shion.jpg)  


- 02_112_2__mod，02_113_3__mod，动作很生动。转身挥手时，换手扶背包。  
![](img/02_112_2__mod.jpg) 
![](img/02_113_3__mod.jpg)  


- 02_116_0,  
![](img/02_116_0__mod.jpg) 
![](img/02_116_0__mod_force-line.jpg)  


- 02_146_4, 走路时，紫可能习惯左手翘起。  
![](img/02_146_4__mod.jpg) 
![](img/02_175_0__.jpg)  



- 02_150_4,  
![](img/02_150_4__mod.jpg) 
![](img/02_150_4__mod_force-line.jpg) 


- 02_164_5, 欢快地跑。右脚尖上扬、右臂的动作很生动。  
![](img/02_164_5__mod.jpg)  
对比叶子(伤心地跑开)03_083_0：  
![](img/03_083_0__mod.jpg)  


- 02_192_2，02_192_4， 八不为找妻子京子而贸然闯入若苗家。此时，他自知理亏、尴尬，不好意思露面。动作很生动--拖着步子、身体左右摇晃。  
![](img/02_192_2__mod.jpg)  
下图重心可能在右脚，也可能不是(而是视角产生的错觉)：    
![](img/02_192_4__hanzu.jpg)  


- 02_192_4，八不与京子和好。两人相依而站，京子两手相握于腰前、腿没有站直--像是传统日本女性穿和服的那种姿态。  
![](img/02_192_4__hanzu.jpg) 
![](img/02_192_4__kyoko.jpg)  


- 02_195_4，紫依靠着空：  
![](img/02_195_4__mod.jpg) 
![](img/02_195_4__mod_force-line.jpg)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
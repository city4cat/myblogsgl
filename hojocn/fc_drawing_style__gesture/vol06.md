# FC的画风-动作、姿态  

## 目录  
[Readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

按动作、姿态分类：  
[吃](./gesture_eat.md), 
[其他](./gesture_else.md), 
[跳](./gesture_jump.md), 
[跑](./gesture_run.md), 
[坐](./gesture_sit.md), 
[蹲](./gesture_squat.md), 
[站](./gesture_stand.md), 
[走](./gesture_walk.md), 


## Vol06

- 06_004_3  
![](img/06_004_3__mod.jpg) 
![](img/06_004_3__mod_fl.jpg) 

- 06_005_5，  
![](img/06_005_5__mod.jpg) 
![](img/06_005_5__mod_fl.jpg) 

- 06_006_5，  
![](img/06_006_5__mod.jpg) 
![](img/06_006_5__mod_fl.jpg) 

- 06_008_3，  
![](img/06_008_3__mod.jpg) 
![](img/06_008_3__mod_fl.jpg) 

- 06_010_3，  
![](img/06_010_3__mod.jpg) 
![](img/06_010_3__mod_fl.jpg) 

- 06_018_4，  
![](img/06_018_4__mod.jpg) 
![](img/06_018_4__mod_fl.jpg) 

- 06_019_7，  
![](img/06_019_7__mod.jpg) 
![](img/06_019_7__mod_fl.jpg) 

- 06_023_4，弹烟灰的动作很生动：  
![](img/06_023_4__mod.jpg) 

- 06_023_5，  
![](img/06_023_5__mod.jpg) 
![](img/06_023_5__mod_fl.jpg) 

- 06_026_0，  
![](img/06_026_0__mod.jpg) 
![](img/06_026_0__mod_fl.jpg) 

- 06_026_4，  
![](img/06_026_4__mod.jpg) 
![](img/06_026_4__mod_fl.jpg) 

- 06_027_2，  
![](img/06_027_2__mod.jpg) 
![](img/06_027_2__mod_fl.jpg) 

- 06_042_0，  
![](img/06_042_0__mod.jpg) 
![](img/06_042_0__mod_fl.jpg) 

- 06_045_1，  
![](img/06_045_1__mod.jpg) 
![](img/06_045_1__mod_fl.jpg) 

- 06_052_0，  
![](img/06_052_0__mod.jpg) 
![](img/06_052_0__mod_fl.jpg) 

- 06_055_0，  
![](img/06_055_0__mod.jpg) 
![](img/06_055_0__mod_fl.jpg) 

- 06_058_2，  
![](img/06_058_2__mod.jpg) 
![](img/06_058_2__mod_fl.jpg) 

- 06_059_3，  
![](img/06_059_3__mod.jpg) 
![](img/06_059_3__mod_fl.jpg) 

- 06_062_1，  
![](img/06_062_1__mod.jpg) 
![](img/06_062_1__mod_fl.jpg) 

- 06_062_3，  
![](img/06_062_3__mod.jpg) 
![](img/06_062_3__mod_fl.jpg) 

- 06_062_5，  
![](img/06_062_5__mod.jpg) 
![](img/06_062_5__mod_fl.jpg) 

- 06_064_3，  
![](img/06_064_3__mod.jpg) 
![](img/06_064_3__mod_fl.jpg) 

- 06_065_4，夜店老板的站姿有点娘    
![](img/06_065_4__mod.jpg) 
![](img/06_065_4__mod_fl.jpg) 

- 06_069_3，  
![](img/06_069_3__mod.jpg) 
![](img/06_069_3__mod_fl.jpg) 

- 06_076_1，  
![](img/06_076_1__mod.jpg) 
![](img/06_076_1__mod_fl.jpg) 

- 06_079_1，  
![](img/06_079_1__mod.jpg) 
![](img/06_079_1__mod_fl.jpg) 

- 06_079_4，  
![](img/06_079_4__mod.jpg) 
![](img/06_079_4__mod_fl.jpg) 

- 06_080_0，  
![](img/06_080_0_mod.jpg) 
![](img/06_080_0_mod_fl.jpg) 

- 06_085_0，  
![](img/06_085_0__mod.jpg) 
![](img/06_085_0__mod_fl.jpg) 

- 06_086_0，  
![](img/06_086_0__mod.jpg) 
![](img/06_086_0__mod_fl.jpg) 

- 06_088_2，  
![](img/06_088_2__mod.jpg) 
![](img/06_088_2__mod_fl.jpg) 

- 06_089_0，  
![](img/06_089_0__mod.jpg) 
![](img/06_089_0__mod_fl.jpg) 

- 06_089_4，  
![](img/06_089_4__mod.jpg) 
![](img/06_089_4__mod_fl.jpg) 

- 06_090_2，  
![](img/06_090_2__mod.jpg) 
![](img/06_090_2__mod_fl.jpg) 

- 06_090_3，  
![](img/06_090_3__mod.jpg) 
![](img/06_090_3__mod_fl.jpg) 

- 06_090_5，  
![](img/06_090_5__mod.jpg) 
![](img/06_090_5__mod_fl.jpg) 

- 06_091_0，  
![](img/06_091_0__mod.jpg) 
![](img/06_091_0__mod_fl.jpg) 

- 06_093_2，  
![](img/06_093_2__mod.jpg) 
![](img/06_093_2__mod_fl.jpg) 

- 06_094_1，  
![](img/06_094_1__mod.jpg) 
![](img/06_094_1__mod_fl.jpg) 

- 06_116_5，06_117_4，这个角度和下颌处的画法似乎在猫眼里多见：  
![](img/06_116_5__.jpg) 
![](img/06_117_4__mod.jpg) 

- 06_120_2，  
![](img/06_120_2__mod.jpg) 
![](img/06_120_2__mod_fl.jpg) 

- 06_120_5，  
![](img/06_120_5__mod.jpg) 
![](img/06_120_5__mod_fl.jpg) 

- 06_126_0，  
![](img/06_126_0__mod.jpg) 
![](img/06_126_0__mod_fl.jpg) 

- 06_128_6，  
![](img/06_128_6__mod.jpg) 
![](img/06_128_6__mod_fl.jpg) 

- 06_137_0，   
![](img/06_137_0__mod.jpg) 
![](img/06_137_0__mod_fl.jpg) 

- 06_144，06_163。齐藤茜是一个怎样的女孩子，刻画得很生动了：内八字，跑步时手臂左右摇摆  
![](img/06_144_2__crop0.jpg) 
![](img/06_163_5__.jpg) 
![](img/06_163_6__.jpg)  

- 06_146_3，  
![](img/06_146_3__mod.jpg) 
![](img/06_146_3__mod_fl.jpg) 

- 06_151_1，  
![](img/06_151_1__mod.jpg) 
![](img/06_151_1__mod_fl.jpg) 

- 06_163_1，  
![](img/06_163_1__mod.jpg) 
![](img/06_163_1__mod_fl.jpg) 

- 06_166_2，  
![](img/06_166_2__mod.jpg) 
![](img/06_166_2__mod_fl.jpg) 

- 06_172_1，  
![](img/06_172_1__mod.jpg) 
![](img/06_172_1__mod_fl.jpg) 

- 06_172_3。这电话是谁在递给谁？  
![](./img/06_172_3__.jpg)  
只能是紫递给雅彦。因为递东西时，递出方通常会看着接收方，而接收方通常会看着所要接的那个东西。

- 06_174_1，  
![](img/06_174_1__mod.jpg) 
![](img/06_174_1__mod_fl.jpg) 

- 06_177_0，  
![](img/06_177_0__mod.jpg) 
![](img/06_177_0__mod_fl.jpg) 

- 06_177_3，  
![](img/06_177_3__mod.jpg) 
![](img/06_177_3__mod_fl.jpg) 

- 06_182_2，  
![](img/06_182_2__mod.jpg) 
![](img/06_182_2__mod_fl.jpg) 

- 06_186_4，  
![](img/06_186_4__mod.jpg) 
![](img/06_186_4__mod_fl.jpg) 

- 06_190_0，  
![](img/06_190_0__mod.jpg) 
![](img/06_190_0__mod_fl.jpg) 


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
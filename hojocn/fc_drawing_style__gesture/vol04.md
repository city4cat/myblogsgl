# FC的画风-动作、姿态  

## 目录  
[Readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

按动作、姿态分类：  
[吃](./gesture_eat.md), 
[其他](./gesture_else.md), 
[跳](./gesture_jump.md), 
[跑](./gesture_run.md), 
[坐](./gesture_sit.md), 
[蹲](./gesture_squat.md), 
[站](./gesture_stand.md), 
[走](./gesture_walk.md), 


## Vol04

- 04_005_3,  
![](img/04_005_3__mod.jpg) 
![](img/04_005_3__mod_force-line.jpg)  


- 04_008_0,  
![](img/04_008_0__mod.jpg) 
![](img/04_008_0__mod_force-line.jpg)  


- 04_024_0,  
![](img/04_024_0__mod.jpg) 
![](img/04_024_0__mod_force-line.jpg)  


- 04_025_2,  
![](img/04_025_2__mod.jpg) 
![](img/04_025_2__mod_force-line.jpg)  


- 04_025_5,  
![](img/04_025_5__mod.jpg) 
![](img/04_025_5__mod_force-line.jpg)  


- 04_029_0,  
![](img/04_029_0__mod.jpg) 
![](img/04_029_0__mod_force-line.jpg)  

 
- 04_031_0,  
![](img/04_031_0__mod.jpg) 
![](img/04_031_0__mod_force-line.jpg)  


- 04_031_5,  
![](img/04_031_5__mod.jpg) 
![](img/04_031_5__mod_force-line.jpg)  


- 04_032_5,  
![](img/04_032_5__mod.jpg) 
![](img/04_032_5__mod_force-line.jpg)   


- 04_033_4,  
![](img/04_033_4__mod.jpg) 
![](img/04_033_4__mod_force-line.jpg)   


- 04_034_6, 04_070_4, 空跑步。  
![](img/04_034_6__mod.jpg) 
![](img/04_070_4__mod.jpg) 
![](img/04_076_1__mod.jpg) 
![](img/04_076_5__mod.jpg) 



- 04_035_0,  
![](img/04_035_0__mod.jpg) 
![](img/04_035_0__mod_force-line.jpg)  


- 04_037_4,  
![](img/04_037_4__mod.jpg) 
![](img/04_037_4__mod_force-line.jpg)  


- 04_038_0,  
![](img/04_038_0__mod.jpg) 
![](img/04_038_0__mod_force-line.jpg)  


- 04_040_0,  
![](img/04_040_0__mod.jpg) 
![](img/04_040_0__mod_force-line.jpg)  


- 04_043_3,  
![](img/04_043_3__mod.jpg) 
![](img/04_043_3__mod_force-line.jpg)  


- 04_047_5, 04_049_4, 爷爷跑步。  
![](img/04_047_5__mod.jpg) 
![](img/04_047_5__mod_force-line.jpg)  

![](img/04_049_4__mod.jpg) 
![](img/04_049_4__mod_force-line.jpg)  


- 04_048_0,  
![](img/04_048_0__mod.jpg) 
![](img/04_048_0__mod_force-line.jpg)  


- 04_049_5,  
![](img/04_049_5__mod.jpg) 
![](img/04_049_5__mod_force-line.jpg)  


- 04_050_0,  
![](img/04_050_0__mod.jpg) 
![](img/04_050_0__mod_force-line.jpg)  


- 04_050_2，  
![](img/04_050_2__mod.jpg) 
![](img/04_050_2__mod_force-line.jpg)  


- 04_054_4，  
![](img/04_054_4__mod.jpg) 
![](img/04_054_4__mod_force-line.jpg)  


- 04_057_0，  
![](img/04_057_0__mod.jpg) 
![](img/04_057_0__mod_force-line.jpg)  


- 04_059_2，  
![](img/04_059_2__mod.jpg) 
![](img/04_059_2__mod_force-line.jpg)  


- 04_065_0，  
![](img/04_065_0__mod.jpg) 
![](img/04_065_0__mod_force-line.jpg)  


- 04_065_1，04_065_2，04_065_4，04_066_1，抹化妆品    
![](img/04_065_1__mod.jpg) 
![](img/04_065_2__mod.jpg) 
![](img/04_065_4__mod.jpg) 
![](img/04_066_1__mod.jpg) 

![](img/04_065_1__mod_force-line.jpg) 
![](img/04_065_2__mod_force-line.jpg) 
![](img/04_065_4__mod_force-line.jpg) 
![](img/04_066_1__mod_force-line.jpg) 


- 04_068_3__mod，  
![](img/04_068_3__mod.jpg) 
![](img/04_068_3__mod_force-line.jpg)  


- 04_070_1，  
![](img/04_070_1__mod.jpg) 
![](img/04_070_1__mod_force-line.jpg)  


- 04_071_0，紫跑步。     
![](img/04_071_0__mod.jpg) 
![](img/04_071_0__mod_force-line.jpg)  


- 04_087_1，     
![](img/04_087_1__mod.jpg) 
![](img/04_087_1__mod_force-line.jpg)  


- 04_092_7，     
![](img/04_092_7__mod.jpg) 
![](img/04_092_7__mod_force-line.jpg)  


- 04_105_5__mod，     
![](img/04_105_5__mod.jpg) 
![](img/04_105_5__mod_force-line.jpg)  


- 04_106_0，     
![](img/04_106_0__mod.jpg) 
![](img/04_106_0__mod_force-line.jpg)  


- 04_108_3，     
![](img/04_108_3__mod.jpg) 
![](img/04_108_3__mod_force-line.jpg)  


- 04_117_5，     
![](img/04_117_5__mod.jpg) 
![](img/04_117_5__mod_force-line.jpg)  


- 04_121_3，     
![](img/04_121_3__mod.jpg) 
![](img/04_121_3__mod_force-line.jpg)  


- 04_123_1，     
![](img/04_123_1__mod.jpg) 
![](img/04_123_1__mod_force-line.jpg)  


- 04_123_4，     
![](img/04_123_4__mod.jpg) 
![](img/04_123_4__mod_force-line.jpg)  


- 04_125_0，     
![](img/04_125_0__mod.jpg) 
![](img/04_125_0__mod_force-line.jpg)  


- 04_128_0，     
![](img/04_128_0__mod.jpg) 
![](img/04_128_0__mod_force-line.jpg)  


- 04_128_3，     
![](img/04_128_3__mod.jpg) 
![](img/04_128_3__mod_force-line.jpg)  


- 04_130_3，     
![](img/04_130_3__mod.jpg) 
![](img/04_130_3__mod_force-line.jpg)  


- 04_136_0，     
![](img/04_136_0__mod.jpg) 
![](img/04_136_0__mod_force-line.jpg)  


- 04_136_2，     
![](img/04_136_2__mod.jpg) 
![](img/04_136_2__mod_force-line.jpg)  


- 04_137_1，     
![](img/04_137_1__mod.jpg) 
![](img/04_137_1__mod_force-line.jpg)  


- 04_142_0，手抚摸头发的姿势：       
![](img/04_142_0__mod.jpg) 
![](img/04_142_0__mod_force-line.jpg)  


- 04_145_6，     
![](img/04_145_6__mod.jpg) 
![](img/04_145_6__mod_force-line.jpg)  


- 04_153_0，     
![](img/04_153_0__mod.jpg) 
![](img/04_153_0__mod_force-line.jpg)  


- 04_154_2，叶子跑步：        
![](img/04_154_2__mod.jpg) 
![](img/04_154_2__mod_force-line.jpg)  


- 04_154_5，     
![](img/04_154_5__mod.jpg) 
![](img/04_154_5__mod_force-line.jpg)  


- 04_157_1，     
![](img/04_157_1__mod.jpg) 
![](img/04_157_1__mod_force-line.jpg)  


- 04_161_2，     
![](img/04_161_2__mod.jpg) 
![](img/04_161_2__mod_force-line.jpg)  


- 04_162_1，     
![](img/04_162_1__mod.jpg) 
![](img/04_162_1__mod_force-line.jpg)  


- 04_162_2，     
![](img/04_162_2__mod.jpg) 
![](img/04_162_2__mod_force-line.jpg)  


- 04_163_1，     
![](img/04_163_1__mod.jpg) 
![](img/04_163_1__mod_force-line.jpg)  


- 04_168_0，     
![](img/04_168_0__mod.jpg) 
![](img/04_168_0__mod_force-line.jpg)  


- 04_174_2，     
![](img/04_174_2__mod.jpg) 
![](img/04_174_2__mod_force-line.jpg)  


- 04_187_0__mod，     
![](img/04_187_0__mod.jpg) 
![](img/04_187_0__mod_force-line.jpg)  



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
# 整理FC的一些信息  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96822))  

--------------------------------------------------

## FC时间线

[FC Timeline](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_information/fc_timeline.md)

--------------------------------------------------
## 卷-章节-起始页码-标题
```
---卷----章节-----起始页码------章节英文标题  /  章节中文标题
Vol01	CH001	page003		"家庭"的初体验  	First "family" Life Experience  		  
Vol01	CH002	page043		天体之夜  	A Torrid Night  		  
Vol01	CH003	page073		温暖的饭  	A Hot Meal  		
Vol01	CH004	page099		开学礼的条件  	Entry Ceremony  		  
Vol01	CH005	page125		棒球的回忆  	The Glove Of Memories  		
Vol01	CH006	page153		在远处遥望的初恋  	A First Discrete Love  		
Vol01	CH007	page179		助一臂之力  	Just A Little Service  		
Vol02	CH008	page003		母亲的回忆  	Mother's Gift  		
Vol02	CH009	page031		家庭旅行!！  	Family Trip!  		
Vol02	CH010	page059		不请自来的助手  	The Stubborn Assistant  		  
Vol02	CH011	page087		仰慕的人  	The One I Admire  		  
Vol02	CH012	page115		紫苑的不道德交际!？  	Shion Courtesan!?  		
Vol02	CH013	page141		银幕上的紫苑  	The Girl On The Screen  		
Vol02	CH014	page169		充满回忆的同学会  	A Day To Remember Another Life  		
Vol03	CH015	page003		女星的诞生!？  	A Star Is Born!?  		
Vol03	CH016	page031		扮女人的测试  	Transvestite Apprentice  		
Vol03	CH017	page059		追击  	Repetition  		
Vol03	CH018	page087		意想不到的助手  	An Unexpected Collaborator  		
Vol03	CH019	page113		辰已，纯情的爱…!?  	Tatsumi's Unwavering Love  		  
Vol03	CH020	page141		阔别了18年的妹妹  	A Little Sister Not Seen For Over 18 Years  
Vol03	CH021	page169		父与"女"!  	The Father And The "daughter"!  		
Vol04	CH022	page003		顺子的过文定日子  	An Engagement Gift For Yoriko  		  
Vol04	CH023	page029		冤枉路  	Turnabout  		  
Vol04	CH024	page057		爸爸倒下了？  	Father Has Fainted?  		  
Vol04	CH025	page085		谁是鬼魂？  	Where's The Ghost?  		  
Vol04	CH026	page113		两位观众  	Two Spectators  		  
Vol04	CH027	page141		把雅彦还给我！  	Rescue Masahiko!!  		  
Vol04	CH028	page169		19岁的生辰  	His Nineteenth Birthday  		  
Vol05	CH029	page003		扮女人比赛  	Crossdressing Competition  		  
Vol05	CH030	page031		亡命驾驶  	Maniac At The Wheel  		  
Vol05	CH031	page059		你喜欢妇科吗？  	Gynaecology? You're Kidding Me!  		  
Vol05	CH032	page086		各人的平安夜  	A Xmas Like The Others  		  
Vol05	CH033	page113		紫苑的初恋  	Shion's First Love  		  
Vol05	CH034	page141		叶子的求爱!！  	Yoko Makes Her Move!!  		  
Vol05	CH035	page169		拜祭母亲  	At Mother's Graveside  		  
Vol06	CH036	page003		雅彦的独立作战  	Masahiko's Plan For Independence  		  
Vol06	CH037	page031		危险的兼职  	A Dangerous Job  		  
Vol06	CH038	page057		纯情葵  	The Feelings Of Aoï  		  
Vol06	CH039	page085		Family-若苗  	My Family The Wakanaes  		  
Vol06	CH040	page111		憧憬的婚纱  	Wanted  		  
Vol06	CH041	page137		同性恋师妹-茜  	Akane's Idol  		  
Vol06	CH042	page165		秘密赴东京  	The Secret Trip  		  
Vol07	CH043	page003		愿当养子  	Become My Son!  		  
Vol07	CH044	page031		婚礼的秘密对策  	A Super Secret Plan  		  
Vol07	CH045	page061		钟乳洞内的危险幽香  	The Smell Of Danger From Limestone Cave  
Vol07	CH046	page089		恶梦般的露天温泉  	The Nightmare Otherdoor Bath  		  
Vol07	CH047	page115		两个雅彦  	The Two Masahikos  		  
Vol07	CH048	page143		"雅彦"的真正身份  	The Identity Of Masahiko  		  
Vol07	CH049	page171		两父女  	Father And Child  		  
Vol08	CH050	page003		若苗家的新住客  	A New Member Of The Wakanae Family  		  
Vol08	CH051	page031		薰的诡计  	Kaoru's Stratagem  		  
Vol08	CH052	page059		迈出男人的第一步  	The First Step As A Man  		  
Vol08	CH053	page087		伪装家庭  	Camouflaging As A Family  		  
Vol08	CH054	page115		工作中的爸爸  	Papa's Work  		  
Vol08	CH055	page143		叶子的献身大作战计划  	Yoko's "first Time" Plan  		  
Vol08	CH056	page171		叶子大变身  	Yoko's Transformation  		  
Vol09	CH057	page003		二人世界的生日  	Their Birthday  		  
Vol09	CH058	page031		网上情人  	Mail Lover  		  
Vol09	CH059	page059		战胜劲敌  	Victory Pitch  		  
Vol09	CH060	page085		辰已的心愿  	Tatsumi's Wish  		  
Vol09	CH061	page111		辰已的表白  	Tatsumi's Confession  		  
Vol09	CH062	page139		母亲的印象  	Mother's Image  		  
Vol09	CH063	page167		早纪的诱惑  	Saki's Temptation  		  
Vol10	CH064	page003		薰的表白  	Kaoru's Confession  		  
Vol10	CH065	page031		亲情的价值  	The Price Of Kinship  		  
Vol10	CH066	page059		紫苑的第一志愿  	Shion's First Choice  		  
Vol10	CH067	page087		二人的取材之旅  	Their Material Gathering Journey  		  
Vol10	CH068	page115		雅美的身价  	Masami's Values  		  
Vol10	CH069	page143		恐怖的跟踪者  	The Frightening Stalker  		  
Vol10	CH070	page171		紫苑的诞生  	Shion's Birth  		  
Vol11	CH071	page003		约会的替身  	Substitute Date  		  
Vol11	CH072	page031		各人的圣诞节  	Separate Christmas  		  
Vol11	CH073	page061		阴错阳差的平安夜  	An Eve Of Fated Encounters  		  
Vol11	CH074	page089		物以类聚  	It's All The Same  		  
Vol11	CH075	page117		紫苑的入学试  	Shion's Admission Exams  		  
Vol11	CH076	page145		紫苑的毕业旅行  	Shion's Graduation Trip  		  
Vol11	CH077	page173		紫苑和雅彦共渡一夜  	Shion And Masahiko's Night Together  		  
Vol12	CH078	page003		出外靠旅伴…！  	Dragged Along On The Trip!  		  
Vol12	CH079	page031		雅彦孤军作战！  	Masahiko Fighting Alone!!  		  
Vol12	CH080	page059		充满神秘的成人节！  	The Mysterious Coming-Of-Age Day  		  
Vol12	CH081	page087		是男是女？  	Man Or Woman!?  		  
Vol12	CH082	page115		加入学会攻防战  	Club Recruiting Battle  		  
Vol12	CH083	page143		脚如嘴巴般道出真相  	The Foot Speaks As Well As The Mouth...  
Vol12	CH084	page171		薰的前途  	Kaoru's Path  		  
Vol13	CH085	page003		失乐园  	Lost Paradise  		  
Vol13	CH086	page031		父母心  	Parental Feelings  		  
Vol13	CH087	page059		孩子要独立  	Cutting The Umbilical Cord  		  
Vol13	CH088	page087		似近亦远的东京  	Tokyo, So Near And Yet So Far  		  
Vol13	CH089	page115		母与女  	Mother And Daughter  		  
Vol13	CH090	page141		冲突  	Clash  		  
Vol13	CH091	page169		只做一天老板娘  	Okami For A Day  		  
Vol14	CH092	page003		恐怖的迎新联欢会  	Scary Meeting  		  
Vol14	CH093	page031		再会  	Reunion  		  
Vol14	CH094	page057		浅葱到访  	Asagi's Visit  		  
Vol14	CH095	page083		浅葱突击行动  	Asagi's Plan  		  
Vol14	CH096	page109		哪方面较好呢？  	It Doesn't Matter?!  		  
Vol14	CH097	page137		一天的情人  	Couple For A Night  		  
Vol14	CH098	page163		雅彦的剧本  	Masahiko's Scenario  		  
Vol14	CH099	page189		改变剧情  	Change Of Course  		  
Vol14	CH100	page215		浅葱的反击  	Asagi's Retaliation  		  
Vol14	CH101	page243		奥多摩的分手  	Split At Okutama  		  
Vol14	CH102	page269		家庭(最终话)  	Last Day - End  		  

```
--------------------------------------------------
## 角色名字
```
中文名       英文名                  注释

若苗 紫苑    Wakanae Shion           #  
    塩谷             Shionoya       # "塩"通"盐"。男装时，紫苑在叶子面前自称塩谷（11卷011页）  
柳叶 雅彦    Yanagiba Masahiko       #  
柳叶 雅美    Yanagiba Masami         # 雅彦女装  
若苗 空      Wakanae Sora	          # 婚前：菊地Kikuchi 春香Haruka(ze) (vol02p91) 
若苗 紫      Wakanae Yukari         # 婚前：若苗Wakanae 龙彦Tatsuhiko （02卷171页）  
		
    森                Mori         # 督促空工作的女编辑  
中村 浩美    Nakamura Hiromi        # 若苗空的助手。本名: 中村Nakamura 光浩Mitsuhiro  
山崎 真琴    Yamazaki Makoto        # 若苗空的助手  
横田 进      Yokota Susumu         # 若苗空的助手  
新潟 和子    Niigata Kazuko        # 若苗空的助手。本名: 新潟Niigata 和人Kazuto  
新潟 一马    Niigata Kazuma        # 新潟 和子的孩子：一马（08卷54话）  

菊地 顺子    Kikuchi Yoriko        # 若苗空的妹妹  
菊地 ?      Kikuchi  ?            # 若苗空的父亲。更多信息可参见[1]。  
菊地 节子    Kikuchi  ?            # 若苗空的母亲。若苗紫曾经寄信给奶奶，由信的封面(04_073_2)推测。更多信息可参见[1]。    
奥村 宪司    ?       Kenji         # 若苗空的高中同学，后与顺子结婚。姓氏"奥村"是由(04_023_3)的奥村酒家推测. (06_112_2)证实他的姓氏是奥村。更多信息可参见[1]。  
奥村 章子    ?       Shoko         # 宪司的女儿（04卷07页）更多信息可参见[1]。  
		
浅冈 叶子    Asaoka Yoko           # 雅彦的女友
		
真朱 薰      Masoba Kaoru           #   
    早纪            Saki           # 熏的母亲  
真朱 辰四    Masoba Tatsumi         # 熏的父亲  
     葵            Aoi             # 中文译为：美葵。辰巳夜店里一个喜欢辰巳的(变性)人（06卷38话）  
		
卓也 江岛    Takuya Ejima          # 雅彦的同学，影研社成员（05卷025页）  
藤崎 茜      Fujisaki Akane        # 影研社的一个女生，她喜欢雅美。（06卷41话）  
耕平 仁科    Kohei Nishina         # 影研社的一个男生，雅彦的同班同学，他喜欢雅彦（10卷148页，14卷004页）  
    千夏          Chinatsu        # 千夏Chinatsu(个子高，发色浅)。和紫苑一起加入映研社（12卷141页，12卷158页）  
    麻衣          Mai             # 麻衣Mai(个子矮，发色深)。和紫苑一起加入映研社（12卷141页，12卷158页）  
浅葱 蓝      Asagi Ai             # 紫苑的同学，她从小学到大学一直喜欢紫苑（14卷93话）  
		
齐藤 玲子    Saito Reiko          # 紫苑是她的初恋(第1卷 第6话)  
松下 敏史    Matsushita Toshifumi # 棒球运动员, 他喜欢紫苑（9卷59话）  
		
     理沙           Risa         # 理沙Risa(个子高，发色浅)，紫苑的高中同学，和紫苑一起高中毕业旅行（11卷76话）  
     美菜           Mika         # 美菜Mika(个子矮，发色深)，紫苑的高中同学，和紫苑一起高中毕业旅行（11卷76话）  
齐藤 文哉    Saitou Fumiya        # 美菜Mika 的男友, 穿黑上衣、灰裤子（12卷010页）  
     一树          Kazuki        # 文哉Fumiya 的同学, 戴一幅眼镜、短裤（12卷012页）  
     阿透          Tooru         # 文哉Fumiya 的同学，穿浅色上衣、黑裤子（12卷012页）  
     哲            Te           # 美菜Mika 的另一个男友（12卷55页）  
		
古屋 公弘    Furuya Kimihiro      # 和浩美网恋的人（09卷58话）  
古屋 朋美    Furuya Tomomi        # 古屋 公弘的妹妹（09卷58话）  
		
八不 京子    Hanzu Kyoko          # 紫的中学插花社的成员。婚前：Isaka井阪 Kyoko京子（02卷14话）  
    绫子          Aya            # 紫的中学插花社的成员（02卷14话178页）  ，矮、胖
    典子          Tenko          # 紫的中学插花社的成员（02卷14话178页）  ，高、瘦、戴眼镜
		
注：以上英文名源自FC英文版  		
			
人物日文名的出处：  
柳叶（01_004_6）  
雅彦（01_006_1）  
若苗紫（01_007_4）  
紫苑（01_020_4）
空（01_021_6）
新潟 和子(01_066_0)
中村 浩美(01_066_1)
山崎 真琴(01_066_1)
浅冈叶子（02_064_5, 03_008_1）
春风空（02_091_3）
江岛（02_121_0）卓也(05_025_0, 10_066_5)
若苗龙彦（02_170_1）
横田（03_009_1） 进（03_010_1）
菊地春香（03_142_2）
菊地顺子（03_146_3）
奥村(06_112_2) 宪司（03_179_3）
真朱熏(07_152_6)
早纪(09_137_1)
仁科(10_148_4)耕平(14_004_4)
理沙(11_146_4)
美菜(11_146_4)
```

### 雅彦、紫苑的喜欢/被喜欢人物关系  

("？？？"和"？"均表示FC里未提及的内容)

>雅彦（童年）:
>>- 喜欢的女生：紫苑，
>>- 喜欢的男生：？
>>- 被男生喜欢：？
>>- 被女生喜欢：？？？

>雅彦（青少年）：
>>- 喜欢的女生：紫苑，
>>- 喜欢的男生：？
>>- 被男生喜欢：仁科，
>>- 被女生喜欢：？？？（注：雅彦变得成熟果断之后，可能会被女生喜欢）


>雅美（童年?）：
>>- 喜欢的女生：？
>>- 喜欢的男生：？
>>- 被男生喜欢：？
>>- 被女生喜欢：？

>雅美（青少年）：
>>- 喜欢的女生：？
>>- 喜欢的男生：？
>>- 被男生喜欢：辰巳，熏?(注：熏喜欢雅美可能仅仅是为了报复辰巳)，
>>- 被女生喜欢：藤崎Fujisaki茜Akane，


>紫苑（童年）：
>>- 喜欢的女生：？
>>- 喜欢的男生：？？？
>>- 被男生喜欢：雅彦，Matsushita松下Toshifumi敏史，
>>- 被女生喜欢：？

>紫苑（青少年）：
>>- 喜欢的女生：？
>>- 喜欢的男生：光浩/浩美，雅彦，
>>- 被男生喜欢：雅彦，江岛，Matsushita松下Toshifumi敏史，路人甲（卷5，32话，88页），？？？
>>- 被女生喜欢：浅葱，藤崎Fujisaki茜Akane，


>盐谷/紫苑男装（童年）：
>>- 喜欢的女生：浅葱
>>- 喜欢的男生：？
>>- 被男生喜欢：？
>>- 被女生喜欢：齐藤Saito玲子Reiko，浅葱，

>盐谷/紫苑男装（青少年）：
>>- 喜欢的女生：？
>>- 喜欢的男生：？
>>- 被男生喜欢：？
>>- 被女生喜欢：浅葱，

**参考资料**:  
[1]. [F.COMPO応援隊 - 登場人物紹介 - 若苗家](../zz_www.biglobe.ne.jp_julianus/fcompo/charawaka.md)  


--------------------------------------------------

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处




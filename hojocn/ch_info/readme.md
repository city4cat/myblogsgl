
# CH的一些信息 ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96942))    

## 1. 卷-话-标题：  
[CITY HUNTER 香港玉皇朝版（共35卷/191话）](./ch_subtitle_35_hk.md)  
[CITY HUNTER 台湾东立版（共35卷/191话）](./ch_subtitle_35_tw.md)  
[CITY HUNTER 《Complete Edition》香港玉皇朝版(共32卷/336话) ](./chce_subtitle_32_hk.md)  
注： 在HTKJ中，每话标题被称为"Subtitle"，意思是"子标题"，而不是"字幕"。  


## 2. [关于CH中部分角色的名字](./ch_name.md)  
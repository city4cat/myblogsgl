source:  
http://bdzoom.com/7393/mangas/zoom-manga-f-compo-t1-par-tsukasa-hojo/

# T1 par Tsukasa Hojo  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96829))  
FC的两个法语版(Panini版、Tonkam版)的比较

7 novembre 2010,  
Par Gwenaël Jacquet

Panini réédite enfin la série  » Family Compo  » ou plus simplement  » F.Compo  » comme indiqué en gros sur la couverture des albums. Précédemment édité par Tonkam, elle fut retirée du catalogue de cet éditeur en 2004, comme tous le reste des ?uvres de Tsukasa Hojo suite à la politique de Shueisha, l’éditeur Japonais, de ne plus avoir qu’un seul interlocuteur dans chaque pays ou les ?uvres du mangaka sont publiées ; ceci afin de garder un niveau de qualité constant. Pour la France et l’Italie, ce fut donc Panini.  
Panini最终将重新发行系列“Family Compo”(或更简单地发行“F.Compo”），如专辑封面所示。 它是由Tonkam先前编辑的，它于2004年从出版商的目录中撤回，就像日本出版商集英社（Shueisha）的政策（在每个国家/地区仅建立一个联系人）一样，北条司的所有其他作品一样。 或发表漫画的作品； 为了保持恒定的质量水平。 因此对于法国和意大利来说，就是Panini。

La première version française de  » Family Compo  » est sortie en 1999 chez Tonkam dans une version respectant le format original japonais, 12,5 x 18 cm. La nouvelle édition de Panini comics reprend le format de luxe déjà en vigueur sur les séries  » City Hunter  » et  » Cat’s Eye « , soit 14,5 x 21 cm. Mais les différences ne s’arrêtent pas à une question de taille, nous allons voir cela ensemble.  
Tonkam于1999年发行了第一个法语版本的《非常家庭》，该版本尊重原始的日语格式12.5 x 18 cm。 Panini漫画的新版本使用了在“ City Hunter”和“Cat's Eye”系列中已经生效的豪华格式，即14.5 x 21 cm。 但是差异并不仅限于大小问题，我们将一起看到。

Les deux versions placées de manière homothétique l’une à côté de l’autre. © Tsukasa Hojo – Panini 2010 – Tonkam 1999  
两个版本相互相对地放置。©Tsukasa Hojo-Panini 2010-Tonkam 1999

En premier, j’aimerais vous replacer ce manga dans le contexte de l’époque. Tsukasa Hojo est sur le point de terminer  » City Hunter « , la série qui l’a consacré auprès du public. En 1995, il débute une nouvelle série «  Rash  » qui ne fut pas accueillie convenablement par les lecteurs. Comme nous sommes au Japon, pays où les contraintes éditoriales sont très rigoureuses, l’aventure fut stoppée au bout de seulement deux numéros.  
首先，我想把这个漫画放在时间的背景下。 北条司即将完成“城市猎人”，这是他专为大众制作的系列电影。1995年，他开始了一个新系列“ Rash”，该系列并没有受到读者的欢迎。就像我们在日本一样，这个国家的编辑限制非常严格，因此冒险活动仅在发布了两期后就停止了。


Hojo a néanmoins une grande idée, comme il l’explique en introduction de  » Family compo  » :  » je pensais qu’une histoire traitant d’un couple, dont le mari et la femme ont inversé leur rôle pourrait être amusante « . Or, la rédaction de Jump, le magazine mythique pour jeunes garçons qui édite déjà les autres mangas de cet auteur, pense que le sujet est trop osé pour son lectorat. Têtu, Hojo demandera à être publié dans un autre magazine de Shueisha, Allman. Destiné à un public plus adulte le sujet semblait porteur. La série fut en effet un succès durant quatre ans avec ses quatorze volumes.  
但是，北条有一个好主意，正如他在“F.Compo”的引言中解释的那样：“我认为关于一对夫妻的故事，夫妻之间的角色转换可能很有趣”。 但是，《少年Jump》的编辑人员已经出版了该作者的另一本漫画，该杂志是有关少年男孩的漫画杂志，他们认为该主题对读者而言实在太胆了。 北条固执，将要求在另一家集英社杂志《Allman》上发表。 该杂志面向更多的成年人，似乎很有前途。 该连载的十四卷确实是四年来的成功。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-EP-blue.jpg)  
© Tsukasa Hojo – Panini 2010

L’histoire en elle même est assez banale et utilise les ficelles classiques de l’auteur pour créer des situations burlesques basées sur le quiproquo. Masahiko Yanagiba se retrouve orphelin à seize ans. Son père vient de décéder dans un accident de voiture et il n’a quasiment jamais connu sa mère. Sans ressources, il voit sonner à sa porte la femme de son oncle qui lui propose de le prendre en charge afin qu’il puisse continuer ses études. Le lecteur se rendra vite compte que sa tante est en faite son oncle et que son mari est en faite sa femme. La famille Wakanae est un couple de travestis.  
故事本身相当普通，并根据作者的误解使用作者的经典弦乐创作出滑稽的情节。 柳叶雅彦在16岁时发现自己是一个孤儿。 他的父亲死于一场车祸，他几乎不记得他的母亲。在没有生活费的情况下，他看到舅母来访，后者愿意收养他，以便他继续学业。 读者很快就会意识到，他的舅母实际上是他的舅舅，而她的丈夫实际上是他的妻子。 若苗家族是异装夫妇。

Le plus drôle dans l’histoire est qu’ils ont un enfant : Shion. Et si pour le moment, c’est une fille, il lui est arrivé de se travestir en garçon durant sa scolarité. Bien sûr, Masahiko est un peu perturbé par ces révélations, mais il finira rapidement par les apprécier en tant que famille.  
故事中最有趣的是他们有一个孩子：紫苑。 虽然目前是女孩，但她有时会在上学时打扮成男孩。当然，雅彦会对这些感到有些不安，但作为一个家庭而带给他的感动，他很快就会喜欢上他们。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/fin-blue.jpg)  
© Tsukasa Hojo – Panini 2010

Néanmoins, Masahiko désire hardiment percer le mystère de Shion : est-ce un garçon ou une fille ? Cette situation restera pour toujours ambigüe et sera le leitmotiv de l’histoire. En effet, la série, même si elle se clôture sur un événement heureux, ne répond pas à cette question centrale. De quel sexe est Shion?? C’est une fin ouverte, mais qui laisse le lecteur dans l’expectative.  
Tsukasa Hojo a toujours eu ce problème avec ses séries longues, il ne sait pas comme les finir et préfère laisser l’histoire en suspend comme s’il espérait un jour les continuer toutes. Dans la préface du dernier volume chez Tonkam il raconte même avoir reçu de nombreuses lettres de lecteurs déçus l’encourageant à continuer la série. Or, il répond clairement que  » l’histoire serait toujours incomplète, car il n’y a jamais de point final à l’histoire d’une famille « . Une manière détournée pour éluder les questions laissées en suspend. Et il conclut : «  ce qu’ils deviendront après le dernier chapitre, je vous laisse à vous, chers lecteurs, le soin de l’imaginer ! « .  
然而，雅彦大胆地希望弄清楚紫苑的奥秘：是男孩还是女孩？ 这种情况将永远模糊不清，并将成为历史的主题。 确实，该系列即使以开心的事件结尾，也无法回答这个核心问题。 紫苑是什么性别？ 这是一个开放的结局，但是却使读者感到怀疑。
北条司长期以来一直遇到这个问题，他不知道如何结束他们，而是宁愿把故事搁置一旁，好像他希望一整天都继续下去。 在Tonkam上最后一卷的序言中，他甚至提到收到了许多失望的读者的来信，鼓励他继续该系列作品。 但是，他明确地回答说：“故事是永远讲不完的，因为家庭故事永远不会有终点。” 以一种回旋方式来回避悬而未决的问题。 他总结说：“最后一章之后，他们将成为什么样子，只能留给亲爱的读者们想像一下了！ ”。

Comme on dit  » Tout ça pour ça ! « . On en attendait plus, on aurait en effet aimé en savoir encore plus sur les aventures de Masahiko et Shion. On aurait voulu les suivre jusqu’à un point clef de leur vie et non s’arrêter abruptement au meilleur moment. De toutes les séries longues qu’a réalisées Tsukasa Hojo, «  Family Compo  » est la plus courte (1).  
正如他们所说：“一切都是为了那样！ ”。 我们期望更多，我们的确希望知道更多关于雅彦和紫苑的故事。 我们希望跟随他们到达他们生活中的重要时刻，而不是在最佳时间突然停下来。 北条司制作的所有长篇小说中，“ Family Compo”是最短的（1）。

Voyons un peu maintenant clairement ce qui différencie les deux éditions françaises de  » Family Compo « .  
现在让我们清楚地看到两个法语版的"Family Compo"的不同之处。

En premier, j’en ai déjà parlé, la taille des livres. L’édition Panin, qui est plus grande, comporte également plus de pages avec un chapitre supplémentaire qui correspond au début du second volume chez Tonkam. Ce qui est dommage, c’est que ce chapitre était à l’origine en couleur chez Tonkam et se retrouve en noir et blanc chez Panini. De plus, on remarque que le papier utilisé par Tonkam est de meilleure qualité : vraiment blanc et non jaune, et avec un touché plus agréable malgré une épaisseur identique. Mais le papier ne fait pas tout, car l’impression de Panini est bien plus fine et bouche moins les trames tout en respectant la finesse du trait de l’auteur.  
首先，我已经说过了，书的大小。Panin版的篇幅较大，页数也较多，多出了一章，对应的是第二卷在Tonkam版的开头。遗憾的是，这一章原本是Tonkam版的彩色版，现在Panin版的是黑白版。此外，我们注意到Tonkam使用的纸张质量更好：真正的白色而不是黄色，尽管厚度相同，但手感更舒适。但纸张并不是万能的，因为Panin的印刷品在尊重作者线条的精细度的同时，也更加精细，减少了画面的拥挤。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-APT.jpg)  
© Tsukasa Hojo – Panini 2010 – Tonkam 1999

Dés la première planche couleur, on peut facilement différencier les deux éditions. Celle de Panini à gauche et celle de Tonkam à droite.
La première édition chez Tonkam utilisait des lettrages de couleur et traduisait toutes les onomatopées à la différence du lettrage noir et des caractères japonais présents chez Panini.  
从第一个彩色分格，我们可以很容易地区分这两个版本。 Panin在左边Tonkam在右边。Tonkam的第一版使用彩色文字并翻译所有拟声词，不像Panini的黑色文字和日语字符。

La version Panini ayant une édition plus grande, elle a gardé une taille de police sensiblement identique à la version Tonkam. De ce fait, l’écriture flotte plus dans les bulles et cela allège les pages.  
Panin版本较大，保持了与Tonkam版相同的字体大小。因此，文字更多的漂浮在气泡中，这使得页面更明亮。 

On remarquera aussi que la traduction est plus rigoureuse chez Panini : plus formelle, elle semble plus proche de la version japonaise. Néanmoins, la version Tonkam, mise à côté, reste très agréable à lire et les textes coulent un peu mieux, car l’ensemble paraît plus vivant, moins coincé et plus spontané.  
还可以注意到，Panin的翻译更严谨：更正式，似乎更接近日文版。尽管如此，Tonkam版读起来依然非常愉快，文字也更流畅一些，因为整体看起来更生动，不那么拗口，更随性。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-BP.jpg)  
© Tsukasa Hojo – Panini 2010

Cette seconde planche de la version Panini permet de voir la finesse d’impression et une plus grande richesse de couleur que la version de Tonkam ci-dessous. Néanmoins, du fait de l’utilisation d’un papier jaunâtre, cela change la tonalité des couleurs de l’image.  
这个Panini版的第二版，可以看到印刷的精细度和色彩的丰富度比下面的Tonkam版更高。然而，由于使用了淡黄色的纸张，它改变了图像的色调。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-BT.jpg)  
© Tsukasa Hojo – Tonkam 1999

Chez Tonkam, les couleurs sont plus fades et perdent de leur intensité du fait de l’impression moins soutenue.  
在Tonkam版本里，由于印刷的强度小，导致色彩更暗淡。

Après les planches couleur, passons maintenant aux pages en noir et blanc.  
说完彩色页，我们再来看看黑白页。

Cet extrait est tiré de la première partie lorsque Masahiko découvre la vérité sur son oncle.  
本节选自第一部分雅彦发现舅父的真相时。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-CP-blue-2.jpg)  
© Tsukasa Hojo – Panini 2010

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-CT-blue.jpg)  
© Tsukasa Hojo – Tonkam 1999

Grâce à ces deux scans, on remarque clairement l’impression bouchée sur la version Tonkam. Tout est plus sombre, les dégradés sont quasiment inexistants et les détails disparaissent sous la charge d’encre.  
得益于这两张扫描图，你可以清楚地发现Tonkam版本的色块。一切都比较暗，渐变几乎不存在，在墨汁的冲刷下细节消失了。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-D2PT-blue-2.jpg)  
© Tsukasa Hojo – Panini 2010 – Tonkam 1999

Dans cette scène ou Masahiko découvre les attribues masculins de sa tante, on remarque nettement sur la version Tonkam à droite, les pertes au niveau du travail de trame ainsi que les traits bouchés, notamment l’ombre sous le cou qui perd en détail ou les reflets dans les cheveux qui deviennent des pâtés blancs informes.  
在雅彦发现舅母的男性特征的这一幕中，我们可以清楚地看到右边的Tonkam版本中，网点线条的丢失和墨块的特征，特别是脖子下的阴影失去了细节，或者头发上的反光变成了无形的白色斑点。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-FPT-blue.jpg)  
© Tsukasa Hojo – Panini 2010 – Tonkam 1999

Même dans les pages couleurs lors de la publication en revue et imprimé en noir dans l’album on remarque une nette différence dans la densité de la trame et son rendu final. Ces deux scans reprennent la version Panini en haut et la version Tonkam en bas sans retouche et en taille réelle.  
即使在彩色页面中(杂志上印刷出版并在单行本中以黑色印刷)，网点的密度及其最终渲染也存在明显差异。 这两张图上面的是Panini版本和下面的是Tonkam版本，没有进行润饰，而是真实尺寸。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-G1P-blue.jpg)   
© Tsukasa Hojo – Panini 2010

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-GPT-blue.jpg)  
© Tsukasa Hojo – Panini 2010 – Tonkam 1999

Cette scène correspond au moment ou Shion joue un tour à Masahiko en lui faisant croire qu’elle ve se dévoilée nue devant lui et lui révéler sa vraie nature. Avec cet exemple, on voit que ce n’est pas le fruit du hasard. La trame a disparu de moitié sur les habits de Shion et les ombres sont bouchées, notamment sous le nez, le cou, les cheveux et les plis de son bustier.  
此场景对应于紫苑欺骗雅彦那一幕，让雅彦相信她在他面前裸体以展示自己与生俱来的性别。通过这个例子，我们看到这(注：上述印刷问题)不是巧合。紫苑衣服上的网点消失了一半、阴影被遮住了，尤其是在她的鼻子、脖子、头发和紧身胸衣的褶皱下。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-D3PT-blue.jpg)  
© Tsukasa Hojo – Panini 2010 – Tonkam 1999

Dans la version de droite de Tonkam, le parti pris a été de traduire les onomatopées alors que Panini a plutôt cherché à conserver le dessin original de l’auteur en traduisant à côté en petit les divers bruits émaillant la série.  
在右侧的Tonkam版本中，偏向于翻译拟声词，而Panini版则试图通过翻译系列中的各种杂音来保持作者的原始画面。

Et toujours cette perte de détail dans la trame, notamment dans le dégradé sur la maison en haut en gauche.  
网点的细节仍然失真，尤其是左上角房屋中的渐变。


Comme quoi, même en noir et blanc une bande dessinée demande beaucoup de travail pour respecter le dessin d’un auteur et le rendre de la meilleure manière qui soit. C’est une alchimie complexe entre qualité de reproduction, couleur du papier et taux d’encrage. On comprend mieux maintenant ce qui a poussé Shueisha à vouloir uniformiser par le haut la publication des mangas de Tsukasa Hojo. Cet auteur ayant, en plus, un trait réaliste très fouillé et assez fin. Donc demandant une rigueur dans la reproduction qui manquait clairement a certains éditeurs. On pensera évidement à la première édition de  » City Hunter  » chez J’ai lu qui fut un massacre, comme tous les mangas qui sont sortis de leurs presses.  
即使是黑白的漫画，也要尊重作者的画作、以最好的方式呈现出来，这就需要下很大的功夫。 它是印刷质量、纸张颜色和着墨率之间的复杂炼金术。现在我们更明白是什么原因促使集英社想要从上面规范出版北条司的漫画。另外，作者还具有非常详细且非常精细的现实思路。因此要求印刷的严谨性，这对一些出版商来说显然是缺失的。很明显，人们会想到《城市猎人》在J'ai lu的第一版，这是一场屠杀，就像他们出版社出品的所有漫画一样。

Comme je vous l’indiquais en début d’article, le dernier chapitre de l’édition Deluxe correspond au premier du second volume publié chez Tonkam. Cette édition de 1999 avait des pages couleur qui ne sont pas reprises dans celle de Panini en 2010. Néanmoins, la qualité de reproduction de Panni rend honneur au travail de Tsukasa Hojo. Du coup, vaut-il mieux avoir une première édition au trait bouché, mais comportant des pages d’introduction en couleur, ou une version noir et blanc de bonne qualité et de format plus grand ? À vous de voir en fonction de ce que vous trouverez, sachant qu’aujourd’hui il est quasiment impossible de se procurer l’édition de Tonkam.  
正如我在文章开头告诉您的那样，Panini豪华版的最后一章对应Tonkam发行的第二卷的第一章。 1999年版的彩页在Panini 2010年版中没有复现，但是Panni的印刷质量确实归功于北条司的作品。 因此，最好是使用封闭线的第一版，但要有介绍性的彩色页面，或者使用质量较高的黑白版本或较大格式的黑白版本？ 要哪个版本是你的选择，要知道今天几乎是不可能找到Tonkam版的。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-V2.jpg)  
![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-V2A.jpg)  
© Tsukasa Hojo – Tonkam 1999

La page d’introduction du second album original qui, du coup, saute avec cette réédition.  
第二卷原版单行本的介绍页，结果，随着这次重新发行而引人注目。

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-V2BPT.jpg)  
© Tsukasa Hojo – Panini 2010 – Tonkam 1999

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-V2CP.jpg)  
© Tsukasa Hojo – Panini 2010

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-V2CT.jpg)  
© Tsukasa Hojo – Tonkam 1999

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-V2DP.jpg)  
© Tsukasa Hojo – Panini 2010

![](http://bdzoom.com/v_2/wp-content/uploads/V1/IMG/jpg/F-Compo-V2DT.jpg)  
© Tsukasa Hojo – Tonkam 1999

Remplie d’humour et extrêmement divertissante, la vie de la famille Wakanae est beaucoup plus terre-à-terre que celle de Ryo Saeba dans  » City Hunter « . Mais comme elle comporte son lot d’imprévus, cette série est l’une des plus réalistes de Tsukasa Hojo. Une histoire sur la tolérance, le respect de soi ainsi que des autres, l’acceptation de son corps et de ses singularisés sans porter de jugement sur la vie d’autrui. «  Family Compo  » reste un classique du manga à découvrir ou redécouvrir avec cette très bonne réédition.  
与《城市猎人》中的冴羽獠相比，若苗家的生活幽默而极富娱乐性、脚踏实地。 但是，出乎意料的是，这个系列是北条司最现实的连载之一。 一个关于宽容、尊重自己和他人的故事，接受自己的身体和身体的奇怪之处、不评判他人生活。 借助高质量的重版，《非常家庭》仍是值得发现或重新发现的漫画经典。

Gwenaël JACQUET

«  F. Compo  » T1 par Tsukasa Hojo
Éditions Panini (9,95€).

(1)  » Cat’s Eye  » 18 volumes –  » City Hunter  » 35 volumes –  » Angel Heart  » 33 volumes, la série étant toujours en cours de publication.


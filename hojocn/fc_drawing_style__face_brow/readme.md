
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  



# FC的画风-面部-眉毛  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96885))  


- **如何描述眉毛**  
先了解一下目前人们是如何表述眉毛的，之后再使用这些方法、术语来描述FC里角色的眉毛。  
    - 文章[1]里把眉毛放在眼部研究:    
        ![](img/BiometricStudyOfEyelidShape...Fig2.jpg)  
        (Landmarks or fiducials for our photogrammetric analysis([1]里的Fig. 2))  
        
        Rp, right pupil;  
        Lp, left pupil;  
        ML, most medial point of limbus;  
        LL, most lateral point of limbus;  
        Rzy, right zygion;  
        Lzy, left zygion;  
        en, endocanthion;  
        ex, exocanthion;  
        Ren, right endocanthion;  
        Len, left endocanthion;  
        pi, palpebrae inferius;  
        ps, palpebrae superius;  
        Uepi, midpoint between en and ps;  
        Lepi, midpoint between en and pi;  
        Ucan, midpoint between ps and ex;  
        Lcan, midpoint between pi and ex;  
        es, eyelid sulcus;  
        br1, lowest meeting point of eyebrow to vertical line from en;  
        br2, lowest meeting point of eyebrow to vertical line from Uepi;  
        br3, lowest meeting point of eyebrow to vertical line from ps;  
        br4, lowest meeting point of eyebrow to vertical line from Ucan;  
        br5, lowest meeting point of eyebrow to vertical line from ex;  
        al, most lateral point of alar curvature;  
        Mb, most medial point of eyebrow;  
        Hb, highest point of eyebrow;    
        Lb, most lateral point of eyebrow  
        
        从以上的mark点关系可以看出，"眼部的一些mark点"决定了"眉毛的多数mark点"：  
        en/ps/ex/ --> en/Uepi/ps/Ucan/ex --> br1/br2/br3/br4/br5  
        
    - [6] 眉毛的结构:眉头(Mb)、眉腰(br1/br2/br3/br4)、眉峰(Hb)、眉尾，眉尖(Lb)  
        ![](img/zhihu.00.mod.jpg)  
        各部分可能的变化：  
        眉头: 远(out)/近(in)；（眉间距远、近）  
        眉腰：直(line)/弯(curve)；  
        眉峰：高(high)/低(low)/靠里(in)/靠外(out)；  
        眉尾：短(short)/长(long)；  
        眉尖：钝(blunt)/尖(acute)；  

    - [5] 列举了5种眉形： Straight, Round, Soft Angle, Sharp Angle, S-shape.    
        ![](img/MakeupArtistBrowCharts.Straight.jpg) 
        ![](img/MakeupArtistBrowCharts.Round.jpg) 
        ![](img/MakeupArtistBrowCharts.SoftAngle.jpg) 
        ![](img/MakeupArtistBrowCharts.SharpAngle.jpg) 
        ![](img/MakeupArtistBrowCharts.S-shape.jpg)  

    - [2]   
        - 把眉形分为5种： Straight, Curved, Naturally Thick, Arched, Thin.  
        ![](img/TheMicrobladingBible(2016).01.straight.jpg) 
        ![](img/TheMicrobladingBible(2016).01.curved.jpg) 
        ![](img/TheMicrobladingBible(2016).01.arched.jpg) 
        ![](img/TheMicrobladingBible(2016).01.thick.jpg) 
        ![](img/TheMicrobladingBible(2016).01.thin.jpg)  
 
        - 眉毛最高点的位置关系：  
        ![](img/TheMicrobladingBible(2016).02.jpg)  
        - Anatomy of a brow:  
        ![](img/TheMicrobladingBible(2016).03.jpg)  
        - 眉毛与合适的脸形：  
            - Oval faces ~ softly angled brows  
            - Heart-shaped faces ~ low rounded arches(a natural look) / high arch brow (elongate a short face)  
            - Round faces ~ high arches(elongating the face, adding angles)  
            - Square faces ~ soft curves(soften sharp angles)  
            - Diamond-shaped faces ~ medium high soft curves  
 
    - [3]  
        - 眉毛与合适的脸形：  
            - Oval-face ~ soft-angled brow,  
            - Heart-face ~ (low-arched/high-arched)round-brow,  
            - Long-Face ~ flat brow,  
            - Round-Face ~ high-arched brow,  
            - Square-Face ~ Angled brow / curved brow,  
            - Diamond-Face ~ curved brow / round-brow:  
            ![](img/eyebrowz.ovalshape.jpg) 
            ![](img/eyebrowz.heart.jpg) 
            ![](img/eyebrowz.long.jpg) 
            ![](img/eyebrowz.round.jpg) 
            ![](img/eyebrowz.square.jpg) 
            ![](img/eyebrowz.diamond.jpg)  

    - [4] 列举了7种眉形：  
        ![](img/MakingFaces.1999.audrey.jpg) 
        ![](img/MakingFaces.1999.brooke.jpg) 
        ![](img/MakingFaces.1999.clara.jpg) 
        ![](img/MakingFaces.1999.divine.jpg) 
        ![](img/MakingFaces.1999.elizabeth.jpg) 
        ![](img/MakingFaces.1999.marilyn.jpg) 
        ![](img/MakingFaces.1999.marlene.jpg)  


- 本文描述眉毛的方法：  
    - 使用[5]里的记法描述眉型：  
        - Straight:  
                ![](img/MakeupArtistBrowCharts.Straight.jpg) 
                ![](img/TheMicrobladingBible(2016).01.straight.jpg) 
    
        - Round:   
                ![](img/MakeupArtistBrowCharts.Round.jpg) 
                ![](img/TheMicrobladingBible(2016).01.curved.jpg) 
    
        - SoftAngle:  
                ![](img/MakeupArtistBrowCharts.SoftAngle.jpg) 
                ![](img/TheMicrobladingBible(2016).01.thin.jpg) 
    
        - SharpAngle:  
                ![](img/MakeupArtistBrowCharts.SharpAngle.jpg) 
                ![](img/TheMicrobladingBible(2016).01.arched.jpg) 
    
        - S-shape:  
                ![](img/MakeupArtistBrowCharts.S-shape.jpg) 
                ![](img/TheMicrobladingBible(2016).01.thick.jpg) 

    - 眉毛(不同部分)粗细程度分为5类： 1（细）<-- 2（稍细）<-- 3（中等）--> 4（稍粗）--> 5（粗）
    例如，一个Straight眉毛，眉头粗、眉腰中等粗细、眉尾细，可记为： Straight/3-2-1


- 比较眉毛时有一些难度，因为：  
    - 角色常常有各种表情，眉毛随之变化；（或者，只处理无表情的角色镜头）    
    - （经常）镜头里一个角色的左右眉毛不对称（在FC里，经常见到一个镜头里角色的左右脸的五官不对称）；  
  所以，以下只能算是一个粗糙的比较；打算主要比较左脸的眉毛（因为角色发型向右脸偏，所以左脸眉毛显露多）。  


- **雅彦(Masahiko)-(正面)眉**：  
    - basis:  
        - 图片：03_131_4，13_116_3，13_116_4，13_118_6，13_139_1，13_157_0，14_149_5，14_283_4，
        - 特点：成组的图片数量最多。Straight/3-3-1。      
        - 这些图片叠加后的效果：  
        ![](img/brow_front_masahiko_basis.png)   

    - tail-thick:  
        - 图片：01_023_4, 01_081_5,02_063_4   
        - 特点：比basis组的眉尾粗。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_masahiko_tail-thick.png)  



- **紫苑(Shion)-(正面)眉**：  
    - basis:  
        - 图片：01_042_0, 01_047_0, 03_094_2, 08_096_2, 09_083_2,  14_282_1, 11_043_7
        - 特点：成组的图片数量最多。 Straight/3-3-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shion_basis.png)   
        - 和雅彦basis比较，雅彦的眉粗。  

    - tail-low:  
        - 图片：02_119_6, 10_008_6, 12_104_1, 12_104_3, 14_281_6, 04_064_6, 11_133_2,     
        - 特点：比basis组的眉尾(稍微)低。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shion_tail-low.png)  

    - -Hb:  
        - 图片：03_003_0，07_097_0  
        - 特点：Hb点靠内。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shion_-Hb.png)  

    - curved-middle:  
        - 图片：03_003_0, 04_015_5,  
        - 特点：眉腰弯曲。眉型：Round。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shion_curved-middle.png)  


- **塩谷/盐谷(Shionoya)(紫苑男装)-(正面)眉**：  
盐谷多数带帽子，遮盖了眉毛。所以，可选择的镜头不多。所以，这里虽然作了比较，但可信度不高。  
    - basis:  
        - 图片：12_143_0，10_125_0，14_067_4，
        - 特点：成组的图片数量最多。 眉型：Straight/3-3-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shya_basis.png)   
        - 该组(红色)和雅彦basis比较、和紫苑basis比较。  
        ![](img/brow_front_shya_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_shya_basis_vs_shion_basis.jpg) 


- **雅美(Masami)(雅彦女装)-(正面)眉**：  
选择正面、无表情镜头，各自差异大，没有成组的。  


- **若苗空(Sora)-正面**  
    - basis:  
        - 图片：06_135_2, 02_011_1
        - 特点：成组的图片数量最多。 眉型：Straight/3-3-3。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_sora_basis.png)   
        - 该组(红色)和雅彦basis比较。（该组眉毛更粗、更直、眉腰更长）   
        ![](img/brow_front_sora_basis_vs_masahiko_basis.jpg)  
        - 该组(红色)和紫苑basis比较。（该组眉毛更粗、更直、眉腰更长，Hb点更高）  
        ![](img/brow_front_sora_basis_vs_shion_basis.jpg)  


- **若苗紫(Yukari)-正面**  
对比该角色的右脸眉毛（因为发型左偏分）。  
    - basis:  
        - 图片：10_057_0, 01_005_0, 05_042_1, 05_092_2, 05_183_6, 06_135_2, 10_035_3, 08_017_5,  
        - 特点：成组的图片数量最多。 眉型：Straight/3-3-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_basis.png)   
        - 该组(红色)水平翻转并下移后，和雅彦basis比较（该组眉毛更粗、更直、眉腰更长）、和紫苑basis比较:   
        ![](img/brow_front_yukari_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_yukari_basis_vs_shion_basis.jpg)  

    - Hb+:  
        - 图片：07_059_3, 09_155_5, 10_020_4,   
        - 特点：比basis组的Hb点(眉峰)高。 眉型：Straight。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_Hb+.png)   

    - Hb-:  
        - 图片：08_011_5, 02_189_0，    
        - 特点：比basis组的Hb点(眉峰)低。 眉型：Straight。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_Hb-.png)  

    - Hb-_Lb-out:  
        - 图片：08_011_5, 02_189_0，    
        - 特点：比basis组的Hb点(眉峰)低，Lbd点突出(眉尖长)。 眉型：Straight。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_Hb-_Lb-out.png)  

    - angle:  
        - 图片：06_111_0, 01_025_4，02_004_4    
        - 特点：比basis组的眉峰尖角明显。 眉型：Sharp Angle。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_angle.png)  

    - thick:  
        - 图片：02_146_4，06_092_0，      
        - 特点：比basis组的眉粗。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_thick.png)  

    - thin:  
        - 图片：01_124_1，02_000a        
        - 特点：比basis组的眉细。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_thin.png)  


- **浅冈叶子(Yoko)-正面**  
    - basis:  
        - 图片：13_172_0, 13_142_1, 08_152_6, 03_041_2,  
        - 特点：成组的图片数量最多。 眉型：SoftAngle/2-2-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_basis.png)   
        - 该组(红色)平移后和雅彦basis比较、和紫苑basis比较。对比看出：该角色的眉毛细:   
        ![](img/brow_front_yoko_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_yoko_basis_vs_shion_basis.jpg)  

    - curve:  
        - 图片： 06_007_3, 05_160_2, 05_097_5,  
        - 特点： 眉型Angle。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_curve.png)   

    - -Hb:  
        - 图片： 02_092_5, 02_065_0  
        - 特点： Hb点低（Lb随之也低）。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_-Hb.png)   

    - +Hb:  
        - 图片： 10_119_3，05_142_1，13_116_3，     
        - 特点： Hb点高（Lb随之也高）。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_+Hb.png)   

    - -Lb:  
        - 图片： 10_119_3，05_142_1，13_116_3，     
        - 特点： Lb点低（Hb稍低）。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_-Lb.png)   


- **浩美(Hiromi)-正面**  
该角色很多镜头里眉毛被头发遮住了一半。采用右脸眉毛。    
    - basis:  
        - 图片：09_043_4, 05_163_0，04_091_2，    
        - 特点：成组的图片数量最多。 眉型：Round/1-1-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_hiromi_basis.png)   
        - 该组(红色)水平翻转后和雅彦basis比较、和紫苑basis比较。对比看出：该角色眉毛细且上挑:   
        ![](img/brow_front_hiromi_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_hiromi_basis_vs_shion_basis.jpg)  


- **熏(Kaoru)-正面**  
采用右脸眉毛。眉毛边沿不整齐(有毛刺)。    
    - basis:  
        - 图片：08_062_2, 08_004_0, 08_036_5, 07_193_5,   
        - 特点：成组的图片数量最多。 眉型：Round/3-3-3。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kaoru_basis.png)   
        - 该组(红色)水平翻转、上移后和雅彦basis比较、和紫苑basis比较。该角色眉毛稍细、稍上扬(Hb点高):   
        ![](img/brow_front_kaoru_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_kaoru_basis_vs_shion_basis.jpg)  


- **江岛(Ejima)-正面**  
没有可以成组的镜头。   


- **辰巳(Tatsumi)-正面**  
没有可以成组的镜头。  


- **导演-正面**  
    - basis:  
        - 图片：03_110_2, 03_066_4,   
        - 特点：成组的图片数量最多。 眉型：Round/1-2-1(三角)。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_director_basis.png)   
        - 该组(红色)和雅彦basis比较。该角色眉毛短:   
        ![](img/brow_front_director_basis_vs_masahiko_basis.jpg)  


- **早纪(Saki)-正面**  
    - basis:  
        - 图片：09_175_1, 09_174_4, 09_152_3,     
        - 特点：成组的图片数量最多。 眉型：Round/2-1-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_saki_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较。该角色眉毛细:   
        ![](img/brow_front_saki_basis_vs_masahiko_basis.jpg)  


- **真琴(Makoto)-正面**  
    - basis:  
        - 图片：11_035_0, 04_176_1, 06_005_6,     
        - 特点：成组的图片数量最多。 眉型：Round/2-2-2。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_makoto_basis.png)   
        - 该组(红色)和雅彦basis比较、和紫苑basis比较。该角色眉毛细:   
        ![](img/brow_front_makoto_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_makoto_basis_vs_shion_basis.jpg)  

    - asymmetry:  
        - 图片：08_110_0, 07_072_0    
        - 特点：左脸眉毛与basis组一致。左右眉不对称。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_makoto_asymmetry.png)   


- **摄像师-正面**  
    - basis:  
        - 图片：07_104_1, 14_171_4, 12_148_0, 10_067_3     
        - 特点：成组的图片数量最多。 SharpAngle/1-3-1(三角)。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_cameraman_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较:   
        ![](img/brow_front_cameraman_basis_vs_masahiko_basis.jpg)  
    

- **浅葱(Asagi)-正面**  
采用右脸的眉毛（因为左脸眉毛被头发遮住一半）  
    - basis:  
        - 图片：14_250_1, 14_234_0, 14_123_0,      
        - 特点：成组的图片数量最多。 Round/2-1-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_asagi_basis.png)   
        - 该组(红色)水平翻转、上移后，和雅彦basis比较、和紫苑basis比较、和紫basis(水平翻转)比较、和早纪basis比较。该角色眉毛细:   
        ![](img/brow_front_asagi_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_asagi_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_asagi_basis_vs_yukari_basis.jpg) 
        ![](img/brow_front_asagi_basis_vs_saki_basis.jpg)  

    - soft-angle:  
        - 图片：14_057_0, 14_039_0     
        - 特点：眉型： soft-angle。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_asagi_soft-angle.png)   

    - straight:  
        - 图片：14_185_2, 14_169_5     
        - 特点：眉型： straight。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_asagi_straight.png)   

    - 14_067_6，类似basis， 但眉头更粗、像蝌蚪。    
        ![](img/14_067_6__mod.jpg)   


- **和子(Kazuko)-正面**  
    - basis:  
        - 图片：08_109_3, 07_075_0，      
        - 特点：成组的图片数量最多。 SharpAngle/3-3-3。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kazuko_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和空basis比较:   
        ![](img/brow_front_kazuko_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_kazuko_basis_vs_sora_basis.jpg)  

    - basis(man):  
        - 图片：08_120_0，08_120_0,        
        - 特点：男装时，成组的图片数量最多。 眉型：Round/4-4-4。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kazuko_basis(man).png)   
        - 该组(红色)上移后，和雅彦basis比较、和空basis比较。该组眉毛粗:   
        ![](img/brow_front_kazuko_basis(man)_vs_masahiko_basis.jpg) 
        ![](img/brow_front_kazuko_basis(man)_vs_sora_basis.jpg)  

    - 女装时比男装时眉毛稍细。但有一个例外：08_141_3。  
        ![](img/08_141_3__.jpg)  



- **爷爷-正面**  
待完成  


- **横田进(Susumu)-正面**  
采用右脸的眉毛（因为左脸眉毛被头发遮住一半）。  
    - basis:  
        - 图片：03_019_0, 03_012_5,     
        - 特点：成组的图片数量最多。 Straight/2-2-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_susumu_basis.png)   
        - 该组(红色)翻转、右上移后，和雅彦basis比较、和空basis比较。该组眉毛稍细：     
        ![](img/brow_front_susumu_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_susumu_basis_vs_sora_basis.jpg)   


- **宪司(Kenji)-正面**  
    - basis:  
        - 图片：04_051_4, 04_035_1，04_014_1    
        - 特点：成组的图片数量多、表情较为平静。 SharpAngle/5-5-5。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kenji_basis.png)   
        - 该组(红色)翻转、右上移后，和雅彦basis比较、和空basis比较。该组眉毛稍粗：     
        ![](img/brow_front_kenji_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_kenji_basis_vs_sora_basis.jpg)   

    - anger:  
        - 图片：03_189_5，04_010_0，04_039_5，    
        - 特点：怒、喜。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kenji_anger.png)  


- **顺子(Yoriko)-正面**  
    - basis:  
        - 图片：03_155_5, 04_039_0, 03_172_0,      
        - 特点：成组的图片数量最多。 Straight/2-2-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoriko_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较、和紫basis比较。该组眉毛较细:   
        ![](img/brow_front_yoriko_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_yoriko_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_yoriko_basis_vs_yukari_basis.jpg)  

    - soft-angle:  
        - 图片：03_147_1, 10_173_3     
        - 特点： 眉型：soft-angle。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoriko_soft-angle.png)  


- **森(Mori)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：10_108_4, 10_102_1,      
        - 特点：成组的图片数量最多。 Round/1-1-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_mori_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较、和紫basis比较。该组眉毛较细:   
        ![](img/brow_front_mori_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_mori_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_mori_basis_vs_yukari_basis.jpg)  
        - 该组(红色)上移后，和早纪basis比较、和浅葱basis比较：  
        ![](img/brow_front_mori_basis_vs_saki_basis.jpg) 
        ![](img/brow_front_mori_basis_vs_asagi_basis.jpg)  


- **叶子母-正面**  
    - basis:  
        - 图片：13_161_3, 13_136_4_        
        - 特点：成组的图片数量最多。 SoftAngle/2-2-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_ykma_basis.png)   
        - 该组(红色)翻转、上移后，和雅彦basis比较、和紫苑basis比较、和紫basis比较。该组眉毛较细：  
        ![](img/brow_front_ykma_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_ykma_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_ykma_basis_vs_yukari_basis.jpg)  
        - 和叶子basis比较:   
        ![](img/brow_front_ykma_basis_vs_yoko_basis.jpg)  


- **理沙(Risa)-正面**  
采用右脸的眉毛（因为左脸眉毛被头发遮住一半）  
    - basis:  
        - 图片：12_024_6, 12_025_0     
        - 特点：成组的图片数量最多。 Round/4-4-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_risa_basis.png)   
        - 该组(红色)水平翻转、上移后，和雅彦basis比较、和紫苑basis比较、和紫basis(水平翻转)比较:     
        ![](img/brow_front_risa_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_risa_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_risa_basis_vs_yukari_basis.jpg)  


- **美菜(Mika)-正面**  
    - basis:  
        - 图片：12_033_5, 12_008_6，     
        - 特点：成组的图片数量最多。 SoftAngle/1-1-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_mika_basis.png)   
        - 该组(红色)和雅彦basis比较、和紫苑basis比较、和紫basis(水平翻转)比较。该组眉毛较细：  
        ![](img/brow_front_mika_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_mika_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_mika_basis_vs_yukari_basis.jpg)  


- **齐藤茜(Akane)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：06_163_4, 06_145_7, 06_144_5       
        - 特点：成组的图片数量最多。 Straight/1-2-1(三角)。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_akane_basis.png)   
        - 该组(红色)翻转、上移后，和雅彦basis比较、和紫苑basis比较、和紫basis比较:   
        ![](img/brow_front_akane_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_akane_basis_vs_shion_basis.jpg)  

    - asymmetry:  
        - 图片：06_157_1, 07_099_3      
        - 特点：两边的眉毛不对称。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_akane_asymmetry.png)   


- **仁科(Nishina)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：14_028_6, 14_008_7, 14_014_5,   
        - 特点：成组的图片数量最多。 SharpAngle/3-3-3。   
        - 这些图片叠加后的效果：  
        ![](img/brow_front_nishina_basis.png)  
        - 该组(红色)翻转、上移后，和雅彦basis比较、和空basis比较。眉毛一样粗:    
        ![](img/brow_front_nishina_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_nishina_basis_vs_sora_basis.jpg)  

    - short-flat:  
        - 图片：12_077_0, 14_009_5  
        - 特点： 眉毛短、平、粗。   
        - 这些图片叠加后的效果：  
        ![](img/brow_front_nishina_short-flat.png)  


- **葵(Aoi)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：06_081_3, 09_101_6, 06_042_1,         
        - 特点：成组的图片数量最多。 Straight/3-3-3。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_aoi_basis.png)   
        - 该组(红色)翻转、上移后，和雅彦basis比较:   
        ![](img/brow_front_aoi_basis_vs_masahiko_basis.jpg)  


- **奶奶-正面**  
未处理。  


- **文哉(Fumiya)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：12_040_3, 12_012_3, 12_010_3         
        - 特点：成组的图片数量最多。 SharpAngle/1-3-1(三角)。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_fumiya_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和摄像师basis比较：  
        ![](img/brow_front_fumiya_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_fumiya_basis_vs_cameraman_basis.jpg)  


- **齐藤玲子(Reiko)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：01_163_3, 01_169_6,          
        - 特点：成组的图片数量最多。 Straight/1-1-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_reiko_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较、和早纪basis(水平翻转)比较。该组眉毛较细：  
        ![](img/brow_front_reiko_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_reiko_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_reiko_basis_vs_saki_basis.jpg)  


- **松下敏史(Toshifumi)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：09_072_5, 09_076_5,          
        - 特点：成组的图片数量最多。 愤怒。SharpAngle/4-4-4。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_matsu_basis.png)   
        - 该组(红色)翻转、下移后，和雅彦basis比较。该组眉毛较粗：  
        ![](img/brow_front_matsu_basis_vs_masahiko_basis.jpg)  


- **章子(Shoko)-正面**  
待完成  


- **阿透(Tooru)-正面**  
没有可以成组的镜头。12_012_3显示的眉型：Straight，形状是三角形。  
![](img/12_012_3__.jpg)  


- **京子(Kyoko)-正面**  
没有可以成组的镜头。  


- **一马(Kazuma)-正面**  
待完成  


- **一树(Kazuki)-正面**  
没有可以成组的镜头。 12_012_3和雅彦basis比较。基本一致，眉尾稍粗、稍长：  
![](img/brow_front_kazuki_12_012_3.png) 
![](img/brow_front_kazuki_12_012_3_vs_masahiko_basis.jpg)  


- **千夏(Chinatsu)-正面**  
采用右脸的眉毛。  
    - basis:  
        - 图片：12_141_0, 14_016_2,          
        - 特点：成组的图片数量最多。 Round/3-2-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_chinatsu_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较。眉腰倾斜度大：  
        ![](img/brow_front_chinatsu_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_chinatsu_basis_vs_shion_basis.jpg)  


- **麻衣(Mai)-正面**  
    - basis:  
        - 图片：12_159_3, 12_141_0,          
        - 特点：成组的图片数量最多。 Round/1-1-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_mai_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较、和早纪basis(水平翻转)比较。眉腰倾斜度大：  
        ![](img/brow_front_mai_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_mai_basis_vs_shion_basis.jpg)  


- **绫子(Aya)-正面**  
    - basis:  
        - 图片：02_177_3, 02_189_4,          
        - 特点：成组的图片数量最多。 SharpAngle/3-2-1。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_aya_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较。眉腰倾斜度大：  
        ![](img/brow_front_aya_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_aya_basis_vs_shion_basis.jpg)  


- **典子(Tenko)-正面**  
没有可以成组的镜头。  


- **古屋公弘(Kimihiro)-正面**  
没有可以成组的镜头。 09_051_6和雅彦basis比较。基本一致，眉尾稍粗、稍长：  
![](img/brow_front_kimihiro_09_051_6.png) 
![](img/brow_front_kimihiro_09_051_6_vs_masahiko_basis.jpg)  


- **古屋朋美(Tomomi)-正面**  
没有可以成组的镜头。  


- **八不(Hanzu)-正面**  
没有可以成组的镜头。   


- **哲(Te)-正面**  
没有可以成组的镜头。  


- 排列如下(角色左眉, 放大4倍):  
![](img/brow_front_x4/brow_front_masahiko.jpg) 
![](img/brow_front_x4/brow_front_masami.jpg) 
![](img/brow_front_x4/brow_front_shion.jpg) 
![](img/brow_front_x4/brow_front_shya.jpg) 
![](img/brow_front_x4/brow_front_sora.jpg) 
![](img/brow_front_x4/brow_front_yukari.jpg) 
![](img/brow_front_x4/brow_front_yoko.jpg) 
![](img/brow_front_x4/brow_front_hiromi.jpg) 
![](img/brow_front_x4/brow_front_kaoru.jpg) 
![](img/brow_front_x4/brow_front_ejima.jpg) 
![](img/brow_front_x4/brow_front_tatsumi.jpg) 
![](img/brow_front_x4/brow_front_makoto.jpg) 
![](img/brow_front_x4/brow_front_director.jpg) 
![](img/brow_front_x4/brow_front_saki.jpg) 
![](img/brow_front_x4/brow_front_asagi.jpg) 
![](img/brow_front_x4/brow_front_cameraman.jpg) 
![](img/brow_front_x4/brow_front_yoriko.jpg) 
![](img/brow_front_x4/brow_front_mori.jpg) 
![](img/brow_front_x4/brow_front_kazuko.jpg) 
![](img/brow_front_x4/brow_front_grandpa.jpg) 
![](img/brow_front_x4/brow_front_susumu.jpg) 
![](img/brow_front_x4/brow_front_kenji.jpg) 
![](img/brow_front_x4/brow_front_ykma.jpg) 
![](img/brow_front_x4/brow_front_risa.jpg) 
![](img/brow_front_x4/brow_front_mika.jpg) 
![](img/brow_front_x4/brow_front_akane.jpg) 
![](img/brow_front_x4/brow_front_nishina.jpg) 
![](img/brow_front_x4/brow_front_aoi.jpg) 
![](img/brow_front_x4/brow_front_grandma.jpg) 
![](img/brow_front_x4/brow_front_fumiya.jpg) 
![](img/brow_front_x4/brow_front_reiko.jpg) 
![](img/brow_front_x4/brow_front_matsu.jpg) 
![](img/brow_front_x4/brow_front_shoko.jpg) 
![](img/brow_front_x4/brow_front_tooru.jpg) 
![](img/brow_front_x4/brow_front_kyoko.jpg) 
![](img/brow_front_x4/brow_front_kazuma.jpg) 
![](img/brow_front_x4/brow_front_kazuki.jpg) 
![](img/brow_front_x4/brow_front_chinatsu.jpg) 
![](img/brow_front_x4/brow_front_mai.jpg) 
![](img/brow_front_x4/brow_front_aya.jpg) 
![](img/brow_front_x4/brow_front_tenko.jpg) 
![](img/brow_front_x4/brow_front_kimihiro.jpg) 
![](img/brow_front_x4/brow_front_tomomi.jpg) 
![](img/brow_front_x4/brow_front_hanzu.jpg) 
![](img/brow_front_x4/brow_front_te.jpg) 


- **总结**

    - **把类似的眉毛分组： **  
        Straight/3-3-1： 雅彦basis ～紫苑basis～shya basis～紫basis  
        Straight/3-3-3： sora basis ～ aoi basis？（遮蔽）  
        Straight/2-2-1： susumu basis～yoriko basis  
        Straight/1-2-1(三角)：  akane  
        Straight/1-1-1: reiko  
        Round/2-1-1： asagi basis～saki basis  
        Round/1-1-1： hiromi basis ～mori basis？（遮蔽）~ mai basis  
        Round/2-2-2： makoto basis  
        Round/3-3-3： kaoru basis？（遮蔽）
        Round/1-2-1,短(三角)： director basis  
        Round/4-4-1： risa basis  
        Round/3-2-1： chinatsu basis  
        SoftAngle/2-2-1： yoko basis～ykma basis  
        SoftAngle/1-1-1： mika basis  
        SharpAngle/3-2-1： aya basis  
        SharpAngle/3-3-3： kazuko basis～nishina basis
        SharpAngle/5-5-5： kenji basis
        SharpAngle/4-4-4： matsu basis  
        SharpAngle/1-3-1(三角)： cameraman basis ～ fumiya basis  
    
        最核心的四个角色(雅彦、紫苑、空、紫)的眉型都是Straight。


    - **合并眉毛粗细度(合并2,3,4)，从而减少分组**：  
        Straight/3-3-1： 雅彦basis ～紫苑basis～shya basis～紫basis **～～** susumu basis～yoriko basis   
        Straight/3-3-3： sora basis ～ aoi basis？（遮蔽）  
        Straight/1-3-1(三角)：  akane  
        Straight/1-1-1: reiko  
        Round/3-1-1： asagi basis～saki basis  
        Round/1-1-1： hiromi basis ～mori basis？（遮蔽）~ mai basis  
        Round/3-3-3： kaoru basis？（遮蔽）～ makoto basis  
        Round/1-3-1,短(三角)： director basis  
        Round/3-3-1： risa basis ～ chinatsu basis  
        SoftAngle/3-3-1： yoko basis～ykma basis  
        SoftAngle/1-1-1： mika basis  
        SharpAngle/3-3-1： aya basis  
        SharpAngle/3-3-3： kazuko basis～nishina basis**～～**matsu basis  
        SharpAngle/5-5-5： kenji basis
        SharpAngle/1-3-1(三角)： cameraman basis ～ fumiya basis  
        

    - **进一步合并较为类似的眉毛，从而减少分组**：  
        Straight/3-3-3： sora basis ～ aoi basis？（遮蔽） **～～～** kaoru basis？（遮蔽）～ makoto basis  
        Straight/3-3-1： 雅彦basis ～紫苑basis～shya basis～紫basis ～～ susumu basis～yoriko basis **～～～**  yoko basis～ykma basis  
        Straight/1-1-1: reiko  **～～～** mika basis **～～～** akane **～～～** hiromi basis ～mori basis？（遮蔽）~ mai basis  
        Round/3-3-1： risa basis ～ chinatsu basis  
        Round/3-1-1： asagi basis～saki basis  
        Round/1-3-1,短(三角)： director basis  
        SharpAngle/3-3-3： kazuko basis～nishina basis～～matsu basis **～～～** kenji basis  
        SharpAngle/3-3-1： aya basis  
        SharpAngle/1-3-1(三角)： cameraman basis ～ fumiya basis  



**参考资料**：  
[1]. Biometric Study of Eyelid Shape and Dimensions of Different Races with References to Beauty. Seung Chul Rhee, Kyoung-Sik Woo, Bongsik Kwon. Aesth Plast Surg (2012), DOI 10.1007/s00266-012-9937-7   
[2]. The Microblading Bible. 2016. Corinne Asch.  
[3]. [https://www.eyebrowz.com](https://www.eyebrowz.com)   
[4]. Making Faces. 1997. Kevyn Aucoin.  
[5]. Makeup Artist Brow Charts. 2016. Gina M. Reyna.  
[6]. [眉形综合篇！这是一份超全面超详细的眉形攻略 - 知乎](https://zhuanlan.zhihu.com/p/32484098)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - （如果图片或格式有问题，可以访问[这个链接](./readme.md)）


# FC的画风-面部比例  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96743))  


人像面部素描（成年男性/成年女性）一般会遵循如下的比例：

图1

![](img/DrawingTheHeadHands.AndrewLoomis.male-female.jpg)

- 红色标记**头顶至下巴**的**竖直**二等分；
- 蓝色标记**发迹线至下巴**的**竖直**三等分；
- 绿色标记**双眼外眼角间**的**水平**三等分；

--------------------------------------------------
##  紫苑
图2

![](img/proportion_shion_profile.jpg) 

从上图可以看出：

- 红色和蓝色的标记符合人像素描比例；
- 从第一行的三幅图里的绿色标记可以看出：内眼角略宽于1/3。作者可能是按照少年人像比例画的紫苑。（这里有个[参考图](img/DrawingTheHeadHands.AndrewLoomis.jpg)  ）；

--------------------------------------------------
##  雅彦
图3

![](img/masahiko.jpg)

从上图可以看出：

- 红色和蓝色的标记符合人像素描比例；
- 从第一行的绿色标记可以看出：内眼角略宽于1/3。作者可能是按照少年人像比例画的雅彦。


--------------------------------------------------
##  紫
图4

![](img/yukari.jpg)

从上图可以看出：

- 红色和蓝色的标记符合人像素描比例；
- 关于眼部水平三等分的绿色标记：
    - 第一行是FC里紫第一次出场，内眼角略宽于1/3。差别很小。
    - 其他图里的内眼角宽度(与1/3比起来)似乎略有增加。但这个差别和紫苑、雅彦的比起来，似乎要小一些。

--------------------------------------------------
##  空
图5

![](img/sora.jpg)

从上图可以看出：

- 红色和蓝色的标记符合人像素描比例；
- 从绿色标记可以看出：内眼角略宽于1/3


----
- **关于FC里内眼角间距略大于外眼角间距的1/3（素描教程里给的刚好是1/3）**:

    - 查了一下AH里的香莹和獠，也是[这样的](img/AH.jpg)。
    - 也可能是我哪里理解的不对，或者测量的位置不对。
    - 测了一下[实际的人像](https://fstoppers.com/portraits/average-faces-women-around-world-2944)。考虑到人像照片里的外眼角有内边沿和外边沿，两者都[测了一下](img/average-faces-of-women_china_korea_jap.jpg)。  
        - 如果以外眼角的**外**边沿作三等分的话，内眼角的宽度比例刚刚好、或略大于1/3；  
        - 如果以外眼角的**内**边沿作三等分的话，内眼角的宽度比例明显大于1/3。  
        - 这样看来，关于内眼角的宽度比例，FC里使用的比例更接近真实的人像比例。  
    - 还有一种可能，如下图所示，以上以为的内眼角是点Ri0/Li0, 实际内眼角(Ri1/Li1)没有被画出：  
    ![](../fc_drawing_style__face_eye/img/01_023_4__eye_illustration.jpg)  
    - 还有一种可能的原因：内眼角间距略大，**可能**会显得比较漂亮。文献[1]通过统计得出结论“regardless of race, attractive faces have relatively wide-set eyes in comparison to average faces of the same race(无论哪个人种，相比于普通面孔，漂亮面孔的眼睛看起来较宽)”。但，这是一个粗糙的表述；更详细的表述是[1]：无论人种，漂亮面孔的瞳孔间距(Interpupillary Distance(IPD))较宽；但内眼角间距(Intercanthal Distance(ICD))不一定较宽)。  


- 14_163_0，紫苑和雅美这张肖像对比图，反倒是没有遵循速写比例（或者，可以认为人物略微低头）。不知作者为何这么画：如果为了对比两人相貌，为何不按照正确的五官比例画？  
![](img/14_163_0__.jpg)  
用这种对比方法对比主要角色：  
（下图左1）用方法4比较（左：紫苑group2；右：雅彦group2）；      
（下图左2）用方法4比较（左：紫苑group3；右：雅彦group2）；      
（下图左3）用方法4比较（左：盐谷psudo-front；右：雅彦group2）；      
（下图左4）用方法4比较（左：盐谷right-top；右：雅彦group2）；      
![](../fc_drawing_style__face/img2/face_front_cmp__shion-group2_vs_masahiko-group2_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shion-group3_vs_masahiko-group2_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shya-psudo-front_vs_masahiko-group2_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shya-right-top_vs_masahiko-group2_rl-cmp-p.jpg) 

（下图左1）用方法4比较（左：紫苑group2；右：雅美group0）；   
（下图左2）用方法4比较（左：紫苑group3；右：雅美group0）；   
（下图左3）用方法4比较（左：盐谷psudo-front；右：雅美group0）；   
（下图左4）用方法4比较（左：盐谷right-top；右：雅美group0）；   
![](../fc_drawing_style__face/img2/face_front_cmp__shion-group2_vs_masami-group0_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shion-group3_vs_masami-group0_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shya-psudo-front_vs_masami-group0_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shya-right-top_vs_masami-group0_rl-cmp-p.jpg) 

（下图左1）用方法4比较（左：紫苑group2；右：雅美group1）；                
（下图左2）用方法4比较（左：紫苑group3；右：雅美group1）；       
（下图左3）用方法4比较（左：盐谷psudo-front；右：雅美group1）；       
（下图左4）用方法4比较（左：盐谷right-top；右：雅美group1）；     
![](../fc_drawing_style__face/img2/face_front_cmp__shion-group2_vs_masami-group1_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shion-group3_vs_masami-group1_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shya-psudo-front_vs_masami-group1_rl-cmp-p.jpg) 
![](../fc_drawing_style__face/img2/face_front_cmp__shya-right-top_vs_masami-group1_rl-cmp-p.jpg) 



**参考资料**:  
1. [Biometric Study of Eyelid Shape and Dimensions of Different Races with References to Beauty](https://www.researchgate.net/publication/229322530_Biometric_Study_of_Eyelid_Shape_and_Dimensions_of_Different_Races_with_References_to_Beauty). Aesth Plast Surg (2012). Seung Chul Rhee, Kyoung-Sik Woo, Bongsik Kwon. DOI 10.1007/s00266-012-9937-7  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处




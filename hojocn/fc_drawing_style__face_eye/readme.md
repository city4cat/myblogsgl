
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](readme.md)  


**FC的画风-面部-眼部**  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96776))  

- **上眼皮和眉毛之间的那条线**  
    - **紫苑**  
如下图，上眼皮和眉毛之间的这条线在北条司漫画里非常常见。(如果我没有记错的话，应该是在《猫眼》的中期或后期就有了这种画法。)  
图3:  
![](img/proportion_shion_profile_with_eyeline.jpg)  
从上图可以看出：  
- 上眼皮和眉毛之间的这条线在原本眉毛的位置；  
- 眉毛随之上移；  
- 在显示发迹线的3幅图里，有2副图里的发迹线也上移了；  

看起来的确很漂亮，但我一直很好奇为什么这样画。我试着擦去这条线，看看效果：  
图4:  
![](img/proportion_shion_profile_with_eyeline_hide-line.jpg)  
虽然看起来也挺漂亮的。那么，上眼皮和眉毛之间的这条线到底有什么用呢？  

关于眉毛上移，[1]里认为：“we noticed a clear relation of greater lifting of the eyebrow corresponding to more satisfactory overall final results.(我们注意到一个明显的关系：眉毛越上移，最终效果越令人满意)”[1]。由此，我的理解是，把眉毛位置画得靠上一些，人物会显得漂亮。（但，[2]里的观点相反："eyebrows are lower in attractive faces than in average faces and that the absolute distance between eyebrow and eyelid of attractive faces is relatively smaller than that in average faces. ... if the plastic surgeon lifts the patient’s eyebrows to a position that is too high, especially the medial eyebrow, it usually goes against the aesthetics of facial attractiveness."）。此外，[3]里66页提到，对于眉毛到眼睛的距离，女性大于男性。    



《How To Draw Anime And Game Characters》(Volume1)里第40页，以一个角色为例，说眼睛和眉毛间的这条线表示双眼皮："The lines for the eyes and eyelids are wide apart. There are definite double eyelids. The lines for the eyelids and eyebrows meet at the brow(眼睛和(上)眼皮之间很宽。双眼皮很明显。眼皮和眉毛相交于眉毛)."  
![](img/HowToDrawAnimeAndGameCharacters_vol1_040.jpg)  

- **睫毛**  
FC里，睫毛是女性的一个重要特征。女性的睫毛很长。男性要么不画睫毛、要么睫毛很短。比如，紫苑的睫毛很长；但男装的紫苑常常不画睫毛；即使男装的紫苑是特写，睫毛也很短。如下图：  
![](img/eyelash_shion_boy.jpg)  

    - 紫苑女装不画睫毛时的样子：  
    ![](img/02_148_0__.jpg)  


- **侧面的睫毛**  
北条司漫画里，当一个人物侧面时，常常会把另一侧的睫毛画出来，如下图第一行：  
![](img/eyelash_side.jpg)  
从图里紫苑右侧的睫毛位置来说，左侧的睫毛应该看不见才对(当然，也可以说人物侧面的角度刚好使左侧的睫毛露出来了；但FC里这种画法实在太常见了，以至于我不认为这和侧面的角度有关)。图里第二行是去掉左侧睫毛，做个对比。我个人的感觉是：加上另一侧的睫毛，左侧的紫苑显得立体了。右侧的雅彦效果不明显。图里第三行是两侧的睫毛高度相同的情况（另一侧的睫毛缩短后好看一些）。  
后来发现这种情况是实际存在的：  
![](img/narcos_s02_eyelash_0.jpg) 
![](img/narcos_s02_eyelash_1.jpg) 
![](img/narcos_s02_eyelash_2.jpg) 
![](img/narcos_s02_eyelash_3.jpg) 
![](img/narcos_s02_eyelash_4.jpg)  
(图片来自《Narcos》第二季)  



- **侧面**

侧面角度不大时，另一侧的眼部会用阴影代替：  
![](img/14_021_0__.jpg) 
![](img/14_021_4__.jpg) 
![](img/05_015_6__.jpg)  

- **眼角**  
    - 以01_023_4为例：  
        ![](img/01_023_4__eye.jpg) 
        ![](img/01_023_4__eye_illustration.jpg)  
        - Ro0/Lo0：外眼角。  
        - Ro1/Lo1：外眼角处的折线的末端点（外眼角向内侧下方的斜线的末端点）。  
        - Ri0/Li0：内侧上眼皮的起点。  
        - Ri1/Li1：内眼角。（FC里大多数情况下，内眼角没有被画出）  

    - 01_023_4里，左右外眼角均被画成折线：Lo0点至Lo1点，Ro0点至Ro1点。FC里大多数情况下，有一侧外眼角不是折线(无Ro1/Lo1点)。  
    - 两侧外眼角均为折线：  
        - 雅彦：  
        ![](img/03_131_4__eye.jpg) 
        ![](img/13_116_4__eye.jpg) 
        ![](img/13_118_6__eye.jpg) 
        ![](img/13_139_1__eye.jpg) 
        ![](img/13_157_0__eye.jpg) 
        ![](img/14_149_5__eye.jpg) 
        - 雅美：  
        ![](img/07_048_0__eye.jpg) 
        ![](img/07_178_2__eye.jpg) 
        ![](img/14_254_5__eye.jpg)  

    - 只有一侧外眼角为折线：  
        - 雅美：  
        ![](img/03_031_2__eye.jpg) 
        ![](img/12_016_5__eye.jpg) 
        ![](img/12_054_4__eye.jpg)  

    - 两侧外眼角均无折线：  
        - 雅彦：  
        ![](img/01_081_5__eye.jpg) 
        ![](img/02_063_4__eye.jpg) 
        ![](img/03_139_0__eye.jpg) 
        ![](img/13_129_5__eye.jpg) 
        ![](img/14_043_2__eye.jpg) 

    - 其他：  
        - 雅彦：  
        ![](img/01_149_3__eye.jpg)  
        - 雅美：  
        ![](img/03_114_0__eye.jpg)  
        右眼外眼角用竖线：  
        ![](img/04_161_1__eye.jpg) 
        ![](img/06_155_2__eye.jpg)  

    - 如果左右脸对称，那么内眼角(Ri1/Li1)（或上眼皮起点(Ri0/Li0)）的连线应该和外眼角(Ro0/Lo0)连线平行。例如14_234_0浅葱的这个镜头：  
    ![](img/14_234_0__eye_corner_line.jpg)  
    但FC里有一些图里(常常没有画内眼角(Ri1/Li1))"上眼皮起点的连线(Ri0-Li0)"与"外眼角和对侧外眼角折线末端的连线(Ro1-Lo0或Ro0-Lo1)"更接近平行。例如, 14_074_2里浅葱的这个镜头：    
    ![](img/14_074_2__eye_corner_line.jpg)  
    再例如03_155_5里顺子的这个镜头(有内眼角)：  
    ![](img/03_155_5__mod.jpg)  
    ![](img/03_155_5__eye_corner_line.jpg)  

    - Macro Shot(微距镜头)和Extreme Close Up(E.C.U.)(极特写镜头/大特写镜头/极近距镜头)时，会画出内眼角处眼皮内侧的厚度：  
    ![](img/14_283_3__.jpg)  
    ![](img/14_283_4__eye.jpg)  



- **下眼眶**
    - 下眼眶的线条常常有断点、甚至是大段空白(下图蓝色箭头所示)：  
    ![](img/06_069_4__mod.jpg) 
    ![](img/01_081_5__mod.jpg)  

    - 画得细致时，会画出上下眼皮的厚度：  
    ![](img/04_141_0__eye.jpg)  

    - 常常用"下眼皮的睫毛"和"下眼皮与眼球交线"之间的空白表现下眼皮的厚度。如下图，绿色线条所示下眼皮的厚度：  
    ![](img/08_017_5__mod.jpg)  

    - 01_104_1，紫苑的眼睛显得不太自然（这种不太自然画法好像在猫眼里比较多）:  
    ![](img/01_104_1__.jpg)  
    尝试了两种方法让01_104_1里紫苑的眼睛自然一些：  
        - 方法1，画出下眼眶(以下分别是下眼眶向下弯曲、不弯曲、稍微向上弯曲)：    
        ![](img/01_104_1__mod_1.jpg) 
        ![](img/01_104_1__mod_2.jpg) 
        ![](img/01_104_1__mod_3.jpg) 
        
        - 方法2，删去"眼睛和眉毛之间的那条线"：   
        ![](img/01_104_1__mod_cove_line_between_eye_and_brow.jpg)  
        方法2解释了"同样是眼睛斜视、同样没有下眼眶，02_049_0(下图)就看起来很自然"的原因：因为02_049_0里人物没有"眼睛和眉毛之间的那条线"：  
        ![](img/02_049_0__.jpg)  


    - 正视图时，下眼眶线常常是向下弯曲或不弯曲。极少数情况下，下眼眶线向上弯曲（可能是因为"眯眼睛"这个动作）：  
        下眼眶线向下弯曲:  
            ![](img/04_141_0__eye.jpg)  
        下眼眶线不弯曲:  
            ![](img/06_069_4__.jpg)  
        紫的右眼下眼眶线稍微向上弯曲（左眼下眼眶线不弯曲）：  
            ![](img/08_017_5__.jpg)  
        紫的左眼下眼眶线向上弯曲（右眼下眼眶线不弯曲）：  
            ![](img/04_000a_0__.jpg)  
        眼睑明显时，轻微眯眼睛就有这个效果：  
            ![](img/IzumiAshikawa_in_Daikanbu_Burai_1968.jpg)  
            （图片源自《Daikanbu Burai》(1968),芦川泉/芦川いづみ/Izumi Ashikawa)

- 11_015_5, （紫苑想知道自己扮男装是否会被人识破，所以在雅彦和叶子约会时，紫苑以盐谷的身份出现。后来，雅彦因为兼职而先离开，所以由紫苑陪叶子买衣服）事后，叶子说：“不过真叫人惊讶呀！你对服装了如指掌，像女孩子一样”。  
此处叶子斜眼看盐谷(紫苑男装)，眼睛有高光(下左图)。一般来说，这种Medium Close Up镜头是不画眼部高光的，因为距离足够远、眼睛足够小。去掉高光(下右图)，做个比对：  
![](img/11_015_5__.jpg) 
![](img/11_015_5__no_high-light.jpg)  

<a name="更真实的眼睛画法"></a>  

- **更真实的眼睛画法**  
    - 下图（[3]里81页）显示了“眼睛向上看”的细节；  
        ![](img/TheArtistsCompleteGuideToFacialExpression.p081.A.jpg)  
        ![](img/TheArtistsCompleteGuideToFacialExpression.p081.B.jpg)  
        ![](img/TheArtistsCompleteGuideToFacialExpression.p081.C.jpg)  
        ![](img/TheArtistsCompleteGuideToFacialExpression.p081.D.jpg)  
    
    - 下图（[3]里83页）显示了“眼睛向下看”的细节(与"闭眼"的细微差别)。  
        ![](img/TheArtistsCompleteGuideToFacialExpression.p083.jpg)  
      FC里的“眼睛向下看”（01_188_3, 02_011_1，09_036_0, 09_041_7, 10_119_3，）：  
        ![](img/01_188_3__mod.jpg) 
        ![](img/02_011_1__mod.jpg) 
        ![](img/09_036_0__mod.jpg) 
        ![](img/09_041_7__mod.jpg) 
        ![](img/10_119_3__mod.jpg)   
    
    - 下图（[3]里84页）显示了“斜眼看”的细节。作者认为“斜眼看时，两眼视线方向不一致”是不正常的([3]里87页)。：    
        ![](img/TheArtistsCompleteGuideToFacialExpression.p084.jpg)  
      FC有这样的画法(04_141_0)：  
      ![](img/04_141_0__eye.jpg)  


- **其他**  
    - 09_027. 叶子酒宿醒来，满脸疲惫和憔悴。画得太传神了！（左图）但我看了半天也没太明白这种疲惫和憔悴的感觉是怎么表现的：用短睫毛表现的吗？但短睫毛表现的是男性化啊。用头发的非白和面部的高光表现的吗？再后来我尝试把眉毛下移到正常位置（右图），做了个比较：      
    ![](./img/09_027_0__.jpg)

    - 09_113_3,  
    ![](img/09_113_3__.jpg)  

    - 10_058_3, （早纪离开后，辰巳和熏和好（两人一起外出吃饭），两人斗嘴被紫苑和雅彦在阳台上看到）紫苑和雅彦都是眯眼笑，但紫苑眯眼的线条圆滑，雅彦眯眼的线条有尖角。我好奇为什么这么画，所以调换两人的眼睛(下图左二)，做个对比。调换后，似乎没什么不合适，所以，[疑问]不知道为什么这么画。      
    ![](img/10_058_3__crop0.jpg) 
    ![](img/10_058_3__crop0_mod.jpg)  

    - 10_182_0, 左脸虽然是阴影，但仍画出了睫毛和眼球的轮廓。  
    ![](img/10_182_0__crop.jpg)  

    - 11_025_0，眼睛向后看。  
    ![](img/11_025_0__.jpg)  
    
    - 11_060_0，因头发遮挡，熏的左眼省略。(效果有点恐怖)  
    ![](img/11_060_0__.jpg)  
    
    - 11_016_5,11_016_6, 眼睛侧面，角膜未凸起。对比一下角膜凸起的画法：    
    ![](img/11_016_5__.jpg) 
    ![](img/11_016_5__mod.jpg)  
    ![](img/11_016_6__.jpg) 
    ![](img/11_016_6__mod.jpg)  

    - 12_013_5，内眼角和鼻梁之间有一个凹陷。  
    ![](img/12_013_5__crop1.jpg)  
    
    - 12_023_2, 12_023_7, 12_121_6, 12_150_2. 侧面的眼睛。角膜未明显凸起，可能是因为不是严格的侧面。    
    ![](img/12_023_2__crop.jpg) 
    ![](img/12_023_7__crop.jpg) 
    ![](img/12_121_6__crop0.jpg) 
    ![](img/12_150_2__crop.jpg)  

    - 12_110, 人物的外眼角（雅彦的右眼角、紫苑的左眼角）画成了弧线：  
    ![](./img/12_110_1__.jpg) 
    ![](./img/12_110_3__.jpg)  

    - 12_162_4，江岛和摄影师的表情是迷惑。没有眼眶。

    - 12_191_3，侧后方的眼睛：  
    ![](img/12_191_3__crop0.jpg)  
    熏的另一侧眼睛的简化画法。  
    ![](img/12_191_3__crop1.jpg)  
    

    - 13_010_2, 下眼皮的厚度.  
    ![](img/13_010_2__crop.jpg)  
    
    - 13_048_0, 眼睛画得比较简单。无眼皮厚度，下眼皮睫毛很少。  
    ![](img/13_048_0__.jpg) 
    ![](img/13_048_0__crop.jpg)  
    
    - 13_072_3，眼部特写。  
    ![](img/13_072_3__crop.jpg) 

    - 13_128_5，13_129_6（叶子带雅彦、盐谷回到自家旅馆。回忆"母亲是否讨厌自己"的事情）叶子面部是水平，但左右眼高度不同。13_128_5和13_129_6都是这样，所以应该不是偶然的。但作者为什么这么画呢？[疑问]：  
        ![](./img/13_128_5__.jpg)  
        ![](./img/13_128_5__mod2.jpg)  
        ![](./img/13_129_6__.jpg) 
        ![](./img/13_129_6__mod2.jpg)  
        我把图分隔为两个，对比一下：  
        ![](./img/13_128_5__.jpg) 
        ![](./img/13_128_5__mod.jpg)  
        ![](./img/13_129_6__.jpg) 
        ![](./img/13_129_6__mod.jpg)  

    - 14_014_5, 仁科(眯眼睛)的眼睛修长、下眼眶是圆滑的弧线。这种画法好像在CH里多见。  
    ![](img/14_014_5__mod.jpg) 

    - 14_016_6, 画出了下眼睑。  
    ![](img/14_016_6__.jpg) 
    ![](img/14_016_6__crop0.jpg) 
    
    - 14_016_7,。  
    ![](img/14_007_4__.jpg) 
    ![](img/14_007_4__crop0.jpg) 
    
    - 14_036_4, 14_036_6, 画出了对侧的眼睛。  
    ![](img/14_036_4__.jpg) 
    ![](img/14_036_4__crop0.jpg)  
    ![](img/14_036_6__.jpg) 
    ![](img/14_036_6__crop0.jpg) 




**参考资料**:  
[1]. [Is an Objective Measuring System for Facial Attractiveness Possible?](https://www.researchgate.net/publication/6388998_An_Objective_System_for_Measuring_Facial_Attractiveness), Mounir Bashour, 2005.  
[2]. [Biometric Study of Eyelid Shape and Dimensions of Different Races with References to Beauty](https://www.researchgate.net/publication/229322530_Biometric_Study_of_Eyelid_Shape_and_Dimensions_of_Different_Races_with_References_to_Beauty). Aesth Plast Surg (2012). Seung Chul Rhee, Kyoung-Sik Woo, Bongsik Kwon. DOI 10.1007/s00266-012-9937-7  
[3]. The Artists Complete Guide to Facial Expression, 1990, Gary Faigin.  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

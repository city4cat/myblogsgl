
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 原链接：[FC的画风-面部-年龄变化](./readme.md)

**FC的画风-面部-年龄变化**  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96892))  
- 素描教程[1]里给出的"面部比例随年龄的变化"：  
![](../fc_drawing_style__face_proportion/img/DrawingTheHeadHands.AndrewLoomis.jpg)  

- 05_129_1，角色面部比例符合[1]里的"school girls"(约8～12岁)：  
![](img/05_129_1__mod.jpg)  
此时雅彦的相貌应该是03_020_0(基于[2]里的推测）：  
![](img/03_020_0__mod.jpg)   
但由于这个镜头里角色有惊讶的表情，所以不能准确说明该时期的角色五官符合标准，只能说大概符合标准。  


- 05_125_0，不适合判断面部比例，因为从"耳朵的相对位置"判断，角色可能稍微低头。  
![](img/05_125_0__mod.jpg)  




**参考资料**：  
1. Drawing The Head & Hands, Andrew Loomis.  
2. [FC时间线](../fc_information/fc_timeline.md)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

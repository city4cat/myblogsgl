
source:  
https://www.projectnerd.it/2018-01-a-romics-di-aprile-tsukasa-hojo-il-padre-di-occhi-di-gatto-e-city-hunter/

（比起[Google Translate](https://translate.google.cn)，[DeepL Translate](https://www.deepl.com/translator)对本文的翻译较好）

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_romics/img/tsukasa-hojo.jpg)

# A Romics di Aprile Tsukasa Hojo, il padre di Occhi di Gatto e City Hunter  
4月，猫眼与城市猎人之父 北条司将会出席Romics大会

Alan RossiPosted on 19 gennaio 2018 by Alan Rossi  
Alan Rossi发表于2018年1月19日，Alan Rossi

## A Romics di Aprile 2018 sarà presente il grande mangaka Tsukasa Hojo, celebre creatore di Occhi di Gatto, City Hunter ed Angel Heart.  
在2018年4月举行的Romics大会上，将有著名的猫眼、城市猎人和天使心的作者，伟大的漫画家北条司。

Ecco il comunicato rilasciato sul sito ufficiale di Romics riguardo la presenza del mangaka, presso il Romics che si svolgerà ad Aprile 2018:  
这是Romics官方网站上发布的有关漫画家于2018年4月出席Romics的新闻：

>>    La XXIII edizione di Romics celebrerà il grande mangaka Tsukasa Hojo con l’assegnazione del Romics d’Oro alla carriera. Celebre creatore delle serie Occhi di Gatto, City Hunter e Angel Heart, Hojo si appassiona al fumetto fin da bambino grazie all’amicizia con un compagno che ambisce a diventare un fumettista professionista. Dopo il debutto nel 1980 come mangaka professionista su Shonen Jump con l’opera Ore wa Otoko da!, inizia con Nobuhiko Morie una stretta collaborazione che lo porterà nel 1981 alla pubblicazione di Occhi di Gatto (Cat’s Eye). L’opera, incentrata sulle vicende di tre sorelle abili e agili ladre a caccia di opere d’arte, conquista fin da subito il pubblico portando immediata popolarità all’autore sia per i temi affrontati sia per scelta, singolare ai tempi, di proporre personaggi femminili come protagonisti in un mercato in cui la figura femminile era relegata ad un ruolo secondario. Il successo è enorme: nel 1983 viene realizzata dalla Tokyo Movie Shinsha un anime che andrà in onda in Italia nel 1985 e sarà trasmesso in Francia, Spagna, Germania, Brasile, Cina e Filippine. Nel 1985 debutta su Shonen Jump la serie investigativa City Hunter. Il successo in patria è ancor maggiore di quello di Occhi di Gatto e consacra definitivamente Hojo come disegnatore di riferimento per il fumetto nipponico. Nel 1987 lo studio Sunrise realizza una serie anime di City Hunter che sarà esportata in decine di paesi. Nel 2001, sulla base del consolidato successo di City Hunter, Hojo realizza Angel Heart, uno spin-off della serie ambientato in un futuro parallelo. Anche per Angel Heart viene realizzata una serie anime dalla TMS Entertainment, trasmessa in Giappone dal 2005 al 2006, attualmente inedita in Italia.  
第23届Romics将向伟大的漫画家北条司颁发Romics d'Oro终身成就奖。 北条是《猫眼》、《城市猎人》和《天使心》系列的著名作者，由于他与一个渴望成为专业漫画家的伙伴之间的友谊，他从小就对漫画充满热情。1980年，他以《我是男子汉！》作为职业漫画家在《少年Jump》上出道。之后，他开始与堀江信彦密切合作，并于1981年出版了《猫眼》。这部作品以三姐妹的冒险经历为中心，讲述了三个娴熟敏捷的盗贼在追寻艺术品的过程中的故事，立即征服了公众，使作者立即受到欢迎，这既是因为所处理的主题，也是因为作者选择了在当时不同寻常的方式，在女性形象被贬为次要角色的市场中提出女性角色作为主角。取得了巨大的成功：1983年，东京电影新社制作了一部动画，该动画于1985年在意大利播出，并在法国、西班牙、德国、巴西、中国和菲律宾播出。1985年，侦探系列《城市猎人》在《少年Jump》上首次亮相。在本国取得的成功甚至比《猫眼》还要大，并且可以肯定地把北条信奉为日本漫画的标杆。1987年，Sunrise工作室制作了《城市猎人》动画系列，出口数十个国家。2001年，基于《城市猎人》的成功，北条创作了以平行世界为背景的系列衍生作品《天使心》。另外，《天使心》的动画系列是由TMS娱乐公司制作的，该系列于2005年至2006年在日本播出，目前尚未在意大利发行。
     

>>    Tsukasa Hojo è famoso per il suo stile caratterizzato dal tratto elegante e raffinato e da un elevato realismo: pur seguendo l’impostazione classica del fumetto giapponese, i suoi personaggi sono caratterizzati da tratti per lo più occidentali. Contenuti drammatici, profondità delle situazioni che spesso analizzano l’animo umano nei suoi lati più complessi e una buona dose di umorismo lo rendono adatto a un pubblico adulto.  
北条司以其优雅、精致的笔触和高度的现实主义风格而闻名：在遵循日本漫画的经典方法的同时，他的角色大多具有西方特色。 戏剧性的内容，深入浅出的情境，往往能分析出人类灵魂最复杂的一面，加上良好的幽默感，使其适合成年观众观看。 
     

>>    La XXIII edizione di Romics celebra l’artista giapponese con l’assegnazione del Romics d’Oro alla carriera e con una mostra monografica che esporrà tavole e illustrazioni delle sue opere principali: Occhi di Gatto, City Hunter, Angel Heart. Hojo sarà inoltre protagonista di un incontro al Pala Romics per ripercorrere e festeggiare la sua lunga carriera di grandi successi internazionali.  
 第23届Romics展通过授予Romics d'Oro终身成就奖和一个专题展览来庆祝这位日本漫画家，该专题展览将展示他的主要作品的图画和插图：《猫眼》、《城市猎人》、《天使心》。 北条还将是Pala Romics会议的主角，回顾并庆祝他在国际上取得巨大成功的漫长职业生涯。


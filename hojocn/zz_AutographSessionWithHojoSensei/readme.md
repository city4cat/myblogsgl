([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96793))  
(图片缩小了，如果需要大图，可访问原链接：https://www.manga-audition.com/manga-insider-mayu_21/)


![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSC_0075.jpg)


MANGA INSIDER MAYU #21 – Autograph Session With Hojo Sensei!

北条司签售会(2015.07.25)

Mayuna Mizutani, 04/08/2015, 6 min read

Hello everyone!

大家好！


After a 1 week break, Manga Insider is back with episode 21!

休息1周后，Manga Insider的第21集又回来了！


Today’s theme is… “Autograph sessions”!

今天的主题是…“签售会”！


On 25th July 2015, an autograph session by Hojo sensei was held in Shinjuku, Tokyo.

2015年7月25日，北条老师在东京新宿举行了签售会。


Autograph sessions are a special opportunity for the fans to meet their favorite manga artist face-to-face! For them, this is an once-in-a-lifetime chance, so we want everything to go perfectly!

签售会是粉丝们与他们最喜欢的漫画家面对面交流的一个很特别的机会！ 对于粉丝来说，这是千载难逢的机会，所以我们希望一切顺利！


What happens exactly? What role do the editors play?
Read on to find out!


到底会发生什么？ 编辑担当什么角色？
仔细阅读就会找到答案！


The autograph session was held at a famous bookstore near Shinjuku station, which regularly plays host to these events.

签售会在新宿站附近的一家著名的书店举行，该书店定期举办这些活动。


Look at that full schedule!

看完整的时间表！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0251.jpg)


The 25th was Hojo-sensei’s event. The following day was an event for Abi Umeda, creator of ‘Kujira no Kora wa Sajou ni Utau’. In August, they’re hosting the BL artist Mariko Nekono, followed by the creators of ‘Trigger’, Toshiyuki Itakura (also a comedian & member of the comic duo “Impulse”) and Yuji Takemura. And finally, Ami Sugimoto, creator of ‘Fantasium’.

25日是北条老师专场。 第二天是《Kujira no Kora wa Sajou ni Utau》的作者Abi Umeda的专场。 8月，将主持BL漫画家Mariko Nekono的活动，然后是《Trigger》的作者Itakura Toshiyuki（也是喜剧演员和漫画《Impulse》的成员）和Takemura Yuji。 最后，《Fantasium》的创造者杉本亚美。


(By the way, Yuji Takemura also drew the 1st and 2nd episodes of Gifuu DouDou!!)

（顺便说一句，Yuji Takemura也画了Gifuu DouDou的第一集和第二集!!）


The arrangement is a team effort by the bookstore staff, the editors and the sales team. The last 30 minutes before the session is especially busy, with a flurry of activity to get everything ready!

这是书店员工、编辑和销售团队共同安排的。 开始前的最后30分钟特别忙，要忙各种事情使得一切就绪！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0258.jpg)


We can see many signs of consideration during the session.

活动期间，可以看到许多事情考虑得很周到。

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0263.jpg)


On the left is a piece of paper that’s inserted into the book after the artist signs it. This is so that the ink doesn’t stain any of the other pages. We call this “Ategami”.

左边是一张纸，供漫画家签名后插入书中。 这样一来，墨水就不会弄脏其他页面。 我们称其为“ Ategami”。


On the right are postcards for the fans. There’s Ryo (City Hunter) and Xiang Ying (Angel Heart). These are a special thanks to the fans for buying the manga, and taking the time to come to the autograph session.

右边是为粉丝准备的明信片。 有獠（城市猎人）和香莹（天使心）。 特别感谢粉丝们购买漫画，并有时间参加签售会。


The walls and partitions are also covered in artwork so that the fans don’t get bored while they wait!

墙壁和隔板也贴有作品，以免粉丝们在等待时感到无聊！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0277.jpg)

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSC_0081-1280x857.jpg)

These beautiful flowers were sent by Haruto Umeza-sensei, a former assistant of Hojo-sensei, who is currently drawing a serial for Grand Jump.

这些美丽的花是由Haruto Umeza老师(梅泽春人？)送来的，他曾经是北条老师的助手，目前他在Grand Jump上连载作品。

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0269.jpg)


There was also this table laid out.

这张桌子也摆好了。

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0267.jpg)


These are volumes 1 & 2 of the new CITY HUNTER XYZ Edition. We have to keep the fans up to date with the latest releases!

有新版的CITY HUNTER XYZ的第1卷和第2卷。我们必须让粉丝知道有最新的发行版！


Take a peep outside… look how many people showed up!

撇一眼……看看来了多少人！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0294.jpg)


Among them, there was even a fan who came all the way from Singapore! There really are no international barriers when it comes to manga!

其中，甚至有一个粉丝从新加坡赶来！ 对于漫画，国与国之间就没有障碍！


While waiting to meet Hojo-sensei, the fans filled in surveys, which will hopefully become valuable data for the marketing team!

在等待与北条老师见面时，粉丝们填写了调查问卷，这些问卷对营销团队来说很重要！


These character cutouts and posters were also placed outside.

这些人物剪纸和海报也放在外面。

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0255.jpg)


They’re not only eye-catching, but also give the event a festive feel.

他们不仅引人注目，而且使活动充满节日气氛。


Of course, everyone wanted to take lots of photos, and these were a popular choice!

当然，每个人都想拍摄很多照片，这是一个不错的选择！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0284.jpg)


We also had a nice selection of City Hunter goods for sale!

我们也有精选的City Hunter待售商品！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0288.jpg)


Any City Hunter fan will definitely want to make a few purchases.

无论哪个City Hunter迷，肯定是要买一些的。


Finally, the event begins!

终于，活动开始了！


Japan is currently in the grip of a sweltering heatwave, with temperatures of up to 35℃. Even in the shade, that’s no joke!

日本目前被热浪袭击，温度高达35摄氏度。 即使在阴凉处，这也不是闹着玩的！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSC_0073.jpg)


We’re making the fans wait in such harsh conditions… we’ve got to manage the queue real quick!

粉丝们正在酷暑下等侯着……我们必须得让队排得短一些


For ease of management, we divide them into small groups of 5-10 people, and call in each group, one at a time. But even with the air-con running at full-blast, with all these people crammed in the room, it’s hot!

为了便于管理，我们将他们分成5-10人的小队，一次招进一队。 但是，即使空调开到最大，所有这些人挤在房间里，还是很热！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0300.jpg)


During an autograph session, the editors stay close to the artist, and assist him in any way they can. Today, Mr. Tanaka and Mr. Watanabe were on duty.

在签售会上，编辑与紧挨着漫画家坐，尽可能地帮一些忙。 今天，轮到了田中先生和渡边先生。

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0316.jpg)


To be specific, the editors take each fan’s book and ticket, insert the “Ategami” after Hojo-sensei has signed the book, and then pass the signed book and postcards back to the fan.

具体来说，编辑会拿着粉丝的书和票，在北条老师签名后插入“ Ategami”，然后将签名的书和明信片传给粉丝。


You might think that the artist could handle that himself, but we want the artist free to concentrate on signing and interacting with the fans. If the editors aren’t there to help, the desk might get very messy!

你可能会认为漫画家可以自己解决这个问题，但我们希望漫画家全身心地专注于与签名以及和粉丝互动。 如果没有编辑来帮忙，办公桌可能会变得非常混乱！


During the session, we received a surprise visit from Kazue Ikura, the voice actress who plays the role of Kaori in the City Hunter and Angel Heart anime! She came in hidden among the fans, carrying a large bouquet of flowers! Even Hojo-sensei was surprised!

在活动期间，我们见到了在城市猎人和天使心动画中香的声优仓村一休（Kazue Ikura）！ 她藏在粉丝中间，捧着一大束鲜花！ 甚至北条老师也感到惊讶！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSC_0087.jpg)


They seem truly glad to see each other!

他们见到对方似乎都很高兴！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSC_0093.jpg)


The editors were hard at work in the background!

编辑们在后台忙碌！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0305.jpg)


The writer in charge of doing an article on this autograph session, and Mr. Yamanaka from the ZENON editorial team were both snapping away, taking pictures to record the event.

负责在签售会上撰文的作家，以及ZENON编辑团队的Yamanaka先生都被抢购一空，拍照留念以记录此次活动。


As this year is also the 30th anniversary of City Hunter, there was a poster prepared for the fans to write messages of congratulations. 

由于今年也是“城市猎人”30周年纪念，因此我们为粉丝准备了一张海报，供他们写下祝福。

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0307.jpg)


Several members of the editorial team encouraged the fans to leave messages for Hojo-sensei. Above, you can see Ms. Ikura writing a message on Kaori’s signature 100t hammer!

编辑团队的几位成员鼓励粉丝为北条老师留言。 在上方，您可以看到Ikura女士在香标志性的100t锤子上面写了一条信息！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0321.jpg)


Visitors from overseas, fans almost dropping from the heat, a mother with a baby car, Ms. Ikura’s surprise visit… you never know what’s going to happen, but everyone was on the ball, and the event was a resounding success! 

来自海外的粉丝，不顾炎热赶来的粉丝，推着婴儿车的母亲，Ikura小姐的意外来到……你不知道会发生什么，但是所有人都专注其中，活动很成功！


The autograph session ran for a total of 2 hours, during which Hojo-sensei was signing books the whole time! Thank you Sensei!

签售会总共进行了2个小时，在此期间，北条老师一直在为书签名！ 谢谢老师！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSC_0075.jpg)


Just like the setup, the cleanup is also a team effort by the bookstore staff, editors and sales team.

就像事前准备一样，事后的清理工作也由书店员工、编辑和销售团队共同完成。

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0324.jpg)


They left everything neat and tidy, and ready for the next event! 

他们把所有东西都整理得整齐整齐，为下一场活动做好了准备！


During the autograph session, the fans left lots of gifts!

在签售会上，粉丝们留下了很多礼物！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0322.jpg)


Don’t worry, they were safely delivered to Hojo-sensei!

不用担心，它们已经安全地送到了北条老师那里！


This kind of autograph session isn’t limited to successful manga artists like Hojo-sensei. New artists also often hold them to promote their work.

这种签售会不仅限于成功的漫画家，例如北条老师。 新人漫画家也经常举行活动来推销自己的作品。


Since we live in a time when the publication industry is said to be in decline, it’s important to have events which motivate people to buy books!

由于我们生活在一个出版业正在衰落的时代，因此举办这样的活动促使人们买书非常重要！


When I was small, I received an autographed copy of my favorite children’s book, and I still like to page through it from time to time. Having a book signed by the original author is definitely something special. If everyone who attended the autograph session left with a happy memory in their hearts, then it was definitely worth holding this event.

当我小的时候，我收到了最喜欢的儿童读物的亲笔签名，我现在还不时从头看到尾它。 由原作者签名的书绝对是非常珍贵的。 如果参加签售会的人心中都留下了美好的回忆，那么这次活动绝对是值得的。


Finally, I’d like to show you the message left by Hojo-sensei on the event’s poster.

最后，我想向您展示北条老师在活动海报上的留言。

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_AutographSessionWithHojoSensei/img/mim21_DSCN0320.jpg)


“30 years have passed, but there are still people who are supporting me. I’m so grateful, and so glad that I drew this series.”

“ 30年过去了，但是现在仍有人支持我。 我很感激，也很高兴我画了这个系列。”


The artist loves his fans, and the fans love the artist… they both support each other. Isn’t it beautiful to have a relationship like this?

漫画家喜欢他粉丝，粉丝也热爱漫画家……他们俩互相支持。 这样的关系不是很美吗？


Have you ever been to an autograph session?
Do you have any ideas for a great event?

你去过签售会吗？
你对重大活动有任何想法吗？


Keep those opinions and comments coming!

请发表您的意见和评论吧！


[catlist name=”MANGA INSIDER MAYU” thumbnail=”yes” thumbnail_size=160 content=”no” date=”yes” dateformat=”M j, Y G:i” template=page_list]

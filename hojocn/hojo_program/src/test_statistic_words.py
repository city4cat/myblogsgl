''' 
Count the Chinese characters in files located in a directory (and its subdirectories).

Usage:
python3 ./test_statistic_words.py

'''

import os
import time

import util_logging
import util_common
import util_dir_file_filter as uf


def chinese_char_count_in_string(string_data):
    count_cn = 0
    for s in string_data:
        if '\u4e00'<= s <='\u9fef': # for (often used) Chinese characters
            count_cn += 1
            
    return count_cn


def statistic_words_in_files(filepath_list):
    count = 0
    
    for filepath in filepath_list:
        with open(filepath, 'rb') as f:
            content = f.read().decode()
            count += chinese_char_count_in_string(content)
            
    return count
            

def statistic_words_original(root_dir):
    
    # ------------------------------------
    include_filters_list = ['*.md', '*.txt']
    exclude_filters_list = []
    exclude_dir_patterns = ['zz_', 'tmp']
    exclude_file_prefix = []
    # ------------------------------------

    util_logging.info('1 ----------------')
    filepath_list0 = \
    uf.test_gather_files(
    root_dir = root_dir,
    include_filters = include_filters_list,
    exclude_filters = exclude_filters_list
    )
    filepath_list0.sort()
    uf.print_list(filepath_list0, 'filepath_list0', show_details=False)

    util_logging.info('2 ----------------')
    filepath_list1, excluded_list = uf.exclude_dir(filepath_list0, exclude_dir_patterns)
    uf.print_list(excluded_list, 'excluded dirs', show_details=False)
    
    filepath_list2, excluded_list = uf.exclude_files_by_prefix(filepath_list1, exclude_file_prefix)
    uf.print_list(excluded_list, 'excluded files', show_details=False)    
    
    uf.print_list(filepath_list2, 'original files', show_details=True)    
    count = statistic_words_in_files(filepath_list2)
    
    info = 'In original articles, Chinese characters count: %f(10K)'%(count/10000)
    
    return info
    
    
def statistic_words_zz(root_dir):
    include_filters_list = ['*.md', '*.txt']
    exclude_filters_list = []
    exclude_dir_patterns = ['ce_','ch_','ss_','fc_','ah_', 'ah2_', 'hojo_', 'tmp']
    exclude_file_prefix = []
    # ------------------------------------

    util_logging.info('1 ----------------')
    filepath_list0 = \
    uf.test_gather_files(
    root_dir = root_dir,
    include_filters = include_filters_list,
    exclude_filters = exclude_filters_list
    )
    filepath_list0.sort()
    uf.print_list(filepath_list0, 'filepath_list0', show_details=False)

    filepath_list1, excluded_list = uf.exclude_dir(filepath_list0, exclude_dir_patterns)
    uf.print_list(excluded_list, 'excluded', show_details=False)

    filepath_list2, excluded_list = uf.exclude_files_by_prefix(filepath_list1, exclude_file_prefix)
    uf.print_list(excluded_list, 'excluded', show_details=False)   
    
    uf.print_list(filepath_list2, 'zz files', show_details=True)   
    count = statistic_words_in_files(filepath_list2)
    
    info = 'In ZZ artiles, Chinese characters count: %f(10K)'%(count/10000)
    
    return info


def test_main():
    print('Start >>>')

    os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'
    util_logging.setup()

    username = os.path.expanduser('~').split('/')[-1]

    root_dir='/media/%s/Traveller/usb/dev/myblogsgl/hojocn'%(username)
    #root_dir='/media/%s/Traveller/usb/dev/myblogsgl/hojocn/zz_htkj_cn'%(username)
    #root_dir='/media/%s/Traveller/usb/dev/myblogsgl/hojocn/zz_www.biglobe.ne.jp_julianus'%(username)
    #root_dir='/media/%s/Traveller/usb/plan/src/test_dir'%(username)
    assert os.path.isdir(root_dir), root_dir

    info0 = statistic_words_original(root_dir)

    info1 = statistic_words_zz(root_dir)
    
    print('=============== Information ===================')
    print('root_dir=', root_dir)
    print(info0)
    print(info1)
    
    print('<<< End')
    

if '__main__' == __name__:

    test_main()

    
    
    

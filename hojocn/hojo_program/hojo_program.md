
# 北条司作品研究纲领(A Study Program For The Work Of Hojo Tsukasa)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96789))  


## 动机

- 想回答如下问题：  
    - 1.都说北条司的作品好，究竟好在哪里？（详见[北条司作品里的优点](./hojo_good_elements_in_the_works.md)）  
    - 2.北条司的成功能否被复制？（详见[北条司及其作品之编年史](./hojo_annals.md)）    

- 我个人精力有限。所以，把这个计划发布出来，有兴趣的人可以参与进来做其感兴趣的内容。


## 方法

以文学创作理论为例。文学创作已经有一套成熟的理论，它告诉人们故事怎样讲出来能够吸引人。通过学习文学创作理论，然后套用到北条司的作品上，我们或许可以理解“为什么北条司这个作品里的某些故事、某些人物会如此吸引人？”。

同理，把电影镜头理论套用到的北条司的作品上，我们或许可以理解“为什么北条司这个作品里的某些画面会如此吸引人？”。

同理，对于下面计划里提到的绘画理论、漫画创作理论、音乐创作理论等，把这些理论套用到北条司的作品上，或许能更好地理解和欣赏北条司的作品。

此外，可以把其他的小说、漫画、电影拿来和北条司的作品做比较。看看两者有何异同；比起北条司的作品，它们是否有更高明之处。  


## 计划

这仅是我个人暂时的一些想法，欢迎大家提供各种想法。  
- 对于北条司的某一部作品，可以考虑以下事情：  
    - 从编剧的角度分析该作品（类似分析一部小说[1]）           
        - 分析作品的社会背景。社会背景对该作品的影响[1]。可参考：            
            - Modern Japan A Historical Survey(5th edition, 2012), Mikiso Hane, Louis G. Perez            
            - The Historical Consumer Consumption and Everyday Life in Japan, 1850–2000 (2012), Penelope Francks, Janet Hunter,              
            - 现代日本史-从德川时代到21世纪(2017), 安德鲁·戈登 著, 李朝津 译            
        - 如何分析作品里的文本[1]，包括但不限于:  
            - 作品的结构、作者的情感（包括：生活情感、审美情感、不同时期的情感，等）              
            - 作者的心理结构、当时的社会心理              
            - 台词的本义、衍生义、想象。  
            - 由某个台词所触发的读者的感受              
            - 作品的意义[1]              
            - 在翻译上，中文版有没有错误或不妥之处  
            - 有没有可改进之处？              
        - 分析故事线          
            - 有没有可改进之处？  
        - 分析人物          
            - 有没有可改进之处？  
        - 分析作品中的幽默  
        - 分析文字量（即，如果改写为小说，大概有多少字?）  
    - 如何从电影的角度分析该作品，包括但不限于：      
        - 镜头语言          
            - 有没有可改进之处？  
        - 由某个镜头所触发的读者的感受          
    - 如何从绘画的角度分析该作品，包括但不限于：      
        - 服饰  
        - 表现技巧          
            - 有没有可改进之处？          
    - 如何从漫画的角度分析该作品，包括但不限于：      
        - 连载的时间          
        - 每个故事的长度（镜头数）          
        - 每页镜头数          
    - 如何从音乐的角度分析该作品（从北条司的一篇访谈里[2]，我猜测北条司作品里用到了音乐的创作方法）        
    - 如何从戏剧的角度分析该作品（从北条司的一篇访谈里[2]，我猜测北条司作品里用到了戏剧的创作方法）      
    - 如何从其他角度分析该作品      
        - 作品里的隐藏彩蛋  
    - 作品里的事物和现实世界的对应。作品里的事物(衣、食、住、行、人名、情节等等)是否在现实世界里有原型。这些原型是否有寓意、是否影响了作品?比如，FC里雅彦的姓氏“柳叶/Yanagiba”，可能和日本(首家？)同志酒吧“柳木/Yanagi”有关[4]；熏曾经的乐队名"失乐园"可能和陈淑桦有关[4]。FC的考证正在进行中[3, 4]。  
    
    
- 以上是分析某一个作品。当分析了北条司的多部作品后，对比这些不同作品之间的异同，或许可以发现北条司作品的创作变化历程（比如，画风的变化）。  


## 参考资料

1. [读书笔记:谈小说的分析方法](https://zhuanlan.zhihu.com/p/27645756)

2. [北条司访谈–“直到最后一刻”（"Until the very last moment"）](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_until-the-very-last-moment/readme.md) 北条司提到：“仅靠看漫画是无法成长的。最好能体验各种各样的媒体形式，例如电影，音乐，小说，戏剧……如果没有这些媒体形式，漫画家的世界将会萎缩，并且会不断地萎缩下去。”

3. [FC实物集](https://gitlab.com/city4cat/myblogs/-/blob/master/hojocn/fc_things_in_real_world/readme.md)

4. [FC的一些细节](https://gitlab.com/city4cat/myblogs/-/blob/master/hojocn/fc_details/readme.md)

5. [北条司访谈系列 专辑一](http://www.hojocn.com/bbs/viewthread.php?tid=3917)  
6. [北条司访谈系列 专辑二](http://www.hojocn.com/bbs/viewthread.php?tid=4047)  
7. [老北一篇少见的访谈](http://www.hojocn.com/bbs/viewthread.php?tid=2869)  
8. [北条的专访](http://www.hojocn.com/bbs/viewthread.php?tid=2587)  

 

# Todo List  

- 搬运twitter 上北条司相关的信息(文字/图片)。  
暂时在这帖子里做个尝试: http://www.hojocn.com/bbs/viewthread.php?tid=96791  
Twitter上几乎每天都有关于北条司的新的信息，有的是官方账号发的消息，有些是粉丝自己发的tag 。天下漫迷是一家，但因为被墙，所以不方便看这些信息，也无法和他们交流，真太让人悲伤了。可以搬运自己感兴趣的信息。因为翻墙很不方便，所以，搬运还是要花不少时间的。  

- 分享论坛所没有的那些与北条司及其作品相关的各种资料和信息。时隔久远，当年的一些网站都无法访问了、当年很容易找到的资源和信息，现在都找不到了。可能这些信息当年被你们保存下来了，请分享出来吧。  

- 翻译类:  
(虽然可以用在线翻译，但总归还是要有人付出时间和精力，还需要校对人名等等。所以我觉得翻译这些信息还是有意义的。CatNj很多年前就提到过可以组织人手翻译北条司相关信息的事情，时至今日我很佩服ta的远见。)  
    - 翻译”北条司研究序说(Hojo Tsukasa Kenkyu Josetsu)”。  
        网址: http://chiakik.la.coocan.jp/htkj/start.htm。可以挑选自己感兴趣的章节翻译。  
    - 翻译: http://ryosaeba.canalblog.com/。网站有些文章还挺有趣的，可以挑选自己感兴趣的文章翻译。  
    - 翻译: https://xyzcityhunter.wordpress.com。网站有些文章还挺有趣的，可以挑选自己感兴趣的文章翻译。  
    - 翻译twitter上搬运来的信息(文字/图片)。  
    - 翻译Julianus的文章  
        http://www5f.biglobe.ne.jp/~julianus/support.htm  
        现在可访问的连接是：
        https://web.archive.org/web/2005 ... ulianus/support.htm  
        这个博主不仅考据了FC，还考据了北条司的其他作品  
        （相关帖子：http://www.hojocn.com/bbs/viewthread.php?tid=96790 "求助，当年日本一个专门关于FC的网站，现在无法访问了"）  

- 北条司作品人物相貌 与 古典油画里人物相貌 的联系  
- 《北条司拾遗集 35th周年纪念》文字录入  
- 用人体解剖素描的方法分析北条司作品中的人体绘画。  
- Kinesiology这个领域的研究方法能否用来分析北条司作品中的人体绘画，比如跳舞、跑步等动作。  
- [獠的原型? - HTKJ](../zz_htkj_cn/ch/chmodel.md)里提到北条司最喜欢的5本书。或许这能考证北条司的编剧。  
- 《漫画王国的崩坏》(by 西村繁男)。或许这能考证北条司的创作经历。  
- 《再见了我的青春-少年Jump》(by 西村繁男)，以及由该书改编的漫画《少年Readom》(或《爆漫编辑》)(by 次原隆二)。或许这能考证北条司的创作经历。(信息来源：专栏[日漫里的满地坑](https://zhuanlan.zhihu.com/c_1262011326710067200)里的文章[西村繁男到鸟岛和彦，周刊少年Jump编辑部的创作路线斗争](https://zhuanlan.zhihu.com/p/380023020)  )  


# Finished List  
- FC素材库, (http://www.hojocn.com/bbs/viewthread.php?tid=96741 )。已完成一个版本。需进一步更新，目前更新至03_075_3。    
- FC表情包。(./hojocn/fc_expression)  




--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



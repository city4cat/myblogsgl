（如果图片或格式有问题，可以访问[这个链接](https://gitlab.com/city4cat/myblogs/blob/master/hojocn/fc_surname_vs_jap_surname/readme.md)）


# FC里的人物姓名与日本姓氏  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96745))  

偶然的机会发现FC里的个别人名和日本姓氏有些相似，但又不同。于是整理了一些。个別人名暂时參考了老公寓里的[一张图](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_surname_vs_jap_surname/fc_roles_names.jpg) 

##### 格式：
- FC里的人物姓名 （**黑体表示真实存在的姓氏**）
    - 近似的真实的日本姓氏

##### 人物姓名与其近似的日本姓氏
- 若苗Wakanae 
    - 若菜Wakana 
- 紫苑Shion
    - 志藤/紫藤Shito 
    - 鹽野Shiono
    - 鹽谷/鹽野谷Shionoya  （男装时，在叶子面前紫苑自称塩谷（11卷011页））
- 空Sora（空结婚前的名字：菊地Kikuchi 春香Haruka ）
    - 空谷Kukoku
- 紫Yukari
    - 若菜Wakana 
    - 村崎/紫Murasaki
    - 湯川/湯河Yukawa
- 龙彦Tatsuhiko (紫结婚前的姓名：若苗Wakanae 龙彦Tatsuhiko)
    - Tatsukawa 達川
    - Tatsuke 田附/田付
    - Tatsukuchi 辰口
    - Tatsumatsu 辰松
    - Tatsumi 辰已/巽/立見/田積
    - Tatsumura 龍村
    - Tatsuno 立野/辰野/龍野
    - Tatsuoka 辰岡
    - Tatsusaki 龍崎
    - Tatsuta 龍田/立田

- 柳叶Yanagiba
    - Yanagi 柳
    - Yanagibashi 柳橋
    - Yanagida 柳田
- 雅彦Masahiko，雅美Masami
    - Masada 正田
    - Masago 真砂
    - Masai 正井。「木正」(木正是一個字，左邊為「木」字旁，右邊為「正」)井
    - Masaki 正木/「木正」木/政木
    - Masamori 正森
    - Masamune 正宗
    - Masamura 正村
    - Masaoka 正岡/政岡
    - Masata 政田
    - Masatomi 正富
    - Masayama 當山

- ？？ ？？ （雅彦的妈妈）
- ？？ ？？ （雅彦的爸爸）

- **浅冈Asaoka** 叶子Yoko(Youko)  
    - Asaoka 淺岡/朝岡


- **菊地**
    - Kikuchi 菊池/菊地


- ？？ （空的爸爸：菊地 ？？ ）
- 节子 （空的妈妈：菊地 节子）

- 奥村

- 顺子Yoriko （空的妹妹：奥村 顺子Yoriko）
    - Yorifuji 依藤
    - Yorishima 庵島
    - Yorita 依田
- 宪司Kenji （空的同学：奥村 宪司Kenji）
- 章子Shoko （宪司的女儿：奥村 章子Shoko）

- **中村Nakamura** 浩美Hiromi （空的助手，本名: Nakamura中村 Mitsuhiro光浩）
    - Nakamura 中村/仲村
    - Hiromasa 廣政
    - Hiromoto 廣本
- 新潟Niigata 和子Kazuko （空的助手）(本名: Niigata新潟 Kazuto和人)
    - Nishikata 西方/西形/西片/西潟
    - Nii 新居/新/丹生/丹/二井/仁井
- **山崎Yamazaki** 真琴Makoto （空的助手）

- **横田Yokota** 进Susuma（空的助手）
    - Shin 秦/新/進/心
    - Suzuki 鈴木/鈴樹/鐸木/鈴記/鈴紀/鈴杵/鈴置/鈴城/錫木/須須木/壽壽木/周周木/壽洲貴/壽松木/進來
    - Susukida 薄田
- ？？ **森Mori**（督促空工作的女编辑）
    - Komori 小森/古森/小守
    - Mori 森/盛/守/毛利

- 真朱Masoba 薰Kaoru
    - Masaoka 正岡/政岡
    - Masada 正田
- **辰四Tatsumi**（薰的爸爸：真朱Masoba 辰四Tatsumi）
    - Tatsumi 辰已/巽/立見/田積
- **早纪Saki**（薰的妈妈：真朱Masoba 早纪Saki）
    - Saki 前。（saki和其他字母組合後，多翻譯為“崎”）

- 卓也Takuya **江岛Ejima** （雅彦的同学）
    - Takubo 田窪/田久保
    - Takuma 宅間/詫間/宅磨
    - Takura 田倉
    - Takusagawa 田草川
    - Takusari 田鎖

- **藤崎Fujisaki** 茜Akane（雅彦同学）
    - Fujisaki 藤崎
- **仁科Nishina** 耕平Kohei （迷恋雅彦的人）
    - Nishina 仁科
- ？？ 美葵Aoi （喜欢辰巳的人）
    - Aoi

- 浅葱Asagi 蓝（紫苑的初恋）
    - Sasagi 佐佐木

- **齐藤Saito** 玲子Reiko (紫苑(小学男装)是她的初恋)    - 

- Hanzu八不 Kyoko京子 (紫的中学插花社的成员)
    - Hanyu 羽生
    - Hanzawa 半澤/榛澤
    - Kyogoku 京極
    - Kyono 京野
- **Isaka井阪** Kyoko京子（八不京子结婚前的姓名）
香港玉皇朝高清版里的字迹像是“岸田”（第2卷171页），即：
  **Kishida岸田** Kyoko京子



-----------------

- **柳叶Yanagiba 雅彦Masahiko**  
    - '彦'字的日文解释：男子的美称（与'姬'(女子的美称)相对）。应该是一个常见名，比如，井上雄彦、堀江信彦。  
    - 作者北条司为雅彦取的姓氏"柳叶(Yanagiba)"可能与下面的历史[11]有关：  
    "1945 – Gay bar Yanagi opened in Japan"  
    (日本（首家？）同性恋酒吧"柳木(やなぎ(ゲイバー)，英文：Yanagi)" 于1945年开业)  


- **若苗Wakanae 紫苑Shion**  
    - FC第2卷卷首语：“查看字典，"紫"这个字不念做"缘"这个读音，但是，我始终很想把她(?)的名字些作"紫"而念做"缘"。只因，在《日本语大辞典》中，有记载紫色亦称“缘之色”，词典中解释的意思是“比喻因某一种关系的缘故，使爱情有其他的演变”。我想这正好跟紫的性格吻合，所以把她的名字改成"紫"而读作"缘"...”。我猜，紫苑的名字会不会和此有关：紫缘-->紫苑（“苑”和“缘”的发音近似） 
    - [关于"紫苑"这个名字](./zz_shion_jianshu/zz_shion_jianshu.md)  
    - [紫菀？紫苑？——灵魂燃烧！](https://www.jianshu.com/p/c04d26ca68c)    


- **若苗Wakanae 空Sora**   
北条司的短篇《蔚蓝长空》创作于1995年，其故事原型(参考文献[9])里提到高知县空军(简称高知空)(详见[10])。FC创作于1996年，"若苗 空"的名字里也有“空”，其家乡也在高知。不知这两者是否有联系。  


- **浅冈Asaoka 叶子Yoko**  
叶子姓氏“浅冈”。叶子家在奥多摩，据[12]猜测具体地点可能是"御岳渓谷"，门前的河流是"多摩川"。此外，有[多摩川浅間神社(Sengen Shrine)](https://pixta.jp/photo/64779711)，在多摩川的下游，距离御岳渓谷直线距离约50km。 不知道叶子的姓氏"浅冈"和浅間神社是否有关系。


- **森Mori**  
森是若苗空的编辑。[13]提到：  
"Among his former assistants there were Inoue Takehiko, Umezawa Haruto & Yanagawa Yoshihiro. Morita Masanori applied as his assistant, but he introduced Morita to HARA Tetsuo instead as he already had enough assistants at that time."  
(他(北条司)之前的助手有 井上雄彦(Inoue Takehiko)[14]，梅泽春人(Umezawa Haruto)，柳川喜弘(Yanagawa Yoshihiro)。森田真法(Morita Masanori) 曾应聘当他(北条司)的助手，但他(北条司)把 森田 介绍给了 原哲夫，因为他(北条司)当时的助手已经满了。)   
所以，我推测FC里的编辑 森的名字可能源于 森田真法[15]。    
    

---------------

- 14_215，提到爷爷和空分别要给新生儿取名为“龙之介”、“龙马”，其他人觉得这两个名字都很糟糕。  
之所以有“龙”字，可能与2000年是龙年有关（但此时FC的剧情应该是1999年8月份）。日语里龍是旧字体，竜是日本的简化字[4]。  
    - 爷爷给新生儿取名"龙之介"。“之介”是日本常用名。20世纪初，"之助"、“之介”在正式日本人名中大量出现。20世纪70年代后"之助"已经成为复古风名字的代表。[5]  
    - 空和给新生儿取名"龙马"，并说“生男孩子的话，始终是叫龙马最好! 这才像土佐男人的名字嘛！”。相关人物：   
        - 坂本龙马（明治维新的重要功臣，其家乡正是“土佐“(高知三旧称[6])。此外，高知还有其铜像和紀念馆[7]，所以此人是高知的历史名人，是高知人的骄傲。所以空提议的名字“龙马”，应该与此人有关）；  
        - 剑豪龙马（《海贼王》里的人物。）；  
        - 越前龙马（《网球王子》）。    



##### 參考链接：

1. [Table of Common Japanese Surnames](http://htmfiles.englishhome.org/Japsurnames/Japsurnames.htm)  
2. [日本常用姓氏表收集](https://www.douban.com/note/353116593/)  
3. [日本为何多达十三万种姓氏?](http://book.sina.com.cn/z/ribenxingshi/)  
4. [为什么日语里芥川龙之介的 龍 字不是 竜字， 日语里有龍这个字吗](https://zhidao.baidu.com/question/455131769590688325.html)  
5. [日本人名中的"之助"、“之介”是什么意思](https://zhidao.baidu.com/question/751964630780136084.html)  
6. [百度百科-土佐](https://baike.baidu.com/item/土佐)  
7. [【四國-高知】桂浜公園．日本名海岸百選．坂本龍馬銅像](https://www.travalearth.com/post-31405926/)  
8. [...]()       
9. [永末千里『かえらざる翼』](http://www.warbirds.jp/senri/08tubasa/tubasa.html)  
10. [解读《蔚蓝长空》](../ss_blue-sky/readme.md)  
11. [Timeline of LGBT history, 20th century](https://en.wikipedia.org/wiki/Timeline_of_LGBT_history,_20th_century)  
12. [FC实物集](../fc_things_in_real_world/readme.md)  
13. [北条司 - MangaUpdates](https://www.mangaupdates.com/authors.html?id=350)  
14. [柳川喜弘](https://www.mangaupdates.com/authors.html?id=12391)  
15. [森田真法](https://www.mangaupdates.com/authors.html?id=994)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处




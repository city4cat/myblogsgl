
**关于北条司的签名**  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96838))  

目前发现的北条司(Tsukasa Hojo)的签名有以下几种:

1. **Tsukasa +年月**  
   常见于漫画和插画。例如, FC里每话开篇的插画:  
	![](img/10_001_0__sign.jpg)  

2. **Tsukasa + 北条 + 年月日**  
   **Tsukasa + 北条 + 年**    
   **Tsukasa + 北条 + 季节**  
   常见于出席活动时的签名、给粉丝回信时的签名。例如：  
    - 出席熊本国际漫画祭(2017)时，现场作画的签名[^1]及视频[^2]:  
	![](img/signature_2017_spring.jpg)       
	![](img/signature_2019_04_07.gif)  

    - 粉丝收藏的签名[^3]。下图里能辨认出"北条"二字：  
    ![](img/signature_1983.jpg)  

    - 粉丝收藏的签名[^3]。  
    ![](img/signature_1984.jpg)  

3. **Tsukasa + 北条**    
   Cat's Eye 40周年纪念原画展(2022), 小册子里前言中的签名[^4]。能辨认出"北"字：  
    ![](img/signature_2022.jpg)  

综合这些签名，我对2中的签名辨认如下：  
![](img/signature_2017_spring.jpg) 
![](img/signature_2017_spring_mod.jpg)   


**参考链接**：  
[1] [熊本国际漫画祭本周举办 尾田、北条司等众多知名漫画家参与 - 178ACG](https://acg.178.com/201704/286049004430_s.html)  
[2] [今日北条司到熊本COAMIX漫画LAB现场绘画冴羽獠 - QQ视频](https://v.qq.com/x/page/e085865yho3.html)  
[3] [Planar6ele (関谷) - Twitter](https://twitter.com/planar6ele/status/1525744023037616130)  
[4] [猫眼三姐妹-城市猎人_官方 - 微博](https://m.weibo.cn/detail/4770088307131896)  

[^1]: https://acg.178.com/201704/286049004430_s.html  
[^2]: https://v.qq.com/x/page/e085865yho3.html  
[^3]: https://twitter.com/planar6ele/status/1525744023037616130  
[^4]: https://m.weibo.cn/detail/4770088307131896  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处





# 关于北条司最喜欢的5本书 ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96940))

北条司研究叙说(HTKJ)里的文章《獠的原型？》[^1]里提到了北条司最喜欢的5本书。这里是其中部分书的简介，以及它们和北条司作品的联系。    

- Flowers for Algernon（Daniel Keyes, 1966）
    - 一些信息： 中译名为“献给阿尔吉侬的花束”。科幻小说。这部小说是悲剧。    
    - 可能的联系： 北条司的作品有悲剧倾向[^2]。  


- 人はなぜ助平になったか（戸川幸夫著）  
    - 一些信息： 该书名的意思是"为什么人们会成为帮手"。未找到该书，但该作者有与之近似的作品『ヒトはなぜ助平になったか 性談動物記』（参见[戸川幸夫](https://ja.wikipedia.org/wiki/戸川幸夫)）  
    - 可能的联系：  


- Spenser Series（Robert B. Parker）  
    - 一些信息： "Spenser"中译为"斯宾塞"。小说塑造了著名侦探Spenser。Amazon上列出的["The Spenser"](https://www.amazon.com/dp/B075VG4WJG)系列中有50部作品。其中Robert Parker执笔的有40部，出版时间从1974年～2013年。未发现有中文版，部分作品有电子版。《Five Classic Spenser Mysteries》一书里收录了5部作品： A Catskill Eagle， Early Autumn， God Save the Child， The Godwulf Manuscript， Mortal Stakes。  
    - 可能的联系：  
        - Spenser会仔细打量所遇到的女性，比如她们是否漂亮、是否有气质[^1]。CH里的獠喜欢女人。    
        - 小说对食物的描述很详细。有关于Spenser三餐的食物的描述[^1]。CH、FC里都有大量用餐的画面。    
        - Spenser很自信，有好几次，当谈到女人时，他被描述为『只要给我这个微笑，我就是你的了』[^1]。CH里的獠很自信。    
        - Spenser曾是一名拳击手，手臂强壮，喜欢锻炼，肌肉像钢铁一样[^1]。CH里的獠很强壮，经常在香不在家时锻炼。    
        - 小说里的角色Hawk是一个黑人光头大汉，他后来成为Spenser的助手[^1]。CH里海怪肤色黝黑、光头、是獠的朋友。此外，两人名字也有相似之处：海怪的名字里有“隼”字，而隼(Falcon)和鹰(Hawk)是类似的猛禽。猛禽Falcon的飞行速度更快，猛禽Hawk的体格一般会更大[^3]。人们习惯把类似鹰的猛禽统称为"Hawk"[^5]。(关于隼(Falcon)、鹰(Hawk)、雕(Eagle)的详细比较可参见资料[^3] [^4] [^5] [^6] [^7] )。  


- Childhood's End（Arthur C. Clarke，1960）  
    - 一些信息： 中译名为“童年的终结”。科幻小说。小说讲述了被称作超主(Overlords)的外星人殖民地球的故事。
    - 可能的联系： 文中描述超主戴着墨镜："...(超主)已脱去了那件防护外套，但仍然戴着深色眼镜，虽说屋里的光线已经暗了不少。校长怀疑戴眼镜是否出于某种心理需要，或者只不过是一种伪装。人们本来就难以看懂超主的心思，戴了这副眼镜就更没有指望了。..."。北条司本人经常戴墨镜。  


- 七瀬Series（家族八景、七瀬ふたたび、エディプスの恋人）（筒井康隆）  
    - 《家族八景》（1975）  
        - 一些信息： 会读心术的女主角七濑以佣人的身份先后受雇于8个家庭。以她的所见所闻揭示了家庭关系中的种种丑恶。七濑的读心术不需要肢体接触，且可远距离实施。有同名的中文版。  
        - 可能的联系： 
            - 北条司的作品常常以家庭为主题，且都是温馨的故事。  
            - CH里的西九条紗羅会读心术，但需要肢体接触。  

    - 《七瀬ふたたび》（1978）。  
        - 一些信息： 未找到中文版。由作品改编的同名电影被译为"又见七濑"。
        - 可能的联系： 

    - 《エディプスの恋人》(1981)  
        - 一些信息： 著作名直译为"Oedipus的恋人"。未找到中文版。Oedipus(Odipus/Oidipous/俄狄浦斯)是欧洲文学史上典型的命运悲剧人物。是希腊神话中Thebe的国王Laius和王后Jocasta的儿子，他在不知情的情况下，杀死了自己的父亲并娶了自己的母亲[^8]。Oedipus情结又称作恋母情结[^9]。     
        - 可能的联系：  


## 参考资料：  
[1] [獠的原型？ - HTKJ](../zz_htkj_cn/ch/chmodel.md)   
[2] [欢迎来到猫之眼——动画版的可取之处——《猫眼三姐妹》番外3 - Bilibili](https://www.bilibili.com/read/cv15635629)  
[3] [Falcon Vs Hawk: Key Facts And Differences](https://www.seabirdsanctuary.org/falcon-vs-hawk/)  
[4] [隼(Falcon) - Wikipedia](https://en.wikipedia.org/wiki/Falcon),  
[5] [鹰(Hawk) - Wikipedia](https://en.wikipedia.org/wiki/Hawk),  
[6] [雕(Eagle) - Wikipedia](https://en.wikipedia.org/wiki/Eagle),  
[7] [鹰、鹫、隼、鵟、雕的区别 - Zhihu](https://www.zhihu.com/question/61280007)   
[8] [俄狄浦斯 - zhihu](https://zhuanlan.zhihu.com/p/386563029)  
[9] [恋母情结 - 百度百科](https://baike.baidu.com/item/恋母情结/574425)  


[^1]: [獠的原型？ - HTKJ](../zz_htkj_cn/ch/chmodel.md)   
[^2]: [欢迎来到猫之眼——动画版的可取之处——《猫眼三姐妹》番外3 - Bilibili](https://www.bilibili.com/read/cv15635629)  
[^3]: [Falcon Vs Hawk: Key Facts And Differences](https://www.seabirdsanctuary.org/falcon-vs-hawk/)  
[^4]: [隼(Falcon) - Wikipedia](https://en.wikipedia.org/wiki/Falcon),  
[^5]: [鹰(Hawk) - Wikipedia](https://en.wikipedia.org/wiki/Hawk),  
[^6]: [雕(Eagle) - Wikipedia](https://en.wikipedia.org/wiki/Eagle),  
[^7]: [鹰、鹫、隼、鵟、雕的区别 - Zhihu](https://www.zhihu.com/question/61280007)   
[^8]: [俄狄浦斯 - zhihu](https://zhuanlan.zhihu.com/p/386563029)  
[^9]: [恋母情结 - 百度百科](https://baike.baidu.com/item/恋母情结/574425)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


source:  
https://www.manga-audition.com/the-kumamoto-international-manga-camp-2019-report-day-1/

https://www.manga-audition.com/the-kumamoto-international-manga-camp-2019-report-day-2/

https://www.manga-audition.com/the-kumamoto-international-manga-camp-2019-report-day-3/

https://www.manga-audition.com/the-kumamoto-international-manga-camp-2019-report-day-4/

https://www.manga-audition.com/the-kumamoto-international-manga-camp-2019-report-the-final-day/

-------------------------------------------

## The Kumamoto International Manga Camp 2019 REPORT – Day 1  
2019熊本国际漫画营的报道 - 第1天  

Enrico Croce, 18/11/2019, 4 min read

Hello SMA Community!
您好SMA社区！(译注：SMA指Silent Manga Audition(默白漫画大赛))

It’s that special time of the year again…  
又是一年中的特殊时间…

Christmas you say? Close enough… it’s the Kumamoto International Manga Camp Report!  
你说圣诞节？够近了……这是熊本国际漫画营的报道！

Over the next few weeks, we will be updating you on all the manga making action of the Camp. So sit back, grab your drink of choice and enjoy the first installment!  
在接下来的几周内，我们将向您介绍营地中所有制作漫画的动作。因此，坐下来，拿起您选择的饮料，享受第一期！

For the uninitiated, let us give you a brief explanation about the Camp…  
对于初学者，让我们给您简要介绍一下营地吧。

The Kumamoto International Manga Camp (KIMC) is manga event held in the beautiful town of Takamori, on Japan’s Kyushu island. Exclusively open to SMA MasterClass members, creators who have attained the highest awards in the SILENT MANGA AUDITION®, the Camp offers the chance to learn more about making manga and the manga industry.  
熊本国际漫画营（KIMC）是在日本九州岛美丽的小镇高森举行的漫画活动。该营地仅向SMA MasterClass成员开放，这些成员在SILENT MANGA AUDITION®中获得了最高奖项，该营地提供了学习更多有关制作漫画和漫画产业的机会。

For more information about the SMA MasterClass, please click the banner below.  
有关SMA MasterClass的更多信息，请单击下面的横幅。

The 2019 Report will focus on the four newest members of the MasterClass, including two members from Indonesia, one from Malaysia and the last from Brazil.  
2019年报告将重点关注大师班的四个最新成员，其中包括印度尼西亚的两名成员，马来西亚的一名成员和巴西的一名成员。

The Camp kicks off with a glittering Opening Ceremony to welcome the newest members of the SMA MasterClass. With the opening speech by former the Chief-Editor of “Weekly Shonen Jump” and COAMIX Inc. CEO Nobuhiko Horie still ringing in their ears, the invited guests, including the winners of SMA CHINA hold their collective breath and await the handing out of the SMA MasterClass certificates!  
夏令营以闪亮的开幕式拉开序幕，欢迎SMA MasterClass的最新成员。前《 Weekly Shonen Jump》主编和COAMIX Inc.首席执行官Nobuhiko Horie的开幕词仍在耳边，与会的嘉宾，包括SMA CHINA的获奖者，都屏住了呼吸，等待着分发SMA MasterClass证书！
 

 

The first to receive the much coveted SMA MasterClass certificate is Malaysian manga artist Harihtaroon.  
第一位获得令人渴望的SMA MasterClass证书的是马来西亚漫画家Harihtaroon。
 

 

Receiving the Grand Prix award for her amazing SMA EX4 entry Lucky Charm, Harihtaroon’s tireless work with the SMA has paid off with her induction into the SMA MasterClass.  
Harihtaroon与SMA EX4的出色结合获得了Lucky Charm大奖，并因此获得了大奖赛的桂冠，她在SMA MasterClass中的不懈努力使她获得了回报。

Congratulations, Harihtaroon, you deserve it!  
恭喜，Harihtaroon，实至名归！

Next up to receive his certificate from Horie-san is Indonesian mangaka Dedy Korniawan. Dedy’s SMA-EX4 entry First Flight wowed the judges, earning him a Grand Prix Runner-up prize and a place in the SMA MasterClass.  
接下来要获得Horie-san颁发的证书的是印度尼西亚漫画家Dedy Korniawan。 Dedy的SMA-EX4参赛作品First Flight赢得了评委的赞誉，为他赢得了大奖赛亚军和SMA MasterClass的一席之地。
 

 

Following Dedy on stage is Dedy’s fellow countryman, and manga prodigy RIZA aL ASSAMI.  
Dedy的同胞，漫画神童RIZA aL ASSAMI紧随Dedy登台。
 

With 3 GP Runner-up Awards and a Grand Prix Award for his SMA11 entry REBORN, Riza is by far the highest awarded manga artist to join the SMA MasterClass, prompting much excitement for his continued work with the SILENT MANGA AUDITION®!  
里扎（Riza）凭借其SMA11参赛作品REBORN获得了3个GP季军奖和一个大奖赛奖，是迄今为止参加SMA MasterClass的获得最高奖项的漫画家，这为他与SILENT MANGAAUDITION®的持续合作带来了极大的兴奋！

Welcome to the team Riza.  
欢迎加入Riza团队。

Last, but by no means least is Brazilian manga supremo Heitor Amatsu!  
最后，同样引人注目的是巴西漫画超人Heitor Amatsu！
 

 

With several award winning works to his name, including his action packed SMA10 entry BUS, STOP!, Heitor has consistently received praise from the SMA judging panel. Indeed, Tomizawa sensei expressed delighted surprise at the Brazilian wonder kid’s expert depiction of speed for creator as young as Heitor!  
Heitor凭借他的名字获奖无数，其中包括动作十足的SMA10入门级BUS，STOP！，Heitor一直受到SMA评审团的一致好评。 确实，富泽老师对巴西神奇小子对像Heitor一样的创作者的速度描绘出了令人惊讶的惊喜。

With our four international manga stars admiring their well deserved certificates, it’s time to pose for the cameras before enjoying the amazing culinary spread of locally produced, delicious Japanese delicacies!  
我们的四位国际漫画明星都对他们当之无愧的证书表示赞赏，是时候摆姿势让相机在享受本地制作的美味日本美味的美味佳肴的时候了！
 

 

Without the unwavering support of the amazing people of Takamori, the Kumamoto International Manga Camp 2019 would not be possible.  
没有高森(Takamori)人的坚定支持，2019年熊本国际漫画营将是不可能的。

Located within the foothills of the imposing Mount Aso on Japan’s southern island of Kyushu, Takamori is a riot of lush green valleys, breathtaking mountains and unique natural wonders. Quite possibly the best place to study manga!  
高森位于日本南部岛屿九州的雄伟的阿苏山山脚下，葱郁的绿色山谷，壮丽的山脉和独特的自然奇观纷繁而至。 可能是学习漫画的最佳去处！

(For more information about Takamori, take a look at last year reports)  
（有关高森的更多信息，请查看去年的报告）

The SMA MasterClass ceremony of 2019 comes to close with a speech from Takamori’s mayor Mr. Taisei Kusamura, welcoming the international guests from every corner of the globe to one of Japan’s most outstanding areas of natural beauty.  
高森市市长草村大成先生的演讲在2019年SMA MasterClass典礼闭幕时欢迎来自世界各地的国际宾客来到日本最杰出的自然美景地区之一。
 

 

So without further ado, let the 2019 Kumamoto International Manga Camp begin!!  
因此，事不宜迟，让我们开始2019年的熊本国际漫画训练营!!
 

 

See you next week…  
下周见...

CIAO!  
再见！

-------------------------------------------

## The Kumamoto International Manga Camp 2019 REPORT – Day 2  
2019熊本国际漫画营的报道 - 第2天  

Enrico Croce, 25/11/2019, 4 min read

THE STORY SO FAR …  
目前进展...

Day 2 of the 2019 Kumamoto International Manga Camp began with a celebratory welcome speech from COAMIX Inc. CEO Nobuhiko Horie whose mission, in collaboration with the town of Takamori, is to expand the Japanese manga industry into the world. Day 2 also included the Camp’s first of six Manga Workshops, where our invited guests got the chance to learn from successful Japanese mangaka and experienced Japanese editors.   
2019年熊本国际漫画夏令营的第二天以COAMIX Inc.的庆祝致欢迎词开始。首席执行官堀江信彦与高森镇合作，旨在将日本漫画产业扩展到世界。第2天还包括了该营的六个漫画工作室中的第一个，我们的受邀嘉宾有机会向成功的日本漫画家和经验丰富的日本编辑学习。

So without further ado, let’s delve into the excitement of Day 2!  
因此，事不宜迟，让我们深入探讨第2天的兴奋！

Surrounded by the breathtaking landscapes of Takamori, the Kumamoto International Manga Camp takes place within the sprawling “campus” of the Takamori Folk School. More than just an opportunity to learn manga making from the professionals, the Camp also offers the chance to experience the wonders of this beautiful area of Kyushu.  
熊本国际漫画训练营被高森民俗学校庞大的“校园”所包围，周围环绕着高森壮丽的风景。训练营不仅是向专业人士学习漫画制作的机会，还为您提供了九州这个美丽地区的奇观。

The school’s gymnasium, a typical structure recognizable throughout Japan is the location of the first activity. An absorbing lecture about Takamori’s hidden delights, conducted by an enthusiastic member of the the Minami Aso Railway Takamori Line service.    
学校的体育馆是首次活动的举办地，在日本全国都是公认的典型建筑。由南阿苏铁路高森线服务的热情成员主持的关于高森隐藏的乐趣的精彩演讲。

Featuring prominently in the Hojo-sensei helmed Silent Manga movie “Angel Sign”, the Takamori Railway gave fresh look to the cinematic interpretation of “Beginning and Farewell”, by German SMA MasterClass member Vincent Lange.  
高森铁路在北条导演的无声漫画电影《天使印记》中脱颖而出，使德国SMA MasterClass成员Vincent Lange对电影《开始与永别》的诠释焕然一新。

For a rundown of the SMA manga featured in “Angel Sign”, click the banner below:  
要查看“天使标志”中的SMA漫画，请点击以下横幅：  
![](https://www.manga-audition.com/wp/wp-content/uploads/2019/08/banner.png)

https://www.manga-audition.com/manga-movie-angel-sign-release-date-and-poster-art-revealed/
 

As the lecture comes to a close, the invited manga artists are surprised by a very special guest… Takamori’s adorable mascot Kazemaru!  
随着演讲的结束，特邀嘉宾对漫画家们感到非常惊讶……高森的可爱吉祥物Kazemaru！


With a hearty lunch over with, our international guests are primed for their very first editorial session with Japanese editors!  
享用了丰盛的午餐后，我们的国际宾客将准备与日本编辑进行首次编辑交流！

Prior to the Camp, we asked all our invited guests to create a very simple manga “name” showing a unique character perform their “special power” and accompanied with character concept designs. This “name” is to be the focus of these sessions, giving the MasterClass the chance for some effective feedback.  
在训练营之前，我们要求所有邀请的客人创建一个非常简单的漫画“名称”，以展现出独特的角色表现出他们的“特殊能力”并伴随角色概念设计。 这些“名称”将成为这些活动的重点，使MasterClass有机会获得一些有效的反馈。

In the pictures: (Picture of the left) Monthly Comic Zenon Chief Editor Watanabe-san, Zenon editor Takaya-san.
(Picture of the right) Web Comic Zenyon Chief Editor Akiyama-san, Comic Tatan Chief Editor Hisanaga-san, Zenon editor Oniki-san
在图片中：（左图）月刊漫画Zenon主编渡边san，Zenon编辑Takaya san。
（右图）网络漫画Zenyon主编秋山山，Comic Tatan主编久贺山，Zenon编辑鬼木山

We have covered the role of the manga editor extensively on our media channels, but for the newest members of the SILENT MANGA AUDITION® Community, here’s a recap…  
我们已经在媒体渠道上广泛报道了漫画编辑的作用，但是对于SILENT MANGAAUDITION®社区的最新成员，这里有个回顾……

The role of the manga editor is to get the very best out of the artist, helping to deliver effective and entertaining manga to hungry readers!  
漫画编辑的作用是从漫画家中获得最大的收益，从而为渴求的读者提供有效而有趣的漫画！

As the first reader of their artist’s work, the editor will spot what elements need to change or remove, where the action needs to speed up or slow down, and even advise on the placement of speech bubbles and kakimoji.  
作为漫画家作品的第一读者，编辑将发现需要更改或删除的元素，需要加快或减慢动作的位置，甚至可以建议放置对话框和柿子。

Additionally, the editor may also come up with a manga concept that best exhibits an artist’s strengths. From developing the idea with the artist to exhaustively researching background for additional depth, without the editor to hold the reins on creative work, the artist may very well forget the audience as they passionately create!  
此外，编辑者可能还会提出一种漫画概念，以最好地展现漫画家的长处。 从与漫画家一起提出想法，到深入研究背景以进一步深入研究，而没有编辑约束工作里创造性，艺术家很可能会因为热情地创作而忘记了观众！

Chief Editor, Hisanaga san explains Yoonmi how to improve even more her already entertaining manga story!  
Hisanaga san的主编向Yoonmi解释了如何改善她已经很有趣的漫画故事！

Sadly, the content of the SMA editorial sessions must remain a secret between artist and editor, but we can at least briefly show you these intense and fulfilling meetings with these dynamic snapshots!  
可悲的是，必须在漫画家和编辑之间保密SMA编辑会议的内容，但是我们至少可以通过这些动态快照向您简要介绍这些紧张而充实的会议！

With our guests reeling over the invaluable advice of the Zenon editors, it was time to grab some popcorn and settle down to watch the much anticipated Silent Manga movie “Angel Sign”!  
当我们的客人对Zenon编辑的宝贵建议感到满意时，是时候抢些爆米花并坐下来观看备受期待的默白漫画电影《天使印记》了！
 

“Manga is like a movie storyboard”… and so began Horie’s introduction to “Angel Sign”. As producer, the COAMIX Inc. CEO has been involved in the creation “Angel Sign” from the very beginning, utilizing his years of manga editorial experience to help create something very special indeed.  
“漫画就像是一个电影故事板”，因此Horie开始介绍“天使印记”。 作为制作人，COAMIX Inc.首席执行官从一开始就参与创作“天使印记”，利用他多年的漫画编辑经验来帮助创作确实非常特别的作品。

Featuring a sparkling showcase of international directing and acting talent, the feature length film is arguably the first large scale multi-media product, created from YOUR work with the SMA!  
这部长篇故事片的特色是闪亮地展示了国际导演和表演人才的，可以说是您与SMA合作创作的第一款大型多媒体产品！ 

Currently screening as part of the Indonesian Japanese Film Festival, the movie is delighting fans of Japanese cinema throughout the region. If you happen to be in Indonesia, make sure to get yourself a ticket to be in with a chance of winning some signed merchandise from the movie’s project director, and manga making legend Tsukasa Hojo!  
目前正在作为印度尼西亚日本电影节的一部分进行放映，这部电影使整个地区的日本电影迷感到高兴。 如果您碰巧是在印度尼西亚，请确保自己有票可入，有机会赢得电影项目总监和漫画制作传奇北条司的一些签名商品！

Click the banner for more details:  
单击横幅以获取更多详细信息：  

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/1-2-1280x1280.png)

[https://www.manga-audition.com/your-love-letter-to-angel-sign/](https://www.manga-audition.com/your-love-letter-to-angel-sign/)

See you next Monday,  
下星期一见，

CIAO!  
再见！
 

P.S. for a quick peek behind the scenes of “Angel Sign”, check out our series of short films (be sure to activate the English subs 😉).  
P.S. 要快速浏览“天使印记”的幕后故事，请查看我们的短片系列（请确保激活英语字幕😉 ）。

-------------------------------------------

## The Kumamoto International Manga Camp 2019 REPORT – Day 3  
2019熊本国际漫画营的报道 - 第3天  

Enrico Croce, 02/12/2019, 4 min read

THE STORY SO FAR …  
目前进展...

With the invaluable feedback and advice from the Zenon editors still swirling in the minds of our international mangaka, we dive headlong into Day 3.  
Zenon编辑的宝贵反馈和建议仍萦绕我们国际漫画界的头脑中时，我们奋力进入第3天。

Featuring an intense look at the manga industry in the Manga Meetings, more creative workshops and delicious culinary experiences, the third day of the Kumamoto International Manga Festival promises to be even more action packed than the first two!  
熊本国际漫画节的第三天将以漫画集会中的漫画产业为主题，提供更多创意工作坊和美味的烹饪体验，比前两个活动更加精彩！
 

Day 3 kicks off with a serious tone… an in-depth look at the manga business. With four tables manned by the cream of the manga industry, Tsukasa HOJO, Ryuji TSUGIHARA, Jun TOMIZAWA and Nobuhiko HORIE, our invited guests are offered the exclusive chance to direct questions, as well as receive insights from these giants of manga.  
第3天以严肃的语气开始……深入了解漫画业务。我们有四张桌子由漫画界的大神北条司，次原隆二，富泽顺和堀江信彦主持，我们的受邀客人将有机会直接提出问题，并获得这些漫画巨头的见解。

With an emphasis on the differences between manga and Western comics, the meeting looked at the very foundations manga is built upon… the ability to entertain readers. Artists and editors work together to achieve this vital mission, shinning a light on the motivations of professional manga in Japan.  
会议着重强调了漫画与西方漫画之间的区别，这次会议着眼于漫画的基础……娱乐读者的功能。漫画家和编辑人员共同努力实现这一重要使命，从而揭示了日本专业漫画的动机。

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.718.jpg)

At the meeting’s close, COAMIX Inc. CEO Horie-san reaffirms his commitment to further develop manga in the 21st century with his plans for making Takamori the capital of international manga. Make sure to keep an eye on our site and SNS channels for more info on this TOP SECRET project!  
在会议结束时，COAMIX公司。 堀江社长重申了在21世纪进一步发展漫画的决心，他计划将高森打造为国际漫画之都。 请务必留意我们的网站和SNS频道，了解有关这个绝密项目的更多信息！

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.711.jpg)

As our guests begin to make their way into the bright Kyushu sunshine, [SMA China Tsukasa Hojo award](https://bcy.net/item/detail/6606203593584279822) winner [A-Ton](https://bcy.net/u/2420602) takes the opportunity to present Hojo-sensei with a special 60th birthday present!  
当我们的客人开始进入明媚的九州阳光时，[SMA中国北条司奖](https://bcy.net/item/detail/6606203593584279822)获得者[A-Ton](https://bcy.net/u/2420602)借此机会为北条老师送上一份特别的60岁生日礼物!

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.789-1280x853.jpg)  
![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.792-1280x853.jpg)

A quick stop for a group photo and then it’s time for lunch!  
匆匆停留拍张大合照，然后就可以吃午饭了！请看SMA老师在黑板上的精彩涂鸦。

NOTE: check out the SMA sensei’s sublime doodles on the board…  
注：看看SMA老师在黑板上的崇高涂鸦......。

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.811.jpg)  

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.815.jpg)  

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.810.jpg)

Say “KUMAMOTO INTERNATIONAL MANGA CAMP!”  
上面写着"熊本国际漫画营！"

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.825-1280x853.jpg)

At the Manga Camp, we believe you have to work for your lunch! So with chopsticks at the ready, our guests had enormous fun catching their food as delicious somen noodles cascaded down three bamboo slides. Nagashi-somen (flowing noodles) is a staple activity in Japan, making for a unique dining experience!  
在漫画营，我们相信你必须活动活动才能得到你的午餐！所以，我们的客人们准备好了筷子，从三根竹子上流下美味的面条，非常有趣。因此，我们的客人们拿着筷子，从三条竹子滑梯上滑落下来，美味的面条让我们非常开心。流面是日本的主要活动，是一种独特的用餐体验。

![](https://www.manga-audition.com/wp/wp-content/uploads/2019/11/mc19.846.jpg)

Now fully fed, it’s time to continue with the manga workshops. Using the feedback received in Day 2, our international guests make improvements on their “name” assignments under the experienced eyes of the Zenon editorial department.  
吃饱喝足后，继续进行漫画工作坊。根据第二天的反馈，国际嘉宾们在Zenon编辑部经验丰富的目光下，对自己的 "名字 "作业进行改进。

A serious and focussed tone fills the classroom as our mangaka apply the feedback and advice they received. With several consultations with the editors, the goal is to impress the Zenon team… arguably a manga artists first reader in our mission to create effective and entertaining manga.  
课堂上弥漫着严肃而专注的语气，我们的漫画家们将收到的反馈和建议加以应用。通过与编辑的多次协商，我们的目标是给Zenon团队留下深刻的印象......可以说是漫画家的第一读者，我们的使命是创作出有效而有趣的漫画。

…No time to waste going back to the desk, let’s just work here!  
.没有时间浪费在回去办公桌上，我们就在这里工作吧!
 

The day draws to close with a celebratory meal… Takamori style! With fire pits gently roasting delicious dengaku, the invited guests of the KIMC 2019 bond over their shared the experiences at the camp, drink to the health of each other and discuss the one subject that brought them all together… manga!  
一天的活动在庆功宴中接近尾声......高森式的庆功宴! 伴随着火盆轻轻地烤着美味的田乐，KIMC 2019的受邀嘉宾们在共同的营会经历中结下了不解之缘，彼此为健康干杯，并讨论着让他们聚集在一起的话题......漫画!

But before we close the book on Day 3, Monthly Comic Zenon Editor-in-Chief Watanabe-san stands to deliver a rousing speech…  
不过在第三天收书之前，月刊漫画Zenon主编渡边站在这里发表了一段激昂的演讲......

    Thank you everyone for joining us here in Japan. We at Monthly Comic Zenon would like to assure each of you that we will do our very best to help you debut in Japan.”  
    感谢大家来日本参加我们的活动。我们月刊漫画泽农向各位保证，我们会尽全力帮助各位在日本出道。"

    Now… KAMPAI!  
    现在...
 

We hope you enjoyed Day 3, and that you felt you were with us in Takamori! Be sure to keep following our SNS pages to read our 4th REPORT of the KIMC 2019.   
希望大家喜欢第三天的活动，希望大家能感受到和我们一起在高森的感觉! 请务必持续关注我们的SNS页面，阅读我们2019年KIMC的第4次REPORT。  

See you in 7 days, CIAO!  
7天后见，再见!


-------------------------------------------

## The Kumamoto International Manga Camp 2019 REPORT – Day 4  
2019熊本国际漫画营的报道 - 第4天   

Enrico Croce， 09/12/2019， 6 min read

THE STORY SO FAR …  
目前的进展...

After an intense day of manga making under the guiding eyes of the Monthly Comic Zenon editors, our international guests have earned a little R&R! So with that in mind, the fourth and final day of the Kumamoto International Manga Camp 2019 offered the opportunity to experience traditional Takamori with guided tours of the region’s hidden gems.  
在月刊《Zenant Comic》编辑的指导下忙碌了一天的漫画制作之后，我们的国际宾客获得了一些R＆R！因此，考虑到这一点，2019年熊本国际漫画训练营的第四​​天也是最后一天，提供了在该地区隐藏的宝石导游陪同下体验传统高森的机会。

As the sun rises, members of the Kumamoto International Manga Camp 2019 gather for an unforgettable experience. Hidden amongst the bows of ancient trees is the ornate and etherial Buddhist temple of Ganzo, where our group will be introduced to the mediative practice of Zazen.  
随着太阳升起，2019年熊本国际漫画营的成员们聚集在一起，享受难忘的经历。隐藏在古树的弓箭之中的是甘佐华丽而空灵的佛教寺庙，我们的小组将在此介绍Zazen的调解实践。

It is said that a visit to the Ganzo temple will ensure a successful future for young people starting out on their careers. What could more apt for our manga making MasterClass and their mission to debut in Japan?!  
据说，参观Ganzo寺院将确保年轻的事业成功的未来。我们的漫画制作MasterClass以及他们在日本首次亮相的使命更适合什么呢？

The head monk introduces himself and gives our rapt guests a brief explanation about the Zazen meditation method. Adopting the “lotus” position, the international manga artists place both hands on knees, palms down, and form a circle with their thumb and forefinger before gently swaying. With this calming motion, one can eventually the reach incredibly calming levels of enlightenment.  
主持自我介绍，并向我们的速成嘉宾简要介绍了Zazen冥想方法。国际漫画家采用“莲花”姿势，将双手放在膝盖上，手掌朝下，并用拇指和食指围成一个圆圈，然后轻轻摇动。通过这种镇静运动，最终可以达到令人难以置信的平静启蒙水平。

But be careful not enjoy the calming sensations too much, for any monk found snoozing is in for a short, sharp wakeup call! With a brief demonstration involving a large stick and willing volunteers, it’s time to bid farewell to our holy hosts and set out on a mission of discovery.  
但请注意不要过分享受平静感，因为如果有和尚发现打盹的话，都会短暂而尖锐地叫醒您！在简短的演示中，大手杖和自愿的志愿者到场了，是时候告别我们的圣餐主机并着手进行发现任务了。

Splitting into groups, our guests are offered the chance to experience three of Takamori’s thriving industries…  
分成小组，我们的客人将有机会体验高森市蓬勃发展的三个行业……

The first group visits a Tatami manufacturer. Made from soft rush straw, the Tatami has been used to cover the floors of traditional Japanese homes since before the Heian period (794 to 1185). Fascinated by the ancient process of creating these traditional floor coverings, the business owner ensures we never forget our visit by gifting us some beautiful samples of decorative Tatami. ありがとう!!  
第一组参观榻榻米制造商。从平安时代（794年到1185年）之前，榻榻米用柔软的草编稻草制成，被用于覆盖日本传统房屋的地板。公司老板对创建这些传统地板的古老工艺着迷，并向我们赠送了一些精美的装饰榻榻米样品，以确保我们永远不会忘记我们的光临。谢谢！!

With time to spare before lunch, the group then made their way to a bookshop where our manga artists pored over the inexhaustive selection of manga for sale. 
午餐前有时间抽出时间，小组成员前往一家书店，我们的漫画家专心研究了无穷无尽的漫画供出售。

The second group embarks on a much more tastier experience, with a visit to Takamori’s celebrated Miso and Soy Sauce factory!  
第二组参观了高森著名的豆酱和酱油工厂，从而带来了更加美味的体验！

Miso, a traditional seasoning produced by fermenting soybeans with salt and kōji is a staple addition to most Japanese dishes. But perhaps an even more popular condiment is Soy Sauce, made from fermented paste of soybeans, roasted grain, brine, and Aspergillus oryzae. The factory, where both of these delicious Japanese delicacies are produced use methods that have hardly changed for hundreds of years.  
豆酱是传统的调味料，是用盐和枸杞发酵大豆制成的，是大多数日本料理的主要添加物。但也许更受欢迎的调味品是酱油，它由大豆，烤谷物，盐水和米曲霉的发酵糊制成。生产这两种美味日本料理的工厂使用了几百年来几乎没有改变的方法。

With pots of miso soup greedily sampled, the owner of this delightful factory surprises us with Soy Sauce flavored ice cream! (Believe us, it was much tastier than it sounds!)   
贪婪地品尝了几桶豆酱汤，这家令人愉快的工厂的老板用酱油味的冰淇淋给我们惊喜！ （相信我们，这比听起来要好吃得多！）

Have you ever seen inside a Miso factory? Enjoy this magical place by clicking on the pictures below…  
您曾在豆酱工厂内见过吗？点击下面的图片，享受这个神奇的地方...

The third group experience a more modern take on Japan with a visit to a pachinko parlor followed by a guided tour of Japan’s national tipple… sake!  
第三组参观弹珠机，体验日本的现代气息，然后带队参观日本的国酒。

Originally built as a children’s toy in the 1920’s as a take on the pinball machine, Pachinko has a more adult fanbase today thanks to its “jackpot” feature. Modern Pachinko machines feature short anime sequences to enhance the gambling experience, which instantly piqued the interest of our anime loving guests!  
Pachinko最初是在1920年代以弹球机的形式作为儿童玩具而制造的，如今由于其“中奖”功能，其粉丝数量已经增加。现代Pachinko机器采用短动画序列来增强赌博体验，立即激起了我们喜爱动画的客人的兴趣！

Escaping the deafening din of the Pachinko Parlor, the group delved into Japan’s “partying” past with a visit to a Sake brewery. The origins of sake are lost to the mists of time, with the earliest mention being attributed to the 3rd century Chinese text Records of the Three Kingdoms with a reference to the Japanese “drinking and dancing”.  Made from fermented rice (rice wine), the process is more akin to brewing beer than wine making. Whatever sake’s origins or brewing process,  our international guests all agreed on one thing… sake is tasty!  
该小组逃离了弹球机的震耳欲聋的喧嚣，并参观了日本清酒酿造厂，深入研究了日本的“派对”过去。清酒的起源已被时间的迷雾所模糊，最早提到的是三世纪中国文本《三国志》，其中提到了日本的“喝酒跳舞”。用发酵米（米酒）制成，此过程更像酿造啤酒，而不是酿酒。无论清酒的来源或酿造过程，我们的国际客人都同意一件事……清酒可口！

Fresh from their traditionally merry (looking at you “sake” group!) experiences, our guests regroup for an unforgettably “festive” lunch!  
从他们传统上的快乐（看着您的“清酒”团体！）体验中脱颖而出，我们的客人重新聚在一起，享受难忘的“节日”午餐！

A “Matsuri” festival is a traditional celebration attributed to the rice paddy harvest. With games including Goldfish scooping (or in this case, rubber ball scooping) and shooting galleries, the festival is a yearly highlight for children and adults alike.    
“Matsuri”节日是稻米丰收的传统庆祝活动。通过举办包括金鱼捞（或在本例中为橡胶球捞）和射击场的游戏，该节日每年对儿童和成人均是一个亮点。

With fresh bento boxes (lunch box) handed out to our hungry guests, the wonderful people of Takamori guide the manga artists on a whistle stop tour of their mini-Matsuri!  
通过向饥饿的客人们分发新鲜的便当盒（午餐盒），高森的好人带领漫画家们在迷你松饼的哨声中游览！

Even Tsugihara sensei has joined the party (Wonder if he’s thinking about his editor with that determined look in his aiming eye??)  
甚至次原隆二老师也加入了这个聚会（如果他以瞄准的目光注视着他的编辑，那真是太好了？）

With the festive morning over with, its time to get back into the classroom and learn more about the wonderful medium of manga!  
随着节日的早晨结束，是时候回到教室并进一步了解漫画的奇妙媒介了！

This time, we take a look at idea generation with Tsugihara sensei and the origins of manga and its impact on how we create pages today with Tomizawa sensei. Paper and pens at the ready, our students of manga settle into their seats and begin an invaluable learning experience.  
这次，我们来看一下次原隆二老师产生的想法以及漫画的起源以及它对今天我们使用富泽顺老师创建页面的影响。漫画纸上的笔和笔准备就绪后，我们的漫画学生就座并开始了宝贵的学习体验。

NOTE: Due to the highly secretive nature of manga making, we can only show you a couple of snapshots from these exclusive lessons!  
注意：由于漫画制作的高度机密性，我们只能为您展示这些独家课程中的几个快照！

    “What I will try to explain you today, is how to generate a manga-style idea… It all begins with a beautiful lie…”  
    “我今天将尝试向您解释的是如何产生漫画风格的想法……这一切都始于美丽的谎言……”
 

    “Did you know that our eyes automatically follow the panel layout you create? Let’s try to create a very easy to follow page of manga today!”  
    “您知道我们的眼睛会自动跟随您创建的面板布局吗？让我们今天尝试创建一个非常易于理解的漫画页面！”

To benefit from the wisest of words from these two manga masters, join the MasterClass through the SMA! But in the meantime, please check out the gallery for a little taste of what the SMA MasterClass has to offer.  
要从这两个漫画大师的智慧中受益，请通过SMA加入MasterClass！但与此同时，请查看图库以了解SMA MasterClass提供的功能。

And with those final lessons over, the 2019 Kumamoto International Manga Camp comes to a close. But thankfully, the fun is just starting with a spectacular BBQ and entertainment from the incredibly talented students of the Takamori High School!  
最后的课程结束后，2019年熊本国际漫画训练营结束了。但值得庆幸的是，乐趣只是从壮观的烧烤和来自高森高中的才华横溢的学生的娱乐中开始！

With stands offering sizzling meats, fish, vegetables and drinks, not to mention the ever present rice and noodle dishes, our international guests settle in for the entertainment. First is the incredible sound of the High School band playing every anime hit you can imagine, followed by a death defying display of martial arts thanks to the Kendo Club!  
我们的国际宾客们的摊位提供热气腾腾的肉类，鱼类，蔬菜和饮料，更不用说曾经出现过的米饭和面条了，我们的国际宾客们定居下来享受娱乐。首先是高中乐队令人难以置信的声音，播放您可以想象的每部动漫作品，其次是剑道社，这是一场令人叹为观止的武术表演！

What an incredible send off from the welcoming people of Takamori!  
高森欢迎人们发出令人难以置信的惊喜！



-------------------------------------------

## The Kumamoto International Manga Camp 2019 REPORT – THE FINAL DAY   
2019熊本国际漫画营的报道 - 最后一天  

Enrico Croce，16/12/2019，3 min read

THE STORY SO FAR …  
目前的进展...

After four intensive days of manga workshops, ideas generation and culture exchanges, not to mention fun and friendship, our international guests bid a fond farewell to the captivating town of Takamori to spend the day soaking in the sites of Kumamoto City!  
经过为期四天的紧张的漫画工作坊，思想交流和文化交流，更不用说乐趣和友谊了，我们的国际宾客向迷人的高森镇告别，度过了一天在浸泡在熊本市内的地方！
 

The final day of the Kumamoto International Manga Camp 2019 starts with warm welcome from manga royalty! The enigmatic Monkey D Luffy from the acclaimed manga series “One Piece” strikes an imposing pose much to the delight of our guests. Created by Oda Eiichiro sensei, a Kumamoto native, the bendy pirate has almost eclipsed the mighty Kumamon for the city’s mascot top spot!  
熊本国际漫画营2019的最后一天从漫画皇室的热烈欢迎开始！来自著名漫画系列“One Piece”的神秘猴子D Luffy摆出一个令人印象深刻的姿势，使我们的客人感到非常高兴。由熊本人Oda  Eiichiro老师创建，这位易弯曲的海盗几乎使强大的Kumamon黯然失色，成为该市吉祥物的头把交椅！

Following the devastating earthquake of 2016, Oda sensei donated ¥800 million ($7.1 million) in the name of Luffy to help in the reconstruction of prefecture, making the pirate a hero on AND off the page! (source: japantimes.co.jp)  
在2016年毁灭性的地震之后，小田老师以路飞（Luffy）的名义捐赠了8亿日元（合710万美元），帮助该县重建，使海盗成为了英雄！ （资料来源：japantimes.co.jp）

The bronze Luffy statue can be found in front of the Kumamato Prefectural Government building.  
青铜的路飞雕像可以在熊马户县政府大楼前找到。

Next up is a botanical bonanza, with a gentle stay in the exquisite Suizenji Park. Created in 1632 by Lord Hosokawa Tadatoshi of Kumamoto as a statement of his family’s majesty, the finely sculpted landscape is a typical example of Japanese gardening at its best.  
接下来是一个郁郁葱葱的植物园，您可以在精致的Suizenji公园中度过悠闲时光。由熊本县的细川忠敏勋爵（Lord Hosokawa Tadatoshi）于1632年创建，以表达其家族的威严，这幅精美的景观是日本最佳园艺的典范。

Following the serene oasis of the park, it’s time for lunch! With a delicious array of Japanese delicacies awaiting our group, we proceed to discuss the fun of the Camp and prepare for a visit to the imposing behemoth that is Kumamoto Castle! For more info this UNESCO heritage site, check out LAST YEAR’S REPORT.  
沿着公园宁静的绿洲，该吃午饭了！伴随着美味可口的日本佳肴等待着我们的小组，我们继续讨论营地的乐趣，并为参观宏伟的熊本城堡巨兽做准备！有关联合国教科文组织遗产的更多信息，请查阅《上一年的报告》。

This year’s Camp happened to coincide with the 24th World Women’s Handball Championship, an event the city honored with a magnificent Kumamon straw statue. French MasterClass alumni Nicolas David couldn’t help but pose with everyone’s favorite bear sporting the colors of the Japanese team!  
今年的夏令营恰逢第24届世界女子手球锦标赛，该市以宏伟的Kumamon稻草塑像而得奖。法国大师班的校友尼古拉斯·戴维（Nicolas David）忍不住摆出了每个人都喜欢的带有日本队色彩的熊！

Now it’s down to business with a visit to the Kumamoto COAMIX Inc., manga LAB! Located in the heart of the city, the manga LAB serves as an editorial outpost for the region’s professional manga artists.    
现在就来拜访熊本COAMIX Inc.（漫画实验室）吧！漫画实验室位于市中心，是该地区专业漫画家的编辑基地。

As reported in the Kumamoto Manga Festival instalment, the manga LAB was established to save artists the trouble of traveling to Tokyo to have their work assessed. With resources ranging from text books to the latest hardware and software, the LAB is a pioneering effort to spread the joy of manga making to Kyushu.  
正如熊本漫画节所报道的那样，建立了漫画实验室是为了节省艺术家前往东京进行作品评估的麻烦。 LAB拥有从教科书到最新硬件和软件的资源，是将漫画制作的乐趣传播到九州的开创性工作。

With a huge screen linking directly to the Tokyo COAMIX Inc. office, the Tokyo based editors can conduct editorial meetings without the time and expense of travel. We are proud to be breaking technological boundaries in our mission to usher in entertaining manga!  
通过直接连接到东京COAMIX Inc.办公室的大屏幕，驻东京的编辑人员可以进行编辑会议，而无需花费时间和金钱。我们为突破娱乐性漫画的使命而打破技术界限感到自豪！

After a fascinating introduction to the work of the LAB by Mocchi, it’s time for our final group photo. And of course, your intrepid reporter just HAS to sit near to the mighty Kenshiro…!  
在对Mocchi的LAB的工作进行了有趣的介绍之后，该是我们最后合影的时候了。当然，您的勇敢记者只是坐在强大的Kenshiro附近...！

With the manga LAB tour over with, we can officially mark the end of the Kumamoto International Manga Camp 2019. We hope you enjoyed reading about the fun our manga making guests had, from serious study to fun-filled escapades, the KIMC has something for everyone!  
随着漫画LAB之旅的结束，我们可以正式纪念2019年熊本国际漫画夏令营的结束。我们希望您喜欢阅读我们的漫画使宾客享受的乐趣，从认真研究到充满乐趣的出路，KIMC为您准备了一些东西大家！

If you’re still wondering how to join future manga Camps, then follow these three east steps:  
如果您仍想知道如何加入未来的漫画营，请遵循以下三个步骤：

1) JOIN the Audition
参加大赛

2) WIN a high award
赢得大奖

3) ENTER the MasterClass!
进入大师班！

Simple, isn’t it?
很简单，不是吗？

We hope to meet YOU very soon! Until then, keep reading our articles and be inspired to make manga!  
我们希望很快见到您！ 在此之前，请继续阅读我们的文章并激发制作漫画的灵感！

-------------------------------------------





"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
    - 如有错误，请指出，非常感谢！  


# FC的画风-面部-鼻(正面)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96887))  


- **如何描述正面的鼻子**  
先了解一下目前人们是如何描述正面的鼻子，之后再使用这些方法、术语来描述FC里的正面的鼻子。  
    - 鼻子的结构[4]：  
        1. ball(鼻头?)  
        2. wings(鼻翼)  
        3. septum(鼻中隔)  
        4. bridge(鼻梁)  
        5. tip(鼻尖)。 鼻头上部。一般有高光。    
        6. sides(鼻侧)  
        7. root(鼻根)  
    ![](./img/how-to-draw-a-nose-4-6.jpg)  

    
    - 男性、女性鼻子的差异[4]：  
    ![](./img/how-to-draw-a-nose-5-1_002.jpg)  

    - 鼻子正面的变化(鼻头、鼻孔的大小和比例不同)[4]：  
    ![](./img/how-to-draw-a-nose-5-3_002.jpg)  


    - [1]中给出了面部mark点。其中与鼻子相关的mark点:  
        - 4: glabella (g), 眉间点  
        - 5: sellion (se), 鼻梁点  
        - 6/7: alare (al), 鼻翼点  
        - 8: subnasale (sn), 鼻下点, (Il[2])    
    ![](../fc_drawing_style__face/img2/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig3.png)    
      


    - [2]中有关于鼻子的非常详细介绍。  

    - 正面鼻子的一些变化[3]：  
        - 正面鼻子的三部分及其变化：  
            - 鼻梁(bridge): Width((正面)宽度), Shape((侧面)形状)，Angle((侧面)角度)  
            - 鼻孔(nostrils): Size((正面)大小)，Height((正面)高度)，Tilt((侧面)倾斜度)， Flare((侧面)外翻?)  
            - 鼻尖(tip):  Shape((正面)形状)，Size((正面)大小)，Angle((侧面)角度)，Bulbousness(大蒜鼻)  
        - 正面鼻子，整体有长度和宽度的变化：  
            - Length(长度): Short(短), Medium(中), Long(长).  
            - Width(宽度): Narrow(窄), Medium(中), Wide(宽).   
        ![](./img/MurphAinmire_NoseStudy_deviantart.jpg)  


- **FC里鼻子(正面)的一些特点**:  
通过观察，FC里角色的鼻子(正面)有如下特点：  

    - 下图中鼻子处的红色曲线：  
      ![](./img/nose_middle.jpg)  
      我猜，这条线是Dorsum(鼻背)[2]和Lateral Wall(鼻侧)[2]左侧的交界线，是阴影的交界线。绝大多数情况，位于正面面部中垂线(上图蓝色竖线)的右侧。  
      注：Bridge(鼻梁)和Dorsum(鼻背)是不同位置。Bridge在Dorsum上方，Dorsum区域长，详见[2]。它们和[1]中的点4、点5不一致或稍微有些差异。    
      对比一下。下图左中右三张图，中图是左图和右图的叠加：  
      ![](./img/01_025_4__mod.jpg) 
      ![](./img/01_025_4__04_000a_0__.jpg) 
      ![](./img/04_000a_0__mod.jpg) 

    - 下图中S区域：  
      ![](./img/nose_middle.jpg)  
      鼻侧处的阴影区域。有时该区域被涂上阴影。       

    - 为了便于比较不同角色的鼻子的特点，我使用以下mark点：  
      ![](./img/nose_mark.jpg)  
    

------

- **雅彦(Masahiko)**：  
    - basis:  
        - 图片: 03_131_4, 11_129_3, 13_116_4, 13_118_6, 13_139_1,       
        - 特点：     
        - 叠加：  
            ![](./img/nose_front_masahiko_basis.png)  


- **雅美(Masami)(雅彦女装)**：  
    - front:  
        - 图片: 12_054_4, 07_178_2, 07_048_0, 03_027_3,        
        - 特点：     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_masami_front.png) 
            ![](./img/nose_mark_masami_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 14_246_3, 04_141_0, 03_114_0,        
        - 特点：镜头俯视鼻子。       
        - 叠加：  
            ![](./img/nose_front_masami_top.png)  


- **紫苑(Shion)**：  
    - basis:  
        - 图片: 14_282_1, 12_104_3, 11_043_7, 09_083_2, 04_139_5, 03_003_0, 
        - 特点： 成组的图片数量最多。相比于雅彦basis，鼻孔小、鼻孔更朝下、鼻尖低(几乎看不到鼻尖)、鼻侧边线圆滑。   
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_shion_basis.png) 
            ![](./img/nose_mark_shion_vs_masahiko_basis.jpg)  

    - tip:  
        - 图片: 07_188_0, 04_189_5，10_008_6，02_119_6
        - 特点： 鼻尖明显。和雅彦basis几乎一致，比雅彦basis鼻孔小。         
        - 叠加：  
            ![](./img/nose_front_shion_tip.png)  


- **塩谷/盐谷(Shionoya)(紫苑男装)**：  
    - basis:  
        - 图片: 14_067_4, 12_151_4，12_150_4，  
        - 特点： 成组的图片数量最多。鼻尖明显。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_shya_basis.png) 
            ![](./img/nose_mark_shya_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 11_009_3, 14_025_0,  
        - 特点： 镜头俯视鼻子。       
        - 叠加：  
            ![](./img/nose_front_shya_top.png)  

    - 对比12_150_4、12_143_0，鼻尖、鼻孔稍微抬起。  


- **若苗空(Sora)**  
    - basis:  
        - 图片: 08_050_2，01_152_0, 01_00a,  
        - 特点： 成组的图片数量最多。鼻尖低于雅彦basis，鼻孔间距宽于雅彦basis。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_sora_basis.png) 
            ![](./img/nose_mark_sora_vs_masahiko_basis.jpg)  

    - down:  
        - 图片: 04_015_0, 02_191_3, 08_063_3,  
        - 特点： 镜头仰视鼻子。       
        - 叠加：  
            ![](./img/nose_front_sora_down.png)  


- **若苗紫(Yukari)**  
    - basis:  
        - 图片: 10_048_7,09_155_5，08_066_3，08_017_5，05_050_6，04_062_4，04_022_2，02_018_6，01_200_4，01_043_0，01_025_4，    
        - 特点： 成组的图片数量最多。鼻尖明显，鼻尖低于雅彦basis。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_yukari_basis.png) 
            ![](./img/nose_mark_yukari_vs_masahiko_basis.jpg)  

    - straight:  
        - 图片: 05_183_6, 01_030_0
        - 特点： 鼻尖不明显。       
        - 叠加：  
            ![](./img/nose_front_yukari_straight.png)  

    - mama:  
        - 图片: 01_025_4, 02_027_2,  
        - 特点： 雅彦的妈妈。Dorsum(鼻背)长。         
        - 叠加：  
            ![](./img/nose_front_yukari_mama.png)  


- **浅冈叶子(Yoko)**  
    - basis:  
        - 图片: 13_142_1，13_111_3, 08_160_3,06_007_3, 05_160_2,04_178_0,     03_041_2, 
        - 特点： 成组的图片数量最多。鼻孔间距比雅彦basis窄。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_yoko_basis.png) 
            ![](./img/nose_mark_yoko_vs_masahiko_basis.jpg)  

    - down:  
        - 图片: 13_101_3, 13_116_3, 
        - 特点： 镜头仰视鼻子。       
        - 叠加：  
            ![](./img/nose_front_yoko_down.png)  

    - short:  
        - 图片: 02_072_6, 08_149_2, 05_097_5,  
        - 特点： 鼻子短。         
        - 叠加：  
            ![](./img/nose_front_yoko_short.png)  


- **浩美(Hiromi)**  

    - top:  
        - 图片: 09_058_0, 09_036_0 
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_hiromi_top.png)  

    - basis:  
        - 图片: 04_091_2, 07_106_1, 05_163_0, 05_130_2
        - 特点： 成组的图片数量最多。相比雅彦basis，鼻孔小、鼻尖稍低。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_hiromi_basis.png) 
            ![](./img/nose_mark_hiromi_vs_masahiko_basis.jpg)  

    - down2:  
        - 图片: 09_034_7, 05_164_0 
        - 特点： 镜头仰视的程度很大。       
        - 叠加：  
            ![](./img/nose_front_hiromi_down2.png)  

    - tilt:  
        - 图片: 03_008_3，01_191_5， 
        - 特点： 鼻侧线倾斜明显。       
        - 叠加：  
            ![](./img/nose_front_hiromi_tilt.png)  


- **熏(Kaoru)**  
    - top2:  
        - 图片: 13_011_1，08_173_2，08_035_5   
        - 特点： 镜头俯视程度大。         
        - 叠加：  
            ![](./img/nose_front_kaoru_top2.png)  

    - top:  
        - 图片: 13_042_0__r，13_010_1__r，12_171_0__r，11_045_1，09_101_3， 08_062_2__r，08_036_5
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_kaoru_top.png)  

    - basis:  
        - 图片: 08_127_0，07_152_3，07_193_5，08_013_4，11_105_3，
        - 特点： 镜头平视时，成组的图片数量最多。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_kaoru_basis.png) 
            ![](./img/nose_mark_kaoru_vs_masahiko_basis.jpg)  

    - down:  
        - 图片: 13_018_1__r, 13_075_6__r   
        - 特点： 镜头仰视。       
        - 叠加：  
            ![](./img/nose_front_kaoru_down.png)  



- **江岛(Ejima)**  
    - down:  
        - 图片: 05_025_0，05_012_0，03_004_4
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_ejima_down.png)  

    - basis:  
        - 图片: 10_075_5，12_163_6, 14_025_1
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻尖稍低。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_ejima_basis.png) 
            ![](./img/nose_mark_ejima_basis_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 12_140_5，10_128_6
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_ejima_top.png)  


- **辰巳(Tatsumi)**  
    - down2:  
        - 图片: 13_081_7，09_110_3，08_035_2,08_033_0__r,07_168_5,09_106_3__r,  
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_tatsumi_down2.png)  

    - basis:  
        - 图片: 13_074_4，11_113_1, 09_134_6,09_097_2,09_093_7,07_184_0,06_041_0, 06_027_3,06_026_5, 04_139_4, 
        - 特点： 镜头平视时，成组的图片数量最多。鼻孔宽于雅彦basis。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_tatsumi_basis.png) 
            ![](./img/nose_mark_tatsumi_basis_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 06_072_6，06_028_4, 04_135_6, 03_136_3, 03_122_0,  
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_tatsumi_top.png)  

- **导演**  
    - down:  
        - 图片: 12_139_1, 03_110_2,03_066_4,  
        - 特点： 镜头稍微仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_director_down.png)  

    - front:  
        - 图片: 03_099_1, 03_061_2,03_035_4,
        - 特点： 鼻子短，鼻孔宽于雅彦basis。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_director_front.png) 
            ![](./img/nose_mark_director_front_vs_masahiko_basis.jpg)  

- **早纪(Saki)**  
    - top:  
        - 图片: 10_019_2，11_114_0，10_011_2__r，10_010_1__r，
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_saki_top.png)  

    - basis:  
        - 图片: 09_182_4，09_181_1, 
        - 特点： 镜头平视时，成组的图片数量最多。几乎和雅彦basis一致(鼻尖稍低)      
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_saki_basis.png) 
            ![](./img/nose_mark_saki_vs_masahiko_basis.jpg)  


- **真琴(Makoto)**  
    - no-nostril:  
        - 图片: 09_056_6
        - 特点： 鼻孔未显示。         
        - 叠加：  
            ![](./img/nose_front_makoto_no-nostril.png)  

    - no-nostril2:  
        - 图片: 07_072_0，
        - 特点： 鼻孔未显示。         
        - 叠加：  
            ![](./img/nose_front_makoto_no-nostril2.png)  

    - basis:  
        - 图片: 11_035_0,07_108_1，07_097_0，07_075_5，03_021_2， 
        - 特点： 镜头平视时，成组的图片数量最多。鼻孔间距稍小于雅彦basis。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_makoto_basis.png) 
            ![](./img/nose_mark_makoto_vs_masahiko_basis.jpg)  

    - down:  
        - 图片: 07_112_5，06_103_2，06_005_6，05_163_0，03_013_0，
        - 特点： 镜头仰视。         
        - 叠加：  
            ![](./img/nose_front_makoto_down.png)  


- **摄像师**  
    - basis:  
        - 图片: 14_193_1, 14_171_4, 14_036_4, 12_163_6, 12_157_5, 12_148_0, 10_067_3_r, 07_104_1, 03_070_4, 
        - 特点： 镜头平视时，成组的图片数量最多。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_cameraman_basis.png) 
            ![](./img/nose_mark_cameraman_basis_vs_masahiko_basis.jpg)  

    - down2:  
        - 图片: 14_194_6, 14_171_1, 10_063_1, (03_098_4), 
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_cameraman_down2.png)  


- **浅葱(Asagi)**  
    - basis:  
        - 图片: 14_074_2,14_073_7,14_057_0,14_039_0, 
        - 特点： 镜头平视时，成组的图片数量最多。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
            ![](./img/nose_front_asagi_basis.png) 
            ![](./img/nose_mark_asagi_vs_masahiko_basis.jpg)  

    - down:  
        - 图片: 14_123_0，14_234_0,  
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_asagi_down.png)  


- **和子(Kazuko)**  
    - down2:  
        - 图片: 08_126_5, 07_051_1, 06_103_2, 05_163_0, 04_189_1, 03_021_2,  01_121_3, 
        - 特点： 镜头仰视的程度更大。         
        - 叠加：  
            ![](./img/nose_front_kazuko_down2.png)   

    - down:  
        - 图片: 08_141_3, 08_113_4__r 
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_kazuko_down.png)   

    - basis:  
        - 图片: 08_120_0_r, 08_120_3，08_091_5， 
        - 特点： 镜头平视时，成组的图片数量最多。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_kazuko_basis.png) 
            ![](./img/nose_mark_kazuko_basis_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 11_085_2，08_133_4,08_109_3，07_075_0，07_074_8，05_061_3，02_080_4,(01_069_0, 01_066_0), 01_052_2, 
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_kazuko_top.png)   

    - top2:  
        - 图片: 10_163_2,  
        - 特点： 镜头俯视的程度更大。         
        - 叠加：  
            ![](./img/nose_front_kazuko_top2.png)   


- **爷爷**  
待完成  


- **横田进(Susumu)**  
    - down2:  
        - 图片: 08_117_6, 08_135_2, 05_163_0, 06_103_2, 03_030_0,  
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_susumu_down2.png)   

    - basis:  
        - 图片: 04_189_1, 03_094_1，03_021_2，(03_016_5), 03_014_5, 03_013_3, 03_012_5,  
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻尖稍高。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_susumu_basis.png) 
            ![](./img/nose_mark_susumu_basis_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 05_065_5, (08_050_0, 05_065_0), 03_019_0,  
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_susumu_top.png)   


- **宪司(Kenji)**  
    - down3:  
        - 图片: 07_026_1, 07_006_6, 03_175_4,   
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_kenji_down3.png)   

    - down2:  
        - 图片: 04_082_4, 04_060_5, 04_011_1,  
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_kenji_down2.png)   

    - down:  
        - 图片: 10_173_2, 07_007_2, 04_010_0 
        - 特点： 镜头仰视鼻子。         
        - 叠加：  
            ![](./img/nose_front_kenji_down.png)   

    - basis:  
        - 图片: 04_035_1__r, 03_189_5, 03_187_2, 03_179_4,  
        - 特点： 镜头平视时，成组的图片数量最多。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_kenji_basis.png) 
            ![](./img/nose_mark_kenji_basis_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 04_060_0, 04_051_4, 04_039_5,04_038_2__r, 04_014_3, 04_014_1,   
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_kenji_top.png)   

    - top2:  
        - 图片: 04_052_5,  
        - 特点： 镜头俯视鼻子。         
        - 叠加：  
            ![](./img/nose_front_kenji_top2.png)   


- **顺子(Yoriko)**  
    - basis:  
        - 图片: (06_187_0), 04_016_3, 04_005_5,03_172_0,03_155_5,
        - 特点： 镜头平视时，成组的图片数量最多。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_yoriko_basis.png) 
            ![](./img/nose_mark_yoriko_basis_vs_masahiko_basis.jpg)  

    - front2:  
        - 图片: 10_173_3, 04_039_0 
        - 特点： 和basis组几乎一致，但鼻梁处的斜线有差异。和雅彦basis几乎一致。           
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_yoriko_front2.png) 
            ![](./img/nose_mark_yoriko_front2_vs_masahiko_basis.jpg)  


- **森(Mori)**  
    - basis:  
        - 图片: (10_107_3, 10_102_3, 02_074_2), 10_102_1, 10_095_0,
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔间距小、鼻孔小。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_mori_basis.png) 
            ![](./img/nose_mark_mori_basis_vs_masahiko_basis.jpg)  

    - 2个不同侧面的镜头：10_093_4，10_109_6。和basis一起组成鼻子转动的动画：  
            ![](./img/nose_front_mori_basis.png) 
            ![](./img/nose_front_mori_10_093_4.jpg) 
            ![](./img/nose_front_mori_10_109_6__r.jpg) 
            ![](./img/nose_front_mori_turn_anim.gif)  


- **叶子母**  
    - down:  
        - 图片: 13_136_4  
        - 特点： 镜头仰视。  
        - 叠加：  
            ![](./img/nose_front_ykma_down.png)  

    - basis:  
        - 图片: 13_161_3,13_103_3
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔小。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
            ![](./img/nose_front_ykma_basis.png) 
            ![](./img/nose_mark_ykma_basis_vs_masahiko_basis.jpg)  




- **理沙(Risa)**  
    - basis:  
        - 图片: 11_147_1, 12_025_0, 
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔小、鼻尖略低。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_risa_basis.png) 
            ![](./img/nose_mark_risa_basis_vs_masahiko_basis.jpg)  


- **美菜(Mika)**  
    - basis:  
        - 图片: 12_033_5,12_024_4,12_012_1,12_008_6,
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔小、鼻孔间距小。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_mika_basis.png) 
            ![](./img/nose_mark_mika_basis_vs_masahiko_basis.jpg)  

    - front2:  
        - 图片: 12_053_3, 12_055_3  
        - 特点： 镜头仰视。  
        - 叠加：相比basis组，鼻侧线偏右。    
            ![](./img/nose_front_mika_front2.png)  


- **齐藤茜(Akane)**  
    - basis:  
        - 图片: 07_099_3,06_145_7,06_144_5,06_157_1,
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻子稍短。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_akane_basis.png) 
            ![](./img/nose_mark_akane_basis_vs_masahiko_basis.jpg)  

    - no-nostrils:  
        - 图片: 07_100_2, 06_159_2  
        - 特点： 未显示鼻孔。  
        - 叠加：相比basis组，鼻侧线偏右。    
            ![](./img/nose_front_akane_no-nostrils.png)  


- **仁科(Nishina)**  
    - down2:  
        - 图片: 14_027_4,10_147_6   
        - 特点： 镜头仰视。  
        - 叠加：  
            ![](./img/nose_front_nishina_down2.png)  

    - basis:  
        - 图片: 14_028_6, 14_014_5, 14_008_7, 
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻尖稍低。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_nishina_basis.png) 
            ![](./img/nose_mark_nishina_basis_vs_masahiko_basis.jpg)  


- **葵(Aoi)**  
    - down2:  
        - 图片: 06_077_1, 06_067_1, 06_056_0, 06_039_0   
        - 特点： 镜头仰视。  
        - 叠加：  
            ![](./img/nose_front_aoi_down2.png)  

    - down:  
        - 图片: 09_101_6,  
        - 特点： 镜头仰视。  
        - 叠加：  
            ![](./img/nose_front_aoi_down.png)  

    - basis:  
        - 图片: 06_075_4, 06_073_3, 06_073_4, 06_080_6,  
        - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔更朝下(没有雅彦basis鼻孔那么朝天)。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
            ![](./img/nose_front_aoi_basis.png) 
            ![](./img/nose_mark_aoi_basis_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 06_042_1   
        - 特点： 镜头俯视。  
        - 叠加：  
            ![](./img/nose_front_aoi_top.png)  

    - 06_081_3角色稍微向左转头，能感觉到06_081_3里的鼻子和basis组里的鼻子稍微有些差异(转向)：  
        ![](./img/nose_front_aoi_basis.png) 
        ![](./img/nose_front_aoi_06_081_3_vs_aoi_basis__2.gif) 
        ![](./img/nose_front_aoi_06_081_3.png)  


- **奶奶**  
未处理。  


- **文哉(Fumiya)**  
    - down2:  
        - 图片: 12_040_3, 12_035_7,  
        - 特点： 镜头仰视。  
        - 叠加：  
            ![](./img/nose_front_fumiya_down2.png)  

    - basis:  
        - 图片: 12_012_3, 12_010_3,   
        - 特点： 镜头平视时，成组的图片数量最多。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
            ![](./img/nose_front_fumiya_basis.png) 
            ![](./img/nose_mark_fumiya_basis_vs_masahiko_basis.jpg)  


- **齐藤玲子(Reiko)**  
    - 01_169_6:  
        - 图片: 01_169_6,
        - 特点：年幼时的样子。相比于雅彦basis，鼻子稍短。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_reiko_01_169_6.png) 
            ![](./img/nose_mark_reiko_01_169_6_vs_masahiko_basis.jpg)  


- **松下敏史(Toshifumi)**  
    - top:  
        - 图片: 09_077_5,09_084_4,
        - 特点：镜头稍微俯视。相比于雅彦basis，鼻孔和鼻尖更朝下。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_matsu_top.png) 
            ![](./img/nose_mark_matsu_top_vs_masahiko_basis.jpg)  


- **章子(Shoko)**  
待完成  


- **阿透(Tooru)**  
    - front:  
        - 图片: 12_038_3,
        - 特点：。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
            ![](./img/nose_front_tooru_front.png) 
            ![](./img/nose_mark_tooru_front_vs_masahiko_basis.jpg)  


- **京子(Kyoko)**  
    - basis:  
        - 图片: 02_177_2,  
        - 特点： 镜头平视时，成组的图片数量最多。鼻头更明显(更圆)。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_kyoko_basis.png) 
            ![](./img/nose_mark_kyoko_basis_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 02_186_2_r,02_186_0_r,  
        - 特点： 镜头俯视。相比于雅彦basis，鼻子稍短。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_kyoko_top.png)  


- **一马(Kazuma)**  
待完成  


- **一树(Kazuki)**  
    - front:  
        - 图片: 12_012_3,  
        - 特点： 相比于雅彦basis，鼻孔明显（更朝天）。该镜头里，因为右脸嘴角微笑，所以右脸鼻孔稍靠上（这种细致程度让人惊讶！）。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_kazuki_front.png) 
            ![](./img/nose_mark_kazuki_front_vs_masahiko_basis.jpg)  


- **千夏(Chinatsu)**  
    - basis:  
        - 图片: (14_016_2), 12_159_3, 12_141_0,  
        - 特点： 成组的图片数量最多。鼻侧线圆滑。相比于雅彦basis，鼻孔间距小。           
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_chinatsu_basis.png) 
            ![](./img/nose_mark_chinatsu_basis_vs_masahiko_basis.jpg)  


- **麻衣(Mai)**  
    - basis:  
        - 图片: 12_159_3, 12_141_0,  
        - 特点： 成组的图片数量最多。相比于雅彦basis，鼻孔间距小。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_mai_basis.png) 
            ![](./img/nose_mark_mai_basis_vs_masahiko_basis.jpg)  


- **绫子(Aya)**  
    - basis:  
        - 图片: 02_189_4, 02_180_5, 02_177_3_ 
        - 特点： 成组的图片数量最多。鼻子稍短，鼻头大，鼻翼明显。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_aya_basis.png) 
            ![](./img/nose_mark_aya_basis_vs_masahiko_basis.jpg)  


- **典子(Tenko)**  
    - basis:  
        - 图片: 02_180_6, 02_178_7,  
        - 特点： 成组的图片数量最多。鼻子稍短，鼻头大，鼻翼明显。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_tenko_basis.png) 
            ![](./img/nose_mark_tenko_basis_vs_masahiko_basis.jpg)  


- **古屋公弘(Kimihiro)**  
    - front:  
        - 图片: 09_051_6,  
        - 特点： 相比于雅彦basis，鼻孔更朝下。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色), 几乎一致：  
            ![](./img/nose_front_kimihiro_front.png) 
            ![](./img/nose_mark_kimihiro_front_vs_masahiko_basis.jpg)  


- **古屋朋美(Tomomi)**  
    - front:  
        - 图片: 09_047_1,  
        - 特点： 和雅彦basis几乎一致。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_tomomi_front.png) 
            ![](./img/nose_mark_tomomi_front_vs_masahiko_basis.jpg)  

    - top:  
        - 图片: 09_042_6_r,  
        - 特点： 镜头俯视。         
        - 叠加：  
            ![](./img/nose_front_tomomi_top.png)  


- **八不(Hanzu)**  
    - top:  
        - 图片: 02_183_7,  
        - 特点： 镜头稍微俯视。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/nose_front_hanzu_top.png) 
            ![](./img/nose_mark_hanzu_top_vs_masahiko_basis.jpg)  


- **哲(Te)**  
没有合适的图。  


------

- **排列如下(角色鼻子, 放大2倍)**:  
![](img/nose_front_x2/nose_front_masahiko.jpg) 
![](img/nose_front_x2/nose_front_masami.jpg) 
![](img/nose_front_x2/nose_front_shion.jpg) 
![](img/nose_front_x2/nose_front_shya.jpg) 
![](img/nose_front_x2/nose_front_sora.jpg) 
![](img/nose_front_x2/nose_front_yukari.jpg) 
![](img/nose_front_x2/nose_front_yoko.jpg) 
![](img/nose_front_x2/nose_front_hiromi.jpg) 
![](img/nose_front_x2/nose_front_kaoru.jpg) 
![](img/nose_front_x2/nose_front_ejima.jpg) 
![](img/nose_front_x2/nose_front_tatsumi.jpg) 
![](img/nose_front_x2/nose_front_makoto.jpg) 
![](img/nose_front_x2/nose_front_director.jpg) 
![](img/nose_front_x2/nose_front_saki.jpg) 
![](img/nose_front_x2/nose_front_asagi.jpg) 
![](img/nose_front_x2/nose_front_cameraman.jpg) 
![](img/nose_front_x2/nose_front_yoriko.jpg) 
![](img/nose_front_x2/nose_front_mori.jpg) 
![](img/nose_front_x2/nose_front_kazuko.jpg) 
![](img/nose_front_x2/nose_front_grandpa.jpg) 
![](img/nose_front_x2/nose_front_susumu.jpg) 
![](img/nose_front_x2/nose_front_kenji.jpg) 
![](img/nose_front_x2/nose_front_ykma.jpg) 
![](img/nose_front_x2/nose_front_risa.jpg) 
![](img/nose_front_x2/nose_front_mika.jpg) 
![](img/nose_front_x2/nose_front_akane.jpg) 
![](img/nose_front_x2/nose_front_nishina.jpg) 
![](img/nose_front_x2/nose_front_aoi.jpg) 
![](img/nose_front_x2/nose_front_grandma.jpg) 
![](img/nose_front_x2/nose_front_fumiya.jpg) 
![](img/nose_front_x2/nose_front_reiko.jpg) 
![](img/nose_front_x2/nose_front_matsu.jpg) 
![](img/nose_front_x2/nose_front_shoko.jpg) 
![](img/nose_front_x2/nose_front_tooru.jpg) 
![](img/nose_front_x2/nose_front_kyoko.jpg) 
![](img/nose_front_x2/nose_front_kazuma.jpg) 
![](img/nose_front_x2/nose_front_kazuki.jpg) 
![](img/nose_front_x2/nose_front_chinatsu.jpg) 
![](img/nose_front_x2/nose_front_mai.jpg) 
![](img/nose_front_x2/nose_front_aya.jpg) 
![](img/nose_front_x2/nose_front_tenko.jpg) 
![](img/nose_front_x2/nose_front_kimihiro.jpg) 
![](img/nose_front_x2/nose_front_tomomi.jpg) 
![](img/nose_front_x2/nose_front_hanzu.jpg) 
![](img/nose_front_x2/nose_front_te.jpg) 


- **把类似的鼻子分组**  
同一{}的鼻子被认为是同样的。例如，在该合并分类中，可以认为masahiko和asagi的鼻子是一样的。
    - {masahiko, masami front, shya front, asagi, fumiya, tooru ， kazuki, ejima, nishina, susumu, aoi, matsu, kimihiro}  
      ![](img/nose_front_x2/nose_front_masahiko.jpg) 
      ![](img/nose_front_x2/nose_front_masami.jpg) 
      ![](img/nose_front_x2/nose_front_shya.jpg) 
      ![](img/nose_front_x2/nose_front_asagi.jpg) 
      ![](img/nose_front_x2/nose_front_fumiya.jpg) 
      ![](img/nose_front_x2/nose_front_tooru.jpg) 
      ![](img/nose_front_x2/nose_front_kazuki.jpg) 
      ![](img/nose_front_x2/nose_front_ejima.jpg) 
      ![](img/nose_front_x2/nose_front_nishina.jpg) 
      ![](img/nose_front_x2/nose_front_susumu.jpg) 
      ![](img/nose_front_x2/nose_front_aoi.jpg) 
      ![](img/nose_front_x2/nose_front_matsu.jpg) 
      ![](img/nose_front_x2/nose_front_kimihiro.jpg)  
    
    - {shion, risa}  
      ![](img/nose_front_x2/nose_front_shion.jpg) 
      ![](img/nose_front_x2/nose_front_risa.jpg)  
    
    - {yukari basis, hiromi, kaoru front}  
      ![](img/nose_front_x2/nose_front_yukari.jpg) 
      ![](img/nose_front_x2/nose_front_hiromi.jpg) 
      ![](img/nose_front_x2/nose_front_kaoru.jpg)   
    
    - {sora}    
      ![](img/nose_front_x2/nose_front_sora.jpg)  
    
    - {makoto, yoko, saki, mori, tomomi}   
      ![](img/nose_front_x2/nose_front_makoto.jpg)  
      ![](img/nose_front_x2/nose_front_yoko.jpg) 
      ![](img/nose_front_x2/nose_front_saki.jpg) 
      ![](img/nose_front_x2/nose_front_mori.jpg) 
      ![](img/nose_front_x2/nose_front_tomomi.jpg)  
    
    - {yoriko, mika, chinatsu, mai}  
      ![](img/nose_front_x2/nose_front_yoriko.jpg) 
      ![](img/nose_front_x2/nose_front_mika.jpg) 
      ![](img/nose_front_x2/nose_front_chinatsu.jpg) 
      ![](img/nose_front_x2/nose_front_mai.jpg) 
    
    - {ykma}  
      ![](img/nose_front_x2/nose_front_ykma.jpg)  
    
    - {akane, reiko} (鼻子稍短)  
      ![](img/nose_front_x2/nose_front_akane.jpg) 
      ![](img/nose_front_x2/nose_front_reiko.jpg)  
    
    - {kyoko}  
      ![](img/nose_front_x2/nose_front_kyoko.jpg)  
    
    - {aya, tenko}  
      ![](img/nose_front_x2/nose_front_aya.jpg) 
      ![](img/nose_front_x2/nose_front_tenko.jpg)  
    
    - {tatsumi}  
      ![](img/nose_front_x2/nose_front_tatsumi.jpg) 
    
    - {director, cameraman}  
      ![](img/nose_front_x2/nose_front_director.jpg) 
      ![](img/nose_front_x2/nose_front_cameraman.jpg) 
    
    - {kazuko, kenji} (比导演/摄像师的鼻子大)  
      ![](img/nose_front_x2/nose_front_kazuko.jpg) 
      ![](img/nose_front_x2/nose_front_kenji.jpg)  
    
    - {hanzu}  
      ![](img/nose_front_x2/nose_front_hanzu.jpg) 
    

- **进一步减少分组**  
同一{}的鼻子被认为是同样的。例如，在该合并分类中，可以认为masahiko和kyoko的鼻子是一样的。
    - {masahiko, masami front, shya front, asagi, fumiya, tooru ， kazuki, ejima, nishina, susumu, aoi, matsu, kimihiro, shion, risa, yukari basis, hiromi, kaoru front, yoko, saki, makoto, mori, tomomi, yoriko, mika, chinatsu, mai, ykma, sora, kyoko, akane, reiko}  
      ![](img/nose_front_x2/nose_front_masahiko.jpg) 
      ![](img/nose_front_x2/nose_front_masami.jpg) 
      ![](img/nose_front_x2/nose_front_shya.jpg) 
      ![](img/nose_front_x2/nose_front_asagi.jpg) 
      ![](img/nose_front_x2/nose_front_fumiya.jpg) 
      ![](img/nose_front_x2/nose_front_tooru.jpg) 
      ![](img/nose_front_x2/nose_front_kazuki.jpg) 
      ![](img/nose_front_x2/nose_front_ejima.jpg) 
      ![](img/nose_front_x2/nose_front_nishina.jpg) 
      ![](img/nose_front_x2/nose_front_susumu.jpg) 
      ![](img/nose_front_x2/nose_front_aoi.jpg) 
      ![](img/nose_front_x2/nose_front_matsu.jpg) 
      ![](img/nose_front_x2/nose_front_kimihiro.jpg) 
      ![](img/nose_front_x2/nose_front_shion.jpg) 
      ![](img/nose_front_x2/nose_front_risa.jpg) 
      ![](img/nose_front_x2/nose_front_yukari.jpg) 
      ![](img/nose_front_x2/nose_front_hiromi.jpg) 
      ![](img/nose_front_x2/nose_front_kaoru.jpg) 
      ![](img/nose_front_x2/nose_front_yoko.jpg) 
      ![](img/nose_front_x2/nose_front_saki.jpg) 
      ![](img/nose_front_x2/nose_front_makoto.jpg) 
      ![](img/nose_front_x2/nose_front_mori.jpg) 
      ![](img/nose_front_x2/nose_front_tomomi.jpg) 
      ![](img/nose_front_x2/nose_front_yoriko.jpg) 
      ![](img/nose_front_x2/nose_front_mika.jpg) 
      ![](img/nose_front_x2/nose_front_chinatsu.jpg) 
      ![](img/nose_front_x2/nose_front_mai.jpg) 
      ![](img/nose_front_x2/nose_front_ykma.jpg) 
      ![](img/nose_front_x2/nose_front_sora.jpg) 
      ![](img/nose_front_x2/nose_front_kyoko.jpg) 
      ![](img/nose_front_x2/nose_front_akane.jpg) 
      ![](img/nose_front_x2/nose_front_reiko.jpg)  
    
    - {aya, tenko, director, cameraman, kazuko, kenji, }  
      ![](img/nose_front_x2/nose_front_aya.jpg) 
      ![](img/nose_front_x2/nose_front_tenko.jpg) 
      ![](img/nose_front_x2/nose_front_director.jpg) 
      ![](img/nose_front_x2/nose_front_cameraman.jpg) 
      ![](img/nose_front_x2/nose_front_kazuko.jpg) 
      ![](img/nose_front_x2/nose_front_kenji.jpg) 
    
    - {tatsumi}  
      ![](img/nose_front_x2/nose_front_tatsumi.jpg) 
    
    - {hanzu}  
      ![](img/nose_front_x2/nose_front_hanzu.jpg) 



**参考资料**：  
1. [Differences between Caucasian and Asian attractive faces](https://www.researchgate.net/publication/318548459), Seung Chul Rhee, 2017.  
2. Form of the Head and Neck, 2021, Uldis Zarins.  
3. [MurphAinmire's Nose Study - deviantart](https://www.deviantart.com/murphainmire/art/Nose-Study-691739426)  
4. [How to Draw a Nose - tuts+]()  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


以下内容仅是[《漫画作品中画面的细节度的一种衡量方法》](./readme.md)一文的简单总结。  


## 总结  

我的初衷是：首先，尝试衡量漫画作品中画面的写实程度；然后，以AH的写实程度来衡量其他作品的写实程度；最后，定义AH的写实程度为1、单位为"Hojo"，以致敬北条司。  

实际中，首先，由于一些困难，所以将"画面的写实程度"用"画面的细节度"来近似地衡量，进而寻找后者的衡量方法，并最终诉诸于线条数和着色数。由于某些类型的线条数和着色数的统计方法并不精确，所以该方法是半定量的。  

其次，将该方法应用于北条司的部分作品后，结果能反映出这些作品的画面有不同的细节度（如下表）。  

|           |                                                 |  
| --------- |  -----------------------------------------------|  
| **作品W**  | **作品W与AH的细节度的比值** (单位: Hojo) |  
| CE(前期) | 0.70                       |  
| CE(中期) | 0.72                       |  
| CE(后期) | 0.69                       |  
|         |                            |  
| CH(前期) | 0.48                       |  
| CH(中期) | 0.66                       |  
| CH(后期) | 0.91                       |  
|         |                            |  
| FC(前期) | 0.59                       |  
| FC(中期) | 0.71                       |  
| FC(后期) | 0.92                       |  
|         |                            |  
| AH      | 1.00                       |  

最后，因为考虑到如下因素：  

- 北条司的漫画作品是业内公认的写实风格的代表;  
- 因为AH是北条司连载时间最长的作品，所以AH能代表北条司的绘画风格;  
- 有理由假设："所画出的物体的逼真程度"等价于“所画出的物体的表面的细节度”；  
- 类似于用国际单位制"千克"来衡量某个物体的质量，即，因为"某物体的质量"相对于"国际千克原器的质量"的比值是X，所以简称"某物体的质量为X千克"。  

所以，我有一个提议：  

- 以AH的画面的细节度为基准，将"作品W相对于作品AH的细节度的比值"称为"作品W的画面的细节度"，单位为"Hojo"（简称"H"），以致敬北条司。  

例如，上表的结果表明"FC前期的细节度为0.59H"。如果假设细节度等价于写实程度，那么可认为"FC前期的写实程度为0.59H"。  

   

--------------------------------------------------   
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  

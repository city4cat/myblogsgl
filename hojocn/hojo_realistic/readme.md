（如果文中的数学公式无法显示，可下载[本文的PDF版](./readme.pdf)。）  

# 漫画作品中画面的细节度的一种衡量方法

## 1. 动机&摘要  

笔者的初衷：寻找一种方法来衡量漫画作品的画面的逼真程度（写实程度），并量化该方法。基于此，鉴于北条司的绘画风格和成就，以及《Angel Heart》是其连载时间最长的作品，所以可将《Angel Heart》的画面的逼真程度作为基准，从而衡量其他漫画作品的画面的逼真程度。进一步，可定义《Angel Heart》的画面逼真程度为1、单位为"Hojo"，以致敬北条司。  

一方面，所画出的物体的逼真程度与所画出的物体的形状有关，但困难是未找到简单的方法来衡量所画出的物体的形状是否逼真；另一方面，如果能建立一些合理的假设，则"画面的细节度"能近似地衡量"画面的逼真程度"（即下文的C6）；所以，基于这些假设，笔者转而研究画面的细节度，并将其量化为线条计数和着色计数。最后，该方法被用来比较了北条司的几个不同漫画作品的细节度。  


## 2. 术语和符号     

- CE： 北条司的作品《Cat's Eye》的缩写。  

- CH： 北条司的作品《City Hunter》的缩写。  

- FC： 北条司的作品《Family Compo》的缩写。  

- AH： 北条司的作品《Angel Heart》的缩写。  

- pc: "property of the character"的缩写。  

- ps: "property of the scene"的缩写。  

- panel/分格/镜头/分镜/  
本文不加区分地使用这些词表示漫画页面中的一个方格。例如，下图左图有4个panel(第4个panel无边框)，如右图红框、蓝框所示。  
![](img/ah_01_0001.jpg) 
![](img/ah_01_0001_mod.jpg)  
漫画里panel多为矩形，少数为不规则凸多边形，未见有凹多边形。  

- 笔触（Stroke）  
    - 绘画中的笔法。又称肌理，常指油画和水粉画中运笔的痕迹。也指作画过程中画笔接触画面时所留下的痕迹。([笔触 - Baike](https://baike.baidu.com/item/笔触/4155298))    
    - Stroke: 1) a single movement of a pen or brush when you are writing or painting. 2) a line made by a pen or brush.  ([Stroke - LDict English Dictionary]())  

- 线条（Line）  
绘画时描画的曲线、直线、粗线、细线等的统称。  ([线条 - 汉文学网](https：//cd.hwxnet.com))  

- Darkness  
    - the quality of being dark in shade or color。 ([Darkness - Merriam-Webster](https://www.merriam-webster.com/dictionary/darkness))  
    - the direct opposite of lightness, is defined as a lack of illumination, an absence of visible light, or a surface that absorbs light, such as black or brown.  ([Darkness - wikipedia](https://en.wikipedia.org/wiki/Darkness))  

- Shade  
    - comparative darkness or obscurity owing to interception of the rays of light. ([Shade - Merriam-Webster](https://www.merriam-webster.com/dictionary/shade))  
    - In color theory, ... a shade is a mixture with black, which increases darkness. ([Tint, shade and tone](https://en.wikipedia.org/wiki/Tint,_shade_and_tone), [Tint v.s. Shade v.s. Tone in one image](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Tint-tone-shade.svg/260px-Tint-tone-shade.svg.png))  

- Shading  
    - Shading refers to the depiction of depth perception in ... illustrations (in visual art) by varying the level of darkness. ([Shading - Wikipedia](https://en.wikipedia.org/wiki/Shading))
    - the use of marking made within outlines to suggest three-dimensionality, shadow, or degrees of light and dark in a picture or drawing. ([Shading - Merriam-Webster](https://www.merriam-webster.com/dictionary/shading))  

- Texture(纹理):  
    - the visual or tactile surface characteristics and appearance of something. ([Texture - Merriam-Webster](https://www.merriam-webster.com/dictionary/texture))    
    - Smoothness, roughness, or bumpiness of the surface of an object.([Texture - wikipedia](https://en.wikipedia.org/wiki/Texture))  

- :=  
该符号表示"定义"。例如，"变量/表达式A := 表达式B"的含义为：变量/表达式A 被定义为 表达式B。  

- ==>  
该符号表示"推导出"。例如，"命题A ==> 命题B"的含义为：由命题A可以推导出命题B。  

- v[i]  
该符号表示向量v的分量i。例如，若v=(2, 3, 5), 则v[0]=2, v[1]=3, v[2]=5.  

- 正相关(Positive Correlation)  
"变量A正相关于变量B"是指A和B的变动方向相同--若A增大，则B随之增大；若A减小，则B随之减小。[^1]    

- Set(T): 类型为T的所有元素组成的集合。  

- \#S: 集合S中元素的个数。  

- 0和自然数所组成的集合:  
$$
\mathbb{N_0}
$$  

- 0和正实数所组成的集合:  
$$
\mathbb{R_{\ge0}}
$$  


## 3. 一些限定、定义、假设和推理  

第一，将研究对象限定为长期连载的商业黑白漫画(Manga)。原因是：一方面，若只考察画面的逼真程度，则素描、超真实感绘画作品显然比商业连载漫画更逼真。但这些绘画技巧很耗时，不适合漫画连载、不适合讲故事。例如，Oscar Ukonu[^2]的一幅画作要6周时间。另一方面，若只考虑漫画产出速度，那么画得越简单则画得越快。但画面过于简单，导致画面不逼真、读者少。这两方面因素相互制约。而长期连载的商业漫画能平衡这两方面的因素。如果没有特别说明，下文中的"漫画"均指长期连载的商业黑白漫画。        

长期连载的商业漫画有以下特点：  

- 因为长期连载，所以受欢迎程度高；  
- 因为是商业连载，所以漫画作者的截稿期短、时间紧迫。  

黑白漫画作品有以下特点：  

- 绝大多数画面为黑白色。  
- 作品中使用多种方法表现物体表面的shading，例如，排线阴影、交叉排线阴影、网点阴影、网点纹理。  
- 用不同灰度表现如下效果：  
    - 阴影。例如，头在衣服上的投影阴影、衣服褶皱的遮挡阴影(Ambient Occlusion)；    
    - 高光。例如，头发、衣服上的高光；  
    - 全反射。例如，茶几、窗玻璃的全反射效果；  
    - 二次反射。例如，茶杯在茶几上的倒影；  
    - 多次反射；  
    - 折射；  


第二，笔者认为"所画出的物体的逼真程度"同时包含以下两方面：  
(R1) 所画出的物体的形状的逼真程度。即，针对被画物体的形状，所画出的物体的线条能在多大程度上描绘出其变化。  
(R2) 所画出的物体的明暗的逼真程度。即，针对被画物体的表面的明暗、色彩，所画出的物体的表面着色能在多大程度上描绘出其变化。   
(注：由于本文面向黑白漫画，所以忽略(R2)中的色彩。）  
因为笔者暂未找到衡量(1)的方法，所以，退而求其次地认为如下假设是正确的：    
  
- (A1) "所画出的物体的逼真程度"等价于"所画出的物体的明暗的逼真程度"。   

A1的合理之处源于经验。A1的不合理之处是它忽略了上述(1)，例如，即便很多漫画角色的眼睛的画得很细致，但由于眼睛画得过大，所以依然显得不逼真。本文以下内容认为A1合理，即忽略了其不合理之处，这是本文的一个局限。  

由（R1）引出一个"线条(a line)"的定义： (D1)描画时，一次长时间的落笔至一次长时间的抬笔期间所画的线。其可能是一个点，也可能是具有以下两类特征组合的一条线：直线(Straight Line)/曲线(Curved Line)、实线/虚线。  

由（R2）引出一个"着色(区域)(an area of a shading)"的定义:  (D6)页面上所画的一个连通区域，该区域仅包含且仅包含下列特征之一：  

- 一个排线(Hatching)线条区域： (D3)页面上所画的一个连通区域，该区域有一组间隔相同或近似的排线；  
- 一个交叉排线(Crosshatching)线条区域： (D4)页面上所画的一个连通区域，该区域有一组间隔相同或近似的交叉排线；  
- 一个暗度(Darkness)区域： (D2)页面上所画的一个连通区域，该区域内有相同的暗度(darkness)；  
- 一个纹理(Texture)区域： (D5)页面上所画的一个连通区域或多个相近的非连通区域，这些区域内有相同的纹理；  

本文使用"Darkness"表示画面中某个区域的暗度。原因是： 1)研究对象限于黑白漫画、不考虑色彩。当画面中只有黑白色时，Shade约等于Darkness（见“术语和符号”一节）。 2）Shading和Shade的词义联系紧密，而一个Shading可以包含Darkness的渐变（见“术语和符号”一节）。但考虑到(R2)，本文后续把"一个含有不同暗度的区域"视为多个暗度区域，其中每个暗度区域内的暗度相同。  


第三，因为本文的初衷是关注画面的逼真程度，所以不关注画面中某些类型（如，对话框及其文字、特效、panel边框(frame)），而关注如下类型：      

- 头发(Hair)： 角色的头发。  
- 面部(Face)： 角色的面部五官、颈部、锁骨部。  
- 角色道具(Properties for the Character)：距离角色近的道具。例如，角色身上的服饰、角色手中或肩部的背包、角色手持的武器。     
- 场景道具(Properties for the Scene)：距离角色远的道具。例如，场景的背景(例如，楼、街道、行人、树木)，场景的前景（例如，树叶）。    


第四，因为所画出的线条和着色可以有助于或有害于表现所画出的物体的表面的逼真程度，所以可将所画出的线条和着色按逼真程度而分类为且仅分类为： 有利的、有害的、无用的(既无利也无害)。  


第五，基于经验，笔者认为如下假设是正确的：  

- (A2) "漫画作品的画面逼真度"正相关于"该漫画作品受欢迎程度"。  
- (A3) 若时间紧迫，则漫画家没时间在作品中画无用的线条和着色。  
- (A4) 若所画出的物体的线条数越多，则其细节度越高。    
- (A5) 若所画出的物体的表面所呈现出的色度(Shade)越多，则其细节度越高。  
(A4)、(A5)为后续内容做铺垫。    

由以上假设可以得到以下推论：  

- (A2) ==> (C1)若漫画作品中有害的线条和着色占比多，则受欢迎程度低。 <==>  (C1)若作品受欢迎程度高，则漫画作品中有害的线条和着色占比少。  
- 长期连载的商业漫画 + (C1) ==> (C2)若是长期连载的商业漫画，则有害的线条和着色占比少。  

- 长期连载的商业漫画 + (A3) ==> (C3)若是长期连载的商业漫画，则无用的线条和着色占比少。  
- 线条和着色按逼真程度分类 + (C2) + (C3) ==> (C4)若是长期连载的商业漫画，则有利的线条和着色占比多。  

- (C4) ==> 若是长期连载的商业漫画，则其大部分线条和着色有助于表现所画出的物体的表面的逼真程度；则当其线条和着色增多时，所画出的物体的表面的越逼真--即，  
  (C5)若是长期连载的商业漫画，则其"画面中的线条和着色数目"正相关于"所画出的物体的表面的逼真程度"。  
- (C5) + (A1) ==> (C6)若是长期连载的商业漫画，则其"画面中的线条和着色数目"正相关于"所画出的物体的逼真程度"。  

基于(C6)，笔者用"画面中的线条和着色数目"衡量长期连载的商业漫画的画面的逼真程度。  


## 4. 方法A：  

基于上述所画的不同类型，定义如下量:  

- G_h, S_h: 头发的线条、头发的着色；  
- G_f, S_f: 面部的线条、面部的着色；  
- G_pc, S_pc: 角色道具的线条、角色道具的着色；  
- G_ps, S_ps: 场景道具的线条、角色道具的着色；  

对于画面区域A，定义如下函数：  

- A中的G_h,G_f,G_pc,G_ps,S_h,S_f,S_pc,S_ps定义为:   
$$
\begin{align*} 
G\_h :  Polygon & \longmapsto Set(Line) \\ 
              A & \longrightarrow G\_h(A) := lines\ of\ hairs\ in\ A \\  
G\_f :  Polygon & \longmapsto Set(Line) \\ 
              A & \longrightarrow G\_f(A) := lines\ of\ faces\ in\ A \\  
G\_pc : Polygon & \longmapsto Set(Line) \\ 
              A & \longrightarrow G\_pc(A) := lines\ of\ properties of characters\ in\ A \\  
G\_ps : Polygon & \longmapsto Set(Line) \\ 
              A & \longrightarrow G\_ps(A) := lines\ of\ properties of scenes\ in\ A \\  
S\_h :  Polygon & \longmapsto Set(Shading) \\
              A & \longrightarrow S\_h(A) := shadings\ of\ hairs\ in\ A  \\  
S\_f :  Polygon & \longmapsto Set(Shading) \\  
              A & \longrightarrow S\_f(A) := shadings\ of\ faces\ in\ A \\  
S\_pc : Polygon & \longmapsto Set(Shading) \\
              A & \longrightarrow S\_pc(A) := shadings\ of\ properties of characters\ in\ A \\
S\_ps : Polygon & \longmapsto Set(Shading) \\
              A & \longrightarrow S\_ps(A) := shadings\ of\ properties of scenes\ in\ A \\
\end{align*}
$$  

- A中的形状细节G、着色细节S定义为:   
$$
\begin{align*} 
G : Polygon & \longmapsto Set(Line)^4 \\
          A & \longrightarrow G(A) := ( G\_h(A), G\_f(A), G\_pc(A), G\_ps(A) ) \\    
S : Polygon & \longmapsto Set(Shading)^4 \\
          A & \longrightarrow S(A) := ( S\_h(A), S\_f(A), S\_pc(A), S\_ps(A) ) \\    
\end{align*}
$$  

- A中的"细节(Detail)"定义为：  
$$
\begin{align*} 
detail : Polygon & \longmapsto Set(Line)^4 \times Set(Shading)^4 \\
          A & \longrightarrow detail(A) := ( G(A), S(A) ) \\    
\end{align*}
$$  

- A中的G,S分别所包含的元素的数量定义为dG,dS:   
$$
\begin{align*} 
dG : Polygon & \longmapsto \mathbb{N_0}^4 \\
           A & \longrightarrow dG(A) := ( \#G\_h(A), \#G\_f(A), \#G\_pc(A), \#G\_ps(A) ) \\     
dS : Polygon & \longmapsto \mathbb{N_0}^4 \\
           A & \longrightarrow dS(A) := ( \#S\_h(A), \#S\_f(A), \#S\_pc(A), \#S\_ps(A) ) \\     
\end{align*}
$$    

- 定义空间  
$$
M(V,n):=\mathbb{V}^n
$$    
M(V,n)中的二元关系">="和">"定义如下：  
$$
\begin{align*}  
& 给定x=(x_1, x_2, ..., x_n), y=(y_1, y_2, ..., y_n) \in M(V,n),   \\  
& \begin{aligned}
x \ge y & 定义为: x_i \ge y_i  \quad (i, j=1, 2, ..., n)  \\  
x \gt y & 定义为:   x \ge y, 且至少存在一个j，使得x_j \gt y_j \quad (i, j=1, 2, ..., n)  \\  
\end{aligned}
\end{align*}
$$    
例如，M(R,3)中，(1,2,3)>(0,2,3)>(0,1,2)。  

- A中的"细节度(degree of detail)"dd定义为:   
$$
\begin{align*} 
degree : Set(Line)^4 \times Set(Shading)^4 & \longmapsto M(V,n) \\
            ((x0,x1,x2,x3), (x4,x5,x6,x7)) & \longrightarrow  ?  \\
dd : Polygon & \longmapsto M(V,n) \\  
           A & \longrightarrow dd(A) := degree(detail(A)) 
\end{align*}
$$    
函数degree可根据不同情况有不同的实现。  


### 4.1 细化

首先，为了简化后续的描述，先定义如下符号和函数：  

- avgc(v): 计算向量v各个分量的平均值。  
例如，若v=(1, 3, 8)，则avgc(v)的结果为4.    

- avg(f, Set(x)): 函数f(x)在集合Set(x)上的平均值。   
$$
\begin{align*} 
avg : (X \longmapsto Y)\times Set(X) & \longmapsto Y \\
                                 f,S & \longrightarrow avg(f, S) := \sum_{each\ x\ in\ S}{f(x)} \quad / \quad \#S   \\
\end{align*}
$$    

- U /v V:  "/v"定义为向量U和V的各分量之比；若U和V的分量仍是向量，则递归地做下去。U和V需是相同维度的向量，结果也是相同维度的向量。    
例如，若U=(3, 5, 0), V=(1, 2, 3), 则U /v V的结果为向量(3, 2.5, 0).  

其次，令W为某漫画作品、p为W中的某一个panel（panel属于多边形,一般为矩形）。定义如下若干函数：  

- p中的线条为G(p)。G(p)的4个分量为G_h(p), G_f(p), G_pc(p), G_ps(p) .  

- p中的着色为S(p)。S(p)的4个分量为S_h(p), S_f(p), S_pc(p), S_ps(p) .  

- p的细节为detail(p) = ( G(p), S(p) ).  

- p的细节度为dd(p) = degree(detail(p))。其中函数degree的实现如下：    
$$
\begin{align*} 
degree : Set(Line)^4 \times Set(Shading)^4 & \longmapsto M(\mathbb{N_0},8) \\
            ((g0,g1,g2,g3), (s0,s1,s2,s3)) & \longrightarrow ( \#g0,\#g1,\#g2,\#g3, \#s0,\#s1,\#s2,\#s3 )  \\
\end{align*}
$$    
M(V,n)中的二元关系">"定义了细节度dd()的大小关系，">"满足(A4),(A5)。    

- 若有一组panel P，"P的细节度的平均值"定义为：  
$$
\begin{align*}
AverageDegree : Set(Polygon) & \longmapsto     {}&& M(\mathbb{N_0},8) \\
                           P & \longrightarrow {}&& AverageDegree(P)  \\
&&&\begin{aligned}
:&= avg(dd, P)         \\
 &= avg((\#G, \#S), P) \\
 &= avg((\#G\_h, \#G\_f, \#G\_pc, \#G\_ps, \#S\_h, \#S\_f, \#S\_pc, \#S\_ps), P) \\  
 &= (     avg(\#G\_h,P), avg(\#G\_f,P), avg(\#G\_pc,P), avg(\#G\_ps,P),          \\
 & \qquad avg(\#S\_h,P), avg(\#S\_f,P), avg(\#S\_pc,P), avg(\#S\_ps,P)  )        \\  
\end{aligned}
\end{align*}
$$    
该定义等价于：  
$$
\begin{align*}
AverageDegree : Set(Polygon) & \longmapsto     {}&& M(\mathbb{N_0},8) \\
                           P & \longrightarrow {}&& AverageDegree(P) \\
&&&\begin{aligned}
:&= (  \#\bigcup_{each\ p\ in\ P}  G(p),  \qquad  \# \bigcup_{each\ panel\ p\ in\ P}  S(p) ) \quad / \quad \#P \\  
:&= (  \#\bigcup_{each\ p\ in\ P}  G\_h(p), 
       \#\bigcup_{each\ p\ in\ P}  G\_f(p), 
       \#\bigcup_{each\ p\ in\ P}  G\_pc(p), 
       \#\bigcup_{each\ p\ in\ P}  G\_ps(p),   \\  
 & \qquad \#\bigcup_{each\ p\ in\ P}  S\_h(p), 
       \#\bigcup_{each\ p\ in\ P}  S\_f(p), 
       \#\bigcup_{each\ p\ in\ P}  S\_pc(p), 
       \#\bigcup_{each\ p\ in\ P}  S\_ps(p) ) \quad / \quad \#P \\  
 &= (  \sum_{each\ p\ in\ P}  \#G\_h(p), 
       \sum_{each\ p\ in\ P}  \#G\_f(p), 
       \sum_{each\ p\ in\ P}  \#G\_pc(p), 
       \sum_{each\ p\ in\ P}  \#G\_ps(p),      \\  
 & \qquad \sum_{each\ p\ in\ P}  \#S\_h(p), 
       \sum_{each\ p\ in\ P}  \#S\_f(p), 
       \sum_{each\ p\ in\ P}  \#S\_pc(p), 
       \sum_{each\ p\ in\ P}  \#S\_ps(p) ) \quad / \quad \#P \\  
 &= (     avg(\#G\_h,P), avg(\#G\_f,P), avg(\#G\_pc,P), avg(\#G\_ps,P),    \\
 & \qquad avg(\#S\_h,P), avg(\#S\_f,P), avg(\#S\_pc,P), avg(\#S\_ps,P)  )  \\  
\end{aligned}
\end{align*}
$$    
若将漫画作品W视为一组panel，则"作品W的细节度"为AverageDegree(W)。  

- 若有两组panel A和B，则"A相对于B的细节度的比值"定义为：  
$$
\begin{align*}
RelativeDegree : Set(Polygon) \times Set(Polygon) & \longmapsto     {}&& M(\mathbb{R_{\ge0}},8) \\
                                           (A, B) & \longrightarrow {}&& RelativeDegree(A, B) \\
&&&\begin{aligned}
:&= (  AverageDegree(A) \quad /v \quad AverageDegree(B) )  \\    
:&= (     \frac{avg(\#G\_h,A) }{avg(\#G\_h,B) }, 
          \frac{avg(\#G\_f,A) }{avg(\#G\_f,B) }, 
          \frac{avg(\#G\_pc,A)}{avg(\#G\_pc,B)}, 
          \frac{avg(\#G\_ps,A)}{avg(\#G\_ps,B)},  \\ 
 & \qquad \frac{avg(\#S\_h,A) }{avg(\#S\_h,B) }, 
          \frac{avg(\#S\_f,A) }{avg(\#S\_f,B) }, 
          \frac{avg(\#S\_pc,A)}{avg(\#S\_pc,B)}, 
          \frac{avg(\#S\_ps,A)}{avg(\#S\_ps,B)} ) \\
\end{aligned}
\end{align*}
$$    
该函数值为一个(4+4)维向量，其前四个分量表示"A的平均G的各分量"与"B的平均G的各分量"之比，其后四个分量表示"A的平均S的各分量"与"B的平均S的各分量"之比。  
注: 因为RelativeDegree(A,B)的各个分量均为比值，所以"其各分量的和"以及"其各分量的均值"是有意义的，即下述RelativeDegreeSimplified(A, B)和RelativeDegreeSimplified2(A, B)是有意义的。     

- RelativeDegree(A, B)结果的粗粒化表示：  
$$
\begin{align*}
simplify: M(\mathbb{R_{\ge0}},8) & \longmapsto M(\mathbb{R_{\ge0}},2) \\
                          (u, v) & \longrightarrow  ( avgc(u), avgc(v) )  \\
\\
RelativeDegreeSimplified : Set(Polygon) \times Set(Polygon) & \longmapsto     M(\mathbb{R_{\ge0}},2) \\
                                                     (A, B) & \longrightarrow simplify(RelativeDegree(A, B)) \\
\end{align*}
$$  
该函数值类型是2维向量，其分量0表示"A的平均G的各分量的均值"与"B的平均G的各分量的均值"之比，其分量1表示"A的平均S的各分量的均值"与"B的平均S的各分量的均值"之比；或者其分量0表示"**平均而言，A、B的G之比**"，其分量1表示"**平均而言，A、B的S之比**"。  
注：该函数是为了简化RelativeDegree(A, B)的结果的表示。例如，若RelativeDegree(A，B)=((0.1, 0.2, 0.3, 0.4), (0.5, 0.6, 0.7, 0.8))， 则RelativeDegreeSimplified(A, B) = (0.25, 0.65).      

- RelativeDegree(A, B)结果的进一步粗粒化表示：  
$$
\begin{align*}
simplify2: M(\mathbb{R_{\ge0}},2) & \longmapsto \mathbb{R_{\ge0}} \\
                         (y0, y1) & \longrightarrow avgc((y0,y1)) \\
\\
RelativeDegreeSimplified2 : Set(Polygon) \times Set(Polygon) & \longmapsto     \mathbb{R_{\ge0}} \\
                                                      (A, B) & \longrightarrow simplify2( simplify( RelativeDegree(A, B) ) ) \\
\end{align*}
$$    
该函数值类型是标量，其含义为：A、B的"'(平均)G和(平均)S的各分量之比'的均值"，或"**平均而言，A、B的细节度之比**"。    
注：该函数是为了在RelativeDegreeSimplified(A, B)基础上进一步简化结果的表示。例如，若RelativeDegreeSimplified(A, B) = (0.25, 0.65)， 则RelativeDegreeSimplified2(A, B) = 0.45.      


#### 4.1.1 一个提议  

因为考虑到如下因素：  

- 北条司的漫画作品是业内公认的写实风格的代表;  
- 因为AH是北条司连载时间最长的作品，所以AH能代表北条司的绘画风格;  
- 有理由假设："所画出的物体的逼真程度"等价于“所画出的物体的表面的细节度”（即上文的A1)；  
- 类似于用国际单位制"千克"来衡量某个物体的质量，即，因为"某物体的质量"相对于"国际千克原器的质量"的比值是X，所以简称"某物体的质量为X千克"。  

所以，笔者提议：  

- (P1) 以AH的画面的细节度为基准，将"作品W相对于作品AH的细节度的比值"称为"作品W的画面的细节度"，单位为"Hojo"（简称"H"），以致敬北条司。  

例如: 给定作品W，若 RelativeDegree(W,AH)=((0.1, 0.2, 0.3, 0.4), (0.5, 0.6, 0.7, 0.8))，  
则 RelativeDegreeSimplified(W,AH)=(0.25, 0.65)， RelativeDegreeSimplified2(W,AH)=0.45。  
则可以称"作品W的画面的细节度为(0.25, 0.65)H或0.45H"。   



### 4.2 统计数据  
#### 4.2.1 统计方法
对于panel p中某物体，G_h(p), S_h(p), G_f(p), S_f(p), G_pc(p), S_pc(p), G_ps(p), S_ps(p)的统计方法如下：  

首先，以下的计数规则适用于所有细节。  

- 形状细节G的通用的计数规则：  
    - 若有n个笔触，则计数+n。例如：  
        ![](img/14_003_0__crop.jpg) 
        ![](img/14_003_0__crop_G_f.jpg)  
    - 物体的边沿线条(edge/fold)"。例如：    
        ![](img/ch_35_013_3_crop0.jpg) 
        ![](img/ch_35_013_3_crop0__fold.jpg)  
    - 交叉排线。交叉排线的视觉复杂度更高，所以计数应该更大。例如：    
        ![](img/edge_sequal.jpg) 
        ![](img/edge_cross.jpg)  
    - 若有表示物体运动的n个线条，则计数+n。例如：  
        ![](img/ah_01_018_5__crop0.jpg) 
        ![](img/ah_01_018_5__crop0_G_pc.jpg)  
        ![](img/ch_35_013_5__crop0.jpg) 
        ![](img/ch_35_013_5__crop0_G_pc.jpg)  
        ![](img/ch_35_015_4__crop0.jpg) 
        ![](img/ch_35_015_4__crop0_G_pc.jpg)  

- 着色细节S的通用的计数规则：  
    - 多个相连区域为同一着色，则计数仅+1；n个分离区域为同一着色，则计数+n；  
        ![](img/ce_01_086_1_crop.jpg) 
        ![](img/ce_01_086_1_crop__shading.jpg)  
    - 同一着色的连通区域，按凸多边形(Convex Hull)区域计数。  
        ![](img/ce_01_086_0_crop.jpg) 
        ![](img/ce_01_086_0_crop__shading.jpg)  
    - 若同一着色区域有深浅度不同的n种着色，则计数+n；  
        ![](img/ch_35_013_3_crop1.jpg) 
        ![](img/ch_35_013_3_crop1__shading.jpg)，  
        ![](img/ah_01_090_0_crop0.jpg) 
        ![](img/ah_01_090_0_crop0__shading.jpg)  
    - 无论排线阴影或网点阴影，若是渐变阴影，则计数+2。    
        ![](img/ah_01_007_1_crop0.jpg) 
        ![](img/ah_01_007_1_crop0__shading.jpg)  
    - 白色区域计数为0；若白色区域被深色区域包围（例如，眼部、衣服的高光），则计数；    
        ![](img/ch_35_012_5_crop0.jpg) 
        ![](img/ch_35_012_5_crop0__shading.jpg)  
    - 若有一个阴影排线区域，按计数+1；  
        ![](img/ch_35_013_3_crop0.jpg) 
        ![](img/ch_35_013_3_crop0__hatching.jpg)  
    - 若有n个文字，则计数+n。  
        ![](img/ah_01_011_0.jpg) 
        ![](img/ah_01_011_0__shading.jpg)  
    - 若一个纹理是网点着色（常见于衣服的纹理），则计数+1.  
        ![](img/ah_01_006_1_crop0.jpg) 
        ![](img/ah_01_006_1_crop0__shading.jpg)  
    - 若物体A在物体B上投下阴影，则A（而不是B）的阴影计数+1.  
    - 若有一个表示物体运动效果(如Motion Blur)的着色，则计数+1。例如：  
        ![](img/ah_01_020_5__crop0.jpg) 
        ![](img/ah_01_020_5__crop0_S_pc.jpg)  
        ![](img/ch_35_013_5__crop0.jpg) 
        ![](img/ch_35_013_5__crop0_S_pc.jpg)  

其次，不同类型物体还有各自的细节度的计数规则。如下：    

- G_h(p)： panel p中所有的头发的形状细节度；  
衡量方法是以下所有条目的计数之和：    
    - 头发轮廓边缘的线条数之和。  
        ![](img/ch_35_012_6_crop0.jpg) 
        ![](img/ch_35_012_6_crop0__G_h.jpg)  

- S_h(p)： panel p中所有的头发的着色细节度；   
衡量方法是以下所有条目的计数之和：    
    - 头发所有的高光区域的(一侧)边缘的线条数之和。  
        ![](img/ch_35_012_6_crop0.jpg) 
        ![](img/ch_35_012_6_crop0__S_h.jpg)  
    - 若头发为白色，则计数头发轮廓内的黑色线条数之和。  
        ![](img/ch_35_012_6_crop1.jpg) 
        ![](img/ch_35_012_6_crop1__S_h.jpg)  

- G_f(p)：panel p中所有的面部的形状细节度。包括：眉、眼部、鼻、嘴、脸庞、耳、颈部、锁骨。           
衡量方法是以下所有条目的计数之和：    
    - 若有一条面部皱纹，则计数+1。  
    - 若一根眉毛有部分区域绘制为n条线，则其计数+n。  
        ![](img/ch_35_012_5_crop1.jpg) 
        ![](img/ch_35_012_5_crop1__G_f_0.jpg)  
    - 上眼线、下眼线。每条线，计数+1；若线条粗，则按2倍或3倍计数。  
        ![](img/ch_35_012_6_crop0.jpg) 
        ![](img/ch_35_012_6_crop0__G_h_1.jpg)  
        ![](img/ch_35_012_5_crop2.jpg) 
        ![](img/ch_35_012_5_crop2__G_f.jpg)  
        ![](img/ch_35_012_4_crop0.jpg) 
        ![](img/ch_35_012_4_crop0__G_f_1.jpg) 
    - 上睫毛、下睫毛。若有n条线，则计数+n。  
        ![](img/ch_35_012_4_crop0.jpg) 
        ![](img/ch_35_012_4_crop0__G_f_0.jpg)  
    - 眼仁(瞳孔+虹膜)。若画有瞳孔，则每个瞳孔计数+1。若画有高光亮斑，则每个高光亮斑计数+1。  
        ![](img/ch_35_012_4_crop0.jpg) 
        ![](img/ch_35_012_4_crop0__G_f_2.jpg)  
    - 嘴唇的纹理。若嘴唇的纹理画为n条线（如下图红色线条），则计数+n。  
        ![](img/ch_35_012_4_crop1.jpg) 
        ![](img/ch_35_012_4_crop1__G_f_0.jpg)  

- S_f(p)：panel p中所有的面部的着色细节度。包括：眉、眼部、鼻、嘴、脸庞、耳、颈部、锁骨。     
衡量方法是以下所有条目的计数之和：   
    - 若一根眉毛有部分区域涂黑，则计数+1。  
        ![](img/ch_35_012_5_crop1.jpg) 
        ![](img/ch_35_012_5_crop1__S_f_0.jpg)  
    - 若一个瞳孔有着色，则计数+1；若一个虹膜有着色或纹理线，则计数+1； 若一个虹膜其他部位有黑色，则计数+1；   
        ![](img/ch_35_012_4_crop0.jpg) 
        ![](img/ch_35_012_4_crop0__S_f_0.jpg)  
    - 若鼻梁一侧有阴影区域的轮廓，则计数+1.  
        ![](img/fc_01_007_4_crop0.jpg) 
        ![](img/fc_01_007_4_crop0__S_f_0.jpg)  
    - 若面部有网点阴影，则尽可能按凸多边形区域计数。  
        ![](img/ah_01_012_3.jpg) 
        ![](img/ah_01_012_3__S_f_0.jpg)  
        ![](img/ch_35_012_4.jpg) 
        ![](img/ch_35_012_4__S_f_0.jpg)  
    - 若头部在身体上有阴影，则计数+1.  
        ![](img/fc_01_007_5__crop0.jpg) 
        ![](img/fc_01_007_5__crop0__S_f_0.jpg)  

- G_pc(p)：panel p中所有的角色道具的形状细节度；  
衡量方法是以下所有条目的计数之和：    
    - 对于角色的衣服，忽略衣服的边缘线条，仅计数褶皱的线条数；（需区分阴影排线。阴影排线按S计数）  
        ![](img/fc_01_007_0.jpg) 
        ![](img/fc_01_007_0__G_pc_0.jpg)  
    - 角色四肢(例如，手掌、手背、指甲)的线条;    
        ![](img/fc_01_007_0.jpg) 
        ![](img/fc_01_007_0__G_pc_1.jpg)  

- S_pc(p)：panel p中所有的角色道具的着色细节度；  
衡量方法是以下所有条目的计数之和：    
    - 衣服常常着色为网点纹理。若有一个纹理，则计数+1.  
        ![](img/ah_01_006_1_crop0.jpg) 
        ![](img/ah_01_006_1_crop0__shading.jpg)  

- G_ps(p)：panel p中所有的场景道具的形状细节度；  
衡量方法是以下所有条目的计数之和：   
    - 树木（茂密的树叶）。计数树干的线条数目、茂密的树叶所形成的轮廓的凸弧线数目，如下图。可见，该统计方法不精确。    
        ![](img/ce_01_087_4__crop0.jpg) 
        ![](img/ce_01_087_4__crop0__G_ps.jpg)  
    - 稀疏的树叶。统计可数的该物体类型的个数或簇数，如下图。可见，该统计方法不精确。    
        ![](img/ah_01_005_0_crop0.jpg) 
        ![](img/ah_01_005_0_crop0__G_ps_0.jpg)  
    - 草丛。统计可数的该物体类型的个数或簇数，如下图。可见，该统计方法不精确。   
        ![](img/ah_01_005_0_crop0.jpg) 
        ![](img/ah_01_005_0_crop0__G_ps_1.jpg)  
    - 云。计数轮廓的凸弧线数目，如下图。可见，该统计方法不精确。  
        ![](img/fc_14_190_0_crop0.jpg) 
        ![](img/fc_14_190_0_crop0__G_pc.jpg)  

- S_ps(p)：panel p中所有的场景道具的着色细节度；  
衡量方法是以下所有条目的计数之和：  
    - 树木（茂密的树叶）。计数树干上的、茂密的树叶所形成的着色区域的数目，如下图。可见，该统计方法不精确。  
        ![](img/ce_01_087_4__crop0.jpg) 
        ![](img/ce_01_087_4__crop0__S_ps.jpg)  
    - 稀疏的树叶。统计可数的该物体类型的个数或簇数，如下图。可见，该统计方法不精确。  
        ![](img/ah_01_005_2_crop0.jpg) 
        ![](img/ah_01_005_2_crop0__S_ps.jpg)  
    - 草丛。统计该物体类型的着色区域的数目，如下图。可见，该统计方法不精确。  
        ![](img/ah_01_005_0_crop0.jpg) 
        ![](img/ah_01_005_0_crop0__S_ps_1.jpg)  
    - 云。若轮廓内部有n个着色区域，则计数+n。    
        ![](img/fc_14_190_0_crop0.jpg) 
        ![](img/fc_14_190_0_crop0__S_pc.jpg)  


#### 4.2.2 数据集  
选择数据集的条件：  

- 因为作品中的彩页和黑白页的着色方式差异大，且彩页占极少比例。所以忽略彩页、二色彩页、由彩页转印的黑白页。  
- 因为作品中的扉页绘制得比正文精细，这影响统计结果，且扉页占极少比例。所以忽略扉页。  
- 若某部作品前期和后期的画风差别较大，则分别统计该作品的前期、中期、后期三部分。  

针对北条司的长篇连载作品，依照上述选择条件，得到以下数据集(目前，N=5)：  

- CE(前期): CE单行本（玉皇朝版）卷01，83～(83+N)页中的所有panel（约有7N个panel）。  
- CE(中期): CE单行本（玉皇朝版）卷09，06～(06+N)页中的所有panel（约有7N个panel）。  
- CE(后期): CE单行本（玉皇朝版）卷18，06～(06+N)页中的所有panel（约有7N个panel）。  
- CH(前期): CH单行本（东立版）卷01，38～(38+N)页中的所有panel（约有7N个panel）。  
- CH(中期): CH单行本（东立版）卷17，08～(08+N)页中的所有panel（约有7N个panel）。  
- CH(后期): CH单行本（东立版）卷35，08～(08+N)页中的所有panel（约有7N个panel）。  
- FC(前期): FC单行本（日文版）卷01，07～(07+N)页中的所有panel（约有7N个panel）。  
- FC(中期): FC单行本（日文版）卷07，07～(07+N)页中的所有panel（约有7N个panel）。  
- FC(后期): FC单行本（日文版）卷14，07～(07+N)页中的所有panel（约有7N个panel）。  
- AH: AH单行本（玉皇朝版）卷01， 05～(05+N)页中的所有panel（约有7N个panel）。  

#### 4.2.3 统计结果
将上述统计方法应用于上述数据集，所得统计结果见[这里](./statistic_of_details.ods)。其部分结果如下：  

![](img/table1.jpg)  
表格1。如上所述，RelativeDegreeSimplified(W,AH)的子列G的含义为"平均而言，W、AH的G之比"，其子列S的含义为"平均而言，W、AH的S之比"。RelativeDegreeSimplified2(W,AH)一列的含义为"平均而言，W、AH的细节度之比"。  

![](img/table2.jpg)  
表格2。如上所述，AverageDegree(W)一列的含义为"作品W中每个panel的细节度向量(G,S)的平均值"，其中(G,S)=((G_h,G_f,G_pc,G_ps), (S_h,S_f,S_pc,S_ps))。  

说明：  

- 随着N增大，RelativeDegree(W,AH)的值呈收敛趋势（见[统计结果](./statistic_of_details.ods)中的列“history of AVG(this/AH)”）。这或许有助于说明该方法是有效的。  
- 从上述表格1可以看出：平均每个panel含有至少几十个线条数。可以推断：即便统计某个panel时疏忽了少数几个线条，结果受影响不大。这或许能说明该方法有一定的健壮性。  


## 5. 方法B--一个设想  

与方法A的思路不同。如果知道以下信息：  

- 画者的人数；  
- 每个画者的工作效率；  
- 每个画者的工作时长；  

则可以估计出漫画作品的工作量。基于C4，可用工作量衡量作品的细节度。受此启发，做如下假设：  

- (BA1) 总细节度 := 作品的总工作量 := 画家的工作效率 * 工作时长  
- (BC1) 每个panel的平均细节度 := 每个panel的平均工作量 := 作品的总工作量 / panel的总数目  

由于笔者无法获得漫画家（及其助手）在一部作品上的工作效率和工作时长，所以无法用此方法计算细节度。所以本方法仅仅是一个设想。    


## 6. 总结  

笔者的初衷是：首先，尝试衡量漫画作品中画面的逼真程度；然后，以AH的画面的逼真程度来衡量其他作品的画面的逼真程度；最后，类似将物体质量的单位命名为“千克”，定义AH的画面逼真程度为1、单位为"Hojo"，以致敬北条司。  

本文中，首先，由于"画面的逼真程度"中的难点，所以笔者建立了一些假设，转而研究"画面的细节度"，并寻找其衡量方法，最终诉诸于线条数和着色数。由于4.2.1节中某些种类的物体的线条数和着色数的统计方法并不精确，所以该方法是半定量的。  

其次，将该方法应用于北条司的若干作品后，结果能反映出这些作品的画面有不同的细节度（如下表）。  

|            |                                                    |  
| ---------- | -------------------------------------------------- |  
| **作品W**   | **RelativeDegreeSimplified2(W, AH) ** (单位: Hojo) |  
| CE(前期)    | 0.70                       |  
| CE(中期)    | 0.72                       |  
| CE(后期)    | 0.69                       |  
|            |                            |  
| CH(前期)    | 0.48                       |  
| CH(中期)    | 0.66                       |  
| CH(后期)    | 0.91                       |  
|            |                            |  
| FC(前期)    | 0.59                       |  
| FC(中期)    | 0.71                       |  
| FC(后期)    | 0.92                       |  
|            |                            |  
| AH         | 1.00                       |  

最后，因为考虑到4.1.1中因素，所以笔者提议：以作品AH的画面的细节度为基准，将"作品W相对于作品AH的细节度的比值"称为"作品W的画面的细节度"，单位为"Hojo"（简称"H"），以致敬北条司。例如，上表的结果表明"FC前期的细节度为0.59H"。如果认为线条和着色数目正相关于逼真程度(即C6)，那么可认为"FC前期的逼真程度为0.59H"。  


## 7. 局限性和后续工作   

首先，A1忽略了“所画出的物体的边沿形状可能不准确”这种情况。后续可尝试寻找方法以衡量“所画出的物体的边沿形状是否准确、准确度有多少”。  

其次，4.2.1节中某些种类的物体的线条数和着色数的统计方法并不精确。后续可借鉴图像识别领域的方法，尝试借助软件或工具库来检测图片里的线条数和着色数。   

再次，4.2.2节将上述方法应用于北条司的作品时，数据集的N值太小。当N越大时，结果会越精确。后续可尝试统计更多的panel。  

最后，可将本方法用于北条司的其他作品，或其他漫画家的长期连载的商业漫画作品。  


## 8. 参考资料  
[1] [正相关 - Baike](https://baike.baidu.com/item/正相关/7779692)  
[2] [Oscar Ukonu](https://www.oscarukonu.com)  

[^1]: [正相关 - Baike](https://baike.baidu.com/item/正相关/7779692)
[^2]: [Oscar Ukonu](https://www.oscarukonu.com)  

--------------------------------------------------   
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  


# 可能影响FC创作的（日本）社会背景  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96818))  

- 历史上，日本的文化和主要宗教不敌视同性恋。日本的LGBT权利相对进步。每年有相关活动：东京彩虹骄傲游行、东京国际同性恋电影节。

- 1945年，日本（首个?）同性恋酒吧柳木(やなぎ(ゲイバー))开业。（雅彦的姓氏取为“柳叶”可能与此有关）

- 1992年，东京国际同性恋电影节成立。1996～1998年在东京吉祥寺举办。（FC连载开始于1996年。据说北条司为创作FC，特意异装绕吉祥寺一周。所以他的住所可能离吉祥寺不远。）

- 
《北条司拾遗集.画业35周年纪念》里(145页)有访谈：
----非常家庭的构思来源于哪里呢？
北条： 有段时间，电视上一直播跨性别者特辑和伪娘的节目。其中有一个伪娘说有一个一直想和他结婚的男人。节目企划是对那个男人保密，然后举办两个人的结婚典礼吓他一跳。我看了之后，觉得“啊，很有意思啊”。但是男人和男人是生不出孩子的。“等等，如果是hefemale(女扮男装)和Shemale的话，从生物学上来说是能生出孩子的“（笑）。看起来是普通家庭，但实际上是性别倒错的夫妇。即便是普通家庭剧也会变得非常有趣。实际上，在之前的作品中，男变女，女变男的情节也有很多。


###参考资料及节选

####日本LGBT的权利 (https://en.wikipedia.org/wiki/LGBT_rights_in_Japan)

Lesbian, gay, bisexual, transgender (LGBT) rights in Japan are relatively progressive by Asian standards. Same-sex sexual activity was criminalised only briefly in Japan's history between 1872 and 1880, after which a localised version of the Napoleonic Penal Code was adopted with an equal age of consent.[2] Same-sex couples and households headed by same-sex couples are ineligible for the legal protections available to opposite-sex couples, although since 2015 some cities offer symbolic "partnership certificates" to recognise the relationships of same-sex couples.
按亚洲标准，日本的LGBT权利相对进步。 在1872年至1880年之间，同性性行为在日本的历史上仅被简单地定为犯罪，此后，在同意年龄相等的情况下，采用了《拿破仑刑法》的本地版本。[2] 同性伴侣和以同性伴侣为户主的家庭没有资格获得对异性伴侣的法律保护，尽管自2015年以来，一些城市提供了象征性的“合伙证书”以承认同性伴侣的关系。

Japan's culture and major religions do not have a history of hostility towards homosexuality.[3] A majority of Japanese citizens are reportedly in favor of accepting homosexuality, with a 2013 poll indicating that 54 percent agreed that homosexuality should be accepted by society, while 36 percent disagreed, with a large age gap.[4] Although many political parties have not openly supported or opposed LGBT rights, there are several openly LGBT politicians in office. A law allowing transgender individuals to change their legal gender post-sex reassignment surgery and sterilization was passed in 2003. Discrimination on the basis of sexual orientation and gender identity is banned in certain cities, including Tokyo.[5]
历史上，日本的文化和主要宗教不敌视同性恋。[3] 据报道，大多数日本公民都赞成接受同性恋，2013年的一项民意调查显示，有54％的人同意同性恋应为社会所接受，而36％的人不同意，年龄差距很大。[4] 尽管许多政党并未公开支持或反对LGBT权利，但仍有数名公开LGBT政客在任。 2003年通过了一项法律，允许跨性别者改变其合法的性别分配方法和绝育措施。在包括东京在内的某些城市，禁止基于性取向和性别认同的歧视。[5]

Tokyo Rainbow Pride has been held annually since 2012, with attendance increasing every year.[6] A 2015 opinion poll found that a majority of Japanese supported the legalisation of same-sex marriage.[7] Further opinion polls conducted over the following years have found high levels of support for same-sex marriage among the Japanese public, most notably the younger generation.[8] Speaking to Kyodo News, Vice President of Stonewall Japan Varun Khanna, commented: "We (may) have many criticisms for Japan, but Japan still tends to be one of the most sexually liberated countries in the world, and certainly is the most accepting country among those in Asia, the Middle East, and Africa."[9] 
自2012年以来，每年都会举办一次东京彩虹骄傲游行，参加人数每年都在增加。[6] 2015年的一项民意调查发现，大多数日本人支持同性婚姻合法化。[7] 随后几年进行的进一步民意测验发现，日本公众，尤其是年轻一代，对同性婚姻的支持很高。[8] Stonewall日本公司副总裁瓦伦·卡纳（Varun Khanna）在接受共同社新闻采访时说：“我们（可能）对日本提出很多批评，但日本仍然倾向于成为世界上性解放最多的国家之一，并且当然是接受程度最高的国家 在亚洲，中东和非洲的那些国家中。” [9]


####术语：

Modern Japanese terms for LGBT people include dōseiaisha (同性愛者, literally "same-sex-love person"), gei (ゲイ, "gay"), homosekusharu (ホモセクシャル, "homosexual"), rezubian (レズビアン, "lesbian"), baisekushuaru (バイセクシュアル, "bisexual") and toransujendā (トランスジェンダー, "transgender").[14]
日语中对LGBT人群的现代用语包括dōseiaisha（同性愛者，字面上是“同性爱人”），gei（ゲイ，“同性恋”），homosekusharu（ホモセクシャル，“同性恋”），rezubian（レズビアン，“女同性恋”） ，baisekushuaru（バイセクシュアル，“双性恋”）和toransujendā（トランスジェンダー，“变性”）。[14]


####19世纪LGBT的权利 (https://en.wikipedia.org/wiki/19th_century_in_LGBT_rights)

（注：仅列出与日本相关的）

1880 - The Empire of Japan decriminalized homosexual acts (anal sodomy), having only made them illegal during the early years of the Meiji Restoration.
1880年 - 日本帝国将同性恋行为（anal sodomy）合法化，直到明治维新初期才将其定为非法。


#### LGBT大事年表（20世纪） (https://en.wikipedia.org/wiki/Timeline_of_LGBT_history,_20th_century)

（注：由于条目众多，所以这里仅列出与日本相关的。
更多信息可参见：https://en.wikipedia.org/wiki/Timeline_of_LGBT_history,_20th_century，
及部分中文翻译：https://gitlab.com/city4cat/myblogs/-/blob/master/hojocn/fc_background/Timeline_of_LGBT_history__20th_century.CN.md ）

- 1945 – Gay bar Yanagi opened in Japan.[30]
1945年 – 同性恋酒吧柳木(やなぎ(ゲイバー))在日本开业。[30]

- 1952 – In Japan the male homosexual magazine "Adonis" is launched with the writer Yukio Mishima as a contributor.
1952年 – 日本发行男性同性恋杂志“Adonis”，由作家三岛由纪夫（Yukio Mishima）撰稿。

- 1971 – Ken Togo ran for national election, in Japan.
1971年 – 肯·多哥在日本参加大选。

- 1975 – Twelve women became the first group of women in Japan to publicly identify as lesbians, publishing one issue of a magazine called Subarashi Onna (Wonderful Women).[96] 
1975年 - 十二名女性成为日本第一批被公开认定为女同性恋的女性，并出版了一期名为《 Subarashi Onna》（《杰出女性》）的杂志。[96]

- 1976 – Association of homosexual liberation was founded in Japan.[101]
1976年 - 日本成立了同性恋解放协会。[101] 

-1977 – Platonica Club and Front Runners were founded in Japan.[101] 
1977年 - 在日本成立了柏拉图俱乐部和领跑者。[101]

- 1979 – Japan Gay Center was established in Japan.[101]
1979年 - 日本同性恋中心在日本成立。[101]

- 1982 –  Ken Togo founded the Deracine Party in Japan. 
1982年 - Ken Togo在日本创立了Deracine党。

- 1984 – ILGA Japan is founded in Japan.
1984年 - ILGA Japan在日本成立。

- 1986 – From 1 till 3 May, the 1986, ILGA Asia Conference took place in Japan's capital Tokyo.
1986年 - 5月1日至3日，1986年的ILGA亚洲会议在日本首都东京举行。

- 1992 - Tokyo International Lesbian & Gay Film Festival was held in Japan.
1992年 - 东京国际同性恋电影节在日本举行。

- 1994 - Gay Parade was held in Japan. (8.1994)
1994年 - 同性恋游行在日本举行。 （1994年8月）



#### 东京国际同性恋电影节(东京彩虹胶卷) (https://en.wikipedia.org/wiki/Rainbow_Reel_Tokyo)

Rainbow Reel Tokyo (Japanese: レインボー・リール東京 Reinbō rīru Tōkyō), until 2016 known as Tokyo International Lesbian & Gay Film Festival,[1] (Japanese: 東京国際レズビアン&ゲイ映画祭 Tōkyō kokusai rezubian to gei eigasai), also known by the acronym TILGFF, is an international film festival for LGBT audiences, held annually in Tokyo, the capital city of Japan.
东京彩虹胶卷（日语：レインボー・リール東京 Reinbō rīru Tōkyō），2016年被称为东京国际同性恋电影节，[1]（日语：東京国際レズビアン&ゲイ映画祭 Tōkyō kokusai rezubian to gei eigasai），也被称为TILGFF，是一个针对LGBT观众的国际同性恋电影节，每年在日本首都东京举行。

The Festival was established in 1992 and was held at Nakano Sun Plaza, 6th Floor (中野サンプラザ６F研修室). The next three were held at Kichijōji Baus Theater (吉祥寺バウスシアター). Since 1996, the Festival has been held in July at Spiral Hall in the Aoyama neighborhood of Tokyo.
电影节成立于1992年，当时在中野Sun Plaza６楼（中野太阳广场6楼研修室）举行。 接下来的三届在吉祥寺Baus剧院（吉祥寺バBaus剧院）举行。 自1996年以来，该电影节于每年7月在东京青山附近的Spiral Hall举行。

The 23rd Tokyo International Lesbian & Gay Film Festival was held July 12~21, 2014, at Eurospace in Shibuya, and Spiral Hall in Aoyama. 
第23届东京国际同性恋电影节于2014年7月12日至21日在涩谷Eurospace和青山Spiral Hall举行。

（注：https://en.wikipedia.org/wiki/Rainbow_Reel_Tokyo 里列出了每年参展的作品信息。）


#### 参考链接

https://en.wikipedia.org/wiki/LGBT_history

https://www.history.com/tag/lgbt-history

https://www.pbs.org/outofthepast/

https://en.wikipedia.org/wiki/19th_century_in_LGBT_rights

https://en.wikipedia.org/wiki/Timeline_of_LGBT_history,_20th_century

https://en.wikipedia.org/wiki/Timeline_of_LGBT_history,_21st_century

https://en.wikipedia.org/wiki/Rainbow_Reel_Tokyo

https://en.wikipedia.org/wiki/Timeline_of_Asian_and_Pacific_Islander_diasporic_LGBT_history

https://en.wikipedia.org/wiki/LGBT_rights_by_country_or_territory

https://en.wikipedia.org/wiki/LGBT_rights_in_Japan

https://web.archive.org/web/20131110095918/http://www.hawaii.edu/hivandaids/LGBT_Rights_in_Japan.pdf

https://en.wikipedia.org/wiki/Timeline_of_Asian_and_Pacific_Islander_diasporic_LGBT_history

https://en.wikipedia.org/wiki/Timeline_of_South_Asian_and_diasporic_LGBT_history




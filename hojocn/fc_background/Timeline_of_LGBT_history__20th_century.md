source:
https://en.wikipedia.org/wiki/Timeline_of_LGBT_history,_20th_century

#Timeline of LGBT history, 20th century

###1900s

- 1901 – On 8 June 1901 two women, Marcela Gracia Ibeas and Elisa Sanchez Loriga, attempted to get married in A Coruña (Galicia, Spain). To achieve it Elisa had to adopt a male identity: Mario Sánchez, as listed on the marriage certificate.[1]

- 1903 – In New York City on 21 February 1903, New York police conducted the first United States recorded raid on a gay bathhouse, the Ariston Hotel Baths. 34 men were arrested and 12 brought to trial on sodomy charges; 7 men received sentences ranging from 4 to 20 years in prison.[2]

- 1906 – Potentially the first openly gay American novel with a happy ending, Imre, is published.[3]

- 1907 – Adolf Brand, the activist leader of the Gemeinschaft der Eigenen, working to overturn Paragraph 175, publishes a piece "outing" the imperial chancellor of Germany, Prince Bernhard von Bülow. The Prince sues Brand for libel and clears his name; Brand is sentenced to 18 months in prison.[4]

- 1907–1909 – Harden–Eulenburg affair in Germany[5]
    

###1910s

- 1910 – Emma Goldman first begins speaking publicly in favor of homosexual rights. Magnus Hirschfeld later wrote "she was the first and only woman, indeed the first and only American, to take up the defense of homosexual love before the general public."[6]
    
- 1912 – The first explicit reference to lesbianism in a Mormon magazine occurred when the "Young Woman's Journal" paid tribute to "Sappho of Lesbos[8] "; the Scientific Humanitarian Committee of the Netherlands (NWHK), the first Dutch organization to campaign against anti-homosexual discrimination, is established by Dr. Jacob Schorer.

- 1913 – The word faggot is first used in print in reference to gays in a vocabulary of criminal slang published in Portland, Oregon: "All the faggots [sic] (sissies) will be dressed in drag at the ball tonight".

- 1917 – The October Revolution in Russia repeals the previous criminal code in its entirety—including Article 995.[9][10] Bolshevik leaders reportedly say that "homosexual relationships and heterosexual relationships are treated exactly the same by the law."

- 1919 – In Berlin, Germany, Doctor Magnus Hirschfeld co-founds the Institut für Sexualwissenschaft (Institute for Sex Research), a pioneering private research institute and counseling office. Its library of thousands of books was destroyed by Nazis in May 1933.[11][12][13]

- 1919 - Different from the Others, one of the first explicitly gay films, is released. Magnus Hirschfeld has a cameo in the film and partially funded its production


###1920s[edit]

- 1921 – In England, an attempt to make lesbianism illegal for the first time in Britain's history fails.[14]

- 1922 – A new criminal code comes into force in the USSR officially decriminalizing homosexual acts.

- 1923 – The word fag is first used in print in reference to gays in Nels Anderson's The Hobo: "Fairies or Fags are men or boys who exploit sex for profit."

- 1923 – Lesbian Elsa Gidlow, born in England, published the first volume of openly lesbian love poetry in the United States, titled "On a Grey Thread."[15]

- 1924 – The first homosexual rights organization in America is founded by Henry Gerber in Chicago— the Society for Human Rights.[16] The group exists for a few months before disbanding under police pressure.[17] Paraguay and Peru legalize homosexuality.

- 1926 – The New York Times is the first major publication to use the word homosexuality.[3] Iconic lesbian café Eve's Hangout in Greenwich Village closed after police raid.

- 1927 - Karol Szymanowski, Poland's openly gay composer, is appointed chief of Poland's state-owned national music school, the Fryderyk Chopin Music Academy.

- 1928 – The Well of Loneliness by Radclyffe Hall is published in the UK and later in the United States. This sparks great legal controversy and brings the topic of homosexuality to public conversation.

- 1929 – On 22 May, Katharine Lee Bates, author of America the Beautiful dies. On 16 October, a Reichstag Committee votes to repeal Paragraph 175; the Nazis' rise to power prevents the implementation of the vote.


###1930s[edit]

- 1931 - A group of transvestites from Barcelona known as "Las Carolinas" carries out the first documented LGTB demonstration in history. They do so after the destruction of a centric public bath of Barcelona (Spain) which was a common LGTB meeting place at the time.[18]

- 1931 - Mädchen in Uniform, one of the first explicitly lesbian films and the first pro-lesbian film, is released.

- 1931 - In Berlin in 1931, Dora Richter became the first known transgender woman to undergo vaginoplasty.[19][20]

- 1932 – Poland codifies the homosexual and heterosexual age of consent equally at 15. Polish law never had criminalized homosexuality, although occupying powers had outlawed it in 1835.[21]

- 1933 – New Danish penalty law decriminalizes homosexuality.

- 1933 – The National Socialist German Workers Party bans homosexual groups. Homosexuals are sent to concentration camps. Nazis burn the library of Magnus Hirschfeld's Institute for Sexual Research, and destroy the Institute; Denmark and Philippines decriminalizes homosexuality. Homosexual acts are recriminalized in the USSR.

- 1934 – Uruguay decriminalizes homosexuality. The USSR once again criminalizes muzhelozhstvo (specific Russian definition of "male sexual intercourse with male", literally "man lying with man"), punishable by up to 5 years in prison – more for the coercion or involvement of minors.[22]

- 1936 – Mona's 440 Club, the first lesbian bar in America, opened in San Francisco in 1936.[23][24] Mona's waitresses and female performers wore tuxedos and patrons dressed their roles.[24]

- 1936 – Federico García Lorca, Spanish poet, is shot at the beginning of the Spanish civil war.

- 1937 – The first use of the pink triangle for gay men in Nazi concentration camps.

- 1938 – The word gay is used for the first time on film in reference to homosexuality in the film Bringing Up Baby.[25]

- 1939 – Frances V. Rummell, an educator and a teacher of French at Stephens College, published an autobiography under the title Diana: A Strange Autobiography; it was the first explicitly lesbian autobiography in which two women end up happily together.[26] This autobiography was published with a note stating "The publishers wish it expressly understood that this is a true story, the first of its kind ever offered to the general reading public".[26]


###1940s[edit]

- 1940 – Iceland decriminalizes homosexuality; the NWHK is disbanded in the Netherlands in May due to the German invasion, and most of its archive is voluntarily destroyed, while the rest is confiscated by Nazi soldiers.

- 1941 – Transsexuality was first used in reference to homosexuality and bisexuality.

- 1942 – Switzerland decriminalizes homosexuality, with the age of consent set at 20.

- 1944 – Sweden decriminalizes homosexuality, with the age of consent set at 18 and Suriname legalizes homosexuality.

- 1944 – The first prominent American to reveal his homosexuality was the poet Robert Duncan. This occurred when in 1944, using his own name in the anarchist magazine Politics, he wrote that homosexuals were an oppressed minority.[27]

- 1945 – The Holocaust ends and it is estimated that between about 3,000 to about 9,000 homosexuals died in Nazi concentration and death camps, while it is estimated that between about 2,000 to about 6,000 homosexual survivors in Nazi concentration and death camps were required to serve out the full term of their sentences under Paragraph 175 in prison. The first gay bar in post-World War II Berlin opened in the summer of 1945, and the first drag ball took place in American sector of West Berlin in the fall of 1945.[28] Four honourably discharged gay veterans form the Veterans Benevolent Association, the first LGBT veterans' group.[29] Gay bar Yanagi opened in Japan.[30]

- 1946 – "COC" (Dutch acronym for "Center for Culture and Recreation"), one of the earliest homophile organizations, is founded in the Netherlands. It is the oldest surviving LGBT organization.

- 1946 – Plastic surgeon Harold Gillies carries out sex reassignment surgery on Michael Dillon in Britain.

- 1947-1948 – Vice Versa, the first American lesbian publication, is written and self-published by Lisa Ben (real name Edith Eyde) in Los Angeles.

- 1948 – "Forbundet af 1948" ("League of 1948"), a homosexual group, is formed in Denmark.

- 1948 – The communist authorities of Poland make 15 the age of consent for all sexual acts, homosexual or heterosexual.


###1950s[edit]

- 1950 – The Organization for Sexual Equality, now Swedish Federation for Lesbian, Gay, Bisexual and Transgender Rights (RFSL), is formed in Sweden; East Germany partially abrogates the Nazis' emendations to Paragraph 175; The Mattachine Society, the first sustained American homosexual group, is founded in Los Angeles (11 November); 190 individuals in the United States are dismissed from government employment for their sexual orientation, commencing the Lavender scare.

- 1951 – Greece decriminalizes homosexuality.

- 1951 – Jordan In 1951, a revision of the Jordanian Criminal Code legalized private, adult, non-commercial, and consensual sodomy, with the age of consent set at 16.

- 1952 – "Spring Fire," the first lesbian paperback novel, and the beginning of the lesbian pulp fiction genre, was published in 1952 and sold 1.5 million copies.[31][32] It was written by lesbian Marijane Meaker under the false name Vin Packer.[31]

- 1952 – In the spring of 1952, Dale Jennings was arrested in Los Angeles for allegedly soliciting a police officer in a bathroom in Westlake Park, now known as MacArthur Park. His trial drew national attention to the Mattachine Society, and membership increased drastically after Jennings contested the charges, resulting in a hung jury.[33]

- 1952 – Christine Jorgensen becomes the first widely publicized person to have undergone sex reassignment surgery, in this case, male to female, creating a world-wide sensation.

- 1952 – In Japan the male homosexual magazine "Adonis" is launched with the writer Yukio Mishima as a contributor.

- 1953 – The Diana Foundation was founded on 19 March 1953 in Houston, TX by a small group of friends. The Diana Foundation is a nonprofit organization and recognized as the oldest continuously active gay organization in the United States and hosts two annual fundraising events including its Diana Awards.[34]

- 1954 – 7 June–Mathematical and computer genius Alan Turing commits suicide by cyanide poisoning, 18 months after being given a choice between two years in prison or libido-reducing hormone treatment for a year as a punishment for homosexuality.[35] A succession of well-known men, including Lord Montagu, Michael Pitt-Rivers and Peter Wildeblood, were convicted of homosexual offences as British police pursued a McCarthy-like purge of Society homosexuals.[36] Arcadie, the first homosexual group in France, is formed.

- 1955 – The Daughters of Bilitis (DOB) was founded in San Francisco by four lesbian couples (including Del Martin and Phyllis Lyon) and was the first national lesbian political and social organization in the United States.[37] The group's name came from "Songs of Bilitis," a lesbian-themed song cycle by French poet Pierre Louÿs, which described the fictional Bilitis as a resident of the Isle of Lesbos alongside Sappho.[37] DOB's activities included hosting public forums on homosexuality, offering support to isolated, married, and mothering lesbians, and participating in research activities.[37] Mattachine Society New York chapter founded.

- 1956 – The Ladder, the first nationally distributed lesbian publication in the United States, began publication.

- 1956 – Thailand decriminalizes homosexual acts.

- 1956 – Florida Legislative Investigation Committee (commonly known as the Johns Committee) established. Failing to find communist ties to civil rights organizations, it investigates homosexuals as a threat to national security.

- 1957 – The word "Transsexual" is coined by U.S. physician Harry Benjamin; The Wolfenden Committee's report recommends decriminalizing consensual homosexual behaviour between adults in the United Kingdom; Psychologist Evelyn Hooker publishes a study showing that homosexual men are as well adjusted as non-homosexual men, which becomes a major factor in the American Psychiatric Association removing homosexuality from its handbook of disorders in 1973. Homoerotic artist Tom of Finland first published on the cover of Physique Pictorial magazine from Los Angeles.[38]

- 1958 – The Homosexual Law Reform Society is founded in the United Kingdom; Barbara Gittings founds the New York chapter of Daughters of Bilitis.

- 1958 – One, Inc. v. Olesen, 355 U.S. 371 (1958), was the first U.S. Supreme Court ruling to deal with homosexuality and the first to address free speech rights with respect to homosexuality. The Supreme Court reversed a lower court ruling that the gay magazine ONE magazine violated obscenity laws, thus upholding constitutional protection for pro-homosexual writing.[39]

- 1958 - The first gay leather bar in the United States, the Gold Coast, opened in Chicago in 1958. It was founded by Dom Orejudos and Chuck Renslow.

- 1959 – ITV, at the time the UK's only national commercial broadcaster, broadcasts the UK’s first TV gay drama, South, starring Peter Wyngarde.[40] The first homosexual uprising in the USA occurs at Cooper's Doughnuts in Los Angeles, USA; rioters were arrested by LAPD.[41]
    
    
###1960s[edit]

- 1960 – Cpls. Fannie Mae Clackum and Grace Garner, U.S. Air Force reservists in the late 1940s and early 1950s, became the first people to successfully challenge their discharges from the U.S. military for being gay, although the ruling turned on the fact that there wasn't enough evidence to show the women were lesbians.[42] According to scholars, since at least as early as 1960, Executive Order 10450 was applied to ban transgender individuals from serving in the United States military.[43]

- 1961 – Hungary decriminalizes sodomy; Victim is the first English-language film to use the word "homosexual", and premieres in the UK on 31 August 1961; the Vatican declares that anyone who is "affected by the perverse inclination" towards homosexuality should not be allowed to take religious vows or be ordained within the Roman Catholic Church; The Rejected, the first documentary on homosexuality broadcast on American television, is first broadcast on KQED TV in San Francisco on 11 September 1961; José Sarria becomes the first openly gay candidate for public office in the United States when he runs for the San Francisco Board of Supervisors.[44]

- 1962 – Illinois becomes the first U.S. state to remove sodomy law from its criminal code through passage of the American Law Institute's Model Penal Code. While the adopted code did not penalize private sexual relations, it criminalized acts of "Open Lewdness;"[45][46] Czechoslovakia decriminalizes sodomy. The Tavern Guild, the first gay business association in the United States, was created by gay bar owners in 1962 as a response to continued police harassment and closing of gay bars (including the Tay-Bush Inn raid), and continued until 1995.[47]

- 1963 – East Coast Homophile Organizations (ECHO) is established in Philadelphia; initial members include the regional chapters of Daughters of Bilitis, Janus Society, and Mattachine Society.

- 1963 – Israel de facto decriminalizes sodomy and sexual acts between men by judicial decision against the enforcement of the relevant section in the old British-mandate law from 1936 (which in fact was never enforced).[citation needed]

- 1964 – Canada sees its first gay-positive organization, ASK, and first gay magazines: ASK Newsletter (in Vancouver), and Gay (by Gay Publishing Company of Toronto). Gay was the first periodical to use the term 'Gay' in the title and expanded quickly, including outstripping the distribution of American publications under the name Gay International. These were quickly followed by Two (by Gayboy (later Kamp) Publishing Company of Toronto).[48][49]

- 1964 – Canada: In March 1964, Ted Northe founds the 'Imperial Court of Canada' a monarchist society compromised primarily of drag personalities and becomes a driving force in the effort to achieve equality in Canada. The Courts of Canada now have over 14 chapters across the country and is the oldest, continuously running, LGBT Organization in Canada.

- 1964 – The first photograph of lesbians on the cover of lesbian magazine The Ladder was done in September 1964, showing two women from the back, on a beach looking out to sea.

- 1964 – The June 1964 Paul Welch Life article entitled "Homosexuality In America" was the first time a national publication reported on gay issues.

- 1964 – Created in 1964, the Council on Religion and the Homosexual was the first group in the U.S. to use the word "homosexual" in its name.

- 1965 – The Council on Religion and the Homosexual held an event where local politicians could be questioned about issues concerning gay and lesbian people, including police intimidation. The event marks the first known instance of "the gay vote" being sought.

- 1965 – Everett George Klippert, the last person imprisoned in Canada for homosexuality, is arrested for private, consensual sex with men. After being assessed "incurably homosexual", he is sentenced to an indefinite "preventive detention" as a dangerous sexual offender. This was considered by many Canadians to be extremely homophobic, and prompted sympathetic articles in Maclean's and The Toronto Star, eventually leading to increased calls for legal reform in Canada which passed in 1969.[50]

- 1965 – Conservatively dressed gays and lesbians demonstrate outside Independence Hall in Philadelphia on 4 July 1965. This was the first in a series of Annual Reminders that took place through 1969.

- 1965 – Vanguard, an organization of LGBT youth in the low-income Tenderloin district, was created in 1965. It is considered the first Gay Liberation organization in the U.S.[51][52]

- 1966 – The Mattachine Society stages a "Sip-In" at Julius Bar in New York City challenging a New York State Liquor Authority prohibiting serving alcohol to gays; the National Planning Conference of Homophile Organizations is established (to become NACHO—North American Conference of Homophile Organizations later that year); the Compton's Cafeteria Riot occurred in August 1966 by transgender women and Vanguard members in the Tenderloin district of San Francisco. This incident was one of the first recorded transgender riots in United States history, preceding the more famous 1969 Stonewall Riots in New York City by three years. Vanguard was founded to demonstrate for equal rights. The first lesbian to appear on the cover of the lesbian magazine The Ladder with her face showing was Lilli Vincenz in January 1966. A coalition of Homosexual organizations organized demonstrations for Armed Forces Day to protest the exclusion of LGBT from the U.S. armed services. The Los Angeles group held a 15-car motorcade, which has been identified as the nation's first gay pride parade.[53] The National Transsexual Counseling Unit is created, becoming the first transgender organization ever and first American transgender organization ever.[54] The Society for Individual Rights opened America’s first gay and lesbian community center.

- 1967 – The Black Cat Tavern in the Silver Lake neighborhood of Los Angeles is raided on New Year's Day by 12 plainclothes police officers who beat and arrested employees and patrons. The raid prompted a series of protests that began on 5 January 1967, organized by P.R.I.D.E. (Personal Rights in Defense and Education). It's the first use of the term "Pride" that came to be associated with LGBT rights.

- 1967 – The Advocate was first published in September as "The Los Angeles Advocate," a local newsletter alerting gay men to police raids in Los Angeles gay bars.

- 1967 – The Sexual Offences Act 1967 decriminalised homosexual acts between two men over 21 years of age in private in England and Wales.;[55] The act did not apply to Scotland, Northern Ireland nor the Channel Islands; The book Homosexual Behavior Among Males by Wainwright Churchill breaks ground as a scientific study approaching homosexuality as a fact of life and introduces the term "homoerotophobia", a possible precursor to "homophobia"; The Oscar Wilde Bookshop, the world's first homosexual-oriented bookstore, opens in New York City; A raid on the Black Cat Tavern in Los Angeles, California promotes homosexual rights activity. The Student Homophile League at Columbia University is the first institutionally recognized gay student group in the United States.[citation needed]

- 1967 –Grupo Nuestro Mundo (English: "Our World Group") is formed in Greater Buenos Aires, the first gay rights organization in Argentina and Latin America.[56][57] The Homosexuals, a 1967 episode of the documentary television series CBS Reports, was the first network documentary dealing with the topic of homosexuality.

- 1968 – Paragraph 175 is eased in East Germany decriminalizing homosexual acts over the age of 18; Bulgaria decriminalizes adult homosexual relations. In Los Angeles, following the arrest of two patrons in a raid, The Patch owner Lee Glaze organized the other patrons to move on the police station. After buying out a nearby flower shop, the demonstrators caravanned to the station, festooned it with the flowers and bailed out the arrested men.[53] According to the online encyclopedia glbtq.com, "In the aftermath of the riot at Compton's, a network of transgender social, psychological, and medical support services was established, which culminated in 1968 with the creation of the National Transsexual Counseling Unit [NTCU], the first such peer-run support and advocacy organization in the world".[58]

- 1969 – The Stonewall riots occur in New York City; Paragraph 175 is eased in West Germany; Bill C-150 is passed, decriminalizing homosexuality in Canada. Pierre Trudeau, the Prime Minister of Canada, is quoted as having said: "The state has no place in the bedrooms of the nation."; Poland decriminalizes homosexual prostitution; An Australian arm of the Daughters of Bilitis forms in Melbourne and is considered Australia's first homosexual rights organisation.[citation needed] The Gay Liberation Front is formed in America, and it is the first gay organization to use "gay" in its name. On 31 December 1969, the Cockettes perform for the first time at the Palace Theatre on Union and Columbus in the North Beach neighborhood of San Francisco.


###1970s[edit]

- 1970 – The first Gay Liberation Day March is held in New York City; The first LGBT Pride Parade is held in New York; The first "Gay-in" held in San Francisco; Carl Wittman writes A Gay Manifesto;[59][60] CAMP (Campaign Against Moral Persecution) is formed in Australia;[61][62] The Task Force on Gay Liberation formed within the American Library Association. Now known as the GLBT Round Table, this organization is the oldest LGBTQ professional organization in the United States.[63] In November, the first gay rights march occurs in the UK at Highbury Fields following the arrest of an activist from the Young Liberals for importuning.[64]

- 1971 – Campaign Against Moral Persecution (CAMP) is officially established on 6 February 1971, at the first public gathering of gay women and men in Australia, which takes place in a church hall in Balmain,[65] Society Five (a homosexual rights organization) is formed in Melbourne, Australia; Homosexuality is decriminalized in Austria, Costa Rica and Finland; Colorado and Oregon repeal sodomy laws; Idaho repeals the sodomy law — Then re-instates the repealed sodomy law because of outrage among Mormons and Catholics.[66][67] The Netherlands changes the homosexual age of consent to 16, the same as the straight age of consent; The U.S. Libertarian Party calls for the repeal of all victimless crime laws, including the sodomy laws; Dr. Frank Kameny becomes the first openly gay candidate for the United States Congress; The University of Michigan establishes the first collegiate LGBT programs office, then known as the "Gay Advocate's Office." The UK Gay Liberation Front (GLF) was recognized as a political movement in the national press and was holding weekly meetings of 200 to 300 people.;[68] George Klippert, the last man jailed for homosexuality in Canada, is released from prison. Ken Togo ran for national election, in Japan. During a UCLA conference called "The Homosexual in America," Betty Berzon became the first psychotherapist in the country to come out as gay to the public.[69] Boys in the Sand was the first gay porn film to include credits, to achieve crossover success, to be reviewed by Variety,[70] and one of the earliest porn films, after 1969's Blue Movie[71][72][73][74] by Andy Warhol, to gain mainstream credibility, preceding 1972's Deep Throat by nearly a year. It was promoted with an advertising campaign unprecedented for a pornographic feature, premiered in New York City in 1971 and was an immediate critical and commercial success.[75] The Alice B. Toklas Democratic Club, founded in San Francisco in 1971, was the first gay Democratic club of the United States.

- 1972 – Sweden becomes the first country in the world to allow transsexuals to legally change their sex, and provides free hormone therapy;[76] Hawaii legalizes homosexuality; In South Australia, a consenting adults in private-type legal defence was introduced; Norway decriminalizes homosexuality; East Lansing, Michigan and Ann Arbor, Michigan and San Francisco, California become the first cities in United States to pass a homosexual rights ordinance. Jim Foster, of San Francisco and Madeline Davis, of Buffalo, New York, the first openly gay and lesbian delegates to the Democratic Convention, give the first speeches advocating a gay rights plank in the Democratic Party Platform. "Stonewall Nation", the first gay anthem is written and recorded by Madeline Davis and is produced on 45 rpm record by the Mattachine Society of the Niagara Frontier. Lesbianism 101, the first lesbianism course in the U.S., is taught at the University of Buffalo by Margaret Small and Madeline Davis. Queens, NY schoolteacher Jeanne Manford marched with her gay son, gay rights activist Morty Manford, in New York's Christopher Street Liberation Day march. This was the origin of the straight ally movement and of PFLAG - (originally Parents of Gays, then Parents, Families and Friends of Lesbians and Gays, now simply PFLAG.)[77] Nancy Wechsler became the first openly gay or lesbian person in political office in America; she was elected to the Ann Arbor City Council in 1972 as a member of the Human Rights Party and came out as a lesbian during her first and only term there.[78] Also in 1972, Camille Mitchell became the first open lesbian to be awarded custody of her children in a divorce case, although the judge restricted the arrangement by precluding Ms. Mitchell's lover from moving in with her and the children.[79] Freda Smith became the first openly lesbian minister in the Metropolitan Community Church (she was also their first female minister).[80][81] Beth Chayim Chadashim was founded in 1972 as the first LGBT synagogue in the world, and the first LGBT synagogue recognized by the Union for Reform Judaism.[82] A Quaker group, the Committee of Friends on Bisexuality, issued the "Ithaca Statement on Bisexuality" supporting bisexuals.[83] The Statement, which may have been "the first public declaration of the bisexual movement" and "was certainly the first statement on bisexuality issued by an American religious assembly," appeared in the Quaker Friends Journal and The Advocate in 1972.[84][85][86] Today Quakers have varying opinions on LGBT people and rights, with some Quaker groups more accepting than others.[87] The first gay bar in San Francisco to have clear windows was Twin Peaks Tavern, which removed its blacked-out windows in 1972. Jack Fritscher’s book Popular Witchcraft Straight from the Witch's Mouth, the first book to investigate gay Wicca and witchcraft, was published.[88]

- 1973 – On 15 October the Australian and New Zealand College of Psychiatry Federal Council declares homosexuality not an illness – the first such body in the world to do so; in December the American Psychiatric Association removes homosexuality from its Diagnostic and Statistical Manual of Mental Disorders (DSM-II), based largely on the research and advocacy of Evelyn Hooker. The first formal meeting of PFLAG took place on 26 March 1973 at the Metropolitan-Duane Methodist Church in Greenwich Village (now the Church of the Village). Approximately 20 people attended, including founder Jeanne Manford, her husband Jules, son Morty, Dick and Amy Ashworth, Metropolitan Community Church founder Reverend Troy Perry, and more.[77] Malta legalizes homosexuality; In West Germany, the age of consent is reduced for homosexuals to 18 (though it is 14 for heterosexuals).[citation needed]; Sally Miller Gearhart became the first open lesbian to obtain a tenure-track faculty position when she was hired by San Francisco State University, where she helped establish one of the first women and gender study programs in the country.[89] Lavender Country, an American country music band, released a self-titled album which is the first known gay-themed album in country music history.[90]

- 1974 – Chile allows a trans person to legally change her name and gender on the birth certificate after undergoing sex reassignment surgery, becoming the second country in the world to do so.[91] Kathy Kozachenko becomes the first openly gay American elected to public office when she wins a seat on the Ann Arbor, Michigan city council; In New York City Dr. Fritz Klein founds the Bisexual Forum, the first support group for the Bisexual Community; Elaine Noble becomes the second openly gay American elected to public office when she wins a seat in the Massachusetts State House; Inspired by Noble, Minnesota state legislator Allan Spear comes out in a newspaper interview; Ohio repeals sodomy laws. Robert Grant founds American Christian Cause to oppose the "gay agenda", the beginning of modern Christian politics in America. In London, the first openly LGBT telephone help line opens, followed one year later by the Brighton Lesbian and Gay Switchboard;[citation needed] the Brunswick Four are arrested on 5 January 1974, in Toronto, Ontario. This incident of Lesbophobia galvanizes the Toronto Lesbian and Gay community;[92] the National Socialist League (The Gay Nazi Party) is founded in Los Angeles, California.[citation needed] The first openly gay or lesbian person to be elected to any political office in America was Kathy Kozachenko, who was elected to the Ann Arbor City Council in April 1974.[93] Also in 1974, the Lesbian Herstory Archives opened to the public in the New York apartment of lesbian couple Joan Nestle and Deborah Edel; it has the world's largest collection of materials by and about lesbians and their communities.[94] Also in 1974, Angela Morley became the first openly transgender person to be nominated for an Academy Award, when she was nominated for one in the category of Best Music, Original Song Score/Adaptation for The Little Prince (1974), a nomination shared with Alan Jay Lerner, Frederick Loewe, and Douglas Gamley. The world's first gay softball league was formed in San Francisco in 1974 as the Community Softball League, which eventually included both women's and men's teams. The teams, usually sponsored by gay bars, competed against each other and against the San Francisco Police softball team.[95]

- 1975 – Twelve women became the first group of women in Japan to publicly identify as lesbians, publishing one issue of a magazine called Subarashi Onna (Wonderful Women).[96] Homosexuality is legalized in California due to the Consenting Adult Sex Bill, authored by and successfully lobbied for in the state legislature by State Assemblyman from San Francisco Willie Brown; Leonard Matlovich, a Technical Sergeant in the United States Air Force, becomes the first U.S. gay service member to purposely out himself to fight their ban; South Australia becomes the first state in Australia to make homosexuality legal between consenting adults in private. Panama is the third country in the world to allow transsexuals who have gone through gender reassignment surgery to get their personal documents reflecting their new sex;[citation needed] UK journal Gay Left begins publication;[97] Minneapolis becomes the first city in the United States to pass trans-inclusive civil rights protection legislation;[98] Clela Rorex, a clerk in Boulder County, Colorado, issues the first same-sex marriage licenses in the United States, issuing the very first of them to Dave McCord and Dave Zamora, on 26 March 1975.[99] Six same-sex marriages were performed as a result of her giving out licenses, but all of the marriages were overturned later that year.[99] Maureen Colquhoun becomes the UK's first out lesbian MP after coming out in 1975. She is the UK's first openly gay MP. Gay American Indians, the first gay American Indian liberation organization, is founded.[100]

- 1976 – Robert Grant founds the Christian Voice to take his anti-homosexual-rights crusade national in United States; the Homosexual Law Reform Coalition and the Gay Teachers Group are started in Australia; the Australian Capital Territory decriminalizes homosexuality between consenting adults in private and equalizes the age of consent; Out Minnesota state legislator Allan Spear is reelected; and Denmark equalizes the age of consent.[citation needed] Association of homosexual liberation was founded in Japan.[101] Tom Gallagher became the first United States Foreign Service officer to come out as gay; he quit the Foreign Service after that, as he would have been unable to obtain a security clearance.[102][103][104] Patricia Nell Warren's third novel, The Fancy Dancer (1976) was the first bestseller to portray a gay priest and to explore gay life in a small town.

-1977 – Harvey Milk is elected city-county supervisor in San Francisco, becoming the first openly gay or lesbian candidate elected to political office in California, the seventh openly gay/lesbian elected official nationally, and the third man to be openly gay at time of his election. Dade County, Florida enacts a Human Rights Ordinance; it is repealed the same year after a militant anti-homosexual-rights campaign led by Anita Bryant. Quebec becomes the first jurisdiction larger than a city or county in the world to prohibit discrimination based on sexual orientation in the public and private sectors; Croatia, Montenegro, Slovenia and Vojvodina legalise homosexuality.[citation needed] Welsh author Jeffrey Weeks publishes Coming Out;[105]
    Original eight-color version of the LGBT pride flag
    Publication of the first issue of Gaysweek, NYC's first mainstream gay weekly. Police raided a house outside of Boston outraging the gay community. In response the Boston-Boise Committee was formed.[106] Anne Holmes became the first openly lesbian minister ordained by the United Church of Christ;[107] Ellen Barrett became the first openly lesbian priest ordained by the Episcopal Church of the United States (serving the Diocese of New York).[108][109] The first lesbian mystery novel in America was published; it was Angel Dance, by Mary F. Beal.[110][111] The National Center for Lesbian Rights was founded. Shakuntala Devi published the first[112] study of homosexuality in India.[113][114] Platonica Club and Front Runners were founded in Japan.[101] San Francisco hosted the world's first gay film festival in 1977.[115] Peter Adair, Nancy Adair and other members of the Mariposa Film Group premiered the groundbreaking documentary on coming out, Word Is Out: Stories of Some of Our Lives, at the Castro Theater in 1977. The film was the first feature-length documentary on gay identity by gay and lesbian filmmakers.[116][117] Beth Chayim Chadashim became the first LGBT synagogue to own its own building.[82] On March 26, 1977, Frank Kameny and a dozen other members of the gay and lesbian community, under the leadership of the then-National Gay Task Force, briefed then-Public Liaison Midge Costanza on much-needed changes in federal laws and policies. This was the first time that gay rights were officially discussed at the White House.[118][119]

- 1978 – San Francisco Supervisor Harvey Milk and Mayor George Moscone are assassinated by former Supervisor Dan White; the first Sydney Gay and Lesbian Mardi Gras is held, with 2000 people attending and 53 subsequently arrested and some seriously beaten by police. The rainbow flag is first used as a symbol of gay pride; Sweden establishes a uniform age of consent. Samois, the earliest known lesbian-feminist BDSM organization, is founded in San Francisco; well-known members of the group include Patrick Califia and Gayle Rubin; the group is among the very earliest advocates of what came to be known as sex-positive feminism[citation needed]; The International Lesbian and Gay Association (ILGA) is established.[120] Robin Tyler became the first out lesbian on U.S. national television, appearing on a Showtime comedy special hosted by Phyllis Diller. The same year she released her comedy album, Always a Bridesmaid, Never a Groom, the first comedy album by an out lesbian.[121] The San Francisco Lesbian/Gay Freedom Band was founded by Jon Reed Sims in 1978 as the San Francisco Gay Freedom Day Marching Band and Twirling Corp. Upon its founding in 1978, it became the first openly gay musical group in the world. San Francisco became the first city in America to have a recruitment drive for gay police officers, bringing in over 350 applications.[122] Allen Bennett became the first openly gay rabbi in the United States.[123]

- 1979 – The first national homosexual rights march on Washington, D.C. is held; The White Night riots occur, Harry Hay issues the first call for a Radical Faerie gathering in Arizona, and The Sisters of Perpetual Indulgence first appear in public on Easter Sunday in San Francisco.[124] Cuba and Spain decriminalize homosexuality.[citation needed] Esta Noche, a gay bar located at 3079 16th & Mission Street in San Francisco, was the first gay Latino bar in San Francisco, and first opened in 1979. A number of people in Sweden called in sick with a case of being homosexual, in protest of homosexuality being classified as an illness. This was followed by an activist occupation of the main office of the National Board of Health and Welfare. Within a few months, Sweden became the first country in the world to remove homosexuality as an illness.[76] Japan Gay Center was established in Japan.[101] Grady Quinn and Randy Rohl became the first known gay couple to attend a high school prom when they attended the Lincoln High School prom in Sioux Falls, South Dakota on May 23, 1979.[125]


###1980s[edit]

- 1980 – The United States Democratic Party becomes the first major political party in the U.S. to endorse a homosexual rights platform plank; Scotland decriminalizes homosexuality; The Human Rights Campaign Fund is founded by Steve Endean; The Human Rights Campaign is America's largest civil rights organization working to achieve lesbian, gay, bisexual and transgender equality.[126] Lionel Blue becomes the first British rabbi to come out as gay;[127] "Becoming Visible: The First Black Lesbian Conference" is held at the Women's Building, from October 17 to 19, 1980. It has been credited as the first conference for African-American lesbian women.[128] The Socialist Party USA nominates an openly gay man, David McReynolds, as its (and America's) first openly gay presidential candidate in 1980.[129]

- 1981 – The European Court of Human Rights in Dudgeon v United Kingdom strikes down Northern Ireland's criminalisation of homosexual acts between consenting adults, leading to Northern Ireland decriminalising homosexual sex the following year; Victoria (Australia) and Colombia decriminalize homosexuality with a uniform age of consent; The Moral Majority starts its anti-homosexual crusade; Norway becomes the first country in the world to enact a law to prevent discrimination against homosexuals; Hong Kong's first sex-change operation is performed. The first official documentation of the condition to be known as AIDS was published by the US Centers for Disease Control and Prevention (CDC) on 5 June 1981.[130] Tennis player Billie Jean King became the first prominent professional athlete to come out as a lesbian, when her relationship with her secretary Marilyn Barnett became public in a May 1981 "palimony" lawsuit filed by Barnett.[131] Due to this she lost all of her endorsements.[132] Mary C. Morgan became the first openly gay or lesbian judge in America when she was appointed by California Governor Jerry Brown to the San Francisco Municipal Court.[133] Randy Shilts was hired as a national correspondent by the San Francisco Chronicle, becoming "the first openly gay reporter with a gay 'beat' in the American mainstream press."[134] A Sergeant of the New York City Police Department (Charles H. Cochrane Jr.) came out as gay during a city council hearing. This made him the first New York City Police Department member to publicly announce his homosexuality.[135]

- 1982 – Laguna Beach, CA elects the first openly gay mayor in United States history; France equalizes the age of consent; The first Gay Games is held in San Francisco, attracting 1,600 participants; Northern Ireland decriminalizes homosexuality; Wisconsin becomes the first US state to ban discrimination against homosexuals; New South Wales becomes the first Australian state to outlaw discrimination on the basis of actual or perceived homosexuality. The condition to be known as AIDS had acquired a number of names – GRID5 (gay-related immune deficiency), ‘gay cancer’, ‘community-acquired immune dysfunction’ and ‘gay compromise syndrome’[136] The CDC used the term AIDS for the first time in September 1982, when it reported that an average of one to two cases of AIDS were being diagnosed in America every day.[137] Ken Togo founded the Deracine Party in Japan. New York City police sergeant Charles H. Cochrane Jr. and former Fairview, New Jersey sergeant Sam Ciccone formed the first group targeted at the needs of gay members of law enforcement, the Gay Officers Action League (GOAL).[138] Portugal decriminalizes homosexuality for the second time in its history.

- 1983 – Massachusetts Representative Gerry Studds reveals he is gay on the floor of the House, becoming the first openly gay member of Congress; Guernsey (Including Alderney, Herm and Sark) decriminalizes homosexuality; AIDS is described as a "gay plague" by Reverend Jerry Falwell; BiPOL, the first and oldest bisexual political organization, is founded in San Francisco by bisexual activists Autumn Courtney, Lani Ka'ahumanu, Arlene Krantz, David Lourea, Bill Mack, Alan Rockway, and Maggi Rubenstein; Governor Jerry Brown appoints Herb Donaldson as the first openly gay male municipal court judge in the State of California;[139][140] David Scondras becomes the first openly gay official elected to the Boston City Council.[141]

- 1984 – The lesbian and gay association "Ten Percent Club" is formed in Hong Kong; Massachusetts voters reelect representative Gerry Studds, despite his revealing himself as homosexual the year before; New South Wales and the Northern Territory in Australia make homosexual acts legal; Chris Smith, newly elected to the UK parliament declares: "My name is Chris Smith. I'm the Labour MP for Islington South and Finsbury, and I'm gay", making him the first openly out homosexual male politician in the UK parliament (Maureen Colquhoun being the first openly out homosexual politician in the UK in 1975). The Argentine Homosexual Community (Comunidad Homosexual Argentina, CHA) is formed uniting several different and preexisting groups. Berkeley, California becomes the first city in the U.S. to adopt a program of domestic partnership health benefits for city employees; West Hollywood, CA is founded and becomes the first known city to elect a city council where a majority of the members are openly gay or lesbian. Reconstructionist Judaism became the first Jewish denomination to allow openly gay and lesbian rabbis and cantors.[142] ILGA Japan is founded in Japan. On Our Backs, the first women-run erotica magazine and the first magazine to feature lesbian erotica for a lesbian audience in the United States, was first published in 1984 by Debi Sundahl and Myrna Elana, with the contributions of Susie Bright, Nan Kinney, Honey Lee Cottrell, Dawn Lewis, Happy Hyder, Tee Corinne, Jewelle Gomez, Judith Stein, Joan Nestle, and Patrick Califia.[143] In 1984, BiPOL sponsored the first bisexual rights rally, outside the Democratic National Convention in San Francisco. The rally featured nine speakers from civil rights groups allied with the bisexual movement.

- 1985 – France prohibits discrimination based on lifestyle (moeurs) in employment and services; the first memorial to gay Holocaust victims is dedicated; Belgium equalizes the age of consent; the Restoration Church of Jesus Christ (informally called the Gay Mormon Church) is founded by Antonio A. Feliz.[144] Actor Rock Hudson dies of AIDS. He is the first major public figure known to have died from an AIDS-related illness.[145] Terry Sweeney becomes Saturday Night Live's first openly gay male cast member; Sweeney was "out" prior to being hired as a cast member.[146] The Bisexual Resource Center (BRC) in Massachusetts has served the bisexual community since 1985. Liverpool-based soap opera, Brookside featured the first openly gay character on a British TV series this year.[147]

- 1986 – Homosexual Law Reform Act passed in New Zealand, legalizing sex between males over 16; Haiti decriminalizes homosexuality, June in Bowers v. Hardwick case, U.S. Supreme Court upholds Georgia law forbidding oral or anal sex, ruling that the constitutional right to privacy does not extend to homosexual relations, but it does not state whether the law can be enforced against heterosexuals. Becky Smith and Annie Afleck became the first openly lesbian couple in America granted legal, joint adoption of a child.[148] From 1 till 3 May, the 1986, ILGA Asia Conference took place in Japan's capital Tokyo.[149] The Dutch Remonstrants are the world's first Christian denomination to perform same-sex unions and marriages.[150] Canadian province Ontario passes anti-discrimination legislation. Hill Street Blues featured the first lesbian recurring character on a major network; the character was a police officer called Kate McBride, played by Lindsay Crouse.[151]

- 1987 – AIDS Coalition to Unleash Power(ACT-UP) founded in the US in response to the US government's slow response in dealing with the AIDS crisis.[152] ACT UP stages its first major demonstration, seventeen protesters are arrested; U.S. Congressman Barney Frank comes out. Boulder, Colorado citizens pass the first referendum to ban discrimination based on sexual orientation.[153][154] In New York City a group of Bisexual LGBT rights activist including Brenda Howard found the New York Area Bisexual Network (NYABN); Homomonument, a memorial to persecuted homosexuals, opens in Amsterdam. David Norris is the first openly gay person to be elected to public office in the Republic of Ireland. A group of 75 bisexuals marched in the 1987 March On Washington For Gay and Lesbian Rights, which was the first nationwide bisexual gathering. The article "The Bisexual Movement: Are We Visible Yet?", by Lani Ka'ahumanu, appeared in the official Civil Disobedience Handbook for the March. It was the first article about bisexuals and the emerging bisexual movement to be published in a national lesbian or gay publication.[155] Canadian province of Manitoba and territory Yukon ban sexual orientation discrimination.

- 1988 – Sweden is the first country to pass laws protecting homosexual regarding social services, taxes, and inheritances. The anti-gay Section 28 passes in England and Wales; Scotland enacts almost identical legislation; Canadian MP Svend Robinson comes out; Canada lowers the age of consent for sodomy to 18; Belize and Israel decriminalize (de jure) sodomy and sexual acts between men (the relevant section in the old British-mandate law from 1936 was never enforced in Israel). After losing an Irish High Court case (1980) and an Irish Supreme Court case (1983), David Norris takes his case (Norris v. Ireland) to the European Court of Human Rights. The European Court strikes down the Irish law criminalising male-to-male sex on the grounds of privacy. Stacy Offner became the first openly lesbian rabbi hired by a mainstream Jewish congregation, Shir Tikvah Congregation of Minneapolis (a Reform Jewish congregation).[156][157] Robert Dover became the first openly gay Olympic athlete when he came out in 1988.[158]

- 1989 – Western Australia decriminalizes male homosexuality (but the age of consent is set at 21); Liechtenstein legalizes homosexuality; Denmark is the first country in the world to enact registered partnership laws (like a civil union) for same-sex couples, with most of the same rights as marriage (excluding the right to adoption (until June 2010) and the right to marriage in a church).

- 1989 - Braschi v Stahl Associates Co was a landmark case in which the New York court ruled that a gay couple that had lived together for a decade could be considered a family under New York City's rent control regulations. The decision in favor of litigant Miguel Braschi said that protection against eviction "should not rest on fictitious legal distinctions or genetic history, but instead should find its foundation in the reality of family life."[159][160][161]


###1990s[edit]

- 1991 4th October, Gary Doyle born Carpenterstown (Actually blanch) infamous drag queen and co parent to Louis

    - Equalization of age of consent: Czechoslovakia (see Czech Republic, Slovakia)

    - Decriminalisation of homosexuality: UK Crown Dependency of Jersey and the Australian state of Queensland

    - LGBT Organizations founded: BiNet USA (USA), OutRage! (UK), Queer Nation (USA), The Humsafar Trust (India)

    - Homosexuality no longer an illness: The World Health Organization

    - Other: Justin Fashanu is the first professional footballer to come out in the press.

    - Reform Judaism decided to allow openly lesbian and gay rabbis and cantors.[162]

    - Dale McCormick became the first open lesbian elected to a state Senate (she was elected to the Maine Senate).[163]

    - In 1990, the Union for Reform Judaism announced a national policy declaring lesbian and gay Jews to be full and equal members of the religious community. Its principal body, the Central Conference of American Rabbis (CCAR), officially endorsed a report of their committee on homosexuality and rabbis. They concluded that "all rabbis, regardless of sexual orientation, be accorded the opportunity to fulfill the sacred vocation that they have chosen" and that "all Jews are religiously equal regardless of their sexual orientation."

    - The oldest national bisexuality organization in the United States, BiNet USA, was founded in 1990. It was originally called the North American Multicultural Bisexual Network (NAMBN), and had its first meeting at the first National Bisexual Conference in America.[164][164][165] This first conference was held in San Francisco in 1990, and sponsored by BiPOL. Over 450 people attended from 20 states and 5 countries, and the mayor of San Francisco sent a proclamation "commending the bisexual rights community for its leadership in the cause of social justice," and declaring June 23, 1990 Bisexual Pride Day.
        - The first Eagle Creek Saloon, that opened on the 1800 block of Market Street in San Francisco in 1990 and closed in 1993, was the first black-owned gay bar in the city.[166]

- 1991

    - Decriminalisation of homosexuality: Bahamas, Hong Kong and Ukraine
    
    - AIDS Related: The red ribbon is first used as a symbol of the campaign against HIV/AIDS.
    
    - Anti-discrimination legislation: Canadian province Nova Scotia (sexual orientation)
    
    - Sherry Harris was elected to the City Council in Seattle, Washington, making her the first openly lesbian African-American elected official.[167]
    
    - The first lesbian kiss on television occurred; it was on L.A. Law between the fictional characters of C.J. Lamb (played by Amanda Donohoe) and Abby (Michele Greene).[168]
    
    - The Chicago Gay and Lesbian Hall of Fame was created in June 1991.[169] The hall of fame is the first "municipal institution of its kind in the United States, and possibly in the world."[169]
    
    - The AB101 Veto Riot occurs in California in response to then-Mayor of California Pete Wilson vetoing the Assembly Bill 101, a bill that would prohibit employers from discriminating against employees because of their sexual orientation.[170]

- 1992
    - Equalization of age of consent: Iceland, Luxembourg and Switzerland
    
    - Decriminalisation of homosexuality: Estonia and Latvia
    
    - Repeal of Sodomy laws: UK Crown Dependency of Isle of Man (homosexuality still illegal until 1994)
    
    - End to ban on gay people in the military: Australia, Canada
    
    - Recriminalisation of homosexuality: Nicaragua (until Mar 2008)
    
    - Anti-discrimination legislation: Canadian provinces New Brunswick and British Columbia (sexual orientation)
    
    - Althea Garrison was elected as the first transgender state legislator in America, and served one term in the Massachusetts House of Representatives; however, it was not publicly known she was transgender when she was elected.[171]
    
    - The Lesbian Avengers was founded in New York City by Ana María Simo, Sarah Schulman, Maxine Wolfe, Anne-Christine d'Adesky, Marie Honan, and Anne Maguire
    
    - Tokyo International Lesbian & Gay Film Festival was held in Japan.
    
    - Then-Mayor of California Pete Wilson signs the Assembly Bill 101, prohibiting employers in California from discriminating against employees based on their sexual orientation.[172]

- 1993
    - Civil Union/Registered Partnership laws:
    
        - Passed and Came into effect: Norway (without adoption until 2009, replaced with same-sex marriage in 2008/09)
        
    - Repeal of Sodomy laws: Australian Territory of Norfolk Island
    
    - Decriminalisation of homosexuality: Belarus, UK Crown Dependency of Gibraltar, Ireland, Lithuania, Russia (with the exception of the Chechen Republic);
    
    - Anti-discrimination legislation:
    
        - US state of Minnesota (gender identity)
        
        - New Zealand parliament passes the Human Rights Amendment Act which outlaws discrimination on the grounds of sexual orientation or HIV
        
        - Canadian province Saskatchewan (sexual orientation)
        
    - End to ban on gay people in the military: New Zealand
    
    - Significant LGBT Murders: Brandon Teena
    
    - Melissa Etheridge came out as a lesbian.
    
    - The Triangle Ball was held; it was the first inaugural ball in America to ever be held in honor of gays and lesbians.
    
    - The first Dyke March (a march for lesbians and their straight female allies, planned by the Lesbian Avengers) was held, with 20,000 women marching.[173][174]
    
    - Roberta Achtenberg became the first openly gay or lesbian person to be nominated by the president and confirmed by the U.S. Senate when she was appointed to the position of Assistant Secretary for Fair Housing and Equal Opportunity by President Bill Clinton.[175]
    
    - Lea DeLaria was "the first openly gay comic to break the late-night talk-show barrier" with her 1993 appearance on The Arsenio Hall Show.[176]
    
    - In December 1993 Lea DeLaria hosted Comedy Central's Out There, the first all-gay stand-up comedy special.[176]
    
    - Before the "Don't Ask Don't Tell" policy was enacted in 1993, lesbians and bisexual women and gay men and bisexual men were banned from serving in the military.[177] In 1993 the "Don't Ask Don't Tell" policy was enacted, which mandated that the military could not ask servicemembers about their sexual orientation.[178][179] However, until the policy was ended in 2011 service members were still expelled from the military if they engaged in sexual conduct with a member of the same sex, stated that they were lesbian, gay, or bisexual, and/or married or attempted to marry someone of the same sex.[180]
        
        
- 1994

    - Unregistered Cohabitation recognition:
        
        - Passed and Came into effect: Israel (without adoption, without step-adoption until 2005)
    
    - Anti-discrimination legislation: South Africa (sexual orientation, interim constitution)
    
    - Decriminalisation of homosexuality: Bermuda, UK Crown Dependency of Isle of Man and Serbia, Commonwealth of Australia
    
    - Equalization of age of consent:
        
        - Partial: UK reduces the age of consent for homosexual men to 18;
    
    - Homosexuality no longer an illness: American Medical Association
    
    - LGBT Organizations founded: National Coalition for Gay and Lesbian Equality (South Africa)
    
    - Other : Canada grants refugee status to homosexuals fearing for their well-being in their native country; Toonen v. Australia decided by UN Human Rights Committee; fear of persecution due to sexual orientation becomes grounds for asylum in the United States.[181]
    
    - Deborah Batts became the first openly gay or lesbian federal judge; she was appointed to the U.S. District Court in New York.[182][183]
    
    - The Gay Asian Pacific Alliance and Asian Pacific Sister joined the Chinese New Year Parade in San Francisco, which was the first time that queer Asian American communities had attended in a publicly ethnic activity.[184]
    
    - Gay Parade was held in Japan. (8.1994)
    
    - Wilson Cruz became the first gay actor to play an openly gay character in a leading role in a television series (My So-Called Life).
    
    - Steve Gunderson was outed as gay on the House floor by representative Bob Dornan (R-CA) during a debate over federal funding for gay-friendly curricula,[185] making him one of the first openly gay members of the U.S. Congress and the first openly gay U.S. Republican representative.[186]
    
    - Liverpool-based soap opera, Brookside, broadcast the UK's first pre-watershed lesbian kiss.[187]
    
    - The broadcast of Pedro Zamora and Sean Sasser's commitment ceremony in 1994, in which they exchanged vows, was the first such same-sex ceremony in television history, and is considered a landmark in the history of the medium.[188][189]

-1995
        - Civil Union/Registered Partnership laws:
            
            - Passed and Came into effect: Sweden (with adoption, replaced with same-sex marriage in April 2009)
            
        - Anti-discrimination legislation: Canadian province Newfoundland and Labrador (sexual orientation)
        
        - Decriminalisation of homosexuality: Albania and Moldova
        
        - AIDS Related: Triple combination therapy of drugs such as 3TC, AZT and ddC shown to be effective in treating HIV, the virus responsible for AIDS[190]
        
        - LGBT Organizations founded: Gay Advice Darlington/Durham was founded by local gay and bisexual men, and has developed into a Charity that works with and for the Lesbian, Gay, Bisexual and Transgender (LGBT) Community of County Durham and Darlington.
        
        - Other : The Human Rights Campaign drops the word "Fund" from their title and broadens their mission to promote "an America where gay, lesbian, bisexual and transgender people are ensured equality and embraced as full members of the American family at home, at work and in every community;"
        
        - Rachel Maddow became the first openly gay or lesbian American to win an international Rhodes Scholarship.
        
        - Kings Cross Steelers, the world's first gay rugby club, was founded.[191]
        
        - Rabbi Margaret Wenig's essay "Truly Welcoming Lesbian and Gay Jews" was published in The Jewish Condition: Essays on Contemporary Judaism Honoring Rabbi Alexander M. Schindler; it was the first published argument to the Jewish community on behalf of civil marriage for gay couples.[192]
        
        - Ed Flanagan served as Vermont's State Auditor from 1993 through 2001, becoming the first openly gay, statewide-elected official in the United States when he came out in 1995, before his 1996 reelection.[193]
        
- 1996
        
        - Civil Union/Registered Partnership laws:
            
            - Passed and Came into effect: Iceland (with step-adoption, without joint adoption until 2006, replaced with same-sex marriage in 2010)
        
        - Unregistered Cohabitation recognition:
            
            - Passed and Came into effect: Hungary (replaced with registered partnerships in 2009)
        
        - Restriction of LGBT partnership rights: USA, (federal, see DOMA)
        
        - Equalization of age of consent: Burkina Faso
        
        - Decriminalisation of homosexuality: Romania, North Macedonia, Macau
        
        - Anti-discrimination legislation: Canada (federal, sexual orientation)
        
        - The first lesbian wedding on television occurred, held for fictional characters Carol (played by Jane Sibbett) and Susan (played by Jessica Hecht) on the TV show "Friends".[194]
        
        - The first openly gay speaker at a Republican National Convention was Log Cabin Republicans member Steve Fong of California in 1996.[195]
        
        - Muffin Spencer-Devlin became the first LPGA player to come out as gay.[196]

- 1997
        
        - Anti-discrimination legislation: Fiji (sexual orientation, constitution) and South Africa (sexual orientation, constitution)
        
        - Equalization of age of consent: Russia
        
        - Decriminalisation of homosexuality: Ecuador, Venezuela and the Australian state of Tasmania
        
        - Other : Israeli President Ezer Weizman compares homosexuality to alcoholism in front of high school students.[197] The UK extends immigration rights to same-sex couples akin to marriage; Ellen DeGeneres came out as a lesbian, one of the first celebrities to do so.[198] Furthermore, later that year her character Ellen Morgan came out as a lesbian on the TV show Ellen, making Ellen DeGeneres the first openly lesbian actress to play an openly lesbian character on television.[199] Patria Jiménez became the first openly gay person to win a position in the Mexican Congress, doing so for the Party of the Democratic Revolution.[200] The Gay and Lesbian Medical Association launched the Journal of the Gay and Lesbian Medical Association, the world's first peer-reviewed, multi-disciplinary journal dedicated to LGBT health. The first open-mouth kiss between two women on prime time television occurred, on ABC's Relativity.[201]

- 1998
        
        - Anti-discrimination legislation: Ecuador (sexual orientation, constitution), Ireland (sexual orientation) and the Canadian provinces of Prince Edward Island (sexual orientation) and Alberta (court ruling only; legislation amended in 2009)
        
        - Significant LGBT Murders: Rita Hester, Matthew Shepard
        
        - Decriminalisation of homosexuality: Bosnia and Herzegovina, Kazakhstan, Kyrgyzstan, South Africa (retroactive to 1994), Republic of Cyprus and Tajikistan
        
        - Equalization of age of consent: Croatia and Latvia
        
        - End to ban on gay people in the military: Romania, South Africa
        
        - Gender identity was added to the mission of Parents and Friends of Lesbians and Gays after a vote at their annual meeting in San Francisco.[202] Parents and Friends of Lesbians and Gays is the first national LGBT organization to officially adopt a transgender-inclusion policy for its work.[203]
        
        - Tammy Baldwin became the first openly gay or lesbian non-incumbent ever elected to Congress, and the first open lesbian ever elected to Congress, winning Wisconsin's 2nd congressional district seat over Josephine Musser.[204][205]
        
        - Dana International became the first transsexual to win the Eurovision Song Contest, representing Israel with the song "Diva".[206]
        
        - Robert Halford comes out as being the first openly gay heavy metal musician.[207]
        
        - The first bisexual pride flag was unveiled on 5 December 1998.[208]
        
        - Julie Hesmondhalgh first began to play Hayley Anne Patterson, British TV's first transgender character.[209]
        
        - BiNet USA hosted the First National Institute on Bisexuality and HIV/AIDS.[210]

- 1999
        
        - Civil Union/Registered Partnership laws:
            
            - Passed and Came into effect: US State of California (without adoption, without step adoption until 2001, same-sex marriage in June 2008-November 2008)
            
            - Passed and Came into effect: France
        
        - Decriminalisation of homosexuality: Chile
        
        - Equalization of age of consent: Finland (without adoption)
        
        - LGBT Organizations founded: "Queer Youth Alliance" (UK)
        
        - Other: Israel's supreme court recognizes a lesbian partner as another legal mother of her partner's biological son; South Africa grants spousal immigration benefits to same-sex partners.
        
        - Steven Greenberg publicly came out as gay in an article in the Israeli newspaper Maariv. As he has a rabbinic ordination from the Orthodox rabbinical seminary of Yeshiva University (RIETS), he is generally described as the first openly gay Orthodox Jewish rabbi.[211] However, some Orthodox Jews, including many rabbis, dispute his being an Orthodox rabbi.[212]
        
        - The Transgender Day of Remembrance was founded in 1999 by Gwendolyn Ann Smith, a trans woman who is a graphic designer, columnist, and activist,[213] to memorialize the murder of Rita Hester in Allston, Massachusetts.[214] Since its inception, TDoR has been held annually on 20 November,[215] and it has slowly evolved from the web-based project started by Smith into an international day of action.
        
        - In 1999, the first Celebrate Bisexuality Day was organized by Michael Page, Gigi Raven Wilbur, and Wendy Curry.[216]


###2000s

- 2000
        
        - Civil Union/Registered Partnership laws:
            
            - Passed and Came into effect: US State of Vermont
        
        - Anti-discrimination legislation: South Africa (discrimination, hate speech, harassment; see PEPUDA)
        
        - Revoking of discrimination legislation: UK subdivision of Scotland (Section 28)
        
        - End to ban on gay people in the military: United Kingdom. See also: Sexual orientation and military service and Stonewall
        
        - Equalization of age of consent: Belarus, Israel and United Kingdom (passed eff. 2001)
        
        - Decriminalisation of homosexuality: Azerbaijan and Georgia
        
        - Other: In Germany the Bundestag officially apologizes to gays and lesbians persecuted under the Nazi regime, and for "harm done to homosexual citizens up to 1969"; Israel recognizes same-sex relations for immigration purposes for a foreign partner of an Israeli resident.
        
        - The Transgender Pride flag was first shown, at a pride parade in Phoenix, Arizona.
        
        - Hillary Clinton became the first First Lady to march in an LGBT pride parade.[217]










   
        

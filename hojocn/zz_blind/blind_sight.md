
# 一些盲人其实能看见，而这种吓人的能力存在所有人脑中  

source: https://mp.weixin.qq.com/s/Ola9948uw0wUI8B2zgFGbQ  

环球科学 2022-11-09 20:00  
来源｜把科学带回家（id：steamforkids）  
撰文｜七君  

说到失明，大家一般都会认为失明的人什么也看不到。可令人震惊的是，一些盲人实际上可以“看见”，只不过他们自己意识不到而已。发现了这个现象的科学家一开始饱受质疑，但是现在它已经被大部分研究者接受了。

大家先来看看这段视频——  
![](img/blind_sight_00.gif) 
![](img/blind_sight_01.gif)  
这位盲人（浅色裤子的）能够“看见”并避让障碍物。（图片来源：doi.org/10.1016/j.cub.2008.11.002） 

这段视频看起来没什么毛病，不就是一个人绕开房间里的杂物吗？可是你要知道，这个能绕开障碍物的人是一个盲人，而这段视频是他的研究者拍摄下来的。
盲人能看见，这怎么可能？
没错，不仅是你，连这个盲人自己都觉得不可思议，因为他确实没有“看见”，仅仅是凭着说不出的感觉行走的。
要不是研究者们拍摄了视频、进行了详尽的研究，并把结果发表在顶级期刊上，相信许多人都会认为这是扯淡。
事情要从这位盲人是怎么失明的说起。

![](img/blind_sight_02.jpg)  
图片来源：wikimedia  


这位化名为TN的盲人实际上是一位医生，老家是非洲的布隆迪。在失明之前，他在瑞士为世界卫生组织工作。2003年，五十出头的TN在短短几周内遭遇了两次中风。
这两次中风彻底摧毁了他大脑里负责“看见”的部位——视觉皮层。虽然他的眼睛和其他脑区没有受伤，但是因为脑子里的“显卡”坏掉了，他也因此失明了。

![](img/blind_sight_03.gif)  
视觉皮层和眼球之间的通路。（图片来源：见水印）

TN来到了瑞士日内瓦大学附属医院就医，并在那里遇到了研究者Alan Pegna。Pegna 给TN做了好多实验。他惊讶地发现，这个盲人虽然失明，但却能读懂摆照片里的惊恐表情，并和普通人一样表现出了害怕。  
Pegna 把这个神奇的现象发表了出来。哈佛大学和荷兰蒂尔堡大学（Tilburg University）的神经科学家 Beatrice de Gelder 看到了这项研究，觉得非常有趣，因此在2007年邀请TN飞来荷兰参与她的研究。  

![](img/blind_sight_04.jpg)  
图片来源：wikimedia  

一开始，走障碍迷宫并不在实验计划之内。然而有一天，细心的研究者发现TN居然能避开障碍物，于是有了让他走障碍迷宫的大胆想法。
对于这种“盲人摸象”的实验，TN本人最初是拒绝的。de Gelder 回忆：“一开始他也很紧张，不知道要做什么，因为毕竟自己是个盲人嘛。”但是后来他还是同意了。事实证明，他居然真的能绕开房间里的垃圾箱、三脚架、一叠纸还有好几个盒子。
有没有一种可能，那就是TN其实并没有失明呢？
为了确认他真的看不见，研究者们给他做了大量脑成像和配套实验。结果显示，他的视觉皮层并没有任何和视觉相关的活动。研究者还指出，并没有证据显示这个盲人可以像蝙蝠那样通过回声定位来观察四周。

![](img/blind_sight_05.gif)  
de Gelder实验室的核磁共振成像实验设备。（图片来源：见水印）


剩下的就只有一种可能了，那就是他的大脑看见了，但是他的意识没有看见。

不论原因为何，这是研究者们首次发现视觉皮层完全损毁的人还具有视觉能力。这种能力被称为盲视（Blindsight），而这项研究发表在2008年的一期 Current Biology上。同年，Nature 也对这个奇异的现象进行了报道。所以，TN到底为什么能够“盲视”呢？
目前的主流理论和人脑中一个古老的视觉系统有关。
刚才说过TN大脑的“显卡”完全挂了，这个“显卡”位于枕叶，也就是后脑勺的部位，它叫做初级视皮层（或者说纹状皮层）。

![](img/blind_sight_06.jpg)  
初级视觉皮层（红色）（图片来源：wikimedia）

不过，哺乳动物的“显卡”有两个，除了上面这个比较高级的，还有一个原始的。这个低配的“显卡”是所有脊椎动物共通的，鱼类、两栖动物以及爬行动物都有，它位于中脑的视顶盖（optic tectum），也能接收来自视网膜的信号。


![](img/blind_sight_07.gif)  
中脑（红色）（图片来源：physiopedia）


视顶盖是脊椎动物脑中最保守的“硬件”之一，不管是原始脊椎动物（比如七鳃鳗）还是人类都有。由于功能过于基础和硬核，所有脊椎动物都没有把它淘汰掉。
那视顶盖的功能是什么呢？
借助内部能认地标的位置细胞（Place cell）、能追踪头部方位的头方向细胞（head-direction cells），还有能识别边界的边界细胞（border cells），视顶盖可以协助脊椎动物捕食、逃跑还有转头。

![](img/blind_sight_08.gif)  
蝌蚪的视顶盖（粉红色圆球所指）使其作出躲避行为。（图片来源：blogs.brown.edu/aizenmanlab/projects/）


不过，不同脊椎动物对视顶盖的依赖程度不同。麻省理工学院的脑科学教授Richard Held 指出，在较为低等的哺乳动物中，中脑视觉系统起到了重要的作用。在视顶盖上搞一些小动作甚至可以操控一些简单的脊椎动物，比如斑马鱼，让它们出现“幻觉”。

![](img/blind_sight_09.jpg)  
斑马鱼相较人类较为原始，它们的行为受到视顶盖支配（图片来源：wikimedia）

演化程度接近人类的灵长目动物虽然不会完全被视顶盖操控，但也能靠它在失明的情况下导航。
1965年就有一项经典研究证实了这一点。在这个实验中，剑桥大学的心理学家 Lawrence Weiskrantz 和同事切除了猕猴“海伦”的初级视皮层。手术后，海伦什么也看不见了。

![](img/blind_sight_10.gif)  
失明的猕猴海伦能够躲避障碍物，并捡起食物。（图片来源：Nicholas Humphrey）



当时参与了这项研究，后来成为伦敦政经学院心理学教授的 Nicholas Humphrey 觉得很奇怪，他不相信视顶盖对哺乳动物没影响，于是继续对海伦进行实验。

在和海伦的互动中，他发现海伦能从他的手上精准取走食物，甚至还会去摸他的手电筒。术后七年，海伦可以像视频中的那样自如地绕开障碍物，抓起地上的花生。


![](img/blind_sight_11.gif)  
失明的猕猴海伦能够“看见”食物。（图片来源：Nicholas Humphrey）

Weiskrantz 感到很有趣。后来，他找到了一个化名为DB的盲人。这个病人左半球的视觉皮层部分受损，因此看不到右侧视野的东西（左半球对应右侧视野，右半球对应左侧视野）。

![](img/blind_sight_12.gif)  
对一侧失明的患者，研究者通常会给他们的不同视野呈现不同信息。（图片来源：见水印）



DB认为自己看不到右侧视野里的任何东西，但是 Weiskrantz 让他猜猜看。令人惊讶的是，DB的盲猜正确率达到了100%，连他自己都觉得相当意外。

1973年，Weiskrantz 把这个在缺乏视觉皮层处理的情况下出现视觉感知，或者说盲人能“看见”的现象取名为盲视，这是盲视首次出现在文献中。不过一开始，盲视现象饱受争议，然而随着越来越多的病例出现，现在已经不会有人怀疑它的存在了。


那么，正常人也能盲视吗？


De Gelder 认为普通人也具有盲视能力，只不过我们意识不到而已。2010年，在发表在 Nature Reviews Neuroscience 上的一项综述中 De Gelder 和同事解释道，由于视顶盖的存在，实际上健康的人也能因为看到什么而无意识地涌起情绪。


盲视或许就是那个一直被我们忽略的“第六感”吧。

它让心看见，心却看不见它。


## 参考资料：
- De Gelder, Beatrice, et al "Intact navigation skills after bilateral loss of striate cortex." Current biology 18.24 (2008): R1128-R1129.
- Weiskrantz, L., et al. "Visual capacity in the hemianopic field following a restricted occipital ablation." (1974): 709-728.
- Tamietto, Marco, and Beatrice De Gelder. "Neural bases of the non-conscious perception of emotional signals." Nature Reviews Neuroscience 11.10 (2010): 697-709.
- Cowey, Alam. "The blindsight saga." Experimental brain research 200.1 (2010):3-24.
- Helen: "A Blind Monkey Who Sees Everything". Nick Humphrey
- www.beatricedegelder.com/videos.html
- aeom.co/essays/how-blindsight-answers-the-hard-problem-of-consciousness
- www.nytimes.com/2008/t2/23/health/23blin.html
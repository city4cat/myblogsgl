
# FC的分镜

目录： 
[readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

--------------------------------------------------

## Vol03	CH015	page003		女星的诞生!？  	A Star Is Born!?  		
### 03_011_2：  
叶子和雅彦听说学长横田进曾经在这里给空做助手，惊呆了。这个镜头无边框。  


----------------------------------------------------------

## Vol03	CH016	page031		扮女人的测试  	Transvestite Apprentice  		
### 03_045_3：  
雅美、紫苑、叶子三人逛街。三人肩膀处的衣服边线有加粗。我猜，可能是为了以此突出前景人物。  


----------------------------------------------------------

## Vol03	CH017	page059		追击  	Repetition  		
### 03_064_4：  
紫苑的镜头破格，且无边框。  

### 03_082_3：  
这个镜头有畸变--门的线条、地板的线条、楼梯的线条都略有弯曲。应该使用这个效果来表示“看到雅彦和江岛kiss后，叶子的头有点儿懵”的效果。蓝色线条是我标记的直线。  
![](./img/03_082_3__.jpg)


----------------------------------------------------------

## Vol03	CH018	page087		意想不到的助手  	An Unexpected Collaborator  


----------------------------------------------------------	
	
## Vol03	CH019	page113		辰已，纯情的爱…!?  	Tatsumi's Unwavering Love  	


----------------------------------------------------------	
  
## Vol03	CH020	page141		阔别了18年的妹妹  	A Little Sister Not Seen For Over 18 Years  


----------------------------------------------------------

## Vol03	CH021	page169		父与"女"!  	The Father And The "daughter"!  		
### 03_182_2:  
前景人物加了阴影，且阴影边缘模糊。我猜，这可能是为了表现景深(DOF)效果--焦点在背景人物(宪司)，前景人物(若苗空)模糊。  
![](img/03_182_2__.jpg)  


----------------------------------------------------------

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

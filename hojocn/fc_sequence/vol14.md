
# FC的分镜

目录： 
[readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

--------------------------------------------------


## Vol14	CH092	page003		恐怖的迎新联欢会  	Scary Meeting  		  

### 14_011：  
左边的背景和右边的不一样，用来表示不同人群的不同心情：  
![](./img/14_011_3__.jpg)  

### 14_012：  
构图。两个镜头左侧(角色背后)空白大，说明左侧有戏。同时，两个连续镜头都是左侧空白大，“左侧有戏”的效果被加强（比如，恐怖片可用这种手法）。  
![](./img/14_012_1__.jpg) 
![](./img/14_012_2__.jpg)  


### 14_013：  
镜头横跨整个页宽，可表示这个镜头持续时间长。同时人物画得小，我猜，这使得"镜头持续时间长"的效果被加强。  
![](./img/14_013_0__.jpg)

### 14_014：  
雅彦关注紫苑，突然仁科闯入：  
![](./img/14_014_4__cn.jpg) 
![](./img/14_014_5__cn.jpg)  
镜头2相比于镜头1的构图，紫苑的位置左移。  
两图里的紫苑线条一模一样，[疑问]这能说明FC制作流程相关的一些事情吗？（是否是复制图片？）


### 14_020：  
江岛接受挑战。对话框分隔两人，似乎能从构图上增加对立的气氛：  
![](./img/14_020_3__cn.jpg)  
14_020，14_022, 继续用这种镜头表现二人的对立：  
![](./img/14_020_4_5__cn.jpg) 
![](./img/14_022_2_3__cn.jpg)  


### 14_021_1:  
看到江岛和若苗拼酒，雅彦：“他们二人的样子有点古怪...我去看一下”。  
镜头仰视，可能是表现雅彦的勇敢。  
![](img/14_021_1__.jpg)  

### 14_023：  
前景人物加了阴影。画面更有层次感。  
![](./img/14_023_5__.jpg)  

### 14_024_0:  
镜头倾斜，可能是表现角色喝醉的感觉。  
![](img/14_024_0__.jpg)  

### 14_024：  
雅彦喝醉，胳膊支撑沙发不稳，跌倒：  
![](./img/14_024_2__.jpg)  


### 14_025_0:  
帽子处的着色有偏移，可能是表现角色喝醉的感觉。  
![](img/14_025_0__.jpg) 
![](img/14_025_0__crop0.jpg) 
![](img/14_025_0__crop1.jpg) 

### 14_027：  
神秘人物（浩美）出现。边缘线很粗，和角色接触部分的边框被抹去；不知道这是表示什么效果。（我猜，粗线条是为了区分前景人物和背景。类似02_016_1, 02_016_2的分析）  
![](./img/14_027_2__.jpg)   
01_162_6也有类似的镜头：     
![](./img/01_162_6__.jpg)   

### 14_027_3:  
酒后慢慢醒来的镜头。  
![](img/14_027_3__.jpg)  

### 14_028：  
雅彦问紫苑的情况，仁科说：“你不用担心”。这里讲述昨晚紫苑的镜头有边框，讲昨晚浩美营救紫苑、并背紫苑回家。  
![](./img/14_028_6_7__cn.jpg)  
“后来我才知道”，这又进入雅彦的独白：  
![](./img/14_029_3__cn.jpg)  
然后又说起江岛，用一个空白搭配内心独白：  
![](./img/14_029_5__cn.jpg)  
以上这段叙事方式很赞！算是倒叙？或插叙？  



----------------------------------------------------------

## Vol14	CH093	page031		再会  	Reunion  		  

### 14_032_1:  
江岛告诉雅彦“传说中的『幻之女』已经找到了”， 雅彦大惊。  
镜头里角色面部加阴影，背景线条用横线。  
![](img/14_032_1__.jpg)  

### 14_032：  
在课堂上雅彦大喊，忘记了是在上课。旁人的目光聚集向雅彦，是之成为焦点。另，旁人只有简单线条，没有上色，也是为了突出主角。    
![](./img/14_032_4__.jpg)    


### 14_035：  
江岛手部的动作衔接镜头：  
![](./img/14_035_4__.jpg) 
![](./img/14_035_6__.jpg)  

### 14_036：  
镜头2压住镜头1。以往都是后续镜头被压住。  
![](./img/14_036_0_2__cn.jpg)  

### 14_036：  
摄像师抽烟的动作衔接镜头：  
![](./img/14_036_2__.jpg) 
![](./img/14_036_4__.jpg)  


### 14_037：  
雅彦和盐谷从门缝里偷窥『幻之女』。  
镜头里门框加了阴影。    
![](./img/14_037_3__.jpg) 
![](./img/14_037_4__.jpg)  

### 14_037：  
推迟浅葱的露面：  
![](./img/14_037_5__.jpg) 
![](./img/14_037_6__.jpg) 
![](./img/14_038_3__.jpg) 
![](./img/14_038_4__.jpg)  
露面时被背景的门框围住，构图上来讲，有突出人物的作用：  
![](./img/14_039_0__.jpg)  

### 14_038：  
左侧路人和景物都画的简略：  
![](./img/14_038_1__.jpg)  

### 14_038：  
借江岛的大喊，把紫苑介绍给浅葱：  
![](./img/14_038_3_4__cn.jpg)  
同时也借江岛之口，把浅葱介绍给紫苑（和读者）：  
![](./img/14_039_1_3__cn.jpg)  

### 14_042：  
背景虚化  
![](./img/14_042_6__.jpg)

### 14_044：  
内心独白，头部用非白过渡：  
![](./img/14_044_4__.jpg)  

### 14_046：  
上方的两个镜头挨着页边，底部的镜头也挨着页边：  
![](./img/14_046.jpg)  

### 14_046：  
给浅葱一个镜头，然后进入她的内心独白：  
![](./img/14_046_3_4__cn.jpg)  


### 14_048_5，14_052_7:  
紫苑回忆小时候的浅葱。  
过渡到回忆画面。第一个回忆画面无边框。      
![](./img/14_048_5_6__.jpg)  
回忆的最后一个画面，即将退出回忆，此画面无边框。  
![](img/14_052_7__.jpg)  


### 14_050：  
用1页（8个镜头）概括了紫苑和浅葱之间的打闹。紫苑整蛊浅葱，2个镜头；浅葱整蛊紫苑，2个镜头，然后是两人日常打闹斗嘴：  
![](./img/14_050_0__cn.jpg)  


### 14_053：  
雅彦的鼻子有阴影：  
![](./img/14_053_5__.jpg)  


### 14_054：  
墙上的光影与远处背景的暗色形成对比，有层次感：  
![](./img/14_054_4__.jpg)  


### 14_055：  
紫苑手部动作衔接镜头：  
![](./img/14_055_2__.jpg) 
![](./img/14_055_3_4__.jpg)





----------------------------------------------------------

## Vol14	CH094	page057		浅葱到访  	Asagi's Visit  		  
### 14_055, 14_062：  
这两幅图一模一样，但墨色深浅不同。为什么？[疑问]（不排除是扫描导致的）  
![](./img/14_055_3_4__.jpg) 
![](./img/14_062_0_1__.jpg) 

### 14_055, 14_062：  
上一话结尾：  
![](./img/14_055_3_4__.jpg) 
![](./img/14_055_5__.jpg)  
本话除了借用外，还增加了几个镜头：  
![](./img/14_062_2_4__cn.jpg)  
我猜，上一话由于篇幅限制所以匆匆结尾，这里补上。

### 14_062，14_063
雅彦提到之前"浅葱表白紫苑"的场景（蓝框所示）。借助翻页切换至回忆里的场景，镜头实线边框。回忆完毕，切换至当前场景（蓝框所示），镜头无两侧边框。  
![](img/14_062_063__mod.jpg)  


### 14_063_2:  
雅彦："她向你说出那样的话，感觉如何？"（指浅葱表白紫苑）  
紫苑："如何......？初恋情人对你真情表白......总会感到开心之类的吧......"  
然后的镜头如下，画面人物分别位于左1/3和右1/3，雅彦的眼睛位于低位，暗示其心情不佳。    
![](img/14_063_2__.jpg)  

### 14_065_1:  
该场景结束的过渡镜头。  
![](img/14_065_1__.jpg)  

### 14_066：  
左下角的镜头的布局没有和上部的镜头对齐：  
![](./img/14_066_4_6__.jpg)

### 14_068_4:  
浅葱来到若苗家，紫苑想尽快带她出门，以免自己男装的身份被父母发现，但浅葱想和紫苑待在屋里(暧昧)。    
紫苑：“天气这么好，于其闷在家里，不如出去逛一下吧！”  
浅葱：“哇～～～这个主意虽好...”(下图所示该镜头)  
镜头里对话框遮眼睛，突出其内心活动。  
![](img/14_068_4__.jpg)  

### 14_068_6:  
紫苑心想：“果然.....没那么容易能够带她（浅葱）出去......”
镜头（下图左）里紫苑位于画面右1/2，眼睛斜视左侧；眼白应该是加了灰网点阴影，表内心活动。让眼睛向右侧斜视（下图右），对比一下：  
![](img/14_068_6__.jpg)<--------------------------------->
![](img/14_068_6__mod.jpg) 


### 14_071_5:  
紫苑得知若苗紫这么快回家，惊讶和紧张。  
镜头里面部整体加了网点阴影，头发加了竖排线。  
![](img/14_071_5__.jpg)  

### 14_075_0:  
若苗紫看到浅葱的鞋，知道紫苑的朋友来道家里做客，便端茶水去款待。雅彦看到后紧张，因为若苗紫会看到男装的紫苑。 
茶水位于镜头中央，是二人的焦点。   
![](img/14_075_0__.jpg)  

### 14_076_0:  
浅葱主动拥抱紫苑。  
镜头使用黑色背景，表示惊讶或紧张，不是浪漫的气氛。  
![](img/14_076_0__.jpg)  

### 14_080：  
两人望向窗外：紫苑不在，不知道去哪里了。这里没有画窗户，而是留了空白。可能是为了强化“不知道去哪里了”这个效果。    
![](./img/14_080_0__.jpg)  



----------------------------------------------------------

## Vol14	CH095	page083		浅葱突击行动  	Asagi's Plan  		  

### 14_094_6:  
紫苑带浅葱匆匆逃出若苗家。  
紫苑心里想：“糟了～～～～被妈妈看见了......”   
人物破格。  
![](img/14_094_6__.jpg)  

### 14_095_5:  
紫苑带浅葱匆匆逃出若苗家后送浅葱去车站，雅彦跟来。  
镜头（下图左一）先给雅彦一个黑影，增加神秘感，同时省略了背景。
再让雅彦亮相（下图左二）。    
![](img/14_095_5__.jpg) 
![](img/14_095_6__.jpg) 

### 14_096_6,14_098_5  
浅葱(14_096_6)：“你刚才想拥有我那件事......是向我的表白，表示OK的答复吧.....”  
浅葱(14_098_5)：“我在想...你是坏男孩的话，我也跟你做坏女孩...可是...最终也做不成......”  
两个镜头里有表示浪漫的光晕。  
![](img/14_096_6__.jpg) 
![](img/14_098_5__.jpg) 

### 14_097_0:  
近景人物加面部阴影，远景人物面部未加阴影。[疑问]为什么？  
![](img/14_097_0__.jpg)  

### 14_099_2:  
一切都是浅葱的一厢情愿。浅葱：“不过...并不表示我会放弃的......你没有对我作出任何事情，是因为你尊重我吧？”  
接下来的镜头里有大面积的阴影，且机车很高。如果按照[14_285_0](#14_285_0)里的解释，这些可能暗示两人未来的关系不顺利。  
![](img/14_099_2__.jpg)  

### 14_100_0, 14_118_0：  
两幅图一样，但后者画面粗糙:  
![](./img/14_100_0__.jpg) 
![](./img/14_118_0__.jpg)  

### 14_101_4:  
背景虚化。  
![](img/14_101_4__.jpg)  

### 14_104_4:  
回家路上，紫苑焦虑：“我应该怎样向妈妈....解释呢.....”  
镜头里建筑物的阴影，暗示后续可能的紧张。  
![](img/14_104_4__.jpg)  

### 14_105_3:  
紫苑对若苗紫说：“我会给你好好解释的。”  
背景使用聚焦线条，表示人物精神抖擞，暗示其内心胸有成竹。  
![](img/14_105_3__.jpg)  
### 14_112：  
本话开始的回忆镜头全是黑色边框背景。回忆的最后一个镜头的边框背景加了白色，可能是示意将要退出回忆。  
![](./img/14_112_6__.jpg)






----------------------------------------------------------

## Vol14	CH096	page109		哪方面较好呢？  	It Doesn't Matter?!  		  


### 14_115_0_2   
如红框所示，或许可以看作用盐谷(的衣服)衔接镜头。  
![](img/14_115_0_2__.jpg) 
![](img/14_115_0_2__mod.jpg)  

### 14_119_3：  
雅彦：真是令人摸不着头脑的女人。连你的性别也未弄清楚，就说喜欢你...(14_119_0)万一被她知道了你真正的性别，情况可能就不同了....对吗？。这个镜头，雅彦的头是倾斜的, 想要表达什么？[疑问])  
![](./img/14_119_0_4__.jpg)  


### 14_121：  
给紫苑一个局部镜头、然后是紫苑背影。推迟人物的露面，增加神秘感（背景虚化）：  
![](./img/14_121_4__.jpg) 
![](./img/14_121_5__.jpg) 

### 14_123：  
为了求证浅葱把紫苑当作男生还是女生，紫苑约浅葱见面，并以女装示之。浅葱惊讶地认出了紫苑。  
镜头倾斜，表示人物内心不平静。   
![](./img/14_123_0__.jpg)


### 14_126：  
进入回忆：  
![](./img/14_126_0__cn.jpg)  
用不同年龄的紫苑来衔接镜头，退出回忆：  
![](./img/14_127_4_5__cn.jpg)


### 14_126_4:  
浅葱曾经被女生表白。  
画面里浅葱的阴影有偏移。  
![](img/14_126_4__.jpg)  

### 14_127_3:  
面部女生的表白，浅葱内心纠结“我也十分渴望得到爱...但是我是个同性恋者？！不是！我是个普通的女孩子！！绝对...绝对是！！”。  
镜头里黑白高对比度，角色位于画面1/3以下。  
![](img/14_127_3__.jpg)  

### 14_129：  
背景虚化：  
![](./img/14_129_0__.jpg)  


### 14_130_0:  
远处的背景虚化。  
![](img/14_130_0__.jpg)  

### 14_131_0，14_131_1  
或许可以认为用紫苑的头发和衣服过渡两个镜头（红框内）。  
![](img/14_131_0_1__mod.jpg)  


----------------------------------------------------------

## Vol14	CH097	page137		一天的情人  	Couple For A Night  		  

### 14_142_0:  
远处的路人加了阴影，但其中的主角未加阴影。  
![](img/14_142_0__.jpg)  

### 14_143_4:  
雅彦和紫苑进入公园时要求展示彼此是恋人的证据，比如接吻。紫苑上前解释：“你看！他不是紧张得满头大汗吗？他是个超级慢热的人，别说接吻，光是拖手就羞得满脸通红了。来这里的目的也是想藉这热闹气氛......”  
紫苑手的位置和构图都突出了雅彦。同时，对话框里的文字可以超越边框。  
![](img/14_143_4__.jpg)  

### 14_148_5：  
前景人物加了阴影。


### 14_155：  
僵尸的画法有层次感：  
![](./img/14_155_4__.jpg)

### 14_156_1:  
背景有轻微的畸变。  
![](img/14_156_1__.jpg)  

### 14_156：   
鬼屋内，紫苑被抢走，情急之下雅彦用手机联系以确定寻找方向。  
镜头倾斜，表示人物内心不平静。  
![](./img/14_156_1__.jpg)
![](./img/14_156_7__.jpg)


### 14_157_1:  
鬼屋内，雅彦听到了紫苑的手机铃声。  
画面里大量斜排线。  
![](img/14_157_1__.jpg)  

### 14_158：  
鬼屋内，雅彦来到紫苑被绑的屋子，这时有灯光：  
![](./img/14_158_0__.jpg)  
后来背景变暗，直至黑色。应该是灯熄灭了。三个镜头表示人在黑暗中。    
![](./img/14_158_3__.jpg) 
![](./img/14_158_4__.jpg) 
![](./img/14_158_5__.jpg)  
同时14_159整个页面背景是黑色。  


----------------------------------------------------------

## Vol14	CH098	page163		雅彦的剧本  	Masahiko's Scenario  		  

### 14_165：  
想念一个人的画法：  
![](./img/14_165_0__.jpg)


### 14_165：  
借助提及某人(紫苑和雅彦)来切换镜头：  
![](./img/14_165_7__cn.jpg) 
![](./img/14_166_0__cn.jpg)  


### 14_171：  
表示惊讶，用黑色背景：  
![](./img/14_171_3__.jpg)  

### 14_172：  
两人在影研社屋里谈话，之后直接切换至两人在走廊里私聊：  
![](./img/14_172_5__.jpg) 
![](./img/14_172_6__.jpg)  
两人动作成对比，一个紧张生气，一个悠闲自在：  
![](./img/14_173_0__.jpg)  

### 14_176_5，14_176_6  
左下角的镜头是该场景结束的镜头。  
![](img/14_176_5_6__.jpg) 

### 14_184_3:  
雅美正在苦恼，浅葱前来谈话。  
![](img/14_184_3__.jpg)  

### 14_187_0:  
浅葱：“我不会把紫苑让给你的！！不管是现实或是做戏也是！”  
雅彦：“......”  
镜头里的人物倾斜，或许表示其内心不平静。  
![](img/14_187_0__.jpg)  


### 14_187：  
之前预演（14_168），现在正式开始拍戏(雅美和浅葱谈话)，剧中的镜头用的是黑色边框背景：  
![](./img/14_187_3__.jpg)


----------------------------------------------------------

### Vol14	CH099	page189		改变剧情  	Change Of Course  		  

### 14_191_6:  
叶子漫画得奖后的惊喜。   
![](img/14_191_6__.jpg)  


### 14_203：  
雅彦和紫苑正在吵架，叶子来电。  
镜头里两人面部加阴影，可能是表示惊讶。同时，从构图来看，手机铃声把二人分开，暗示二人因吵架而有心里隔阂：  
![](./img/14_203_0_3__mod.jpg)  
同时，如蓝框所示，手机铃声破格，并和手机连成一个画面。中文版的翻译文字没有和手机连成一个画面：  
![](./img/14_203_0_3__cn.jpg)  


### 14_203，14_204，14_205：   
这些镜头让人感觉到紫苑和雅彦心里的隔阂：  
![](./img/14_203_6__.jpg) 
![](./img/14_204_4__.jpg) 
![](./img/14_204_7__.jpg) 
![](./img/14_205_0_2__cn.jpg) 


### 14_208_3，14_209_1，14_210_5，  
雅彦的话令叶子生气。  
![](img/14_208_3__.jpg) 
![](img/14_209_1__.jpg) 
![](img/14_210_5__.jpg) 

### 14_209：  
叶子很生气，右脸阴影用的是均匀网点。同时面部有发丝的阴影：  
![](./img/14_209_1__.jpg)  


### 14_212：  
之前雅彦和叶子在车站争吵，叶子离去。这里镜头过渡到叶子下车的地点。  
镜头的切换方式（左侧斜框）：  
![](./img/14_212_5__.jpg)  




----------------------------------------------------------

## Vol14	CH100	page215		浅葱的反击  	Asagi's Retaliation  		  

### 14_218_5:  
和叶子争吵后，雅彦晚饭也吃的无精嗒彩。  
镜头背景加了氤氲的气息。  
![](img/14_218_5__.jpg)  

### 14_221：  
在页面结尾的左下角插入一个镜头, 此处可能是表示空看到手机屏幕上的信息：  
![](./img/14_221_4_6__cn.jpg)  


### 14_222_2:  
镜头稍为倾斜，表示人物内心的不平静。  
![](img/14_222_2__.jpg)  

### 14_222_4:  
雅彦回忆在车站被闸机阻拦而没有追回叶子。  
回忆镜头：  
![](img/14_222_4__.jpg)  

### 14_224：  
从构图上看，熏的背后空间大，这暗示她背后有戏：  
![](./img/14_224_6__cn.jpg)  
紧接着，借助翻页，紫苑在熏背后出现：  
![](./img/14_225_0_1__cn.jpg)  


### 14_230：  
这个画面被分为了左右两个镜头：  
![](./img/14_230_1__.jpg)  

### 14_231：  
背景虚化，突出二人：  
![](./img/14_231_2__.jpg)  

### 14_233_2：  
雅彦、紫苑、浅葱拍戏的第二天。浅葱：“...无论你是男人也好，女人也好(14_233_0)...我已经决定了。我喜欢你！不管你是什么性别！我只依循自己的情感去爱...我已经下定决心了(14_233_1)！与你邂逅...是命运的安排(14_233_2)...”。下面这个镜头：  
![](./img/14_233_2__.jpg)  
![](./img/14_233_2__mod.jpg)  
我觉得这个镜头更突出紫苑，这可能是因为：  
1. 紫苑同时位于1/2和1/3处；  
2. 紫苑和背景占据的画面更大(大于1/2) （蓝色斜线）  
如果这个镜头更突出的是紫苑，那么我猜，在说上面那些台词时，浅葱眼睛看的是紫苑、而不是雅美。前一个镜头浅葱说对白时脸泛红晕支持这个猜测。所以，紫苑听到浅葱的这些话，脸上也泛红晕(14_233_3)。  
![](./img/14_233_3__.jpg)  
后续（14_251_3）浅葱也说吻的是紫苑，支持这个猜测。  
![](./img/14_251_3__cn.jpg)  


### 14_234：  
背景树叶用黑白斜线，增加对比度，衬托惊讶的表情：  
![](./img/14_234_5__.jpg)  


### 14_237_1：  
左侧头发处用横线来表现(雅彦/雅美)吃惊的效果：  
![](./img/14_237_1__.jpg)  


### 14_238_3:  
表示看到的内容。  
![](img/14_238_3__.jpg)  


----------------------------------------------------------

## Vol14	CH101	page243		奥多摩的分手  	Split At Okutama 		  

### 14_244：  
开篇是雅美在去奥多摩的电车上，然后用回忆的镜头开始倒叙(插叙?)。进入回忆的镜头。：  
![](./img/14_244_5__.jpg)  
接着，依然是切换场景的镜头（右上角的斜框）:  
![](./img/14_245_0__cn.jpg)  

### 14_245_1:  
雅彦问紫苑为什么要跑开。紫苑说：“因为我猜是雅彦的话，就该会溜走。”。雅彦内心受到震撼。 
镜头里的扭曲特效表示人物内心的震撼。     
![](img/14_245_1__.jpg)  

### 14_248, 14_249：  
（雅彦在莫多摩下车后看见叶子）两个连续镜头被分开放在了两页上。如果放在一页上，可能会更有动感：  
![](./img/14_248_5__14_249_0__.jpg)  
两张图的雅美和车站背景是一模一样的，是否是拷贝的？是否可以反映出一些漫画制作流程？


### 14_252, 14_253：  
紫苑离开咖啡厅，拿着雅彦的书包，抬头望天空，想到雅彦。然后（借助翻页）镜头切换至雅彦这边的剧情：  
![](./img/14_252_5__.jpg) 
![](./img/14_252_6__.jpg) 
![](./img/14_253_0__.jpg) 
![](./img/14_253_1__.jpg) 


### 14_254_5:  
背景特效可能表示紧张气氛。  
![](img/14_254_5__.jpg)  

### 14_255：  
倒影，很独特的镜头：  
![](./img/14_255_0__.jpg)  


### 14_256_0:  
雅彦被叶子发"好人卡"。  
画面背景衬托人物内心感受。  
![](img/14_256_0__.jpg)  

### 14_256：  
蓝色框里的两个镜头似乎能拼凑出一个人物。[疑问]不知道是否是作者有意为之？如果是，这是要表达什么？  
![](./img/14_256_0_2__cn_mod.jpg)


### 14_257：  
一个画面分隔为两个镜头：  
![](./img/14_257_2__.jpg)  

### 14_258_0:  
![](img/14_258_0__.jpg)  

### 14_258_1:  
面部阴影为(按面部上下而不是画面上下)的网点竖线。  
![](img/14_258_1__.jpg)  

### 14_258_3，14_258_4，14_258_5:  
倒影，很独特。树叶打破水面的平静，预示着两人关系破裂。  
![](img/14_258_3__.jpg) 
![](img/14_258_4__.jpg) 
![](img/14_258_5__.jpg) 

### 14_258：  
这个倒影，反过来的话，叶子的表情略微有点微笑。[疑问]为什么会这样？  
![](./img/14_258_4__mod.jpg)


### 14_259，14_260：  
人物面部黑色，表示人物心情不好：  
![](./img/14_259_1__.jpg) 
![](./img/14_260_5__.jpg)  

### 14_261_0：  
表示内心想起某人。回忆的人用白色头发，并镂空：  
![](./img/14_261_0__.jpg)  

### 14_262：  
地面风的蜗旋+落叶烘托低落的心情：  
![](./img/14_262_6__.jpg)  

### 14_265_3:  
熏开车接雅彦回家。雅彦在心里不断地喃喃自语：“对不起....叶子....”  
![](img/14_265_3__.jpg)  

### 14_266_2,14_266_3:  
叶子：“......”  
雅彦：“对不起....叶子....”  
连续两个镜头，对比分手后两人的情绪。  
![](img/14_266_2__.jpg) 
![](img/14_266_3__.jpg) 

### 14_266_6：  
从构图上讲，月亮在画面的1/3分割处.  
![](./img/14_266_6__.jpg) 


### 14_267_0:  
同一个画面分为三个镜头。一个新场景开始时的定场镜头。    
![](img/14_267_0__.jpg)  

----------------------------------------------------------

## Vol14	CH102	page269		家庭(最终话)  	Last Day - End  
### 14_269：  
第102话，最终话“家庭”。  
开篇的标题是这样的：  
![](./img/14_269_1__.jpg)  
开篇的画面放在了最后一页（连载于2000年10月）：  
![](./img/14_295_0__small.jpg)  


### 14_273:  
在高知下飞机后，雅彦：“紫苑, 我...有话要跟你说！”。  
镜头里屋檐遮住雅彦的面部，凸显其内心活动。  
![](./img/14_273_6__.jpg)  
然后镜头转到(14_274_0)妇产医院(持田产妇人科医院)。镜头再转回来时，雅彦和紫苑已经来到了海滩。


### 14_281，14_282，14_283，14_285，14_288，14_290,   
一些局部镜头也都画了背景:  
![](./img/14_281_6__.jpg) 
![](./img/14_282_0__.jpg) 
![](./img/14_283_0__.jpg) 
![](./img/14_285_4__.jpg) 
![](./img/14_288_1__.jpg) 
![](./img/14_290_3__.jpg)  


### 14_282_5:  
![](img/14_282_5__.jpg)  

### 14_283:  
从雅彦的眼睛里看到紫苑，镜头向后移动，逐渐显示出雅彦的整个面部：  
![](./img/14_283_2__.jpg) 
![](./img/14_283_3__.jpg) 
![](./img/14_283_4__.jpg) 


### 14_283:  
图片左下方，紫苑的脸上有三条线，应该是发丝（有点像猫胡须）：  
![](./img/14_283_5__.jpg)  


### 14_285:  
有海风，紫苑时不时在整理头发：  
![](./img/14_285_2__.jpg) 
![](./img/14_285_4__.jpg)  


### 14_284:  
镜头充满了音效: 雅彦表白紫苑，然后两人沉默，沉浸在海浪声里，任由海浪洗刷着岸边....  
![](./img/14_284.jpg)


### 14_290:  
雅彦紧张地等待紫苑说出她自己的性别，这里镜头倾斜和畸变可能是为了增加悬疑气氛：  
![](./img/14_290_3__.jpg) 
![](./img/14_290_5__.jpg)  


### 14_292_4,14_292_5,14_292_6,14_292_7:  
该场景结束，镜头慢慢上摇退出的效果：  
![](./img/14_292_4_7__.jpg)  


### 14_283，14_284, 14_285:  
在海滩，镜头如下：  
- 14_283_0(C.U.), 雅彦：“我...幸好能够饰演你这角色...”  
- 14_283_1(E.C.U.), 雅彦："我在你立场，看清楚了自己...我讨厌如此没出息的自己...所以..."  
- 14_283_2(Macro Shot), 雅彦："已经...够了...！不过..."  
- 14_283_3(Macro Shot), 雅彦："我连自己的心意也清楚地看到了。。。我清除知道了一件事"  
- 14_283_4(C.U.), 14_283_5(C.U.).  雅彦、紫苑互相凝视。"  
- 14_284_0(M.S.), 雅彦："紫苑...我...我喜欢你!"(虽然这个镜头拉远了，但是由于整个画面大，所以依然表示角色情感强烈)  
- 14_284_1(E.L.S.), 海面...  
- 14_284_2(L.S.?)，14_284_3(L.S.?)，海浪冲刷着两人的脚...  
- 14_285_0(L.S./Two Shot), 紫苑：真心...?  
- 14_285_1(C.U./O.T.S.), 14_285_2(C.U./O.T.S.), 雅彦坚定的目光(给出了肯定的回答),两人对视。  
- 可以看出镜头从C.U.拉近至Macro Shot，应该是表现雅彦的情感逐渐增强；然后镜头拉远至C.U.、再远至M.S.、再远至E.L.S.(海面)；然后镜头又拉近至L.S.、再近至C.U.。  
- 镜头从C.U.拉近至Macro Shot，应该是表现雅彦的情感逐渐增强；在Macro Shot处雅彦情感应该最强烈。疑问：为什么在Macro Shot处不让雅彦表白？而是在之后镜头拉远之后才说出"我喜欢你"。  

<a name="14_285_0"></a>  
### 14_285_0:  {#14_285_0}  
(雅彦表白：“紫苑...我...我喜欢你!”)紫苑：“真心...?”：  
        ![](img/14_285_300.jpg)  
FC的这个镜头和《Rumble Fish (1983)》里一个镜头[2, p6]很相似：  
        ![](img/RumbleFish-1983.jpg)  
对此镜头的解释[2, p7]包括：背景的海面高得像一堵墙，寓意主人公的未来没有机会。  

可以把该镜头里海平面高度降低，对比一下：  
        ![](img/14_285_mod300.jpg)  
个人感觉，海平面降低之后，画面明亮了，且气氛轻快了一些。所以，高海面可能的确有消极含义，这和后续情节吻合：后续紫苑并没有完全接受雅彦作为男人的表白--仅接受雅彦作为朋友的表白--紫苑:"我也是...很喜欢雅彦你...不过，不是对男人的那种，应该是朋友那种"。  

注：从下面这张照片可以看出，海平面降低之后，不影响前景人物显示为黑色轮廓：  
        ![](img/img_0302_300.jpg)  
(图片源自[慧田哲学-年度最佳笑话:专家与骗子](https://mp.weixin.qq.com/s/n_un4Qi8buToMObYmajYKA))


### 海滩表白的某些情节符合"角色情感变化带有较长的镜头序列片段":    
![](shot_emotion/fc_shot_emotion_vol14_ch102_confession0.jpg) 
![](shot_emotion/fc_shot_emotion_vol14_ch102_confession1.jpg) 
![](shot_emotion/fc_shot_emotion_vol14_ch102_confession2.jpg)  
本话更多镜头统计和大概变化趋势：[fc_shot_emotion_vol14_ch102.pdf](shot_emotion/fc_shot_emotion_vol14_ch102.pdf)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

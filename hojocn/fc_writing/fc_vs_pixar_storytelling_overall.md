
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- 说明：  
    - 《Pixar Storytelling》(by Dean Movshovitz)一书里总结出一些Pixar的故事讲述规则。本文将这些规则对照FC的剧情，试图找出FC里是否有某些剧情能够符合这些规则。针对FC的每一话故事，可以写单独的文章去分析其符合哪些故事讲述规则(不限于Pixar故事讲述规则)，但这暂时不是本文要做的事。 
    - 虽然北条司创作FC时应该未参照Pixar故事讲述规则，但既然FC很感人，我猜FC的剧情可能和这些故事讲述规则有某些共同点。虽然无法得知北条司的编剧技巧，但或许能借助这些故事讲述规则来推测FC剧情的特点。 
    - 选择《Pixar Storytelling》的原因是： 1. 在动画电影里，Pixar带给我的感动最多，所以我觉得Pixar的故事讲述规则可能会很有代表性； 2.发现有这本书。     
    - 《Pixar Storytelling》只关注了Pixar电影，忽略了一些其他的故事讲述规则。      
    - 和Pixar电影不同，FC是长篇连载。FC的每一话或几话讲述一个故事；主角们的情感发展作为长故事线贯穿在FC的各个故事里；而每个小故事又有自身的短故事线。在《Pixar Storytelling》里总结的诸多规则中，有些符合FC里的长故事线、有些符合FC里的短故事线。本文大致把FC的剧情对照到"Pixar的故事讲述规则"。  
    - 本文按照《Pixar Storytelling》的内容顺序，列出其总结出的规则和说明（英文及译文）；针对每一项，尽量辅以FC的剧情。  
    - 以(DIY)前缀开始的段落，是书里其所在章里"Do It Yourself"的内容。
    - 以(Summary)前缀开始的段落，是书里其所在章里"Summary"的内容。
    - 对于《Pixar Storytelling》一书里的内容，我可能有理解错误之处。欢迎指出错误，谢谢。      
    - 类似03_143这样的字符串表示"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似03_143_7这样的字符串表示"卷-页-分格(panel)/分镜"编号。xx_yyy_z表示第xx卷、第yyy页、第z分格(panel)/分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  

-----------------------

-----------------------

**"Pixar的故事讲述规则"对照FC的剧情**  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96855))    

_Pixar Storytelling, Rules for Effective Storytelling Based on Pixar’s Greatest Films_, by Dean Movshovitz  

.  
.  

-----------------------

**CHAPTER 1, CHOOSING AN IDEA(第一章 选一个想法)**  

**"Mother Lodes"--Choosing Ideas That Have a LOT of Potential("母矿"--选择有很多潜力的想法)**  

Choosing an idea for your film is a bit like selecting where to set up a gold mine. Some places will offer you a few nuggets, and in others you’ll hit the mother lode. Both starting points can be the basis for a great story—those nuggets are still gold—but “mother lodes,” those relatable ideas that offer many levels of clear drama and narrative options, tend to be easier to develop and more accessible to audiences.  
为电影选择一个主意有点像选择在哪里开设金矿。有些地方会为你提供一些金块，而在其他地方，你会找到母矿。 以这两个为起点都可以创作出很棒的故事（毕竟金块也是金子啊），但“母矿”--提供多层次的、清晰的、剧情和叙事选择的那些相关观念--往往更容易开发剧本，观众也更容易接受。  

Clearly, Pixar goes for the mother lodes. Part of the studio’s success comes from its ability to recognize and develop strong, engaging ideas, which usually come with powerful, built-in emotional weight. These ideas evoke a rich exotic world (whether monsters, toys, or superheroes) that offers many possibilities for imaginative set pieces, visual richness, and original scenes. More importantly, these ideas contain tremendous physical and emotional stakes, which makes them immediately enticing and accessible.  
显然，Pixar很喜欢母矿。 Pixar成功的部分原因在于其有能力识别和开发出强大且引人入胜的想法，而这些想法常常有深厚的内心情感。这些想法唤起了一个丰富的奇异的世界（无论是怪物、玩具还是超级英雄），为想象中的场景、丰富视觉感受和独特的场景提供了许多可能性。 更重要的是，这些想法包含着巨大的生理和情感的投入，这使它们很具吸引力并易于被观众接受。  

(DIY)What is **the core idea** of your story?    
- 这里的"核心想法(the core idea)"和第十章的"主题（theme）"不同。我猜, FC的核心想法是LGBT（FC的主题是亲情）。通过一对性别反转的夫妇的家庭来展现。  

(DIY)Does it offer **many possibilities** for dramatic moments?   
- LGBT群体和常人有很多不同，应该会有很多新奇的故事。  

**Leaving the Comfort Zone: More "Discomfort" = More Story(离开舒适区：越多"不适" = 越多故事 )**  

To truly upset a character, you must create a weakness or fear that you can tap into. Therefore, Pixar creates a pre-existing problem in each protagonist’s world.  
要一个角色真正地苦恼，你必须要让他有个可利用的弱点或害怕。 因此，Pixar的每个主角的世界里都有一个预先存在的缺陷。  

- 在FC里，作者北条司也经常加入回忆的情节来讲述以前的事。其中不乏悲伤或不愉快的往事：  

- 紫苑：  
    - 初恋光浩变成了浩美。  
    - 当她把初恋男生变女生的故事告诉同学们后，受到同学们的嘲笑。
    - 因父母特别、初恋的男生变女生，而性别意识模糊。
   
- 雅彦：  
    - 母亲早已去世。父亲最近去世。如果雅彦不留恋家庭的话，这个创伤会小一些；但雅彦反倒是一个非常渴望家庭的人。  
    - 遗产几乎为0。需要自己谋生。  
    - 初到若苗家，被若苗夫妇的秘密所震惊，无法忍受而离家出走。
    - 喜欢紫苑，却不知她的性别，所以不敢表白，进而压抑自己的感情。
    - 母亲曾送的杯子被摔碎，很伤心。
    - 虽不情愿，但被影研社、被辰巳要求异装。

- 若苗夫妇：
    - 性别反转。所以他们与常人之间肯定会有很多摩擦，由此会带来各种不适。比如，若苗夫妇出席雅彦的大学典礼时引起的骚乱。
    - 比"不适"更严重的事情。例如，若苗空和自己的父亲抗争、和原生家庭决裂；若苗紫和自己的原生家庭决裂。

- 叶子：
    - 和母亲关系不和。
    - 和雅彦的关系停滞，这遭同学嘲笑。
    

**A Character and World That Vie for Adventure—The Existing Flaw(角色和所处世界相互争夺冒险--现有缺陷)**

These emotional cracks are what makes us human, and they are what will make your characters compelling.  
正是这些情感上的裂痕使我们成为了一般人，并且这些情感裂痕会使你的角色很有吸引力。  

Ideally, even before the gears of the plot start to turn, there should be a problem in your protagonist’s life or world.   
理想情况下，即使故事情节还没开始，主角的生活里或世界里也应有一个缺陷。  

Once you have found **the existing flaw** in your core idea, craft a story that pushes it to the extreme. .... Whatever the existing flaw, it must be clearly related to the plot you have crafted for your protagonist. The more these two work in tandem, the higher the emotional stakes will be and the more invested your audience will be.  
一旦发现核心思想中存在“现有缺陷”，就精心设计一个故事将“现有缺陷”推向极端。 ....无论存在什么缺陷，它都必须与主角的情节相关。 这两个协同得越多，情感投入就越高，观众也就越投入。  


- 雅彦  
    - 现有缺陷：缺乏家庭的温暖；优柔寡断。  
    - 推向极端：  
        - 雅彦失去家庭(母亲早已去世，父亲最近去世), 举目无亲。  
        - 雅彦梦想有一个(完整的)家庭。（如果雅彦不留恋家庭的话，这个创伤会小一些；但雅彦反倒是一个非常渴望家庭的人）  
        - 父亲留给雅彦的遗产几乎为零。需要自己谋生，正在找工作。  
        - 雅彦即将上大学（可能辍学）。    
        - 不相信好事(紫提出的收养和供自己念大学)会发生在自己身上，这说明雅彦心理已经有了变化(自卑？)。  

- 紫苑  
    - 现有缺陷：受家庭环境影响，性取向不明确。  
    - 推向极端：  
        - 喜欢的男生变成了女生（这意味着，紫苑**永远**不能和他发展爱情(异性恋)了）。  
        - 该男生是紫苑的初恋。  
        - 发生在圣诞节（与温馨的节日气氛形成反差）。  

- 若苗空  
    - 现有缺陷：渴望做男子汉，但困难重重。  
    - 推向极端：  
        - 父亲阻挠。校服风波。  
        - 和原生家庭断绝来往。  

- 若苗紫
    - 现有缺陷：渴望做女人，但困难重重。
    - 推向极端：
        - 和原生家庭断绝来往。

- 真朱熏
    - 现有缺陷：原生家庭问题。
    - 推向极端：
        - 父亲辰巳不是熏的亲生父亲。
        - 母亲早纪眼里只有钱和男人。

- 浅冈叶子
    - 现有缺陷：
        - 渴望实现梦想（做漫画家），但被母亲要求继承旅馆。
        - 在感情上渴望得到雅彦的爱，但一直得不到。
    - 推向极端：
        - 和母亲争吵。
        - 母亲年纪大了，需要为母亲考虑。
        - 雅彦一直不主动。

- 新潟和子：
    - 现有缺陷：性取向和一般人不同
    - 推向极端：  
        - 参加奥运会代表选拔赛，却在赛场上结束了柔道生涯结束
        - 渐渐，普通生活令ta愈来愈痛苦。ta不能在忍受以男人的身份生活下去了，快抑压不住，要爆发了(08_108_1)。


**Economy: How Every Moment in Ratatouille Stems from Its Core Idea（不浪费：《料理鼠王》如何时时刻刻从核心思想中汲取灵感）**  

This is economy. Everything in your screenplay should relate to your core idea, to your main conflict.    
这就是不浪费。 剧本中的所有内容都应能关系到核心思想和主要冲突。  

- FC里的配角：  
    - 江岛卓也：
        - 在和女性交往这方面，江岛和雅彦形成对比。
        - 紫苑加入影研社后，江岛讨厌他、多次为难他。第一次，影研社新人提交作品，向借机把紫苑踢出影研社。结果，紫苑的短篇《足》大受好评。第二次，影研社迎新会上，江岛和紫苑拼酒。借此展现紫苑不畏霸凌的性格。
    
    - 影研社的导演：
        - (为筹集影研社的资金)多次利用雅彦。这与雅彦的善良形成对比。    
        
    - 助手们：
        - 助手们在若苗家人接触一段时间后，都变成了女人。这与雅彦形成对比。


(Summary)Every moment in Ratatouille evolved out of its core idea. The same should be true for any story you write. Once you have a good idea, such as one of those discussed in the first part of this chapter, treat it as a seed that you must sprout into story. Let it grow, step by step, hewing close to its core. What isn’t part of this essence, this seed, probably shouldn’t be part of your story and should be pruned mercilessly.  
《料理鼠王》时刻都从其核心思想演变而来。 你写的任何故事都应如此。 一旦有一个好主意（例如本章第一部分中讨论的主意），就将其视为种子，必须发芽变成故事。 让它一步一步地成长、紧贴其核心。 与核心无关的内容，很可能不该成为故事的一部分，应该无情地剪掉。  

(Summary)Uncomfortable characters are so appealing because we all like feeling comfortable. And once our cushy existence is taken from us, we need to reconcile these new circumstances with who we are and what we have lost. This desire creates scenes and conflicts for your characters and story. Pixar’s characters go to great lengths to retrieve what they have lost. Watching them react to their new circumstances, fighting, and growing, is what makes Pixar’s films so moving and enjoyable. ...Pixar excels in putting characters in the worst places possible for them.  
让人不舒服的角色会很引人注目，因为我们都喜欢感到舒服。 一旦我们的生活有麻烦了，我们就需要将这些新情况与我们自身以及失去的东西相调和。 这种渴望为你的角色和故事创造了场景和冲突。Pixar的角色都竭尽全力找回他们所失去的东西。 角色们对新环境的反映、战斗和成长，使Pixar的电影非常感人、令人愉悦...Pixar擅长将角色们放在对他们最糟糕的地方。  


(DIY)Do they(the flaw, the plot) mutually enhance and enrich each other?  

(DIY)Are you constantly exploring and expanding the seed of your story as it progresses?   

- FC里逐渐展开每个人曾经(痛苦的)经历。  

.  
.  

-----------------------

**CHAPTER 2, CREATING COMPELLING CHARACTERS(第2章，创建吸引人的角色)**  

**Interesting Characters Care Deeply（有趣的角色深切关心）**  

Once you have a strong idea for a movie, your next step should be getting to know your lead characters.  
一旦对电影有了一个好主意，下一步应该是了解你的主角。  

- FC里的主角：雅彦，紫苑，熏(?)。

- 和Pixar电影不同，FC作为长篇漫画，每一话的故事可以相互独立。虽然有些人物在FC里是配角，但这些配角会在某些故事里成为主角。例如，叶子和她母亲都是配角，但在CH088至CH091里，二人是主角。  


Strong, unique characters are the secret to a movie’s success. No matter what your story is, the events that construct it are happening to someone. And that someone better be interesting and, more importantly, must care about what is happening to and around them.  
强大而独特的角色是电影成功的秘诀。 无论你的故事是什么，构成故事的事件总会发生在某人身上。 最好让某个角色变得有趣，更重要的是，他们必须关心身边的事情。  

Pixar has created many unforgettable characters:....Why do these characters resonate so strongly? Well, they are all one of a kind. ... But for them to compel an audience, you must also endow them with a deep passion for something...anything. We care because they care....  
Pixar创造了许多令人难忘的角色：....为什么这些角色会产生如此强烈的共鸣？ 好吧，它们都是一种。 但是。为了使他们吸引观众，你还必须让他们对某些事物(可以是任何事物)有强烈的热情。 我们会关心那些事物，因为角色关心它们。  


- 雅彦
    - 认为重要的事物：
        - 坚持做男生。
        - 善良。温泉之旅时，面对紫苑劝说，雅彦不想空和紫的愿望落空，所以坚持选择正确行车路线。
        - 善良。叶子最初为了见到空，而谎称喜欢雅彦。后来，雅彦得知真相后，依然愿意帮助叶子实现她的愿望。
        - 熏被不良玩伴勒索。空不明确表示要雅彦去帮助熏，但雅彦最后决定要那么做。
        - 紫苑高中毕业旅行，碰到有不良企图的男生。雅彦坚持要灌醉他们，不让他们的阴谋得逞。
    - (DIY)对爱情、友谊、死亡、自由、幸福的看法，以及原因：
        - 

- 紫苑
    - 认为重要的事物：
        - 面对雅彦的阻挠，坚持加入影研社。
        - 带病坚持参加入学考试。
    - (DIY)对爱情、友谊、死亡、自由、幸福的看法，以及原因：
        - 

- 熏
    - 认为重要的事物：
        - 坚持做音乐
    - (DIY)对爱情、友谊、死亡、自由、幸福的看法，以及原因：
        - 

- 空
    - 认为重要的事物：
        - 工作。按时交稿。 
        - 坚持做男人，和原生家庭决裂。
        - 为紫的梦想(有孩子)，暂时放弃自己的梦想(漫画连载)  
        - 紫苑生病时，关心紫苑。
    - (DIY)对爱情、友谊、死亡、自由、幸福的看法，以及原因：
        - 爱情和婚姻应该是自由的。为此极力反对父亲安排顺子的婚姻，也极力撮合顺子和宪司。

- 紫
    - 认为重要的事物：
        - 坚持做女人，和原生家庭断绝往来。  
        - 紫苑生病时，关心紫苑。  
        - 熏很晚回来时，关心熏。  
        - 雅彦、紫苑、熏在外打架后，关心他们。  
    - (DIY)对爱情、友谊、死亡、自由、幸福的看法，以及原因：
        - 


.... Good characters care. Great characters care because they have strong opinions.  
好的角色会关心(身边的事物)。 伟大的角色会关心(身边的事物)，因为他们有很强烈的观念。  

**The Best Opinions Come from (Painful) Experience(最好的意见来自（痛苦的）经历 )**

Of course, opinions don’t just manifest out of the ether. Think of your own opinions. They are most likely conclusions you have arrived at through your upbringing and years of education and experience. The same is true for your characters. You don’t need to flash back to fifth grade to explain every attitude your character has, but strong opinions are often shaped by a character’s past. It’s even better if that past is rife with conflict and tension.  
当然，观念不能无中生有。 想想你自己的观点。 这些都是你在成长、多年的教育和经验中得出的结论。 你的角色也是如此。 你无需回到角色五年级去解释角色的每种态度，但是强烈的观念通常是由角色的过去经历所塑造的。 如果过去经历充满冲突和紧张，就更好了。  


总结：  
痛苦的经历(Painful Experience)      
--> 鲜明的观念(人生观和价值观)(Best Opinions)   
--> 强烈的热情 (Strong Caring/passion)   
--> 鲜活的角色 (Good Character)   
--> 好的情节 (good plots)   
--> 吸引读者 (to compel reader)   


- 《北条司拾遗集.画业35周年纪念》里(120页)有访谈：  
    北条：...实际上，在创作登场人物的过程中，会仔细地考虑角色的家族构成或者家庭环境等。因为这一部分不明确的话，角色是不会有魅力的。对这一点敷衍了事从而创作出的角色会缺乏深度。让人感觉不到人情味... 
    
- 空和原生家庭的故事、紫和原生家庭的故事，让角色更真实。  

Sometimes it takes only one line to give us all the background we need. ...  
有时只需要一句话就可以为我们提供所需的所有背景。 ...  

- 紫和原生家庭断绝往来，就是一笔带过。  

Pixar’s flashbacks work so well because they are usually very economical, entertaining and well-placed. They usually occur at the beginning of the film, making them more of a prologue than a flashback,or they arrive organically as the story unfolds, as part of a character’s action or reaction。 Longer flashbacks succeed because Pixar treats them as independent short films, rather than as supplementary material for the main plot.   
Pixar的回顾做得很好，因为它们通常短小精悍、富有娱乐且对剧情有用。 它们通常发生在电影的开头，比起回顾这更像是一个序幕；也会随着故事的展开而自然涉及到这些信息，这就会作为角色的动作或反应的一部分。更长的回顾也有，而且很成功，因为Pixar将它们作成了独立的短片，而不是作为主情节的补充。  

- FC里也有大篇幅的回忆。空和紫的19岁生日；空和父亲之间的"校服风波"；紫苑的初恋...  

(Summary)Creating compelling characters is one of the biggest challenges you will face as a writer. You must use all your originality and insight to create a distinct and memorable individual whose story, appearance, and world are unique. Most importantly, your characters should care about ideas, values, and people, ideally out of a specific, opinionated point of view. When your characters’ opinions are rooted in their experiences, especially painful experiences, it gives them depth and makes them more realistic. These three tools— passion, opinions, and experience—make the events in your story more meaningful and dramatic for your characters, and by proxy, for your audience.  
创建引人入胜的角色是作家将面临的最大挑战之一。 你必须利用自己的独创性和洞察力来创造一个独特而令人难忘的个体，其故事、外形和所处的世界都很独特。最重要的是，你的角色应该关心一些想法、价值观和其他人，理想情况下，角色应该有自己特定的观点。当角色的观念源于他们的经历(尤其是痛苦的经历)时，角色会变得有深度、更真实。 三个工具--热情、观念和经历--使故事中的事件对于角色和代理（对于代理人）更有意义和戏剧性。  

.  
.  

-----------------------

**CHAPTER 3, CREATING EMPATHY(第三章 建立同理心)**  

**The Three Levels of Liking(喜欢的三个层次)**  

1st level: (good looking). you can “like” characters—for their obvious, external traits.  
层次1：（美貌）。你可以因为角色明显的外部特征而“喜欢”他们。  

2nd level: (positive/attractive traits). they’re just entertaining, or knowledgeable and passionate about something. Maybe they’re funny or talented.  
层次2：（积极/有吸引力的特征）。角色很有趣、或很博学、或对某事物有热情。也许他们很搞笑或很有才华。  

3rd level: (empathy). it can include less desirable aspects of a character. We forgive our friends their trespasses because we know them better and understand where they’re coming from. allowing the audience to view the character as their proxy. The character becomes a surrogate for the audience’s own hopes and fears.  The third layer requires more details and originality, as well as more patience from your audience, but is more rewarding and creates a stronger bond with viewers.  
层次3：（同理心）。它可能包含角色不太好的一面。我们原谅朋友的错误，因为我们比较了解他们，并且了解他们为什么会那么做。允许观众将角色视为他们的代理。角色成为观众自己的希望和恐惧的代理人。层次3需要更多的细节和独创性，以及观众更多的耐心，但层次3回报高，能紧紧地抓住观众。  


- 雅彦
    - (DIY)习惯：
    - (DIY)爱好：音乐
    - (DIY)例程：
    
- 紫苑
    - (DIY)习惯：
    - (DIY)爱好：
    - (DIY)例程：

- 熏
    - (DIY)习惯：
    - (DIY)爱好：音乐
    - (DIY)例程：
    
- 空
    - (DIY)习惯：
    - (DIY)爱好：
    - (DIY)例程：

- 紫
    - (DIY)习惯：
    - (DIY)爱好：
    - (DIY)例程：

- 叶子
    - (DIY)习惯：
    - (DIY)爱好：绘画
    - (DIY)例程：



Use it wisely; the further you take us into the heart of someone completely different, the more rewarding and transformative our journey will be.  
明智地使用它；你越是将我们带入完全不同的人的心中，我们的旅程回报越高、更具变革性。  

There’s another method of creating empathy with a character, and arguably it is the most crucial: **Place them in trouble**. But remember that no one enjoys merely watching someone else suffer—simply tormenting your characters will cause more **pity and aversion** than empathy. Instead, characters should be placed in harm’s way and then forced to bravely chart their course out of it.  
还有一种方法能让人对角色产生同理心，可以说这是最关键的：“给角色制造麻烦”。但是请记住，没有人会喜欢看着别人受折磨--仅仅折磨你的角色，会引起更多的“怜悯和厌恶”而不是同理心。相反，角色应该被置于危险之中，然后迫使角色勇敢地走出困境。  

**Desire and Motive（欲望与动机）**

How can you express someone’s inner world in a manner to which an audience can relate? One way is to focus on a character’s desires....When we see a character truly desire something, we almost immediately take their side and hope they obtain it. Why? Because we hope to have our goals met just like the character does.  
你如何表达某人的内心世界，并让观众感到置身其中？ 一种方法是专注于角色的欲望...当我们看到角色真正渴望某件事时，我们几乎会感同身受，希望角色得到它。 为什么？ 因为我们希望自己能实现自己的目标，就像角色能实现他们自己的目标一样。  

It’s not enough to know only what a character wants. As storytellers, we must also know (and convey) **why** they want it. .... This is especially important when designing antagonistic characters.   
仅知道角色想要什么是不够的。 作为讲故事的人，我们还必须知道角色为什么想要它（并传达给观众） ....这在设计对反派角色时尤其重要。  


- 比如，妈妈送给雅彦的杯子被摔碎。FC借杯子摔碎的一刹那，回忆了母亲送雅彦这个杯子的原因和经过。因为这是妈妈送给雅彦唯一的(圣诞)礼物(02_005_4)，因此雅彦很伤心。

- 雅彦
    - (DIY)目标(goal)或行动(campaign)/：希望有完整的家庭    
        - (DIY)这么做的原因强烈吗？  强烈
        - (DIY)为达目标，如何努力？  
        - (DIY)非常努力或半途而废？  
        - (DIY)挑战来自于外部、实际，或是来自于内心、情感？  来源于内心、情感
    - (DIY)目标(goal)或行动(campaign)/：希望维持生计
        - (DIY)这么做的原因强烈吗？ 强烈，因为父亲去世后留下的钱不多 
        - (DIY)为达目标，如何努力？ 看招聘启示，找工作 
        - (DIY)非常努力或半途而废？ 觉得招聘启示里的工作都很无聊 
        - (DIY)挑战来自于外部、实际，或是来自于内心、情感？  来源于外部      
    - (DIY)目标(goal)或行动(campaign)/：保留妈妈送自己的杯子  
        - (DIY)这么做的原因强烈吗？  强烈。因为这是雅彦唯一和母亲有关的事物了
        - (DIY)为达目标，如何努力？  把杯柄粘上。杯子摔碎后，试图拼好碎片。
        - (DIY)非常努力或半途而废？  努力。
        - (DIY)挑战来自于外部、实际，或是来自于内心、情感？来源于内心、情感
        
- 真朱辰巳
    - (DIY)目标(goal)或行动(campaign)/：渴望家庭  
        - (DIY)这么做的原因强烈吗？  强烈。原因是他缺少家庭，他和早纪不是正式夫妻，而且早纪不在他身边。  
        - (DIY)为达目标，如何努力？  追求若苗紫
        - (DIY)非常努力或半途而废？  努力
        - (DIY)挑战来自于外部、实际，或是来自于内心、情感？来源于内心、情感
        
- 真朱熏
    - (DIY)目标(goal)或行动(campaign)/：成为男子汉 
        - (DIY)这么做的原因强烈吗？  强烈。原因是：不要成为早纪那样的人 
        - (DIY)为达目标，如何努力？  离开早纪。异装
        - (DIY)非常努力或半途而废？  
        - (DIY)挑战来自于外部、实际，或是来自于内心、情感？来源于内心、情感
        
- 影研社导演
    - (DIY)目标(goal)或行动(campaign)/：为影研社吸纳新人、筹集资金 
        - (DIY)这么做的原因强烈吗？  强烈。 
        - (DIY)为达目标，如何努力？  要求雅彦女装。自己剪辑影片。说服雅彦女装参加学校圣诞聚会
        - (DIY)非常努力或半途而废？  
        - (DIY)挑战来自于外部、实际，或是来自于内心、情感？来源于外部 


**“When Remedies Are Past, the Griefs Are Ended”(当补救措施过去时，悲伤就结束了)**

... a character can give up and accept their fate only after every imaginable course of action has been tried.  
角色只有在尝试了所有能想到的措施之后，才能放弃并接受命运。  

Movie characters should have flaws, but among those flaws you’ll almost never find defeatism. The characters in Pixar’s films never give up. They will look death in the eye; they will conquer their deepest fears; they will change and adapt, if doing so offers a chance for them to get what they want. This is part of why Pixar’s films are so satisfying. It’s why you must design a strong reason for your character’s desire. This motivation must be powerful enough to propel them through many trials.  
电影角色应该有缺陷，但是在这些缺陷里，你几乎永远找不到失败主义。Pixar电影中的角色永不放弃。他们直面死亡；他们克服自己内心深处的恐惧；他们会改变和适应，如果他们这样做能有机会获得自己想要之物的话。这就是为什么Pixar的电影如此令人满意的部分原因。这就是为什么你必须为角色的愿望设计一个强有力的理由。这种动机必须足够强大，以推动他们经历许多考验。  

Again, this relates to our identification with the character. We wish to live life this way, implacably following our desires. But we usually don’t. Movies show us the risks and rewards, the trials and joys, of those who do.  
同样，这把角色和我们自己联系起来了。我们希望过这种生活--坚定地遵从自己的愿望--但我们通常过不了这样的生活。电影向我们展示了这样做的人(所经历的)的风险和回报、磨难和喜悦。  

This isn’t to say that movie characters can’t give up. As a matter of fact, they practically must give up—for a moment.  
这并不是说电影角色不能放弃。实际上，他们实际上必须暂时地放弃。  

A character that gives up easily frustrates an audience. A character that suffers a true, earned failure will resonate strongly with anyone invested in their quest.  
轻易就放弃的角色会使观众感到沮丧。一个遭受了真正失败的角色，他会令观众产生强烈的共鸣。  

There are many ways for a protagonist to demonstrate their determination—a character’s journey isn’t measured in miles, but in painful experiences and overcoming obstacles.  
主角可以通过多种方式来展现自己的决心--角色的旅程不是以距离来衡量的，而是以经历的痛苦和所克服障碍来衡量的。  

- 比如，妈妈送给雅彦的杯子先是杯柄掉了，而且杯子被空刷洗笔而弄得很脏。但雅彦没有放弃，洗净并粘好杯柄后打算继续用。接着，杯子被摔碎。雅彦仍试图把碎片拼好，但失败了。生气的他，愤怒地想要砸碎杯子，但却下不了手。后来，紫开导他，若苗家三人每人又送雅彦一个新杯子。雅彦很开心，说明他已经释怀了（放弃了那个破碎的杯子）。

- 例如，紫苑凌晨跟随江岛的车去影研社拍戏。雅彦为保护紫苑，便骑单车猛追，但最后精疲力尽，只好放弃（所幸后来发现了新线索）。

(Summary)For the audience to transcend from liking your characters to empathizing with them, you must create rich, specific characters. Through a process of discovery, you must dole out the idiosyncrasies of your fictional creations while highlighting what is relatable, human, and universal about them. One of the most universal things is desire. Giving your character a clear goal and a strong motivation behind it will help people empathize with your character, even when their actions are questionable. Lastly, while pursuing their goals, characters should be bold and determined, bravely battling their self-doubt, and never giving up until they have done everything imaginable to achieve their goal.  
为了使观众从喜欢角色变为同情角色，你必须创建丰富、具体的角色。 通过不断地挖掘，你必须消除虚构作品的特质，同时强调其相关性、人性和普遍性。 欲望是最普遍的事物之一。 给角色一个清晰的目标和强烈的动机，将有助于人们理解角色，即便角色的行为本身值得怀疑。 最后，在追求目标时，角色应该勇敢而果断，勇于和他们的自我怀疑作斗争，并且在尽一切可能实现目标之前绝不放弃。  


(DIY)How do you feel about the characters in your story?  

(DIY)Are they unique or generic?  

(DIY)Do they have habits, hobbies, or routines that make them feel real and specific?  

(DIY)Do they have a clear goal or a campaign that an audience can get behind?  

(DIY)Is the reason for pursuing this goal strong and fleshed out?  

(DIY)In pursuit of this goal, how hard and far do characters push themselves?  

(DIY)Do they go to great lengths or merely try half-heartedly?  

(DIY)Are their challenges external and physical, or interpersonal and emotional?    

.  
.  

-----------------------

**CHAPTER 4，DRAMA AND CONFLICT（第四章 戏剧与冲突）**

**More Than Life and Death（不仅仅是生与死）**

The more unique, original, and layered these obstacles are, the more your story will stand out and satisfy your audience.  
这些障碍越独特、越新颖、越有层次，你的故事就越能脱颖而出，并使观众满意。  
.  
Keep in mind that life-threatening situations are just a starting point. Death is an obstacle to all goals, but merely living is a dull goal. Designing specific, personal goals for your protagonists, based on their opinions and desires, can lead you toward more interesting conflicts that you should develop and explore.  
请记住，危及生命的情形只是一个起点。 死亡对所有目标而言都是障碍，但生活平淡就是一个很无趣的目标。 根据主角的观念和愿望，为他们设计特定的个人目标，可以使你找到更有趣的冲突。  


- 雅彦的目标： 紫苑的性别究竟是男是女？
    
- 紫苑的目标: 今后自己究竟做男生还是女生？
    
- 熏的目标： 成为男子汉

- 叶子的目标： 成为漫画家;得到爱情.

- 江岛的目标： 成为演员


Another way to define conflict is as a situation in which two opposite forces struggle with each other. This means that every conflict has a built-in question: Who will win? While this view pertains to the physical conflicts, it is particularly useful when discussing internal conflicts.  
定义冲突的另一种方式是，设定一种情形，让两种相对的力量相互斗争。这意味着每个冲突都有一个随之而来问题：(两方力量里)哪一方会赢？尽管此观点与肢体冲突有关，但在讨论内心冲突时特别有用。  

opinions are fuel for conflict. Once a character cares deeply about something, you can create powerful, emotional conflict surrounding that emotion. Conflicts rooted in a character’s opinions and emotions are a little harder to convey, both because they exist in a character’s mind, and because they are more specific and less universal than life or death. They require deeper understanding of the character and world you’ve created. You must explain the emotional constitution that allows your character to have these opinions. (This explanation is more formally known as “exposition.”) Furthermore, you must highlight the submerged conflict question, the two forces at play.  
观念是冲突的动力。一旦角色从心底地关心某事，你就可以围绕该情感建立强烈的情感冲突。源于角色观念和情感的冲突不容易传达(给观众)，这不仅因为这样的冲突存在于角色的心里，也是因为它们更具体、不像生死攸关的冲突那么普遍。这种冲突需要对你角色和角色所处的世界有更深的了解。你必须能解释角色的哪些情感导致了他有那种观念。 （这种解释更正式地被称为“展现”。）此外，你必须把被埋没的冲突问题凸显出来，即在起作用的两方力量。  

(Summary) Conflict is a collision of two opposing forces, which offers a dramatic question to an audience: “Which force will win?” Pixar’s films often depict dangerous worlds rife with life-and-death situations, where losing the struggle would mean the demise of a protagonist. While these conflicts are entertaining and easily relatable, Pixar strives to create deep emotional effects on its audience. To achieve them, they create strong internal conflicts.  
冲突是两个对立力量的碰撞，这会给观众带来了一个戏剧性的问题：“哪个力量会获胜？”Pixar的电影常常描绘出生死攸关的危险世界，在这种情况下，不抗争就意味着主人公的消亡。这些冲突既有趣又容易引人注目，但Pixar努力让观众在内心深处受到感染。为此，角色们有了强烈的内心冲突。  


- 总结：强烈的内心冲突 --> 观众在内心深处受到感染。  

- 例如，叶子和她母亲的冲突，熏和辰巳的冲突、熏和早纪的冲突...

- 再例如，对于优柔寡断的雅彦来说，冲突无处不在：要不要异装、要不要看紫苑的相册以确定她的性别、...

- FC里常常用内心独白的形式呈现内心的冲突  


**Creating and Communicating Emotional Conflict 创造和沟通情感冲突**

Conflict is more effective when there is something your character is risking. This is where the opinions we discussed in the first chapter come in.  
当你的角色冒某些风险时，冲突会更有效。 这就是我们在第一章中讨论过的观念的来源。  

Your characters must care about something.When they stand to lose that something, you have conflict.  
你的角色必须关心某事物，当他们面临要失去那个事物时，就会有冲突。  

(Summary) These kinds of conflicts are challenging to create and communicate. They must be rooted in the opinions and beliefs of a character and must put them in danger of losing something dear, usually a part of their identity. To express the emotional forces struggling within, we must find filmable, external expressions of the conflicts: other characters, mementos, dialogue, or a symbol system that is clear to the audience. Making the situation extreme also helps convey the meaning of your character’s struggle.  
这类冲突对于创建和沟通很有挑战。 他们必须植根于角色的观念和信念，必须使他们面对失去珍爱的东西（通常是其身份特征的一部分）的危险。 为了表达内在挣扎的情感力量，我们必须找到冲突的可供拍摄的、外部的表达：别的角色、纪念品、对话或对观众显而易见的各种符号。让情况变得极端还有助于呈现角色挣扎的意义。  

- 例如，叶子和母亲的冲突。如果叶子母胜利(要叶子继承旅馆)，则叶子失去梦想(无法当漫画家)。借助冲突，说明叶子很想当漫画家。  

- 例如，空和紫结婚后的冲突：紫想要小孩；而空不想生小孩，因为刚刚得到漫画连载的机会。说明紫渴望完整的家庭(有孩子)，而空渴望事业有成。但最终，为了紫的梦想，空暂时放弃了自己的梦想，说明空更在乎紫。  


**Making the Stakes Real and Larger Than Life（让筹码更真实、高于生活）**  

Pixar’s films deal with extremes. This means that no matter what the stakes are, Pixar will amplify them as much as possible.  
Pixar的电影里到处都是极端的例子。 这意味着，不管筹码是什么，都会被Pixar尽可能地放大。 

- 例如，紫苑的初恋--光浩--由男生变成了女生。这意味着紫苑**永远**不可能和她初恋的男生发展感情了。

**Exposing and Changing Characters—A Chance at Construction（暴露和变化的性格--一种建构的机会）**  

Conflict is needed because, well, audiences find it enjoyable. They are presented with two opposing forces and want to know which will win—very much like spectator sports, which have entertained masses since the dawn of time.... Pixar’s films usually offer something more in this struggle. In their movies, conflict tends to expose or change something emotional within the core of their characters.  
之所以需要冲突，是因为观众觉得它很有趣。观众面对着两方对立的力量，想知道哪一种会赢--就像观看体育比赛一样，从一开始就吸引了大众....Pixar电影通常会在斗争中加入更多的东西。在他们的电影中，冲突往往会暴露或改变角色核心的某些情感。  

This is the flip side of the destructive force mentioned earlier. Characters must be in danger of destruction, but they must also have a chance at construction as well. They must stand a chance of surviving the threat they’re facing, and of emerging from the struggle stronger and whole—perhaps defeating the flaw mentioned earlier. If a force is defeated but the protagonist remains susceptible to the same form and strength of attack again, then they haven’t truly defeated that force. Furthermore, the movie would feel futile.  
这是前面提到的破坏力的另一面。角色必须处于被毁灭的危险中，但是他们也必须有机会建构。他们必须有机会度过自己所面临的威胁、有机会从更大更广的斗争中摆脱出来--也许可以克服前面提到的缺陷。如果一方力量被击败，但主角仍易受同样形式和强度的攻击，那么他们还没有真正打败该这方力量。同时，电影会很平淡。  

In storytelling, the most powerful instance of construction is personal, when a protagonist must change something deep within themselves to achieve their goal. This is deeply moving because true change is extremely hard to achieve—in fiction and in life. We resist change, and so do our characters, because change entails risk. Did you ever try to kick a bad habit? Or embark on some new venture? Change doesn’t just bring with it a new, unknown territory and the risk of ridicule or failure—it also forces us to dismantle some part of us. Every change isn’t just a birth but also a death, as characters must part with some deep aspect of themselves.  
在讲故事中，当主角必须通过改变自己内心深处的某些东西来实现其目标时，最有力的建构的例子是个人。这能从内心深处感动观众，因为真正的改变在小说和现实中很难实现。我们不愿改变，所以角色也不愿改变，改变会带来风险。你是否曾试过改掉某个不良习惯？或开始新的冒险？改变不仅会带来新的未知领域，还会带来嘲笑或失败的风险，而且还迫使我们审视自己的某些方面。每个变化都意味着新生和死亡，因为角色必须与自身的某些深层部分分开。  

... Notice how this change is both death and birth: Yes, the toys found a new home that seems to solve all their problems, but this solution forces them to leave their cherished owner, Andy. Change always comes with a price.  
...请注意，这种变化如何既意味着死亡又意味着新生：是的，(《玩具总动员》里)这些玩具找到了一个似乎可以解决所有问题的新家，但是这种解决方案迫使他们离开了自己珍爱的主人安迪。改变总是要付出代价的。  

This means a protagonist’s goal might change throughout your story.  
这意味着主角的目标可能会在整个故事中发生变化。  

- 例如，空和紫生孩子前，空的梦想是漫画连载、事业有成。有了孩子后，空的梦想变成了：为了我这可爱的妻子和孩子...为了不破坏这个家庭的每张笑脸，我要比以前更加努力！！(10_190_2)  


If the conflict in your story merely allows your character to show their skills, or to stretch them, you’re only halfway there. Try cranking up the discomfort, forcing your characters to dispense with whatever baggage is hindering them, and build themselves anew, to deal with the threats you’ve created.  
如果故事中的冲突仅使你的角色能够展示或扩大他们的技能，那么你仅仅是做了一半而已。尝试刺激不适感、迫使角色放弃任何阻碍他们的包袱，并重建自我，以应对危险。  

(Summary) The best kind of conflict offers a chance for both destruction and construction, which would have a fortifying effect against the antagonistic forces. Construction can only come from change, to which characters and people are naturally averse. Therefore, the quality of the conflict in your script is measured by the believable change it propels in your characters. Change is the measuring unit of conflict.  
好的冲突为破坏和构建都提供了可能，这将对付反派。构建只能来自变化，而角色和人本能地厌恶变化。因此，剧本中冲突的好坏，是由角色的变化来衡量的。变化是冲突的衡量单位。  

(Summary) Pixar also wisely exacerbates smaller conflicts by manipulating expectations. ... Before springing something good or bad on your characters and audience, set opposite expectations for a stronger effect.  
Pixar还聪明地把小冲突变得很剧烈，已改变观众的期望。 ...在给角色和观众带来好事或坏事之前，要设定相反的期望以获得更强的效果。  

(DIY)What is the main dramatic question in your script? What is the answer the audience must stick around to see?  Does this question have an emotional component?  

- 例如，CH043，爷爷阻止顺子的婚礼，提出条件为"收养雅彦"。主要的戏剧问题是：顺子的婚礼能否顺利举行？雅彦会被爷爷收养吗？。读者希望顺子婚礼能顺利举行，而且不希望雅彦被爷爷收养。若顺子婚礼不能顺利举行，几乎所有人都会伤心。如果雅彦被爷爷收养，雅彦、若苗家都会伤心。  

(DIY)Have you found an original, organic way of expressing this inner struggle in the physical world of your story?  

- 雅彦内心很纠结要不要被爷爷收养。CH043用若苗紫和空的对话、雅彦的噩梦、顺子因得不到父亲的祝福，来表现雅彦内心的纠结。    

(DIY)Is there a force capable of destroying your characters?  

(DIY)Is there a force in your story capable of pushing the characters to construct something new and stronger?  

- 例如，14卷里，雅彦从"看到浅葱吻紫苑后,吃醋"，到"和紫苑讨论改变剧本"，再到"不追回叶子"，再到"看到紫苑转身逃跑后,震惊"，再到"去奥多摩和叶子分手"，再到"意识到自己真正喜欢的是紫苑"，再到"表白紫苑"。可以看出，这些过程里雅彦的内心一直在转变。  

(DIY)Do your characters change in a clear, discernible way because of the conflicts they face?  

- 例如，14卷里，雅彦从"不追回叶子"，再到"看到紫苑转身逃跑后,震惊"，再到"去奥多摩和叶子分手"，再到"表白紫苑"。这些都是他主动的行为。    

.  
.  

-----------------------

**CHAPTER 5， PIXAR’S STRUCTURE（第五章 Pixar故事讲述的结构）**  

**A Word on Structure**  

(Summary)There are many forms of structure. Most agree on the three parts of story: setup, trials, and climax and resolution. They also focus on the major events stories must have: one to start it, one to end it and one to three more in between. These events are integral parts of the main story of your film, and each pertains directly to your protagonist’s main problem. They are what happens in your story, whereas the scenes around them depict how and why they happen.  
结构有很多种形式。 大多数人都同意故事有三个部分：开端、经过、高潮和结局。 还要有主事件：一个作为开端，一个作为结束，这两者之间还要有一到三个主事件。 这些事件组成影片主要故事，每个事件都直接关系到主角的主要问题。 这些事件是故事中发生的事，而围绕它们的场景则表明了事件发生的原因和经过。  


- Pixar故事讲述的结构总结：  
    - setup(开端)(1st major event(主事件1))  
    - trials(经过)：  
        - 2nd major event(主事件2) / 1st plot point(剧情点1)  
        - 3rd major event(主事件3) / 2nd plot point(剧情点2)  
        - 4th major event(主事件4) / 3rd plot point(剧情点3)  
    - climax(高潮) / The last major event(最终主事件)  
    - (2nd climax(第二高潮))  
    - resolution(结局)  


- 因为FC是长篇连载，有时候几话联合讲一个故事。所以，每一话不一定符合上述的叙事结构。但有些单话故事是符合上述叙事结构的。
比如，CH069"恐怖的跟踪者"(仁科出场)的结构是这样的：
    - 开端（1st major event）：紫苑被跟踪。
    - 经过（the middle）：要找出跟踪者。
        - 2nd (major) event (1st plot point)。雅彦和江岛负责侦察，江岛被报复。
        - 3rd (major) event (2nd plot point)。影研社录影带被盗。
        - 4th (major) event (3rd plot point)。叶子被跟踪，并被要求和雅彦分手。
        - 5th (major) event (4th plot point)。跟踪者以录影带为筹码，要挟和雅彦见面。
    - 高潮(climax)：The last major event 。抓捕跟踪者，真相大白。
    - 第二高潮(2nd climax)：说明仁科的作案的不良动机。助手们如何安抚仁科、如何消解该不良动机。
    - 结局(resolution)：仁科表示不会放弃雅美(为以后的故事做铺垫)

- FC每一话的叙事结构常常以"页"分隔--某一页开始新的叙事阶段。

- FC作为长篇故事，有两种故事线：1.短线：每一话（或每几话）讲一个故事； 2.长线：主要角色的矛盾(雅彦和紫苑的感情、雅彦和叶子的关系、紫苑想做男生还是女生，等)的发展，贯穿于整个FC的各个故事里。


**Major Events—Whats, Hows, and Whys（主事件--是什么、如何做、为什么）**

If most scenes in your script deal with overcoming obstacles, major events are often about revising goals, perceptions, and strategies.   
如果脚本中的大多数场景都与克服障碍有关，那么主事件通常与改变(角色的)目标、看法和策略有关。  

Pixar creates more plot points and major events than it initially seems. The writers use subplots, parallel plots, and internal plots to create more stories and thus more major events. This story inflation seems to keep the audience’s interest and satisfy them emotionally and narratively.  
Pixar建立的剧情点和主事件比最初看起来要多。 编剧使用支线故事、平行故事和内心情节来讲述更多故事，从而创建更多主事件。 这似乎可以保持观众的兴趣，并在情感和叙事上让观众满意。   

**A Multilayered Storytelling Cake （故事叙述的多层蛋糕）**

The main plot should be epic, adventurous, and rife with action, with an emphasis on life-or-death, physical situations.   
主线情节应该是史诗般的、充满冒险和行动的，并强调生死攸关和外部实际的状况。  

Under that main plot, there will be an emotional, internal story.  
在主线情节下，会有一个内心的情感故事。  

All the studio’s stories also feature a major, complex bonding process with another equally flawed character from a different world.  
Pixar工作室的所有故事都有主要的、复杂的情感纽带故事，辅以一个来自不同世界的、同样有缺陷的角色。  

Sometimes Pixar will add another subplot, usually taking place parallel to the main plot.  
有时，Pixar会添加另一个支线情节，通常与主线情节平行。  

This triple and sometimes quadruple structure seems to satisfy all the reasons we go to the movies: high-end adventure, meaningful relationships, and deep emotional struggles. Pixar’s skill in juggling these layers, and threading them together strongly, seamlessly, and honestly is what sets them apart. The different layers reinforce one another, giving emotional weight to the daring actions and clear dramatic expression to the emotional conflicts.  
这种三线--有时是四线--的结构似乎能吸引观众：高端的冒险、有意义的关系、以及深刻的情感斗争。Pixar的与众不同在于它能处理这些多故事线，并将它们牢固、无缝、可信地连接在一起。不同的情节之间相互强化，使勇敢的行为带有强烈的情感，也清晰地表达情感冲突。  

(Summary)Pixar’s films usually have a layered structure that involves an adventurous life-or-death action story, an interpersonal story of bonding, and an inner emotional struggle. These layers are interconnected. This structure serves as a force multiplier, enriching each of these stories separately and creating more major events, which audiences seem to enjoy. Often, Pixar’s films will include yet another storyline, taking place concurrently with the main story.  
Pixar电影通常具有多线结构，涉及冒险的生死攸关的故事、人与人之间的纽带关系、以及内心的情感挣扎。这些故事线相互交织。这种故事结构能放大叙事的效果、丰富了每个故事，并创建了更多观众喜欢的主事件。Pixar电影通常会包含另一个故事线，它与主线同时发生。   

**Bonding Stories（情感纽带故事）**

As mentioned, conflict is crucial to your story and, yes, there is something deeply satisfying about watching a character fight against the odds for something they deeply believe in. But .... Watching two people meet and immediately become best friends isn’t moving. Nor is watching two people hate each other for an entire movie, only to turn on a dime and fall in love in the last minutes of the film.  
如前所述，冲突对你的故事至关重要，是的，人们喜欢看角色与他们的宿敌斗争。但是，...看着两个人见面并立即成为最好的朋友，并不会让人很感动。同样，看着两个人从头到尾都是彼此讨厌，只在电影的最后一分钟坠入爱河，这也不会令人感动。  

Bonding should involve two characters who have clear reasons not to like each other. Only through a parallel process of external events pushing them together and inner changes that remove the relationship’s emotional obstacles, can they come together in a way that is truly meaningful.  
情感纽带应包含两个角色，这些角色有明确的理由不喜欢彼此。只有通过并行的外部事件将它们融合在一起、通过内心变化来消除情感隔阂，他们融合在一起才变得真正有意义。  

Sometimes bonding is propelled by discoveries about the other characters. ..... Sometimes it is a self-discovery....  
有时，情感纽带是由挖掘其他角色来推动的 .....有时则需要挖掘该角色本身....  

- 例如，雅彦和叶子的感情，时好时坏；雅彦和紫苑的感情，逐渐增加相互间的好感。

- 例如，雅彦不知道紫苑的性别是男是女，导致他不敢正视"自己喜欢紫苑这个事实"。浅葱吻紫苑带给雅彦很大触动。紫苑和雅彦讨论剧情，实际也是在坦白和讨论两人之间的矛盾。叶子主动提出分手，同样也给雅彦很大触动。这一切都推动雅彦内心意识的觉醒，最终意识到自己喜欢的是紫苑。  

**Double Climaxes （双高潮）**

Many of Pixar’s films feature a double climax. The main story is resolved in a grand, high-stakes sequence, as in most films. In Pixar’s films, this storytelling peak is usually followed by a quieter climax scene, designed to resolve one of the more internal stories.  
Pixar的许多电影都具有双高潮。 如同大多数电影一样，主故事以宏伟、高风险的顺序得到解决。 在Pixar的电影中，通常在讲故事的高峰之后是一个稍微平静的高潮，目的是解决内心的故事。  

Pixar goes a step further by creating an inner world and existing flaw for these characters.  
Pixar进一步为这些角色创建内心世界和现有缺陷。  

Whenever you’re adding an element to your story, make sure it’s inherent in your original concept. Try to explore and expand existing characters, settings, plots, and themes. If you’re adding a more external element, take extra care to develop it in a way that interacts meaningfully with your core characters and story.  
每当你在故事中添加元素时，请确保其和你的原始概念中是一脉相承的。尝试探索和扩展现有的角色、设置、情节和主题。如果你要添加更多外部元素，请格外小心，使其与核心角色和故事的互动要有意义。   

(Summary)This structure also results in a double climax, where Pixar stages a grand, suspenseful, awe-inspiring, life-or-death sequence--the climax of the film’s main plot--which is followed by a moving emotional climax. This double whammy creates powerfully moving and satisfying endings.  
这种结构还导致了双高潮，一系列宏大、悬念、令人敬畏的、生死攸关的镜头--影片主情节的高潮，随后是动人的情感高潮。 这种双重高潮能产生强大而令人满意的结局。   

- FC的也有很多第二高潮。例如:  
    - CH008"母亲的回忆"。高潮：借雅彦拿筷子姿势，紫向雅彦讲述“母亲的回忆已经变成你的习惯、成为你自己的一部分了”；第二高潮：若苗家三人送给雅彦三个新杯子。 
    - CH073"阴错阳差的平安夜"。高潮：雅彦和叶子的冲突；第二高潮：下雪了，所有人迎来了白色圣诞节。
    - CH074"物以类聚"。高潮：早纪和辰巳冲突(两人终于见面)；第二高潮：新年到来时，两人和好。


(DIY)Does your story feature all of Pixar’s three layers?  

(DIY)Do you have a high-stakes action plot with chases, set pieces (powerful scenes that are very exciting, emotional, original, elaborate, or otherwise memorable and grand), and life-or-death situations?  

(DIY)Is there an honest bonding plot in your story?  

(DIY)Is it deeply connected to the main story you’re telling? 

(DIY)Are the characters kept apart by conflicting points of view and emotional obstacles? 

(DIY)Does your protagonist have an internal struggle they must resolve? (DIY)Is that struggle strongly connected to your other stories? (DIY)Is it dramatized externally through these other stories?  
- 紫苑想要确定自己的性别认同。雅彦一直没有意识到自己真正喜欢紫苑、一直纠结紫苑的真正性别。FC里多次表现出雅彦想知道紫苑的真正性别，同时因为不知道答案，也影响到了某些剧情(或成为借口)。 

(DIY)Do each of these stories reach a climax in a strong, clear way, distinguishing them if necessary?  

.  
.  

-----------------------

**CHAPTER 6，CASTING CHARACTERS (第六章 铸造角色)**

**Your Story as an Efficient Machine（把你的故事作为高效的机器）**  

Everything you introduce in your script—every line, character, theme, or piece of information—must have a function and be part of the grand scheme. Often, all this means is exploring how to make the most out of every preexisting element in your script.  
你在脚本中引入的所有内容（每行、每个字符、每个主题或每条信息）都必须具有功能，并成为整个故事的一部分。 通常，所有这些方法都在探索如何充分利用脚本中每个已有元素。  
.  
- 例如，CH075"紫苑的入学试". 紫苑考试完毕后在家养病，雅彦陪着。(11_138)有一个小细节：加湿器响了，雅彦给它加水。这个看似和剧情无关的小细节, 可能有两个原因：
    - 紫苑在睡觉，为了让两人聊天，就用加湿器的铃声把紫苑吵醒。
    - 紫苑说自己考试可能会挂，雅彦愣住了。由于雅彦在给加湿器加水，所以背对着紫苑，所以紫苑看不到雅彦的表情，所以才会继续问：“（我考不上的话）你很高兴吧？”  

**Characters as Plot Functions（仅具有情节功能的角色）**  

Most of your characters will have a meaningful relationship with your protagonist. They will be their friends, lovers, mentors, or adversaries. However, some characters in your script will simply have a story function to perform. It’s important to design these characters with care, so the mechanism that necessitated their existence isn’t too transparent.  
你的大多数角色都会与主角保持必要的关系。他们将成为他们的朋友、恋人、导师或对手。 但剧本中的某些角色将仅具有推动故事的功能。设计这些角色要很用心，以确保它们存在的机制不太容易被识破。  

In Brave, Merida longs to have her destiny changed. This leads her to a witch.  
在《勇敢传说》中，Merida渴望改变自己的命运。 这导致她成为一个女巫。  

- 我不确定，FC里的葵、影研社的导演、摄影师算不算仅具有情节功能的角色.  


**Keep Your Writing Honest（写作时要诚实）**  

让角色的行为合理、可信。 

**Designing Distinct Characters（设计独特的角色）**  

Make your characters different emotionally and physically. Whether farmhands or finance brokers, actors or chefs, be sure to make each of your characters stand out.  
使你的角色在情感和体态上都与众不同。 无论是农场主还是金融经纪人、演员还是厨师，请务必使你的每个角色脱颖而出。 

- 雅彦：善良，犹豫不决，
- 紫苑：聪明，漂亮，勇敢。
- 空：男子汉，敬业，
- 紫：美丽，贤惠，
- 叶子：坚持梦想，渴望爱情，嫉妒
- 熏：大大咧咧，不良少年，
- 助手浩美：漂亮，
- 助手真琴：漂亮，电脑白痴，胆小
- 助手和子：大块头，深沉
- 助手横田进：大块头，无私（为和子的孩子来参观，甘愿委屈自己穿男装）
- 爷爷：身材矮、固执、守旧
- 奶奶：和蔼  


(Summary)Treat your story as an efficient machine. Every part of it should be treated with care and should be a part of your grand scheme. Nothing should be missing, and nothing should be redundant. This is especially true for your characters. While some characters may have specific functions to perform—obstacles, catalysts, and so on—they must also be drawn with care and imbued with their own stories and personalities. Never sacrifice honesty for originality or coolness. No matter what awesome invention you come up with, work hard to tie it to an emotional reality that is part of your fictional universe.  
将故事视为高效的机器。它的每个部分都应谨慎对待，并应成为你宏伟计划的一部分。不要缺东西、也要冗余。 对于角色尤其如此。尽管某些角色可能具有特定的执行功能（障碍物、催化剂等），但也必须谨慎设计，并赋予他们自己的故事和个性。切勿为创意或酷炫而让角色不真实。无论你想出什么伟大的发明，都要努力将其与虚构的世界里的情感现实联系起来。  


(DIY)Review the characters and settings in your script. 

(DIY)Are they(the characters and settings) all part of your core idea? 

(DIY)Is there something missing? 

(DIY)Is there something you can do without? 

(DIY)Are any elements nothing more than perfunctory? 

(DIY)What can you do to make your characters interesting, specific, or entertaining?

(DIY)Have you employed fun inventions retained only for their cool factor, but which are dishonest in their emotional truth? (DIY)How can you fix that?  

.  
.  

-----------------------

**CHAPTER 7， VILLAINS（第七章 坏人）**

**A Word about Antagonism（关于反派）**

Antagonism refers to anything that stands between your protagonist and their goal. It can be a character, an object, a concept, or even the protagonist themself.  
对抗是指主角与目标之间存在的任何事物。它可以是角色、对象、概念、甚至主角自己。  

Often, though, they are a more innocuous character who happens to inadvertently make life harder for your protagonists.  
但通常，它们是一个更单纯的角色，恰巧无意中使你的主角们的生活更加艰难。  

**Evil versus Troublesome（邪恶 v.s. 惹人烦）**  

When discussing antagonistic characters, we must make a distinction between “evil” and “troublesome.”  
在讨论反派角色时，我们必须区分角色是“邪恶”还是“惹人烦”。  

“Evil” characters have no regard for morals or fairness, and are indifferent or even joyous at the pain of others. Nevertheless, they should still have their own story and their own reasons for their actions....Everything that makes your protagonist interesting--an existing flaw, experience, point of view, idiosyncrasies--your villain should have too.  
“邪恶的”角色不顾道德或公平，对别人的痛苦漠不关心甚至幸灾乐祸。 尽管如此，他们仍然应该有自己的故事和这么做的理由.....已有的缺陷、经历、观点，这些都会让你的主角很有趣，反派也应该有这些。  

Villains make our protagonist’s life harder because they enjoy malicious activities or because they prioritize their pleasure over someone else’s wellbeing. Other antagonists may mean well but just happen to cause our protagonists grief. Let’s call them “troublesome.”  
坏人让主角的生活更糟糕，这是因为坏人喜欢邪恶的事情，或是因为他们把自己的快乐建立在别人的痛苦之上。另外一些反派，他们也许很刻薄，但仅仅导致我们的主角伤心，我们称这样的角色为“惹人烦”。  

Pixar films often feature two main antagonists—one benevolent and one malicious--Buzz/Sid, Princess/Hopper, Jessie/Stinky Pete, Boo/Randall, and so on. Ratatouille has three main antagonists: One evil (Skinner), one benevolent (Linguini) and a “Good” Villain (Anton Ego).  
Pixar电影通常有两个主要的反派--一个善意、一个恶毒。《料理鼠王》有三个反派：一个邪恶、一个有善意、一个"好"坏人。  

**“Good” Villains（"好"坏人）**  

“Good” villains appear, on the surface, to belong to the malicious group. They are often mean, indifferent to the pain they cause, and even terrifying. What differentiates them from malicious antagonists ... is that they have a benevolent core set of beliefs that strive to benefit their community. This is important. They can’t be purely self-interested. The “good” villains aren’t nice, but they have positive core values. They never unfairly target anyone, and they don’t enjoy hurting people. Their moral compass is in place, and they have certain lines they will never cross.  
从表面上看，"好"坏人似乎属于反派。他们通常是刻薄、漠视所造成的痛苦、甚至令人害怕。他们与恶毒的反派之所以不同，是因为他们善良，并能惠及身边的人。这很重要。他们不是完全出于自身利益。 "好"坏人...虽然不好，但它们具有积极的核心价值观。他们从不会不公平地对待任何人，也不喜欢伤害别人。他们的道德罗盘没有掉线，他们有一定的底线，永远不会越过底线。  

**Antagonists as Mirror Images（以反派为镜）**  

Some antagonists will have a thematic relationship to your protagonist. They will be a distorted mirror image, presenting our heroes with their darkest fears, or at least shining a light on their fallacies and weaknesses.  
有些反派会与你的主角存在(与主题有关的)联系。他们是扭曲的镜像，向主角展示他们最令人恐惧的阴暗面，或者至少让人看到他们的谬论和弱点。  


- 早纪：邪恶的角色。

- 辰巳：早期可能算作"邪恶"，后期可能算是"惹人烦"。

- 熏：早期可能算作"邪恶"，后期可能算是"惹人烦"。

- 影研社的导演、摄像师：迫使雅彦异装时算是"惹人烦"。

- 江岛
    - 因讨厌男装的紫苑，而想踢紫苑出影研社。这时可能算是"邪恶"。
    - 在酒吧里开应新会时，江岛因妒忌男装的紫苑受人欢迎，便让紫苑多喝酒。这时可能算是"邪恶"。

- 爷爷
    - 阻止空异装，可能算是"好"坏人。
    - 为女儿顺子安排婚姻，阻止顺子和宪司结婚，可能算是"好"坏人。
    - 打算收养雅彦，可能算是"惹人烦"。
    
- 叶子母
    - 要叶子继承旅馆，可能算是"好"坏人。


(Summary)A charismatic, cackling villain can be a lot of fun (especially when voiced by Kevin Spacey or Kelsey Grammer). Some of your stories will need those.  
一个有魅力的、咯咯笑的坏人可能会很有趣。你的某些故事可能会用到这种角色。  

(Summary)Keep in mind that many antagonists have good intentions and can be complex characters. When looking for antagonists to put in your hero’s way, don’t limit yourself to villains. Friends, supporting characters, and even the environment itself can provide you with plenty of dramatic fodder.  
请记住，许多反派会有好的初衷，并且角色本身可能很复杂。想妨碍主人公时，请不要局限于坏人。朋友、配角、甚至环境本身都可以为你提供大量生动的素材。   


(DIY)Who are the characters standing in your protagonist’s way? 

(DIY)Are they evil for the sake of evil, or do they have reasons for their maliciousness? 

(DIY)How can you make them more understandable and relatable? 

(DIY)Is there a way to make them benevolent while retaining the opposition they present to your hero? 

(DIY)Are your characters’ allies constantly helping them out, or do they challenge them as well? 

(DIY)Friendship must be earned, and even then, it is there to challenge just as much as it is to support.  

.  
.  

-----------------------

**CHAPTER 8，DEVELOPING AN IDEA（第八章 构想）**  

**Plotting versus Exploring（按计划制订 v.s. 探索）**  

- 两种方式：  
    - Plotting(按计划制订):  
        you know the end... You must now design stops that will reach the end.  
        你已设定了结局...然后，设计出如何一步一步地走向结局。  

    - Exploring(探索):  
        You don't know the end...explore your universe.  
        你没有设定结局...探索你的宇宙。  
            - to subvert expectations.反转  
            - to impose creative limitations on your story. 给你的故事增加创造性的限制。  

These approaches aren’t mutually exclusive. You’ll often switch between them.  
这些方法并不互相排斥。 你经常会在它们之间切换。  

- 《北条司拾遗集.画业35周年纪念》里(147页)有访谈： 最后，非常家庭以后情节会任何展开呢？  
    - 北条： 会有紫苑的性别问题，该如何处理呢（笑）。或许不会明确地画出来（笑）。雅彦和紫苑是第二代性别倒错夫妇。剧情本身没有没有大事件发生，也没什么计划，一边看角色的动态一边展开情节，老实说，未来的事一点也没考虑。只是，这是一部不同的作品，有着常年一直保留的素材，它以后会变成什么样子呢，经请大家期待（笑）。
由此可以看出，作者北条司在设计FC剧情时，已经确定雅彦和紫苑最终会在一起，但紫苑的性别并未确定。这也说明作者在探索多种可能的剧情。  

- 关于"to subvert expectations.（反转）"，我觉得一个例子可能是CH044"婚礼的秘密对策"里的情节反转再反转。CH044里，爷爷看到雅彦女装出现，意识到收养雅彦的计划破灭。但爷爷突然意识到“雅彦女装”可能是一个伎俩、目的是若苗家让爷爷放弃收养雅彦。意识到这是个伎俩后，爷爷哈哈大笑。后来空的助手赶来，了解到"助手们在若苗家待了一段时间后都选择变成女人"后，爷爷意识到雅彦女装可能是真的--收养雅彦的计划最终破灭。  

**Focus Your Canvas--Creative Limitations（关注你的画布--创意限制）**  

In his TED Talk, Andrew Stanton discusses some of the self-imposed limitations Pixar’s writers decided on when they began, among them, not having a love story. Selecting that one limitation must’ve forced the writers to consider what the relationships in their films would then be, thus inevitably pushing them in new, interesting directions.  
在TED演讲中，Andrew Stanton讨论了Pixar编剧们在开始创作时就自己设置的一些限制，其中包括没有爱情故事。选择一个限制必须迫使编剧考虑电影中的各个关系，从而不可避免地将这些关系推向新的、有趣的方向。  

(Summary) Part of Pixar’s success is due to the studio’s originality and unique inventions. These stem from exploring your fictional universe and learning about all the places, people, emotions, and ways of life it has to offer. You don’t have to use all your findings, but you should incorporate what helps you develop your plot and character arc. One of the best ways to explore your universe is to try to subvert expectations. Another way is to impose creative limitations on your story.  
Pixar工作室的成功部分归功于独创性和独特的发明。 这源于探索你的虚构世界，了解里面需要有哪些场所、人、情感及其生活方式。你不必用到全部，但应结合它们以便于开发情节和角色弧。探索虚拟世界的最好方法之一就是反转剧情。另一种方法是对故事添加创意限制。   

(DIY)How much do you know about your fictional world?

(DIY)Have you allowed yourself to wander through its streets and fields, talking to its denizens, or did you only mine the parts needed for your plot? 

(DIY)Consider the expectations from the material you’re developing.

(DIY)Where can you subvert those expectations to create a moment that will surprise and delight your audience? 

(DIY)If you’re stuck, consider what you don’t want to happen in your story world. Anything you eliminate will inevitably point toward something you would like to incorporate.  
如果你没有头绪，请考虑在故事世界中不想发生哪些事情。 有了这些不想要的内容，想要的内容就会显现出来。  

.  
.  

-----------------------

**CHAPTER 9, ENDINGS（第9章 结局）**  

**Coincidence versus Character（巧合 v.s. 角色）**  

A good ending must make sense without being predictable. It should come with a bit of a surprise ... One key way to reach this goal is to tie the ending deeply to the actions and constitution of your protagonist. The final action must be a direct result of the journey your characters have taken. In other words, avoid coincidences.  
一个好的结局必须是合理的、不可预测的。 它应该带有一些惊喜... 达到此目标的一种关键方法是将结局与主角的行为和构建紧密联系在一起。最后的行为必须是你的角色经历的直接结果。换句话说，避免巧合。  

- FC共102话，从CH098"雅彦的剧本"里影研社计划拍戏开始，情节朝着结局一步步地推进，雅彦的内心一点点地在变化：先是，浅葱表白、亲吻紫苑，使得雅彦吃醋。接着，雅彦和紫苑在游乐场鬼屋一天情人、配合默契。然后，雅彦和紫苑借讨论剧本而坦白并讨论两人间的分歧。紧接着，雅彦未支持叶子的梦想，导致叶子离去，而雅彦未追回叶子。再然后，借拍戏时紫苑转身逃跑，雅彦看到了自己的逃避。这直接促成他去奥多摩和叶子分手，并意识到自己真正喜欢的是紫苑。最终，雅彦表白紫苑，紫苑表示愿做女生。所以，雅彦虽优柔寡断，但他最终能表白也是合理的，不是巧合。  

**Back to the Beginning—Answering a Question the Audience Forgot（回到开始—回答观众遗忘的问题）**  

One way to craft a strong ending is to answer a question the audience has probably forgotten or hadn’t even thought about but should have.  
建立良好结局的一种方法是回答观众可能已经忘记或没有想到(但应该想到)的问题。  

**Resolution—Showing the New, Healthy World（结局--显示新的、健康的世界）**  

We’ve discussed how audiences love seeing characters change. This change, while satisfying, feels more meaningful and carries more weight when it creates a ripple effect. In many of Pixar’s films, the journey the protagonists undertake often creates a better world for the people around them, fixing the flaw in the world.  
我们已经讨论了观众如何喜爱看角色变化。这种变化令人满意，在产生涟漪效应时让人感觉更有意义、更有份量。在Pixar的许多电影中，主角们的旅程通常会为周围的人创造一个更好的世界、修复世界上的缺陷。  

- FC里雅彦一直喜欢紫苑，但由于他性格软弱、一直没有表白。所以，最终他能勇敢地迈出这一步，是很好的转变。同时，由于雅彦的表白，紫苑也表示愿做女生，从而解决了一直困扰她的问题。  

(Summary)Your ending must be a reflection of your character and a direct result of the path upon which they were set. It shouldn’t be expected or predictable, but it must be tied to your protagonist’s journey. One way to create this effect is to have the ending relate to a seed you have subtly planted earlier in the film. Hopefully the audience forgets about that detail. When this seed pays off in your resolution, the audience will feel an increased sense of cohesiveness, strengthening the meaning of your ending. Pixar film endings often involve creating a better world. The most moving of endings show the positive results of the journey your character has taken, preferably in a visual way.  
结局必须反映你的角色，并且是角色一路发展而来的结果。结局要与预想的不同、不可预测，但必须与主角的历程联系在一起。产生这种效果的一种方法是预先在电影中巧妙地放置一个情节，然后让结尾与此情节有关。希望观众忘了那个情节。当这情节在结局中展现时，观众会感到情节前后呼应，结局的意义就增强了。Pixar的电影结局通常涉及创造更美好的世界。好的结尾会以视觉效果呈现角色所经历的积极结果。   
.  

(DIY)Is your ending a coincidence, or is it linked through a chain of causality to your character’s actions? 

(DIY)Does it tell us something new about your character’s personality? 

(DIY)Does your ending feel like an inseparable part of your story? 

(DIY)Is it linked strongly to your plot through dramatic questions you’ve left unanswered until later in the story? 

(DIY)Lastly, does your story create a ripple effect? 

(DIY)Does it change something in the people, community, or world surrounding your protagonist? 

(DIY)Is there a clear, potent visual way to express this change?  

.  
.  

-----------------------

**CHAPTER 10, THEME（第10章 主题）**  

**What Is Theme?（什么是主题？）**  

What does your story present and explore that is universal and timeless? What is inherently human about it?  
你的故事所呈现和探索的那些普遍而永恒的东西是什么？ 它本质上是什么呢？  

- 《北条司拾遗集.画业35周年纪念》里有访谈里谈到FC的主题：  
那么，直截了当地说，《天使心》的主题是什么？
    - 北条：这一点被朋友指出后才注意到，流淌在我作品根底的主题是“亲情”。我自己是在《非常家庭》中第一次想到，之前一直没有意识到。说起来的确是那样。实际上，在创作登场人物的过程中，会仔细地考虑角色的家族构成或者家庭环境等。因为这一部分不明确的话，角色是不会有魅力的。对这一点敷衍了事从而创作出的角色会缺乏深度。让人感觉不到人情味...(120页)  
“家人不是靠血缘，而是依靠心与心的结合“(146页)  

**Creating Theme, Step 1: Recognizing What Your Story Is About（创建主题，第1步：认识你的故事）**  

Ideally, your theme arises organically from your story.  
理想情况下，你的主题是从故事中自然产生的。  

- FC开篇借雅彦的悲惨状况，表现了他对家庭的渴望。  


**Creating Theme, Step 2: Permeate Your Theme Ubiquitously Throughout Your Story（创建主题，第2步：在整个故事中无所不在地渗透主题）**  

You must make your theme present in your universe. There are several ways to do so.  
你必须使主题出现在你的故事中。有几种方法可以做到这一点。  

1.If you are creating a character as an embodiment of a value, it is often better to make it a supporting character. Create a complex environment that depicts struggles with different attitudes and values, and populate it with characters who embody aspects of your theme (which often are also the conflicting aspects of your main protagonist),  
如果要让一个角色体现某种价值观，通常最好使其成为配角。创建一个复杂的环境，用不同的态度和价值观来描绘斗争，同时让体现主题的角色们（通常是和主角相冲突的那些方面）生活在这个环境下。    

- 例如，菊地春香(若苗空)坚持自己的人生目标：做男子汉。在菊地家和父亲抗争。菊地顺子(空的妹妹)渴望婚姻自由。奥村宪司和若苗空都为此和父亲争取和抗争。  

2.To reinforce your theme, you can sometimes craft an antagonist as a reflection of your hero. 
为了强化主题，有时你可以精心设计一个反派来反衬你的主角。 

- 例如，真朱家(熏、辰巳、早纪)是若苗家的对立面。熏的原生家庭问题和若苗家成对比。

3.You can also imbue objects with thematic meanings.
你还可以把主题赋予一些事物。

Most of Pixar’s films simply assert their themes, or a variation on them, at some point or another.  
大多数Pixar的电影都只是在某个地方简单地明了说出主题或主题的变形。 

- FC里有两处明确地表明主题：
    -  雅彦和叶子母的谈话。叶子母："人们聚集在一起，内心的温暖感就会不断提高，这就是家庭的温暖。而且这种温暖更会把人拉近...所以必须把温暖不断提高...(13_139_0)"。此处，主题的承载物是叶子家的旅馆。
    -  大结局时，以旁白的形式表述："很有家庭温暖----人与人的邂逅就是这样......新的家族成员诞生了----家庭便会更温暖----二人变三人，然后增加至四人......心里的温暖亦随之增加----然后，那温暖感更进一步把人拉近，温暖感向外扩散，连紧着每个心......很有家庭温暖----"。此处，主题的承载物可能是新生婴儿。

(Summary)Theme is the part of your story that is universal and abstract. It isn’t part of your plot. It is what your plot expresses. Your theme should emerge naturally from the fictional universe you’ve chosen to explore. Once you’ve found your theme, use plot, characters, locations, objects, and dialogue to make it as present as possible in your screenplay.  
主题是你故事中具有普遍性和抽象性的部分。主题不是剧情的一部分。而是情节所要表达的。主题应该自然地出现在你选择探索的虚构世界中。找到主题后，请使用剧情、角色、地点、物体和对话来使其尽可能在剧本中展现出来。

Childhood, family, sadness, maturity, death: These universal themes, placed at the core of our existence, are surprisingly underexplored in mainstream popular culture (compared with issues such as love or crime). Pixar’s brave choice of themes and its equally brave (and wise) explorations of these themes are part of what sets the studio’s films apart.  
童年，家庭，悲伤，成熟，死亡：这些普遍的主题--我们存在的核心--在主流流行文化中未得到充分挖掘（与爱情或犯罪等问题相比），真是令人惊讶。Pixar勇敢地选择这些主题，且勇敢（而明智）地探索这些主题，是其与众不同的原因。 


(DIY)What is your story about? 

(DIY)What are the abstract questions or issues it flirts with or explores? 

(DIY)Can you make these themes more present throughout your story via objects, dialogue, or characters?  

.  
.  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处




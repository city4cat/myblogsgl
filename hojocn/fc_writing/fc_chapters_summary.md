（欢迎补充，欢迎指出错误）

## FC每话情节概括  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96853))  

```
---卷----章节----起始页码-----章节英文标题  /  章节中文标题
Vol01	CH001	page003		"家庭"的初体验  	First "family" Life Experience  		  
FC的每一话约27页，但这第一话有40页。3～12页介绍雅彦的背景；13～18页在去若苗家的路上；19～42页在若苗家的故事，抛出第一个大情节：若苗夫妇的性别反转。


Vol01	CH002	page043		天体之夜  	A Torrid Night  		  
雅彦搬到若苗家第一天：   
- 雅彦闯了祸（弄脏了空的画稿）。但空和助手们对此很宽容；  
- 雅彦参加了空和助手们的聚会，回忆起"若苗夫妇性别反转"的事情。为第3话雅彦出走做铺垫。  


Vol01	CH003	page073		温暖的饭  	A Hot Meal  		
受内因(雅彦无法接受"若苗夫妇的性别反转"的事情)影响，雅彦最终是否离开若苗家。(CH003讲的是内因，CH039、CH043讲的是外因)。  

（我猜，作者北条司的思路是这样的：雅彦搬入若苗家，肯定会有不适应的地方，进而会有出走的想法。与其住了很久后再出走，不如一开始就出走。因为越早出走，挽回的几率更大（所以在前一话安排雅彦参加空和助手们的聚会）。同时，通过雅彦离家出走、又回来、最终决定留下来，来表现若苗家有真正吸引他的元素。）


Vol01	CH004	page099		开学礼的条件  	Entry Ceremony  		  
雅彦要求若苗夫妇按他们的生物性别着装来参加自己的开学典礼--雅彦给若苗家增添麻烦后会怎样。  


Vol01	CH005	page125		棒球的回忆  	The Glove Of Memories  		
引入“紫苑到底是男是女”这个话题。


Vol01	CH006	page153		在远处遥望的初恋  	A First Discrete Love  		
紫苑小学男装时被女生喜欢的故事（为此，之前一话讲了紫苑小学时时而男装、时而女装的情节）


Vol01	CH007	page179		助一臂之力  	Just A Little Service  		
紫 支持空的工作--紫代替浩美去临时去夜总会工作一天。    
- 空的工作情况（截至日前必须交稿）。  
- 紫为家庭的付出。1. 紫 为了 空 的工作，而甘愿付出（替浩美当班）。2. 紫 当班回来，不顾辛苦（一路背着雅彦回来），立刻给空做饭。  
- 雅彦对若苗夫妇的帮助(帮助空照看在夜总会当班的紫)。  


Vol02	CH008	page003		母亲的回忆  	Mother's Gift  		
妈妈送给雅彦的马克杯被摔碎了。在我看来，这个故事涉及了这些话题：  
- 雅彦入住若苗家后，为了能更好的融入这个家庭，可能需要或多或少地放手一些原先家庭的情感。
- 若苗家人犯了错(间接导致摔破杯子)之后，雅彦会怎样。
- 若苗夫妇对紫苑的家教（杯子虽然被粘好，但雅彦还是很伤心；紫苑对此很生气，紫因此开导紫苑）。    
- 吃饭时，紫苑抱怨雅彦（雅彦不在场）来到若苗家，空对紫苑的话很生气。说明空不允许紫苑嫌弃雅彦。  


Vol02	CH009	page031		家庭旅行!！  	Family Trip!  		
一家人决定去泡温泉。在我看来，这个故事是在讲：  
- 雅彦为若苗夫妇着想（雅彦阻止紫苑破坏温泉之旅）。
- 紫苑意识到雅彦并不讨厌若苗夫妇。
- 雅彦和紫苑两人(在车内)第一次亲密接触。  
- 若苗夫妇对雅彦的包容（雅彦带错了路）


Vol02	CH010	page059		不请自来的助手  	The Stubborn Assistant  
Vol02	CH011	page087		仰慕的人  	The One I Admire  	
引入雅彦的女朋友叶子。从此开启三角恋：紫苑-雅彦-叶子。  

我猜，为了不让雅彦和紫苑的感情发展的太快，所以需要给紫苑或者雅彦安排一个异性朋友。同时，因为FC主要面对男性读者，所以需要让女主角倒追男主角，所以需要给男主角安排一个女配角作女朋友。此外，叶子和雅彦的情感以谎言开始（叶子为进入若苗家见空，所以谎称自己喜欢雅彦），似乎注定这段感情不会有结果。


Vol02	CH012	page115		紫苑的不道德交际!？  	Shion Courtesan!?  		
Vol02	CH013	page141		银幕上的紫苑  	The Girl On The Screen    
CH012开始拍电影的话题。

我猜，因为雅彦性格中庸--对什么都没有特别大的兴致--所以他入学后没有主动加入影研社。所以，需要迫使他加入影研社。所以，让影研社因美貌而找上紫苑；为了保护紫苑，所以雅彦跟随影研社拍摄；最后被要挟加入影研社。

		
Vol02	CH014	page169		充满回忆的同学会  	A Day To Remember Another Life  		
若苗夫妇的婚姻观（如何维持婚姻）。  


Vol03	CH015	page003		女星的诞生!？  	A Star Is Born!?  		
因影研社的要求，雅彦开始异装。开启FC里异装的话题。


Vol03	CH016	page031		扮女人的测试  	Transvestite Apprentice  		
Vol03	CH017	page059		追击  	Repetition  		
叶子两次看到雅彦异装。两人的关系降至冰点。紫苑如何对待"雅彦和叶子的关系"。  

我猜，作者想把雅美画成一个优雅的女生；所以，需要让雅彦避免异装时各种不雅举止；所以安排人指导他。  


Vol03	CH018	page087		意想不到的助手  	An Unexpected Collaborator  		
雅美有了第一个追随者----辰巳登场。  

Vol03	CH019	page113		辰已，纯情的爱…!?  	Tatsumi's Unwavering Love  		  
雅彦折返学校为辰巳送伞展现了他的性格特点。  


Vol03	CH020	page141		阔别了18年的妹妹  	A Little Sister Not Seen For Over 18 Years  
开启故事线：CH020 - CH024。 
讲的是某些人婚姻的痛苦、无奈，以及为婚姻自由而斗争。还有：  
- 空和父亲之间的亲情。  
- 空的成长历程（小学、中学、校服风波）  （CH021）

Vol03	CH021	page169		父与"女"!  	The Father And The "daughter"!  		
Vol04	CH022	page003		顺子的过文定日子  	An Engagement Gift For Yoriko  		  
Vol04	CH023	page029		冤枉路  	Turnabout  		  
Vol04	CH024	page057		爸爸倒下了？  	Father Has Fainted?  		  


Vol04	CH025	page085		谁是鬼魂？  	Where's The Ghost?  		  
- 江岛在海边旅馆打工，雅彦、紫苑和紫来此度假。江岛夜袭失败。  
- 雅彦和紫苑（在礁石后面）的第二次亲密接触。


Vol04	CH026	page113		两位观众  	Two Spectators  		  
雅彦和江岛的影片在学校放映。叶子原谅雅彦，两人关系回温。雅彦却被辰巳虏走。


Vol04	CH027	page141		把雅彦还给我！  	Rescue Masahiko!!  		  
紫苑、叶子、影研社一行赶去解救被辰巳虏走的雅彦。在卫生间独处时，叶子对雅彦表白（04_160_0）：“你对于我来说...是个比朋友更重要的人！！你是我生命中最特别的男人！！“，以及她对雅彦的要求：“所以，我不想你再作那种不伦不类的打扮！！”


Vol04	CH028	page169		19岁的生辰  	His Nineteenth Birthday  		  
- 庆祝雅彦19岁生日。叶子对雅彦的关系开始恢复（04_191）。
- 更多是在讲 空 和 紫 刚刚开始一起生活(19岁前后)的故事。


Vol05	CH029	page003		扮女人比赛  	Crossdressing Competition  		  
紫苑的高中周日举办开放日（05_004_1）。


Vol05	CH030	page031		亡命驾驶  	Maniac At The Wheel  		  
紫驾车的故事


Vol05	CH031	page059		你喜欢妇科吗？  	Gynaecology? You're Kidding Me!  		
- 空得阑尾炎却不拖稿。表现了空的倔强和敬业。  
- 紫对空的关心（若苗夫妇的二人关系）。  
  

Vol05	CH032	page086		各人的平安夜  	A Xmas Like The Others  		  
雅彦曾经的平安夜。


Vol05	CH033	page113		紫苑的初恋  	Shion's First Love  		  
紫苑在情感上的创伤。

（紫苑集美丽、智力和体力于一身，几乎没有弱点或缺点。FC的主题是家庭，包括个人成长。那么，给紫苑设定一个困难，并最终让她克服这个困难。这样会比较吸引人。）

Vol05	CH034	page141		叶子的求爱！！  	Yoko Makes Her Move!!  		  
叶子和雅彦的关系进一步升温。


Vol05	CH035	page169		拜祭母亲  	At Mother's Graveside  		  
- 回忆：小雅彦的初恋是小紫苑。
- 紫 和父母的矛盾。紫 和姐姐（雅彦的妈妈）的亲情。姐姐支持并帮助 紫 入读女子高中（05_179_5）。


Vol06	CH036	page003		雅彦的独立作战  	Masahiko's Plan For Independence  		  
开启故事线：CH036 - CH039。叶子希望雅彦兼职，为的是让他能离开若苗家。为CH039做铺垫。

Vol06	CH037	page031		危险的兼职  	A Dangerous Job  		  
Vol06	CH038	page057		纯情葵  	The Feelings Of Aoï  		  
Vol06	CH039	page085		Family-若苗  	My Family The Wakanaes  		  
受外因（被女友要求独立）影响，雅彦是否会离开若苗家。(CH003讲的是内因，CH039、CH043讲的是外因)  


Vol06	CH040	page111		憧憬的婚纱  	Wanted  		  
子女对父母的关心(雅彦和紫苑为母亲实现婚纱照的愿望)。


Vol06	CH041	page137		同性恋师妹-茜  	Akane's Idol  		  
雅美被女生喜欢的故事。


Vol06	CH042	page165		秘密赴东京  	The Secret Trip  		  
开启故事线：CH042 - CH044  

Vol07	CH043	page003		愿当养子  	Become My Son!  		  
Vol07	CH044	page031		婚礼的秘密对策  	A Super Secret Plan  		  
受外因（被爷爷收为养子）影响，雅彦是否会离开若苗家。(CH003讲的是内因，CH039、CH043讲的是外因)


Vol07	CH045	page061		钟乳洞内的危险幽香  	The Smell Of Danger From Limestone Cave  
雅彦和紫苑的亲密接触。

Vol07	CH046	page089		恶梦般的露天温泉  	The Nightmare Otherdoor Bath  		  
发现雅彦是男生后，齐藤茜转而喜欢紫苑。  
（我猜，让齐藤茜抛弃雅彦，进而让齐藤茜远离影研社；目的是为了之后紫苑男装加入影研社后不被人认出。）


Vol07	CH047	page115		两个雅彦  	The Two Masahikos  		  
开启故事线： CH047 - CH052，从熏登场，直到熏融入若苗家。期间逐渐展开真朱家(熏、辰巳、早纪)的故事。  
（从CH047至CH050，讲的是真朱家的亲子关系、家庭教育等故事。如果说若苗家代表了家庭里积极的一面，那么真朱家就代表了家庭里消极的一面。北条司最初想把FC的主角紫苑设计成熏这样的不良少年，可见作者非常想讲这样的故事。）

Vol07	CH048	page143		"雅彦"的真正身份  	The Identity Of Masahiko  		  
熏对父母的看法(07_158，07_159，07_164), 熏对女人的看法(07_166_0,07_166_1, 07_166_2),雅彦对辰巳家教方法的建议（07_183）。  

Vol07	CH049	page171		两父女  	Father And Child  		  
Vol08	CH050	page003		若苗家的新住客  	A New Member Of The Wakanae Family  
熏入住若苗家，开启被治愈、被教育的旅程。

Vol08	CH051	page031		薰的诡计  	Kaoru's Stratagem  		  
辰巳同意"熏入住若苗家"。

Vol08	CH052	page059		迈出男人的第一步  	The First Step As A Man  		  
熏如何融入若苗家。  
（熏是不良少年，所以她如何融入若苗家就会是个问题，例如，CH050里紫苑不欢迎熏入住。作者的思路是，让若苗家帮助熏解决困难(帮其摆脱昔日的不良玩伴)，以此来消除相互间的隔阂。）


Vol08	CH053	page087		伪装家庭  	Camouflaging As A Family  		  
助手和子的家庭故事。  

Vol08	CH054	page115		工作中的爸爸  	Papa's Work  		  
横田进为和子(的孩子)而委屈自己穿男装。（助手们之间的友谊和关心）  


Vol08	CH055	page143		叶子的献身大作战计划  	Yoko's "first Time" Plan  		  
开启故事线：CH055 - CH057。雅彦和叶子关系起伏。

Vol08	CH056	page171		叶子大变身  	Yoko's Transformation  		  
为了雅彦，叶子甘愿异装。  
Vol09	CH057	page003		二人世界的生日  	Their Birthday  		  


Vol09	CH058	page031		网上情人  	Mail Lover  	
浩美 网恋的故事。

Vol09	CH059	page059		战胜劲敌  	Victory Pitch  		  
紫苑 被人(松下敏史)暗恋的故事。


Vol09	CH060	page085		辰已的心愿  	Tatsumi's Wish  		  
辰巳 渴望家庭。

Vol09	CH061	page111		辰已的表白  	Tatsumi's Confession  		  
紫 面对第三者(辰巳)的故事。(CH061和CH063分别反映了若苗夫妇如何面对第三者的诱惑)


Vol09	CH062	page139		母亲的印象  	Mother's Image  		
早纪 登场。

Vol09	CH063	page167		早纪的诱惑  	Saki's Temptation  
空 面对第三者(早纪)的故事。(CH061和CH063分别反映了若苗夫妇如何面对第三者的诱惑)


Vol10	CH064	page003		薰的表白  	Kaoru's Confession  		  
辰巳 愿意做 熏 的父亲。

Vol10	CH065	page031		亲情的价值  	The Price Of Kinship  		  
辰巳 为了 熏，愿意倾家荡产并借款。熏和辰巳的关系开始缓和。


Vol10	CH066	page059		紫苑的第一志愿  	Shion's First Choice  		  
紫苑 决定报考武蔵野产业大学。(紫苑 推进二人关系)

Vol10	CH067	page087		二人的取材之旅  	Their Material Gathering Journey  		  
空 对婚姻的忠诚。

Vol10	CH068	page115		雅美的身价  	Masami's Values  		  
公开雅美的真实身份。为 仁科 出场做铺垫。

Vol10	CH069	page143		恐怖的跟踪者  	The Frightening Stalker  		  
雅彦 被男生(仁科)追求。


Vol10	CH070	page171		紫苑的诞生  	Shion's Birth  		  
空 为 紫 的梦想（有小孩）而推迟自己的梦想（漫画连载）。(若苗夫妇的相互支持，甚至是牺牲自己的利益)


Vol11	CH071	page003		约会的替身  	Substitute Date  		  
- 叶子 酒喝多了，在盐谷面前诉苦、抱怨雅彦。紫苑 比 叶子 更懂 雅彦。
- 叶子 和 雅彦 关系好转。


Vol11	CH072	page031		各人的圣诞节  	Separate Christmas  		  
Vol11	CH073	page061		阴错阳差的平安夜  	An Eve Of Fated Encounters  		  
叶子 认为被 雅彦 抛弃。叶子和雅彦之间没有足够的默契（信任）。熏 和 辰巳 的关系好转。结尾处，辰巳感冒打喷嚏，为下一话做铺垫。

Vol11	CH074	page089		物以类聚  	It's All The Same  		  
早纪最终和辰巳和好。早纪的戏结束。结尾处，辰巳和早纪感冒，为下一话做铺垫。

Vol11	CH075	page117		紫苑的入学试  	Shion's Admission Exams  		  
紫苑带病坚持考试。雅彦对紫苑说(11_141_4)：“其实我是希望你和我念同一所大学”。(雅彦 推进二人关系)



Vol11	CH076	page145		紫苑的毕业旅行  	Shion's Graduation Trip  		  
开启故事线：CH076 - CH079  

Vol11	CH077	page173		紫苑和雅彦共渡一夜  	Shion And Masahiko's Night Together  
紫苑 推进二人关系。

Vol12	CH078	page003		出外靠旅伴…！  	Dragged Along On The Trip!  		  
(美菜带来的)男生们对紫苑等女生有不良企图(反派人物出现，剧情向高潮推进)。

Vol12	CH079	page031		雅彦孤军作战！  	Masahiko Fighting Alone!!  		  
遇到困难、且没有紫苑的帮助时，雅彦如何独自解决问题。事后，紫苑为自己的固执道歉，并赞扬雅彦。（CH079和CH092分别讲述，两人在没有对方帮助时，如何解决困难）  

(生活中夫妻二人总有无法帮助对方的情况，本话就是讲紫苑和雅彦的这种故事。)


Vol12	CH080	page059		充满神秘的成人节！  	The Mysterious Coming-Of-Age Day  
雅彦的成人礼（分别有江岛和紫苑两个版本）

Vol12	CH081	page087		是男是女？  	Man Or Woman!?  	
紫苑入学典礼。父母对紫苑的爱和困惑。紫苑体谅父母。（若苗家的家教、亲子关系）

Vol12	CH082	page115		加入学会攻防战  	Club Recruiting Battle  		  
雅彦虽不愿意紫苑加入影研社，但看到紫苑的认真，便在众人面前支持紫苑加入。（雅彦 支持紫苑的梦想）


Vol12	CH083	page143		脚如嘴巴般道出真相  	The Foot Speaks As Well As The Mouth...   
紫苑对影研社的看法、对自己前途的规划、报考武产大的原因、报考英文系的原因。《足》展现出紫苑的天赋。


Vol12	CH084	page171		薰的前途  	Kaoru's Path  		  
开启故事线：CH084 - CH087  

Vol13	CH085	page003		失乐园  	Lost Paradise  		  
Vol13	CH086	page031		父母心  	Parental Feelings  		  
辰巳过度帮助熏，导致失乐园乐队解散

Vol13	CH087	page059		孩子要独立  	Cutting The Umbilical Cord  		  
辰巳放手让熏自立。熏不再逃避。


Vol13	CH088	page087		似近亦远的东京  	Tokyo, So Near And Yet So Far  		  
开启故事线：CH088 - CH091。叶子从小就想看看外面的世界，并梦想成为漫画家。

Vol13	CH089	page115		母与女  	Mother And Daughter  		  
回忆叶子小时候的、以及和叶子母之间的事

Vol13	CH090	page141		冲突  	Clash  		  
叶子和母亲的冲突。

Vol13	CH091	page169		只做一天老板娘  	Okami For A Day  		  
叶子关心、体谅母亲。


Vol14	CH092	page003		恐怖的迎新联欢会  	Scary Meeting  		  
- 紫苑不畏(江岛的)霸凌。
- 遇到困难并且没有雅彦的帮助时，紫苑是如何独自解决问题。（CH079和CH092分别讲述，两人在没有对方帮助时，如何解决困难）


Vol14	CH093	page031		再会  	Reunion  		  
浅葱登场，表白紫苑。开启故事线: CH093 - CH100。（紫苑最终拒绝浅葱，就是选择自己成为女生、选择雅彦。）

Vol14	CH094	page057		浅葱到访  	Asagi's Visit  		  
Vol14	CH095	page083		浅葱突击行动  	Asagi's Plan  		  
CH094、CH095讲的是：浅葱到访若苗家，推进她和紫苑的关系。

Vol14	CH096	page109		哪方面较好呢？  	It Doesn't Matter?!  		  
- 浅葱讲述自己的故事。无法吻女装的紫苑（为后续(CH100)情节反转做铺垫）  
- 浅葱决定出演影研社的电影。  
- 浅葱喜欢紫苑，无论紫苑是男是女--爱情可以超越性别。（浅葱和雅彦形成对比--雅彦只喜欢女装的紫苑）  

Vol14	CH097	page137		一天的情人  	Couple For A Night  		  
雅彦和紫苑的默契。预示着两人在情感路上经受住了考验。（紫苑 和 雅彦 推动二人关系）


Vol14	CH098	page163		雅彦的剧本  	Masahiko's Scenario  		  
影研社决定采用雅彦的剧本拍摄。

Vol14	CH099	page189		改变剧情  	Change Of Course  		  
- 雅彦和紫苑讨论剧情，也是讨论现实中二人的矛盾。  
- 叶子得不到雅彦的支持，二人关系破裂，雅彦没有追回叶子。

Vol14	CH100	page215		浅葱的反击  	Asagi's Retaliation  		  
戏中，浅葱吻了雅美，紫苑的转身逃跑震惊了雅彦。雅彦决定去和叶子分手。

Vol14	CH101	page243		奥多摩的分手  	Split At Okutama  		  
雅彦和叶子分手。雅彦意识到自己真正喜欢的是紫苑。（雅彦 推动二人关系）

Vol14	CH102	page269		家庭(最终话)  	Last Day - End  		  
雅彦表白紫苑。紫苑表示要做女生。

```

--------------------------------------------------

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


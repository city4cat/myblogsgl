

"FC"是北条司作品"Family Compo"的缩写，该作品的名称有: ファミリーコ ンポ / F.COMPO / Family Compo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  

- 说明：  
    - 类似03_143这样的字符串表示"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似03_143_7这样的字符串表示"卷-页-分格(panel)/分镜"编号。xx_yyy_z表示第xx卷、第yyy页、第z分格(panel)/分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  


**FC每话剧情与"Pixar的故事讲述规则"的对照**  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96859))  


- **Vol01	CH001   page003		"家庭"的初体验  	First "family" Life Experience**  		  

    - FC的每一话约27页，但这第一话有40页。3～12页介绍雅彦的背景；13～18页在去若苗家的路上；19～42页在若苗家的故事，抛出第一个大情节：若苗夫妇的性别反转。  
    
    - FC开篇(3～12页)，雅彦在骑车去往若苗家的路上，看到欢乐的一家三口后，很羡慕，并沉浸其中。被列车的鸣笛打断后，雅彦回忆起当天若苗紫到访柳叶家的事。
    
    - FC的开篇非常悲伤，我猜，这可能由以下因素促成：
        - 雅彦现有的一个缺陷：缺乏家庭的温暖。把该缺陷推向极端：  
            - 雅彦失去家庭(母亲早已去世，父亲最近去世), 举目无亲。  
            - 雅彦梦想有一个(完整的)家庭。（如果雅彦不留恋家庭的话，这个创伤会小一些；但雅彦反倒是一个非常渴望家庭的人。下文猜测作者如何让雅彦变得渴望家庭。）  
            - 父亲留给雅彦的遗产几乎为零。需要自己谋生，正在找工作。  
            - 雅彦即将上大学（可能辍学）。      
            - 不相信好事(紫提出的收养和供自己念大学)会发生在自己身上，这说明雅彦心理已经有了变化(自卑？)。  
            
        - 此外，我猜，作者在漫画里还使用以下手法制造悲伤：    
            - 雅彦路遇欢乐的一家三口。（雅彦的凄惨与他们的欢乐形成对比）    
            - (01_004_0)一缕青烟在父亲的肖像前飘过，有种"父亲肖像破裂"的错觉。对于父亲的照片来说，该镜头是"近距离镜头(Close Up(C.U))"，可能是让读者感受照片里父亲的微笑，而这微笑又与父亲的去世形成情感上的反差，让人悲伤。  
            - 雅彦的住处零乱。（可能以此表现出他被受打击、没有斗志面对生活） 
            - 雅彦9个悲伤的镜头： 01_004_3, 01_009_0, 01_009_6, 01_010_1, 01_011_0, 01_012_1 ~ 01_012_4.  
            - 在柳叶家，雅彦和若苗紫谈话时:  
                - 有3个俯视镜头(01_008_4, 01_009_2, 01_012_1)，给人压抑。
                - 有2个镜头(01_011_3, 01_012_4)背景用了大量竖线表现阴影，表示压抑和悲伤。
                - 拒绝了若苗紫的好意，雅彦内心自责：我在说谎呢！  
                - 若苗紫被拒绝后，也很失落。  

    - 如何让雅彦变得渴望家庭？  
        我猜，这需要让他成长在一个破碎的家庭里；因此，需要失去一个亲人。又因为，一般来说，孩子对母亲的依恋多过于父亲；所以，选择让雅彦的母亲先去世。最终，在雅彦约7岁时(小学2年级)，其母亲病逝；在单亲家庭里，他逐渐变得渴望一个完整的家。




- **Vol01	CH002	page043		天体之夜  	A Torrid Night**  		  


- **Vol01	CH003	page073		温暖的饭  	A Hot Meal**  		

    - 01_082, 01_083, 这两页很悲伤。  
        - 01_082, 雅彦："冷饭盒味道更好。。。因为已经习惯了。。。但是，从和是开始的呢！？令我养成吃冷饭习惯的。。。是因为----"。接下来是过渡到回忆的镜头(01_082_2)。
            - 回忆里，父亲的镜头没有显示表情(01_082_4,01_082_5,01_083_0)，这可能是因为父子交流少、记忆淡漠；也可能是为了渲染父亲的冷漠。父亲的台词也有些冷漠，和小雅彦的热情放形成反差，制造悲伤的情绪：  
                雅彦："呀！已冷掉了呢！爸爸"(01_082_3)  
                爸爸："什么？雅彦仍未吃晚饭吗？"(01_082_4)  
                雅彦："嗯，我想跟爸爸一起吃。"(01_082_4)  
                爸爸："爸爸说过要晚点回来，叫你趁热吃的吧？"(01_082_5)  
                雅彦："...嗯...."  
                雅彦："爸爸从今天起要经常加班。。。。会在公司吃过饭才回来，你也不用煮二人分量的了。。。"(01_083_0)  
                雅彦："...嗯。"(01_083_0)  
                雅彦：变成只一个人吃饭时，便觉得煮一人分量的饭太麻烦了。。。不知不觉就时常光顾便利店的饭盒。。。  
            - 01_083_0，01_083_1这两个镜头形成对比：道具(雅彦的服饰、背景里的日历、桌上的盒饭、饮料、调料)的差异暗示岁月流逝、物是人非；父亲在镜头里消失，可能指父爱缺失。  
        - 01_083，回忆母亲。先回忆父亲、后回忆母亲；这和母亲先去世、父亲后去世的人物顺序不一致。我猜，可能是一般人对母亲的感情更深，所以把回忆母亲放在后面，利于进一步制造悲伤的情绪。
            - 这里，用电饭煲衔接镜头序列：  
                雅彦：家里的电饭煲便变得没用了...后来，更只成为厨房中的饰品。  
                雅彦：妈妈在的时候，也吃过用它烧出来的熟饭...现在...已记不起来了。。。  
                前两个镜头(01_083_2,01_083_3)：电饭煲渐渐远离镜头，可能在暗示亲情越来越远。  
                后两个镜头(01_083_4,01_083_5)：雅彦动作类似，所以是借他的动作来衔接镜头。因为提到母亲、并站在电饭煲的视角，所以可能是在用电饭煲代指母亲；最后的镜头里电饭煲消失，指母亲的离世。雅彦手部的动作变得松懈，表示雅彦对母亲的离世感到悲伤而无奈。独白“现在已记不起来了”可能是指记不得母亲的样子了  


- **Vol01	CH004	page099		开学礼的条件  	Entry Ceremony**  

  		  
- **Vol01	CH005	page125		棒球的回忆  	The Glove Of Memories**  		


- **Vol01	CH006	page153		在远处遥望的初恋  	A First Discrete Love**  		


- **Vol01	CH007	page179		助一臂之力  	Just A Little Service**  		


- **Vol02	CH008	page003		母亲的回忆  	Mother's Gift**  

    - 开端(主事件1)：   
        杯子的来历：妈妈送给雅彦唯一的(圣诞)礼物(02_005_4)。雅彦的欲望：保留完好的杯子。   
    - 经过-主事件2(剧情点1)：紫苑把杯柄弄掉了，杯子被丢弃。    
    - 经过-主事件3(剧情点2)：雅彦着急地找杯子，最终找到了杯柄和脏兮兮的杯子。
        - 注：让雅彦走出“舒适区”。    
    - 经过-主事件4(剧情点3)：杯柄被粘好。但杯柄的裂痕还是让雅彦有些伤心。
        - 注：让雅彦走出“舒适区”。雅彦的欲望促使粘好杯柄。    
    - 高潮(最终主事件)：杯子被摔碎。雅彦拼补杯子碎片，失败，想砸碎杯子，但又不忍心。    
        - 注：让雅彦走出“舒适区”。雅彦的欲望促使粘好杯子碎片，但失败(最后雅彦应该是放弃了)。
        - 注：02_014, 02_015, 02_016. 杯子摔向地板的过程中，雅彦回忆起妈妈送杯子的情景。温馨的回忆和杯子摔破形成对比，表现出(也让读者感受到)雅彦内心强烈的冲突（因为美好的回忆，所以想要保留杯子，但现在却要摔碎了，这意味着美好的会议也随之而去了）。
    - 第二高潮：雅彦回忆母亲教如何使用筷子。紫安抚雅彦。紫苑送雅彦杯子。
        - 注：雅彦回忆自己学使用筷子的情景(02_024, 02_025, 02_026),紫：“这些(习惯)都遗留在你的身体里，永远不会消失”。这安抚了雅彦的伤痛、重建了雅彦对母亲的怀念。雅彦承认今天的这些事都是美好的回忆(02_029_4)。      
    - 结局：第二天，紫和空也送了雅彦2个杯子(02_029_5)。    

  		
Vol02	CH009	page031		家庭旅行!！  	Family Trip!  		
Vol02	CH010	page059		不请自来的助手  	The Stubborn Assistant  		  
Vol02	CH011	page087		仰慕的人  	The One I Admire  		  
Vol02	CH012	page115		紫苑的不道德交际!？  	Shion Courtesan!?  		
Vol02	CH013	page141		银幕上的紫苑  	The Girl On The Screen  		
Vol02	CH014	page169		充满回忆的同学会  	A Day To Remember Another Life  		
Vol03	CH015	page003		女星的诞生!？  	A Star Is Born!?  		
Vol03	CH016	page031		扮女人的测试  	Transvestite Apprentice  		
Vol03	CH017	page059		追击  	Repetition  		
Vol03	CH018	page087		意想不到的助手  	An Unexpected Collaborator  		
Vol03	CH019	page113		辰已，纯情的爱…!?  	Tatsumi's Unwavering Love  		  
Vol03	CH020	page141		阔别了18年的妹妹  	A Little Sister Not Seen For Over 18 Years  
Vol03	CH021	page169		父与"女"!  	The Father And The "daughter"!  		
Vol04	CH022	page003		顺子的过文定日子  	An Engagement Gift For Yoriko  		  
Vol04	CH023	page029		冤枉路  	Turnabout  		  
Vol04	CH024	page057		爸爸倒下了？  	Father Has Fainted?  		  
Vol04	CH025	page085		谁是鬼魂？  	Where's The Ghost?  		  
Vol04	CH026	page113		两位观众  	Two Spectators  		  
Vol04	CH027	page141		把雅彦还给我！  	Rescue Masahiko!!  		  
Vol04	CH028	page169		19岁的生辰  	His Nineteenth Birthday  		  
Vol05	CH029	page003		扮女人比赛  	Crossdressing Competition  		  
Vol05	CH030	page031		亡命驾驶  	Maniac At The Wheel  		  
Vol05	CH031	page059		你喜欢妇科吗？  	Gynaecology? You're Kidding Me!  		  
Vol05	CH032	page086		各人的平安夜  	A Xmas Like The Others  		  
Vol05	CH033	page113		紫苑的初恋  	Shion's First Love  		  
Vol05	CH034	page141		叶子的求爱!！  	Yoko Makes Her Move!!  		  
Vol05	CH035	page169		拜祭母亲  	At Mother's Graveside  		  
Vol06	CH036	page003		雅彦的独立作战  	Masahiko's Plan For Independence  		  
Vol06	CH037	page031		危险的兼职  	A Dangerous Job  		  
Vol06	CH038	page057		纯情葵  	The Feelings Of Aoï  		  
Vol06	CH039	page085		Family-若苗  	My Family The Wakanaes  		  
Vol06	CH040	page111		憧憬的婚纱  	Wanted  		  
Vol06	CH041	page137		同性恋师妹-茜  	Akane's Idol  		  
Vol06	CH042	page165		秘密赴东京  	The Secret Trip  		  
Vol07	CH043	page003		愿当养子  	Become My Son!  		  
Vol07	CH044	page031		婚礼的秘密对策  	A Super Secret Plan  		  
Vol07	CH045	page061		钟乳洞内的危险幽香  	The Smell Of Danger From Limestone Cave  
Vol07	CH046	page089		恶梦般的露天温泉  	The Nightmare Otherdoor Bath  		  
Vol07	CH047	page115		两个雅彦  	The Two Masahikos  		  
Vol07	CH048	page143		"雅彦"的真正身份  	The Identity Of Masahiko  		  
Vol07	CH049	page171		两父女  	Father And Child  		  
Vol08	CH050	page003		若苗家的新住客  	A New Member Of The Wakanae Family  		  
Vol08	CH051	page031		薰的诡计  	Kaoru's Stratagem  		  
Vol08	CH052	page059		迈出男人的第一步  	The First Step As A Man  		  
Vol08	CH053	page087		伪装家庭  	Camouflaging As A Family  		  
Vol08	CH054	page115		工作中的爸爸  	Papa's Work  		  
Vol08	CH055	page143		叶子的献身大作战计划  	Yoko's "first Time" Plan  		  
Vol08	CH056	page171		叶子大变身  	Yoko's Transformation  		  
Vol09	CH057	page003		二人世界的生日  	Their Birthday  		  
Vol09	CH058	page031		网上情人  	Mail Lover  		  
Vol09	CH059	page059		战胜劲敌  	Victory Pitch  		  
Vol09	CH060	page085		辰已的心愿  	Tatsumi's Wish  		  
Vol09	CH061	page111		辰已的表白  	Tatsumi's Confession  		  
Vol09	CH062	page139		母亲的印象  	Mother's Image  		  
Vol09	CH063	page167		早纪的诱惑  	Saki's Temptation  		  
Vol10	CH064	page003		薰的表白  	Kaoru's Confession  		  
Vol10	CH065	page031		亲情的价值  	The Price Of Kinship  		  
Vol10	CH066	page059		紫苑的第一志愿  	Shion's First Choice  		  
Vol10	CH067	page087		二人的取材之旅  	Their Material Gathering Journey  		  
Vol10	CH068	page115		雅美的身价  	Masami's Values  		  
Vol10	CH069	page143		恐怖的跟踪者  	The Frightening Stalker  		  
Vol10	CH070	page171		紫苑的诞生  	Shion's Birth  		  
Vol11	CH071	page003		约会的替身  	Substitute Date  		  
Vol11	CH072	page031		各人的圣诞节  	Separate Christmas  		  
Vol11	CH073	page061		阴错阳差的平安夜  	An Eve Of Fated Encounters  		  
Vol11	CH074	page089		物以类聚  	It's All The Same  		  
Vol11	CH075	page117		紫苑的入学试  	Shion's Admission Exams  		  
Vol11	CH076	page145		紫苑的毕业旅行  	Shion's Graduation Trip  		  
Vol11	CH077	page173		紫苑和雅彦共渡一夜  	Shion And Masahiko's Night Together  		  
Vol12	CH078	page003		出外靠旅伴…！  	Dragged Along On The Trip!  		  
Vol12	CH079	page031		雅彦孤军作战！  	Masahiko Fighting Alone!!  		  
Vol12	CH080	page059		充满神秘的成人节！  	The Mysterious Coming-Of-Age Day  		  
Vol12	CH081	page087		是男是女？  	Man Or Woman!?  		  
Vol12	CH082	page115		加入学会攻防战  	Club Recruiting Battle  		  
Vol12	CH083	page143		脚如嘴巴般道出真相  	The Foot Speaks As Well As The Mouth...  
Vol12	CH084	page171		薰的前途  	Kaoru's Path  		  
Vol13	CH085	page003		失乐园  	Lost Paradise  		  
Vol13	CH086	page031		父母心  	Parental Feelings  		  
Vol13	CH087	page059		孩子要独立  	Cutting The Umbilical Cord  		  
Vol13	CH088	page087		似近亦远的东京  	Tokyo, So Near And Yet So Far  		  
Vol13	CH089	page115		母与女  	Mother And Daughter  		  
Vol13	CH090	page141		冲突  	Clash  		  
Vol13	CH091	page169		只做一天老板娘  	Okami For A Day  		  
Vol14	CH092	page003		恐怖的迎新联欢会  	Scary Meeting  		  
Vol14	CH093	page031		再会  	Reunion  		  
Vol14	CH094	page057		浅葱到访  	Asagi's Visit  		  
Vol14	CH095	page083		浅葱突击行动  	Asagi's Plan  		  
Vol14	CH096	page109		哪方面较好呢？  	It Doesn't Matter?!  		  
Vol14	CH097	page137		一天的情人  	Couple For A Night  		  
Vol14	CH098	page163		雅彦的剧本  	Masahiko's Scenario  		  
Vol14	CH099	page189		改变剧情  	Change Of Course  		  
Vol14	CH100	page215		浅葱的反击  	Asagi's Retaliation  		  
Vol14	CH101	page243		奥多摩的分手  	Split At Okutama  		  
Vol14	CH102	page269		家庭(最终话)  	Last Day - End  		  


**参考资料：**  
[1] Pixar Storytelling, Rules for Effective Storytelling Based on Pixar’s Greatest Films, Dean Movshovitz  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



"CE"是北条司作品"Cat's Eye"的缩写，该作品的其他名称有: キャッツアイ / 貓眼三姐妹(大陸) / 貓之眼(港) / 貓眼(台)  

"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭(大陆) / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 

"AH"是北条司作品"Angel Heart"的缩写，该作品的其他名称有: エンジェルハート / 天使之心(港) / 天使心(台)  


# 北条司作品里某些人物的相貌原型(猜测)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96868))   

[AH Vol21的卷首语提到](../zz_preambles/ah_preambles.md)：  
"最近看了三、四歲時（大概是首次在電影院）看的電影DVD，令我不禁「咦？」的一聲。電影的女主角是我非常喜愛的女性類型，而壞蛋那邊的女角卻是我最討厭的類型（雖然兩人都有漂亮的面孔）"。  

由此，我猜测，北条司作品里的女性角色的相貌是否会类似于这些影星？

为此，搜集了一些日本1950年代～1960年代的电影和影星(北条司3～4岁时，即1962～1963年)。  

先说推测的结论：  
    - 来生泪(CE)、若苗紫(FC)的相貌原型可能是：芦川泉。  
    - 来生瞳(CE)的相貌原型可能是：吉永小百合。  
    - 来生爱(CE)的相貌原型可能是：田代みどり。  
    - AH Vol21的卷首语提到的电影可能是：《青い山脈》(1963)。电影里“壞蛋那邊的女角”的扮演者可能是：南田洋子。  
    
如果这些猜测正确的话，《青い山脈》的这个场景就是猫眼三姐妹的相貌原型的同台了（从左到右：芦川泉、田代みどり、吉永小百合）：  
![](img/Beyond.The.Green.Hills_14.jpg)  


- **芦川泉 / 芦川いづみ / Izumi Ashikawa**  
    ![](img/IzumiAshikawa_00.jpg) 
    ![](img/IzumiAshikawa_01.jpg) 
    <img src="img/IzumiAshikawa_02.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_03.jpg" height="600"></img> 
    <img src="img/IzumiAshikawa_04.jpg" height="600"></img> 
    <img src="img/IzumiAshikawa_04-2.jpg" height="600"></img> 
    <img src="img/IzumiAshikawa_04-3.jpg" height="600"></img> 
    <img src="img/IzumiAshikawa_04-4.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_04-5.jpg" height="600"></img> 
    <img src="img/IzumiAshikawa_04-6.jpg" height="600"></img> 
    <img src="img/IzumiAshikawa_05.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_06.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_07.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_08.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_09.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_10.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_11.jpg" height="300"></img> 
    <img src="img/IzumiAshikawa_12.jpg" height="300"></img> 

   
    - **部分参演电影和剧照**  
        - 青い山脈 / 青色山脉 / 绿色的山脉 / 蓝山 / Aoi sanmyaku / Beyond The Green Hills, (1963)  
            ![](img/Beyond.The.Green.Hills_00.jpg) 
            ![](img/Beyond.The.Green.Hills_01.jpg) 
            ![](img/Beyond.The.Green.Hills_01-2.jpg) 
            ![](img/Beyond.The.Green.Hills_02.jpg) 
            ![](img/Beyond.The.Green.Hills_03.jpg) 
            ![](img/Beyond.The.Green.Hills_04.jpg) 
            ![](img/Beyond.The.Green.Hills_05.jpg) 
            ![](img/Beyond.The.Green.Hills_06.jpg) 
            ![](img/Beyond.The.Green.Hills_07.jpg) 
            ![](img/Beyond.The.Green.Hills_08.jpg) 
            ![](img/Beyond.The.Green.Hills_09.jpg) 
            ![](img/Beyond.The.Green.Hills_10.jpg) 
            ![](img/Beyond.The.Green.Hills_11.jpg) 
            ![](img/Beyond.The.Green.Hills_12.jpg) 
            ![](img/Beyond.The.Green.Hills_13.gif) 
            ![](img/Beyond.The.Green.Hills_14.gif) 

        - 散弾銃の男 / 持散弹枪的男人 / The Man with a Shotgun, (1961)  
            ![](img/The.Man.with.a.Shotgun.1961.01.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.02.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.03.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.04.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.05.jpg) 

    - 更多照片：  
        - [芦川いづみ - JapaneseClass](https://japaneseclass.jp/img/芦川いづみ?)  
        - [芦川いづみ - GenSun](https://gensun.org/?q=芦川いづみ)  
        - [Izumi Ashikawa - GenSun](https://gensun.org/?q=Izumi+Ashikawa)  

    - **参考资料**  
        - [芦川泉 - douban](https://movie.douban.com/celebrity/1030436/)  
        - [芦川泉 - IMDB](https://www.imdb.com/name/nm0038960/)  
        - [芦川泉 - TMDB](https://www.themoviedb.org/person/1063882-izumi-ashikawa)  
        - [青色山脉 - douban](https://movie.douban.com/subject/10355292/)  
        - [青色山脉 - IMDB](https://www.imdb.com/title/tt0439119/)  
        - [青色山脉 - TMDB](https://www.themoviedb.org/movie/360530-aoi-sanmyaku)  
        - [持散弹枪的男人 - douban](https://movie.douban.com/subject/20430022/)  
        - [持散弹枪的男人 - IMDB](https://www.imdb.com/title/tt0055404/)  
        - [持散弹枪的男人 - TMDB](https://www.themoviedb.org/movie/370969-man-with-a-shotgun)  


- **吉永小百合 / Sayuri Yoshinaga**  
    ![](img/SayuriYoshinaga.jpg) 
    ![](img/SayuriYoshinaga_00.jpg) 
    ![](img/SayuriYoshinaga_01.jpg) 
    ![](img/SayuriYoshinaga_02.jpg) 
    ![](img/SayuriYoshinaga_03.jpg) 
    ![](img/SayuriYoshinaga_04.jpg) 
    ![](img/SayuriYoshinaga_05.jpg) 

    - **部分参演电影和剧照**  
        - 青い山脈 / 青色山脉 / 绿色的山脉 / 蓝山 / Aoi sanmyaku / Beyond The Green Hills, (1963)  
            ![](img/Beyond.The.Green.Hills_05.jpg) 
            ![](img/Beyond.The.Green.Hills_06.jpg) 
            ![](img/Beyond.The.Green.Hills_10.jpg) 
            ![](img/Beyond.The.Green.Hills_13.jpg) 
            ![](img/Beyond.The.Green.Hills_14.jpg) 

    - 更多照片：  
        - [吉永小百合 - GenSun](https://gensun.org/?q=吉永小百合)  
        - [Sayuri Yoshinaga - GenSun](https://gensun.org/?q=Sayuri%20Yoshinaga)  

    - **参考资料**  
        - [吉永小百合 - douban](https://movie.douban.com/celebrity/1033094/)  
        - [吉永小百合 - IMDB](https://www.imdb.com/name/nm0949045/)  
        - [吉永小百合 - TMDB](https://www.themoviedb.org/person/1006012-sayuri-yoshinaga)  
        - [青色山脉 - douban](https://movie.douban.com/subject/10355292/)  
        - [青色山脉 - IMDB](https://www.imdb.com/title/tt0439119/)  
        - [青色山脉 - TMDB](https://www.themoviedb.org/movie/360530-aoi-sanmyaku)  




- **田代みどり / たしろ みどり / Midori Tashiro**  
    ![](img/MidoriTashiro_00.jpg) 
    ![](img/MidoriTashiro_01.jpg) 
    ![](img/MidoriTashiro_02.jpg) 

    - **部分参演电影和剧照**  
        - 青い山脈 / 青色山脉 / 绿色的山脉 / 蓝山 / Aoi sanmyaku / Beyond The Green Hills, (1963)  
            ![](img/Beyond.The.Green.Hills_13.jpg) 
            ![](img/Beyond.The.Green.Hills_14.jpg) 
            ![](img/Beyond.The.Green.Hills_15.jpg) 
            ![](img/Beyond.The.Green.Hills_16.jpg) 
            ![](img/Beyond.The.Green.Hills_17.jpg) 
            ![](img/Beyond.The.Green.Hills_18.jpg) 

    - 更多照片：  
        - [田代みどり - JapaneseClass](https://japaneseclass.jp/img/田代みどり?)  
        - [田代みどり - GenSun](https://gensun.org/?q=田代みどり)  
        - [Midori Tashiro - GenSun](https://gensun.org/?q=Midori%20Tashiro)  

    - **参考资料**  
        - [田代みどり - douban](https://ja.wikipedia.org/wiki/田代みどり)  
        - [田代みどり - IMDB](https://www.imdb.com/name/nm0038972/)  
        - [田代みどり - TMDB](https://www.themoviedb.org/person/1666258-midori-tashiro)  
        - [Midori Tashiro](https://secondhandsongs.com/artist/118414)
        - [青色山脉 - douban](https://movie.douban.com/subject/10355292/)  
        - [青色山脉 - IMDB](https://www.imdb.com/title/tt0439119/)  
        - [青色山脉 - TMDB](https://www.themoviedb.org/movie/360530-aoi-sanmyaku)  





- **南田洋子 / Yôko Minamida**  
    ![](img/YokoMinamida_00.jpg) 

    - **部分参演电影和剧照**  
        - 青い山脈 / 青色山脉 / 绿色的山脉 / 蓝山 / Aoi sanmyaku / Beyond The Green Hills, (1963)  
            ![](img/Beyond.The.Green.Hills_21.jpg) 
            ![](img/Beyond.The.Green.Hills_20.jpg) 
            ![](img/Beyond.The.Green.Hills_19.jpg) 

        - 散弾銃の男 / 持散弹枪的男人 / The Man with a Shotgun, (1961)  
            ![](img/The.Man.with.a.Shotgun.1961.06.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.07.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.08.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.09.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.10.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.11.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.12.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.13.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.14.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.15.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.16.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.17.jpg) 
            ![](img/The.Man.with.a.Shotgun.1961.18.jpg) 

    - 更多照片：  
        - [南田洋子 - JapaneseClass](https://japaneseclass.jp/img/南田洋子?)  
        - [南田洋子 - GenSun](https://gensun.org/?q=南田洋子)  
 
    - **参考资料**  
        - [南田洋子 - douban](https://movie.douban.com/celebrity/1033711/)  
        - [Yôko Minamida - IMDB](https://www.imdb.com/name/nm0590940/)  
        - [Yôko Minamida - TMDB](https://www.themoviedb.org/person/132767-y-ko-minamida)  
        - [青色山脉 - douban](https://movie.douban.com/subject/10355292/)  
        - [青色山脉 - IMDB](https://www.imdb.com/title/tt0439119/)  
        - [青色山脉 - TMDB](https://www.themoviedb.org/movie/360530-aoi-sanmyaku)  



- **高峰秀子 / Hideko Takamine**  


- **和泉雅子 / Masako Izumi**  
https://movie.douban.com/celebrity/1026138/photo/2425654452  
这张图和FC里紫的一张图动作类似。当然这个动作很常见。


- **南野洋子，Laura，小泉今日子**  
Twitter上有人说南野洋子和Laura(劳拉)像泪，小泉今日子（こいずみ きょうこ/Kyoko Koizumi）像爱。  
[https://twitter.com/yae_ch3/status/1527642080574849026](https://twitter.com/yae_ch3/status/1527642080574849026)  
20代の私の目が見るに、  
泪：南野陽子×ローラ  
瞳：工藤静香  
愛：小泉今日子  
だと思ってるんだけど似てません？  
(瞳ちゃんもっと似てる人いるかな…)  
(正如我在二十多岁时看到的，  
泪：南野洋子xLaura  
瞳：工藤静香  
爱：小泉今日子  
我认为她们相似，你觉得呢？  
(有没有人长得更像瞳...))  
[https://twitter.com/yae_ch3/status/1527643879264391169](https://twitter.com/yae_ch3/status/1527643879264391169)  
ローラの骨格がもろ泪さんだし、昔のキョンキョンが愛ちゃんすぎるんだよなぁ来生三姉妹リアルにいたらえらいべっぴんさんだよ  
（Laura的骨架完全像泪，曾经的キョンキョン太像小爱了。来生三姐妹在现实生活中会非常漂亮。）  
    - 南野陽子(みなみの ようこ / Minamino Yoko)：  
    ![](img/rui_MinaminoYoko01.jpg) 
    ![](img/rui_MinaminoYoko02.jpg) （[大图](https://thetv.jp/i/tl/000/0000/0000000603_r.jpg)）  
    
    - Laura:  
    ![](img/rui_Laura.jpg) （[大图](https://pbs.twimg.com/media/FTNIAsnagAAyYtj?format=jpg)）  
    
    - 小泉今日子(こいずみ きょうこ/Kyoko Koizumi):  
    ![](img/ai_KyokoKoizumi.jpg) （[大图](https://pbs.twimg.com/media/FTNIAsoaAAAlfH6?format=jpg)）  
    

待整理：  
[关于昭和女星的更多信息](https://space.bilibili.com/29246903/article)：  
[【2021年11月视频高清化记录】昭和群星篇 ](https://www.bilibili.com/read/cv14179170?spm_id_from=333.999.0.0)  
[【2020年终总结】昭和偶像篇 ](https://www.bilibili.com/read/cv9278027?spm_id_from=333.999.0.0)  
[ 【不作の83年組】昭和史上最弱的一年新人们 ](https://www.bilibili.com/read/cv5099121?spm_id_from=333.999.0.0)  
[【2019总结之昭和偶像】 ](https://www.bilibili.com/read/cv4286284?spm_id_from=333.999.0.0)
[ 【中森明菜】时代的歌姬 歌姬的时代 ](https://www.bilibili.com/read/cv4158497?spm_id_from=333.999.0.0)  
[【昭和老资料查找方法大全】 ](https://www.bilibili.com/read/cv3415990?spm_id_from=333.999.0.0)  
[【当现代遇上后现代】SUPREME与不思議 - 松田聖子与中森明菜 ](https://www.bilibili.com/read/cv2963809?spm_id_from=333.999.0.0)  
[【令和天皇的偶像】柏原芳恵 ](https://www.bilibili.com/read/cv2629550?spm_id_from=333.999.0.0)  
[【花の82年組代表人物介绍③】早見優 石川秀美 シブがき隊 ](https://www.bilibili.com/read/cv780525?spm_id_from=333.999.0.0)  
[ 【花の82年組代表人物介绍②】小泉今日子 ](https://www.bilibili.com/read/cv364850?spm_id_from=333.999.0.0)  
[ 【花の82年組代表性人物介绍①】松本伊代 堀ちえみ 三田寛子 ](https://www.bilibili.com/read/cv321739?spm_id_from=333.999.0.0)  
[ 【昭和故事-おニャン子クラブ】小猫俱乐部风云 ](https://www.bilibili.com/read/cv267370?spm_id_from=333.999.0.0)  














--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

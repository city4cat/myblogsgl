# [北条司研究序説](http://chiakik.la.coocan.jp/htkj/xyz.htm)([中文版](./xyz.md))    
"北条司研究序説", Hojo Tsukasa Kenkyu Josetsu, 简称"HTKJ"。这里是一份它的非官方中文版。    

## 总目录  
为方便查阅，整理了文件目录。如有遗漏，请告知我。谢谢！  

[CONTENTS](./index.md)  

- [封面](./xyz.md)  
- [导言（前页）](./start.md)  
- [简介 与北条作品的邂逅](./deai.md)  
- [第1章 About 北条司](./about/abindex.md)  
    - [职业生涯](./about/keireki.md)  
    - [作品列表](./about/list_works.md) 
    - [北条作品的特点和不足之处](./about/e.md) 
        - [迷人的绘画](./about/e.md)  
        - [浪漫主义者・北条司](./about/romantist.md)  
        - [日常生活](./about/nitijyo.md)   
        - [北条作品的关键词](./about/keywords.md)   
        - [北条作品的世界观](./about/worldview.md)   
        - [北条流的Star System？](./about/starsystem.md)  
        - [结局的倾向](./about/ending.md)  
        - [不画丑陋的东西](./about/ugly.md)  
        - [故事情节的弱点](./about/weakstory.md)  
    - [法国的漫画同人杂志](./about/frenchdoujin.md)  
- [第2章 CAT'S EYE](./ce/ceindex.md) 	
    - [Story&Main Charactors](./ce/cestory-charactor.md)  
    - [Episode Guide](./ce/ceepisodes.md)  
        - [Episode (Subtitle) List](./ce/cesubtitles.md)  
        - 剧情简介&评论  
            - [◆#1 性感的火爆女郎 ～ #20 只要10分鐘](./ce/ceepisode1-20.md)  
            - [◆#21 女人心海底針 ～ #40 倒數計時的戀情](./ce/ceepisode21-40.md)  
            - [◆#41 我的貼身保鏢 ～ #60 愛情呀！求你不要走](./ce/ceepisode41-60.md)  
            - [◆#61 別纏上差勁的女子 ～ #80 致命的吸引力](./ce/ceepisode61-80.md)  
            - [◆#81 愛情的力量 ～ #100 淚小姐的情人](./ce/ceepisode81-100.md)  
            - [◆#101 愛的拼圖 ～ #120 飯盒恐懼癥!](./ce/ceepisode101-120.md)  
            - [◆#121 通告信的秘密 ～ #135 再一次戀愛](./ce/ceepisode121-135.md)  
    - [Animation](./ce/ceanime.md)   
        - [作品解説](./ce/ceanimekaisetsu.md)  
        - [Staff List](./ce/ceanimestaff.md)  
        - [与原作的差异](./ce/ceanimechigai.md)  
    - [Another CAT'S EYE](./ce/ceanother.md)   
    - [Impression](./ce/ceimpress.md)   
- [第3章 CITY HUNTER](./ch/chindex.md)  
    - [Story&Main Charactors](./ch/chstory-charactors.md)  
    - [Episode Guide](./ch/chepisodes.md)  
        - [Episode List](./ch/chsubtitles.md)  
        - 剧情简介和评论
            - [◆#1 不光彩的心下! ～  #10 裸足女星](./ch/chepisode1-10.md)  
            - [◆#11 随着钟声的命运! ～ #20 不可碰护士啊!](./ch/chepisode11-20.md)  
            - [◆#21 槇村遗下的东西 ～ #30 危险的一对!](./cch/hepisode21-30.md)  
            - [◆#31 大家姐是女大学生! ～ #40 天空任飞翔](./ch/chepisode31-40.md)  
            - [◆#41 天使遗忘的礼物!? ～ #50冴子的相亲!!](./ch/chepisode41-50.md)  
            - [◆#51 将记忆抹去… ～ #60 FOREVER, CITY HUNTER !!](./ch/chepisode51-60.md)  
        - [关于短篇「City Hunter」](./ch/chyomikiri.md)  
    - [角色&术语集](./ch/chpeople-words.md)  
        - 角色集
            - [あ](./ch/chpeople-a.md) (译注： 赤川会長, 浅香亜美, 浅香麻美, 麻上亜紀子, 浅野麗香, 麻生霞, 麻生弥生, 阿斯巴王子, 亜月菜摘, 天野翔子 , 阿露玛公主, 伊集院隼人, 稲垣, 岩井善美, 岩崎惠, 岩瀬明, 岩館, 上杉貴子, 海坊主, 浦上真由子, 浦上, 雲竜会的会長, 雲竜会的会長的儿子（政弘）, Elan Dayan, 艾力, 大利根, 及川優希（優希・格蕾斯）, 男老板, 小倉经理, ) 
            - [か](./ch/chpeople-ka.md) (译注： 海原神, 片岡優子, 片岡優子的母亲, 鹿島悦子, 柏木圭子（圭一）, 加納典宏, 神村愛子, 神村幸一郎, 川田温子, 北尾裕貴, 喜多川英二, 喜多川直也, 北原絵梨子, 教授, 銀狐, 日下美佐子, 串田, 黒川, 黒蜥蜴, 军士, Kenny Field, 蝙蝠, 伍藤梓, 伍藤広喜, 小林美幸, ) 
            - [さ](./ch/chpeople-sa.md) (译注： 冴羽獠, 佐藤由美子, Salina, Sanchos, General(将军), Shlomo Dayan, 柴田洋一, 神宮寺遙, 神宮寺道彦, 人口贩运集团的老板, 鈴木, Snake, 芹沢綾子, Sonia Field, ) 
            - [た](./ch/chpeople-ta.md) (译注： 崇司, 高塚秀司, 高宮, 泷川护士, 武田季实子, 武田老人, 立木小白合, 橘葉月, 田宮教授, 長老, 次原舞子, 土屋, 手塚明美, 手塚篤, David Clive, 冬野葉子, 毒针鼠, 友村刑事, 寅吉, ) 
            - [な](./ch/chpeople-na.md) (译注：野上警視総監, 野上冴子, 野上唯香, 野上麗香, 名取和江, 西九条紗羅, 西九条定光, ) 
            - [は](./ch/chpeople-ha.md) (译注： 萩尾直行, 萩尾道子, 畠岡, Harano Kreuz, Baron（男爵）, 氷室剛司, 氷室真希, 日向敏夫, 平山希美子, Falcon, 深町, 伊東定雄, 伏見, Bloody Mary, 不律乱寓, 北條, ) 
            - [ま](./ch/chpeople-ma.md) (译注： 牧野陽子, 牧原梢, 牧原友佳, 槇村香, 槇村秀幸, 政一, 真柴憲一郎, 真柴由加里, 真柴小由里, 松岡拓也, 松岡, 松村渚, Mary, 马鲁麦斯大臣, 美樹, Mick Angel, 皆川由貴, 森居和枝, Morton总统, ) 
            - [や](./ch/chpeople-ya_ra.md) (译注： 柳原, 結城礼子, Yuki Grace, 四谷政男, ) 
            - [ら](./ch/chpeople-ya_ra.md#ra)  (译注： 竜, 竜神沙也加, 竜神信男, 罗丝玛丽・梦, ) 
        - 用語集
            - [枪](./ch/chwords-guns.md) 
            - [车](./ch/chwords-cars.md) 
            - [場所（獠的公寓）](./ch/chwords-apart.md) 
            - [場所（其他）](./ch/chwords-places.md) 
            - [Item](./ch/chwords-items.md) 
            - [其他](./ch/chwords-other.md) 
    - [Animation](./ch/chanime.md)  
        - [动画作品解读](./ch/chanimekaisetsu.md)
        - 关于Staff
            - Main Staff
                - [TV剧](./ch/chanimestaff.md) 
                - [劇場版・OAV・TVSpecial](./ch/chanimespecial.md)  
            - TV连续剧每一集的标题和Staff一览  
                - [CITY HUNTER](./ch/chanime1subtitles.md) 
                - [CITY HUNTER 2](./ch/chanime2subtitles.md) 
                - [CITY HUNTER 3](./ch/chanime3_91subtitles.md) 
                - [CITY HUNTER '91](./ch/chanime3_91subtitles.md#91)  
            - [作画監督のお話](./ch/chanimesakuga.md)  
        - 关于主題歌&BGM
            - [主題歌一覧](./ch/chanimetheme.md)  
            - [Opening、Ending和BGM的故事](./ch/chanimethemeimpress.md)  
        - [与原作的不同之处](./ch/chanimediffer.md)  
        - [动画的优点和缺点](./ch/chanimegoodbad.md)  
    - [Another CITY HUNTER](./ch/chanother.md)  
        - [小説](./ch/chnovel.md)  
        - [电影](./ch/chmovie.md)  
        - [Game](./ch/chgame.md)  
        - [法语版CITY HUNTER](./ch/chfrench.md)  
    - [Mini研究](./ch/chkenkyu.md)   
        - 人物简介
            - [冴羽 獠](./ch/chchara-ryo.md)  
            - [槇村 香](./ch/chchara-kaori.md)  
            - [野上 冴子](./ch/chchara-saeko.md)  
            - [海坊主](./ch/chchara-umibozu.md)  
            - [美樹](./ch/chchara-miki.md)  
            - [槇村 秀幸](./ch/chchara-makimura.md)  
        - [年表](./ch/chnempyo.md)   
        - CH背后的世界
            - [名字的秘密](./ch/chname.md)  
            - [獠的原型？](./ch/chmodel.md)  
            - [獠的過去](./ch/chpast.md)  
            - [City Hunter站在正义的一边？](./ch/chjustice.md)  
            - [为什么香有「女人缘」？](./ch/chkaori.md)  
            - [在槇村名字之下](./ch/chmakimura.md)  
            - [槇村、冴子、獠](./ch/chm_s_r.md)  
            - [如何称呼](./ch/chnamecall.md)  
        - [冴羽家的餐桌](./ch/chsyokutaku.md)
    - [CITY HUNTER 随想](./ch/chessay01.md)  
        - [如何创作CITY HUNTER](./ch/chessay01.md#howto)  
        - [CITY HUNTER的世界観](./ch/chessay02.md)  
        - [視角的故事](./ch/chessay03.md)  
        - [獠和香之间的奇怪关系](./ch/chessay04.md)  
        - [最終回的故事](./ch/chessay05.md)  
- [第4章 F.COMPO](./fc/fcindex.md) 	
    - [Story&Main Charactors](./fc/fc_story-charactors.md)   
    - [品味F.COMPO](./fc/fcenjoy.md) 	
        - [作为跨性别者](./fc/fcenjoy.md#1) 
        - [追求真实](./fc/fcenjoy.md#2) 
        - [难以拍成电影的作品](./fc/fcenjoy.md#3) 
        - [漫画家的生活一瞥…](./fc/fcenjoy.md#4) 
        - [多种多样的剧集](./fc/fcenjoy.md#5)  
    - [Impressions](./fc/fcimpressions.md)    
        - [First Impression](./fc/fcimpressions.md#1) 
        - [若苗家3个有趣的人](./fc/fcimpressions.md#2) 
        - [我的第一个「爱情故事」](./fc/fcimpressions.md#3) 
        - [从家庭剧到青春剧](./fc/fcimpressions.md#4) 
        - [不负责任的结局](./fc/fcimpressions.md#5) 
        - [最后…](./fc/fcimpressions.md#6) 
- [第5章 Angel Heart](./ah/ahindex.md) 	
    - [Story & Main Charactors](./ah/ahstory-charactors.md)  
    - [My First Impression](./ah/ahfirst.md)  
    - [Angel Heart是City Hunter的续集?](./ah/ahsequel.md)  
        - [连载开始前的进展](./ah/ahsequel.md) 
        - [平行世界的含义](./ah/ahparallel.md)  
        - [主人公的另一个相貌](./ah/ahface.md)  
    - [Angel Heart 随想](./ah/ahessay.md)  
        - [好的構成](./ah/ahessay01.md)    
        - [空白的一年](./ah/ahessay02.md)    
        - [世界観的危机](./ah/ahessay02.md#sekai)    
        - [GH的存在意義](./ah/ahessay03.md)    
        - [对人的描写缺乏力度](./ah/ahessay04.md)    
        - [獠和香的egotism](./ah/ahessay05.md)    
        - [品牌化的「北条司」](./ah/ahessay06.md)    
        - [Angel Heart描绘了什么](./ah/ahessay07.md)    
        - [关键人物是谁](./ah/ahessay08.md)    
        - [CH补充 ～连载五周年的后记～](./ah/ahessay09.md)  
- [第6章 其他作品](./other/otindex.md)  
    - 长篇-连载  
        - [阳光少女](./other/komorebi.md) 	
        - [RASH!!](./other/rash_splash.md#rash)  
        - [SPLASH!](./other/rash_splash.md#splash) 	
        - [Parrot](./other/parrot.md)  
        - [战争系列](./other/war.md) 	
            - [蓝天的尽头…－少年们的战场－](./other/war.md#soku)  
            - [少年们的夏天 ～Melody of Jenny～](./other/war.md#jenny)  
            - [American Dream](./other/war.md#dream)  
    - 短篇  
        - [我是男子汉！](./other/oreha_neko.md) 	
        - [白猫少女](./other/oreha_neko.md#neko)  
        - [天使的礼物](./other/tenshi_taxi.md) 	 
        - [TAXI DRIVER](./other/tenshi_taxi.md#taxi)  
        - [樱花盛开时](./other/sakura_plot.md) 	
        - [Family Plot](./other/sakura_plot.md#plot)  
        - [少女的季节 －夏之梦－](./other/summer_air.md) 	
        - [AIR MAN](./other/summer_air.md#air)  
        - [THE EYES OF ASSASSIN](./other/assassin_portrait.md) 	
        - [Portrait of father](./other/assassin_portrait.md#portrait)  
- [后记](./postscript.md)  
- [附录](./appendix/appendix.md)  
    - 链接
        - [官方网站](./appendix/links_official.md) 
        - [粉丝网站](./appendix/links_fan.md) 
        - [其他](./appendix/links_other.md) 
            - [关于链接到HTKJ](./start.md#link)  
            - [奢华的专栏「100个问题」](./column/col_100.md)  
    - 相关资料
        - [单行本](./appendix/tankoubon.md) 
        - [画集・Mook](./appendix/gasyu.md) 
        - [杂志](./appendix/magazine.md) 
        - [小説](./appendix/novels.md) 
        - [CD](./appendix/cd.md) 
        - [视频](./appendix/video.md) 
        - [DVD](./appendix/dvd.md) 
        - [卡带/CDPook](./appendix/cassette.md) 
        - [其他](./appendix/otherbooks.md) 
        - [Web Site](./appendix/websites.md)   
    - HTKJ制作的幕后花絮
        - [2002年2月～6月](./appendix/productionnote1.md)  
        - [2002年7月～9月](./appendix/productionnote2.md)  
        - [2002年10月～12月](./appendix/productionnote3.md)  
        - [2002年12月～2003年11月](./appendix/productionnote4.md)  
        - [2003年12月～2006年11月](./appendix/productionnote.md)  
- [制作日誌](./appendix/productionnote.md)  
- [HTKJ-BBS & HTKJ调查问卷](http://ckanno.hp.infoseek.co.jp/to_bbs.htm)  
- Column
    - [100个问题](./column/col_100.md)
        - [CITY HUNTER粉丝的100个问题的回答](./column/ch_quest100.md)
        - [奇怪的100个问题和回答](./column/chah_quest100.md)
    - [AH故事般的作品](./column/col_ahlike.md) 
    - [CH3 和潜意识](./column/col_ch3.md)
    - [Fan fiction的世界](./column/col_fanfiction.md)
    - [神秘的插图明信片](./column/col_hagaki.md)
    - [北条作品式的戏剧](./column/col_hojolike.md)
    - [北条司和井上雄彦](./column/col_inoue.md)
    - [这里的北条司](./column/col_konna.md)
    - [田中芳樹是CH的粉丝吗？](./column/col_tanaka.md)
    - [獠的声音是俊夫吗？](./column/col_voice.md)
    - [读卖电视动画30年史!](./column/col_yomiuri.md)


## 关于此中文翻译  

### 动机   
首先，"北条司研究序説"(HTKJ)是进一步了解北条司及其作品的必读资料。其次，虽然每个人都可以借助翻译工具去阅读HTKJ，但对于机翻不通顺的句子，每个人的理解可能各有不同。所以想提供一个地方，集众人所长来不断地完善HTKJ中文版。最后，翻译非常耗时；一人翻译，众人受益；HTKJ几乎不更新，所以该翻译是一劳永逸。  

### 关于版权
HTKJ里提到： "当「研究序説」内記事の無断転載および、ページに無断で直接リンクすることを禁じます。（在未经许可的情况下，禁止擅自复制本"研究序説"中的文章，禁止直接链接到这些网页。）"。所以，我想该联系该作者得到翻译的授权，但我找不到作者的联系方式。同时其[首页](http://chiakik.la.coocan.jp)的BBS链接也不可用。希望原作者不反对本翻译。    

### 关于翻译的一些约定
一方面想忠于原文，另一方面个人精力有限。所以需要有取舍，便有了如下翻译约定：  

- 只翻译日文。原因：1）大部分读者有英文阅读能力。2）机翻"日译英"的结果常常比"日译中"的结果好。    
- 以下内容未作翻译或改动：  
    - "序説"意为"序论"、"introduction"。"北条司研究序説"可译为"北条司研究序论"。鉴于这个专术语均由汉字组成，且已被中文fans熟知。所以，本中译版保留"北条司研究序説"。  
    - 保留日文标点符号，例如，『』，「」，等。  
    - 保留日文专有名词或书名里的"・"。  
    - 常见的、易理解的繁体字词未翻译。  
    - 对于英文单词的日文发音，绝大多数直接翻译为英文。例如，"シティーハンター"翻译为"City Hunter"。    
- 暂时舍弃原文里某些段落的格式、表格的格式。待以后完善时再加上这些格式。  


### 其他
"今月之名台词"部分未翻译。其内容似乎只是作者搜集的北条司作品里的部分台词。因个人精力有限，同时它似乎和北条司作品的分析无关，所以，暂时未翻译。  

此中文翻译使用在线翻译[Google Translate](https://translate.google.cn/) 、[DeepL Translate](https://www.deepl.com/translator)，所以译文难免有误。欢迎指出错误，谢谢！

在翻译过程中，对于我觉得翻译不准确的地方，在其段落末尾标有译注(例如：“（译注：待校对）”)，便于后续更有针对性地去完善。    

因HTKJ的版权、文字格式等问题，本中译版未将中译文附在日文之后，而是做成独立于日文的单独文件目录。这既有优点也有缺点。缺点是，校对译文时需要另外访问日文原文网址。  

翻译CH部分时，我对比了CH香港玉皇朝版和台湾东立版。发现香港玉皇朝版的译文更忠于原作。例如，在[獠的過去](./ch/chpast.md) 里提到的「BabyFace」，在东立版中未翻译 。  

如有任何问题，欢迎来此讨论： BBS-[北条司中文网](www.hojocn.com/bbs)， 帖子-[北条司研究序説(中文翻译)](http://www.hojocn.com/bbs/viewthread.php?tid=96936)。    

### 参考资料：  
- [日语在线发音](https://riyutool.com/zaixianfayin/)  
- [日语翻译、发音](https://fanyi.qq.com/)  

## 后续  
虽然已经有HTKJ珠玉在前，但我觉得还是有一些事情可以做。   
第一，HTKJ研究的话题主要集中在信息搜集、编剧等，对北条司的绘画研究得不多。而北条司的绘画又是其作品的一大特点。所以，在绘画这个方向上还可以研究很多话题。  
第二，HTKJ没有研究作品里的镜头语言。  
第三，HTKJ对CH的研究最详细，所以，可以把CH所研究的各个话题应用到其他作品上。  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

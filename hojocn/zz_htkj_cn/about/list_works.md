[INDEX](../index.md) > [About 北条司](./abindex.md) >

# 作品清单

这份名单是基于我的研究。  
我不能保证所有的作品都包括在内。  
(点击作品的标题，进入其描述页面)。  


<CENTER>
<TABLE cellpadding="0" width="95%">
  <TBODY>
    <TR>
      <TD bgcolor="#191970">
      <TABLE border="1" cellpadding="2" width="100%">
        <TBODY>
          <TR>
            <TD bgcolor="#F0F8FF" class="tit10" align="center" width="50">発表年</TD>
            <TD bgcolor="#F0F8FF" class="tit10" align="center" width="200">作品名</TD>
            <TD bgcolor="#F0F8FF" class="tit10" align="center" width="220">掲載</TD>
            <TD bgcolor="#F0F8FF" class="tit10" align="center">备注</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1979<BR>
            (S54)</TD>
            <TD bgcolor="#ffffff" class="tit11">Space・Angel(译注[^0])</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
            <TD bgcolor="#ffffff" class="t10">第18回（1979年下期）手塚賞準入選<BR>
            単行本未収録。</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1980<BR>
            (S55)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/oreha_neko.md#oreha">我是男子汉！</A>(译注[^6])</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1980年8月増刊号</TD>
            <TD bgcolor="#ffffff" class="t10">职业首秀</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10" rowspan="3">1981<BR>
            (S56)</TD>
            <TD bgcolor="#ffffff" class="tit11">三級刑事（サードデカ）</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1981年1月増刊号</TD>
            <TD bgcolor="#ffffff" class="t10">原作：渡海風彦<BR>
            単行本未収録。在官方网站上只能找到一页的图片。</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11">猫❤眼</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1981年29号</TD>
            <TD bgcolor="#ffffff" class="t10">JC第1巻第1話</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../ce/cetop.md">CAT'S EYE</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1981年40号～<BR>
            1984年44号</TD>
            <TD bgcolor="#ffffff" class="t10">1983年～1985年 日本电视网公司的电视动画系列<BR>
            1997年 电影（真人出演）「CAT'S EYE」</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1982<BR>
            (S57)</TD>
            <TD bgcolor="#ffffff" class="tit11">Space・Angel(译注[^0])</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1982年16号</TD>
            <TD bgcolor="#ffffff" class="t10">Jump最受读者喜爱奖（与手冢奖亚军的作品分开评选）<BR>
            単行本未収録。官方网站上只有一页图片。 这个故事似乎是太空歌剧版的「猫・眼」。</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1983<BR>
            (S58)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../ch/chyomikiri.md">City Hunter －XYZ－</A>(译注[^3])</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1983年18号</TD>
            <TD bgcolor="#ffffff" class="t10">Jump最受读者喜爱奖</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1984<BR>
            (S59)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../ch/chyomikiri.md">城市猎人ー<BR>
            　－双刃剑－</A></TD>
            <TD bgcolor="#ffffff" class="t10">Fresh Jump 1984年2月号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10" rowspan="2">1985<BR>
            (S60)</TD>
            <TD bgcolor="#ffffff" class="tit11">CAT'S EYE<BR>
            　再一次恋爱(完結篇)</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1985年6号</TD>
            <TD bgcolor="#ffffff" class="t10">JC第18巻最終話</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../ch/chtop.md">CITY HUNTER</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1985年13号～<BR>
            1991年50号</TD>
            <TD bgcolor="#ffffff" class="t10">1987年～1991年 读卖电视台的电视动画系列<BR>
            1993年 电影（真人出演）「シティーハンター（城市猟人）」</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1986<BR>
            (S61)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/oreha_neko.md#neko">白猫少女</A>(译注[^1])</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1986年6号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10" rowspan="2">1987<BR>
            (S62)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/rash_splash.md#splash">SPLASH !</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1987年4月増刊<BR>
            Super Jump</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/rash_splash.md#splash">SPLASH ! 2</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1987年6月増刊<BR>
            スーパージャンプ</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10" rowspan="2">1988<BR>
            (S63)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/tenshi_taxi.md#tenshi">天使的礼物</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1988年34号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/rash_splash.md#splash">SPLASH ! 3</A></TD>
            <TD bgcolor="#ffffff" class="t10">Super Jump 1988年11月号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1989<BR>
            (H1)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/rash_splash.md#splash">SPLASH ! 4</A></TD>
            <TD bgcolor="#ffffff" class="t10">Super Jump 1989年4月号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1990<BR>
            (H2)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/tenshi_taxi.md#taxi">TAXI DRIVER</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1990年6号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10" rowspan="2">1992<BR>
            (H4)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/sakura_plot.md#plot">Family Plot</A>[^5]</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1992年11号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/summer_air.md#summer">少女的季节<BR>
            　－夏之梦－</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1992年39号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10" rowspan="2">1993<BR>
            (H5)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/sakura_plot.md#sakura">樱花盛开时</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1993年3・4合併号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/komorebi.md">阳光少女</A>(译注[^2])</TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1993年31号～<BR>
            1994年5・6合併号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1994<BR>
            (H6)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/rash_splash.md#rash">RASH!!</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1994年43号～<BR>
            1995年9号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10" rowspan="4">1995<BR>
            (H7)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/war.md#soku">蓝天的尽头…<BR>
            　－少年们的战场－</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1995年16・17号</TD>
            <TD bgcolor="#ffffff" class="t10">原作：二橋進吾</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/war.md#jenny">少年们的夏天<BR>
            　～Melody of Jenny～</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1995年28・29号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/war.md#dream">American Dream</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊少年Jump 1995年36・37合併号</TD>
            <TD bgcolor="#ffffff" class="t10">原作：二橋進吾</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/rash_splash.md#splash">SPLASH ! 5</A></TD>
            <TD bgcolor="#ffffff" class="t10">Manga ALLMAN　1995年11月号</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1996<BR>
            (H8)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../fc/fctop.md">F.COMPO</A>(译注：[^4])</TD>
            <TD bgcolor="#ffffff" class="t10">Manga ALLMAN 1996年No.10～<BR>
            2000年No.23</TD>
            <TD bgcolor="#ffffff" class="t10">在「北条司漫画家20周年記念画册」中，它一直到2000年的第22期，但我已经列出了这本书。（译注：此处翻译需改善）</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">1998<BR>
            (H10)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../other/parrot.md">Parrot</A><BR>
            <A href="../other/assassin_portrait.md#assassin">THE EYES OF ASSASSIN</A><BR>
            <A href="../other/summer_air.md#air">AIR MAN</A><BR>
            <A href="../ce/cesubtitles.md#shortce">CAT'S EYE</A><BR>
            <A href="../other/assassin_portrait.md#portrait">Portrait of father</A></TD>
            <TD bgcolor="#ffffff" class="t10">BART3230 1998年12月号～<BR>
            1999年5月号/Men's non-no</TD>
            <TD bgcolor="#ffffff" class="t10">数码工作：永田太<BR>
            短編集／每件作品出现在哪一期都不清楚。</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10">2001<BR>
            (H13)</TD>
            <TD bgcolor="#ffffff" class="tit11"><A href="../ah/ahtop.md">Angel Heart</A></TD>
            <TD bgcolor="#ffffff" class="t10">週刊Comic Bunch 2001年創刊号～<BR>
            連載中</TD>
            <TD bgcolor="#ffffff" class="t10" align="center">-</TD>
          </TR>
        </TBODY>
      </TABLE>
      </TD>
    </TR>
  </TBODY>
</TABLE>
</CENTER>

译注：  
[^0]  原文"スペース・エンジェル"是"Space Angel"的日文发音。"Space Angel"国内多译为"宇宙天使"。另，[Cat's Eye 40周年原画展](https://edition-88.com/blogs/blog/catseye40th-exhibition)展览了这部作品的重绘版。其[扉页](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/spaceangel_tenji_360_2024x2024.jpg)显示了作品的日文名"スペース エンジェル"和英文名"Space Angels"，即angel为复数形式，但日文名里没有复数's'的发音。    
[^1] 《白猫少女》原著名为"ネコまんまおかわり❤"，直译可能为"再来一份猫饭❤"。中文译名有："白猫少女"、"再续猫缘"、["再来一客猫饭"](https://www.bilibili.com/read/cv15635629)   
[^2] 《阳光少女》原著名为"こもれ陽の下で…"，直译为"在阳光下..."。该作品的其他译名有："Komorebi no Shita de"，"Komorebi no Moto de"，"Under the Dapple Shade"。  
[^3] 原文"シティーハンター"是"City Hunter"的日文发音。  
[^4] "ファミリー・コンポ"是"Family Compo"的日文发音。  
[^5] 原文"ファミリー・プロット"是"Family Plot"的日文发音。国内多译为"家庭故事"。    
[^6] [近两年某中文版短篇集](https://www.jianshu.com/p/acfbe8a02100)中显示了《我是男子汉!》的英文名为"Anachronistic Rhapsody!"（我暂且将它直译为"陈年狂想曲！"） 

[BACK](./keireki.md)  
[NEXT](./e.md)  
# 奇怪的100个问题和回答

※于2002年1月1日修订  

## 首先，从普通问题开始 CITY HUNTER篇
<center>
<table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">01．如何称呼阁下？</td>
      <td class="t11" width="60%" valign="top">我是神野千章。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">02．你成为粉丝多久了？</td>
      <td class="t11" width="60%" valign="top">如果从我知道CH开始算起，那就超过17年了吧?</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">03．你的第一印象是什么？</td>
      <td class="t11" width="60%" valign="top">「好吧，CAT'S EYE的作者现在正在画这种故事，『冴羽獠』是另一个故意想出来的名字」</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">04．你是如何成为一个粉丝的？</td>
      <td class="t11" width="60%" valign="top">我在连载之初就有上述的第一印象，大约是在乐生会篇的时候，但直到我在高中时看了重播的动画，我才真正入迷。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">05．说出你最喜欢的1个人物!</td>
      <td class="t11" width="60%" valign="top">嗯…很难。 毕竟，这将是冴羽獠，不是吗？ 但最近我也喜欢冴子。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">06．说出你最讨厌的1个角色!</td>
      <td class="t11" width="60%" valign="top">神宮寺遙。 (及川優希也不喜欢…在许多方面。)</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">07．你是原作党？还是动画党？</td>
      <td class="t11" width="60%" valign="top">我不能说两者都是。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">08．Ryo的魅力何在？</td>
      <td class="t11" width="60%" valign="top">他很傻，但也很酷。 如果缺少其中之一，就不是獠。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">09．香的魅力何在？</td>
      <td class="t11" width="60%" valign="top">有吸引力，但她自己却没有意识到这一点(?)。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">10．谁是最令人难忘的委托人？</td>
      <td class="t11" width="60%" valign="top">冬野葉子、Bloody Mary、佐藤由美子</td>
    </tr>
    <tr>
</tr></tbody>
</table>
</center>

## 让我们畅所欲言、充满激情和热情　CITY HUNTER篇
<center>
<table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">11．你最喜欢的扉页图是什么？</td>
      <td class="t11" width="60%" valign="top">太多了，我都记不住…(^^;)<br>
      16巻「情人是城市猎人」、22巻「把蟒蛇交给淑女!」、33巻的内封面。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">12．如果你是CH的一个角色，你会是谁？</td>
      <td class="t11" width="60%" valign="top">野上唯香。 我希望我可以写一本畅销书。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">13．如果我想参观冴羽公寓，我应该带什么纪念品？</td>
      <td class="t11" width="60%" valign="top">上等的普通咖啡。 要求将其放入香。（译注：待校对）<br>
      「ちゃ～～んと豆をひいてな!!」(by獠＠20巻)（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">14．你有没有画过CH的插图？</td>
      <td class="t11" width="60%" valign="top">如果是用铅笔，那是很久以前的事了。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">15．你写过CH小说吗？</td>
      <td class="t11" width="60%" valign="top">我在大学笔记本上写了2页。 当我还在高中的时候…。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">16．你在恋人身上寻找什么？</td>
      <td class="t11" width="60%" valign="top">…这与CH有关吗…?(^^;)<br>
      「他真诚而善良，（中间略）最重要的是他足够坚强、可以依靠！」(by牧原友佳@21巻)。但是<br>
      「今天世界上有多少男人会冒着生命危险去保护一个女人？」 (by獠＠19巻)…</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">17．你认为的名场面？</td>
      <td class="t11" width="60%" valign="top">嗯，有很多…仅举一例…<br>
      第32卷第73页，当Mick离开射击场，獠在楼梯口时--两人擦肩而过。 Mick的表情、2人的互动和他们之间的紧张感都很好。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">18．你最喜欢的鸡尾酒是什么？ (假设你想喝)</td>
      <td class="t11" width="60%" valign="top">我试过XYZ，非常喜欢它。 它的味道很浓，但很可口。(^^)</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">19．除了獠，你最喜欢的男性角色是谁？</td>
      <td class="t11" width="60%" valign="top">CH没有很多像样的男性角色(笑)。 所以我决定在这里选普通得海酱。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">20．夸夸你的粉丝</td>
      <td class="t11" width="60%" valign="top">嗯…是什么呢？ 是那张神秘的插图明信片吗？ (内容见HP…)</td>
    </tr>
  </tbody>
</table>
</center>

## 另一个City Hunter，动画篇
<center>
<table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">21．你最喜欢的CH声音是什么？</td>
      <td class="t11" width="60%" valign="top">必须是GET WILD…（译注：待校对）<br>
      但是最近我在想「愛よ消えないで」（译注：爱，不要走开）这首歌的歌词也适合CH。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">22．你认为哪些歌曲有点？（译注：待校对）</td>
      <td class="t11" width="60%" valign="top">最后的TV Special的OP和插曲；重金属不适合CH。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">23．你最喜欢的声优是谁？</td>
      <td class="t11" width="60%" valign="top">伊倉小姐。好运～</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">24．哪些声优不符合形象？</td>
      <td class="t11" width="60%" valign="top">神谷。…撒谎。 但在节目开始时我是这么想的。 现在我认为他是唯一的一个。<br>
      让我看看，嘉宾、为阿尔玛公主、Mary配音的人的声音都不对。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">25．你最喜欢的动画导演是哪一位？</td>
      <td class="t11" width="60%" valign="top">北原健雄、神村幸子、高岡希一</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">26．哪些动画导演不符合形象？</td>
      <td class="t11" width="60%" valign="top">いのまたむつみ（译注：豬股睦美）、谷口守泰、西澤晋<br>
      他们都是老手，擅长他们的工作，但他们的绘画不好…</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">27．动画的优点是什么？</td>
      <td class="t11" width="60%" valign="top">毕竟，剧情比原作写得好。 还有动作。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">28．动画的缺点是什么？</td>
      <td class="t11" width="60%" valign="top">说到底，还是画面不行，对吧？ 很遗憾。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">29．你最喜欢的原创动画故事是什么？</td>
      <td class="t11" width="60%" valign="top">第21集 第30集 第34集 第45集等等…　我经常说，我喜欢每个故事中的某个场景。<br>
      顺便说一下，第100-101集就不太一样了… </td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">30．你拥有任何动漫周边吗？</td>
      <td class="t11" width="60%" valign="top">有。<br>
      橡皮擦、机械铅笔、尺子、底板、笔记本、磁带标签<br>
      还有一个100吨的锤子钥匙圈，但丢失了…(^^;)</td>
    </tr>
  </tbody>
</table>
</center>

## 还有很长，但以我现在的心态，Angel Heart篇（译注：待校对）
<center><table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">31．你买过周刊Comic Bunch 吗？※</td>
      <td class="t11" width="60%" valign="top">2002年底不再买了…(ーー;)</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">32．你对剧透者有什么看法？（译注：待校对）</td>
      <td class="t11" width="60%" valign="top">感谢你们的努力。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">33．你认为谁是AH中的主演？</td>
      <td class="t11" width="60%" valign="top">嗯，应该是Glass Heart（香瑩）。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">34．你认为香会复活吗？</td>
      <td class="t11" width="60%" valign="top">如果她复活了，我就会很生气。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">35．你认为Glass Heart怎么样？</td>
      <td class="t11" width="60%" valign="top">可怜的孩子，作者对她不错。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">36．你如何用颜色来比较Glass Heart、獠和香？（译注：待校对）</td>
      <td class="t11" width="60%" valign="top">Glass Heart：紫<br>
      獠：灰<br>
      香：红</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">37．你买漫画吗？※</td>
      <td class="t11" width="60%" valign="top">是的。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">38．关于该动画（译注：待校对）</td>
      <td class="t11" width="60%" valign="top">不。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">39．你喜欢在一周的哪一天做动画片？</td>
      <td class="t11" width="60%" valign="top">所以不要这样做。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">40．你有手机吗？</td>
      <td class="t11" width="60%" valign="top">有。docomo。</td>
    </tr>
  </tbody>
</table></center>

## 在此休息一下　CITY HUNTER篇
<center><table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">41．关于你在Q12中的角色，你想说什么？</td>
      <td class="t11" width="60%" valign="top">「冴羽先生!」<br>
      …我想喊一次，你会吗？</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">42．香的生日。 你将在哪里庆祝？ 礼物呢？</td>
      <td class="t11" width="60%" valign="top">为什么不偶尔出去吃个饭呢？<br>
      至于礼物…嗯，我想不出来了。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">43．你最想去新宿的哪里？</td>
      <td class="t11" width="60%" valign="top">我去过新宿站的东口和歌舞伎町…还有一些同性恋酒吧。(笑)</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">44．你会在CE咖啡厅里点什么呢？</td>
      <td class="t11" width="60%" valign="top">200日元(?)一杯的咖啡。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">45．你会和谁通宵喝酒（聊天）？</td>
      <td class="t11" width="60%" valign="top">与冴子～♪</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">46．说一句永远不会忘记的伟大台词!</td>
      <td class="t11" width="60%" valign="top">太多了。 好吧，选一个被人说的少的…<br>
      「香!　从现在开始，你不是一个女人!　你是我的搭档！　你愿意跟随我吗？」(2巻)</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">47．你最喜欢的品牌是什么？（by 北原エリ）</td>
      <td class="t11" width="60%" valign="top">没什么特别的。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">48．对于CH的结局，你有什么期待吗？（译注：待校对）</td>
      <td class="t11" width="60%" valign="top">我进入大学后开始拉小提琴。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">49．除了CH，你最喜欢的北条作品是什么？</td>
      <td class="t11" width="60%" valign="top">建议F.COMPO和Parrot。 然后是CAT'S EYE，我就是这样认识了北条司的。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">50．快！CH对你意味着什么？</td>
      <td class="t11" width="60%" valign="top">青春的回忆。</td>
    </tr>
  </tbody>
</table>
</center>


## 带着各种想法和感受　Angel Heart篇
<center>
<table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">51．你对新连载的第一印象如何？</td>
      <td class="t11" width="60%" valign="top">「北条司在说谎!」<br>
      我以为他不会画续集（因为我可以预见它会失败），然后我曾相信他说的新连载不会是CH。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">52．你是如何发现这个新连载的？</td>
      <td class="t11" width="60%" valign="top">什么意思？ 在上网的时候，我想我看到它很快就要开始了。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">53．你认为AH是CH的续集吗？</td>
      <td class="t11" width="60%" valign="top">是续集，虽然是平行的。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">54．你对AH的未来发展有什么期望？</td>
      <td class="t11" width="60%" valign="top">香瑩的自立。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">55．你希望看到哪些人物在未来活跃起来？※</td>
      <td class="t11" width="60%" valign="top">劉信宏。我认为他是关键人物。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">56．AH的作品的主题是什么？ 你的解读。</td>
      <td class="t11" width="60%" valign="top">作者原本想描写一个「女孩成长的故事」和人与人之间的「纽带」，但作品里没有写这些。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">57．你认为Ryo的最后工作是什么？</td>
      <td class="t11" width="60%" valign="top">当你问这句话时，它是关于寻找香之心脏的…但现在它已经毫无意义了。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">58．你想让香在GH中延续下去吗？</td>
      <td class="t11" width="60%" valign="top">嘿嘿…</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">59．你如何看待GH在未来的发展？</td>
      <td class="t11" width="60%" valign="top">她不会像现在这样成长。 除非作者做一些戏剧性变化。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">60．如果Ryo和香在未来不再出现，你还会继续阅读这个连载吗？ 原因是什么？</td>
      <td class="t11" width="60%" valign="top">我想我会浏览，或者看单行本。<br>
      原因是，嗯…就是想看。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">61．官方网站上有哪些内容值得关注？</td>
      <td class="t11" width="60%" valign="top">必须是"作者のMessage"。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">62．粉丝网站上有哪些内容是值得关注的？</td>
      <td class="t11" width="60%" valign="top">对作品的坦诚印象。<br>
      可能也喜欢很多二次創作小説。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">63．不喜欢官方网站的哪些部分!</td>
      <td class="t11" width="60%" valign="top">在Bunch创建之前，它并不特别…现在它的更新速度很慢，信息很单薄，我觉得他忽视了粉丝。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">64．讨厌粉丝网站的哪些部分!</td>
      <td class="t11" width="60%" valign="top">没什么特别的。 因为每个人都不一样。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">65．请就北条先生的致辞说几句话。</td>
      <td class="t11" width="60%" valign="top">人们都在看。 请坚定一点。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">66．你对官方网站的未来有什么期望？</td>
      <td class="t11" width="60%" valign="top">应由有动力的人负责。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">67．如果你能和北条先生聊1分钟，你会聊些什么？</td>
      <td class="t11" width="60%" valign="top">我想为我很久之前发出的一封粉丝信道歉，我甚至都不好意思记住这封信。 (T.T)…大约是15年前…</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">68．你有多少个关联的书签？</td>
      <td class="t11" width="60%" valign="top">有50多个创建网站的文件。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">69．除了CH&AH相关的以外，你经常去什么HP？</td>
      <td class="t11" width="60%" valign="top">与井上直久（Iblard系列的画家和作者）有关的网站。 一个朋友的网站。 在线软件网站。 与ザウルス有关、与UO有关。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">70．你对北条先生的支持程度如何？ (单位：%)</td>
      <td class="t11" width="60%" valign="top">50％。</td>
    </tr>
  </tbody>
</table>
</center>

## 更进一步　Angel Heart篇
<center><table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">71．你买了这套汇编吗？ 原因是什么？</td>
      <td class="t11" width="60%" valign="top">买了。 我很好奇，想看看它是否被修改过什么地方(笑)</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">72．Bunch的东西是逐个都看了吗？（译注：待校对）</td>
      <td class="t11" width="60%" valign="top">我只是为了记录而把AH剪掉，然后把它们处理掉。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">73．你希望在回忆中看到哪些情节？</td>
      <td class="t11" width="60%" valign="top">没有特别的。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">74．要把一个专用的杯子放在CE咖啡厅里，这是什么设计？</td>
      <td class="t11" width="60%" valign="top">ミスドのやつ。（哈哈）我在家里一直在使用它。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">75．如果要换工作，你会为Ryo设想什么样的职业？</td>
      <td class="t11" width="60%" valign="top">嗯…<br>
      私人侦探。 (这算不算换职业…？)</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">76．第1～28话中最令人难忘的台词是什么？※</td>
      <td class="t11" width="60%" valign="top">「……现在做不到…，我已经…忘记该怎么笑了…你一定要给我想起来…，从现在开始…我会保护你…」（27話）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">77．为什么你认为海坊主是黑色的？</td>
      <td class="t11" width="60%" valign="top">这是个平行世界。<br>
      还有…也许是「Spenser Series」的影响？</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">78．请给 GH 起个名字</td>
      <td class="t11" width="60%" valign="top">我相信你们都知道。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">79．你如何用一个汉字来描述ＧＨ、Ryo和香？</td>
      <td class="t11" width="60%" valign="top">GH：虚、獠：旧、香：凭<br>
      …都是某种令人敬畏的人物(ーー;)</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">80．你认为香的车祸是意外吗？</td>
      <td class="t11" width="60%" valign="top">这是一个意外。…但这是北条老师，所以他可能又会被一个故事卡住，说一些「实际上…」之类的事情…（译注：待校对）</td>
    </tr>
  </tbody>
</table>
</center>


## AH还不够吗？！　CITY HUNTER篇
<center>
<table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">81．有什么你想在未来看到的商业化的东西吗？</td>
      <td class="t11" width="60%" valign="top">好吧，仍然是一部TV剧的DVD。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">82．如果你要出售全套电视剧的DVD，你愿意出多少钱？</td>
      <td class="t11" width="60%" valign="top">「北斗神拳」的售价为10万日元，所以我猜想它一定很贵。<br>
      但是，我有勇气一下子付这么多钱。如果像CAT'S EYE那样划分，效果会更好。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">83．对于你过去所处理的东西，你最后悔的是什么？</td>
      <td class="t11" width="60%" valign="top">动画杂志上的专题文章。 这确实是在浪费时间。 如果我有这个时间，我可以写更多的东西！</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">84．如果公开说是续集CH2开始，你会期待什么样的设定？</td>
      <td class="t11" width="60%" valign="top">这很好，就像它一直以来一样。…但我认为没有必要使用留言板。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">85．你是下一个CH Special的原作希望派？ 还是原创期待派？ 原因是什么？</td>
      <td class="t11" width="60%" valign="top">原创（非漫画故事）。 我不认为突然做一个没有在书中被动画化的故事会很有趣。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">86．继续回答Q85，故事是什么？</td>
      <td class="t11" width="60%" valign="top">北条先生一定是脑子里还留有一些他想写的材料（写在书的封面折页上--对吧），所以他说他应该这样做。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">87．除了新宿之外，你还希望看到哪些城市？</td>
      <td class="t11" width="60%" valign="top">例如，横滨。 (曾经是吗？)（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">88．你希望在你的婚礼上拥有（或曾经拥有）什么花束？ 你想让一个男人拿什么花束？</td>
      <td class="t11" width="60%" valign="top">嗯…反正只要它是干净的就行。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">89．如果让你当一天的冴羽Ryo，你会做什么？</td>
      <td class="t11" width="60%" valign="top">在射击场上的练枪！<br>
      到处勾搭女孩，在酒店里mo…(^_^;)＼('_')（译注：待校对）
      哦哦...</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">90．如果Ryo和香有一个孩子，会叫什么？</td>
      <td class="t11" width="60%" valign="top">我没想过。</td>
    </tr>
  </tbody>
</table></center>


## 目前为止已经回答了这个问题　综合篇
<center>
<table style="margin-left : 42pt;margin-right : 42pt;" cellpadding="3">
  <tbody>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">91．你觉得AH的故事内容是否有吸引力？</td>
      <td class="t11" width="60%" valign="top">目前来看，我觉得不是。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">92．Ryo的支持率是多少？ 分别在 CH 和 AH里</td>
      <td class="t11" width="60%" valign="top">CH：70%, AH：20%</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">93．香的支持率是多少？ 分别在 CH 和 AH里</td>
      <td class="t11" width="60%" valign="top">CH: 70%, AH:10%</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">94．Glass Heart的支持率是多少？</td>
      <td class="t11" width="60%" valign="top">如果你从现在开始尽力而为，我会给她大约60％。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">95．你是否期望AH成为一个长期连载？</td>
      <td class="t11" width="60%" valign="top">我不会。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">96．绘画中的触动变化…你最喜欢的时候是什么时候？（译注：待校对）</td>
      <td class="t11" width="60%" valign="top">我也喜欢大概第1～7卷。 不过，这可能是不寻常的。<br>
      其余的是 28 卷之后。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">97．除了 Bunch，你还订阅哪些杂志？</td>
      <td class="t11" width="60%" valign="top">没什么特别的。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">98．AH新连载给你带来了什么？</td>
      <td class="t11" width="60%" valign="top">写「北条司研究序説」的决心。</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">99．AH新连载让你失去了什么？</td>
      <td class="t11" width="60%" valign="top">青春的回忆。 对北条司的信任（好吧，一开始就没怎么信任）。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="40%" valign="top" bgcolor="#F0F8FF">100．请说说你现在的感受。</td>
      <td class="t11" width="60%" valign="top">谢谢你带给我们的快乐。</td>
    </tr>
  </tbody>
</table>
</center>



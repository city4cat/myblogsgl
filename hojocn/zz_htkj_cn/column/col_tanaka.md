【はみ出しコラム】

# 田中芳樹是CH的粉丝吗？

(也许这在粉丝中是众所周知的…)  
田中芳树是一位以「银河英雄传说」、「创龙传」等作品而闻名的人气漫画家，但他似乎一直是个动漫迷，各种动漫作品的名字都出现在他的作品中，有的是原名，有的是改名。 其中有一个描述似乎包括CITY HUNTER。   

## 「银河英雄传说」的后记中…

在他最著名的作品「银河英雄传说」最終巻（第10巻）的后记中，他对其中一个人物Oliver Poplan的评价如下：  

<font color=#660066>
>    然而，在「銀英伝」的故事情节中，只有两个人物本应死亡但却活了下来。 …（中略）…Poplan先生如果财运很好，可能就是Rhett Butler ，如果财运不好，就是City Hunter。 我相信他在这两种情况下都会做得很好。  
>    （摘自德间小说「银河英雄传说」第10卷P242。「City Hunter」为原文）  
</font>

Poplan的角色定位似乎是一个年轻的射手王和一个欢乐的女子。 我明白了。 但「如果你没有财运，你就是一个城市猎人」…哈哈。(^^;)。（译注：待校对）  

## 「创龙传」一书中，有提到Angel Dust…

在描述龙王4兄弟在当代转世的经历的人气系列片「创龙传」中，有两处提到了Angel Dust，这是一种出现在CITY HUNTER中的药物。  

第1处：    
描述敌人藏身之处的部分（我会用这个描述，因为它很复杂），主人公和4兄弟在那里。  

<font color=#660066>
>    站在大厅里，这四名警官都惊呆了。 这是一个大屠杀的场面，就像一群职业摔跤手在服用了一种叫做Angel Dust的药物后，疯狂地在一群人中横冲直撞一样。  
>    （摘自講談社小说「創竜伝」第3卷P99）  
</font>

第2处： 
下面的说法是在讨论被输了4兄弟的血的人时说的：  

<font color=#660066>
>    龙种的血液使摄取了它的人身体显著增强。 然而，例如一种名为「Angel Dust」的药物，既能增加一个人的肌肉力量，又能同时摧毁其精神。 会不会是龙种的血液也有这样的作用？  
>    （摘自講談社小说「創竜伝」第7卷P66）  
</font>

天使尘（Angel Dust）本身是一种真正的药物，所以提到它不一定会和CH有关。 然而，它被用作发出畸形力量的隐喻，这有点让人想起乐生会的CH。   

顺便说一句，「创龙传」的主人公的祖父名为"竜堂司"、姑姑的名字为"鳥羽冴子"。 这有点意思，尽管是巧合。  

INDEX > 付録 > 関連資料

# Web Site  

与北条作品没有直接关系，但我提到过的网站。  

- [Colt's Manufacturing Company, Inc. Home Page](http://www.colt.com/colt/) （Colt的官方网站）  
- [Smith & Wesson Firearms](http://www.smith-wesson.com/) （Smith & Wesson 官方网站）  
- [WILLDERNESS](http://homepage2.nifty.com/flipflopflap/gamers/index.htm)（有一个在TV、漫画和游戏中出现过的枪支的数据库）  
- [TOYOTA](http://www.toyota.co.jp/index.html)  
- [TV Drama Database](http://www.tvdrama-db.com/)  
- [Trans Sexual Forum in Japan](http://www.geocities.co.jp/HeartLand-Hinoki/4126/)  
- [Shelter（手冢奖和赤冢奖得主名单）](http://www.geocities.co.jp/Playtown-Dice/6635/)  
- [预防药物滥用「不。 不要这样做。」主页](http://www.dapc.or.jp/)  
- [日本循環器学会-心臓移植委員会](http://plaza.umin.ac.jp/~hearttp/index.html)  
- [FOOTBALL PARK](http://www.f-park.com/)  
- [PC-ENGINE PARADISE](http://www.geocities.co.jp/HeartLand-Suzuran/9038/index.html)  
- [Restive Horse](http://www2.justnet.ne.jp/~takashikonno/index.html)（TOYOTA SUPRA  专业网站）  

[BACK](./otherbooks.md)  
[NEXT](./productionnote1.md)  
INDEX > 付録 > リンク

# 粉丝网站

以下是我经常浏览的一些粉丝网站。 所有这些都值得一游。  
其信息比官方网站还快!?  

## [JMC](http://www.netlaputa.ne.jp/~arcadia/ah/index.html)
●Webmaster：さいばーりょうさん  

一个为Angel Heart提供支持的网站。 你可以用短剧情的形式阅读本周的最新故事，所以如果你不想读杂志但又想知道故事(^^;)，这里就是你的理想之地。 此外，还有很多关于人物和词汇的详细信息。 还有一个论坛，有来自海外的帖子。  

## [Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/)
●Webmaster：さいばーりょうさん  

这是一个很酷的网站，很好地利用了Flash，并且充满了非常详细的信息，包括涵盖CITY HUNTER所有剧集的动画和原创作品的故事摘要，以及一个术语表。 还有一个墙纸/插图库和一个有趣的小板块。  

## [北条司応援隊](http://www5f.biglobe.ne.jp/~julianus/)
●Webmaster：りっくすさん  

该网站满是与F.COMPO有关的信息和各种讨论，包括网友的贡献。 也有关于其他北条作品的评论。  

## [新まぐろなまぐろ～はいぱあ～](http://yumemagu.pupu.jp/)
●Webmaster：夢路可帆さん  

这个网站包含CH同人文（fan fiction），站长夢路さん的小说非常有趣，值得一读。 这里还有很多粉丝小说和粉丝网站的搜索引擎，所以这是一个浏览CH同人文的好地方。  

[BACK](./links_official.md)  
[NEXT](./links_other.md)  

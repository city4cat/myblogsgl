INDEX > 付録 > リンク

# 其他

网上商店和更多链接。为什么不搜索一下你最喜欢的作品呢？    

## [SHUEISYA BOOKNAVI](http://books.shueisha.co.jp/)

一个由集英社出版的书籍搜索网站。 你也可以下订单。  
如果你使用关键词 「北条司」，你可以搜索过去已经发行的书籍。  
关于ISBN、价格和供货的更多信息，请点击这里。 你还可以看到每本《JUMP》漫画的封面，这些漫画现在很少在书店出售。  

## [Amazon.co.jp](http://www.amazon.co.jp/)

这是一家专业的网上书店。 除书籍外，我们还销售CD、AV产品和游戏软件。  

## [Amazon.fr](http://www.amazon.fr/)

法国的亚马逊在线书店。 北条作品在法国也很受欢迎。 如果你在这个网站上搜索Tsukasa, Hojo，你会发现目前的法语版本。  
在一些地方，有读者的评论，如果你懂法语，请阅读这些评论。  
如果你对英语很熟悉，你也可以使用免费的翻译网站。  
◆[AltaVista - World / Translate](http://babel.altavista.com/)  
一个外国翻译网站。 它将法语翻译成英语。  
如果是英文，你可以通过[ライコス](http://translation.lycos.co.jp/)や[インフォシーク](http://www.infoseek.co.jp/Honyaku?pg=honyaku_top.html&svx=100302&svp=SEEK)等的免费翻译进一步翻译成日文…但可能是奇怪的、没有意义的日文…^^;  

## [Amazon.com](http://www.amazon.com/)

在线专业书店亚马逊的主站（它在网上的名字?）。  
有RAIJIN COMICS版本的CITY HUNTER，画册和动漫DVD。 
RAIJIN COMICS的封面设计就像美国的漫画书一样华而不实。  

[BACK](./links_fan.md)  
[NEXT](./tankoubon.md)  
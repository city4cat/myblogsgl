INDEX > 付録 > 関連資料

# 画集・Mook  

※除非另有说明，所有价格都包括税金（截至2002年）。  

---

　週刊少年Jump特別版　 	JUMP COMICS DELUXE 	★  
　北条司Illustrations  
出版社 	集英社  
定価 	1,000日元  
初版年 	1991年  
ISBN 	4-08-858150-4  
A4大小/106页  
彩色+单色  
	

第一本彩色插图集，以CAT'S EYE、CITY HUNTER为重点，但也包括以前短篇小说的插图。 该书还包括新的海报和插图、对北条先生的采访以及与动漫工作人员的圆桌讨论。  
收录了「SPLASH！」的Part 1～4，这是一个不定期的短篇，在「Super Jump」杂志上以彩色形式连载。  
封面插图的獠（当然）超级酷！我想这是一个很好的例子。利用这幅插图，用图片说明了制作彩色插图的过程。 (在AH连载开始之前，它也被用作北条司的官方网站的横幅。 哦! 我希望它还在…)（译注：该段待核实）  

顺便说一下，我也买了鸟山明的同系列画册…也很棒。(我曾有一段时间沉迷于DRAGON BALL ^^;）  

---

	★  
　北条司漫画家20周年記念画册  
出版社 	集英社  
定価 	2,400日元(不含税)  
初版年 	2000年  
ISBN 	4-08-782598-1  
A4大小/104页  
颜色+2种颜色？   
	

华丽的画册，有精装版和盒子。  
主要插图是F.COMPO的。 紫苑从盒子的切口处探出头来，FC的粉丝们会喜欢的。  
还包括连载结束后画的CITY HUNTER和CAT'S EYE的插图，以及其他短篇和「SPLASH! 5」。 还有一个对漫画家的有趣采访，其中他回顾了他的漫画生涯，并谈到了他对新事务（即目前的COAMIX和Bunch）的感受。  

---

　愛蔵版漫画 	★  
　城市猎人完全指南（译注：需确认该书中文译名）  
出版社 	集英社  
定価 	781日元(不含税)  
初版年 	2000年  
ISBN 	4-08-782038-6  
A5大小/192页  
彩色+单色  
	

顾名思义，这是一本关于CITY HUNTER的人物、剧情和背景的完整指南。 (它可能被称为指南，但只有骨灰级粉丝才会买它^^;）  
这本书不仅介绍了CH，还包括对作者的采访，其中他谈到了F.COMPO的制作和他作为漫画家的生活。  
獠和香的封面插图是在该连载期间画的，所以相貌与CH时有很大不同，但我认为它巧妙地让人想起了JC第1卷的封面插图。  

但这样的书在连载8年后才出来，是很罕见的。类似的书籍也曾为「乌龙派出所」、「浪客剑心」和「棋魂」出版过，但它们是在连载期间或刚结束后出版的。<s>我猜测</s>这证明了CH粉丝的不离不弃。    

---

　週刊少年Jump特別版 	10月25日号・Jump精选集６ 	★  
　城市猎人动漫特辑  
出版社 	集英社  
定価 	550日元  
初版年 	1991年  
A4大小/126页  
彩色+单色   	

一本包含所有四部TV动画系列信息的mook。  
除了工作人员、演员、设定材料和其他与动画有关的数据外，还有北条先生、神谷先生和伊倉小姐的工作人员座谈会，以及编剧遠藤明範写的迷你小说（参见 [「Another CITY HUNTER」の項参照](../ch/chnovel.md)）价格为550日元。 介绍人物和著名剧集的文章使用了角色设计师神村幸子的大量分镜原画。 还有第101话的一个场景。  

它还配有一张1米宽的北条司的原画海报。 顺便说一下，已经出现的人物是:  
冴羽撩/槇村香/野上冴子/海坊主/美樹/野上麗香  
除了常规成员，客串角色有:    
麻上亜希子/西九条紗羅/Bloody・Mary/松村渚/浅香亜美/浅香麻美/索尼娅・菲尔德/阿尔玛/萨琳娜  
一共15人。在众多的客串角色中，为什么会选择这九个人呢…当然，因为他们在动画中出现过，但也许因为他们也是先生最喜欢的角色。（译注：人名待核实）  

---

　週刊少年Jump特別版 		★  
　城市猎人　动画漫画 The Secret Service （前編・後編）  
出版社 	
発行：ホーム社  
発売：集英社  
定価 	每本690日元  
初版年 	1996年1月24日  
? 尺寸（约为一本教科书的大小）  
136页    
彩色  
	

这是TV特辑「The Secret Service」的漫画版。 (这在宫崎骏的动画中也很常见）。  
前・後編都有一个插图…这张插图不是我喜欢的…(^^;) （译注：本句待核实） 
顺便说一下，我听说在同一系列中还有一部「Goodbye My Sweetheart」…但我没有买到。（译注：本句待核实）  

---

　ピアノ弾き語り 		★  
　城市猎人歌曲集　ORIGINAL SOUND TRACK  
発行 	東京音楽書院  
製作協力 	株式会社EPIC・Sony  
定価 	1,545日元(含税)  
初版年 	1990年10月25日  
ISBN 	4-8114-1671-6  
118ページ  
モノクロ  
	

収録曲： CITY HUNTER ～愛よ消えないで～/ゴーゴーヘブン/ANGEL NIGHT ～天使のいる場所～/セイラ/RUNNING TO HORIZON/FOOT STEPS/WHAT'S GOIN' ON/NO NO NO/EARTH ～木の上の方舟～/終りのない傾き/MR. PRIVATE EYE/GIVE ME YOUR LOVE TONIGHT/PARTY DOWN/SUPER GIRL/GET WILD/STILL LOVE HER ～失われた風景～/熱くなれたら
（全17曲）  

这是为钢琴创作的TV系列1～3的OP/ED和插曲集。 如果你对自己的技能有信心，这并不难。   

---

　ピアノ弾き語り 		
　城市猎人歌曲集２　VOCAL MASTER  
発行 	東京音楽書院  
定価 	1,545日元(含税)  
	

収録曲： HOLD ME TIGHT/LONELY LULLABY/WITHOUT YOU/砂のCASTLEのカサノヴァ/CITY HEAT/COOL CITY/SNOW LIGHT SHOWER/熱くなれたら/NAME OF THE GAME/BLUE AIR MESSAGE/街中sophisticate/MAKE UP TONIGHT/RUNNING TO HORIZON/YOUR SECRETS/CHANCE  
（全15曲）  

---

　ピアノ弾き語り 		
　Extra・City Hunter [劇場版]　ORIGINAL MOTION PICTURE SOUNDTRACK  
発行 	東京音楽書院  
定価 	1,236日元(含税)  
	

収録曲： 週末のソルジャー/ASPHALT DANDY/ONE AND ONLY/TROOP IN MIDNIGHT/ONLY IN MY DREAMS/GET OUT!/LOVE GAMES/WISH YOU'RE HERE/NINA/十六夜（じゅうろくや）  
（全10曲）  

---

別冊宝島829号 		★  
　完全保存版　Cat's Eye最強读本  
出版社 	宝島社  
定価 	953日元(不含税)  
初版年 	2003年  
ISBN 	4-7966-3439-8  
A4大小/143页  
彩色+2色    
	
一本描述Cat's Eye系列动画的mook。  
副标题是「对所有73话的深入介绍，充满了神秘、情感、（笑声）和兴奋」。 这无疑是当今最强大的Cat's Eye资料。  
它包括对每个主要人物的解释、各话的故事、工作人员和演员、以及设定资料(如机械和舞台)，还有原创的醒目的水彩画（线稿）。 作家们犀利(?)的评论和意见让人觉得很有趣。 还有一些有趣的栏目，如「这次的裂缝」和「费洛蒙注射」。（译注：「今回の谷間」、「フェロモンショット」不知如何翻译）    

---

別冊宝島862号 		★  
　完全保存版　City Hunter最強読本  
出版社 	宝島社  
定価 	952日元(不含税)  
初版年 	2003年  
ISBN 	4-7966-3504-1  
A4大小/143页  
彩色+2色    
	

本手册是CityHunter系列动画的指南，包括四部TV动画片、三部剧场版和三部TV特别节目。 对于该系列的粉丝来说，这是一本全面的书。  
在对导演児玉、动画导演北原先生和制片人植田先生的采访中，有一些相当有趣的言论。  
关于枪支的设定资料很丰富，但很遗憾的是，可以看到错误…  

---

		★  
　北条司漫画家生活25周年記念　自选Illustration100  
出版社 	徳間書店  
定価 	2800日元(不含税)  
初版年 	2005年  
ISBN 	4-19-8621012  
A4尺寸?/108页  
	

这是一本画册，主要来自于Angel・Heart，从已出版的众多插图中精心挑选。
 这本书装订得很好，我会把它推荐给以前没有买到画册的人，特别是那些已经成为AH粉丝的人。当然，对于老粉来说，这本书也值得买（尽管我不建议那些不喜欢AH的人买）。 书末对漫画家的采访很有趣。  

---

[BACK](./tankoubon.md)  
[NEXT](./magazine.md)  

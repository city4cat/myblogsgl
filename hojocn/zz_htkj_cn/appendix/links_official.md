INDEX > 付録 > リンク

# 官方网站
## [北条司官方网站](http://www.hojo-tsukasa.com/)  {#}

这是北条司先生的官方网站。  
该网站有关于单行本的信息、过去的作品清单、以及出售原创商品的部分，但对漫迷来说，最好的东西是他们可以相互交流信息的BBS。  

## [漫画周刊★Coamix](http://www.coamix.co.jp/)  {#}

这是「漫画周刊」的官方网站，「Angel Heart」目前正在这里连载。 你可以看到本周的出版信息。  
还有一个部分列出了所有以前的封面。  

## [集英社](http://www.shueisha.co.jp/index_f.html)  {#}

出版少年周刊Jump、MANGA ALLMAN（2002年7月停刊）和其他刊物。  

## [新潮社网站](http://www.shinchosha.co.jp/index.html)  {#}

这是新潮社的网站，是漫画周刊的出版商。 你可能已经熟悉在夏天出版的「新潮文库100本」。  

## [児玉兼嗣的制作笔记](http://www5e.biglobe.ne.jp/~tetyou/)  {#}

这是电视剧「CITY HUNTER」1-3、电影和视频系列的导演児玉兼嗣的网站。  
目前该网站主要包含与「名侦探柯南」有关的信息。  

## [欢迎来到屋根裏部屋](http://www5e.biglobe.ne.jp/~yaneura/)  {#}

这是神村幸子的网站，她负责电视剧「CITY HUNTER」1-3和电影的角色设计和绘图。  
这里有一份她的作品清单，但该网站的亮点是她美丽的插图。 还有一个板块，她解释了如何绘画，所以如果你是一个插画家，值得一游。  
在CITY HUNTER部分，展示了原始图纸和故事板。  

[BACK](./apptop.md)  
[NEXT](./links_fan.md)  
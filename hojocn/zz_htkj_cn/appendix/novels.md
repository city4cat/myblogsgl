INDEX > 付録 > 関連資料

# 小説

※除非另有说明，所有价格都包括税金（截至2002年）。  
※关于每件作品的内容，请参考第2章的「[Another CAT'S EYE](../ce/ceanother.md)」、第3章的「[Another CITY HUNTER](../ch/chnovel.md)」。  

---

　JUMP J BOOKS 		★  
　CAT'S EYE  
小説 	高屋敷英夫  
插图 	北条司  
出版社 	集英社  
定価 	760日元  
初版年 	1996年12月18日  
	
収録作品  
 	「I LOVE 瞳」  
	「圣夜・来自天堂的礼物」  

该书最开始有9页漫画序言。  

---

　JUMP J BOOKS 		★  
　CITY HUNTER  
小説 	外池省ニ  
插图 	北条司  
出版社 	集英社  
定価 	760日元  
初版年 	1993年4月30日  
	
収録作品  
 	「通往真理的路线」  
	「明日的复仇」  
	「玫瑰与枪」  

---

　JUMP J BOOKS 	★  
　CITY HUNTER 2  
小説 	稲葉稔  
插图 	北条司  
出版社 	集英社  
定価 	780日元  
初版年 	1997年4月29日  
	
収録作品  
 	「诱饵」  
	「陷阱」  

附有北条司的插图。  

---

　JUMP J BOOKS 		★  
　CITY HUNTER SPECIAL  
小説 	天羽沙夜  
插图 	文本中有卡通的图片  
出版社 	集英社  
定価 	760日元  
初版年 	1995年12月20日  
	
収録作品  
 	新写的  

TV特别节目「The・Secret・Service"」的小说版本。  
包括一张由北条司绘制的插图。  

---

　JUMP J BOOKS 		★  
　CITY HUNTER SPECIAL 2  
小説 	岸間信明  
插图 	文本中有卡通的图片  
出版社 	集英社  
定価 	780日元  
初版年 	1999年4月7日  
	
収録作品  
 	「CITY HUNTER SPECIAL II」  

电视特别节目「紧急现场直播！凶犯冴羽獠的最后日子」的小说版。   
包括一张由北条司绘制的插图。  

[BACK](./magazine.md)  
[NEXT](./cd.md)  
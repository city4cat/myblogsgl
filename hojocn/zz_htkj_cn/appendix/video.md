INDEX > 付録 > 関連資料

# 视频

※除非另有说明，否则所有价格均含税（截止到2002年）。  

Jump to : [[劇場版・Special](./video.md#special) | [真人电影](./video.md#movie)]  


## TV剧

都可以从JVC获得。 所有这些产品现在都只能在商店里买到，而不是在租赁商店里。 它们也曾以LD发行，但现在都已绝版。    
有一些剧集还没有被录制。关于每个视频内容的更多信息，请参考Saeba Ryo的"[Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/)"。  

◆CityHunter　Vol.1～10  

这些剧集是按照CH1的类别分组的（不是按照播出顺序），每卷有三集。  

◆CityHunter2　Vol.1～10  

这些剧集是按照CH2的类别分组的（不是按照播出顺序），每卷有四集。  

◆CityHunter3　全6巻  

CH3的所有13集都包括在内。  

◆CityHunter'91　全6巻  

CH'91的所有13集都包括在内。  

◆CityHunter Special Request　全2巻  

CH1、2里未收录的六集。  

◆City Hunter Super Magnum Version　全8巻  

上述CH1、2的「Special Request」未收录的剧集，现在又按照不同的类别收录在内。  


## 劇場版・Special  {#special}  

---

★
　爱与宿命的连发枪  
発売元 	日本Victor  
発売年 	1989年  
定価 	\12,020(不含税)  
编码 	JHE 0225  
	

包含剧场版的全部内容。  

---

★
　Original Special 2　海湾城市之战  
発売元 	日本Victor  
発売年 	1990年  
定価 	\9,200(不含税)  
编码 	JHE 0285  
	
它也有英文版本（有字幕）。（进入Amazon.co.jp或Amazon.com，搜索"CITY HUNTER"。￥2,394。便宜。^^;）  

---

★
　Original Special 3　百万美元阴谋  
発売元 	日本Victor  
発売年 	1990年  
定価 	\9,200(不含税)  
编码 	JHE 0286  
	

它也有英文版本（有字幕）。（进入Amazon.co.jp或Amazon.com，搜索"CITY HUNTER"。）包装上的标语很搞笑…  
<font color=#660066>"A babe needs a bodyguard.  
An assassin needs glory.  
City Hunter needs the cash."</font>  
<font color=#004d99>（女人需要保镖。刺客需要荣誉。City Hunter需要钱。）</font>  

---

★
　CITY HUNTER Special - The Secret Service  
発売元 	日本Victor  
発売年 	1996年  
定価 	\12,800(不含税)  
编码 	VVAR 1022  
	

包装上的插图是由北条司绘制的。  
它包括从未放映过的特典片段和预告片，所以即使你在TV上看过，也要看看。  
还有一个英文版本（带字幕）。 外套是神村幸子的赛璐珞画。我听说獠的名字叫Joe…  

---

★
　CITY HUNTER Special - Goodbye My Sweetheart  
発売元 	バップ   
発売年 	1997年  
定価 	\3,800(不含税)  
编码 	VPVY64612  
	
似乎没有任何特典视频。  

---

★
　CITY HUNTER Special　紧急直播!?凶恶罪犯寒羽良的死  
発売元 	バップ  
発売年 	1999年  
编码/定価 	VPVY64613／\9,800（不含税）  
VPVY64965／￥18,600  
(2册套装，不含税)  
	
似乎没有任何特典视频。  


## 真人电影  {#movie}

---

　CAT'S EYE  
出版商 	King Records  
出版年 	1998年  
定価 	\4,700(不含税)  
编码 	KIBF-43  
	

CAT'S EYE的真人版由内田有纪主演。  
顺便说一下，还有一个名为「THE CAT'S SECRET」的拍摄视频。  

---

　CITY HUNTER (城市猟人)  
出版商 	Pony Canyon / Suntory  
出版年 	1995年  
定価 	\3,689(不含税)  
编码 	
PCVX10352（字幕版）  
PCVX10353（配音版）  
	
成龙的真人电影。  
包括影片中未包括的场景和NG场景。  

---

[BACK](./cd.md)  
[NEXT](./dvd.md)  


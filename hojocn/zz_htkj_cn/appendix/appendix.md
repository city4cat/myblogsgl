INDEX >

# 附录

## 链接
[ [官方网站](./links_official.md) | [粉丝网站](./links_fan.md) | [其他](./links_other.md) ]

相关链接。  
（[关于链接到HTKJ](../start.md#link)）

[奢华的专栏「100个问题」](../column/col_100.md)  

## 相关资料
[ [单行本](./tankoubon.md) | [画集・Mook](./gasyu.md) | [杂志](./magazine.md) | [小説](./novels.md) | [CD](./cd.md) | [视频](./video.md) | [DVD](./dvd.md) | [卡带/CDPook](./cassette.md) |[其他](./otherbooks.md) | [Web Site](./websites.md) ]

编写本「研究序説」时使用的相关资料和参考资料的清单。  
（请注意，这并不是一份相关资料的详尽清单。）  

※那些在标题右侧标有★的，是我实际买了的。  


## HTKJ制作的幕后花絮
[2002年2月～6月](./productionnote1.md)[2002年7月～9月](./productionnote2.md)[2002年10月～12月](./productionnote3.md)

这是对从开始写手稿到出版第一版期间的记录。  
有很多事情我无法写在「表」里。  
(它们是按日期顺序排列的。)  

[HOME](../index.md)  
[NEXT](./links_official.md)  
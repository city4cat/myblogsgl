INDEX >

# Angel Heart　 天使・心  

随着2001年5月15日的周刊COMIC BUNCH的创刊号的推出，这个CITY HUNTER的续集开始在杂志上连载。 然而，它在许多方面与CH的设定不同，作者说它是一个平行宇宙。   
据说这是北条司「构思5年」、「20年漫画生涯的集大成者」，但不幸的是，其内容必须说是相当草率的。  
在这里，我想讨论一下我觉得Angel Heart存在的一些问题和它今后的展望。  

拒绝。  
这里的这些话真的是发自我内心。 因此，如果你喜欢这部作品、被它感动，我很抱歉。 如果你是这样的读者，请不要读这篇文章。 如果你看完后觉得不舒服，我不负责任......（听起来像某位作者的评论......^^;)   
纯粹的天使心应援站、更详细的信息，我们推荐Saeba Ryo的网站「[JMC](http://www.netlaputa.ne.jp/~arcadia/ah/index.html)」。  

（请注意，本章主要是根据截至2002年12月29日的连载内容编写的。 未来的发展可能会有部分工作与内容不一致。 请提前了解这一点。）  
掲載： 	新潮社 週刊COMIC BUNCH 2001年創刊号～連載中  
収録： 	  
新潮社 BUNCH COMICS「Angel Heart」 (刊行中)  
新潮社 Angel Heart 汇编（週刊COMIC BUNCH8月16日増刊号）  


## CONTENTS  
■	[Story & Main Charactors](./ahstory-charactors.md)  
	：简介和主要人物  
■ 	[My First Impression](./ahfirst.md)  
	：Angel Heart连载开始4日后写的、第一印象。  
■ 	[Angel HeartはCITY HUNTERの続編か?](./ahsequel.md)  
	：以「续集」为关键词考察了与CH世界的关系  
| [连载开始前的进展](./ahsequel.md) 
| [平行世界的含义](./ahparallel.md) 
| [另一个主人公的相貌](./ahface.md)  

■ 	[Angel Heart 随想](./ahessay.md)  
	：关于AH的随想  

[BACK](../index.md)  
[NEXT](./ahstory-charactors.md)  
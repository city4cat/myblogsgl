INDEX > CITY HUNTER > ミニ研究 >

# 人物简介 / 野上 冴子
## 基本信息

<table class="mt11" border="1">
  <tbody>
    <tr>
      <td class="nh8" width="90" valign="bottom" bgcolor="#F0F8FF"><font color="#003366">假名</font></td>
      <td class="nh8" valign="bottom">ノガミ　サエコ</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">全名</td>
      <td class="t11">野上 冴子</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">生年月日</td>
      <td class="t11">不明</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">年齢</td>
      <td class="t11" valign="top">不明</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">性別</td>
      <td class="t11">女</td>
    </tr>
    <tr>
      <td class="tit11" bgcolor="#F0F8FF">血液型</td>
      <td class="t11">不明</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">住所</td>
      <td class="t11">不明（独自生活在一个公寓里）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">職業</td>
      <td class="t11">
      <p>警視庁特捜科<sup>*</sup>的刑警。级别：警部補。（译注：待校对）<br>
      <span style="font-size : 9pt;">* 动画中，它是特别调查部，但这里与原作的最初设定相同。 顺带一提，这两个部门都不存在。</span></p>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">身体的特徴</td>
      <td class="t11">她被描述为与香差不多高，但她真的(?)可能没有那么高。 体重和3围不详（动画中胸围为87）。 头发披肩，有一缕分叉的流动刘海。 她是一个很性感的美女。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">別名</td>
      <td class="t11">没有特别的。<br>
      （唯香的小说中，她被称为「警視庁之女豹」或…？）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">家庭</td>
      <td class="t11">父亲（警察局长）、母亲、妹妹（麗香、唯香、双胞胎姐妹）…换句话说，是5个姐妹中的老大。 单身。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">最喜欢的枪</td>
      <td class="t11">New Nambu M60（由警方提供）。</td>
    </tr>
  </tbody>
</table>

## 経歴

过去槇村做侦探的时候，她和槇村是有名的二人组，据说没有他们不能破的案子。 他们当时的绰号是「大都会警察局的月亮和勺子」。（译注：待校对）  
她和槇村显然爱得很深，以至于他们考虑结婚。  
另一方面，不知道何时何地，但她似乎遇到了獠，并一度发生了爱情，但他们现在是朋友。  

## 特殊技能

-    刀：几把小刀总是用皮带绑在腿上。 请看她的高超技艺!  
-    射撃  
-    利用獠(^^;)  

## 性格

冷静。骄傲的女人（褒义）。她曾经用"女人"作为武器…她似乎是一个职业女性，为她的工作而活着，但实际上…？（译注：待校对）  

## 弱点

不详。 (唯香的取材？父亲包办婚姻的打击？）

## 兴趣爱好

不明  

[BACK](./chchara-kaori.md)  
[NEXT](./chchara-umibozu.md)  
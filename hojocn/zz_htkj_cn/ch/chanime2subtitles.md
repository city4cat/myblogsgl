INDEX > CITY HUNTER > アニメーション >

# CITY HUNTER 2

(译注：[城市猎人 - Wikipedia](https://zh.wikipedia.org/wiki/城市猎人)里的中文标题包含：中文标题、香港TVB標題。下表采用其中的"中文标题"。)  

<CENTER>
<TABLE cellpadding="0" width="90%">
  <TBODY>
    <TR>
      <TD bgcolor="#191970">
      <TABLE border="1" cellpadding="2" width="100%">
        <TBODY>
          <TR>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">放送日</TD>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">話数</TD>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">Subtitle（译注：每集标题）</TD>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">脚本/構成</TD>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">故事板</TD>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">演出</TD>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">动画导演</TD>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">Guest</TD>
            <TD bgcolor="#F0F8FF" align="center" class="tit10">原作</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/2</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">52</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠是未婚夫?!<BR>
            占卜出來的邂逅戀情(前編)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">手塚明美：国生さゆり</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">23</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/9</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">53</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠是未婚夫?!<BR>
            占卜出來的邂逅戀情(後編)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">いのまたむつみ</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">手塚篤：菊池英博</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">23</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/16</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">54</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">阿香被盯上了!!<BR>
            愛的言語是再見(前編)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">銀狐：山寺宏一</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">25</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/23</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">55</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">阿香被盯上了!!<BR>
            愛的言語是再見(後編)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">銀狐：山寺宏一</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">25</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/30</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">56</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">好色的偵探!?<BR>
            美女作家的推理遊戲</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">鎌田秀美</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">神志那弘志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">中山梓：佐久間レイ</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">5/7</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">57</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠是裸體模特兒?!<BR>
            偷竊名畫的美人畫家</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">鎌田秀美</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">いのまたむつみ</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤野遙：富沢美智恵</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">5/14</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">58</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">南方島嶼來的委託<BR>
            椰子姑娘與愛的樂園</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">遠藤明範</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">磯野智</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">南野ちひろ：及川ひとみ</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">5/21</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">59</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">回過神發現是危險的刑警<BR>
            這裡是東京啊，老爸！</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">遠藤明範</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">蒲生真知子：伊藤美紀</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">5/28</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">60</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">超速的戀情!<BR>
            與美女交警的手銬交流</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">星山博之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">磯野智</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">清美：室井深雪</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">6/4</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">61</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">嚴禁風流?!<BR>
            公主的高貴靈氣(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">阿露玛公主：小林由利</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">27</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">6/11</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">62</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">嚴禁風流?!<BR>
            公主的高貴靈氣(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">沙莉娜：原えりこ</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">27</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">6/18</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">63</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">場外是血腥的搏鬥!!<BR>
            戀愛的眼鏡蛇固定</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">神志那弘志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">赤松久美：横沢啓子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">6/25</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">64</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">最新走私工作!!<BR>
            美人牙醫師的愛情治療</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">大野木寛</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">磯野智</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">海音寺さゆり：川島千代子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">7/2</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">65</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">愛能超越金錢?<BR>
            風流男對勤儉女</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">井上敏樹</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">宝田翔子：堀江美都子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">7/9</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">66</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">不要死！海怪!!<BR>
            愛與復仇的麥格農(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">遠藤明範</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">本橋秀之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Catherine Hayward：鶴ひろみ</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">7/16</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">67</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">不要死！海怪!!<BR>
            愛與復仇的麥格農(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">遠藤明範</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Dandy Jack：仲木隆司</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">7/23</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">68</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">立志成為大和撫子?!<BR>
            風流無國界(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">日暮裕一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">磯野智</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">美佐・Williams：冬馬由美</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">7/30</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">69</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">立志成為大和撫子?!<BR>
            風流無國界(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">日暮裕一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Dimethyl ：鈴木泰明</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">8/6</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">70</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">阿香大活躍!!<BR>
            用值得回憶的紀念酒乾杯</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">あまのまあ</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">本橋秀之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">良之介：宮内幸平</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">8/13</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">71</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">阿香喪失記憶!!<BR>
            再見了，可愛的夥伴</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">鎌田秀美</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">神志那弘志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">徹：辻村真人</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">8/20</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">72</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">目標是得金牌!!<BR>
            射擊美人的密集訓練</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">倉橋聖子：大城松美</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">9/3</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">73</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">目標是獠!<BR>
            攝影美女最愛冒險(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">冬野葉子：篠原恵美</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">29</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">9/10</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">74</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">目標是獠!<BR>
            攝影美女最愛冒險(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">組長：田中康郎</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">29</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">9/17</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">75</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">必殺 桃色吐息?!<BR>
            女忍者的大城市物語</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">井上敏樹</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">般若のかえで：丸尾知子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">9/24</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">76</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">被盯上的證人!<BR>
            男裝美女與危險的二人(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">本橋秀之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">柏木圭子：松岡洋子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">30</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">10/1</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">77</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">被盯上的證人!<BR>
            男裝美女與危險的二人(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">神志那弘志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">殺し屋：山寺宏一</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">30</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">10/8</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">78</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">槙村送來的口信<BR>
            回憶是永遠的(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">遠藤明範<BR>
            小林準治</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Kelly Nelson：田原アルノ</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">10/15</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">79</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">槙村送來的口信<BR>
            回憶是永遠的(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">遠藤明範<BR>
            小林準治</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Gilmour：広瀬正志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">10/22</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">80</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">遍體鱗傷的純情!<BR>
            砲灰對天使的祈禱</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">遠藤明範</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">利根川麻衣子：中島千里</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">10/29</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">81</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠你就被騙吧!<BR>
            冴子送給少女的禮物</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">鎌田秀美</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高橋久美子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">少女：鈴木砂織</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">11/5</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">82</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">再見了，朋友…<BR>
            響徹心底的最後槍響(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Angela：さとうあい</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">11/12</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">83</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">再見了，朋友…<BR>
            響徹心底的最後槍響(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">西山明樹彦</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">神志那弘志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Robert Harrison：堀秀行</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">11/19</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">84</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">風流的契約成立!<BR>
            獠與不良少女的旅途(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">日暮裕一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">鮎原ひろみ：上村典子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">11/26</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">85</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">風流的契約成立!<BR>
            獠與不良少女的旅途(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">日暮裕一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">森村さくら：坂本千夏</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">12/3</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">86</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠是指導員<BR>
            究極的風流戰法</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">鎌田秀美</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高橋久美子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">立花恵美：日高のり子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">12/10</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">87</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠是美食家!<BR>
            製麵美人的性感宣傳</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">井上敏樹</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">美加：皆口裕子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">12/17</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">88</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠對香…<BR>
            讓耶誕節充滿著愛(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">アミノテツロウ</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">西山明樹彦</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Yuriko：川村万梨阿</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">12/24</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">89</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠對香…<BR>
            讓耶誕節充滿著愛(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">神志那弘志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">人吉：村松康雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">89 1/14</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">90</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">海兄是帥哥!<BR>
            美女殺手美樹的逼婚(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">本橋秀之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">美樹：小山茉美</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">32</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">1/21</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">91</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">海兄是帥哥!<BR>
            美女殺手美樹的逼婚(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">母親：神代智恵</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">32</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">1/28</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">92</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">一身風流!<BR>
            獠的心與超能力少女(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山口美浩</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">北原健雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">亜紀子：藤田淑子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">33</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">2/4</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">93</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">一身風流!<BR>
            獠的心與超能力少女(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">佐藤千春</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">紗羅：杉山佳寿子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">33</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">2/11</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">94</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠是愛情小偷!<BR>
            藏在魔鏡裡的愛的行蹤(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">麻生霞：富永みーな</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">34</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">2/18</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">95</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠是愛情小偷!<BR>
            藏在魔鏡裡的愛的行蹤(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">西山明樹彦</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高橋久美子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">崇司：塩沢兼人</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">34</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">2/25</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">96</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">十九歲的寡婦!<BR>
            繪畫美女心中的情人(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">本橋秀之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">芹沢綾子：斉藤庄子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">31</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">3/4</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">97</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">十九歲的寡婦!<BR>
            繪畫美女心中的情人(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">政一：戸谷公次</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">31</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">3/11</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">98</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">想談場夢一般的戀愛<BR>
            12歲天使的作戰(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)江上　潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">牧原梢：山野さと子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">36</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">3/18</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">99</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">想談場夢一般的戀愛<BR>
            12歲天使的作戰(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">西山明樹彦</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高橋久美子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">牧原友佳：小沢かおる</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">36</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">3/25</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">100</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">再見了，冷酷無情的城市(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">本橋秀之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高原ルミ：高木早苗</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/1</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">101</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">再見了，冷酷無情的城市(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">山崎和男</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高原幸一郎：秋元洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/8</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">102</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">水晶的預言!<BR>
            阿香的復活記憶之鎖</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)外池省二</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">西沢晋</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">老婆：堀絢子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/15</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">103</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">第二十年的重逢冴羽!<BR>
            妹妹就拜託你了(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)江上　潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高橋久美子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">立木小白合：榊原良子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">35</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/22</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">104</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">第二十年的重逢冴羽!<BR>
            妹妹就拜託你了(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">組長：田中康郎</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">35</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">4/29</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">105</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">十七歲的求婚<BR>
            疑惑的約會恐慌(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">佐藤千春</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">武田季実子：三輪勝恵</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">37</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">5/6</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">106</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">十七歲的求婚<BR>
            疑惑的約會恐慌(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">上杉貴子：佐久間なつみ</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">37</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">5/13</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">107</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">新宿的成功故事<BR>
            鄰居是位美女舞者(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">本橋秀之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">次原舞子：小口久仁子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">28</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">5/20</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">108</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">新宿的成功故事<BR>
            鄰居是位美女舞者(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">冨永恒雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">小林ゆかり</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">伏見：鈴木泰明</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">28</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">5/27</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">109</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠的斯巴達式教育!<BR>
            在後宮長大的小鬼頭王子(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">加瀬充子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">西沢晋</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Nario：白石冬美</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">6/3</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">110</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">獠的斯巴達式教育!<BR>
            在後宮長大的小鬼頭王子(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">平野靖士</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高岡希一</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Grease：仲木隆司</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">6/10</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">111</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">愛是一種魔法<BR>
            魔術師小姐的男性恐懼症</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">外池省二</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">江上潔</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">高橋久美子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Naomi：三浦雅子</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">-</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">6/17</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">112</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">祝你好運，我的殺手<BR>
            兩人的城市街道(前篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">冨永恒雄</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">小林ゆかり</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Mary：高坂真琴</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">39</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">6/24</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">113</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">祝你好運，我的殺手<BR>
            兩人的城市街道(中篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">港野洋介</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">藤本義孝</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">本橋秀之</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">David：石丸博也</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">39</TD>
          </TR>
          <TR>
            <TD bgcolor="#FFFFFF" align="center" class="t10">7/1</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">114</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">祝你好運，我的殺手<BR>
            兩人的城市街道(後篇)</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">(構)儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">儿玉兼嗣</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">今西隆志</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">谷口守泰</TD>
            <TD bgcolor="#FFFFFF" align="center" class="t10">Eric：若本規夫</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">39</TD>
          </TR>
        </TBODY>
      </TABLE>
      </TD>
    </TR>
  </TBODY>
</TABLE>
</CENTER>

[BACK](./chanime1subtitles.md)  
[NEXT](./chanime3_91subtitles.md)  
INDEX > CITY HUNTER > Another CITY HUNTER >

# Game

## PC Engine
「City Hunter」  
制造商 	：Sun電子  
発売年 	：1990年  
价格 	：6300日元  
这似乎是一个普通的冒险游戏。   
我记得很久以前在一家二手商店里惊讶地发现了一个：PC Engine（NEC制造的16位游戏机，1987年发布。 其对应的是世嘉的Mega Drive。… 我没有买它，因为我没有。懂的人才能明白^^;）。 …现在想想，我想它是很价值的资料…（但我不会玩）。（译注：待校对）  

## PC软件 

一款基于「The Secret Service」的游戏发布了。  

*

以上两点在Saeba Ryo的"[Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/)"中有详细介绍，如果你有兴趣，请参考那里。  

## Famicom 软件（译注：待校对）
「Famicom Jump」（译注：待校对）  
制造商 	：Bandai   
发售时间 	：1988年  
价格 	：?  
为纪念周刊少年Jump 20周年而制作的冒险游戏，其中包括到那时为止的所有流行人物。 我认为獠和香是游戏中的人物之一。
在那之后肯定还做过几次。  
但我从来没有玩过。  

[BACK](./chmovie.md)  
[NEXT](./chfrench.md)  
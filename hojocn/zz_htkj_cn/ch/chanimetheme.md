INDEX > CITY HUNTER > アニメーション >

# 主題歌一覧

## TV Series

<center>
<table width="90%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
      <table width="100%" cellpadding="2" border="1">
        <tbody>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#F0F8FF"><font color="#F0F8FF">-</font>番組Title</td>
            <td colspan="3" class="tit10" bgcolor="#F0F8FF" align="center">Opening</td>
            <td colspan="3" class="tit10" bgcolor="#F0F8FF" align="center">Ending</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Title</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Artist</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">放送話数</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Title</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Artist</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">放送話数</td>
          </tr>
          <tr>
            <td class="tit10" rowspan="2" bgcolor="#F0F8FF">CITY HUNTER</td>
            <td class="t10" bgcolor="#ffffff">City Hunter<br>
            ～爱，不要消失</td>
            <td class="t10" bgcolor="#ffffff">小比類巻かほる</td>
            <td class="t10" bgcolor="#ffffff">1-26</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">GET　WILD</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">TM NETWORK</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">1-51</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">ゴーゴーヘブン</td>
            <td class="t10" bgcolor="#ffffff">大沢誉志幸</td>
            <td class="t10" bgcolor="#ffffff">27-51</td>
          </tr>
          <tr>
            <td class="tit10" rowspan="2" bgcolor="#F0F8FF">CITY HUNTER 2</td>
            <td class="t10" bgcolor="#ffffff">Angel Night<br>
            ～天使所在的地方</td>
            <td class="t10" bgcolor="#ffffff">PSY.S</td>
            <td class="t10" bgcolor="#ffffff">52-77</td>
            <td class="t10" bgcolor="#ffffff">Super Girl <br>
            -CITY HUNTER 2-</td>
            <td class="t10" bgcolor="#ffffff">岡村靖幸</td>
            <td class="t10" bgcolor="#ffffff">52-88</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">セイラ<sup>[^*]</sup></td>
            <td class="t10" bgcolor="#ffffff">FENCE OF DEFENCE</td>
            <td class="t10" bgcolor="#ffffff">78-114</td>
            <td class="t10" bgcolor="#ffffff">STILL LOVE HER<br>
            (失落的风景）</td>
            <td class="t10" bgcolor="#ffffff">TM NETWORK</td>
            <td class="t10" bgcolor="#ffffff">89-114</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF">CITY HUNTER 3</td>
            <td class="t10" bgcolor="#ffffff">RUNNING TO HORIZON</td>
            <td class="t10" bgcolor="#ffffff">小室哲哉</td>
            <td class="t10" bgcolor="#ffffff">115-127</td>
            <td class="t10" bgcolor="#ffffff">熱くなれたら</td>
            <td class="t10" bgcolor="#ffffff">鈴木聖美</td>
            <td class="t10" bgcolor="#ffffff">115-127</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF">CITY HUNTER '91</td>
            <td class="t10" bgcolor="#ffffff">DOWN TOWN GAME</td>
            <td class="t10" bgcolor="#ffffff">GWINKO</td>
            <td class="t10" bgcolor="#ffffff">128-140</td>
            <td class="t10" bgcolor="#ffffff">SMILE &amp; SMILE</td>
            <td class="t10" bgcolor="#ffffff">AURA</td>
            <td class="t10" bgcolor="#ffffff">128-140</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
  </tbody>
</table>
</center>

[^*]: 在一些CD上，「セイラ」被写成SARA。  
在这个表格中，我们沿用了我们作为原始材料的动画mook书中的记号。  

(参考：City Hunter Anime Special)  


## 映画・Special

<center>
<table width="90%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
      <table width="100%" cellpadding="2" border="1">
        <tbody>
          <tr>
            <td class="tit10" rowspan="2" bgcolor="#F0F8FF"><font color="#F0F8FF">-</font>番組Title</td>
            <td class="tit10" colspan="2" bgcolor="#F0F8FF" align="center">Opening</td>
            <td class="tit10" colspan="2" bgcolor="#F0F8FF" align="center">Ending</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Tile</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Artist</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Tile</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Artist</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF">愛与宿命的连发枪</td>
            <td class="t10" bgcolor="#ffffff">週末的Soldier</td>
            <td class="t10" bgcolor="#ffffff">金子美香</td>
            <td class="t10" bgcolor="#ffffff">十六夜（じゅうろくや）</td>
            <td class="t10" bgcolor="#ffffff">高橋真梨子</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF">Bay City Wars</td>
            <td class="t10" bgcolor="#ffffff" align="center">-</td>
            <td class="t10" bgcolor="#ffffff" align="center">-</td>
            <td class="t10" bgcolor="#ffffff">ROCK MY LOVE</td>
            <td class="t10" bgcolor="#ffffff">荻野目洋子</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF">百万美元阴谋</td>
            <td class="t10" bgcolor="#ffffff" align="center">-</td>
            <td class="t10" bgcolor="#ffffff" align="center">-</td>
            <td class="t10" bgcolor="#ffffff">MORE MORE しあわせ</td>
            <td class="t10" bgcolor="#ffffff">荻野目洋子</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF">The Secret Service</td>
            <td class="t10" bgcolor="#ffffff">otherwise</td>
            <td class="t10" bgcolor="#ffffff">KONTA</td>
            <td class="t10" bgcolor="#ffffff">WOMAN</td>
            <td class="t10" bgcolor="#ffffff">Anne Lewis</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF">Goodbye My Sweetheart </td>
            <td class="t10" bgcolor="#ffffff">RIDE ON THE NIGHT</td>
            <td class="t10" bgcolor="#ffffff">HUMMING BIRD</td>
            <td class="t10" bgcolor="#ffffff">GET WILD<br>
            (CITY HUNTER SPECIAL '97 VERSION)</td>
            <td class="t10" bgcolor="#ffffff">NAHO</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF">紧急直播 !? 凶恶罪犯冴羽獠的死</td>
            <td class="t10" bgcolor="#ffffff">illusion city</td>
            <td class="t10" bgcolor="#ffffff">sex MACHINEGUNS</td>
            <td class="t10" bgcolor="#ffffff">GET WILD</td>
            <td class="t10" bgcolor="#ffffff">TM NETWORK</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
  </tbody>
</table>
</center>

[BACK](./chanimesakuga.md)  
[NEXT](./chanimethemeimpress.md)  
INDEX > CITY HUNTER > ミニ研究 >

# 冴羽家的餐桌

CITY HUNTER是一部关于主角做清道夫、杀手的电影，但有相当多的家庭剧般的用餐场景。  
在这方面，作者在一次采访中说：    

<font color=#660066>
>    是为了让整个事情有一个真实的感觉。即便是漫画的主人公，也会吃饭、拉屎和睡觉。  
>    （「City Hunter Perfect Guide Book.」P106）  
</font>

他说。  
我认为这不仅给了他一种生活感，而且有助于赋予他个性。 这在第一个故事中已经是这样了，那是创造角色冴羽獠的过程，在接下来的故事「双刃剑」中，在等待罪犯的时候和击退罪犯之后，他从口袋里拿出一个烤红薯吃（笑）。…嗯，对于一个普通的杀手来说，这是常识(?) (试着给Golgo 13一个烤红薯作为例子）。 （译注：待校对）

因此，这里列出了CH中描绘的用餐场景。  

注：    
◆做出决定的人是基于当时的情况。    
◆除了一个项目外，茶、咖啡和其他饮料不包括在内。 顺便说一下，早餐通常包括咖啡。    


<center>
<table width="95%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
      <table width="100%" border="1">
        <tbody>
          <tr>
            <td class="tit9" width="7%" bgcolor="#F0F8FF" align="center">単行本(JC)</td>
            <td class="tit9" width="7%" bgcolor="#F0F8FF" align="center">Page</td>
            <td class="tit9" width="10%" bgcolor="#F0F8FF" align="center">用餐的人</td>
            <td class="tit9" width="5%" bgcolor="#F0F8FF" align="center">時間</td>
            <td class="tit9" width="30%" bgcolor="#F0F8FF" align="center">Menu</td>
            <td class="tit9" width="10%" bgcolor="#F0F8FF" align="center">用餐场所</td>
            <td class="tit9" width="10%" bgcolor="#F0F8FF" align="center">作饭的人</td>
            <td class="tit9" bgcolor="#F0F8FF" align="center">备注</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">短編集</td>
            <td class="t9" bgcolor="#ffffff" align="center">123</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包、沙拉、火腿（牛排？一些配菜)、Coffee</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff">他甚至在舔盘子!</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">138</td>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">带骨头的鸡肉？ 其他晚餐</td>
            <td class="t9" bgcolor="#ffffff" align="center">一流的酒店</td>
            <td class="t9" bgcolor="#ffffff" align="center">一流的酒店</td>
            <td class="t9" bgcolor="#ffffff">吃完后已经叠了大约10个盘子。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">163</td>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">碗里的一堆生鸡蛋，沙拉，火腿，面包</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">175</td>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">夜</td>
            <td class="t9" bgcolor="#ffffff">烤红薯</td>
            <td class="t9" bgcolor="#ffffff" align="center">小巷</td>
            <td class="t9" bgcolor="#ffffff" align="center">？</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">181</td>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">夜</td>
            <td class="t9" bgcolor="#ffffff">烤红薯</td>
            <td class="t9" bgcolor="#ffffff" align="center">小巷</td>
            <td class="t9" bgcolor="#ffffff" align="center">？</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">1巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">11</td>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包、鸡肉条、其他</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">41</td>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">肉骨头、苹果、香肠、其他</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">80</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、菜摘</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">肉类？ 香肠、意大利面条等</td>
            <td class="t9" bgcolor="#ffffff" align="center">SNACK Co Co</td>
            <td class="t9" bgcolor="#ffffff" align="center">SNACK Co Co</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">100</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">不明</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">2巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">10</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包・沙拉</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">71</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">？</td>
            <td class="t9" bgcolor="#ffffff">instant coffee</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">据说，煮沸10秒后关火的水是instant coffee的最佳选择。 (真的吗？)</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">119</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包・沙拉<br>鸡蛋、酸奶和牛奶红毛丹分装纳豆漂浮物(译注：待校对)</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff">…我真的喝过(译注：待校对)</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">156</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、沙也加</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">米饭、味噌汤、沙拉等约4道菜</td>
            <td class="t9" bgcolor="#ffffff" align="center">沙也加的家</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff">一顿丰盛的饭菜，连沙也加都赞不绝口。 甚至还做午餐盒。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">168</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">小吃（译注：待校对）</td>
            <td class="t9" bgcolor="#ffffff">咖喱面包</td>
            <td class="t9" bgcolor="#ffffff" align="center">沙也加的学校</td>
            <td class="t9" bgcolor="#ffffff" align="center"></td>
            <td class="t9" bgcolor="#ffffff">生产商：石坂面包</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">3巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">35</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">超大的热狗・沙拉</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff">第一次使用筷子!</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">105</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、由美子</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">通心粉和法式烩菜混在一起加肉加菜加牛奶，香菌、鱼、贝和500克里脊肉一起煮的杂烩菜<br>
            菜肉泡饭<br>
            薄饼<br>
            炒饭<br>
            三明治<br>
            5份雪糕和奶昔<br>
            番茄、菠萝、香蕉、苹果汁加10个鸡蛋混汽水</td>
            <td class="t9" bgcolor="#ffffff" align="center">西餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">西餐厅</td>
            <td class="t9" bgcolor="#ffffff">由美子点了和獠一样的东西，两个人都狼吞虎咽地吃了起来！…我不关心獠，但由美子…(^^;</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">150</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、由美子</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">像晚餐一样丰盛的早餐（和意大利面条）</td>
            <td class="t9" bgcolor="#ffffff" align="center">西餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">西餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">4巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">9</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、海坊主</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">意大利面条、牛排套餐？ 、葡萄酒、其他</td>
            <td class="t9" bgcolor="#ffffff" align="center">西餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">西餐厅</td>
            <td class="t9" bgcolor="#ffffff">獠对中途离席的海坊主说「你吃的太少啊」（…獠你就是个大胃王）（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">73</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、女学生</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐</td>
            <td class="t9" bgcolor="#ffffff">某种套餐？</td>
            <td class="t9" bgcolor="#ffffff" align="center">大学食堂</td>
            <td class="t9" bgcolor="#ffffff" align="center">大学食堂</td>
            <td class="t9" bgcolor="#ffffff">桌子被掀翻了，最终没能吃成。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">75</td>
            <td class="t9" bgcolor="#ffffff" align="center">優子</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐</td>
            <td class="t9" bgcolor="#ffffff">汉堡套餐？</td>
            <td class="t9" bgcolor="#ffffff" align="center">大学食堂</td>
            <td class="t9" bgcolor="#ffffff" align="center">大学食堂</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">114</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐？</td>
            <td class="t9" bgcolor="#ffffff">不明（约 4 个盘子）</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠？</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">5巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">179</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐？</td>
            <td class="t9" bgcolor="#ffffff">汉堡套餐？ 除主菜外还有 5 个盘子</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">6巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">13</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、陽子</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">不明</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">68</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">肉、香肠、炸虾和其他食物，大约20个盘子</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff">陽子也一起吃早餐，但她几乎没有碰过，因为她对獠的饮食习惯非常失望。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">130</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">肉類？</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">150</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">不明(大约4个盘子）</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">7巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">9</td>
            <td class="t9" bgcolor="#ffffff" align="center">香、渚</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">牛排套餐？</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff">不知道你整个上午都在吃什么…（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">10</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">牛排套餐？</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">酒店的餐厅</td>
            <td class="t9" bgcolor="#ffffff">渚的残羹剩饭（她几乎没吃）…(ーー;)（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">51</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包等（约5盘）</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">？</td>
            <td class="t9" bgcolor="#ffffff">没有描写吃的东西（译注：因为图片太小，看不清食物）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">114</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">像法式料理一样丰盛</td>
            <td class="t9" bgcolor="#ffffff" align="center"></td>
            <td class="t9" bgcolor="#ffffff" align="center">温子</td>
            <td class="t9" bgcolor="#ffffff">顺便说一下，同桌的香和拓也有一块面包…</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">174</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">煎蛋、沙拉、其他</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">？</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">8巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">134</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">汉堡包</td>
            <td class="t9" bgcolor="#ffffff" align="center">Donald Mug</td>
            <td class="t9" bgcolor="#ffffff" align="center">Donald Mug</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">9巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">29</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香</td>
            <td class="t9" bgcolor="#ffffff" align="center">？</td>
            <td class="t9" bgcolor="#ffffff">汉堡包</td>
            <td class="t9" bgcolor="#ffffff" align="center">Donald Mug</td>
            <td class="t9" bgcolor="#ffffff" align="center">Donald Mug</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">68</td>
            <td class="t9" bgcolor="#ffffff" align="center">悦子</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">像晚餐一样丰盛的早餐</td>
            <td class="t9" bgcolor="#ffffff" align="center">Pension Calypso</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff">作为试镜参加者的一顿饭。 顺便说一下，只有悦子1人吃了…</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">86</td>
            <td class="t9" bgcolor="#ffffff" align="center">悦子</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">牛排、沙拉和另一道菜</td>
            <td class="t9" bgcolor="#ffffff" align="center">Pension Calypso</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">10巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">40</td>
            <td class="t9" bgcolor="#ffffff" align="center">喜多川</td>
            <td class="t9" bgcolor="#ffffff" align="center">?</td>
            <td class="t9" bgcolor="#ffffff">牛排？　其他全部菜品（译注：待校对）</td>
            <td class="t9" bgcolor="#ffffff" align="center">喜多川邸</td>
            <td class="t9" bgcolor="#ffffff" align="center">?</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">126</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">?</td>
            <td class="t9" bgcolor="#ffffff">医院食品</td>
            <td class="t9" bgcolor="#ffffff" align="center">病室</td>
            <td class="t9" bgcolor="#ffffff" align="center">佐佐木综合医院</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">144</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">?</td>
            <td class="t9" bgcolor="#ffffff">医院食品</td>
            <td class="t9" bgcolor="#ffffff" align="center">病室</td>
            <td class="t9" bgcolor="#ffffff" align="center">佐佐木综合医院</td>
            <td class="t9" bgcolor="#ffffff">善美喂饭。 啊～～～</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">12巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">82</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">套餐？（大约10堆盘子…)（译注：待校对）</td>
            <td class="t9" bgcolor="#ffffff" align="center">JBS食堂</td>
            <td class="t9" bgcolor="#ffffff" align="center">JBS食堂</td>
            <td class="t9" bgcolor="#ffffff">「今天一整天被你们困在车内…害我食欲全无…」「我的食量与工作量成正比，工作量越大、吃得也就越多～」</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">117</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐?</td>
            <td class="t9" bgcolor="#ffffff">拉面</td>
            <td class="t9" bgcolor="#ffffff" align="center">九州拉面</td>
            <td class="t9" bgcolor="#ffffff" align="center">九州拉面</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">13巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">10</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包卷、沙拉、炸虾、其他（约三份…）</td>
            <td class="t9" bgcolor="#ffffff" align="center">明美的家</td>
            <td class="t9" bgcolor="#ffffff" align="center">明美</td>
            <td class="t9" bgcolor="#ffffff">一手拿着叉子吃饭…（ーー；）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">114</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">小吃</td>
            <td class="t9" bgcolor="#ffffff">炸鸡？</td>
            <td class="t9" bgcolor="#ffffff" align="center">路边长椅</td>
            <td class="t9" bgcolor="#ffffff" align="center">肯德基炸鸡</td>
            <td class="t9" bgcolor="#ffffff">海坊主变装（伪装？）成肯德基爷爷。真希被派去买食物。（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">14巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">9</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">小吃</td>
            <td class="t9" bgcolor="#ffffff">冰淇淋（双份）</td>
            <td class="t9" bgcolor="#ffffff" align="center">新宿散步时</td>
            <td class="t9" bgcolor="#ffffff" align="center">冰激凌店？</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">68</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">米饭、味噌汤、鱼等</td>
            <td class="t9" bgcolor="#ffffff" align="center">教授的家</td>
            <td class="t9" bgcolor="#ffffff" align="center">和江</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">15巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">59</td>
            <td class="t9" bgcolor="#ffffff" align="center">沙莉娜</td>
            <td class="t9" bgcolor="#ffffff" align="center">小吃</td>
            <td class="t9" bgcolor="#ffffff">水果冻糕</td>
            <td class="t9" bgcolor="#ffffff" align="center">公園</td>
            <td class="t9" bgcolor="#ffffff" align="center">?</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">73</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、阿露玛公主、沙莉娜</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐</td>
            <td class="t9" bgcolor="#ffffff">?（西式餐点）</td>
            <td class="t9" bgcolor="#ffffff" align="center">西餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">西餐厅</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">107</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包、牛排（？）、炸虾和另外两道菜。</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香?</td>
            <td class="t9" bgcolor="#ffffff">这个人早上照样吃什么…(ーー;)（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">16巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">10</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、舞子</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">米饭、味噌汤、煎蛋、烤鱼、小碗饭</td>
            <td class="t9" bgcolor="#ffffff" align="center">舞子的卧室</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠?</td>
            <td class="t9" bgcolor="#ffffff">未经允许就进入了舞子的卧室，并做了早餐。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">132</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐</td>
            <td class="t9" bgcolor="#ffffff">汉堡包</td>
            <td class="t9" bgcolor="#ffffff" align="center">公園</td>
            <td class="t9" bgcolor="#ffffff" align="center">Donald Mug</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">144</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、葉子</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">汉堡包？ 沙拉？ 汤？</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">葉子</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">17巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">10</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">獠：不明（7个盘子和一个汤杯），香：不明（1个盘子和一个汤杯）</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香?</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">55</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、葉子</td>
            <td class="t9" bgcolor="#ffffff" align="center">?</td>
            <td class="t9" bgcolor="#ffffff">不明（日本小餐馆可能会提供的东西）</td>
            <td class="t9" bgcolor="#ffffff" align="center">割烹</td>
            <td class="t9" bgcolor="#ffffff" align="center">割烹</td>
            <td class="t9" bgcolor="#ffffff">和葉子约会时去的那家餐馆。 异常体面。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">85</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、圭子</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">米饭、味噌汤、煎蛋（？）</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">141</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、圭子</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">不明（西餐，配米饭）</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">19巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">53</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、亜紀子、紗羅</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">像法式料理一样的丰盛菜品</td>
            <td class="t9" bgcolor="#ffffff" align="center">living</td>
            <td class="t9" bgcolor="#ffffff" align="center">亜紀子</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">93</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、亜紀子、紗羅</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">面包卷，汉堡包，沙拉，汤。</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">紗羅最喜欢的食物。 在亜紀子的教导下，香成功了。…但紗羅并没有吃。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">107</td>
            <td class="t9" bgcolor="#ffffff" align="center">香、紗羅</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐</td>
            <td class="t9" bgcolor="#ffffff">丸子（球状）、沙拉、米饭</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">獠因裸舞而被罚挨饿。 亜紀子的饭也在桌上，但她没有出现。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">20巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">88</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、小百合</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">不明（只有饭后的盘子）</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">145</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">便当?</td>
            <td class="t9" bgcolor="#ffffff">饭团</td>
            <td class="t9" bgcolor="#ffffff" align="center">監禁室</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">「越来越像獠了」（作者的评论）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">21巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">63</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、梢、友佳</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包卷，火腿和鸡蛋，沙拉</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">161</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、希実子、武田老人</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">米饭，味噌汤，主食（不明），腌菜(?)</td>
            <td class="t9" bgcolor="#ffffff" align="center">武田家dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">希実子</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">23巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">13</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、Mary</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">米饭、味噌汤、其他（不明）</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">香对Mary说他是个混混而感到愤怒。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">181</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、翔子</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">三明治、沙拉、其他（不明）</td>
            <td class="t9" bgcolor="#ffffff" align="center">翔子的家</td>
            <td class="t9" bgcolor="#ffffff" align="center">翔子</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">24巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">130</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、由加里、小由里</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">牛排(?)，米饭，沙拉</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">…每个人都在早上吃这种大餐，这一点令人惊讶。（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">25巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">107</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、麻美</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">三明治</td>
            <td class="t9" bgcolor="#ffffff" align="center">麻美的家</td>
            <td class="t9" bgcolor="#ffffff" align="center">麻美</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">26巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">48</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">小吃</td>
            <td class="t9" bgcolor="#ffffff">苹果、香蕉、甜瓜</td>
            <td class="t9" bgcolor="#ffffff" align="center">香的病房</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
            <td class="t9" bgcolor="#ffffff">探望香的果实。…整个瓜，不要带皮吃…(^^;)（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">73</td>
            <td class="t9" bgcolor="#ffffff" align="center">香、真由子、浦上</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">米饭、火腿和鸡蛋、沙拉、炸丸子（译注：待校对）</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">浦上爸爸「哇！好丰富的早餐！！　不知道有多少年没有吃这么好的家常菜了！」</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">101</td>
            <td class="t9" bgcolor="#ffffff" align="center">浦上</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">整只烤火鸡和其他丰盛的晚餐，并配有葡萄酒</td>
            <td class="t9" bgcolor="#ffffff" align="center">living</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">101</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">晚餐</td>
            <td class="t9" bgcolor="#ffffff">剩饭菜…(^^;)</td>
            <td class="t9" bgcolor="#ffffff" align="center">living</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">在橘盒上用餐。 可怜。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">27巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">132</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香</td>
            <td class="t9" bgcolor="#ffffff" align="center">小吃</td>
            <td class="t9" bgcolor="#ffffff">雪糕（香说是双球，獠说是三球…我不知道）（译注：待校对）</td>
            <td class="t9" bgcolor="#ffffff" align="center">21 ICE CREAM</td>
            <td class="t9" bgcolor="#ffffff" align="center">21 ICE CREAM</td>
            <td class="t9" bgcolor="#ffffff">约会时吃冰淇淋。 獠的冰淇淋就要掉了。</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">30巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">180</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠、香、美幸</td>
            <td class="t9" bgcolor="#ffffff" align="center">早餐</td>
            <td class="t9" bgcolor="#ffffff">面包卷，煎蛋，沙拉</td>
            <td class="t9" bgcolor="#ffffff" align="center">dining</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">31巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">54</td>
            <td class="t9" bgcolor="#ffffff" align="center">唯香</td>
            <td class="t9" bgcolor="#ffffff" align="center">小吃</td>
            <td class="t9" bgcolor="#ffffff">7个巧克力冻糕</td>
            <td class="t9" bgcolor="#ffffff" align="center">咖啡店</td>
            <td class="t9" bgcolor="#ffffff" align="center">咖啡店</td>
            <td class="t9" bgcolor="#ffffff" align="center">-</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">〃</td>
            <td class="t9" bgcolor="#ffffff" align="center">118</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐</td>
            <td class="t9" bgcolor="#ffffff">便当</td>
            <td class="t9" bgcolor="#ffffff" align="center">唯香的高校</td>
            <td class="t9" bgcolor="#ffffff" align="center">香</td>
            <td class="t9" bgcolor="#ffffff">唯香嘲笑獠的「愛妻便当」。 名字写的是洒…章鱼仔和锤子鹌鹑蛋…(^^;) 这是一个非常精细的艺术…有谁试过制作吗？（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t9" bgcolor="#ffffff" align="center">34巻</td>
            <td class="t9" bgcolor="#ffffff" align="center">184</td>
            <td class="t9" bgcolor="#ffffff" align="center">獠</td>
            <td class="t9" bgcolor="#ffffff" align="center">午餐?</td>
            <td class="t9" bgcolor="#ffffff">热狗，罐装果汁</td>
            <td class="t9" bgcolor="#ffffff" align="center">新宿站东口</td>
            <td class="t9" bgcolor="#ffffff" align="center">?</td>
            <td class="t9" bgcolor="#ffffff">在等待假CH</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
  </tbody>
</table>
</center>

[BACK](./chnamecall.md)  
[NEXT](./chessay01.md)  
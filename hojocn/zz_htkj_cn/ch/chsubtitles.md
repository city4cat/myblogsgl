INDEX > CITY HUNTER > エピソードガイド >

# Episode List　

点击每一集的编号No.，可以跳到「剧情简介&评论」部分的相关章节。（译注：原表格为"No"列，但表示"编号"应该用"No."）  

（译注：以下大多使用香港玉皇朝版译文。“乐生会”为台湾东立版译文。）  

<center>
<table width="95%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
      <table border="1">
        <tbody>
          <tr>
            <td colspan="2" class="tit10" width="25%" bgcolor="#F0F8FF" align="center">収録巻数</td>
            <td colspan="2" class="tit10" width="20%" bgcolor="#F0F8FF" align="center">Episode</td>
            <td rowspan="2" class="tit10" width="15%" bgcolor="#F0F8FF" align="center">Subtitle<br>(译注：中文版的每一话的标题)</td>
            <td rowspan="2" class="tit10" width="10%" bgcolor="#F0F8FF" align="center">Guest Heroine</td>
            <td rowspan="2" class="tit10" width="10%" bgcolor="#F0F8FF" align="center">主要的Guest</td>
            <td rowspan="2" class="tit9" width="5%" bgcolor="#F0F8FF" align="center">动画片的<br>相关<br>剧集</td>
            <td rowspan="2" class="tit10" bgcolor="#F0F8FF" align="center">备注</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Comics</td>
            <td class="tit9" width="5%" bgcolor="#F0F8FF" align="center">文庫本</td>
            <td class="tit10" bgcolor="#F0F8FF">No.</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Title</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#F0FFF0"><b>&lt;1&gt;<br>
            恐怖的天使尘</b><br>
            S60(1985)年13号～20号</td>
            <td class="t10" rowspan="8" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;1&gt;</b></td>
            <td class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode1-10.md#1">1</a></td>
            <td class="tit10" valign="middle" bgcolor="#FFFFEB">不光彩的心下！</td>
            <td class="t10" bgcolor="#FFFFEB">不光彩的心下！</td>
            <td class="t10" bgcolor="#FFFFEB">岩崎惠</td>
            <td class="t10" bgcolor="#FFFFEB">稲垣</td>
            <td class="t10" bgcolor="#FFFFEB" align="center">3</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode1-10.md#2">2</a></td>
            <td class="tit10" valign="middle" bgcolor="#ffffff">BMW恶魔</td>
            <td class="t10" bgcolor="#ffffff">BMW恶魔</td>
            <td class="t10" bgcolor="#ffffff">亜月菜摘</td>
            <td class="t10" bgcolor="#ffffff">BMWの男</td>
            <td class="t10" bgcolor="#ffffff" align="center">1</td>
            <td class="t10" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode1-10.md#3">3</a></td>
            <td class="tit10" valign="middle" bgcolor="#FFFFEB">黑暗中的杀手！</td>
            <td class="t10" bgcolor="#FFFFEB">黑暗中的杀手！</td>
            <td class="t10" bgcolor="#FFFFEB">槇村香</td>
            <td class="t10" bgcolor="#FFFFEB">贩卖人口的集团老板</td>
            <td class="t10" bgcolor="#FFFFEB" align="center">4</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode1-10.md#4">4</a></td>
            <td class="tit10" valign="middle" bgcolor="#ffffff">恐怖的天使尘</td>
            <td class="t10" bgcolor="#ffffff">恐怖的天使尘</td>
            <td class="t10" bgcolor="#ffffff">-</td>
            <td class="t10" bgcolor="#ffffff">乐生会执行人员</td>
            <td class="t10" bgcolor="#ffffff" align="center">5</td>
            <td class="t10" bgcolor="#ffffff">CD/磁带书「亲吻你的搭档…」中包含了部分内容</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode1-10.md#5">5</a></td>
            <td class="tit10" valign="middle" bgcolor="#FFFFEB">好拍档!</td>
            <td class="t10" bgcolor="#FFFFEB">好拍档!</td>
            <td class="t10" bgcolor="#FFFFEB">(槇村香)</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
            <td class="t10" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="3" valign="top" bgcolor="#ffffff"><b>&lt;2&gt;<br>
            将军的圈套!</b><br>
            S60(1985)年21号～29号</td>
            <td class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode1-10.md#6">6</a></td>
            <td class="tit10" valign="middle" bgcolor="#ffffff">将军的圈套!</td>
            <td class="t10" bgcolor="#ffffff">将军的圈套！</td>
            <td class="t10" bgcolor="#ffffff">(槇村香)</td>
            <td class="t10" bgcolor="#ffffff">将军<br>
            長老（Mayor）</td>
            <td class="t10" bgcolor="#ffffff" align="center">5</td>
            <td class="t10" bgcolor="#ffffff">只有第32話（译注：此句待核实）<br>
            CD/磁带书「亲吻你的搭档…」中包含了部分内容</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode1-10.md#7">7</a></td>
            <td class="tit10" valign="middle" bgcolor="#FFFFEB">不要股息坏人！！</td>
            <td class="t10" bgcolor="#FFFFEB">不要股息坏人！！</td>
            <td class="t10" bgcolor="#FFFFEB">(槇村香)</td>
            <td class="t10" bgcolor="#FFFFEB">龙神信夫（龙神会会長）</td>
            <td class="t10" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" bgcolor="#FFFFEB">仅第48話（译注：此句待核实）</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode1-10.md#8">8</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">危险的家庭教师<font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#ffffff">危险的家庭教师</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">龙神沙也加</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">10</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="4" valign="top" bgcolor="#F0FFF0"><b>&lt;3&gt;<br>
            裸足の女優</b><br>
            S60(1985)年30号～38号</td>
            <td class="t10" rowspan="9" valign="top" bgcolor="#ffffff" align="center"><b>&lt;2&gt;</b></td>
            <td class="t10" bgcolor="#ffffff">失衡的暴力</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode1-10.md#9">9</a></td>
            <td class="tit10" valign="middle" bgcolor="#FFFFEB">等待的少女</td>
            <td class="t10" bgcolor="#FFFFEB">等待的少女</td>
            <td class="t10" bgcolor="#FFFFEB">龙神沙也加</td>
            <td class="t10" bgcolor="#FFFFEB">萩尾直行<br>
            萩尾道子<br>
            松井</td>
            <td class="t10" bgcolor="#FFFFEB" align="center">7</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td rowspan="4" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode1-10.md#10">10</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#ffffff">裸足女星</td>
            <td class="t10" bgcolor="#ffffff">裸足女星</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">佐藤由美子</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">加納典宏<br>
            (海坊主)</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff" align="center">6</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">拍摄危机!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#ffffff"><b>&lt;4&gt;<br>
            随着钟声的命运 </b>!<br>
            S60(1985)年39号～47号</td>
            <td class="t10" bgcolor="#ffffff">动摇的心</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">射击亡灵!</td>
          </tr>
          <tr>
            <td rowspan="3" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode11-20.md#11">11</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#FFFFEB">随着钟声的命运!</td>
            <td class="t10" bgcolor="#FFFFEB">奇怪老师</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">片岡優子</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">四谷政男<br>
            優子の母<br>
            教授<br>
            赤川会長</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB" align="center">15</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">记忆中的吻痕</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">随着钟声的命运!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#F0FFF0"><b>&lt;5&gt;<br>
            千中挑一<br>
            </b>S60(1985)年48号～S61(1986)年6号</td>
            <td class="t10" rowspan="7" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;3&gt;</b></td>
            <td rowspan="4" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode11-20.md#12">12</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#ffffff">不许动那女子!</td>
            <td class="t10" bgcolor="#ffffff">不许动那女子!</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">野上冴子</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">勒尼-达宁<br>
            席洛蒙-达宁<br>
            (教授)</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff" align="center">8</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">冴子首次的活跃场面更接近于动画第59集的场面。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">千中挑一</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">执迷不悟!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">恐怖的男性触觉</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode11-20.md#13">13</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#FFFFEB">哀愁的赌博</td>
            <td class="t10" bgcolor="#FFFFEB">赌后!</td>
            <td class="t10" rowspan="2" bgcolor="#FFFFEB">牧野陽子</td>
            <td class="t10" rowspan="2" bgcolor="#FFFFEB">高宮<br>
            赌场吸血鬼-龙<br>
            岩瀬明</td>
            <td class="t10" rowspan="2" bgcolor="#FFFFEB" align="center">9</td>
            <td class="t10" rowspan="2" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="2" valign="top" bgcolor="#ffffff"><b>&lt;6&gt;<br>
            哀愁的赌博</b><br>
            S61(1986)年7号～15号</td>
            <td class="t10" bgcolor="#FFFFEB">哀愁的赌博</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode11-20.md#14">14</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">不放心那家伙!</td>
            <td class="t10" bgcolor="#ffffff">不放心那家伙!</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">松村渚</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">串田</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">13</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="3" valign="top" bgcolor="#F0FFF0"><b>&lt;7&gt;<br>
            来自危险国度的女人!<br>
            </b>S61(1986)年16号～24号</td>
            <td class="t10" rowspan="8" valign="top" bgcolor="#ffffff" align="center"><b>&lt;4&gt;</b></td>
            <td class="t10" bgcolor="#ffffff">爱是盲目!</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode11-20.md#15">15</a></td>
            <td class="tit10" valign="middle" bgcolor="#FFFFEB">来自危险国度的女人!</td>
            <td class="t10" bgcolor="#FFFFEB">来自危险国度的女人!</td>
            <td class="t10" bgcolor="#FFFFEB">川田温子</td>
            <td class="t10" bgcolor="#FFFFEB">松岡拓也<br>
            阿曼利亚的情报人员<br>
            拓也的父亲</td>
            <td class="t10" bgcolor="#FFFFEB" align="center">12</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td rowspan="3" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode11-20.md#16">16</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#ffffff">空中飞臀!</td>
            <td class="t10" bgcolor="#ffffff">空中飞臀!</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">麻生霞</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">不律乱寓<br>
            山多士</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff" align="center">11</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">在「爱与宿命的连发枪」中使用的梗。</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#ffffff"><b>&lt;8&gt;<br>
            天使的微笑<br>
            </b>S61(1986)年25号～33号</td>
            <td class="t10" bgcolor="#ffffff">飞行博士!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">在心中开放的郁金香</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode11-20.md#17">17</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">天使的微笑</td>
            <td class="t10" bgcolor="#FFFFEB">天使的微笑</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">皆川由貴</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">26</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">爱是什么？</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">阿獠的恋爱讲座</td>
          </tr>
          <tr>
            <td class="t10" rowspan="4" valign="top" bgcolor="#F0FFF0"><b>&lt;9&gt;<br>
            回忆中的海滩<br>
            </b>S61(1986)年34号～42号</td>
            <td class="t10" rowspan="6" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;5&gt;</b></td>
            <td class="t10" bgcolor="#FFFFEB">医生也好，草津的温泉也好!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">初恋</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode11-20.md#18">18</a></td>
            <td class="tit10" valign="middle" bgcolor="#ffffff">回忆中的海滩</td>
            <td class="t10" bgcolor="#ffffff">回忆中的海滩</td>
            <td class="t10" bgcolor="#ffffff">鹿島悦子</td>
            <td class="t10" bgcolor="#ffffff">小倉经理<br>
            日向敏夫<br>
            柴田監督</td>
            <td class="t10" bgcolor="#ffffff" align="center">19</td>
            <td class="t10" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode11-20.md#19">19</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#FFFFEB">抢回来的一个新娘</td>
            <td class="t10" bgcolor="#FFFFEB">抢回来的一个新娘</td>
            <td class="t10" rowspan="2" bgcolor="#FFFFEB">名取和江</td>
            <td class="t10" rowspan="2" bgcolor="#FFFFEB">喜多川英二<br>
            喜多川直也<br>
            (教授)</td>
            <td class="t10" rowspan="2" bgcolor="#FFFFEB" align="center">23</td>
            <td class="t10" rowspan="2" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="2" valign="top" bgcolor="#ffffff"><b>&lt;10&gt;<br>
            不可碰护士啊!<br>
            </b>S61(1986)年43号～51号</td>
            <td class="t10" bgcolor="#FFFFEB">危险解药</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode11-20.md#20">20</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">不可碰护士啊!</td>
            <td class="t10" bgcolor="#ffffff">不可碰护士啊!</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">岩井善美</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">高塚秀司</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">24</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="6" valign="top" bgcolor="#F0FFF0"><b>&lt;11&gt;<br>
            槇村遗下的东西<br>
            </b>S61(1986)年52号～S62(1987)年10号</td>
            <td class="t10" rowspan="11" valign="top" bgcolor="#ffffff" align="center"><b>&lt;6&gt;</b></td>
            <td class="t10" bgcolor="#ffffff">再见后...说声你好!</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode21-30.md#21">21</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">槇村遗下的东西</td>
            <td class="t10" bgcolor="#FFFFEB">某夜的过失</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">野上冴子</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">人口贩卖组织的老板</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">14</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">段子用在动画片#21、「百万美元的阴谋」中</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">胸围大作战!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">槇村遗下的东西</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">獠的受难日</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">獠的纯情故事</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#ffffff"><b>&lt;12&gt;<br>
            充满麻烦的独家新闻!<br>
            </b>S62(1987)11号～19号</td>
            <td rowspan="3" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode21-30.md#22">22</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#ffffff">充满麻烦的独家新闻!</td>
            <td class="t10" bgcolor="#ffffff">美女新闻报道员的实力</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">結城礼子</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">-</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff" align="center">35</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">充满麻烦的独家新闻!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">礼子的紧急新闻报告</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode21-30.md#23">23</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">灰姑娘的梦…</td>
            <td class="t10" bgcolor="#FFFFEB">相逢、恋爱、占卜</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">手塚明美</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">手塚篤<br>
            銀狐</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">52-53</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">灰姑娘的梦…</td>
          </tr>
          <tr>
            <td class="t10" rowspan="6" valign="top" bgcolor="#F0FFF0"><b>&lt;13&gt;<br>
            海坊主委托的工作<br>
            </b>S62(1987)20号～28号</td>
            <td class="t10" rowspan="11" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;7&gt;</b></td>
            <td class="t10" bgcolor="#FFFFEB">黑暗中的咔嚓!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">来！笑一个!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">最讨厌占卜!?</td>
          </tr>
          <tr>
            <td rowspan="3" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode21-30.md#24">24</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#ffffff">最后的演奏会</td>
            <td class="t10" bgcolor="#ffffff">海坊主委托的工作</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">氷室真希</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">(海坊主)<br>
            Snake</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff" align="center">27-28</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">毒蛇出现!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">最后的演奏会</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#ffffff"><b>&lt;14&gt;<br>
            阿香！加油!!<br>
            </b>S62(1987)29号～37号</td>
            <td rowspan="3" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode21-30.md#25">25</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#FFFFEB">阿香！加油!!</td>
            <td class="t10" bgcolor="#FFFFEB">狙击阿香!?</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">(槇村香)</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">銀狐<br>
            (教授)<br>
            (名取和江)</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB" align="center">54-55</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">SAYONARA…</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">阿香！加油!!</td>
          </tr>
          <tr>
            <td rowspan="4" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode21-30.md#26">26</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#ffffff">刚迁到这里来的女人</td>
            <td class="t10" bgcolor="#ffffff">刚迁到这里来的女人</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">野上麗香</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">友村刑事<br>
            深町警部<br>
            黒川警視正<br>
            (野上冴子)-</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff" align="center">41-42</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">已作出行动的犯人</td>
          </tr>
          <tr>
            <td class="t10" rowspan="7" valign="top" bgcolor="#F0FFF0"><b>&lt;15&gt;<br>
            机场示爱<br>
            </b>S62(1987)38号～46号</td>
            <td class="t10" rowspan="14" valign="top" bgcolor="#ffffff" align="center"><b>&lt;8&gt;</b></td>
            <td class="t10" bgcolor="#ffffff">爱情大作战!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">妒嫉一线间</td>
          </tr>
          <tr>
            <td rowspan="4" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode21-30.md#27">27</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#FFFFEB">机场示爱</td>
            <td class="t10" bgcolor="#FFFFEB">“后宫”地狱</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">阿露玛公主<br>
            沙莉娜</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">马鲁麦斯大臣<br>
            (野上麗香)</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB" align="center">61-62</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">东京约会的紧急出动</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">绑架公主!?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">机场示爱</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode21-30.md#28">28</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#ffffff">舞子的私人保镖</td>
            <td class="t10" bgcolor="#ffffff">隔壁的新邻居</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">次原舞子</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">宝石泥棒二人組<br>
            伏見</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff" align="center">107-108</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">在「爱与宿命的连发枪」中使用的段子</td>
          </tr>
          <tr>
            <td class="t10" rowspan="7" valign="top" bgcolor="#ffffff"><b>&lt;16&gt;<br>
            情人是城市猎人<br>
            </b>S62(1987)47号～S63(1988)5号</td>
            <td class="t10" bgcolor="#ffffff">舞子的私人保镖</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">随着本能趋向!?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">要让我保护你吗 !?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">遥远的成功之路 !?</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode21-30.md#29">29</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">情人是城市猎人</td>
            <td class="t10" bgcolor="#FFFFEB">情人是城市猎人</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">冬野葉子</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">关东云龙会的会長<br>
            关东云龙会的会長的儿子（政弘）<br>
            (野上冴子)</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">73-74</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">站在门后的表白</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">秘密兼职的你!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="7" valign="top" bgcolor="#F0FFF0"><b>&lt;17&gt;<br>
            黎明时分的回忆<br>
            </b>S63(1988)6号～15号</td>
            <td class="t10" rowspan="14" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;9&gt;</b></td>
            <td class="t10" bgcolor="#FFFFEB">执迷不悔的叶子!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">黎明时分的回忆</td>
          </tr>
          <tr>
            <td rowspan="4" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode21-30.md#30">30</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#ffffff">危险的一对!</td>
            <td class="t10" bgcolor="#ffffff">迟来的春天</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">柏木圭子（圭一）</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">(野上冴子)</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff" align="center">76-77</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">阿香初次约会</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">危险的一对!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">请原谅我…</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode31-40.md#31">31</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">大家姐是女大学生!</td>
            <td class="t10" bgcolor="#FFFFEB">墓地里的色情狂</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">芹沢綾子</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">政一<br>
            大利根</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">96-97</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="7" valign="top" bgcolor="#ffffff"><b>&lt;18&gt;<br>
            情迷海坊主!!<br>
            </b>S63(1988)16号～25号</td>
            <td class="t10" bgcolor="#FFFFEB">大家姐是女大学生!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">哀伤的寡妇</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">火树银花!！</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">白色画布</td>
          </tr>
          <tr>
            <td rowspan="4" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode31-40.md#32">32</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#ffffff">情迷海坊主!!</td>
            <td class="t10" bgcolor="#ffffff">情迷海坊主！!</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">美樹</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">-</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff" align="center">90-91</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">「出浴的女子」，作者海因茨，载于JCP 154</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">有史以来最大型战术!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">海坊主的爱情</td>
          </tr>
          <tr>
            <td class="t10" rowspan="6" valign="top" bgcolor="#F0FFF0"><b>&lt;19&gt;<br>
            悲哀天使<br>
            </b>S63(1988)26号～35号</td>
            <td class="t10" rowspan="13" valign="top" bgcolor="#ffffff" align="center"><b>&lt;10&gt;</b></td>
            <td class="t10" bgcolor="#ffffff">海坊主所爱的女人</td>
          </tr>
          <tr>
            <td rowspan="3" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode31-40.md#33">33</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#FFFFEB">悲哀天使</td>
            <td class="t10" bgcolor="#FFFFEB">悲哀天使</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">西九条紗羅<br>
            麻上亜紀子</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">西九条定光<br>
            (海坊主)</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB" align="center">92-93</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">CD/磁带书「悲哀天使」。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">吃醋的沙罗</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">给我勇气!</td>
          </tr>
          <tr>
            <td rowspan="4" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode31-40.md#34">34</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#ffffff">“空中的飞臀”重现!</td>
            <td class="t10" bgcolor="#ffffff">“空中的飞臀”重现 !</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">麻生霞</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">崇司<br>
            麻生弥生</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff" align="center">94-95</td>
            <td class="t10" rowspan="4" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">被俘虏的兴奋状态</td>
          </tr>
          <tr>
            <td class="t10" rowspan="7" valign="top" bgcolor="#ffffff"><b>&lt;20&gt;<br>
            默许离别的那一方…<br>
            </b>S63(1988)36号～38号、45号～50号</td>
            <td class="t10" bgcolor="#ffffff">小偷争霸战开始!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">镜子的秘密</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode31-40.md#35">35</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">默许离别的那一方…</td>
            <td class="t10" bgcolor="#FFFFEB">忧愁的亲姐妹</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">立木小白合</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">(野上冴子)</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">103-104</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">连载时发布在#36之后</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">姐妹情深</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">藏于戒指中的梦想</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">冴羽獠，出动吧 !!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">默许离别的那一方…</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#F0FFF0"><b>&lt;21&gt;<br>
            住宅区的呼唤信息<br>
            </b>S63(1988)39号～44号、51・52号、S64(1989)1・2合併号</td>
            <td class="t10" rowspan="10" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;11&gt;</b></td>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode31-40.md#36">36</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">住宅区的呼唤信息</td>
            <td class="t10" bgcolor="#ffffff">住宅区的呼唤信息</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">牧原梢<br>
            牧原友佳</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">岩館<br>
            (野上冴子)</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">98-99</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">梦中憧憬的男士</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode31-40.md#37">37</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">求婚狂想曲!?</td>
            <td class="t10" bgcolor="#FFFFEB">水手服恐惧症!</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">武田季实子</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">武田老人<br>
            求婚者3人組<br>
            上杉貴子<br>
            (野上麗香)</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">105-106</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">求婚狂想曲!?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">分手总在倾心时!?</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#ffffff"><b>&lt;22&gt;<br>
            把蟒蛇交给淑女！<br>
            </b>S64(1989)3・4合併号、H元年(1989)8号、16号～21号</td>
            <td class="t10" bgcolor="#FFFFEB">愤怒的麦林手枪!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">17年来初逢祖母</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode31-40.md#38">38</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">把蟒蛇交给淑女!</td>
            <td class="t10" bgcolor="#ffffff">把蟒蛇交给淑女!</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">神村愛子</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">神村幸一郎</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">118-119</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">连载时发表在#39之后</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">天下无敌的女侦探诞生!?</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode31-40.md#39">39</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">明日的重生</td>
            <td class="t10" bgcolor="#FFFFEB">性感屁股的主人</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">Bloody Mary</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">艾力<br>
            大卫・高飞<br>
            (海坊主)<br>
            (野上冴子)<br>
            (美樹)</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">112-114</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="6" valign="top" bgcolor="#F0FFF0"><b>&lt;23&gt;<br>
            明日的重生<br>
            </b>H元年(1989)9号～15号、22号～24号</td>
            <td class="t10" rowspan="14" valign="top" bgcolor="#ffffff" align="center"><b>&lt;12&gt;</b></td>
            <td class="t10" bgcolor="#FFFFEB">流泪的生日</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">难明女人心…</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">爱比枪更有力!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">明日的重生</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode31-40.md#40">40</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#ffffff">天空任飞翔</td>
            <td class="t10" bgcolor="#ffffff">害怕高飞!?</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">天野翔子</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">蝙蝠</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff" align="center">128</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">不会飞的杀手</td>
          </tr>
          <tr>
            <td class="t10" rowspan="8" valign="top" bgcolor="#ffffff"><b>&lt;24&gt;<br>
            天空任飞翔<br>
            </b>H元年(1989)25号～34号</td>
            <td class="t10" bgcolor="#ffffff">Fright, Flight (恐怖的飞行)</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">民航机场'89</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">天空任飞翔</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode41-50.md#41">41</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">天使遗忘的礼物!?</td>
            <td class="t10" bgcolor="#FFFFEB">天使遗忘的礼物 !?</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">真柴由加里<br>
            真柴小由里</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">蝙蝠<br>
            (海坊主)</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">令人憧憬的背影</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">香！加油努力!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">决斗！蝙蝠VS獠!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">向背影说再见!!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="8" valign="top" bgcolor="#F0FFF0"><b>&lt;25&gt;<br>
            Play it again, Mami !　-再奏一次那首歌!-<br>
            </b>H元年(1989)35号～44号</td>
            <td class="t10" rowspan="13" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;13&gt;</b></td>
            <td rowspan="6" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode41-50.md#42">42</a></td>
            <td class="tit10" rowspan="6" valign="middle" bgcolor="#ffffff">Play it again, Mami ! -再奏一次那首歌!-</td>
            <td class="t10" bgcolor="#ffffff">委托人是幽灵!?</td>
            <td class="t10" rowspan="6" bgcolor="#ffffff">浅香麻美<br>
            浅香亜美</td>
            <td class="t10" rowspan="6" bgcolor="#ffffff">巷道之魔</td>
            <td class="t10" rowspan="6" bgcolor="#ffffff" align="center">132-133</td>
            <td class="t10" rowspan="6" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">双面娃娃!?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">惊悚的黑暗约会</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">镜中的另一个我!?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">火炎中的钢琴家!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">Play it again, Mami ! -再奏一次那首歌!-</td>
          </tr>
          <tr>
            <td rowspan="3" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode41-50.md#43">43</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#FFFFEB">美女与野兽!?</td>
            <td class="t10" bgcolor="#FFFFEB">美女与野兽!?</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">伍藤梓</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">伍藤広喜</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">海港的决斗!!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#ffffff"><b>&lt;26&gt;<br>
            突然的邂逅!!<br>
            </b>H元年(1989)45号～H2(1990)3・4合併号</td>
            <td class="t10" bgcolor="#FFFFEB">泪的街头募捐</td>
          </tr>
          <tr>
            <td rowspan="3" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode41-50.md#44">44</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#ffffff">突然的邂逅!!</td>
            <td class="t10" bgcolor="#ffffff">突然的邂逅!!</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">浦上真由子</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">浦上（真由的父亲）<br>
            泷川护士<br>
            北條</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff" align="center">x</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">被撕裂的心</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">斩不断的情感</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode41-50.md#45">45</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#FFFFEB">如果阿香换上了泳衣!?</td>
            <td class="t10" bgcolor="#FFFFEB">我的爱人，獠!?</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">北原絵梨子</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">品味差的老板<br>
            (野上冴子)<br>
            (海坊主)<br>
            (美樹)</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" rowspan="5" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="8" valign="top" bgcolor="#F0FFF0"><b>&lt;27&gt;<br>
            都市的灰姑娘!!<br>
            </b>H2(1990)5号～14号</td>
            <td class="t10" rowspan="15" valign="top" bgcolor="#ffffff" align="center"><b>&lt;14&gt;</b></td>
            <td class="t10" bgcolor="#FFFFEB">如果阿香换上了泳衣!?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">美丽的焦点!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">外套的秘密!?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">华丽的脱身!!</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode41-50.md#46">46</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">都市的灰姑娘!!</td>
            <td class="t10" bgcolor="#ffffff">都市的灰姑娘!! -前篇-</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">(槇村香)</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">北原絵梨子</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">137</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">都市的灰姑娘!! -後篇-</td>
          </tr>
          <tr>
            <td rowspan="6" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode41-50.md#47">47</a></td>
            <td class="tit10" rowspan="6" valign="middle" bgcolor="#FFFFEB">最好的礼物!!</td>
            <td class="t10" bgcolor="#FFFFEB">想杀的男人!!</td>
            <td class="t10" rowspan="6" bgcolor="#FFFFEB">桑妮亚・菲特</td>
            <td class="t10" rowspan="6" bgcolor="#FFFFEB">肯尼・菲特<br>
            浦上真由子<br>
            (海坊主)<br>
            (美樹)</td>
            <td class="t10" rowspan="6" bgcolor="#FFFFEB" align="center">135-136</td>
            <td class="t10" rowspan="6" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">黎明的决断!!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="7" valign="top" bgcolor="#ffffff"><b>&lt;28&gt;<br>
            胜败的一线!!<br>
            </b>H2(1990)15号～25号</td>
            <td class="t10" bgcolor="#FFFFEB">过去的伤痕</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">决斗开始!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">胜败的一线!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">最好的礼物!!</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode41-50.md#48">48</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#ffffff">两个城市猎人</td>
            <td class="t10" bgcolor="#ffffff">祖父出现了!?</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">神宮寺遙</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">神宮寺道彦<br>
            黒蜥蜴<br>
            (海坊主)</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff" align="center">x</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">只在「The Secret Service」中使用的段子</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">獠的未婚妻受袭 !?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">被诱拐的胸围（附带老伯）!!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="7" valign="top" bgcolor="#F0FFF0"><b>&lt;29&gt;<br>
            伊集院隼人平稳渡过的一天<br>
            </b>H2(1990)26号～34号</td>
            <td class="t10" rowspan="13" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;15&gt;</b></td>
            <td class="t10" bgcolor="#ffffff">卑鄙的陷阱!?</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">两个城市猎人</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode41-50.md#49">49</a></td>
            <td class="tit10" valign="middle" bgcolor="#FFFFEB">伊集院隼人平稳渡过的一天</td>
            <td class="t10" bgcolor="#FFFFEB">伊集院隼人平稳渡过的一天</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
            <td class="t10" bgcolor="#FFFFEB">(海坊主)</td>
            <td class="t10" bgcolor="#FFFFEB" align="center">134</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td rowspan="3" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode41-50.md#50">50</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#ffffff">冴子的相亲!!</td>
            <td class="t10" bgcolor="#ffffff">冴子的相亲!!</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">（槇村香）</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">北尾裕貴<br>
            (野上冴子)</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff" align="center">129</td>
            <td class="t10" rowspan="3" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">哥哥的余像!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">被修改了的剧本!!</td>
          </tr>
          <tr>
            <td rowspan="4" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode51-60.md#51">51</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#FFFFEB">将记忆抹去…</td>
            <td class="t10" bgcolor="#FFFFEB">拼命的拍档</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">及川優希（優希・格蕾斯）</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">畠岡<br>
            阿斯巴王子<br>
            使用催眠术的杀手</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB" align="center">130</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="6" valign="top" bgcolor="#ffffff"><b>&lt;30&gt;<br>
            将记忆抹去…<br>
            </b>H2(1990)36号～45号</td>
            <td class="t10" bgcolor="#FFFFEB">失去的过去!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">呼唤死亡的暗示!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">将记忆抹去…</td>
          </tr>
          <tr>
            <td rowspan="5" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode51-60.md#52">52</a></td>
            <td class="tit10" rowspan="5" valign="middle" bgcolor="#ffffff">唱颂暗号的女人!?</td>
            <td class="t10" bgcolor="#ffffff">心型记号的逃脱者!?</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">小林美幸</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">南加尔西亚特工<br>
            (野上冴子)</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff" align="center">131</td>
            <td class="t10" rowspan="5" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">恐怖的速运!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">唱颂暗号的女人!?</td>
          </tr>
          <tr>
            <td class="t10" rowspan="8" valign="top" bgcolor="#F0FFF0"><b>&lt;31&gt;<br>
            二人同心!!<br>
            </b>H2(1990)46号～H3(1991)5号</td>
            <td class="t10" rowspan="9" valign="top" bgcolor="#ffffff" align="center"><b>&lt;16&gt;</b></td>
            <td class="t10" bgcolor="#ffffff">惊慌大逃亡!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">亲密伙伴!!</td>
          </tr>
          <tr>
            <td rowspan="4" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode51-60.md#53">53</a></td>
            <td class="tit10" rowspan="4" valign="middle" bgcolor="#FFFFEB">二人同心!!</td>
            <td class="t10" bgcolor="#FFFFEB">獠与可怕的姐妹花!!</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">野上唯香</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">野上警察局长<br>
            反政府恐怖分子<br>
            (野上冴子)<br>
            (野上麗香)</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" rowspan="4" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">父亲来袭!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">懦夫!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">二人同心!!</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode51-60.md#54">54</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">上床十次的阴谋!?</td>
            <td class="t10" bgcolor="#ffffff">上床十次的阴谋!?</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">平山希美子（美樹）</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">毒针鼠<br>
            (海坊主)</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">x</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">错失机会!!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="2" valign="top" bgcolor="#ffffff"><b>&lt;32&gt;<br>
            奇妙的关系!!<br>
            </b>H3(1991)6号～16号</td>
            <td class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode51-60.md#55">55</a></td>
            <td class="tit10" valign="middle" bgcolor="#FFFFEB">奇妙的关系!!</td>
            <td class="t10" bgcolor="#FFFFEB">奇妙的关系!!</td>
            <td class="t10" bgcolor="#FFFFEB">（槇村香）</td>
            <td class="t10" bgcolor="#FFFFEB">Mick・Angel</td>
            <td class="t10" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="9" valign="top" bgcolor="#F5F5F5" align="center"><b>&lt;17&gt;</b></td>
            <td class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode51-60.md#56">56</a></td>
            <td class="tit10" valign="middle" bgcolor="#ffffff">照片的回忆!!</td>
            <td class="t10" bgcolor="#ffffff">照片的回忆!!</td>
            <td class="t10" bgcolor="#ffffff">（槇村香）</td>
            <td class="t10" bgcolor="#ffffff">不世井重工会長<br>
            (槇村秀幸)<br>
            (Mick・Angel)</td>
            <td class="t10" bgcolor="#ffffff" align="center">x</td>
            <td class="t10" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" rowspan="6" valign="top" bgcolor="#F0FFF0"><b>&lt;33&gt;<br>
            向地狱出发!!<br>
            </b>H3(1991)17号～27号</td>
            <td rowspan="8" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode51-60.md#57">57</a></td>
            <td class="tit10" rowspan="8" valign="middle" bgcolor="#FFFFEB">泪的鍊咀</td>
            <td class="t10" bgcolor="#FFFFEB">伤感的旅程!!</td>
            <td class="t10" rowspan="8" bgcolor="#FFFFEB">（槇村香）</td>
            <td class="t10" rowspan="8" bgcolor="#FFFFEB">海原神<br>
            Bloody・Mary<br>
            (Mick・Angel)<br>
            (海坊主)<br>
            (美樹)<br>
            (野上冴子)<br>
            (教授)<br>
            (名取和江)</td>
            <td class="t10" rowspan="8" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" rowspan="8" bgcolor="#FFFFEB">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">泪的鍊咀</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">暴风雨的前夕…</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">向地狱出发三!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">爱与恨的边缘… !!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">鍊咀的回忆!!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="5" valign="top" bgcolor="#ffffff"><b>&lt;34&gt;<br>
            冒牌城市猎人登场!!<br>
            </b>H3(1991)28号～39号</td>
            <td class="t10" bgcolor="#FFFFEB">儿子!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">从地狱归来!!</td>
          </tr>
          <tr>
            <td class="t10" rowspan="7" valign="top" bgcolor="#ffffff" align="center"><b>&lt;18&gt;</b></td>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode51-60.md#58">58</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">汪汪间谍!!</td>
            <td class="t10" bgcolor="#ffffff">受伤的英雄!?</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">日下美佐子</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">田宮教授<br>
            柳原</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">x</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">汪汪间谍!!</td>
          </tr>
          <tr>
            <td rowspan="3" class="tit10" bgcolor="#FFFFEB" align="center"><a href="chepisode51-60.md#59">59</a></td>
            <td class="tit10" rowspan="3" valign="middle" bgcolor="#FFFFEB">坦率地剖白!!</td>
            <td class="t10" bgcolor="#FFFFEB">冒牌城市猎人登场!!</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">橘 葉月</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">摩顿总统<br>
            (Mick・Angel)</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB" align="center">x</td>
            <td class="t10" rowspan="3" bgcolor="#FFFFEB">只在「The Secret Service」使用的段子</td>
          </tr>
          <tr>
            <td class="t10" rowspan="4" valign="top" bgcolor="#F0FFF0"><b>&lt;35&gt;<br>
            FOREVER, CITY HUNTER!!<br>
            </b>H3(1991)40号～50号</td>
            <td class="t10" bgcolor="#FFFFEB">狗咬狗骨!!</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFEB">坦率地剖白!!</td>
          </tr>
          <tr>
            <td rowspan="2" class="tit10" bgcolor="#ffffff" align="center"><a href="chepisode51-60.md#60">60</a></td>
            <td class="tit10" rowspan="2" valign="middle" bgcolor="#ffffff">FOREVER, CITY HUNTER!!</td>
            <td class="t10" bgcolor="#ffffff">婚礼的钟声!</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">（槇村香）</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">海拉诺・克洛兹<br>
            (海坊主)<br>
            (美樹)<br>
            (野上冴子)<br>
            (野上麗香)<br>
            (麻生霞)<br>
            (名取和江)<br>
            (Mick・Ange)<br>
            (教授)</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff" align="center">x</td>
            <td class="t10" rowspan="2" bgcolor="#ffffff">-</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#ffffff">FOREVER, CITY HUNTER!!</td>
          </tr>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Comics</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">文庫本</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">No.</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">Title</td>
            <td rowspan="2" class="tit10" bgcolor="#F0F8FF" align="center">Subtitle</td>
            <td rowspan="2" class="tit10" bgcolor="#F0F8FF" align="center">Guest Heroine</td>
            <td rowspan="2" class="tit10" bgcolor="#F0F8FF" align="center">主要的Guest</td>
            <td rowspan="2" class="tit10" bgcolor="#F0F8FF" align="center">动画片的<br>
            相关<br>
            剧集</td>
            <td rowspan="2" class="tit10" bgcolor="#F0F8FF" align="center">备注</td>
          </tr>
          <tr>
            <td colspan="2" class="tit10" width="25%" bgcolor="#F0F8FF" align="center">収録巻数</td>
            <td colspan="2" class="tit10" width="20%" bgcolor="#F0F8FF" align="center">Episode</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
  </tbody>
</table>
</center>

[BACK](./chepisodes.md)  
[NEXT](./chepisode1-10.md)  
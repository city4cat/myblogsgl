INDEX > CITY HUNTER > ミニ研究 > CHの裏の世界

# 如何称呼

我列出了角色之间的互相称呼。  
对制作模仿小说有用吗？  
（译注：表格里保留日文原文。为了简洁，表格里的译注文字由【】表示。）

<center>
<table width="90%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
<table border="1">
  <tbody>
    <tr>
      <td class="t10" rowspan="2" colspan="2" width="20%"></td>
      <td colspan="7" class="tit11" width="80%" bgcolor="#F0FFF0" align="center">对方</td>
    </tr>
    <tr>
      <td class="tit10" width="10%" bgcolor="#F0FFF0" align="center">獠</td>
      <td class="tit10" width="10%" bgcolor="#F0FFF0" align="center">香</td>
      <td class="tit10" width="10%" bgcolor="#F0FFF0" align="center">冴子</td>
      <td class="tit10" width="15%" bgcolor="#F0FFF0" align="center">海坊主</td>
      <td class="tit10" width="10%" bgcolor="#F0FFF0" align="center">美樹</td>
      <td class="tit10" width="10%" bgcolor="#F0FFF0" align="center">2人称</td>
      <td class="tit10" bgcolor="#F0FFF0" align="center">其他</td>
    </tr>
    <tr>
      <td class="tit11" rowspan="5" bgcolor="#F0F8FF" align="center">说话的人</td>
      <td class="tit10" bgcolor="#F0F8FF" align="center">獠</td>
      <td class="t10" bgcolor="#FFFFEB">おれ【我】<br>
            ボク・ボキ【我是】<span style="font-size : 8pt;">（开玩笑时）</span><br>
            </td>
      <td class="t10" bgcolor="#FFFFFF">香<br>
      香ちゃん【香酱】<br>
      おまえ【你】</td>
      <td class="t10" bgcolor="#FFFFFF">冴子<br>
      おまえ【你】</td>
      <td class="t10" bgcolor="#FFFFFF">海坊主<br>
      海ちゃん【海酱】<br>
      タコ坊主・タコ【海怪？】<br>
      おまえ【你】</td>
      <td class="t10" bgcolor="#FFFFFF">美樹ちゃん【美树酱】<span style="font-size : 8pt;">（我不知道为什么这么称呼）【待校对】</span></td>
      <td class="t10" bgcolor="#FFFFFF">あんた【你（熟人间的称呼）】<br>
      おたく【你的家人】<span style="font-size : 8pt;">（初期）</span></td>
      <td class="t10" bgcolor="#FFFFFF">麗香：麗香・麗香君<br>
            <span style="font-size : 8pt;">（忽略"酱"字）</span></td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF" align="center">香</td>
      <td class="t10" bgcolor="#FFFFFF">獠<br>
      あんた【你（熟人间的称呼）】<br>
      おまえ【你】</td>
      <td class="t10" bgcolor="#FFFFEB">あたし【我】<br>
            おれ【我】<span style="font-size : 8pt;">（初期・愤怒时等等）</span><br>
            </td>
      <td class="t10" bgcolor="#FFFFFF">冴子さん【冴子小姐】</td>
      <td class="t10" bgcolor="#FFFFFF">海坊主さん【海坊主先生】</td>
      <td class="t10" bgcolor="#FFFFFF">美樹さん【美樹小姐】</td>
      <td class="t10" bgcolor="#FFFFFF">あなた【你】</td>
      <td class="t10" bgcolor="#FFFFFF">ミック：ミック・ミックさん【Mick：Mick先生】<span style="font-size : 8pt;">（相遇时）</span></td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF" align="center">冴子</td>
      <td class="t10" bgcolor="#FFFFFF">獠<br>
      あなた【你】</td>
      <td class="t10" bgcolor="#FFFFFF">香さん【香小姐？】</td>
      <td class="t10" bgcolor="#FFFFEB">わたし【我】</td>
      <td class="t10" bgcolor="#FFFFFF">?<br>
            <span style="font-size : 8pt;">（动画中的Falcon，在原作中没有这个称呼）</span></td>
      <td class="t10" bgcolor="#FFFFFF">?<br>
            <span style="font-size : 8pt;">（在原作中没有这个称呼）</span></td>
      <td class="t10" bgcolor="#FFFFFF">あなた【你】</td>
      <td class="t10" bgcolor="#FFFFFF" align="center">-</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF" align="center">海坊主</td>
      <td class="t10" bgcolor="#FFFFFF">獠<br>
      冴羽<br>
      おまえ【你】</td>
      <td class="t10" bgcolor="#FFFFFF">香</td>
      <td class="t10" bgcolor="#FFFFFF">?<span style="font-size : 8pt;"><br>
            （原作中没有这样称呼的场景。）</span></td>
      <td class="t10" bgcolor="#FFFFEB">おれ【我】</td>
      <td class="t10" bgcolor="#FFFFFF">美樹</td>
      <td class="t10" bgcolor="#FFFFFF" align="center">-</td>
      <td class="t10" bgcolor="#FFFFFF" align="center">-</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF" align="center">美樹</td>
      <td class="t10" bgcolor="#FFFFFF">冴羽さん【冴羽先生】</td>
      <td class="t10" bgcolor="#FFFFFF">香さん【香小姐】</td>
      <td class="t10" bgcolor="#FFFFFF">冴子さん【冴子小姐】</td>
      <td class="t10" bgcolor="#FFFFFF">Falcon<br>
      あなた【你】</td>
      <td class="t10" bgcolor="#FFFFEB">わたし【我】<br>
      あたし【我】</td>
      <td class="t10" bgcolor="#FFFFFF" align="center">-</td>
      <td class="t10" bgcolor="#FFFFFF" align="center">-</td>
    </tr>
  </tbody>
</table>
</td>
          </tr>
        </tbody>
      </table>
</center>

## 备忘录
### 【獠】
● 	觉得熟悉的人，请放弃姓氏，或者加上「酱」或「君」（加什么取决于这个词和语境）。 不要称呼他们的姓氏。（译注：待校对）    
● 	当对方是教授时、不参与工作的普通人时，或在开玩笑时，会使用「です・ます」语气。 基本上没有被使用。    
● 	偶尔开玩笑的话    
● 	#56的回忆中，由于某些原因，语气有点好战（例如 "我做不到"）。（译注：待校对）    
● 	没有太多使用今天的青年语言。 说话非常坚定，比如说，有明显的口音。（译注：待校对）    

### 【香】  
● 	基本上，男性和女性都要加上「さん(译注:san/桑)」（对女性而言，是在名字后面）。 男孩叫「君」，女孩叫「ちゃん(译注:chan/酱)」。 唯一直呼其名的女性是她最好的朋友絵梨子。    
● 	她说话的方式出奇的女性化。 还有诸如「～よ」「～わ」之类的尾音。 与现在的年轻女性相比，她说话似乎相当整齐。（译注：待校对）
● 	有时使用男性语言（例如在该连载早期生气时）    


[BACK](./chm_s_r.md)  
[NEXT](./chsyokutaku.md)  
INDEX >

# CITY HUNTER　 シティーハンター

继CAT'S EYE之后，在周刊少年Jump上连载该作品。  
在CAT'S EYE连载期间，画了两篇该作品的短篇，在CAT'S EYE结束后，于1985年第13期开始连载。 这是北条作品中最成功的一部，描绘了一个「平时色咪咪但关键时刻毫不含糊」的英雄。1987年，该系列被改编为TV动画，在四年出了四季共140集。  
连载于1991年结束，但此后该系列以各种形式制作了多次，包括小说和电视特别节目。    

这部作品在连载的早期、中期和晚期，在人物塑造和故事发展方面表现出明显的差异和变化。 在该连载的早期部分，故事具有硬汉风格，中间部分具有浪漫的喜剧色彩，而后期则是家庭剧(?) (有粉丝将该连载称为早期黑暗、中期浪漫喜剧、末期爱情…）。 简单地说，主角冴羽獠在连载的开始和结束时几乎是一个不同的人。    
对于那些想享受硬汉剧情的人来说，我们推荐漫画的第1～5卷（相当于文库本的第1～3卷），而对于那些想看到与动画相对接近的冴羽獠，我们推荐继续阅读第23卷（相当于文库本的第12卷）。  
如果你读到这里，仍然想知道故事的其余部分，你应该读到最后。   

续集「Angel Heart」自2001年5月开始连载，但在人物塑造和其他方面存在差异，而且作者表示这是一个平行宇宙。  

掲載： 	週刊少年Jump　1985年13号～1991年50号  
収録： 	  
集英社 JUMP COMICS 「CITY HUNTER」全35巻  
集英社 集英社文庫（Comicク版）「CITY HUNTER」全18巻  
新潮社 BUNCH WORLD 「CITY HUNTER」全39巻  

## CONTENTS
■ 	[Story&Main Charactors](./chstory-charactors.md) 	：简介和主要人物。    
■ 	[Episode Guide](./chepisodes.md) 	：原作的各話解説  
■ 	[角色&用語集](./chpeople-words.md) 	：主要客串角色和词汇表   
■ 	[Animation](./chanime.md) 	：关于动画  
■ 	[Another CITY HUNTER](./chanother.md) 	：漫画和动画之外的拓展  
■ 	[Mini研究](./chkenkyu.md) 	：类似CH研究的角落…   
■ 	[CITY HUNTER 随想](./chessay01.md) 	：CH相关的随想   


[HOME](../index.md)  
[NEXT](./chstory-charactors.md)  

(译注：翻译CH部分时，我对比了CH香港玉皇朝版和台湾东立版。发现香港玉皇朝版的译文更忠于原作。例如，在[獠的過去](./chpast.md)里提到的「BabyFace」，在东立版中未翻译 。)  


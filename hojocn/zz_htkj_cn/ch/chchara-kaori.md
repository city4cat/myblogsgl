INDEX > CITY HUNTER > ミニ研究 >

# 人物简介 / 槇村 香

## 基本信息


<table class="mt11" border="1">
  <tbody>
    <tr>
      <td class="nh8" width="90" valign="bottom" bgcolor="#F0F8FF"><font color="#003366">假名</font></td>
      <td class="nh8" valign="bottom">マキムラ　カオリ</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">全名</td>
      <td class="t11">槇村 香</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">生年月日</td>
      <td class="t11">1965年3月31日</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">年齢</td>
      <td class="t11" valign="top">連載開始時19歳、終了時26歳</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">性別</td>
      <td class="t11">女</td>
    </tr>
    <tr>
      <td class="tit11" bgcolor="#F0F8FF">血液型</td>
      <td class="t11">O型 （动画的設定）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">住所</td>
      <td class="t11">東京都新宿区大字6-???　Saeba公寓401号（后来601…与獠同居）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">職業</td>
      <td class="t11">
      <p>獠的搭档</p>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">身体的特徴</td>
      <td class="t11">身高168.9cm（初3时），体重??kg 公斤、三围尺寸不详（动画中为B87-W59-H86）。 她很美，有模特的气质，但显然她并不突出，因为所有出现在动画中的女性都很美。 
獠甚至说（与Kasumi相比）她是“小乳房”…(^^;)（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">別名</td>
      <td class="t11">出生时的名字叫久石香。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">家庭</td>
      <td class="t11">有一个亲生妹妹（立木小百合）（表面上不知道）。<br>
      生父在她还是婴儿时就死于一场事故，生母在她21岁时就去世了（香不知道）。（译注：待校对）<br>
      养父在她5岁时去世，義兄（秀幸）在她20岁时去世。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">最喜欢的枪</td>
      <td class="t11">Colt Lawman MKIII 357 Magnum。 这是他的哥哥槇村使用过的，被作为纪念品送给了她。</td>
    </tr>
  </tbody>
</table>

## 経歴

另见「[年表](./chnempyo.md)」。  

-    出生后不久，父母就离婚了。 她和姐姐被母亲接管，但被父亲久石纯一带走并失踪。  
-    久石在东京犯下了一起谋杀案，并在逃亡中死于一场事故。 当时追踪他的侦探收留了她，并把她和他的儿子秀幸当作兄妹来抚养。 (更多的是表面上的未知数。 她在上初中时才知道自己是被收养的）  
-    5年后，她的父亲去世。 从那时起，他们似乎一直生活在一起，由一位长兄充当家长，但细节不详。  
-    她高中2年级的春假期间的3月26日，她初次见到獠（#56。此时她并没有表明自己是槇村的妹妹）。  
-    1985年3月26日、与獠重逢（#3；不清楚她在高中毕业后大约一年的时间里做了什么）。  
    同年3月31日，槇村被乐生会杀害，她成为獠的搭档至今。   

## 特殊技能

-    海坊主に教わった各種トラップ  
-    100t的锤子和其他各种击退獠的武器(?)（…这是一种特殊的技能吗…？）   

## 性格

-    她脾气很倔强，嘴巴很刁，但心地善良。  
-    对喜、怒、哀、乐有清晰的感觉。  
-    一个妒忌的人。 在谈到她自己的爱情生活时，笨拙而不敏感。  
-    据獠说，她不善于说谎话，这一点很容易从她的脸上看出来。   

## 弱点

幽灵。 可怕的故事。（译注：待校对）

## 兴趣爱好

-    爵士舞和健美操？  
-    料理？   

[BACK](./chchara-ryo.md)  
[NEXT](./chchara-saeko.md)  
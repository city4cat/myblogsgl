INDEX > CITY HUNTER > アニメーション >

# CITY HUNTER

(译注：[城市猎人 - Wikipedia](https://zh.wikipedia.org/wiki/城市猎人)里的中文标题包含：木棉花字幕標題、國語配音版標題、香港TVB標題。下表采用其中的國語配音版標題。)  

<CENTER>
<TABLE cellpadding="0" width="90%">
  <TBODY>
    <TR>
      <TD bgcolor="#191970">
      <TABLE border="1" cellpadding="2" width="100%">
  <TBODY>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">放送日</TD>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">話数</TD>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">Subtitle（译注：每集标题）</TD>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">脚本/構成</TD>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">故事板</TD>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">演出</TD>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">动画导演</TD>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">Guest</TD>
      <TD bgcolor="#F0F8FF" class="tit10" align="center">原作</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">87 4/6</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">1</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">風流的清道夫<BR>
            XYZ是危險的雞尾酒</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">星山博之</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣<BR>
            江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">亜月菜摘：藤田淑子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">2</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">4/13</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">2</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">殺了我吧!!<BR>
            槍口不是拿來瞄準美女的</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">平野靖士</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">網野哲郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">網野哲郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">鈴木四郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">清水美津子：日高のり子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">短篇 1</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">4/20</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">3</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">愛不要消失!<BR>
            往明日的倒數計時</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">平野靖士</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">滝沢敏文</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">岩崎惠：上田みゆき</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">1</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">4/27</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">4</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">美女蒸發!!<BR>
            精品店是掉進深淵的誘餌</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武上純希</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">黒崎：佐藤正治</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">3</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">5/4</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">5</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">永別了,槇村<BR>
            雨夜中的淚水生日派對</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">井上敏樹</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">富永恒雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">富永恒雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">小林ゆかり</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">将軍：納谷六郎</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">4</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">5/11</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">6</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">不談戀愛的女明星<BR>
            朝向希望的最後一槍</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武上純希</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神志那弘志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">佐藤由美子：戸田恵子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">10</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">5/18</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">7</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">撼動心靈的槍聲<BR>
            悲傷的孤獨女孩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">星山博之</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">網野哲郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">網野哲郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">豬股睦美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">萩尾：清川元夢</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">9</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">5/25</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">8</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">對美女百發百中!?<BR>
            別對女警出手</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">吉川惣司</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">電話の声：大塚芳忠</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">12</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">6/1</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">9</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">賭博皇后<BR>
            華麗的戀愛賭局</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">井上敏樹</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">滝沢敏文</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">牧野陽子：川浪葉子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">13</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">6/8</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">10</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">危險的家庭教師?<BR>
            給女高中生愛的料理</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">日暮裕一</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山口美浩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">龙神沙也加：三田ゆう子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">8</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">6/15</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">11</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">韻律美女<BR>
            最愛鬱金香</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">大川俊道</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">富永恒雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">棚橋一徳</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">小林ゆかり<BR>
            清水恵蔵</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">麻生霞：富永みーな</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">16</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">6/22</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">12</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">最有價值的小孩!<BR>
            來自危險國度的性感美女</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">吉川惣司</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">豬股睦美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">川田温子：潘恵子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">15</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">6/29</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">13</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">我的敵人是美女？<BR>
            史上最大的美女地獄!!</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">吉川惣司</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">Boss：井上瑤</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">21</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">7/6</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">14</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">16歳結婚宣言!<BR>
            偶像的熱吻</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">平野靖士</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山崎和男</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神志那弘志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">松村渚：渕崎ゆり子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">14</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">7/13</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">15</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">獠是女子大學講師?<BR>
            保護美麗的大小姐</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">外池省二</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">網野哲郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山口美浩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">片岡優子：渡辺菜生子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">11</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">7/20</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">16</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">野丫頭空中小姐<BR>
            獠的教官故事</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">星山博之</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">豬股睦美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">堀越ちえみ：坂本千夏</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">7/27</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">17</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">夏天的美女設計師<BR>
            獠的心全是高衩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">日暮裕一</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">大原緑：島津冴子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">8/3</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">18</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">夏夜的神諭?!<BR>
            祈禱師的戀愛啟蒙</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武上純希</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山崎和男</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神志那弘志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">雅姫：荘真由美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">8/10</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">19</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">回憶的浪潮<BR>
            充滿危險的試鏡會</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山口美浩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">豬股睦美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">鹿島悦子：江森浩子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">18</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">8/17</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">20</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山裡來的公主<BR>
            獠最長的一日</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">吉川惣司</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">花姫：平野文</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">8/24</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">21</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">看不見的狙擊者!!<BR>
            獠和冴子的危險遊戲</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">星山博之</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">網野哲郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">鈴木四郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">Smith：桑原たけし</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">8/31</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">22</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">愛的邱比特<BR>
            向鑽石乾杯!!</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">井上敏樹</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">星野アリサ：高田由美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">9/7</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">23</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">毒蜂嗡嗡!!<BR>
            從天而降的新娘</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山口美浩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">豬股睦美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">名取和江：山本百合子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">19</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">9/14</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">24</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">薔薇色的住院生活?<BR>
            被狙擊的白衣天使</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">外池省二</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神志那弘志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">岩井善美：水谷優子</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">20</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">9/21</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">25</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">愛的國際交流?!<BR>
            情敵是金髮美人</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武上純希</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">Angel Heart：吉田理保子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">9/28</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">26</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">愛是什麼呢?<BR>
            獠的正確的戀愛講座</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">藤本義孝</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">皆川由貴：岡本麻弥</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">17</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">10/5</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">27</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">獠與海怪的<BR>
            純情長腿叔叔傳說 (前編)</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">(構)儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">氷室真希：島本須美</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">24</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">10/12</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">28</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">獠與海怪的<BR>
            純情長腿叔叔傳說 (後編)</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">(構)儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山口美浩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">氷室真希：秋元洋介</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">24</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">10/19</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">29</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">委託費500元!?<BR>
            童話美女是獠喜歡的類型</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">星山博之</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">豬股睦美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">村越さおり：高島雅羅</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">10/26</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">30</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">情敵出現!?<BR>
            我要娶阿香小姐</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山崎和男</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神志那弘志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">蛾眉丸信二：三ツ矢雄二</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">11/2</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">31</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">積極的愛!<BR>
            奔馳的少女心!</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">井上敏樹</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">藤本義孝</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">鹿鳴館綾子：千原江里子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">11/9</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">32</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">獠，不要死!!<BR>
            硬漢的麥格農</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">網野哲郎</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">Michael Garland：池田勝</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">11/16</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">33</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加油海怪!!<BR>
            硬派的初戀協奏曲</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">星山博之</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">豬股睦美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江梨子：鵜飼るみ子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">11/23</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">34</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">衝撃!! 獠的父親宣言<BR>
            　要吵醒熟睡的孩子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">外池省二</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神崎貢一</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">藤本義孝</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神村幸子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">吉田奈々：小粥よう子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">11/30</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">35</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">突擊美人播報員<BR>
            獠的極密性感採訪</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武藤裕治</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武藤裕治</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤晴</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">結城礼子：佐々木るん</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">22</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">12/7</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">36</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">女大學生愛的堅持!<BR>
            有人盯上我了</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">平野靖士</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">正木由加里：松井奈桜子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">12/14</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">37</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">新宿仁義一直線!<BR>
            古裝美女的拜師志願(前篇)</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武上純希</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神志那弘志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">吹雪菊之介：松岡ミユキ</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">12/21</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">38</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">新宿仁義一直線!<BR>
            古裝美女的拜師志願(後篇)</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武上純希</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">吹雪団十郎：渡辺猛</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">88 1/4</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">39</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">短路的竹取公主!<BR>
            獠也棘手的記憶喪失</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山口美浩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">耀子：横尾まり</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">1/11</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">40</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">性感的搭檔!<BR>
            洗完澡後的委託費</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">日暮裕一</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">藤本義孝</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神村幸子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">三崎彩：山田栄子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">1/18</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">41</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">冴子的妹妹是女偵探(前篇)<BR>
            ―熱血女孩的大膽秘密</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">豬股睦美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">黒川：田中康郎</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">26</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">1/25</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">42</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">冴子的妹妹是女偵探(後篇)<BR>
            ―熱血女孩的大膽秘密</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">大空組長：飯塚昭三</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">26</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">1/31</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">43</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">少女的愛心餅乾紀念日<BR>
            胸口的痣與初戀之詩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">井上敏樹</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神志那弘志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">夢野詩子：神田和佳</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">2/7</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">44</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">訪問100個小孩的結論?!<BR>
            獠是我們的英雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">鎌田秀美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">藤本義孝</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">鷲尾ひさ子：蒔村三枝子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">2/14</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">45</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">三姊妹的邀約!<BR>
            獠的滑雪天國</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">星山博之</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山口美浩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神村幸子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">高円寺美子：水沢有美</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">2/21</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">46</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">驚險的行竊<BR>
            一切都是為了將來!!</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">小華和為雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">武藤裕治</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">新井豊</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">Mariko：神代智恵</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">2/29</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">47</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">性感是特效藥?!<BR>
            美女球員與愛的球跡</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">あまのまあ</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">今西隆志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">北原健雄</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">岡野由里：松原雅子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">3/7</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">48</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">春天來臨，戀愛的寡婦!<BR>
            來，脫下妳的喪服吧…</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">江上潔</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山崎和男</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">谷口守泰</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">Lelouch・真澄：羽村京子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">3/14</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">49</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">迷途修女<BR>
            好色之徒的愛的指引!?</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">外池省二</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">加瀬充子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神村幸子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">京子：中野聖子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">3/21</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">50</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">史上最強敵手!!<BR>
            獠與香的最終合作(前編)</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">港野洋介</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">藤本義孝</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">神志那弘志</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">Gemma：田原アルノ</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#FFFFFF" class="t10" align="center">3/28</TD>
      <TD align="center" bgcolor="#FFFFFF" class="t10">51</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">史上最強敵手!!<BR>
            獠與香的最終合作(後編)</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">遠藤明範</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">儿玉兼嗣</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">山口美浩</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">磯野智</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">Sabo：稲葉実<BR>
            Denai：玉川砂記子</TD>
      <TD bgcolor="#FFFFFF" class="t10" align="center">-</TD>
    </TR>
  </TBODY>
</TABLE>
</TD>
    </TR>
  </TBODY>
</TABLE>
</CENTER>

[BACK](./chanimespecial.md)  
[NEXT](./chanime2subtitles.md)  
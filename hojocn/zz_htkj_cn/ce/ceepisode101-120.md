INDEX > CAT'S EYE > エピソードガイド >

# #101 愛的拼圖 ～ #120 飯盒恐懼癥!

Jump to： | 101 | 102 | 103 | 104 | 105 | 106 | 107 | 108 | 109 | 110 | 111 | 112 | 113 | 114 | 115 | 116 | 117 | 118 | 119 | 120 |

## #101「愛的拼圖」

对于平野关注的泪，重先生通过让她从大量的相亲照片中选择自己喜欢的类型，制作了照片幻灯片。 令她惊讶的是，那张脸和俊夫的脸一模一样。 由于这一点，平野、竹内和木崎，以及小瞳，都对俊夫产生了怀疑…。  
[动画:无]  

<font color=#004d99>
姐妹们有过相似的品味吗？ 俊夫说他年轻时很像海因茨先生，但你的男朋友或丈夫看起来像你的直系亲属吗？…顺便说一下，我的丈夫…他完全不像我的直系亲属或我喜欢的类型…（笑）我想知道为什么我嫁给他(^^;)
</font>


## #102「給我一個熱吻」

俊夫在最近的一次回家晚了。 他的外套上有女人的香水味。 小瞳很好奇，问俊夫，但俊夫拼命掩饰，平野和竹内也被告知不能说。 当小瞳跟踪俊夫时，她发现等待他的人也是一个女人…!  
[动画:无]  

<font color=#004d99>
这是…这是一个很像北条司的故事…（不评论）
</font>


## #103「刑警的西裝」

小瞳给俊夫买了一套新衣服，因为同一套衣服他已经穿了6年。 然而，在一个雨天，俊夫回家时全身湿透，没有穿西装外套，手里拿着一把女人的雨伞。 小瞳很生气，但几天后，一个女人出现在店里，告诉小瞳关于俊夫的外套的事。 事情的真相是…?  
[动画:无]  

<font color=#004d99>
事实上，我有已经穿了约15年的睡衣(^^;)，但对俊夫，他每天都穿着去上班。我更关心的是，动画中的浅谷和俊夫穿了同样的衣服（笑），但这是一部老动画，所以这也是没办法的。  
</font>


## #104「富勇氣的決定!」

听着重先生、平野和其他人谈论俊夫的行为和性格，小瞳变得很沮丧，认为自己其实并没有最了解俊夫的优点。 当重先生劝告小瞳，她可能在不知不觉中筑起了一道墙，她采取了以下行动…  
[动画:无]  

<font color=#004d99>
Subtitle似乎是对俊夫和小瞳的一种号召。
</font>
(译注：subtitle指每一话的标题)  


## #105「假如是一個美麗的謊言…」

俊夫回到家里，他的衬衫领子上有大量的吻痕。 俊夫解释说是火车上站在他前面的一个女人的，但小瞳拒绝听，说俊夫在撒谎。 那天晚上，小瞳决定和泪一起睡，泪告诉她关于俊夫的事…  
[动画:无]  

<font color=#004d99>
泪的话语对我们的现实生活有一定的作用。  
</font>


## #106「充滿魅力的男人!?」

俊夫的同居生活（？） 平野和武内在发现俊夫同居时，确信他实际上很受异性欢迎（？） 为了证明这一点，他们要求浅谷在他们身上留下吻痕，并试图让小猫也这样做，但他们的尝试都没有成功。 他们最后采取的措施是什么！？  
[动画:无]  

<font color=#004d99>
…一个简单的问题。不涂口红，会有吻痕吗？   
</font>


## #107「我是一家之主?」

有一天晚上，俊夫做了一个噩梦，叫出了泪的名字。 愤怒的小瞳让小爱问他为什么，但这一次，昏昏欲睡的俊夫拥抱了小爱。 当小瞳直接问俊夫他梦到了什么，他告诉她他最深的烦恼(?)是…  
[动画:无]  

<font color=#004d99>
背景上用罗马字写着：「感谢你参加情景剧，我们马上抽出了五位获奖者，我们很快就会宣布获奖者，请注意！」然后是获奖者的名字。 是否有一场情景剧比赛？  
</font>


## #108「三個臭皮匠，勝過一個諸葛亮」

俊夫收到了他嫲嫲的一封信。 他曾向祖母承诺，他将回到父母的家中接管家族企业，为期五年。 特搜组的成员为庆祝俊夫的新工作举办了一个聚会，但愤怒的俊夫试图提醒他们，没有他，他们不能逮捕Cat's Eye…  
[动画:无]  

<font color=#004d99>
…雪中的紧身衣看起来很冷...但这不是第一次了。  
</font>


## #109「可怕的底褲!!」

这封信引发了俊夫的严重内衣恐惧症。 事实上，他的家庭拥有一家定制的女性内衣专门店，作为一个孩子，他对女性内衣的体验很糟糕。 当小爱、武内和平野发现这一点时，他们实施了一个计划，以防止俊夫接管专门店。  
[动画:无]  

<font color=#004d99>
毕竟北条的作品和内衣是密不可分的吧～?  
</font>


## #110「爲了某人而努力…」

今天是爱的入学试结果公布的日子。 然而，当她打电话给告诉姐妹们她已经通过考试时，她们都不在，而照看房子的俊夫也忘记了今天是什么日子。 当小爱回到家时，她很沮丧，希望父亲在这种时候能在身边，但俊夫却独自一人在和什么东西说话。  
[动画:无]  

<font color=#004d99>
有这么多温暖人心的情节，小爱是主角! 录取通知书…啊，现在已经是很久以前的事了……(^^;)  
</font>


## #111「我最重要的女人!」

根据小爱从俊夫那里听到的消息，小瞳很难过，因为俊夫关心他的母亲胜过关心自己。 恋人为俊夫是否会接管家族企业而争吵。 在此期间，俊夫的祖母给他打电话。  
[动画:无]  

<font color=#004d99>
这可能是该系列开始以来两人之间最严重的一次争斗。 瞳和俊夫之间的情感动荡被描绘得相当漂亮。 最后一幕中的两人也很精彩。  
</font>


## #112「發出挑戰書!!」

俊夫最近有些无能为力。 Cat's Eye特搜组的气氛也有些阴郁。 原因是Cat's Eye最近没有出现，犬鳴署内部甚至有传言说特搜组要被解散了。 当小瞳得知此事后，她采取了大胆的行动来为俊夫打气。  
[动画:无]  

<font color=#004d99>
事实上，自从#99以来，没有一集是Cat's Eye偷盗的。 作者的灵感用完了吗，还是说这是计划好的…？我敢肯定，在连载期间，读者们不时等待着Cat's Eye的活动。  
</font>


## #113「劫新娘!」

当神谷在街上看到俊夫和小瞳的行为奇怪时，他认为他们不和。从浅谷的建议中，他错误地认为浅谷还爱着俊夫。 然而，小瞳决定使用她的新爱情武器--手表形状的炸弹来对付神谷，神谷强行要求她和他约会！  
[动画:无]  

<font color=#004d99>
久违的初次登场就大获成功的神谷君(?)。 …我的意思是，这都是因为他的误会(笑)  
</font>


## #114「漫長的一天…」

俊夫给他祖母的磁带信意外地录下了小瞳和小爱之间的对话，关于她们在Cat's Eye的工作。 小瞳和姐妹们试图在俊夫听到之前更换磁带，但浅谷出现在店里，事情出现了转机…  
[动画:无]  

<font color=#004d99>
我想知道俊夫是不是很懒（笑）。用磁带录音也是相当尴尬和麻烦的。  
</font>


## #115「歪曲的時間!」

俊夫要在下一个Cat's Eye的目标--「操纵玩偶的人」的储藏室过夜。 一旦关闭，如果不从里面按密码，房间就无法打开，所以Cat's Eye设计了一个计划，用一个短的电视节目取代他在里面看的电视节目，让俊夫弄错时间。  
[动画:II-第25話(影片开始时，俊夫和女警之间的互动）]  

<font color=#004d99>
嗯，俊夫在动画片开始时的动作戏，我认为是CH（笑）顺便说一下，导演是儿玉。    
</font>


## #116「俊夫的爛攤子」

俊夫曾经照顾过的一个年轻人在他出狱后来探望他。 他说他现在在一个游乐园兼职做毛绒动物。在他逗留期间与祖母争论家事时，俊夫听说这个年轻人受伤了，即将被解雇，于是要替他接下这份兼职工作…  
[动画:无]  

<font color=#004d99>
毛绒动物的表情(!)有点可爱♪  
</font>


## #117「黑夜的誘惑!」

Cat's Eye特搜组搬进了「Cat's Eye」对面的新大楼。 为了了解特搜组的动向，小瞳和姐妹们决定在大楼外的旗杆安装窃听器。 当小瞳在值班的俊夫的注视下安放窃听器时，有人看到俊夫在室内游泳池，泪在那里…!  
[动画:无]  

<font color=#004d99>
不愧是泪......但俊夫......你的行为就像獠(笑)    
</font>


## #118「老鼠也瘋狂…」

神谷最近对Cat's Eye的兴趣比以前更浓了，包括试图窃听「Cat's Eye」和特搜组。 这是因为，在欧洲追寻宝藏时，他遇到了海因茨。 然后，神谷告诉小瞳一些更令人震惊的事情。  
[动画:无]  

<font color=#004d99>
从这一点上看，故事逐渐加快了结尾的速度。  
</font>


## #119「小貓頸上繫鈴!」

最近，浅谷对「Cat's Eye」的监视变得更加严厉。 正是她坚持要把特搜组迁走。 显然，神谷在幕后牵线搭桥，给海因茨提供信息。 不过，他的目标是与Cat's Eye联合起来…  
[动画:无]  

<font color=#004d99>
神谷的精致反派(?)可能是北条众多作品中最有价值的一个。  
</font>


## #120「飯盒恐懼癥!」

浅谷每天都为神谷准备饭菜和午餐。 但她的味觉很差，每天都让神谷吃，神谷吃得很难受。 为了让她意识到自己味觉障碍，神谷决定把俊夫的饭盒和她的换一下，让她看看俊夫的反应…  
[动画:无]  

<font color=#004d99>
「草蒿风味烧小羊」…这究竟是什么菜......我在网上搜索了一下，几乎没有找到什么。 有谁知道吗？  
</font>


[BACK](./ceepisode81-100.md)  
[NEXT](./ceepisode121-135.md)  
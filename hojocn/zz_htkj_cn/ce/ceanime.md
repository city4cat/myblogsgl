INDEX > CAT'S EYE >

# Animation  

## [作品解説](./ceanimekaisetsu.md)

我自己的简短解说。    

## [Staff List](./ceanimestaff.md)

目前，只有主要工作人员…（每一集的Subtitle和工作人员暂时在准备中）。(译注：Subtitle指每话/每集的标题)  

## [与原作的差异](./ceanimechigai.md)

与原作的主要区别。  


[BACK](./cesubtitles.md)  
[NEXT](./ceanimekaisetsu.md)  

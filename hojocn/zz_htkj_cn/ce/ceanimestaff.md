INDEX > CAT'S EYE > アニメーション >

# スタッフリスト

<center>
<table width="95%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
      <table width="100%" border="1">
        <tbody>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF"><font color="#F0F8FF">-</font></td>
            <td class="tit10" width="40%" bgcolor="#F0F8FF" align="center">CAT'S EYE</td>
            <td class="tit10" width="40%" bgcolor="#F0F8FF" align="center">CAT'S EYE (第2季）</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">話数</td>
            <td class="t10" bgcolor="#FFFFFF">全36話</td>
            <td class="t10" bgcolor="#FFFFFF">全37話</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">放送日</td>
            <td class="t10" bgcolor="#FFFFFF">1983年7月11日～1984年3月26日<br>
            毎週月曜日19：00～19：30<br>
            日本テレビ系列</td>
            <td class="t10" bgcolor="#FFFFFF">1984年10月8日～1985年7月8日<br>
            毎週月曜日19：00～19：30<br>
            日本テレビ系列</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">原作</td>
            <td class="t10" bgcolor="#FFFFFF">北条司</td>
            <td class="t10" bgcolor="#FFFFFF">北条司</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">プロデューサー</td>
            <td class="t10" bgcolor="#FFFFFF">初川則夫<br>
            加藤俊三</td>
            <td class="t10" bgcolor="#FFFFFF">初川則夫<br>
            加藤俊三</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">チーフディレクター</td>
            <td class="t10" bgcolor="#FFFFFF">竹内啓雄</td>
            <td class="t10" bgcolor="#FFFFFF">こだま兼嗣</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">シリーズ構成</td>
            <td class="t10" bgcolor="#FFFFFF">飯岡順一</td>
            <td class="t10" bgcolor="#FFFFFF">-</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">シナリオディレクター</td>
            <td class="t10" bgcolor="#FFFFFF">-</td>
            <td class="t10" bgcolor="#FFFFFF">飯岡順一</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">キャラクターデザイン</td>
            <td class="t10" bgcolor="#FFFFFF">杉野昭夫</td>
            <td class="t10" bgcolor="#FFFFFF">平山智</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">総作画監督</td>
            <td class="t10" bgcolor="#FFFFFF">平山智</td>
            <td class="t10" bgcolor="#FFFFFF">平山智</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">音楽</td>
            <td class="t10" bgcolor="#FFFFFF">大谷和夫</td>
            <td class="t10" bgcolor="#FFFFFF">大谷和夫</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">美術監督</td>
            <td class="t10" bgcolor="#FFFFFF">水谷利春</td>
            <td class="t10" bgcolor="#FFFFFF">水谷利春</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">撮影監督</td>
            <td class="t10" bgcolor="#FFFFFF">高橋宏固</td>
            <td class="t10" bgcolor="#FFFFFF">高橋宏固</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">録音監督</td>
            <td class="t10" bgcolor="#FFFFFF">加藤敏</td>
            <td class="t10" bgcolor="#FFFFFF">加藤敏</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">音楽監督</td>
            <td class="t10" bgcolor="#FFFFFF">鈴木清司</td>
            <td class="t10" bgcolor="#FFFFFF">鈴木清司</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">色指定</td>
            <td class="t10" bgcolor="#FFFFFF">山名公枝<br>
            池内道子</td>
            <td class="t10" bgcolor="#FFFFFF">池内道子</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">録音技術</td>
            <td class="t10" bgcolor="#FFFFFF">前田仁信</td>
            <td class="t10" bgcolor="#FFFFFF">前田仁信</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">音響効果</td>
            <td class="t10" bgcolor="#FFFFFF">宮田音響</td>
            <td class="t10" bgcolor="#FFFFFF">-</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">選曲</td>
            <td class="t10" bgcolor="#FFFFFF">合田豊</td>
            <td class="t10" bgcolor="#FFFFFF">合田豊</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">Opening・Ending</td>
            <td class="t10" bgcolor="#FFFFFF">ほんだゆきお</td>
            <td class="t10" bgcolor="#FFFFFF">ほんだゆきお</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">編集</td>
            <td class="t10" bgcolor="#FFFFFF">鶴渕允寿<br>
            高橋和子</td>
            <td class="t10" bgcolor="#FFFFFF">鶴渕允寿<br>
            高橋和子</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">Title</td>
            <td class="t10" bgcolor="#FFFFFF">高具アトリエ</td>
            <td class="t10" bgcolor="#FFFFFF">高具アトリエ</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">広報担当</td>
            <td class="t10" bgcolor="#FFFFFF">行末則雄</td>
            <td class="t10" bgcolor="#FFFFFF">行末則雄</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">制作進行</td>
            <td class="t10" bgcolor="#FFFFFF">松本真<br>
            斉藤俊哉<br>
            野崎公明<br>
            高橋伸治<br>
            南喜長</td>
            <td class="t10" bgcolor="#FFFFFF">-</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">制作担当</td>
            <td class="t10" bgcolor="#FFFFFF">徳永元嘉</td>
            <td class="t10" bgcolor="#FFFFFF">徳永元嘉</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">録音</td>
            <td class="t10" bgcolor="#FFFFFF">東北新社</td>
            <td class="t10" bgcolor="#FFFFFF">東北新社</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">現像</td>
            <td class="t10" bgcolor="#FFFFFF">東京現像所</td>
            <td class="t10" bgcolor="#FFFFFF">東京現像所</td>
          </tr>
          <tr>
            <td class="tit10" width="20%" bgcolor="#F0F8FF">製作</td>
            <td class="t10" bgcolor="#FFFFFF">东京电影新社<br>
            （現TMS Entertainment）</td>
            <td class="t10" bgcolor="#FFFFFF">东京电影新社<br>
            （現TMS Entertainment）</td>
          </tr>
          <tr>
            <td class="tit10" rowspan="2" width="20%" bgcolor="#F0F8FF">OP主題歌</td>
            <td class="tit10" bgcolor="#FFFFFF">「CAT'S EYE」</td>
            <td class="tit10" bgcolor="#FFFFFF">「デリンジャー」</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF">杏里</td>
            <td class="t10" bgcolor="#FFFFFF">刀根麻里子</td>
          </tr>
          <tr>
            <td class="tit10" rowspan="2" width="20%" bgcolor="#F0F8FF">ED主題歌</td>
            <td class="tit10" bgcolor="#FFFFFF">「DANCING WITH THE SUNSHINE」</td>
            <td class="tit10" bgcolor="#FFFFFF">「ホットスタッフ」</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF">日本語版（1～12話）：杏里<br>
            英語版（13～36話）：キャシー・リン</td>
            <td class="t10" bgcolor="#FFFFFF">シェリー・サベッジ</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
  </tbody>
</table>
</center>


## メインキャスト
**来生瞳**: 		戸田恵子  
**来生泪**: 		藤田淑子  
**来生愛**: 		坂本千夏  
**内海俊夫**: 	安原義人  
**永石**: 		大木民夫  
**課長**: 		内海賢二  
**浅谷光子**: 	榊原良子  

参考：DVD「CAT'S EYE」「CAT'S EYE Second Season」包含的小册子  

[BACK](./ceanimekaisetsu.md)  
[NEXT](./ceanimechigai.md)  
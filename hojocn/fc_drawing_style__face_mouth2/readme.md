
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
    - 如有错误，请指出，非常感谢！  


# FC的画风-面部-嘴(正面)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96888))  


- **如何描述嘴(正面)**  
先了解一下目前人们是如何描述嘴，之后再使用这些方法、术语来描述FC里的嘴。  

    - 嘴的结构[3]：  
        - "(Upper/Lower) Lip Vermil(l)ion"也被称为"(Upper/Lower) Lip Body"[2].   
        - "Cupid's Bow"也被称为"Upper Lip Tubercle"[2].  
        ![](img/all-about-lip-shapes-chart-graph.jpg)  

    - [2]中有关于嘴的结构的详细介绍。  

    - [1]中给出了面部mark点。其中与嘴相关的mark点:  
        - 9: labiale superius (ls), 上唇中点  
        - 10: stomion (sto), 口裂点  
        - 11: labiale inferius (li), 下唇中点  
        - 12/13: cheilion (ch), 口角点    
        ![](../fc_drawing_style__face/img2/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig3.png)    

    - 网上关于嘴形/唇形的介绍不一致：  
      ![](img/1c702b61b8761f1e6156bbd1c2742125.jpg) 
      ![](img/7e64b378dc33f71965b9853b03ddde4d.jpg)  
      ![](img/2883062554_a116fae2fe_b.jpg) 
      ![](img/cdf012bc290ee02bda9eb7c239809ab8.jpg)  
      ![](img/75cae257838ca660314609e2dd.jpg) 
      ![](img/95c4055edb2ec00e0f04122e8066aff5-.jpg) 
      ![](img/lip-shapes.jpg)  
      虽然不一致，但有以下共同的嘴形/唇形。(thick/thin指嘴唇(竖向)的厚薄；large/small指嘴(横向)的宽窄)：  
        - 上嘴唇薄  
        - 下嘴唇薄  
        - 上下嘴唇都薄  
        -   

    - 可以借助化妆来改变嘴形/唇形。  



- **应用到FC里**:  
    - FC里嘴(正面)的一些特点：  
        - 很多角色都是下嘴唇比上嘴唇厚。  
    
        - 一般来说，嘴唇向上弯曲表示愤怒；嘴唇向下弯曲表示微笑。但观察发现，嘴角上扬更能表现微笑:  
            - 例如09_099_2，即便嘴唇上弯，嘴角上扬也能让人感觉到微笑：  
              ![](./img/mouth_front_yukari_09_099_2.jpg)  
    
            - 例如06_135_2，即便嘴唇持平，嘴角上扬也能让人感觉到微笑：  
              ![](./img/mouth_front_sora_06_135_2.jpg)  
              类似的还有04_103_2。  
          反之，嘴角下挫，表示不高兴(01_183_4, 12_140_5,)：  
          ![](./img/01_183_4__cropped.jpg)  
          类似的还有12_140_5  
    
        - 闭嘴时，常见嘴唇线不连续--虚线。常见断点在嘴唇中央、嘴角：
            - 嘴唇中央处的断点：  
              ![](./img/mouth_front_sora_level-smile.png)  
            - 嘴唇中央、嘴角处的断点：  
              ![](./img/mouth_front_masami_level-smile.png)  
            - 其他位置的断点：  
              ![](./img/mouth_front_hiromi_07_106_1.jpg) 
              ![](./img/10_191_4_cropped.jpg)  
    
        - 嘴唇中央处的小尖。
          有时上嘴唇中点会向下突出一个小尖。如下图(01_200_4)，以及蓝色所示的小尖：  
            ![](./img/01_200_4__mouth.jpg) 
            ![](./img/01_200_4__mouth_v.jpg)  
          不知道上述"嘴唇中央处的断点"和这个小尖有没有关系。  
    
        - 男性角色(雅彦、盐谷、空)比女性角色(紫苑、雅美、紫)的嘴小(嘴(横向)的宽窄)。
    
    - 使用如下mark点：  
      ![](./img/mouth_front_masahiko_level.png) 
      ![](./img/mouth_front_mark_masahiko_level.jpg) 
      ![](./img/mouth_front_mark_template.jpg)  


------

以下只搜集、对比符合这些条件的嘴的镜头：正面、闭嘴、嘴部无动作或动作幅度轻微。  

- **雅彦(Masahiko)**：  
    - small:  
        - 图片: 02_063_4,       
        - 特点：嘴小(左右嘴角宽度小于level组)     
        - 叠加：  
            ![](./img/mouth_front_masahiko_small.png)  

    - curl-lip:  
        - 图片: 14_149_5, 13_116_3, 11_129_3,05_028_3,        
        - 特点：右嘴角下撇。  
        - 叠加：  
            ![](./img/mouth_front_masahiko_curl-lip.png)  

    - level:  
        - 图片: 13_129_5, 13_118_6，08_109_3，02_042_1，       
        - 特点：嘴唇水平。  
        - 叠加：  
            ![](./img/mouth_front_masahiko_level.png)    

    - downturned:  
        - 图片: 14_283_4, 13_116_4，07_053_4，03_090_0，01_149_3，        
        - 特点：嘴角向下弯。  
        - 叠加：  
            ![](./img/mouth_front_masahiko_downturned.png)  


- **雅美(Masami)(雅彦女装)**：  
    - level-smile:  
        - 图片: 12_054_4, 12_016_5，  
        - 特点：嘴唇水平，但有笑容。  
        - 叠加：  
            ![](./img/mouth_front_masami_level-smile.png)   

    - level:  
        - 图片: 12_054_4, 12_016_5，  
        - 特点：嘴唇水平     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_masami_level.png) 
            ![](./img/mouth_front_mark_masami_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 03_027_1,        
        - 特点：嘴角向下弯     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_masami_downturned.png)    

    - rouged
        - 图片: 14_163_0, 03_031_2, 04_141_0__r, 03_114_0, 03_027_1       
        - 特点：涂了口红。      
        - 叠加：  
            ![](./img/mouth_front_masami_rouged.png)    


- **紫苑(Shion)**：  
    - small:  
        - 图片: 10_008_6, 01_106_3, 08_165_4,   
        - 特点：嘴小。  
        - 叠加：  
            ![](./img/mouth_front_shion_small.png)   

    - smile:  
        - 图片: 04_064_6, 07_097_0，04_189_5, 01_150_7, 01_047_0,   
        - 特点：嘴唇有笑容。  
        - 叠加：  
            ![](./img/mouth_front_shion_smile.png)   

    - level-smile:  
        - 图片: 14_122_0, 09_083_2，04_139_5, 03_003_0,   
        - 特点：嘴唇水平，但有笑容。  
        - 叠加：  
            ![](./img/mouth_front_shion_level-smile.png)   

    - level:  
        - 图片: 11_133_2, 10_008_6， 08_096_2,05_089_2, 03_062_5,02_079_1, 01_161_2,    
        - 特点：嘴唇水平     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_shion_level.png) 
            ![](./img/mouth_front_mark_shion_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 14_282_1, 11_043_7, 07_029_4, 03_094_2,        
        - 特点：嘴角向下弯     
        - 叠加：  
            ![](./img/mouth_front_shion_downturned.png)    

    - downturned2:  
        - 图片: 14_280_6, 14_281_6, 10_172_1, 03_145_4,        
        - 特点：嘴角向下弯程度大于downturned组     
        - 叠加：  
            ![](./img/mouth_front_shion_downturned2.png)    

    - downturned3:  
        - 图片: 02_009_5,        
        - 特点：嘴角向下弯程度大于downturned2组     
        - 叠加：  
            ![](./img/mouth_front_shion_downturned3.png)    

    - rouged
        - 图片: 14_283_5，12_104_3，02_119_6，02_001，         
        - 特点：涂了口红。      
        - 叠加：  
            ![](./img/mouth_front_shion_rouged.png)    



- **塩谷/盐谷(Shionoya)(紫苑男装)**：  
    - level-smile:  
        - 图片: 12_158_6, 12_150_4  
        - 特点：嘴唇水平，但有笑容。  
        - 叠加：  
            ![](./img/mouth_front_shya_level-smile.png)   

    - level:  
        - 图片: 12_151_4,12_143_0,     
        - 特点：嘴唇水平     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_shya_level.png) 
            ![](./img/mouth_front_mark_shya_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 14_010_5_,        
        - 特点：嘴角向下弯     
        - 叠加：  
            ![](./img/mouth_front_shya_downturned.png)    

- **若苗空(Sora)**  
    - smile:  
        - 图片: 05_059_0, 02_000a_0,   
        - 特点：嘴唇有笑容。嘴唇中央处有小尖。  
        - 叠加：  
            ![](./img/mouth_front_sora_smile.png)   

    - level-smile:  
        - 图片: 06_135_2, 06_111_0, 01_152_0,   
        - 特点：嘴唇水平，但有笑容。  
        - 叠加：  
            ![](./img/mouth_front_sora_level-smile.png)   

    - level:  
        - 图片: 02_191_3,    
        - 特点：嘴唇水平     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_sora_level.png) 
            ![](./img/mouth_front_mark_sora_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 04_039_0,04_015_0,        
        - 特点：嘴角向下弯     
        - 叠加：  
            ![](./img/mouth_front_sora_downturned.png)  


- **若苗紫(Yukari)**  
    - smile:  
        - 图片: 08_066_3, 06_111_0, 05_064_2, 04_180_1, 04_000a_0, 01_200_4, 01_043_0  
        - 特点：嘴唇水平，但有笑容。  
        - 叠加：  
            ![](./img/mouth_front_yukari_smile.png)   

    - level:  
        - 图片: 10_035_3, 08_017_5, 05_183_6, 05_174_0, 05_042_1, 04_022_2, 02_004_4, 01_124_1, 01_025_4, 01_005_0,     
        - 特点：嘴唇水平     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_yukari_level.png) 
            ![](./img/mouth_front_mark_yukari_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 09_097_6, 08_011_5,        
        - 特点：嘴角向下弯     
        - 叠加：  
            ![](./img/mouth_front_yukari_downturned.png)  

    - rouged:  
        - 图片: 02_189_0, 04_000a_0, 02_004_4, 01_005_0, 08_017_5, 01_200_4, 01_043_0, 01_025_4,  
        - 特点：涂了口红。       
        - 叠加：  
            ![](./img/mouth_front_yukari_rouged.png)  

    - 02_189_0. 嘴形应该是small:  
      ![](./img/mouth_front_yukari_02_189_0.jpg)  

    - 嘴唇中央处有小尖： 05_183_6，01_200_4，10_035_3。  
    
    - 嘴唇中央处有断点: 08_066_3, 06_111_0, 5_064_2, 04_180_1, 10_035_3, 05_183_6, 05_174_0, 05_042_1, 02_004_4, 01_124_1, 02_004_4,  
    

- **浅冈叶子(Yoko)**  
    - smile:  
        - 图片: 06_007_3, 02_069_6,   
        - 特点：嘴唇有笑容。  
        - 叠加：  
            ![](./img/mouth_front_yoko_smile.png)   

    - level-smile:  
        - 图片: 13_142_1,   
        - 特点：嘴唇水平，但有笑容。  
        - 叠加：  
            ![](./img/mouth_front_yoko_level-smile.png)   

    - level:  
        - 图片: 08_152_6, (10_119_3), 13_154_2, 05_160_2,03_041_2,      
        - 特点：嘴唇水平     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_yoko_level.png) 
            ![](./img/mouth_front_mark_yoko_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 11_056_1, 04_136_3, (13_172_0_r, 08_160_3, )       
        - 特点：嘴角向下弯     
        - 叠加：  
            ![](./img/mouth_front_yoko_downturned.png)  

    - rouged:  
        - 图片: 13_172_0_r, 08_160_3      
        - 特点：涂了口红。       
        - 叠加：  
            ![](./img/mouth_front_yoko_rouged.png)  

    - wide:  
        - 图片: 13_172_0_r , 13_122_2      
        - 特点：嘴大(嘴角距离大)。       
        - 叠加：  
            ![](./img/mouth_front_yoko_wide.png)  


- **浩美(Hiromi)**  
    - level:  
        - 图片: 04_176_1,      
        - 特点：嘴唇水平, 但好像不是完全正面(有点转头)    
          ![](./img/04_176_1__cropped.jpg)  

    - downturned:  
        - 图片: 10_191_4       
        - 特点：嘴角向下弯     
        - 叠加：  
            ![](./img/10_191_4_cropped.jpg)  

    - rouged:  
        - 图片: 13_172_0_r, 08_160_3      
        - 特点：涂了口红。但好像不是完全正面。         
        - 叠加：  
            ![](./img/09_036_0.jpg)  


- **熏(Kaoru)**  
    - level:  
        - 图片: 13_042_0，13_010_1, 12_171_0, 08_071_1,  
        - 特点： 。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_kaoru_level.png) 
            ![](./img/mouth_front_mark_kaoru_level_vs_masahiko_level.jpg)  


- **江岛(Ejima)**  
    - smile:  
        - 图片: 14_025_1, 10_075_5, 06_144_4,   
        - 特点：嘴唇有笑容。  
        - 叠加：  
            ![](./img/mouth_front_ejima_smile.png)   


- **辰巳(Tatsumi)**  
    - smile-wide:  
        - 图片: 04_135_6, 06_026_5,   
        - 特点：嘴唇有笑容, 嘴更宽。  
        - 叠加：  
            ![](./img/mouth_front_tatsumi_smile-wide.png)   

    - smile:  
        - 图片: 06_041_0, 04_139_4, 03_136_3, 03_125_5,   
        - 特点：嘴唇有笑容。  
        - 叠加：  
            ![](./img/mouth_front_tatsumi_smile.png)   

    - level:  
        - 图片: 09_134_6, 09_095_3，  
        - 特点： 镜头平视时，成组的图片数量最多。鼻孔宽于雅彦basis。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_tatsumi_level.png) 
            ![](./img/mouth_front_mark_tatsumi_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 09_110_1, 09_096_6, 09_135_0, 08_048_2_r,        
        - 特点：嘴角向下弯     
        - 叠加：  
            ![](./img/mouth_front_tatsumi_downturned.png)  


- **导演**  
    - smile:  
        - 图片: 03_035_4,   
        - 特点：嘴唇有笑容。  
        - 叠加：  
            ![](./img/mouth_front_director_smile.png)   

    - downturned:  
        - 图片: 06_153_0,        
        - 特点：嘴角向下弯     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_director_downturned.png) 
            ![](./img/mouth_front_mark_director_downturned_vs_masahiko_level.jpg)  

- **早纪(Saki)**  
    - top:  
        - 图片: 11_114_0, 10_011_2__r, 10_010_1__r,   
        - 特点： 镜头俯视。  
        - 叠加：  
            ![](./img/mouth_front_saki_top.png)   

    - smile:  
        - 图片: 10_019_2,11_113_2，     
        - 特点：嘴唇有笑容。  
        - 叠加：  
            ![](./img/mouth_front_saki_smile.png)   

    - curl-lip:  
        - 图片: 10_009_3,       
        - 特点：右嘴角向上弯     
        - 叠加：  
            ![](./img/mouth_front_saki_curl-lip.png)  

    - level:  
        - 图片: (10_040_4, 09_176_2）， 09_175_1,10_009_1, 09_174_4，   
        - 特点： 镜头平视时，成组的图片数量最多      
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_saki_level.png) 
            ![](./img/mouth_front_mark_saki_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 09_176_3, 09_182_4，09_152_3       
        - 特点：嘴角向下弯     
        - 叠加：  
            ![](./img/mouth_front_saki_downturned.png)  

    - downturned2:  
        - 图片: 10_042_5,        
        - 特点：嘴角向下弯程度大     
        - 叠加：  
            ![](./img/mouth_front_saki_downturned2.png)  

    - 大多数镜头里该角色都有口红。  


- **真琴(Makoto)**  
没有合适的图。05_147_0是downturned,但人物是Q版。  


- **摄像师**  
    - downturned:  
        - 图片: 14_194_6, 14_193_1，03_070_4,  
        - 特点：嘴角向下弯     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_cameraman_downturned.png) 
            ![](./img/mouth_front_mark_cameraman_downturned_vs_masahiko_level.jpg) 


- **浅葱(Asagi)**  
    - level-smile:  
        - 图片: 14_250_1, 14_197_0, 14_135_6  
        - 特点：嘴唇水平，但有笑容。  
        - 叠加：  
            ![](./img/mouth_front_asagi_level-smile.png)   

    - level:  
        - 图片: (14_185_2), 14_074_2, 14_073_7, 14_057_0,   
        - 特点：嘴唇水平     
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_asagi_level.png) 
            ![](./img/mouth_front_mark_asagi_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 14_196_1, 14_193_1,  
        - 特点：嘴角向下弯  
        - 叠加：  
            ![](./img/mouth_front_asagi_downturned.png)  


- **和子(Kazuko)**  
    - level:  
        - 图片: 11_085_2, 08_109_3，05_061_3，02_080_4,   
        - 特点： 镜头平视时，成组的图片数量最多。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_kazuko_level.png) 
            ![](./img/mouth_front_mark_kazuko_level_vs_masahiko_level.jpg)   

    - downturned:  
        - 图片: 08_141_3, 07_051_1,05_163_0, 05_147_0, 04_189_1, 03_021_2, 01_069_0, 01_066_0, 01_052_2   
        - 特点：嘴角向下弯  
        - 叠加：  
            ![](./img/mouth_front_kazuko_downturned.png)  


- **爷爷**  
待完成  


- **横田进(Susumu)**  
    - level-smile:  
        - 图片: 03_021_2,  
        - 特点：        
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_susumu_level-smile.png) 
            ![](./img/mouth_front_mark_susumu_level-smile_vs_masahiko_level.jpg)  


- **宪司(Kenji)**  
    - curl-lip:  
        - 图片: 03_189_5, 03_187_2, 
        - 特点： 。       
        - 叠加：  
            ![](./img/mouth_front_kenji_curl-lip.png) 

    - level-smile:  
        - 图片: 04_051_4,  
        - 特点： 。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_kenji_level-smile.png) 
            ![](./img/mouth_front_mark_kenji_level-smile_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 04_038_2__r, 04_014_1  
        - 特点：        
        - 叠加：  
            ![](./img/mouth_front_kenji_downturned.png) 


- **顺子(Yoriko)**  
    - level-smile:  
        - 图片: 10_173_3, 03_155_5, 03_153_2
        - 特点： 。       
        - 叠加：  
            ![](./img/mouth_front_yoriko_level-smile.png) 

    - level:  
        - 图片: 04_016_3,
        - 特点： 。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_yoriko_level.png) 
            ![](./img/mouth_front_mark_yoriko_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 04_005_5   
        - 特点：嘴角向下弯  
        - 叠加：  
            ![](./img/mouth_front_yoriko_downturned.png)  

    - rouged
        - 图片: 03_180_1   
        - 特点：嘴角向下弯  
        - 叠加：  
            ![](./img/mouth_front_yoriko_rouged.png)  


- **森(Mori)**  
    - downturned:  
        - 图片: 06_114_0,
        - 特点： 。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_mori_downturned.png) 
            ![](./img/mouth_front_mark_mori_downturned_vs_masahiko_level.jpg)  


- **叶子母**  
    - level:  
        - 图片: 13_103_3,  
        - 特点： 。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
            ![](./img/mouth_front_ykma_level.png) 
            ![](./img/mouth_front_mark_ykma_level_vs_masahiko_level.jpg)  


- **理沙(Risa)**  
    - level:  
        - 图片: 12_025_0,  
        - 特点： 。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
            ![](./img/mouth_front_risa_level.png) 
            ![](./img/mouth_front_mark_risa_level_vs_masahiko_level.jpg)  


- **美菜(Mika)**  
    - level-smile:  
        - 图片: 12_008_6,  
        - 特点： 。       
        - 叠加：  
            ![](./img/mouth_front_mika_level-smile.png) 

    - level:  
        - 图片: 12_053_3,
        - 特点： 。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_mika_level.png) 
            ![](./img/mouth_front_mark_mika_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 11_167_4,     
        - 特点：嘴角向下弯  
        - 叠加：  
            ![](./img/mouth_front_mika_downturned.png)  


- **齐藤茜(Akane)**  
    - level-smile:  
        - 图片: 07_113_1, 07_099_3, 06_157_1,   
        - 特点： 。       
        - 叠加：  
            ![](./img/mouth_front_akane_level-smile.png) 

    - level:  
        - 图片: 06_145_7,  
        - 特点： 。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_akane_level.png) 
            ![](./img/mouth_front_mark_akane_level_vs_masahiko_level.jpg)  


- **仁科(Nishina)**  
    - downturned:  
        - 图片: 14_028_6  
        - 特点： 。       
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_nishina_downturned.png) 
            ![](./img/mouth_front_mark_nishina_downturned_vs_masahiko_level.jpg)  

- **葵(Aoi)**  
    - level:  
        - 图片: 06_081_3, 06_042_1,  
        - 特点：   
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_aoi_level.png) 
            ![](./img/mouth_front_mark_aoi_level_vs_masahiko_level.jpg)  

    - downturned:  
        - 图片: 06_080_6, 06_056_0, 06_039_0,   
        - 特点： 。       
        - 叠加：  
            ![](./img/mouth_front_aoi_downturned.png)  

- **奶奶**  
未处理。  


- **文哉(Fumiya)**  
    - curl-lip:  
        - 图片:    
        - 特点： 。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_fumiya_curl-lip.png) 
            ![](./img/mouth_front_mark_fumiya_curl-lip_vs_masahiko_level.jpg)  

- **齐藤玲子(Reiko)**  
    - smile:  
        - 图片: 01_169_6,
        - 特点：年幼时的样子。  
        - 叠加：  
            ![](./img/mouth_front_reiko_smile.png)  

    - downturned:  
        - 图片: 01_166_0,
        - 特点：01_166_0水平翻转后与01_166_0拼接而成。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_reiko_downturned.png) 
            ![](./img/mouth_front_mark_reiko_downturned_vs_masahiko_level.jpg)  


- **松下敏史(Toshifumi)**  
    - level:  
        - 图片: 09_077_5,
        - 特点：镜头稍微俯视。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_matsu_level.png) 
            ![](./img/mouth_front_mark_matsu_level_vs_masahiko_level.jpg)  


- **章子(Shoko)**  
待完成  


- **阿透(Tooru)**  
没有合适的图。  


- **京子(Kyoko)**  
    - level:  
        - 图片: 02_186_2_r, 02_186_0__r 
        - 特点： 。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_kyoko_level.png) 
            ![](./img/mouth_front_mark_kyoko_level_vs_masahiko_level.jpg)  


- **一马(Kazuma)**  
待完成  


- **一树(Kazuki)**  
    - level-smile:  
        - 图片: 12_012_3,  
        - 特点： 。  
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_kazuki_level-smile.png) 
            ![](./img/mouth_front_mark_kazuki_level-smile_vs_masahiko_level.jpg)  


- **千夏(Chinatsu)**  
    - smile:  
        - 图片: 12_159_3,  
        - 特点：。           
        - 叠加：  
            ![](./img/mouth_front_chinatsu_smile.png)   


- **麻衣(Mai)**  
    - downturned:  
        - 图片: 12_141_0,  
        - 特点： 。         
        - 叠加：  
            ![](./img/mouth_front_mai_downturned.png)  


- **绫子(Aya)**  
    - downturned:  
        - 图片:  02_189_4,  
        - 特点： 。         
        - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
            ![](./img/mouth_front_aya_downturned.png) 
            ![](./img/mouth_front_mark_aya_downturned_vs_masahiko_level.jpg)  

    - rouged:  
        - 图片:  02_190_0,  
        - 特点： 。         
        - 叠加：  
            ![](./img/mouth_front_aya_rouged.png)   


- **典子(Tenko)**  
没有合适的图。   


- **古屋公弘(Kimihiro)**  
没有合适的图。


- **古屋朋美(Tomomi)**  
没有合适的图。  


- **八不(Hanzu)**  
没有合适的图。  


- **哲(Te)**  
没有合适的图。  


------

- **排列如下(角色嘴, 放大2倍)**:  
![](img/mouth_front_x2/mouth_front_masahiko.jpg) 
![](img/mouth_front_x2/mouth_front_masami.jpg) 
![](img/mouth_front_x2/mouth_front_shion.jpg) 
![](img/mouth_front_x2/mouth_front_shya.jpg) 
![](img/mouth_front_x2/mouth_front_sora.jpg) 
![](img/mouth_front_x2/mouth_front_yukari.jpg) 
![](img/mouth_front_x2/mouth_front_yoko.jpg) 
![](img/mouth_front_x2/mouth_front_hiromi.jpg) 
![](img/mouth_front_x2/mouth_front_kaoru.jpg) 
![](img/mouth_front_x2/mouth_front_ejima.jpg) 
![](img/mouth_front_x2/mouth_front_tatsumi.jpg) 
![](img/mouth_front_x2/mouth_front_makoto.jpg) 
![](img/mouth_front_x2/mouth_front_director.jpg) 
![](img/mouth_front_x2/mouth_front_saki.jpg) 
![](img/mouth_front_x2/mouth_front_asagi.jpg) 
![](img/mouth_front_x2/mouth_front_cameraman.jpg) 
![](img/mouth_front_x2/mouth_front_yoriko.jpg) 
![](img/mouth_front_x2/mouth_front_mori.jpg) 
![](img/mouth_front_x2/mouth_front_kazuko.jpg) 
![](img/mouth_front_x2/mouth_front_grandpa.jpg) 
![](img/mouth_front_x2/mouth_front_susumu.jpg) 
![](img/mouth_front_x2/mouth_front_kenji.jpg) 
![](img/mouth_front_x2/mouth_front_ykma.jpg) 
![](img/mouth_front_x2/mouth_front_risa.jpg) 
![](img/mouth_front_x2/mouth_front_mika.jpg) 
![](img/mouth_front_x2/mouth_front_akane.jpg) 
![](img/mouth_front_x2/mouth_front_nishina.jpg) 
![](img/mouth_front_x2/mouth_front_aoi.jpg) 
![](img/mouth_front_x2/mouth_front_grandma.jpg) 
![](img/mouth_front_x2/mouth_front_fumiya.jpg) 
![](img/mouth_front_x2/mouth_front_reiko.jpg) 
![](img/mouth_front_x2/mouth_front_matsu.jpg) 
![](img/mouth_front_x2/mouth_front_shoko.jpg) 
![](img/mouth_front_x2/mouth_front_tooru.jpg) 
![](img/mouth_front_x2/mouth_front_kyoko.jpg) 
![](img/mouth_front_x2/mouth_front_kazuma.jpg) 
![](img/mouth_front_x2/mouth_front_kazuki.jpg) 
![](img/mouth_front_x2/mouth_front_chinatsu.jpg) 
![](img/mouth_front_x2/mouth_front_mai.jpg) 
![](img/mouth_front_x2/mouth_front_aya.jpg) 
![](img/mouth_front_x2/mouth_front_tenko.jpg) 
![](img/mouth_front_x2/mouth_front_kimihiro.jpg) 
![](img/mouth_front_x2/mouth_front_tomomi.jpg) 
![](img/mouth_front_x2/mouth_front_hanzu.jpg) 
![](img/mouth_front_x2/mouth_front_te.jpg) 


------

- **把类似的嘴分组**  
    - 按“嘴的大小(嘴角之间的宽度)”来粗糙地分组。  
      以下8组，按“嘴的大小”从小到大排列。同一组({})的角色被认为其“嘴的大小”是同样的。例如，在该合并分类中，可以认为shion和yukari的嘴是一样的。  

        1. {reiko}   
        ![](img/mouth_front_x2/mouth_front_reiko.jpg) 
    
        2. {shion, yukari, mai}  
        ![](img/mouth_front_x2/mouth_front_shion.jpg) 
        ![](img/mouth_front_x2/mouth_front_yukari.jpg) 
        ![](img/mouth_front_x2/mouth_front_mai.jpg)  
    
        3. {yoko， asagi, saki, yoriko, ykma, risa, mika, akane, kyoko}  
        ![](img/mouth_front_x2/mouth_front_yoko.jpg) 
        ![](img/mouth_front_x2/mouth_front_asagi.jpg) 
        ![](img/mouth_front_x2/mouth_front_saki.jpg) 
        ![](img/mouth_front_x2/mouth_front_yoriko.jpg) 
        ![](img/mouth_front_x2/mouth_front_ykma.jpg) 
        ![](img/mouth_front_x2/mouth_front_risa.jpg) 
        ![](img/mouth_front_x2/mouth_front_mika.jpg) 
        ![](img/mouth_front_x2/mouth_front_akane.jpg) 
        ![](img/mouth_front_x2/mouth_front_kyoko.jpg) 
    
        4. {masahiko, masami, shya, sora, kaoru}  
        ![](img/mouth_front_x2/mouth_front_masahiko.jpg) 
        ![](img/mouth_front_x2/mouth_front_masami.jpg) 
        ![](img/mouth_front_x2/mouth_front_shya.jpg) 
        ![](img/mouth_front_x2/mouth_front_sora.jpg) 
        ![](img/mouth_front_x2/mouth_front_kaoru.jpg)  

        5. {tatsumi, matsu}  
        ![](img/mouth_front_x2/mouth_front_tatsumi.jpg) 
        ![](img/mouth_front_x2/mouth_front_matsu.jpg) 

        6. {ejima, susumu, aoi, fumiya, kazuki}  
        ![](img/mouth_front_x2/mouth_front_ejima.jpg) 
        ![](img/mouth_front_x2/mouth_front_susumu.jpg) 
        ![](img/mouth_front_x2/mouth_front_aoi.jpg) 
        ![](img/mouth_front_x2/mouth_front_fumiya.jpg) 
        ![](img/mouth_front_x2/mouth_front_kazuki.jpg) 
    
        7. {kazuko}  
        ![](img/mouth_front_x2/mouth_front_kazuko.jpg) 
    
        8. {kenji}  
        ![](img/mouth_front_x2/mouth_front_kenji.jpg) 

    
    - 按口红分类：  
        - {saki，kyoko， mori}  
        ![](img/mouth_front_x2/mouth_front_saki.jpg) 
        ![](img/mouth_front_x2/mouth_front_kyoko.jpg) 
        ![](img/mouth_front_x2/mouth_front_mori.jpg) 
        
        - {susumu, aoi}(下嘴唇很饱满)  
        ![](img/mouth_front_x2/mouth_front_susumu.jpg) 
        ![](img/mouth_front_x2/mouth_front_aoi.jpg) 
    
    - 其他（不易分组的(角色的)嘴）：  
        - {director, cameraman}  
        ![](img/mouth_front_x2/mouth_front_director.jpg) 
        ![](img/mouth_front_x2/mouth_front_cameraman.jpg)  

        - {chinatsu}  
        ![](img/mouth_front_x2/mouth_front_chinatsu.jpg) 
    
        - {aya}  
        ![](img/mouth_front_x2/mouth_front_aya.jpg) 
    
        - {nishina}  
        ![](img/mouth_front_x2/mouth_front_nishina.jpg) 


------

- **进一步(粗粒化)分组**  
    - 按“嘴的大小(嘴角之间的宽度)”来粗糙地分组。  
      以下4组，按“嘴的大小”从小到大排列。同一组({})的角色被认为其“嘴的大小”是同样的。例如，在该合并分类中，可以认为shion和asagi的嘴是一样的。  

        1. {reiko, aya}   
        ![](img/mouth_front_x2/mouth_front_reiko.jpg) 
        ![](img/mouth_front_x2/mouth_front_aya.jpg)  

        2. {shion, yukari, yoko， asagi, saki, yoriko, ykma, risa, mika, akane, kyoko, chinatsu, mai, }  
        ![](img/mouth_front_x2/mouth_front_shion.jpg) 
        ![](img/mouth_front_x2/mouth_front_yukari.jpg) 
        ![](img/mouth_front_x2/mouth_front_yoko.jpg) 
        ![](img/mouth_front_x2/mouth_front_asagi.jpg) 
        ![](img/mouth_front_x2/mouth_front_saki.jpg) 
        ![](img/mouth_front_x2/mouth_front_yoriko.jpg) 
        ![](img/mouth_front_x2/mouth_front_ykma.jpg) 
        ![](img/mouth_front_x2/mouth_front_risa.jpg) 
        ![](img/mouth_front_x2/mouth_front_mika.jpg) 
        ![](img/mouth_front_x2/mouth_front_akane.jpg) 
        ![](img/mouth_front_x2/mouth_front_kyoko.jpg) 
        ![](img/mouth_front_x2/mouth_front_chinatsu.jpg) 
        ![](img/mouth_front_x2/mouth_front_mai.jpg)  
 

        3. {masahiko, masami, shya, sora, kaoru, tatsumi, matsu, ejima, susumu, aoi, fumiya, kazuki, director, cameraman, nishina, }  
        ![](img/mouth_front_x2/mouth_front_masahiko.jpg) 
        ![](img/mouth_front_x2/mouth_front_masami.jpg) 
        ![](img/mouth_front_x2/mouth_front_shya.jpg) 
        ![](img/mouth_front_x2/mouth_front_sora.jpg) 
        ![](img/mouth_front_x2/mouth_front_kaoru.jpg) 
        ![](img/mouth_front_x2/mouth_front_tatsumi.jpg) 
        ![](img/mouth_front_x2/mouth_front_matsu.jpg) 
        ![](img/mouth_front_x2/mouth_front_ejima.jpg) 
        ![](img/mouth_front_x2/mouth_front_susumu.jpg) 
        ![](img/mouth_front_x2/mouth_front_aoi.jpg) 
        ![](img/mouth_front_x2/mouth_front_fumiya.jpg) 
        ![](img/mouth_front_x2/mouth_front_kazuki.jpg) 
        ![](img/mouth_front_x2/mouth_front_director.jpg) 
        ![](img/mouth_front_x2/mouth_front_cameraman.jpg) 
        ![](img/mouth_front_x2/mouth_front_nishina.jpg)  

        4. {kazuko, kenji}  
        ![](img/mouth_front_x2/mouth_front_kazuko.jpg) 
        ![](img/mouth_front_x2/mouth_front_kenji.jpg)  

    - 涂口红：  
        - {saki，kyoko, mori, susumu, aoi}  
        ![](img/mouth_front_x2/mouth_front_saki.jpg) 
        ![](img/mouth_front_x2/mouth_front_kyoko.jpg) 
        ![](img/mouth_front_x2/mouth_front_mori.jpg) 
        ![](img/mouth_front_x2/mouth_front_susumu.jpg) 
        ![](img/mouth_front_x2/mouth_front_aoi.jpg)  


**参考资料**：  
1. [Differences between Caucasian and Asian attractive faces](https://www.researchgate.net/publication/318548459), Seung Chul Rhee, 2017.  
2. Form of the Head and Neck, 2021, Uldis Zarins.  
3. [All About Lip Shapes - Juvly](https://www.juvly.com)  




--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
    - 如有错误，请指出，非常感谢！  


# FC的画风-面部-头发2(正面)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96891))  

几乎每个角色都有自己独特的头发。这里简单罗列如下：  

![](img/hair_front_x1/hair_front_masahiko.jpg) 
![](img/hair_front_x1/hair_front_masami.jpg) 
![](img/hair_front_x1/hair_front_shion.jpg) 
![](img/hair_front_x1/hair_front_shya.jpg) 
![](img/hair_front_x1/hair_front_sora.jpg) 
![](img/hair_front_x1/hair_front_yukari.jpg) 
![](img/hair_front_x1/hair_front_yoko.jpg) 
![](img/hair_front_x1/hair_front_hiromi.jpg) 
![](img/hair_front_x1/hair_front_kaoru.jpg) 
![](img/hair_front_x1/hair_front_ejima.jpg) 
![](img/hair_front_x1/hair_front_tatsumi.jpg) 
![](img/hair_front_x1/hair_front_makoto.jpg) 
![](img/hair_front_x1/hair_front_director.jpg) 
![](img/hair_front_x1/hair_front_saki.jpg) 
![](img/hair_front_x1/hair_front_asagi.jpg) 
![](img/hair_front_x1/hair_front_cameraman.jpg) 
![](img/hair_front_x1/hair_front_yoriko.jpg) 
![](img/hair_front_x1/hair_front_mori.jpg) 
![](img/hair_front_x1/hair_front_kazuko.jpg) 
![](img/hair_front_x1/hair_front_kazuko(male).jpg) 
![](img/hair_front_x1/hair_front_grandpa.jpg) 
![](img/hair_front_x1/hair_front_susumu.jpg) 
![](img/hair_front_x1/hair_front_kenji.jpg) 
![](img/hair_front_x1/hair_front_ykma.jpg) 
![](img/hair_front_x1/hair_front_risa.jpg) 
![](img/hair_front_x1/hair_front_mika.jpg) 
![](img/hair_front_x1/hair_front_akane.jpg) 
![](img/hair_front_x1/hair_front_nishina.jpg) 
![](img/hair_front_x1/hair_front_aoi.jpg) 
![](img/hair_front_x1/hair_front_grandma.jpg) 
![](img/hair_front_x1/hair_front_fumiya.jpg) 
![](img/hair_front_x1/hair_front_reiko.jpg) 
![](img/hair_front_x1/hair_front_matsu.jpg) 
![](img/hair_front_x1/hair_front_shoko.jpg) 
![](img/hair_front_x1/hair_front_tooru.jpg) 
![](img/hair_front_x1/hair_front_kyoko.jpg) 
![](img/hair_front_x1/hair_front_kazuma.jpg) 
![](img/hair_front_x1/hair_front_kazuki.jpg) 
![](img/hair_front_x1/hair_front_chinatsu.jpg) 
![](img/hair_front_x1/hair_front_mai.jpg) 
![](img/hair_front_x1/hair_front_aya.jpg) 
![](img/hair_front_x1/hair_front_tenko.jpg) 
![](img/hair_front_x1/hair_front_kimihiro.jpg) 
![](img/hair_front_x1/hair_front_tomomi.jpg) 
![](img/hair_front_x1/hair_front_hanzu.jpg) 
![](img/hair_front_x1/hair_front_te.jpg) 
  

- 似乎有些人头顶的头发薄(矮)、有些人的厚(高)。比如：
    - 头顶的头发薄的人： 辰巳，早纪，
    - 头顶的头发厚的人： 雅美，浅葱，真琴，


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  config: ["MMLorHTML.js"],
  jax: ["input/TeX", "input/AsciiMath", "output/HTML-CSS", "output/NativeMML"],
  extensions: ["MathMenu.js", "MathZoom.js"],
  TeX: {
    extensions: ["AMSmath.js", "AMSsymbols.js"],
    equationNumbers: {autoNumber: "AMS"}
  }
});
</script>
<script id="MathJax-script" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js"></script>


1================

$$\begin{align}
f(u)  &= \sum_{j=1}^{n} x_jf(U_j)\\
      &= \sum_{j=1}^{n} x_jf(U_j)\\
\end{align}$$

1--------------- \frac{asdf}{sdf} ---------------  

2--------------- $\frac{asdf}{sdf}$ ---------------  

3---------------$$\frac{asdf}{sdf}$$---------------

2=================

<script type="math/tex; mode=display">\begin{align}
f(u)  &= \sum_{j=1}^{n} x_jf(U_j)\\
      &= \sum_{j=1}^{n} x_jf(U_j)\\
\end{align}</script>

1--------------- <script type="math/tex; mode=display">\frac{asdf}{sdf}</script> ---------------  

2--------------- <script type="math/tex; mode=display">$\frac{asdf}{sdf}$</script> ---------------  

3---------------<script type="math/tex; mode=display">$$\frac{asdf}{sdf}$$</script>---------------






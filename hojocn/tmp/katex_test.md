
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.16.0/dist/katex.min.css" integrity="sha384-Xi8rHCmBmhbuyyhbI88391ZKP2dmfnOl4rT9ZfRI7mLTdk1wblIUnrIq35nqwEvC" crossorigin="anonymous">

    <!-- The loading of KaTeX is deferred to speed up page rendering -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.16.0/dist/katex.min.js" integrity="sha384-X/XCfMm41VSsqRNQgDerQczD69XqmjOOOwYQvr/uuC+j4OPoNhVgjdGFwhvN02Ja" crossorigin="anonymous"></script>

    <!-- To automatically render math in text elements, include the auto-render extension: -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.16.0/dist/contrib/auto-render.min.js" integrity="sha384-+XBljXPPiv+OzfbB3cVmLHf4hdUFHlWNZN5spNQ7rmHTXpd7WvJum6fIACpNNfIR" crossorigin="anonymous"
        onload="renderMathInElement(document.body);"></script>


1================

$$\begin{align}
f(u)  &= \sum_{j=1}^{n} x_jf(U_j)\\
      &= \sum_{j=1}^{n} x_jf(U_j)\\
\end{align}$$

1--------------- \frac{asdf}{sdf} ---------------  

2--------------- $\frac{asdf}{sdf}$ ---------------  

3---------------$$\frac{asdf}{sdf}$$---------------

2=================

<script type="math/tex; mode=display">\begin{align}
f(u)  &= \sum_{j=1}^{n} x_jf(U_j)\\
      &= \sum_{j=1}^{n} x_jf(U_j)\\
\end{align}</script>

1--------------- <script type="math/tex; mode=display">\frac{asdf}{sdf}</script> ---------------  

2--------------- <script type="math/tex; mode=display">$\frac{asdf}{sdf}$</script> ---------------  

3---------------<script type="math/tex; mode=display">$$\frac{asdf}{sdf}$$</script>---------------






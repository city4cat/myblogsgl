source:  
https://edition-88.com/blogs/blog/catseye40th-report02  

译注：  

- 本文的[官方中文版](../zz_weibo.cn/catseye40th-report02(2022-11-29).md)  
- 以下为每幅图配上了高分辨率的图片链接。但遗憾的是，这些高分辨率的图片里的大多数配图文字依然看不清。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/articles/IMG_3110_1214ccd0-575f-43a2-8ab5-410086b7f0cc_1400x.jpg) ([大图](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/articles/IMG_3110_1214ccd0-575f-43a2-8ab5-410086b7f0cc_4800x.jpg))  

# キャッツ♥アイ40周年記念原画展〜そしてシティーハンターへ〜　博多 展示会レポート  
2022年 11月 28日  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3139_600x600.jpg) 
([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3139_4800x4800.jpg))    

東京（アーツ千代田 3331 1階 メインギャラリー）で開催された「キャッツ♥アイ40周年記念原画展」。  
ご好評につき、北条司先生のゆかりの地、福岡で巡回展を開催することができました！  
キャナルシティ博多にて、11月30日（水）まで開催しています📢  

【開催情報】  
開催期間：11月19日(土)～11月30日(水)  
開催場所：キャナルシティ博多　サウスビルB１　バンダイナムコ Cross Store博多 内  
営業時間：10：00～21：00　※展示の最終入場は20：30まで  

展示会場の入口には、泪、瞳、愛をイメージした祝花（コアミックス様より）が飾られています！とても華やかです🌹✨  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0985_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0985_4800x4800.jpg))   

この展示会のために描き下ろされたメインビジュアル。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3097_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3097_4800x4800.jpg))   

中に進むと3姉妹が皆様をお出迎え🥰  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3098_e781449e-7090-4fda-bccb-bbdcc355cbb4_1024x1024.jpg)  
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3098_e781449e-7090-4fda-bccb-bbdcc355cbb4_4800x4800.jpg))   

東京会場で北条先生に書いていただいたサインも展示しています🖊  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3099_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3099_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3100_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3100_4800x4800.jpg))   

さらに…博多会場にご来場いただいた方達のために、色紙も書いていただきました‼  
北条先生、ありがとうございます。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_4800x4800.jpg))   

存在感ある豪華な額が美しい原画をより際立たせています✨  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3109_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3109_4800x4800.jpg))   

ガラスケースにはキャツ♥アイの完全版のコミックスや、滅多に見ることができない貴重なネーム、先生が使用していた画材や資料を展示しています。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3107_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3107_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3106_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3106_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3105_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3105_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_4800x4800.jpg))   

キャッツ♥アイのカラー原画や生原稿を近くで見れるチャンス👀  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3127_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3127_4800x4800.jpg))   

「シティーハンター ‐XYZ‐」の貴重な生原稿を始め、数々のカラー原画を厳選して展示。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3131_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3131_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_4800x4800.jpg))   

読み切り「スペース・エンジェル」全83ページが一気に読めるパネル展示エリア。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_4800x4800.jpg))   

フォトスポットは展示会場の中に1ヵ所、外に2カ所設置しています。  
ご来場の際は是非撮って帰って下さいね📸  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3118_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3118_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0986_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0986_4800x4800.jpg))   

最後に物販コーナーをご紹介👐  

受注商品の版画やB2複製原稿、B2高級アートプリントは全て実物を展示しています。  
＼ECでも販売中！／  
[お買い求めはこちらから](https://edition-88.com/collections/hojotsukasa)  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0996_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0996_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_4800x4800.jpg))   

図録はもちろん、通販で完売していた商品も一部再販中！  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0994_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0994_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_1004_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_1004_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0989_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0989_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0993_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0993_4800x4800.jpg))   

「キャッツ♥アイ」と「シティーハンター」グッズがまとめて購入できちゃう！  
日用品として使用できるアイテムから、コレクションアイテムまで色々お取扱いしています🌟  
展示を堪能した後は、ごゆっくりこちらでお買い物もお楽しみ下さいね😊  

 

＊前回（アーツ千代田 3331）の展示会レポートは[こちら](https://edition-88.com/blogs/blog/catseye40th-report)  

[イベント特設ページへ](https://edition-88.com/pages/catseye)  




















































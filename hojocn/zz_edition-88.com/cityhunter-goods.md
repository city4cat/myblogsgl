source: https://edition-88.com/pages/cityhunter-goods  

译注1：原文发表于2022年4月～5月期间。后因2022年11月在福冈县的再次画展而有所更改。本文将更改的内容标记出来。    

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunterlogo_1000x.jpg)  


## 版画

北条司先生の直筆サインが入る豪華な仕様となっているほか、全て原画原寸サイズで作られており、原画そのままの見た目を再現しています。  
エディション数は国内版200、インターナショナル版180、数量限定で販売いたします。  

版画一枚一枚に職人による手作業が施されています。  
シティーハンターの版画では、一枚ごとにパール絵の具の粒子を吹き付けています。  
見る角度によって光沢が強調され、作品の奥行きが表現されています。  


---------------

受注商品  (译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/シティーハンター))  

「シティーハンター」 版画1（週刊少年ジャンプ 1997年 特別編集　SUMMER SPECIAL/巻頭ポスター用イラストより）　

￥59.400（税込）  

●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦471×横622×厚さ20mm  

5/13（金）より通販でも受付開始予定。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/01_1_074c4a0a-369e-482a-a2f6-7f2c941da03f_800x.jpg?v=1650285232)  


-------------


受注商品  (译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/シティーハンター))  

「シティーハンター」 版画2（週刊少年ジャンプ 1989年 第50号 扉絵より）　

￥59.400（税込）  

●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦622×横471×厚さ20mm  

5/13（金）より通販でも受付開始予定。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/01_1_800x.jpg?v=1650285109)  

---------------


受注商品  (译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/シティーハンター))  

「シティーハンター」 版画3（北条司 ILLUSTRATIONS 1991年 付録ポスター用イラストより）　

￥59.400（税込）  

●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦622×横471×厚さ20mm  

5/13（金）より通販でも受付開始予定。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/03_1_800x.jpg?v=1650285192)  

--------------



新作(译注：福冈展新增)  

「シティーハンター」 版画4（週刊少年ジャンプ 1987年 第11号　扉絵より）  

￥59.400（税込）  

●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦471×横622×厚さ20mm  

11/19（土）より通販で販売開始。  
会場では展示のみになります。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hanga-city_400x.jpg)  

--------------

## GOODS

※商品ラインナップ、商品の仕様は予告なく変更になる場合がございます。その場合の詳細は決定次第、サイト内の商品ページやTwitter等にて発表します。  
※商品画像はイメージです。実際の商品とは異なる場合がございます。  
※商品の数には限りがあります。在庫に関するお問い合わせ（現在の在庫数や入荷予定等）にはご対応できません。あらかじめご了承ください。  
※会期前・会期中を問わず購入個数制限を変更する場合がありますのでご了承ください。  
※本会場での決済方法は、現金とクレジットカード（VISA、mastercard、AMERICANEXPRESS）のみご利用いただけます。(译注：福冈展无这句话)  


- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga1.jpg?v=1650535781)  
複製原稿1（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_2.jpg?v=1651475748)  
複製原稿2（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_3.jpg?v=1651475748)  
複製原稿3（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_4.jpg?v=1651475749)  
複製原稿4（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga2.jpg?v=1650535781)  
複製原稿5（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_6.jpg?v=1651475749)  
複製原稿6（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga3.jpg?v=1650535781)  
複製原稿7（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_8.jpg?v=1651475749)  
複製原稿8（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_9.jpg?v=1651475749)  
複製原稿9（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_010.jpg?v=1651475752)  
複製原稿10（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_CF1.jpg?v=1650535781)  
クリアファイル（XYZ/Green）  
¥500（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_CF2.jpg?v=1650535784)  
クリアファイル（XYZ/Navy）  
¥500（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/city-coaster.jpg?v=1651475727)  
アートタイル（全8種類）  
各¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/CITY_mag_1.jpg?v=1651425547)  
マグカップ（XYZ）  
¥1,650（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_hand_towel2.jpg?v=1650535781)  
ハンドタオル（Darts）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_hand_towel1.jpg?v=1650535784)  
ハンドタオル（Sniper）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_towel3.jpg?v=1651426744)  
ハンドタオル（Building）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_megane1.jpg?v=1651426744)  
メガネ拭き（Night view）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_megane-fuki.jpg?v=1650535781)  
メガネ拭き（Building）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_megane3.jpg?v=1651426744)  
メガネ拭き（Beach）  
¥880（税込）  




























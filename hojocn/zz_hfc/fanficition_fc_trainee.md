source:  
http://hojofancity.free.fr/WorkDisplay.php?v=1&st=1&series=3&choix=0&fm=&status=0&s=977&t=

Fanfiction :: FAMILY COMPO: LE STAGIAIRE

Chapter 1 :: L'Homme que je suis

Published: 09-09-09 - Last update: 16-09-09

Comments: Une simple introduction du personnage principal, qui est le seul à date qui est sorti de mon imagination et qui n'appartient pas à Maître Hôjô.

 You’re just too good to be true.... Can’t take my eyes off you...  

 

Ces deux phrases tirées de la chanson “Can’t Take My Eyes Of You”, de Frankie Vallie, résument très bien comment je me suis senti la première fois que je l’ai vue chez Maître Sora...  

 

Mais tout d’abord, je crois que je devrais commencer par me présenter à vous, n’est-ce pas? Je m’appelle Patrick Decker et je suis Canadien. Je viens d’une petite ville nommée Charny, dans la Province de Québec... Il y a quelques années, j’étais friand de sports et je m’entraînais à tous les jours pour garder la forme et une belle musculature. J’avais des chances d’être repêché par les Canadiens de Montréal, la fameuse équipe qui a gagné plus de Coupe Stanley que toute autres équipes dans la Ligue Nationnale de Hockey...  

 

J’étais soigneux de ma personne, je gardais une chevelure courte, un visage glabre et ma vue était perçante. Je marchais d’un pas assuré, le dos bien droit, sans honte ni gêne de ma personne... J’avais un caractère jovial, j’étais ouvert d’esprit et j’étais toujours prêt à aider mon prochain, qui que fut mon prochain.  

 

Et puis un jour de Novembre, ma vie fut transformée à jamais...  

Si Montréal est une ville ou le Hockey est roi, le phénomène des gangs de rues est malheureusement le prince... Deux bandes rivales s’étaient déclaré la guerre et s’attaquaient à tous moment du jour ou de la nuit, sans prendre compte des conséquences que leur petite guéguerre causait... Vous aurez donc deviné que je fus victime de l’une de leurs attaque. En fait, c’était une tentative de meurtre, mais si elle a échoué (parce que le chef de la bande qui était visé a survécu par je ne sais quel miracle), je n’y échappai pas. Je me trouvais à proximité lorsque la voiture piègée explosa, envoyant dans tous les sens débris fumants de verre et de métal tordu.  

 

Deux personnes qui n’étaient que des passants trouvèrent la mort, mais moi, même si je survécus, j’aurais préféré être mort à ce moment-là. Je passai deux semaines dans le coma et lorsque j’en sortis, j’appris que ma jambe gauche, qui avait reçu un énorme éclat du pare-brise, avait été atrocement mutilée au point que je devrais marcher avec une canne pour le restant de ma vie. Les médecins ont quand même pu la sauver, mais j’avais compris à ce moment-là que tout espoir de faire une carrière sportive m’avait été volé et ce, à perpétuité. Non seulement ma jambe fut-elle abîmée, mon visage également avait été atteint par un éclat de métal. J’en garde une longue cicatrice qui trace une diagonale, partant du haut de mon arcade sourcillière droite et descend sous mon oeil gauche pour se terminer au milieu de ma joue...  

 

Longtemps me suis-je dégoûté à toutes les fois ou je me regardais dans un miroir. Pour cacher ma balâfre, je me suis laissé poussé les cheveux en m’éfforçant de cacher mon visage avec. Avant cette tragédie, je m’habillais de vêtements colorés et brillants... Suite à ces événements, comme pour porter le deuil de ma carrière sportive, je me suis mis à ne porter que du noir et du blanc... Je n’ai pas complètement délaissé le Gym, mais j’y suis moins assidu...  

 

Durant ma convalescence, je me suis découvert un talent pour le dessin, plus particulièrement pour le style manga japonnais. Ma blessure m’ayant rendu invalide à tous travaux physique, mon assurance me versant une pension annuelle non négligeable, j’ai décidé d’entreprendre un stage avec un mangaka. C’est à l’hôpital que je découvris “Notre Emblême”, par Sora Wakanaé.  

 

J’en suis alors tombé amoureux. Comme un drogué qui attend avec impatience sa prochaine dose, j’attendais la prochaine publication, comme si j’étais en manque. Dès que j’ai u accès à un ordinateur, j’ai fais quelques recherches rapides sur Internet ou j’ai trouvé le site web de Maître Sora grâce auquel j’ai pu entrer en contact avec lui. Après un échange via E-mail assez bref, mais explicite, Maître Sora accepta de me prendre sous son aile en tant que stagiaire, pour un an. Il accepta de m’héberger dans sa maison pour la durée de mon stage, mais il insista pour me téléphoner afin de parler de quelques “détails importants” qui devaient à tout prix rester confidentiels. Je lui certifiai que j’étais un homme de parole et que j’étais même prêt à signer un contrat avec lui.  

 

Quand le coup de téléphone tant attendu eut lieu, Maître Sora prit le temps de m’expliquer qu’il formait un “couple inversé”... Devant mon incompréhension, il m’expliqua qu’en fait, il était une femme de naissance, mais qu’il avait l’âme d’un homme, et que son épouse, Yukari-san, était un homme de naissance avec le coeur et l’âme d’une femme. J’avoue avoir été surpris, mais je lui certifiai que cette situation ne me gênait absolument pas. Il me parla alors de sa fille, Shion, puis de son neveu Masahiko Yanagiba qu’il hébergeait suite au décès de son père trois ans auparavant. Il termina en me parlant de ses assistantes, trois travestis et une transsexuelle. J’assurai Maître Sora que je n’avais aucun problême à travailler avec ces personnes-là et lui expliquai qu’en fait, cela m’arrangeait, parce que le genre de femmes qui m’attirent sont un peu comme sa femme... Il voulut savoir de quoi j’avais l’air pour qu’il puisse me reconnaître quand il viendrait me chercher à l’aéroport.... C’est avec beaucoup de gêne et de honte que je me décrivis... D’un ton appaisant, presque paternel, il m’assura qu’il ne prêtait pas attention à l’apparence des gens, qu’il préférait regarder la beauté de leurs âmes...  

 

Quand nous nous quittâmes, j’avoue que j’étais surpris parce que je venais d’apprendre... Jamais je n’aurais cru, en voyant sa photo sur le site, ou en l’entendant parler au téléphone, que Maître Sora puisse être en fait une femme... J’étais curieux de voir et d’entendre sa femme... De même que leur fille...  

 

Voilà, vous savez qui je suis, ce qui m’est arrivé pour que j’en vienne à m’intéresser au dessin et comment toute cette histoire a commencé. Et surtout, comment j’ai finalement connu la beauté de l’amour, en dépit de mon horrible apparence...  






Chapter 2 :: Accueil

Published: 14-09-09 - Last update: 16-09-09

Comments: Patrick Decker arrive au Japon et rencontre certains regards étrange sur son chemin à cause de son apparence un peu particulière. Arrivé chez les Wakanaé, il fera la connaissance de Masahiko et de ses amis... NOTE: j'ai pris la liberté de baptiser Le Directeur du club cinéma "Lee" et voici pourquoi: ce fut seulement dans le but de le rendre plus familier. Je n'ai pas eu à chercher bien loin our trouver ce nom: il porte souvent un chandail avec l'inscription "Lee" sur le ventre... un peu comme s'il faisait sa propore publicité. Bonne lecture! 

En principe, mon stage ne devait commencer que la semaine suivante. Cependant, j’ai décidé de devancer mon départ histoire de m’acclimater au décallage horaire, ainsi qu’aux coutûmes de ce pays. La famille Wakanaé ne s’attendant pas à me voir arriver aussi tôt, j’avais là une belle occasion de leur faire une surprise. Force m’est d’avouer qu’en fait je n’y tenais plus d’attendre la date officielle de mon départ, de là ma décision de partir plus tôt pour le Japon!  

 

Quand j’arrivai à l’aéroport de Tokyo, je ne portait qu’un simple jean noir, un tee-shirt blanc et un blouson de cuir également noir. Était-ce simplement mon accoutrement qui attirait le regard des autres passagers, était-ce ma cicatrice, ou était-ce parce que je marchais plus lentement que ces derniers parce que je devais m’appuyer sur ma canne? Je l’ignore, mais les oeillades qu’on me lançait à la dérobée commençaient à m’agacer sérieusement.  

 

Alors que j’allais chercher mes bagages, qui se résumaient en fait qu’en un gros sac à dos de voyage contenant quelques vêtements et un portfolio contenant quelques dessins que j’avais faits avant d’entreprendre ce voyage, il me semblait que tous les gens que je croisais sur ma route me regardaient en catimini tout en se parlant à voix basse. Mal à l’aise, je me hâtai de récupèrer mes bagages et je me dirigeai vers la sortie aussi vite que mes jambes me le permettaient.  

 

Lorsque je mis le pied à l’extérieur, la chaleur de ce mois de juillet fit sentir tout le poids de sa lourdeur sur mes épaules. J’hêlai un taxi et donnai au chauffeur l’adresse de Maître Sora. Ce dernier me parla en français avec un très fort accent, et j’appréciai son effort. Je lui expliquai que je pouvais lui parler en anglais pour lui faciliter la tâche, mais il insista pour me parler dans la langue de Molière, prétextant qu’il voulait se pratiquer, étant donné qu’il devait aller en France le mois suivant pour le mariage de son frère. Parvenu à destination, je payai pour la course, puis je descendis de la voiture. Le chauffeur vint m’aider à sortir mes bagages du coffre arrière de la voiture et me serra la main dans un dernier geste d’adieu, puis il s’en alla.  

 

Je pris quelques minutes pour regarder cette belle et grande maison qui allait devenir mon foyer d’adoption pour l’année qui venait, mon sac à dos et mon portfolio posés à mes pieds. Je fus impressionné de voir sa taille: entourée d’un haute clôture, munie d’un balcon au premier, les murs peints en blanc et le toit recouvert de tuiles noires, elle avait tout d’une maison de rêve. Je levai les yeux vers le ciel et je me départis de mes lunettes fumées pour en admirer le bleu éclatant. Aucun nuage ne paraissait à l’horizon et le soleil dardait de milles rayons brûlants. Combien de temps restai-je planté là? Je l’ignore, mais un lointain bruit de dispute me tira de ma rêverie.  

 

J’aperçus alors un jeune homme au début de la vingtaine qui marchait vers moi, l’air très contrarié. Il parlait d’un ton colèrique et agitait les mains au-dessus de sa tête. Il était accompagné de deux autres jeunes hommes qui étaient le contraire l’un de l’autre: le premier était grand et bien bâti, avec les cheveux noirs tombant devant son visage comme des rideaux; le second étant plus petit, arborant une bedaine bien développée et portant une casquette de travers sur sa tête. Ses yeux étaient cachés derrières des lunettes noires aux montures rondes.  

 

-Tu ferais mieux d’oublier ça, Lee!, disait le jeune homme en colère. Il n’est pas question que je réssucite Masami pour la suite du seul et unique film dans lequel elle a joué! J’ai déjà donné plus qu’à mon tour pour elle!!  

 

-Mais Giba, répliqua Lee, le jeune homme à l’énorme bedaine, le club ciné n’a pas connu autant de succès depuis qu’elle a fait ce film... Il risque de disparaitre si on ne garde pas l’intérêt des gens! On a besoin de Masami! Il n’y a que toi pour...  

-Tu es sourd, ou quoi?!, répliqua Giba et s’arrêtant et en se retournant vers Lee. Je te répète que c’est hors de question! Ejima n’aura qu’à se ficher une robe sur le dos et à faire la fille! Moi, je démissionne!!  

 

Ejima, le jeune homme grand et bien bâti, eut un sursaut et, d’un ton badin, il dit:  

 

-Masahiko, tu m’as déjà vu habillé en fille quand on a travaillé pour Mr. Tatsumi. J’ai l’air horrible! C’est toi qui est criant de vérité dans la peau de Masami!  

 

Je ne pus réprimer un sourire... Ainsi donc, je venais de faire la connaissance de Masahiko Yanagiba, le neveu de Maître Sora. Appuyé sur ma canne, je continuai de regarder le spectacle, amusé à l’idée de voir ce jeune homme aussi révolté à l’idée qu’on ose lui demander de se déguiser en fille pour un film... Ce qu’il avait déjà fait auparavant, semblait-il...  

 

-Masami ne m’a apporté que des ennuis, tu sauras!!!, répliqua Masahiko avec véhémence. À commencer par Tatsumi, justement! Ça m’a pris un sacré bout de temps pour réussir à le convaincre de me foutre la paix définitivement et crois-moi, ça n’a pas été une mince affaire! En plus de m’embarrasser devant Yoko, j’ai du repousser les avance de Akané qui croyait dur comme fer que j’étais une vraie fille! Masami porte malheur aux gens, je vous dis!  

-Giba..., plaida Lee d’une voix suppliante, tu es le seul qui peut sauver le club cinéma de l’université! S’il-te-plaît! Juste pour ce film, et ensuite, ce sera fini! C’est notre dernière année et je veux la terminer en beauté! Ce sera à toi que je le devrai et à personne d’autre!  

-Et pour ça, il va falloir que j’embrasse encore Ejima, c’est ça?!, s’exclama Masahiko avec froideur. Pas question! La dernière fois, il a eu l’air d’aimer ça!  

-Hé! Qu’oses-tu insinuer, toi?!, répliqua l’autre d’un air scandalisé.  

 

Ils avaient continué de se chamailler tout en reprenant leur marche vers moi, sans me remarquer. Ils s’arrêtèrent à nouveau à deux ou trois mètres de moi et Masahiko continua, rouge de colère:  

 

-Dis-moi, Lee, quand tu seras dans le show business, vas-tu te limiter à produire des films avec un travesti comme acteur principal? Il va falloir que tu trouves des arguments plus solides qu’une simple promesse d’un succès fulgurant (non assuré je te signale!)!!  

-Giba, je ne voulais pas en arriver là, mais comme tu ne me donnes pas le choix... tu te rappelles de cette vidéo qu’on a fait de toi dans la douche..., répliqua alors Lee en prenant un ton cajôleur. Je ne l’ai pas encore détruit, au cas ou j’en aurais besoin....  

-Que?!?!?, hoqueta Masahiko, incrédule.  

 

Affichant un air faussement désolé, le maître chanteur dit:  

 

-Ce serait vraiment dommage si par mégarde, lors d’une projection au club, je ferais jouer ce film... Par erreur....  

 

Je vis Masahiko blêmir, tellement que je crus un instant qu’il allait défaillir. Il chancela sur ses pieds, les bras ballant le long de son corps, les yeux écarquillés d’horreur. Ejima regardait la scène avec amusement, les mains dans les poches de son pantalon, un sourire en coin sur les lèvres. Lee avait croisé ses mains sur son énorme ventre, un air satisfait sur son visage, attendant ce que Masahiko allait dire...  

 

N’y pouvant plus de voir le petit manège de Lee, je me décidai à agir. Je me râclai la gorge bruyamment pour attirer l’attention des trois jeunes hommes et je m’approchai d’eux en m’appuyant sur ma canne. Un sourire aux lèvres, je tendis la main vers Masahiko et lui dit:  

 

-Bonjour, Masahiko, tu te souviens de moi?  

-Euh... , répondit ce dernier, l’air incertain, alors qu’il me serrait mollement la main.  

 

Il semblait chercher jusqu’aux tréfonds de sa mémoire un souvenir de moi, qu’il ne trouverait jamais parce qu’il n’avait certainement pas vu ma photo, puis du coin de l’oeil, je remarquai que Ejima et Lee me regardaient d’un air ahuri, l’air de se demander d’ou je pouvais bien débarquer, accoutré comme je l’étais... Et comme mon visage était à découvert parce que j’avais attaché mes cheveux en queue de cheval, ils avaient sûrement du remarquer ma cicatrice...  

 

-C’est moi, Patrick Decker, continuai-je. Je suis votre nouveau pensionnaire pour l’année qui vient!  

 

La lumière sembla s’allumer à l’étage supérieur. Il afficha alors un grand sourire et me sera à nouveau la main, un peu plus vigoureusement ce coup-ci, et il dit:  

 

-Mais oui! Biensûr je me souviens de toi! Tu es le Stagiaire d’Oncle Sora! Tu ne devais arriver que la semaine prochaine, non?  

-C’était le plan d’origine, dis-je, mais disons que je voulais prendre le temps de m’adapter un train de vie de Tokyo, faire un petit tour pour visiter et surtout, prendre le temps de m’ajuster au décallage horaire!  

 

Pendant tout le temps ou Masahiko et moi parlions, Ejima et Lee n’avaient cessé de me dévisager, comme s’ils n’avaient jamais vu un gars dans mon genre auparavant... Comme à l’aéroport, je commençais à en être royalement agacé et plus les secondes passaient, plus je sentais monter en moi l’envie de leur peler le sommet du crâne à coups de canne. Je cessai de sourire et, baissant les yeux vers le sol, m’appuyant à deux mains sur ma canne, je dis d’une voix sombre:  

 

-Masahiko, tu devrais avertir tes copains d’arrêter de me regarder comme ça. Je sais que j’ai une tête à faire peur, mais si je perds patience, je te promets qu’ils ne te harcèleront plus jamais pour que tu t’habilles en Masami, je le jure devant Dieu!  

 

Lee et Ejima eurent un sursaut, puis ils firent un pas en arrière, comme s’ils s’attendaient à ce que je fasse jaillir de ma canne une épée qui y aurait été dissimulée. Certes, j’étais agacé de me faire dévisager, mais je n’avais aucunement envie de les assassiner.  

 

-Laisse tomber, Patrick, répondit Masahiko d’un ton amusé. Le dernier qu’ils ont regardé comme ça, c’est Mr. Tatsumi... parce qu’ils en avaient sérieusement peur!  

 

-Il y avait de quoi, bredouilla Ejima en continuant de me regarder, les yeux remplis de crainte. Ce type est un Yakuza!  

-Je n’en suis pas un, rétorquai-je, alors calmez-vous, je n’ai nullement l’intention de tuer qui que ce soit! Seulement, je sais de quoi j’ai l’air, alors regardez ailleurs!  

 

Tournant délibérément le dos aux deux autres jeunes hommes qui n’avaient en rien perdu de leur malaise, Masahiko m’entraîna vers la maison Wakanaé, une main posée sur mon dos, et Lee protesta d’une voix boudeuse:  

 

-Hé Giba! On n’en a pas terminé avec cette conversation!  

-Moi oui, répondit Masahiko en s’éloignant de ses deux amis. Salut!  

-Alors juste une dernière chose: n’oublie pas qu’il y a une réunion du Club Ciné demain après midi à une heure! T’as intérêt à y être, sinon je te promets que tous les membres sauront quel marque de savon tu utilises pour te laver!  

 

J’entendis Masahiko grincer des dents et je jetai un regard par-dessus mon épaule pour voir Lee qui s’éloignait, la tête basse, les mains derrière son dos. Il était suivi de près par Ejima qui, les mains toujours dans les poches de son pantalon, me lança un regard à la dérobée. Je ne le vis que brièvement, mais il ne me fallut pas beaucoup de temps pour lire dans ses yeux qu’il ne m’aimait pas beaucoup... Comme quoi le vieil adage qui dit qu’on ne peut pas plaire à tout le monde est on ne peut plus vrai.  

 

Je fis mine de me pencher pour attrapper mes bagages, mais Masahiko s’en emparra en disant:  

 

-Laisse-moi faire, tu dois être épuisé... et tu dois avoir chaud avec cette chose sur le dos!  

 

Il lorgnait mon blouson de cuir d’un air amusé. Je sortis de la poche de mon jean un mouchoir et j’épongeai mon front en sueur en disant:  

 

-Quand je me suis embarqué à Montréal, il tombait des cordes et il faisait plutôt froid... Je n’avais pas envisagé qu’il ferait aussi chaud! À bien y penser, j’aurais peut-être dû me changer dans l’avion...  

 

Ricanant, il me conduisit jusqu’au seuil de la porte d’entrée et tandis qu’il foullait dans les poches de son pantalon pour y trouver ses clés, je lui demandai:  

 

-Au fait, qui c’est, cette Masami qui porte autant malheur?  

 

Ses épaules s’affaissèrent et il me regarda d’un ai gêné en me disant:  

 

-C’est un personnage que j’ai incarné dans le premier film étudiant auquel j’ai pris part en tant qu’acteur... J’ai malgré moi un certain talent pour “changer de sexe” à volonté... Et bien souvent, ça se fait CONTRE ma volonté...  

 

J’eus un petit sourire en coin et me voulant rassurant, je dis:  

 

-J’avais compris que c’était le sujet de ta conversation avec tes amis...  

-Tu as entendu ça? Répondit Masahiko d’un air dépité.  

-Bordel! Vous parliez si fort tous les trois.... Il m’aurait fallu être sourd pour ne pas vous entendre! Au moins, grâce à mon intervention, je t’ai temporairement tiré de ce mauvais pas...  

 

Masahiko étira le cou pour voir ses deux amis qui tournaient le coin de la rue, disparaissant de notre champs de vision, et il dit:  

 

-Tu ne les connais pas encore... Il savent se montrer tres persuasifs quand ils s’y mettent... J’ai pas fini d’en entendre parler, tu peux me croire....  

 

Il déverrouilla la porte et nous entrâmes à l’intérieur. Dès que je me trouvais dans le vestibule, je sentis l’air frais de la maison caresser mon visage et je me sentis instantanément plus léger. Alors qu’il alla poser mon sac à dos et mon portforlio au pied de l’escalier qui menait au deuxième étage, je regardai autour de moi. Tout, du plancher jusqu’au plafond, était d’une propreté immaculée.  

Les murs peints en blanc étaient décorés de bouquets de fleurs sèchées qui diffusaient une douce odeur de pot-pourri et le plancher du vestibule était recouvert d’un petit tapis moir ou étaient parfaitement allignées plusieurs paires de chaussures. Masahiko m’enleva mon blouson de sur le dos et il alla le suspendre dans l’un des placard d’entrée.  

 

-Tu ne vas pas garder cette chose sur le dos à l’intérieur! dit-il, l’air réprobateur. Si tu veux bien te déchausser, je vais te prêter une paire de pantoufles.  

 

Je m’assis sur le petit banc près de la porte et j’enlevai mes espadrilles. Voyant la porte qui se trouvait sur ma gauche, je demandai:  

 

-C’est la que se trouve ma chambre?  

-Non, cette pièce sert de débarras. Ta chambre se trouve au deuxième, juste en face de la mienne. Je te ferai visiter la maison plus tard, si tu le veux bien. Tu m’as l’air épuisé.  

-J’avoue que je n’ai pas dormi durant le vol et que je commence à ressentir un peu de fatigue, dis-je.  

-Suis-moi à la cuisine, je vais te donner un petit remontant.  

M’appuyant sur ma canne, je me remis debout et je suivis Masahiko le long d’un petit couloir dont les murs étaient décorés de photos de la famille Wakanaé et au bout duquel se trouvait la salle à manger. Sur ma droite, une porte donnait accès au salon ou je vis le très invitant canapé en L qui me semblait attrocement comfortable. Je n’avais pas l’habitude de me tenir debout aussi longtemps et je commencais à avoir sérieusement besoin de poser mes fesses sur quelque chose de comfortable pendant un très long moment.  

 

La salle à manger était séparée de la cuisine par un petit comptoir et il y avait également un accès au salon. Après m’avoir invité à m’asseoir, Masahiko ouvrit le frigo et il me demanda:  

 

-Qu’est-ce que tu aimerais boire? On a des sodas, du jus de pommes, de la limonade ou si tu veux, on garde une bouteille d’eau glacée dans le frigo...  

-Un verre de limonade serait l’idéal, merci, répondis-je en m’asseyant et en posant ma canne contre le comptoir.  

 

Masahiko alla prendre un verre et le remplit de Limonade, puis il me l’apporta. Il vint s’asseoir devant moi et après un court silence, il dit:  

 

-Ton voyage s’est bien passé au moins?  

-Comme un charme, dis-je en souriant. Ç’a été un peu long, je t’avouerais, mais je ne suis pas fâché d’avoir fait le saut au-dessus de l’océan Pacifique. La vue était à couper le souffle.  

 

Masahiko me regardait en souriant et même s’il semblait me dévisager, je n’en resentis aucun malaise parce qu’il ne le faisait pas de la même façon que ces gens à l’aéroport, ou comme ses amis. Je posai mon verre sur la table et je dis:  

 

-Masahiko, tu es sûrement le premier depuis que je suis arrivé qui ne semble pas être dérangé par mon aspect physique.  

-Pourquoi le serais-je?, me demanda-t-il, étonné.  

-Parce que depuis mon arrivée, je n’ai pas cessé de me faire dévisager par tous ceux que je croisais sur mon chemin...  

-Je n’accorde pas vraiment d’importance à l’apparence des autres, c’est tout, et tu n’as pas à me dire pourquoi tu as cet aspect, mon oncle nous a raconté ton histoire.  

 

Je souris. Maître Sora s’était déjà montré très attentionné à mon égard avant même que j’eus mis le pied chez lui.  

 

-Tu as dû en baver un coup pour récupèrer après ça..., dit-il.  

-Disons que j’ai découvert quelque chose qui m’a donné une nouvelle raison de vivre...  

-Une femme?, demanda-t-il avec un petit sourire complice.  

 

Je pris malgré moi une expression sarcastique et je dis, en désignant à la fois ma jambe et la balâfre qui barrait mon visage:  

 

-Il y a certaines choses qui déplaisent aux femmes....  

 

Masahiko parut sincèrement gêné et il dit:  

 

-Je m’excuse, je ne pensais pas...  

-T’inquiète, même si je ne cherche pas, ça ne veut pas dire que j’ai renoncé...  

-Alors, c’est quoi cette nouvelle raison de vivre?  

 

J’entrepris alors de lui raconter comment j’avais découvert “Notre Emblême”, le manga dont Maître Sora était l’auteur. Je lui parlai également de mon engoument pour le dessin style Manga, comment j’ai développé mon talent et comment j’ai eu l’idée de venir au Japon pour mon stage. Je lui racontai également qu’outre le dessin, j’avais également un grand intérêt pour la musique et combien j’adorais jouer du piano et de la guitare. Embarassé, il me confia qu’il n’avait aucun talent pour le dessin, ni même pour la musique... que son seul véritable talent était de se faire passer pour une fille...  

 

-J'ai ça dans le sang, dit-il d'un air résigné.  

 

Quand je lui demandai comment il en était venu à jouer un personnage de fille dans un film étudiant, il me raconta que tout celà était dû à un hasard, alors que Ejima le réprimandait parce qu’il buvait avec le petit doigt en l’air. Lee avait été témoin de la scène et il avait décidé de lui donner un petit rôle de figuration sous les traits d’une fille. Quand il avait vu de quoi il avait l’air sous les traits de Masami, Lee avait changé les plans originaux pour en faire l’actrice principale...  

Quand je lui demandai qui était Yoko, Masahiko sembla se rembrunir en me disant que Yoko était son ex-petite amie... Il ne voulut pas s'épancher sur les détails qui ont mené à leur séparation, mais il me dit qu’elle avait toujours éprouvé des difficultés à accepter le fait qu’il aie joué le rôle d’une fille à l’écran. Après une série de maladresses, elle avait trouvé le réconfort dans les bras de quelqu’un d’autre... Elle n’en avait jamais vraiment parlé, mais elle croyait que son oncle et sa tante avaient une mauvaise influence sur lui et qu’ils l’incitaient à se travestir.  

 

Je souris et je vidai mon verre d’un trait. Durant la demie-heure qui suivit, nous parlâmes de beaucoup de choses. Durant la conversation, nous allâmes nous asseoir sur le canapé du salon ou je pus enfin détendre mes jambes. Il me parla des événements qui l’avaient amené à venir vivre chez les Wakanaé, comment sa cousine Shion lui il avait fait découvrir que son oncle et sa tante avaient échangés leurs rôles, comment il avait d’abord pris la chose (très difficilement), puis comment il avait appris à les accepter comme ils étaient. Il m’expliqua qu’il avait beaucoup grandi depuis qu’il avait emménagé chez son oncle et sa tante, qu’il avait appris à être plus ouvert d’esprit... et surtout, qu’il avait trouvé une famille ou il n’était plus seul et ou il pouvait toujours compter sur les soutien des autres. Au cours de cette conversation, le sui confiai que j’étais attiré par les femmes dans le genre de sa tante Yukari, chose qu’il accueillit avec le sourire, en disant que cette dernière était déjà prise (pas de chance pour toi, me dit-il en riant) mais qu’il y avait quatre assistantes qui travaillaient avec son oncle, que trois d’entre elles étaient des travesties et que la quatrième était une transsexuelle, ce que je savais déjà parce que Maître Sora m’en avait parlé lors de notre échange téléphonique.  

 

Nous fûmes interrompus par le bruit de la porte d’entrée qu’on claqua sans ménagement et nous entendîmes le bruit d’un sac à dos qu’on jetait par terre avec force. La voix d’une jeune fille, apparament de très mauvaise humeur, se fit alors entendre:  

 

-J’en ai assez de ce vieil imbécile! Pas étonnant qu’on le surnomme le Général! C’est pas un restaurant qu’il dirige, c’est un damné camps de concentration!  

 

Je ne pus réprimer un sourire en entendant cette remarque. Une très belle adolescente fit alors son apparition devant la porte du salon. De tout évidence, les vêtements qu’elle portait constituaient son uniforme de travail: chemise rouge, pantalon noir, tablier blanc et casquette blanche (qu’elle tenait dans sa main). Ses longs cheveux bruns étaient détachés et tombaient en cascades dans son dos. Elle avait l’air de quelqu’un on avait franchement poussé à bout et lorsqu’elle entra dans la pièce, elle salua Masahiko. De toute évidence, elle ne m’avait pas remarqué, trop occupée qu’elle était de pester contre son patron. Pendant cinq bonnes minutes, elle expliqua à son cousin comment “Le Général” jugeait qu’elle n’était pas assez rapide au travail, qu’elle n’était pas assez souriante avec les clients, qu’elle pourrait donner le meilleur d’elle-même...  

 

-Shion, dit Masahiko lorsqu’il put enfin en placer une, nous ne sommes pas seuls!  

-Quoi, de quoi tu parles?, répliqua Shion d’un ton excèdé.  

 

Masahiko me désigna du pouce et la jeune fille se tourna vers moi. Lorsqu’elle me vit, elle eut un sursaut et, l’air partagée entre la surprise et l’effroi, elle me dévisagea de la tête aux pieds, muette de stupéfaction à l’idée qu’elle aie pu me manquer... Un petit sourire amusé sur les lèvres, Masahiko dit:  

 

-Patrick, je te présente Shion Wakanaé, ma chère et tendre cousine. Shion, je te présente Patrick Decker, notre nouveau pentionnaire. 







Chapter 3 :: La famille Wakanaé

Published: 15-09-09 - Last update: 16-09-09

Comments: Ici, Patrick fait la connaissance de la famille Wakanaé. Bonne lecture et merci de vos reviews!

Était-elle simplement incapable de parler, ou était-ce (encore une fois!) mon apparence qui l’avait paralysée, je ne puis le dire, mais pendant quelques instants, le seul bruit qu’on entendit fut le ronronement du moteur du réfrigérateur. Le regard fixé sur moi, le rouge lui monta lentement aux joue et à son expression, je pus deviner qu’elle cherchait activement quelque chose à dire pour se sortir de cette situation plutôt gênante.  

 

-Pensionnaire?!, finit-elle par dire, perplexe. Déjà un autre? Kaoru vient à peine de retourner vivre avec son père que sa chambre est déjà prise?? Je ne me souviens pas d’avoir vu un écriteau qui disait “chambre à louer”.  

-Shion, Patrick est le nouveau Stagiaire d’Oncle Sora, répondit Masahiko. Je croyais que tu étais au courant, ajouta-t-il d’un air taquin.  

 

La compréhension se peignit alors sur le beau visage de Shion. Elle se secoua la tête d’un air embarrassé, puis elle m’adressa un sourire chaleureux et parfaitement craquant en disant:  

 

-Je m’excuse de ne pas t’avoir remarqué avant, dit-elle sur un ton d’excuse, mais je ne m’attendais pas à te voir arriver si tôt! Ta chambre n’est pas encore prête.  

-Kaoru n’a pas emporté tout son stock?, demanda Masahiko, l’air surpris.  

-Non, répondit Shion en s’asseyant à côté de moi. Il avait tellement de trucs à ramener chez lui que les déménageurs engagés par Mr. Tatsumi n’ont pas pu tout caser dans leur camion... Il reste encore plusieurs cartons et ils ont dit qu’ils reviendraient les chercher un peu plus tard cette semaine.  

-Tout ce dont j’ai besoin, dis-je en souriant, c’est d’un lit pour dormir. Rien ne presse.  

-On n’aura qu’à entreposer le reste des trucs de Kaoru dans la chambre de débarras, dit Masahiko.  

 

À nouveau, je sentis le regard de Shion posé sur moi, comme si elle voulait détailler chaque partie de mon visage. Je devais me faire une raison: mon entourage était habitué à mon apparence peu ordinaire, mais j’allais devoir faire montre de patience pour ce qui était des nouveaux gens dont j’allais faire la rencontre ici... J’eus un sourire indulgent et je dis, à brûle-pourpoint, alors qu’elle était un beau milieu d’une tirade contre son patron au restaurant:  

 

-Shion, est-ce que mon aspect te dérange?  

 

Je la pris tellement au dépourvu avec ma question qu’elle eut un mouvement de recul et elle dit, l’air mal à l’aise:  

 

-Non, je... Qu’est-ce que tu veux dire?, balbutia-t-elle.  

-Tu n’as pas cessé de me dévisager depuis que tu es arrivée...  

 

Elle se mordit les lèvres et elle dit d’un air embarassé:  

 

-Je te demande pardon, Patrick, mais c’est plus fort que moi... Comprends-moi bien: je ne cherche pas à te mettre mal à l’aise, j’essaie seulement de voir au-delà de ta cicatrice... Puis-je te demander ce qui t’est arrivé?  

 

J’hochai la tête et j’entrepris de lui résumer ce qui m’était arrivé. La réaction qu’elle eut m’amusa: suggèrer d’engager Mr. Tatsumi et ses hommes sembla à ses yeux une excellente façon de faire payer veux qui étaient responsables de mon accident...  

 

Elle s’excusa et prit congé, prétextant qu’elle avait besoin de sortir de son “habit d’esclave” et de prendre une bonne douche. Alors qu’elle sortait de la pièce, je ne pus m’empêcher de remarquer que Masahiko la regardait avec une certaine affection qui me fit penser qu’il en pinçait peut-être pour elle... Je décidai de me taire parce qu’après tout, ça ne me regardait pas Après tou, je n’étais pas venu au Japon pour jouer les entremetteurs et si quelque chose devait se passer entre eus, autant laisser le temps faire seon oeuvre. Au moment ou j’allais demander à Masahiko de me faire visiter le reste de la maison, la porte d’entrée s’ouvrir à nouveau et nous entendîmes la voix d’une femme dire:  

 

-C’est nous, on est rentrés! Il y a des volontaires pour nous aider à rentrer les courses?  

-On est au salon, tante Yukari! Lança Masahiko en se levant. Attends ici, ils vont en faire une tête!, ajouta-t-il à mon intention, me faisant un petit clin d’oeil d’un air complice.  

 

Je pris le parti de me taire et je tendis l’oreille alors que Masahiko saluait sa tante et son oncle.  

 

-Je ne suis pas seul, il y a quelqu’un au salon, entendis-je Masahiko dire depuis le vestibule.  

-Tiens donc?, dit la voix masculine, mais tout de même douce, d’un homme qui ne devait être nul autre que Maître Sora. Tu veux nous dire qui c’est, ou tu préfères qu’on devine? Ajouta-t-il d’un ton moqueur.  

-Allez voir et vous verrez, je m’occupe de rentrer les courses! Répondit Masahiko avant de fermer derrière lui.  

-Je me demande qui ça peut bien être..., dit Sora d’un ton interrogateur.  

-Vas voir, chéri, je m’occupe de transporter les courses à la cuisine, répondit Yukari.  

 

Je m’étais senti assez calme depuis mon arrivé. J’étais certes un peu fébrile, heureux et j’avais hâte de rencontrer Maître Sora, mais en entendant les pas de ce dernier se rapprocher, j’avais carrément un embouteillage de papillons dans les tripes! Les mains un peu tremblantes, je pris appui sur ma canne et je me levai. Maître Sora entra dans la pièce et lorsqu’il m’aperçut, une expression de surprise mêlée d’incrédulité se peignit sur son visage. Jamais on aurait pu croire en le regardant que Sora Wakanaé était en fait une femme: de la tête aux pieds sa personne n’était que masculinité et virilité incarnées. Il devait faire au moins un mètre quatre-vingt-cinq, moi qui ne fait qu’un mètre quarante-cinq! Ses cheveux noirs étaient attachés en queue de cheval et quelques mèches rebelles retombaient sur son front. Il portait une chemise blanche et un pantalon noir.  

 

J’ignore quelle expression j’affichais à ce moment-là. J’espèrais juste ne pas avoir l’air d’un parfait imbécile. Je n’osais parler le premier, incertain de ce qui sortirait de ma bouche: quelque chose de stupide, quelque chose d’intelligent ou quelque chose d’autre...  

Puis un grand sourire se traça sur ses lèvres et il vint vers moi à grands pas, la main tendue, et il dit:  

 

-Patrick! Ça alors! Tu parles d’une surprise!!  

-Maître S-Sora je suis très heureux d-de vous rencontrer enfin!, marmonnai-je, trop nerveux pour articuler normalement.  

 

Maître Sora m’étreignit solidement et il dit dans un ricannement:  

 

-Oublie les “Maître Sora”, appelle-moi juste Sora! Pas de manières entre nous!  

 

Se tournant vers la cuisine, il lança à l’adresse de sa femme:  

 

-Yukari! Tu ne devineras jamais qui est là! Viens vite!  

-J’arrive! J’arrive!, répondit cette dernière d’un ton empressé.  

 

Quand elle entra à son tour dans la pièce et qu’elle m’aperçut, Yukari afficha le même air de surprise que son mari et elle vint vers moi. Jamais on aurait pu croire que sous ses vêtements elle était un homme. Elle était un peu plus petite que son mari, mais un peu plus grande que moi. J’estima qu’elle devait faire un mètre cinquante-cinq et je la trouvai resplendissante dans sa longue robe noire décorée de fleurs blanches. Je m’attendis à ce qu’elle me serre la main en guise de bienvenue, mais elle m’étreignit doucement dans ses bras et déposa un petit baiser sur ma joue... Ma joue mutilée... Tant de gentillesse firent monter quelques larmes discrètes à mes yeux et une chaleur réchauffa mon coeur. Aucune femme ne m’avait embrassé depuis mon accident...  

 

-Bienvenue dans notre demeure, Patrick, dit-elle de sa voix douce.  

-Nous ne t’attendions pas avant la semaine prochaine, dit Sora en s’asseyant sur le canapé, mais c’est vraiment une très belle coïncidence!  

-Une coïncidence?, dis-je en m’asseyant à mon tour. Je ne comprends pas...  

 

Alors que Yukari retournait à la cuisine pour s’occuper des courses, Sora m’expliqua que son équipe d’assistantes et lui-même avaient terminé la prochaine parution de “Notre Emblême” ce matin-là et qu’ils allaient faire une petite célébration ce soir-là, comme il était de coutûme pour eux.  

 

-Tu vas venir avec nous, Patrick, dit-il d’un ton autoritaire. J’insiste, je ne prendrai pas un non comme réponse!  

-Chéri, il a peut-être envie de se coucher un peu, dit Yukari depuis la cuisine. Après tout il a fait un vol de dix-huit heures en avion, il est sûrement fatigué...  

-Je suis fatigué, c’est vrai, dis-je en souriant, mais je ne veux pas rater une occasion comme celle-ci de participer à une célébration. J’accepte l’invitation avec plaisir!  

-Puis-je te demander pourquoi tu es arrivé plus tôt que prévu?, demanda Sora, le sourire toujours aux lèvres. À moins que je me suis trompé en prenant les dates en note...  

-Il est vrai que je devais arriver la semaine prochaine, mais j’ai décidé de partir une semaine plus tôt pour avoir le temps de m’ajuster au décallage horaire et me familiariser avec les coutûmes du pays.  

-Ahhh! Mais c’est très gentil et respectueux de ta part, répondit Sora.  

 

La porte s’ouvrit à nouveau et Masahiko dit d’une voix haletante:  

 

-Bon sang! Mais vous avez rapporté le magasin au complet, ma parole!  

-Je vais t’aider, répondit Sora en ricannant. Ou est Shion?  

-Elle est rentrée de son travail et elle est allée prendre une douche, répondit Masahiko en déposant les sacs d’emplettes par terre.  

 

Comme toute la famille se trouvait à la cuisine et même si ça me demanda un gros effort, je me relevai et j’allai les rejoindre. Masahiko me tira une chaise et il alla aider son oncle et sa tante qui rangeaient les courses. Yukari se tourna vers moi et me demanda:  

 

-Alors, ce vol, il s’est bien passé?  

-Oui, très bien, répondis-je. Le film était ennuyeux et je n’avais rien amené pour lire, alors j’ai dû faire la causette avec un Espagnol qui ne parlait pas un mot de français et savait à peine parler anglais. Des heures et des heures de plaisir!  

 

Tous ricannèrent et je poursuivis:  

 

-On m’a fait visiter le cockpit et le pilote m’a bien fait rire quand il m’a dit que j’avais une tête à jouer dans les plus grands films d’Hollywood.  

 

Masahiko me regarda d’un air perplexe. Avait-il deviné que j’étais sarcastique quand j’avais dit que le pilote m’avait fait rire? En fait, j’avais eu la très forte envie de me servir de sa tête comme punching bag, mais je m’étais retenu, histoire de ne pas passer pour un terroriste, avec la tête que j’ai... Sora et Yukari semblaient également avoir deviné que j’avais été sarcastique dans mes propos car ils ne dirent rien, mais échangèrent un regard plein de signification: pas de blagues sur l’apparence du stagiaire!  

 

-Masahiko t’a fait visiter le reste de la maison?, me demanda Sora.  

-Non, répondis-je. J’avais l’air épuisé selon lui, alors il a voulu que je reprenne mon souffle.  

-Tu avais sérieusement l’air de quelqu’un qui avait besoin de s’asseoir un peu, répondit Masahiko. Si t’es d’attaque, je vais te faire visiter.  

-Non, laisse-moi m’en occuper, dit Sora en me faisant signe de le suivre. J’en profiterai pour le présenter à mes assistantes.  

 

Je me levai à nouveau (il me sembla que je fis cet exercice un peu trop souvent à mon goût ce jour-là!) et je suivis Sora. Parvenus au pied de l’escalier, il désigna mon sac à dos et mon portfolio en me demandant:  

 

-C’est à toi?  

-Oui, mais attention, le sac est très lourd...  

-C’est pas un probleme pour moi! Répondit-il avec assurance.  

 

C’est aisément qu’il prit mon sac à dos dans une main et mon portfolio dans l’autre et qu’il me conduisit au second étage de la maison. Nous débouchâmes sur un long couloir qui s’étirait sur notre droite. À notre gauche, une petite porte donnait sur un petit salon qui faisait le lien entre les toilettes d’un côté et le studio de dessin de Sora. En prenant soin de marcher plus lentement à cause de ma démarche claudiquante Sora me dit que la première porte sur notre droite s’ouvrait sur sa chambre à coucher à lui et Yukari. Juste à côté, il y avait la chambre de Masahiko et devant celle-ci, il y avait la mienne. L’escalier qui se trouvait au bout du couloir donnait sur la chambre à coucher de Shion qui se trouvait au troisième.  

 

C’est à cette étage que se trouvait également la salle de documentation, mais on ne pouvait y avoir accès que par une échelle qui se trouvait dans le studio de dessin. Sora ouvrit la porte de ma chambre à coucher et il m’y fit entrer le premier. Il déposa mes bagages à côté de la porte et je pris le temps de regarder autout de moi: juste à côté de la porte, il y avait un grand placard à portes coulissantes et, contre le mur opposé, sous l’une des deux fenêtres, se trouvait un grand lit. À part une table de chevet et une commode, il n’y avait aucun autre meuble. Plusieurs boîtes de carton étaient éparpillées un peu partout sur le plancher en bois franc et les murs blancs étaient encore décorés de posters de vedettes rock.  

 

Sora alla ouvrir les fenêtres pour aérer la pièce et il me demanda, un sourire aux lèvres:  

 

-Ça te plaît?  

-Biensûr! Répondis-je rayonnant de bonheur. Ma propre chambre à coucher n’est pas aussi grande! Mon appartement au complet pourrait tenir dans cette chambre à coucher!  

-Alors sens-toi libre de l’arranger comme il te conviendra! Je vais demander à Shion et à Masahiko de te débarasser des affaires de Kaoru et demain, Yukari viendra faire un peu de ménage.  

-Je peux m’en charger moi-même, assurai-je un peu mal à l’aise de donner plus de travail à cette dernière.  

-Pas du tout! Tu es notre invité, il n’est pas question que tu lèves le petit doigt ici!, rétorqua Sora d’un ton sans réplique.  

 

J’allai m’asseoir sur le lit, question de tester son niveau de confort, et je lui demandai:  

 

-Ça fait deux fois que j’entends parler de Kaoru... Qui est-ce, si je puis me permettre?  

-Kaoru, dit Sora en s’asseyant à mes côtés, est le garçon de Mr. Tatsumi, un ami de la famille.  

 

J’eus un mouvement de recul. Il avait dit un ami de la famille?? Je me souvins alors que Ejima avait très clairement mentionné que ce Tatsumi était un Yakuza! Remarquant mon désarroi, Sora en devina la cause et il dit:  

 

-Mr. Tatsumi n’est pas un Yakuza comme les autres. Il peut se montrer très bien élevé et très gentleman quand la situation le demande. Nous avons su gagner le respect mutuel l’un de l’autre et ce, malgré Kaoru... Quand nous avons fait la connaissance de ce dernier, il était en train de traverser une une période de crise identitaire.  

-Il était confu au sujet de son identité sexuelle?  

-Non, il savait qu’il était un homme, mais il a le même “handicap” que moi, disons...  

 

Je ne mis pas longtemps pour comprendre: Kaoru était une fille désireuse d’être un garçon... Et c’était l’enfant de Tatsumi, qui avait eu le coup de foudre pour Masami, qui n’était en fait nul autre que Masahiko...  

Je ne pus réprimer un petit sourire et je dis:  

 

-Je ne peux m’empêcher de penser que beaucoup de monde pourrait en perdre leur latin en essayant de comprendre une telle situation!  

-Pour certaines personnes, cette situation est difficile à comprendre et encore plus difficile à accepter...  

-Mr. Tatsumi a eu du mal à accepter le choix de sa fille... ou plutôt de son garçon?  

-Non, ce n’est pas vraiment ça... C’est plutôt Kaoru qui avait du mal à vivre avec son père... Nous nous somme portés volontaires pour essayer de les racommoder. Ça n’a pas été facile, mais nous avons finalement réussi... Et la semaine dernière, Kaoru a décidé de tenter le coup et d’aller vivre chez son père. Jamais je n’avais vu Tatsumi aussi heureux.  

-Il vient souvent ici? Demandai-je.  

-De temps en temps, il vient rendre visite à la famille. Il est très reconnaissant pour ce que nous avons fait pour lui et son enfant. Tu le rencontreras très certainement au cours de ton séjour ici. Tu verras, il peut être effrayant au premier abord, mais il est très sympatique...  

-Un type comme moi, quoi!, dis-je dans un ricannement.  

-Je ne comprends pas la portée de tes propos...  

 

Je lui racontai brièvement les regards que j’avais croisés d’abord dans l’avion, puis à l’aéroport et ensuite avec Ejima et Lee. Je passai sous silence le fait que Shion m’avait également dévisagé pour ne pas lui attirer des ennuis et soulignai plutôt que Masahiko et elle avaient été les seuls qui ne m’avaient pas déshabillé du regard.  

 

-Eh bien tu n’as rien à craindre de mes assistantes, je leur ai parlé de toi. Je leur ai décrit ton apparence et ta condition physique. Elles ont l’habitude de se faire remarquer ou qu’elles aillent et de subir les moqueries des gens... Ce sont les dernières personnes au monde qui se moqueront de toi, je puis te l’assurer.  

-Merci de m’avoir montré autant de considération avant même que je n’arrive dans votre maison, dis-je en inclinant la tête en signe de reconnaissance, touché par tant de gentillesse.  

 

Sora me donna une solide tape dans le dos et il se leva en disant:  

 

-Viens, il est temps que je te présente à mes très estimées assistantes. Elles doivent être prêtes à aller célébrer la nouvelle parution!  





Chapter 4 :: Et Cupidon fit mouche!

Published: 17-09-09 - Last update: 17-09-09

Comments: Patrick rencontre finalement les charmantes assistantes de Maître Sora... et sera particulièrement envoûté par l'une d'entre elles... mais est-ce que ce sera réciproque?

En dépit des paroles réconfortantes prononcées par Sora, je ne pouvais m’empêcher de ressentir une certaine appréhension à l’idée de rencontrer ses assistantes, même si cette rencontre promettait d’être des plus intéressantes. Au fait, pourquoi étais-je si nerveux de les rencontrer? Comme Maître Sora me l’avait assuré, ces personnes-là avaient l’habitude d’être jugées à cause de ce qu’elles étaient, alors je n’avais aucune crainte à avoir! J’aurais toutefois aimé savoir de quoi elles avaient l’air, pour que je ne fusse pas surpris si jamais certaines d’entre elles avaient une apparence plutôt masculine!  

 

Sora me conduisit jusqu’au bout du couloir et il ouvrit la porte devant nous. Il m’expliqua que l’endroit ou nous nous trouvions servait de mini-vestibule; sur notre gauche, il y avait une porte qui s’ouvrait sur une petite salle de bain, alors que la porte sur notre droite donnait accès au studio de dessin. Comme le reste de la maison, la petite pièce ou nous nous trouvions était immaculée, peinte en blanc et le plancher était recouvert d’une moquette noire.  

 

Comme je faisais mine de retirer l’élastique qui tenait mes cheveux attachés, Maître Sora m’adressa un regard de reproche et me demanda, une main sur la poignée de la porte du studio:  

 

-Je peux savoir ce que tu fabriques?  

-Euh... J’allais...  

-Cacher ton visage?, dit-il d’un ton de reproche. Patrick, ici, tu n’as pas à avoir honte de ce que tu as l’air! C’est aux autres d’apprendre à voir au-delà de tes marques! Tout ce que tu as à faire, c’est de te montrer tel que tu es!  

 

Je sentis une chaleur envahir mon visage: à coup sûr je devais avoir le visage en feu. Je reserrai l’élastique de mes cheveux et me forçai à sourire. Sora posa une main sur mon épaule et me sourit d’un air encourageant. Là-dessus, il tourna la poignée et ouvrit la porte. Nous pénétrâmes à l’intérieur d’une grande pièce carrée brillament éclairée. Le bureau de Maître Sora se trouvait à notre gauche, devant une grande porte patio décorée de rideaux blancs. Au centre de la pièce, quatre pupitres avaient été collés les uns contre les autres, formant une vaste table carrée, et sur notre droite, une échelle menait à la salle de documentation. Derrière les pupitres des assistantes, il y avait une porte fermée; Maître Sora m’expliqua que c’était la chambre à coucher des assistantes ou elles devaient être en train de se préparer pour leur célébration. Derrière l’échelle qui menait à la salle de documentation, il y avait un placard dont la porte était ouverte et à l’intérieur duquel nous provenait des bruits étouffés, comme si quelqu’un était en train de fouiller dans des boîtes.  

 

-Hé, oh! Il y a quelqu’un?, lança Maître Sora vers le placard.  

-Il y a moi!  

 

Le personne qui venait de répondre avait une voix enjouée, mais vraiment étrange: on y sentait une certaine féminité, mais elle faussait (comme si Luciano Pavarotti avait essayé d’imiter Lara Fabian) et on pouvait facilement deviner que la personne à qui cette voix appartenait n’était pas très féminine...  

 

-Kazuko, tu peux venir ici une minute, j’aimerais te présenter quelqu’un!  

-J’arrive!, répondit joyeusement Kazuko en sortant du placard.  

 

J’eus malgré moi un sursaut lorsque je la vis: Kazuko devait faire dans les deux mètres, sans exagérer. La musculature de son corps et les traits carrés de son visage ne laissaient aucun doute sur son véritable sexe et que d’adopter une attitude féminine devait être un effort de tous les instants! Elle portait une courte perruque noire et était vêtue d’une robe rouge, courte et sans manches qui découvrait des épaules larges et des bras musclés. Elle affichait un large sourire et venait vers nous d’un pas lent.  

 

-Bon après-midi, patron!, dit-elle. Qui est donc ce jeune homme?  

-Kazu, laisse-moi te présenter Patrick Decker. C’est le stagiaire que j’ai accepté de prendre pour un an. Patrick, voici Kazuko Niigata, l’une de mes charmante assistantes.  

-Enchantée de te rencontrer, Patrick!, répondit Kazuko en me serra la main chaleureusement.  

 

Je pris le temps de regarder ses mains, justement. La mienne semblait disparaître complètement dans les siennes. On aurait été tentés de croire que de si grosses mains pouvaient assommer un taureau ou étouffer un boeuf, mais la poignée de main qu’elle me donna fut empreinte de douceur et de délicatesse, tout à l’opposé de son apparence.  

 

-Enchanté, répondis-je, un peu gêné. J’espère que nous allons bien nous entendre.  

-Tu verras qu’ici, tout le monde aime tout le monde, répondit Kazuko dans un ricannement.  

-Parlant de tout le monde, dit Sora, ou sont les autres?  

-Elles n’ont pas encore fini de se préparer, Patron.  

-Ahh ces femmes!, répondit Maître Sora dans un soupir. Il faut toujours qu’elles compétitionnent pour être la plus jolie!  

 

Je ne pus réprimer un ricannement quand j’entendis quelqu’un répliquer, derrière la porte:  

 

-Nous ne nous faisons pas la compétition, patron! Nous voulons seulement vous faire honneur!  

 

Cette voix était plus féminine que celle de Kazuko, mais on y devinait quelques accents de masculinité. Quelques secondes plus tard, deux femmes sortirent de la chambre des assistantes. La première que Sora me présenta était plus petite que Kazuko, mais tout de même masculine dans les traits de son visage et de par son physique. Elle portait un jean bleu très serré et un bustier noir qui ne laissait pas grand place à l’imagination quand à la taille de sa poitrine...  

 

-Patrick, voici Susumu Yokota. Susumu, je te présente Patrick Decker, notre stagiaire.  

-Enchantée, Patrick, répondit cette dernière avec un grand sourire, m’embrassant sur la joue droite.  

-Très heureux de te rencontrer également... Pardonne-moi ma curiosité, mais tu ne serais pas celle qui est parti aux États-Unis pendant un an pour y subir une opération de changement de sexe?  

-Exactement!, répondit Susumu avec enthousiasme. Je suis une femme de la tête aux pieds!  

-Je te félicite d’avoir eu le courage d’entreprendre un tel changement!, dis-je.  

-Merci, répondit-elle d’un air timide, mais c’est quelque chose que je devais faire, sinon je devenais complètement marteau!  

 

Maître Sora désigna la seconde femme. Cette dernière était tout à l’opposé de Kazuko et de Susumu: alors que ces dernières étaient encore masculines en dépit des efforts qu’elles avaient faits pour parraître plus féminines, celle-ci était, à l’exemple de Yukari, la féminité incarnée. Ses cheveux bruns-clairs tombaient derrière son dos et sur ses épaules et ses petits yeux noirs étaient pétillants d’espièglerie. Elle portait une longue robe bleu-ciel à manches longues et ses petites lèvres rouges s’étirèrent en un sourire accueillant lorsqu’elle me fit la collade et qu’elle m’embrassa à son tour.  

 

-Je m’appelle Hiromi Nakamura et je suis très heureuse de faire ta connaissance, dit-elle d’une voix douce, à peine masculine.  

-Très heureux également, répondis-je, un grand sourire aux lèvres.  

-Tu verras qu’on s’amuse ferme ici, même quand on travail aussi durement qu’on vient de le faire!, dit-elle d’un air moqueur.  

-Hiromi, ou est Makoto?, demanda Maître Sora. On part dans une demie-heure.  

-Elle arrive dans un instant, elle finit de faire sa manucure, répondit Susumu en s’asseyant à son pupitre.  

 

J’en profitai pour regarder autour de moi afin de détailler un peu plus les murs qui m’entouraient. Décidement, Maître Sora aimait la couleur blanche! Les murs étaient immaculés et décorés des couvertures grand format des publications de “Notre Emblême”. Pendant que Kazuko étaient retournée fouiller dans le placard, Sora et Susumu discutaient de l’endroit ou nous allions nous rendre pour leur petite célébration et Hiromi regardait dehors d’un air rêveur. La porte de la chambre des assistantes s’ouvrir et j’entendis à cet instant la plus harmonieuse et la plus belle des voix qu’il me fut donné d’entendre:  

 

-Me voilà! Me voilà! Je suis prête à faire la fête!  

 

Je me retournai et en l’apercevant, ce fut comme si je venais de recevoir un coup de poing sur la gueule. Devant moi se tenait la plus belle femme qu’il m’eut été de voir de toute ma vie. Toute petite, toute menue, elle portait un débardeur blanc sous un chemisier rose sans manches noué au-dessus de sa taille, d’un jean noir moulant et chaussée d’escarpins rouges. Ses cheveux noirs étaient mi-longs et elle portait une paire de lunettes à montures rouges carrées. Tout autour de moi cessa soudain d’exister: je ne voyais plus qu’elle. Il me sembla que si je la quittais des yeux, ne serait-ce qu’un seul instant, elle disparaîtrait pour toujours et je me refusai de vivre pareil drame. Incapable de dire quoi que ce soit, la bouche entrouverte, je ne pus détacher mes yeux de cette jeune femme à cause de qui mon coeur s’était mis à courir un sprint.  

 

Elle s’était figée devant moi, elle-même incapable de bouger ou de dire quoi que ce soit, alors qu’elle était en train d’attacher l’une de ses boucles d’oreilles. Évidemment mon apparence la troublait. Ça ne pouvait être que ça et rien d’autre. Jamais un tel ange de beauté, de délicatesse et de raffinement ne pouvait ressentir à mon égard autre chose que dégoût répulsion!  

 

-Patrick, voici Makoto Yamazaki, entendis-je à peine dire Maître Sora, d’une voix lointaine.  

 

Pas de réaction, ni de moi, ni de la belle jeune fille. Maître Sora apparut devant moi, l’air amusé, et il fit claquer des doigts devant mes yeux en disant:  

 

-Ça va? Quelque chose cloche?  

-Hein?, répondis-je d’une voix trainante qui était à des kilomètres de la mienne.  

 

Comment avoir l’air d’un parfait imbécile? Faites comme moi: ayez l’air hébèté et répondez aux questions qu’on vous pose d’un air stupide... Vous verrez, ça marche, si vous voulez avoir l’air d’un crétin anobli!  

Maître Sora sourit et dit d’un air plus insistant:  

 

-Je t’ai demandé si ça allait?  

-Ezzzee... Oui?, répondis-je.  

 

Quelque part, j’entendis quelqu’un pouffer de rire, ce qui me secoua un peu et me sortit de ma torpeur. Je vis Susumu et Hiromi qui cachaient leur fou-rire derrière leurs mains, s’efforçant de regarder ailleurs. Kazuko avait cessé de fouiller dans le placard et me regardait d’un air interrogateur.  

Je regardai à nouveau Maître Sora et je bredouillai:  

 

-Ç-ça v-va, juste un peu de f-fatigue, j-je pense...  

 

Je reportai mon regard vers la jeune fille qui n’avait cessé de me regarder et Maître Sora dit, en se râclant la gorge:  

 

-Patrick, voici Makoto Yamazaki. Makoto, je te présente Patrick Decker, le stagiaire qui passera une année en notre compagnie...  

 

Makoto... Il me sembla alors qu’aucun autre nom ne pouvait être aussi beau que celui-ci; qu’il fusse masculin ou féminin ne m’était d’aucune importance. Je retrouvai par miracle l’usage de mes jambes et prenant solidement appui sur ma canne, je m’avançai lentement vers Makoto qui ne me quittait pas des yeux. Décidément, ces yeux noirs étaient plus envoûtants que le pendule d’un hypnotiseur. D’un geste hésitant, elle me tendit la main et je la pris dans la mienne. Plutôt que de me contenter de la serrer, je me penchai en avant et je lui fis un baise-main. Quelle douceur! Était-ce vraiment la main d’un homme que je venais d’embrasser?? Quelle importance?  

 

-J-je s-suis enchanté d-de te rencontrer..., dis-je en bégayant lamentablement.  

-Je suis très heureuse de te rencontrer aussi, Patrick, répondit-elle de sa voix douce. J’espère que tu te plairas en notre compagnie...  

 

Les battements de mon coeur redoublèrent d’intensité lorsqu’elle m’adressa le plus beau et le plus angélique des sourires et je sentis mes genoux défaillir. Prononcé par elle, mon nom n’était plus aussi commun tout d’un coup. Je m’appuyai plus solidement sur ma canne et Maître Sora tira une chaise afin que je m’y assoie. Makoto et moi continuions de nous contempler mutuellement, tandis que les autres nous regardaient d’un air amusé. Je tenais toujours dans la mienne la main de Makoto (il me semblait impensable que je la lui rendre!) et elle continuait de me sourire. Maître Sora me donna une solide tape dans le dos et il dit:  

 

-Bon je vois que le courant passe avec mes assistantes, et spécialement avec celle dont tu tiens toujours la main, mais nous, on a quelque chose à célébrer et le reste de la famille va se joindre à nous... À moins que tu ne veuilles rester ici, auquel cas tu devras rendre à Makoto sa main, ma femme est prête à te tenir compagnie...  

 

Comme si elle était devenue subitement brûlante, je lâchai la main de Makoto et dit:  

 

-Non, je veux vous accompagner, mais d’abord, si vous me le permettez, j’aimerais bien aller à la salle de bain...  

-Tu connais le chemin, dit Maître Sora avec un sourire entendu.  

-Merci, dis-je en me relevant avec peine.  

 

Mes satanés genoux étaient flageollants et mes bras étaient aussi lourds que du plomb. Je m’efforçai cependant de marcher d’un pas normal vers la porte et Makoto se précipita au-devant de moi pour m’ouvrir. Nous échangeâmes à nouveau un regard bref, accompagné d’un sourire (craquant venant d’elle, sans doûte crispé venant de moi) et je sortis. Je me précipitai à l’intérieur de la salle de bain et m’y enfermai.  

Je laissai tomber ma canne sur le linoléum et je m’accrochai aux rebords de l’évier. Levant la tête, je vis mon reflet dans le miroir: j’avais une mine horrible. Mon teint avait blêmi et faisait resortir avec plus d’intensité le rose de ma cicatrice. Deux demi-lunes violacées se dessinaient sous mes yeux injectés de sang et de la sueur perlait sur mon front. Mes lèvres, comme mes mains, tremblaient.  

J’ouvris le robinet d’eau froide et m’en aspergeai le visage à plusieurs reprises. Le contact de l’eau me calma un peu. Pourquoi me sentais-je comme ça?? Pourquoi tombais-je en déconfiture de la sorte?  

Une petit voix narquoise me répondit, des tréfonds de ma tête:  

 

-Parce que t’es amoureux, espèce d’imbécile!!!!  

 

Je pris une profonde inspiration et j’appuyai mon front sur le rebord de l’évier. Amoureux? Moi? Jamais je n’avais ressenti quelque chose de semblable auparavant. Certes j’avais vécu quelques amourettes à l’école, mais jamais je n’avais éprouvés de tels sentiments pour quelqu’un! C’est dans des moments pareils qu’on aimerait bien avoir un guide décrivant les étapes pour se calmer les nerfs afin d’éviter d’avoir l’air d’un parfait crétin! J’aspergeai à nouveau mon visage d’eau glacée et je fermai le robinet. J’épongeai mon visage à l’aide d’une serviette et au moment ou je me penchai pour reprendre ma canne, quelqu’un ouvrit la porte sans même prendre la peine de cogner auparavant. Je la reçus en pleine tête et la force du coup m’envoya promener sur le dos.  

 

-Aïe!! Bordel de merde!, maugréai-je en m’asseyant, massant mon front endolori.  

 

Voilà tout ce dont j’avais besoin: une belle bosse! J’entendis quelqu’un éclater de rire et, levant les yeux, je vis Shion qui, pliée en deux, me regardait, incapable de se contrôler. J’entrepris de me relever avec beaucoup de difficulté et, riant toujours, elle me tendit une main pour m’aider à me remettre debout.  

 

-Je... Je m’excuse!, parvint-elle à dire entre deux fou rire, s’appuyant contre le bord de la porte, c’est mon père qui m’envoie: on est prêt à partir et on n’attend plus que toi!  

-J’arrive, maugréai-je, frottant toujours mon front, les dents serrées.  

 

Je sentais déjà la bosse qui poussait et Shion, vêtue d’un tee-shirt blanc et d’une jupe mauve, tentait de réprimer son fou rire, sans grand résultats... Elle m’accompagna au premier étage, étouffant ses rires dans sa main, et je vis que Yukari nous attendait devant la porte. En nous voyant, Shion qui riait et moi qui massait mon front, elle dit d’un air vaguement amusée:  

 

-Qu’est-ce qui s’est passé?  

 

Shion eut un geste de la main qui signifiait “ne me pose pas de question” et elle se précipita dehors en laissant libre cours à ses ricannements. Se tournant vers moi, Yukari répéta, d’un air intriguée:  

 

-Qu’est-ce qui s’est passé?  

-Shion a cru bon de me faire pousser une prune sur front...  

 

D’un geste délicat, Yukari écarta ma main et ses yeux s’écarquillèrent de stupeur en voyant la bosse.  

 

-Comment c’est arrivé?, demanda-t-elle, légèrement amusée.  

-Seulement petit un accident. J’avais laissé tomber ma canne, je me suis penché pour la ramasser et Shion a ouvert la porte. J’étais juste trop près, voilà tout. On y va?  

-Tu es certain que tu ne veux pas rester ici pour te reposer?, me demanda-t-elle. Tu as un mine de déterré...  

-Non, ça va! Allons-y! Faut pas faire attendre les autres!, dis-je en m’efforçant d’être enthousiaste.  

 

Comme pour faire écho à ce que je venais de dire, j’entendis un coup de klaxon. Nous sortîmes à l’extérieur et je vis Sora qui nous attendait à côté d’une fourgonnette-taxi, ainsi que ses assistantes. Bien que plus spacieuse, une fourgonnette comportait huits places... et nous étions neuf, plus le chauffeur qui pianottait d’impatience sur son volant... Donc sept places disponibles pour neuf personnes... Comment résoudre ce dilemme?  

 

-J’ai appelé ce taxi parce qu’il était impensable que nous prenions ma voiture, dit Sora en souriant.  

-Mais va-t-on avoir assez de place, chéri?, demanda Yukari alors que nous nous approchions de la fourgonnette.  

-Biensûr, on n’aura qu’à se tasser un peu, répliqua son mari sans se départir de son sourire.  

 

Shion et Masahiko s’étaient déjà assis sur la dernière banquette, aux côtés de Susumu. En me voyant, Shion se remit à ricanner et Masahiko lui donna un coup de coude en lui disant de se taire.  

Tandis que Sora allait s’asseoir à l’avant, Kazuko monta à son tour, suivie de Hiromi qui s’installa sur ses genoux, passant un bras autour de son cou. Makoto était adossée contre la fourgonnette et regardait le bout de ses escarpins. De temps à autres, pendant tout ce temps-là, elle m’avait décoché un ou deux regards brefs, discrets et timides, avant de retourner à la contemplation de ses escarpins.  

 

-Montez, Yukari-san, dit-elle avec politesse. Je prendrai la dernière place...  

 

La dernière place, avait-elle dit??? Je me rendis compte qu’elle parlait de moi et soudain, une vague de sueur froide descendit le long de mon échine. Pendant que Yukari prenait place sur la banquette, Makoto, sans oser me regarder dans les yeux, me demanda:  

 

-J’espère que tu ne vois aucun inconvénient à ce que je m’assieds sur tes genoux, à moins que ça ne te cause un problême?  

-N... Non aucun problême, dis-je en tendant la canne à Yukari qui la posa sur ses genoux.  

 

Je m’accrochai aux rebords de la portière et je me donnai un élan afin d’y grimper. Je poussai un soupir en m’asseyant et Makoto vint s’asseoir sur mes cuisses. Elle referma la portière et passa un bras autour de mon cou, les pommettes roses. Hésitant, j’enlaçai sa taille, un sourire gêné sur les lèvres, et Sora dit:  

 

-Chauffeur, veuillez nous conduire au Bistro “Le Piano-Bar”, s’il-vous-plaît.  

 

Au moment ou la fourgonnette se mit en route, je sentis mon coeur me refaire le coup du marathon dans ma poitrine. Makoto était lègère comme une plume et elle se blottissait contre moi sans retenue, sans toutefois oser me regarder. Elle s’était ramassée sur elle-même, sa tête appuyée contre mon épaule et je sentais son souffle court dans le creux de mon cou. La chaleur de son corps, mêlée au doux parfum de rose qu’elle dégageait, avaient quelque chose de réconfortants. Je fermai les yeux pour essayer de me calmer... Peine perdue. Mon coeur battait la chamade et semblait être décidé à défoncer ma poitrine. Derrière moi, j’entendais Shion pouffer de rire et Masahiko me dit, ricanneur:  

 

-Alors, tu as fais connaissance avec la porte de la salle de bain du deuxième?  

-Ouais, m’efforçai-je de dire sur un ton badin. Je crois qu’elle et moi deviendrons intimes sous peu... On a eu un contact assez significatif, si je puis te dire... Grâce à Shion qui a très bien su jouer les entre-metteuses!  

 

Makoto regarda la bosse sur mon front et elle dit, amusée:  

 

-Tu voulais garder un souvenir fracassant de ta premier journée avec nous, Patrick?  

-C’est en quelque sorte ce qui s’est passé, dis-je avec un sourire forcé.  

 

À nouveau, elle m’adressa un sourire craquant et le sentiment que j’en ressentis fut comparable à une chute vertigineuse et sans fin. Ironiquement, ça avait quelque chose d’euphorisant, moi qui avait toujours eu peur des hauteurs! Assise sur mes genoux, elle était devenue le centre de toute mon attention, le centre de mon Univers. Le monde cessa d’exister; ce néant nous enveloppant dans un cocon de solitude silencieuse, ou ceux qui nous accompagnaient à ce moment-là n’étaient que de vagues ombres inconnues.  

 

Combien de temps dura le trajet? Je l’ignore.  

Quelqu’un nous parla-t-il? Probablement...  

Une chose seulement était certaine: nos regards ne se quittèrent pas une seule minute tout le temps que nous passâmes dans la fourgonnette... Je le sentais...  

 

J’étais amoureux.... 





Chapter 5 :: "Concert Involontaire"

Published: 22-09-09 - Last update: 14-11-09

Comments: J'ai pris mon temps pour écrire celui-ci parce que je voulais être certain d'écrire un chapitre à la hauteur du titre... Ici, Patrick montre qu'il n'y a pas que dans le dessin qu'il a du talent...


Pourquoi fallait-il que j’accepte l’invitation de Maître Sora, alors que j’aurais pu me défiler facilement, rester à la résidence Wakanaé en compagnie de Yukari-san et me reposer? En faisant un rapide calcul, je devais être réveillé depuis au moins trente-six heures et mon corps me criait qu’il avait besoin de repos... Grand bêta que je suis... Ne voulant pas offenser Maître Sora, j’ai tout bonnement accepté, peu importe si je risquais de sombrer dans le sommeil à n’importe quel moment de la soirée.  

 

L’endroit ou nous nous rendions était un bistro appelé “Le Piano-Bar”. Je m’étais attendu à ce que ce soit un endroit tranquille, ou les seuls sons ambiants consisteraient en des bruits de coutellerie sur des assiettes et un petit bruit de conversation étouffé, avec un petit fond musical bien peinard... Rien de bien stimulant, en quelque sorte. Dieu avais-je eu tort de croire ça! Pour commencer, le bistro était bondé; trouver une place pour le groupe de neuf personnes que nous étions n’allait pas être une mince affaire. La salle à manger était une pièce rectangulaire faiblement éclairée ou les clients étaient assis autour de tables carrées décorées de nappes rouges. De petites lampes noires occupaient le centre des tables, éclairant faiblement les visages des convives. Le seul véritable éclairage venait de la scène, à l’autre bout de la pièce. Les clients étaient bruyants et leurs conversations étaient à peine couvert par la musique d’un orchestre composé d’un batteur, de deux guitaristes (dont l’un chantait), ainsi que d’un contre-bassiste. Personne n’était assis au piano à queue noir qui occupait l’extrême gauche de la scène. Tous les musiciens portaient un habit trois-pieces noir à noeud papillon et étaient soigneusement coiffés. Entre la scène et la première rangée de tables, il y avait une modeste pièce de danse ou deux trois couples dansaient lentement au rythme de la chanson “Smoke Gets in your eyes”. J’eus un rapide coup d’oeil autour de moi et, voyant la propreté avec laquelle les clients étaient vêtus, je pensai que je devais sérieusement détonner avec mes jeans et mon tee-shirt... Au moins, dans cette semi-obscurité, la majorité de mes défauts seraient moins visibles...  

 

Une hôtesse nous conduisit à deux tables tout juste devant la scène et nous nous y installâmes. Sora, Yuraki, Shion et Masahiko étaient assis à la première table et moi, j’étais assis avec les assistantes, entre Susumu à ma gauche et Makoto à ma droite. Nous avions rapproché les tables l’une contre l’autre afin de mieux se comprendre pendant la conversation. Quelques minutes plus tard, un serveur vint à notre table. En voyant Susumu et Kazuko, il eut une drôle d’expression de malaise, mais il se contenta de dire:  

 

-Puis-je prendre votre commande?  

-Oui, répondit Sora. Champagne pour tout le monde.  

-Un verre de jus d’orange pour notre fille qui n’a pas encore l’âge de boire de l’alcool, s’empressa de dire Yukari en voyant Shion qui frottait d’un air avide ses mains l’une contre l’autre.  

-Maman!, protesta cette dernière, un air déconfit sur le visage. J’ai dix-sept ans, je ne suis plus une enfant!  

-Shion, tu n’es pas majeure!, rétorqua Yukari d’un ton sans réplique. Détail que ton père semble oublier quelques fois!, ajouta-t-elle en décochant un regard chargé de reproches vers Sora qui eut un sourire gêné.  

 

Shion croisa ses bras sur sa poitrine d’un air boudeur et Masahiko dit, histoire de changer de sujet:  

 

-Patrick me parlait plus tôt aujourd’hui qu’il était un bon musicien.  

-C’est vrai, tu joues de la musique?, demanda Hiromi en posant son menton sur ses mains jointes, accoudée sur la table. J’aimerais entendre ça!  

 

Elle étais assise juste devant moi et je pouvais voir l’enthousiasme luire dans ses yeux espiègles. Je levai une main et, l’air gêné, je dis:  

 

-N’exagérons rien! Je suis loin d’être un musicien professionnel comme ceux-là!, dis-je en désignant l’orchestre qui entonnait un autre vieil air, “Stand By Me”.  

-Ce ne sont pas des pros, dit Susumu. Ici, il n’y a que des amateurs qui montent sur cette scène pour faire leurs numéros.  

-Pourtant, à les voir, j’aurais cru qu’ils étaient...  

-Non, dit Masahiko. Certains clients aiment frimer, alors ils font tout pour se faire remarquer... Certains espèrent même qu’un agent se trouve dans la salle et les remarque, mais ici, ce n’est qu’un petit bistro plutôt ordinaire. Personne d’important ne vient ici.  

-Tu oublies le patron, répliqua Susumu sur un ton de reproche.  

-Personne d’important du domaine de la musique, précisa Masahiko avec un petit sourire entendu.  

-Un jour, un groupe de filles sont venues ici pour chanter du Hard Rock... habillées seulement de leurs sous-vêtements..., dit Kazuko avec un certain dédain dans la voix. Elles se sont fait éjecter avant même d’avoir fini le deuxième couplet...  

 

Je souris en essayant d’imaginer la scène... Pathétique... Certaines personnes ne reculaient vraiment devant rien pour se faire remarquer! Pendant que Kazuko et Susumu discutaient de la pluie et du beau temps, j’observai du coin de l’oeil Makoto qui, assise à côté de moi, n’avait pas ouvert la bouche depuis qu’on était descendus de la fourgonnette. Elle continuait de me regarder à la dérobée, un timide sourire aux lèvres. Mon coeur, qui s’était quelque peu calmé, dérapait toujours quand nos yeux se croisaient.  

En nous asseyant à notre arrivée, j’avais cru remarquer qu’elle avait approché sa chaise de quelques centimètres vers la mienne. Elle m’avait adressé un petit sourire, mais n’avait rien dit. Ses mains étaient croisées sur la tables et quelques fois, j’ai cru voir qu’elle esquissait un mouvement vers moi, comme pour les rapprocher des miennes.  

 

Le serveur revint avec nos coupes de champagne et les déposa devant nous sur les tables. Lorsqu’il prit congé, Maître Sora se leva, sa coupe dans sa main, et il dit:  

 

-Je voudrais porter un toast à la réussite et à la parution du prochain numéro de notre manga “Notre Emblême”. Puisse cette nouvelle parution être, comme les autres, couronnée de succès!  

 

Nous levâmmes nos coupes, puis Maître Sora ajouta:  

 

-J’aimerais par la même occasion, souhaiter officiellement la bienvenue à notre ami, Patrick Decker, qui va passer la prochaine année en notre compagnie! Que ton séjour avec nous t’apporte joie, bonheur, connaissance et accomplissement de soi!  

 

Tous levèrent leurs coupes et, l’air gêné, je me levai à mon tour et dis:  

 

-Je ne suis pas doué pour les discours, alors je vais tâcher de faire ça court Je voudrais commencer par remercier Maître Sora--  

-Appelle-moi Sora, je te l’ai dit: pas de manières entre nous!, répliqua ce dernier en riant.  

-Bon, très bien..., dis-je, pris au dépourvu. J’aimerais remercier Sora, ainsi que sa famille, pour avoir accepté de m’accueillir dans son quotidien. Je souhaite ardemment faire honneur aux enseignements qui me seront prodigués en votre compagnie et si je pouvais les évaluer en richesses, je sais que je serais l’homme le plus riche du monde.  

Ce petit discours me vallut quelques étreintes de mes nouveaux amis.  

 

Masahiko me demanda, alors que je me rasseyais:  

 

-J’aimerais bien t’entendre jouer, Patrick, tu veux?  

-Je ne sais pas trop.... Dis-je en posant ma coupe sur la table  

-Ah oui, ce serait super! Répondit Hiromi en tapant dans ses mains d’un air enthousiaste.  

-Je ne saurais pas quoi jouer, protestai-je, gêné.  

-Peu importe, tant que c’est quelque chose qui bouge!, intervint Shion en me regardant avec insistance.  

-Ne le forcez pas, s’il ne veut pas, déclara Sora d’un ton ferme. Il a fait un long voyage et il n’est pas venu ici pour jouer au Jukebox.  

-Et en plus, jouer en solo, c’est pas ce que j’appelle du divertissement qui bouge, poursuivis-je, reconnaissant de l’intervention de Sora.  

 

L’orchestre amateur venait de quitter la scène, sous quelques applaudissement mitigés et les membres avaient regagné leur table, l’air déçus du peu de succès de leur représentation. Masahiko regarda dans leur direction et il dit:  

 

-Si je parvienss à convaincre ces gars-là de remonter pour jouer avec toi, tu accepterais?  

-Masahiko, j’ai dit non, répliqua son oncle d’un ton ferme.  

 

Masahiko eut une moue de déception et je regardai le piano à queue. Je reportai mon regard sur chacun des membres de notre groupe et je vis que cinq d’entre eux (Shion, Kazuko, Susumu, Hiromi et Masahiko) me regardaient avec insistance pour que j’accepte. Sora et Yukari, eux, avaient l’air de me dire que je ne devais pas me sentir obligé de monter sur la scène. Makoto, elle, me regardait avec un mélange d’insistante timide et d’enthousiasme. J’eus un soupir et je dis, vaincu:  

 

-Bon, d’accord. La majorité l’emporte. Je ferai une ou deux chansons, mais pas davantage.  

-Super! S’exclama Masahiko en selevant de sa chaise, comme s’il avait posé ses fesses sur une punaise.  

-Je viens avec toi!, déclara Shion en se levant pour le suivre.  

 

Les suivant du regard, je les vis accourir vers la table des musiciens et ils s’assirent avec eux pour leur parler.  

 

-Patrick, tu n’as pas à faire de spectacle ce soir, tu as une mine terrible, dit Yukari d’une voix douce.  

-Bah! Ce n’est pas quelque chose de si terrible et ça va leur faire plaisir, dis-je en lui souriant.  

-Qu’est-ce que tu vas nous jouer, alors?, demanda Hiromi avec avidité.  

 

Oups! Je n’avais pas pensé à celle-là... Qu’est-ce que les gens aimaient comme musique dans cet endroit? Les clients avaient l’air guindés et je ne connaissais que deux ou trois airs de musique classique. Comme je ne voulais que jouer deux ou trois chansons, j’allais simplement devoir fouiller dans le répertoire de celles que je connaissais.... Je ressentis une certaine déception en voyant les musiciens quitter leur table (j’aurais préféré rester bien peinard à ma table avec mes amis) pour se diriger vers la scène alors que Masahiko et Shion revenaient vers nous, un grand sourire aux lèvres. En s’asseyant Masahiko dit:  

 

-Je leur ai dit que tu ne parlais pas japonais, mais ils comprennent très bien l’anglais, alors tu n’aurais qu’à t’exprimer dans cette langue avec eux. Vas-y, ils t’attendent!  

 

L’embouteillage de papillons dans mes tripes revint subitement. Encore une fois, j’avais accepté sur un coup de tête de faire quelque chose dont je n’avais pas vraiment envie de faire... Sur le coup, j’ai eu peur d’avoir l’air faux en chantant... De quoi j’aurais l’air si je dégueulais sur le clavier du piano??  

Je me levai, les fourmis plein les jambes, puis je me dirigeai lentement vers la scène. En me voyant à la lumière de la scène, les musiciens eurent la même réaction face à mon apparence, mais je ne fis pas attention à leur attitude et je m’approchai du chanteur. Utilisant mon meilleur anglais, je me présentai:  

 

-Je m’appelle Patrick Decker, enchanté de vous connaître.  

-Je m’appelle Ijiro Samotomo, répondit le chanteur. Voici Fong-Tse Tong, le batteur, Fujisaki Jinfujin, est guitariste, comme moi et Mao-Che Fuyamasa, est notre contre-bassiste.  

 

Nous nous serrâmes la main à tour de rôle et je dis:  

 

-Je vais m’occuper de jouer du piano et de chanter, si vous me le permettez. Quel genre de musique les gens d’ici aiment écouter?  

-Ah de tout, répondit Ijiro. Nous on joue de tout également. On aime particulièrement les vieux styles, mais on joue de la musique récente aussi. Dis-nous un titre et si on connais, on va te suivre.  

 

Je pris un moment pour réfléchir, regardant la foule devant nous. La plupart des clients nous ignoraient souverainement, trop occupés par leurs assiettes et les rares personnes qui daignaient nous regarder le faisaient avec moquerie et septicisme. Seuls mes nouveaux amis nous regardaient d’un air enthousiastes et je remarquai que Makoto ne m’avait pas quitté des yeux et qu’elle avait gardé sont petit sourire timide qu’elle m’adressait toujours.  

 

-Vous connaissez “Mess Around”, de Ray Charles?, demandai-je en me tournant vers les musiciens.  

-Oui, oui, oui! Répondit Ijiro avec un large sourire. On connait, mais j’ai du mal avec les paroles.  

-Laisse-moi le rôle du chanteur, dis-je. Il y en a un de vous qui sait jouer de la trompette ou du saxophone?  

-Oui, moi, répondit Fujisaki en levant la main.  

-Okay, alors à vos places, dis-je en allant m’asseoir derrière la piano à queue.  

 

J’ajustai le micro à la hauteur de ma bouche et, me tournant vers les clients, je dis, en anglais:  

 

-Bonsoir à tous! J’espère que vous vous amusez avec nous. Je m’appelle Patrick Decker et je viens tout juste de débarquer du Canada, un endroit ou tous sont bienvenus!  

 

Quelques applaudissements peu convainquants accueillirent mes propos. Je ne m’attendais pas à grand chose non plus.  

 

-C’est un grand plaisir pour moi de me retrouver ici devant vous ce soir et j’espère que les quelques chansons que je vais jouer pour vous, en compagnie de mes nouveaux amis, vont vous plaire. J’aimerais saluer les gens qui ont si généreusement accepté de m’accueillir dans leur maison comme un membre de leur famille. Ils sont assis juste ici, devant moi: La famille Wakanaé et leurs amis!  

 

Shion et Masahiko, imités par Susumu et Hiromi, tapèrent dans leurs mains et sifflèrent pour se faire voir. Yukari et Sora eurent un petit sourire gêné et me saluèrent de la main, alors que Kazuko, plutôt inexpressive, leva sa coupe vers moi. Seule Makoto tentait de ne pas se faire remarquer, alors qu’elle me regardait sans s’être départie de son sourire.  

Je fis craquer mes doigts et les fis courir sur le clavier afin de vérifier que le piano était bien accordé. Aucune anicroche. J’entamai alors le début de la chanson, puis je fus suivi des autres musiciens et dès que j’entamai le premier couplet, toute la nervosité que je ressentais me quitta d’un coup. Ce fut comme si je me retrouvais dans mon élément, à l’aise derrière le clavier du piano.  

 

Je tentai de garder mes yeux fixés sur mes doigts afin de ne pas voir la réaction des clients, mais durant le solo de piano, suivi du solo de saxophone, je ne pus résister à la temptation de regarder. Surprise: une dizaine de couples dansaient au rythme de la musique.  

 

À la fin de la chanson, Ijiro s’approcha de moi et il dit:  

 

-Tu joues comme un pro! Il faut absolument en faire une autre!  

 

Je regardai les couples qui étaient restés sur la piste, attendant que nous reprenions le “spectacle”. Plusieurs personnes avaient applaudi de bon coeur, mais la question que je me posai à ce moment-là fut si c’était parce qu’ils avaient vraiment aimé ou si c’était juste pour nous encourager à déguerpir au plus vite...  

 

-Great Balls of Fire? Demandai-je  

-En avant la musique! S’exclama Ijiro en reprenant sa place.  

 

À la minute ou je commençai à chanter, plusieurs personnes se précipitèrent sur la piste de dance, Masahiko et Shion compris. Tout en chantant, je pouvais voir qu’ils dansaient magnifiquement ensemble... Comme s’ils étaient faits pour être un couple...  

 

Bien vite, les deux ou trois chansons que je voulais chanter devinrent quatre ou cinq, puis six...  

À la fin de la douzième chanson (Pledging My Love, de Johnny Ace), je fis mine de me lever, mais la foule scandait, en anglais:  

 

-Une autre! Une autre!  

 

Je regardai ma montre. J’avais commencé à jouer vers les sept heures du soir, il était maintenant neuf heures. Ijiro revint vers moi et il me dit:  

 

-Patrick, la soirée est encore jeune, tu ne peux pas t’en aller tout de suite!  

-Mais je commence à être fatigué, vieux..., protestai-je, la voix lègèrement éraillée.  

-On n’a jamais vu ça ici auparavant!, répliqua-t-il. Regarde, la piste est presque pleine!  

 

En effet, il ne restait que très peu de personnes aux tables. Même Sora et Yukari étaient venus faire leur tour de piste, accompagnés des assistantes. Makoto était restée assise à sa place, sans cesserde me regarder, comme si elle avait voulu que je l’invite à faire un tour de piste et à toutes les fois ou nos regards s’étaient croisés, elle avait instantanément rougi.  

 

-Il faut continuer, me lança Mao-Che Fuyamasa avec insistance. Si on arrête maintenant, ils vont nous réduire en poussière!  

 

Me rasseyant, je me creusai les méninges, me demandant ce que je pouvais bien leur jouer.... Jusqu’alors, nous n’avions joué que des airs rétros... si nous en changions, nos danseurs risquaient de retourner s’asseoir...  

 

-Mesdames et messieurs, dis-je aux clients. Nous vous remercions de vos applaudissements et de votre enthousiasme. Veuillez nous accorder quelques instants afin que nous trouvions dans notre répertoire une autre chanson qui vous fera délirer... En attendant, un serveur aurait-il l’amabilité de m’apporter un grand verre d’eau, j’ai la gorge sèche!  

 

Quelques rires accompagnèrent mon annonce. Je décidai de jouer une pièce instrumentale de Floyd Kramer: “Last Date”. Quelque chose de doux, de calme, pour permettre à tous de respirer un peu. Les notes jouées normalement au violon furent jouées à la guitare par Ijiro et Fong-Tse Tong. Une pièce sentimentale qui permirent aux couples de danser au rythme de leur amour. Je jetai un rapide coup d’oeil en direction de Makoto et je lui souris. Comme j’aurais voulu danser avec elle sur cet air! Quelque chose dans ses yeux me laissait sous-entendre qu’elle partageait peut-être cet envie...  

 

Le rythme reprit de plus belle lorsque nous entonnâmes “Long Tall Sally”, puis “Good Golly, Miss Molly”. Je faillis cependant me tromper royalement quand nous jouâmes “Splish Splash”, mais Ijiro vint à ma rescousse. Une autre heure passa jusqu’à ce que je décide enfin de cesser de jouer. Je terminai ce “concert involontaire” avec la chanson “The Glory of Love”. J’avais la voix enrouée et mes doigts étaient fatigués. Ijiro et sa bande vinrent me remercier de leur avoir donné la chance de jouer avec autant d’entrain, puis ils prirent congé après m’avoir serré une dernière fois la main à tour de rôle. Les clients nous avaient applaudis copieusement à la fin de chaque chansons et en redemandaient toujours plus. Leurs encouragements m’avaient donné un regain d’énergie qui, après le “spectacle”, s’était épuisé. Je tenais à peine sur mes deux jambes. En allant me rasseoir à notre table, je fus accueilli par Masahiko qui ne cessait de me donner de grandes tapes dans le dos comme pour me féliciter du tabac que je venais de faire et par Shion qui, très remontée, ne cessait de me dire que je lui avais fait grandement plaisir en la faisant danser comme ça. À la voir ainsi hyperactive, je la soupçonnai d’avoir profité que son père était venu sur la piste de danse avec Yukari pour lui voler quelques gorgées de champagne. Susumu et Hiromi étaient aussi très enthousiastes du petit numéro musical que je venais de donner. Discrète, Makoto se contenta de me dire:  

 

-Tu joues très bien, tu as beaucoup de talent... et tu as une très belle voix quand tu chantes.  

 

Je ne savais plus quoi penser à son propos... Une minute, elle semblait sur le point de me sauter au cou pour m’embrasser, la seconde d’après elle semblait prête à se sauver de moi en prennant ses jambes à son cou.  

 

-Tu avais dit que tu n’étais pas un pro, dit Sora avec un large sourire, mais tu pourrais facilement faire carrière dans ce domaine, si jamais tu te lassais du dessin...  

-C’est quelque chose que je fais comme passe-temps, répliquai-je gêné. Je ne pense pas avoir le potentiel pour faire une carrière internationnale.  

-Tu pourrais facilement, répondit alors Makoto en me souriant.  

 

D’un geste timide, elle posa une main sur la mienne. Le contact fut chaud, délicat et empreint de douceur, comme si elle voulait prendre garde de me pas me briser la main. Plongeant son regard dans mes yeux, et elle ajouta, d’une voix tendre:  

 

-Que ce soit pour dessiner ou pour jouer du piano, tes mains sont très précieuses et grâce à elles, on comprend clairement que tu exprimes ce que tu as de plus beau au fond de ton coeur...  

-C’est très gentil de ta part de me dire ça, je tâcherai de m’en souvenir quand je rejouerai du piano..., répliquai-je d’une voix étrangement douce.  

 

Les paroles qu’elle venait de prononcer m’étaient allées droit au coeur et je ne pus réprimer un petit sourire. Lorsque j’étouffai un long baillement, Yukari décréta d’un ton sans réplique qu’il était temps pour tout le monde de rentrer à la maison. Pendant que nous nous préparions à quitter nos tables, j’entendis Masahiko dire pour lui-même, dans ce qui semblait être un soupir de soulagement:  

 

-Au moins, cette fois-ci, Oncle Sora et ses assistantes ne se sont pas mise à danser nus devant tout le monde!  

-Plaît-il?!, dis-je, incertain d’avoir bien compris ce qu’il venait de dire.  

-Rien, rien, rien, s’empressa de dire Makoto, levant les mains devant elle, l’air embarrassée.  

 

Susumu se pencha vers moi et murmura à mon oreille, un air malicieux se peignant sur son visage:  

 

-Une petite habitude que nous avons entre nous quand nous allons au Karaoké Do Ré Mi. Cette fois-ci, le Patron a pensé que ce serait innapproprié de t’y amener puisque tu n’es pas encore initié à nos coutûmes... Lorsque ce sera le cas, tu verras que Makoto est une très bonne danseuse!  

-Su!!, siffla Makoto, les joues enflammées, arborant un air de reproche. Ne m’embarasse pas devant Patrick-san!  

 

J’eus un petit sourire en coin et, par égard pour elle, je prétendis n’avoir rien entendu. Tandis que nous nous dirigions vers la sortie, plusieurs clients se levèrent sur mon passage pour me serrer la main et pour me dire combien ils avaient aimé ma petite prestation musicale. Je les remerciai pour leur compliments en leur serrant la main aussi chaleureusement que possible et, lorsque nous fûmes arrivés à l’extérieur, Shion me demanda, l’air un peu pompette:  

 

-Tu n’as pas signé d’autographes, Patrick?  

-J’avais égaré mon stylo, répondis-je, le plus sérieusement du monde.  

 

Shion éclata de rire en s’appuyant contre l’épaule de son cousin qui vit, mal à l’aise, que Yukari regardait sa fille d’un air courroucé. À l’extérieur, l’air était encore chaud, mais moins étouffant que cet après-midi-là, lors de mon arrivée. Quand la fourgonnette taxi se garra devant nous, nous reprîmes nos place à l’intérieur et Makoto, une fois de plus, vint s’asseoir sur mes genoux. Durant le trajet, Shion s’endormit, la tête posée sur l’épaule de Masahiko et Hiromi, passablement ivre, ne cessait de nous regarder, semblant envier Makoto. En posant les yeux sur elle, je ressentis à nouveau la flèche de Cupidon transpercer mon coeur...  

 

Était-il possible qu’il puisse y avoir une place au soleil pour un monstre de laideur tel que moi? 








Chapter 6 :: Du sommeil, enfin! ...ou peut-être pas encore!

Published: 14-11-09 - Last update: 14-11-09

Comments: Après une longue période de vide et de hauts et de bas dans ma vie personnelle, je vous offre un nouveau chapitre qui apporte la conclusion à la première journée de Patrick chez les Wakanaés. On en apprend encore un peu plus sur lui, alors qu'il glisse lentement vers les limbes du sommeil tant mérité...

Note à moi-même: ne plus jamais passer plus de 24 heures sans sommeil... Plus que ça, c’est de l’indécence... Pire, ça tient du meurtre!!  

J’étais si fatigué que j’avais de la difficulté à garder les yeux ouverts, arrivant à peine à me tenir debout quand j’entrai à l’intérieur de la résidence des Wakanaé. Il me semblait même entendre ma canne protester de craquements sous le poids que je faisais peser sur elle. Il m’était impossible de demander l’aide de Masahiko: le pauvre bougre était occupé à porter le poids d’une Shion à la démarche incertaine, alors que Yukari-san soutenait Sora qui ne semblait pas tellement conscient de ce qui se passait autour de lui. Les assistantes nous avaient quittés sur le seuil de la porte pour aller se coucher dans leur chambre au deuxième étage et chacune d’elles avaient insisté pour me faire une dernière collade (celle de Kazuko faillit me broyer les côtes). Pendant un moment, il m’avait semblé que Makoto hésitait entre me suivre ou aller se coucher, mais elle décida finalemtn de prendre congé.  

 

-Tu veux certainement aller te mettre au lit immédiatement, me demanda Yukari en laissant Sora s'écrouler lourdement sur le sofa du salon.  

-Disons que j’entends l’appel du lit qui se fait de plus en plus insistant, répliquai-je en m’asseyant sur le sofa, à côté de maître Sora qui s’était mis à ronfler doucement.  

J’arrivai à peine à cacher un bâillement et j’ajoutai:  

 

-Je suis si fatigué que j’ignore même si j’aurai la force de me déshabiller...  

-Alors file te coucher, répondit Yukari en souriant. Je vais être capable de m’occuper de Sora moi-même.  

-Est-ce qu’il va falloir que je transporte Shion à sa chambre tout seul?, protesta Masahiko depuis le pied de l’escalier. Elle est pas grosse, mais elle est d’une lourdeur quand elle a bu!  

-J’ai deux bras et trois jambes, dis-je en riant, mais l’un de mes bras est occupé à tenir ma troisième jambe!  

 

Masahiko ne sembla pas comprendre ma petite farce, qui n’était pas tellement drôle dans le fond, et il entreprit la difficile tâche d’aider sa cousine à monter les marches de l’escalier. Il me sembla qu’elle marmonna quelque chose du genre “je veux pas me coucher”, mais je ne pouvais en être certain. Yukari-san me regarda avec gêne et elle me dit:  

 

-Tu dois avoir une drôle d’opinion sur nous: Shion est ivre alors qu’elle est encore mineure et mon mari qui ne peut supporter les effets secondaires de l’alcool!  

 

J’eus un petit sourire et je lui dis, l’air compatissant:  

 

-Je ne pense rien qui pourrais vous être désobligeant, Yukari-san. Si vous saviez...  

-Commence par me tutoyer, dit-elle en souriant. Et qu’est-ce que je ne sais pas?  

 

Je regardai Maître Sora qui ronflait doucement et je dis, un peu réticent:  

 

-C’est toi qui risquerait d’avoir une drôle d’opinion sur ma personne...  

-Tu n’as pas à me le dire si tu ne le veux pas, répondit Yukari en s’asseyant à côté de moi.  

 

La voir si près aurait pu me révéler quelques indices sur son sexe: c’était un homme travesti, je le savais, mais je ne pouvais voir aucun trait de masculinité dans ce visage bienveillant qui était tourné vers moi et dont le tendre regard semblait percer la façade de dur à cuir que je présentais lorsque je me sentais embarassé.  

 

Je déposai ma canne sur le sol et, croisant les bras sur ma poitrine, je dis:  

 

-Shion est d’une humeur lègère quand elle boit: l’alcool semble la rendre taquine et moqueuse, à ce que j’ai pu constater. Pour ce qui est de Sora-san, un verre l’assomme... Et toi?  

-Je ne bois à peu près jamais, sauf lors de grandes occasions, mais je m’arrête bien avant de ressentir les premiers symptômes de l’ivresse.  

-Rien de mal alors... En ce qui me concerne, j’évite l’alcool comme la peste. Quand on m’en offre, je la refuse et si on insiste, je prends le verre, mais je ne le bois pas. Finalement, il s’évente et il faut le larguer.  

-Ça te rend malade?  

-Non. Vous connaissez l’histoire du Docteur Jekyll et de Mister Hyde?  

-Oui, mais quel est le rapport?  

-Je suis doux et paisible de nature, même s’il m’arrive d’être nerveux et timide dans certaines circonstances... Toutefois, quand j’ai un verre dans le nez, je deviens le contraire de ce que je suis: je perds toute inhibitions, je deviens maussade et impatient. Si on pousse le mauvais bouton, je pète un plomb... Je pourrais facilement assommer d’un coup de poing quelqu’un d’aussi massif que Kazuko... Vous tous me détesteriez si jamais je prenais un verre...  

 

Yukari me regarda d’un air incrédule, incapable d’imaginer un nabot comme moi applatissant une géante comme Kazu... J’eus un sourire gêné et réprimant un autre bâillement, j’ajoutai:  

 

-Au moins, je suis assez conscient de ces faits et c’est pourquoi je n’avale rien qui a plus d’un demi pourcent d’alcool...  

-C’est rassurant, dit-elle dans un ricannement. J’aimerais bien que Shion soit raisonnable comme toi, mais parfois, la maturité semble lui faire défaut...  

-Ta fille est tout à fait respectable et la maturité vient avec l’âge... Elle est encore jeune et comme les jeunes de son âge, elle veut être traîtée comme si elle était plus agée... ça lui passera.  

 

Il y eut un court silence, puis elle dit:  

 

-Il se fait tard, tu devrais aller te coucher.  

-Le lit hurle mon nom..., dis en feignant de tendre l’oreille. J’arrive!!, lançai-je vers le plafond, avec un sourire, feignant de répondre à l’appel imaginaire de mon lit.  

 

Yukari ramassa ma canne et me la rendit, puis je m’extirpai du sofa pour me mettre debout. Nous nous souhaitâmes bonne nuit et je montai lentement les marches de l’escalier. À la dernière marche, ma jambe faible me trahit et je faillis m’étaller de tout mon long sur le sol, mais Masahiko me rattrappa au dernier moment et me soutint un instant avant de dire:  

 

-Est-ce que tu veux que je t’accompagne jusqu’à ta chambre?  

-Je pense que c’est un bonne idée, dis-je avec reconnaissance en m’appuyant sur lui. Ma satanée jambe a tendance à me jouer des tours quand je suis aussi fatigué...  

 

Nous marchâmes côte à côte jusqu’à ma chambre, Masahiko ajustant son pas avec le mien. Il ouvrit la porte et m’attira à l’intérieur. Il me soutint jusqu’à ce que je fus assis sur le lit et il me demanda:  

 

-T’as besoin d’autre chose? Y a qu’à demander.  

-Une nouvelle jambe et un nouveau visage, si t’as ces articles en stock, je suis preneur, dis-je, l’air lugubre.  

 

Masahiko ne put retenir un petit ricannement et il s’assit à mes côtés. Me donnant une tape dans le dos, il dit:  

 

-C’est tout un concert que tu nous as donné au Bistro, tout à l’heure. Tu aurais pu faire carrière dans la musique!  

-Je ne joue pas aussi bien que Liberace et je ne chante pas aussi bien que Roy Orbison, mais je me débrouille..., dis-je humblement. C’est quelque chose que cet accident ne m’a pas enlevé... Quant à la carrière, avec la tête que j’ai, je ferais fuir les foules plutôt que les attirer!  

 

Masahiko m’adressa un air de reproche et il dit:  

 

-Patrick, tu es comme tu es et c’est très bien ainsi. Si tu n’avais pas eu cet accident, tu ne serais peut-être pas ici, en ce moment. Tu m’as dit que tu avais pris conscience de ton talent de dessinateur durant ta convalescence et que c’est à l’hôpital que tu as pris connaissance des oeuvres de mon oncle Sora... Si cette voiture ne t’avait pas explosé à la figure, tu ne nous aurais pas émerveillé avec tes chansons...  

-Ce fut un honneur de frôler la mort pour vous rencontrer, dis-je, sarcastique.  

 

Masahiko ne sembla pas remarquer le ton de ma voix et il dit:  

 

-Une journée déjà s’est écoulée de ton séjour ici et tu en as déjà vu de belles, non? Ma cousine et mon oncle ivres, les assistantes...  

-Tes amis qui veulent te voir habillé en fille..., ajoutai-je avec malice.  

-Ouais, mes amis qui veulent me voir habillé en fille... répéta-t-il avec froideur, les dents serrées. J’avais oublié que je devais les rencontrer demain, ceux-là!  

-Ils ne m’aiment pas beaucoup, dis-je.  

-Ne porte pas attention à leurs regards. Ils n’ont jamais rencontré quelqu’un comme toi avant.  

-Quelqu’un comme moi?, répétai-je aussitôt, piqué au vif. Qu’est-ce que tu veux dire?  

-Ne te fâche pas, répondit-il, mal à l’aise. J’ai pas voulu t’offenser, loin de moi cette idée. Je voulais simplement dire qu’ils étaient mal à l’aise face à ton aspect et ton handicap... Ils t’ont pris pour un Yakuza, c’est certain. T’as pas l’air d’un tendre...  

 

Je me détendis... Ma vive réaction était clairement injustifiée.  

 

-Je m’excuse si j’ai paru fâché, dis-je. Je suis épuisé... Et j’ai encore quelques complexes à cause de mon apparence... Entouré de gens aussi positifs et accueillants, je ne peux que m’en guérir, à la longue...  

 

Masahiko se leva et il dit:  

 

-Je te laisse dormir en paix, alors. Bonne nuit et repose-toi bien!  

-Merci! Bonne chance avec tes copains demain, tu me raconteras!  

-Sois certain que je te reviendrai là-dessus! À plus!  

 

Il allait refermer la porte derrière lui quand j’entendis Shion dire, la voix traînante:  

 

-Laisse-moi passer, Masahiko! Je veux lui souhaiter bonne nuit!  

-Il est couché! Répondit Masahiko  

-C’est pas vrai!, protesta l’adolescente en poussant son cousin, forçant ce dernier à revenir à l’intérieur de la chambre à reculon. Il trébucha dans l’une des boîtes de Kaoru et il tomba à la renverse.  

 

Shion me regarda avec un petit sourire malicieux et, se tournant vers son cousin, l’air sévère, elle dit:  

 

-Tu es un sacré menteur, Masahiko Yanagiba! Il n’est pas au lit! Il n’est même pas déshabillé!  

-C’est malin!, protesta l’autre en se mettant debout, massant son fessier endolori. J’aurais pu me casser le cou en tombant!  

 

Shion grogna d’exaspération et elle vint s’agenouiller devant moi, un petit sourire aux lèvres. Ses pommettes et le bout de son nez étaient rosés et ses yeux étaient perdus dans le vague, mais elle me regardait avec gentillesse et elle dit:  

 

-Je suis peut-être pompette, mais j’ai quelque chose à te dire et je suis très sincère!  

 

-Je t’écoute, Shion, dis-je avec amusement, en dépit du sommeil qui m’engourdissait de plus en plus.  

 

Elle prit une grande inspiration et elle dit, avec fermeté:  

 

-Je t’ai entendu à travers la porte et je veux que tu arrêtes de penser que tu es laid! Tu ne l’es pas, même que je te trouve très beau.  

-Shion, c’est pas le moment de le draguer! Siffla Masahiko en tentant de remettre sa cousine debout.  

 

Elle se défit de l’emprise de son cousin d’un geste vif du bras et elle dit:  

 

-Ben quoi, ben quoi? Il a l’air d’un voyou et ça me plait! Et si ça me plait, ça veut dire que ça va plaire aux autres!  

-Shion tu est saoûle, vas donc te coucher!, répondit Masahiko en tentant à nouveau d’entraîner sa cousine hors de ma chambre à coucher.  

Elle se dégagea à nouveau et elle dit, d’un air enjoué:  

 

-Attends un peu! Faut le border, s’il est aussi fatigué!  

 

Avant même que j’eusse le temps de comprendre ce qu’elle avait en tête, Shion se saisit fermement de mon tee-shirt et elle me l’enleva de sur le dos. Elle se figea alors sur place lorsqu’elle vit mon torse découvert: l’explosion de la voiture n’avait pas fait que mutiler mon visage ou m’handicaper une jambe. Le feu avait brûlé une partie de mon côté droit dans le dos et des éclats de verre avaient zèbré mon ventre et mes pectoreaux de cicatrices profondes. Mon épaule gauche était marqué d’un X rosé et mon bras droit était marqué d’une brûlure en forme de “S” . Ce lot de souvenirs douloureux pétrifièrent la jeune fille et son cousin qui me regardèrent un instant, interdits et stupéfait, Shion tenant mon chandail devant elle, comme si elle s’apprêtait à l’accrocher sur une corde à linge. Atrocement gêné, je détournai mon visage d’eux, sentant le rouge me monter aux joues, serrant les couvertures dans mes poings si fort que les jointures de mes doigts étaient blanchies. Masahiko prit lentement Shion par un bras et lui dit, alors qu’il l’entraînait à l’extérieur de la pièce, les dents serrées, la colère faisant trembler sa voix:  

 

-Maintenant, on sort d’ici gentiment et on le laisse tranquille! Il est fatigué et il a besoin de beaucoup de repos après le spectacle qu’il nous a gentiment offert! C’est d’accord, Shion?  

 

Incapable de détacher son regard de moi, Shion hocha distraitement la tête et laissa tomber mon chandail négligement sur le sol. Masahiko la poussa litéralement hors de ma chambre et il dit, mal à l’aise:  

 

-Désolé, elle...  

-... est ivre, je sais, répondis-je, osant à peine le regarder en face. Je paris qu’elle me trouve moins beau, à présent...  

-C’est pas vrai! Répondit Shion depuis le couloir.  

-Bonne nuit Patrick!, dit Masahiko, l’air découragé, avant de refermer la porte derrière lui.  

 

Je restai un long moment immobile, assis sur mon lit. La réaction de Shion avait été la même que bien d’autres femmes avant elle: elles me disaient toutes que mon visage ne les rebutait pas, mais quand elles me voyaient torse nu, c’était une toute autre histoire... Je pensai alors à Makoto... Si quelque chose de sérieux devait se passer entre nous, aurait-elle la même réaction, elle aussi? Rien de bien rassurant...  

J’entrepris de préparer mon lit afin de m’y glisser quand un frappa doucement à ma porte.  

 

-Une minute, dis-je en m’empressant de remettre mon chandail. Entrez!  

 

Hiromi, Susumu et Kazuko entrèrent alors dans ma chambre, un large sourire illuminant leurs visages et elles vinrent à nouveau me serrer tour à tour dans leurs bras pour me souhaiter bonne nuit. Une nouvelle fois je sentais que je passais au broyeur quand Kazuko m’enserra dans ses bras et Hiromi fit preuve de plus de délicatesse lorsqu’elle m’étreignit. Susumu et Kazu sortirent et Hiromi s’attarda un moment au cours duquel elle me demanda:  

 

-Tu avais l’air songeur quand nous sommes entrées. Il y a quelque chose qui ne va pas?  

-Dans un moment de délire d’ivresse, Shion a décidé de me border... Dis-je après un court silence. Et elle a commencé par m’enlever mon chandail...  

 

Devant son air interrogateur, j’enlevai mon chandail dans un soupir et j’attendis sa réaction; elle n’en n’eut aucune. Elle me regarda un instant sans rien dire, puis dans un haussement d’épaules, elle dit:  

 

-Et alors? Je ne comprends pas...  

-Alors c’est que tu as besoin de lunettes, Hiromi... Dis-je un peu plus froidement que je ne l’aurais voulu...  

-Ou c’est peut-être toi qui a besoin d’une greffe de raison...  

 

Je regardai Hiromi un instant, incapable de comprendre le sens de ses paroles et, devant mon air interrogateur, elle dit:  

 

-La famille qui vit sous ce toit, ainsi que leurs amies, sont logés à la même enseigne que toi, mon vieux. Si tu crois que tu es le seul qu’on regarde comme un monstre de la nature, qu’on dévisage ou qu’on murmure dans son dos, alors détrompe-toi! Tu n’es pas le seul à faire face aux préjugés de ton entourage. Certaines d’entre nous ont été rejetées par leur famille et l’une d’entre nous vit sa vie à l’insu de sa famille pour ne pas la perdre...  

-Au moins, vous n’avez pas l’air d’un steak à demi brûlé!, répondis-je froidement. Vous ètes belles! Moi, c’est avec un sac de patate sur la tête que j’ai le plus de chance de rencontrer quelqu’un!  

-Si ça pouvait te rendre plus brillant... Répondit Hiromi dans un ricanement.  

 

Je serrai les poings et les dents, mais n’ajoutai rien... je ne voulais pas me faire d’ennemies le premier soir...  

 

-Les Wakanaé sont ta nouvelle famille et ils ne vont pas te rejeter à cause de ta différence. Il en est de même pour le quatuor que nous, les assistantes de Maître Sora, formons. Ici, c’est un hâvre de paix pour les gens qui sortent de l’ordinaire. Penses-y quand tu seras dans le doûte.  

 

Hiromi vint m’embrasser sur ma joue mutilée et elle se dirigea vers la porte. Au moment de sortir, elle ajouta:  

 

-Au fait, je ne sais pas ce que tu lui as fait, mais Makoto n’a pas dit un seul mot depuis qu’on est rentrées. D’habitude, elle n’arrête pas de jacasser quand on revient de l’une de nos fêtes, au point ou on l’assommerait parce qu’elle nous empêche de dormir... Mais là, c’est le silence complet. Tu dois lui faire effet...  

 

Elle me fit un clin d’oeil complice avant de fermer la porte et me laissa là, seul, planté au milieu de la pièce, dans un silence lourd. La seule que j’attendais cette nuit-là ne daigna pas venir me rendre visite.... Je le savais, c’était trop beau pour être vrai... Résigné, je me déshabillai et je me glissai sous les couvertures, en dépit de la chaleur qui planait dans la chambre.  

Les yeux fixés au plafond, je ne pus m’empêcher de penser à Makoto... Si je lui faisais un tel effet, était-ce une bonne chose? Je m’étais interdit de tomber amoureux et j’avais enfreint cette résolution. Si quelque chose devait se passer entre elle et moi, cette relation était condamnée à l’échec, peu importe le nombre de moments de bonheur et de joie que nous partagerions ensemble: mon séjour au Japon était destiné à être bref. Il ne durerait pas définitivement. Les pleurs dominaient ce chemin, alors que si je taisais mes sentiments, ce chemin-là serait condamné à être dominé par d’amers regrets...  

 

Ces pensées me tourmentèrent un long moment. Quand je me retournai et que je fermai les yeux, il ne me fallut cependant pas longtemps pour sombrer dans un lourd sommeil bienfaisant... 







Chapter 7 :: À coeurs ouverts

Published: 26-11-09 - Last update: 26-11-09

Comments: On apprend à connaitre dans ce chapitre un côté un peu plus émotif en ce qui a traît aux personnages principaux... avec quelques interrogations.


Ce furent les chauds rayons du soleil qui me réveillèrent le lendemain matin. Ma fenêtre possèdant pas de rideaux, le soleil brillait joyeusement sur mon visage, m’ordonnant de me réveiller et de sauter hors du lit. Je me retournai sur le ventre, enfouissant mon visage dans mon oreiller, tentant de me replonger dans la noirceur, mais le mal était fait: j’étais réveillé et il n’était plus question pour moi de faire la grasse matinée. De mauvaise grâce, je m’assis dans mon lit et je regardai autour de moi. La grande chambre à coucher qu’on me prêtait était baignée d’une chaude lumière et une brise tiède s’y engouffrait par la fenêtre ouverte à la tête de mon lit. J’avais passé une nuit sans rêve, dont le sommeil réparateur m’avait fait le plus grand bien.  

Je me frottai les yeux et m’étirai de tout mon long avant de me laisser retomber sur le dos, les yeux fixés sur le plafond de ma chambre, un petit sourire aux lèvres. Je me surpris à penser que j’allais vivre dans cette belle famille pour une année entière, à tenter de faire honneur à mes engagements envers Maître Sora... en plus d’essayer de composer avec le fait que j’aimais une personne qui ne m’aimait pas de la même façon en retour.  

 

Un grand éclat de rire à l’extérieur me poussa à regarder par la fenêtre. J’aperçus Masahiko qui s’éloignait de la maison, les deux mains dans les poches de son jean, la tête basse, un sac à dos accroché à son épaule. Il marchait d’un pas raide en donnant de grands coups de pieds rageurs dans les caillous qu’il trouvait sur son chemin. J’entendis Shion s’exclamer, depuis le porche:  

 

-Hé!! Attends! Tu es certain de n’avoir pas oublié d’apporter ta robe noire? Tu sais, celle qui rend Tatsumi complètement fou!  

 

Je réprimai un fou rire et j’eus tout juste le temps de voir Masahiko adresser à sa cousine un geste pas très poli de la main... Je sortis hors du lit et je fouillai dans ma valise afin de trouver des vêtements propres. Je pris un tee-shirt noir et un jean bleu que je revêtis et je me saisis de ma canne. Ma satanée jambe avait repris un peu de force, mais pas suffisament pour que je puisse m’appuyer dessus en tout sécurité. Je me rendis à la salle de bain de l’étage et je me regardai dans le miroir. J’avais une mine affreuse, avec mes cheveux en bataille, mon menton mal rasé et mes yeux encore bouffis de sommeil. Je m’aspergeai le visage d’eau froide, puis je peignai mes cheveux sans les attacher. Je voulais juste dissimuler mon visage un tout petit peu, question de cacher un peu mes “défauts”.  

 

Je sortis de la salle de bain et je descendis lentement l’escalier. À mesure que je m’approchais de la salle à manger, j’entendis des bribes d’une conversation entre Yukari-San et Makoto. Avant de me juger sur ce que je vais vous dire, comprenez-moi bien: je ne suis pas du genre à écouter aux portes, ni à espionner les autres, mais malgré moi , la curiosité l’emporta et, me faisant discret, j’écoutai ce qu’elles se disaient à voix basse, appuyé sur ma canne.  

 

-Depuis que je l’ai vu, disait Makoto, je n’arrête pas de penser à lui. Il s’est passé très peu de temps depuis qu’on s’est rencontré, mais il m’a charmée et attirée dès que mes yeux se sont posés sur lui...  

 

-Je te comprends, répondit Yukari d’une voix appaisante, mais pourquoi tu ne le lui dis pas? Je suis certaine qu’il serait plus qu’heureux si tu t’ouvrais à lui.  

 

Un petit silence ou je devinais une certaine hésitation, puis Makoto dit, un peu mal à l’aise:  

 

-Et s’il ne partage pas les mêmes sentiments à mon égard? Je ne pourrais supporter un autre rejet...  

-Qu’est-ce qui te fait penser qu’il te rejettera?  

-Parce que ce serait trop beau s’il acceptait que l’on se fréquente... Répondit Makoto dans un soupir.  

 

Je serrai les mâchoires et mes mains se crispèrent sur le pommeau de ma canne. Je n’eus aucune difficulté à me représenter ce beau prince charmant qui avait séduit Makoto: un grand homme bien bâti, aux traits parfaits, au visage et au corps sans marques de mutilation... Un homme capable de marcher sans l’aide d’une canne, sans avoir besoin de cacher son visage derrière un rideau de cheveux... Je pris une grande inspiration afin de calmer la course folle de mon coeur et je continuai d’écouter parce que soudainement, je voulais avoir mal, je voulais souffrir: de cette façon, mes illusions d’une romance avec cette belle assistante seraient freinées au plus vite.  

 

-Makoto, poursuivit Yukari sur un ton toujours appaisant, si tu ne t’ouvre pas à lui, tu pourrais le regretter amèrement toute ta vie. Tu pourrais même être très étonnée de sa réponse.  

-Mais j’ai vraiment peur qu’il me rejette! Les hommes qui aiment les femmes comme nous sont tellement difficiles à trouver... Vous et Maître Sora avez eu la chance de vous trouver et vous avez pu fonder une famille, mais le rôle d’une mère c’est un rôle que je ne pourrai jamais jouer parce que l’homme que je recherche n’est pas le même genre d’homme que votre mari...  

-Peu importe, répondit Yukari, la vie est trop longue quand on la vit seule. Le seul conseil que je puisse te donner, c’est de tenter ta chance avec lui. Si jamais il n’est pas intéressé, tu auras le mérite d’avoir essayé. Si par contre il s’avère qu’il est aussi épris de toi, alors tu auras la satisfaction d’avoir trouvé ton âme-soeur.  

 

Un autre silence, un autre coup de poignard au centre de mon coeur meurtri, puis d’une voix timide, Makoto dit:  

 

-La première fois que je l’ai vu, je n’ai su que dire ou que faire. Je le trouvais tellement beau, tellement séduisant... Il m’a envoûtée au premier coup d’oeil. Le monde a cessé d’exister. Il n’y avait que lui, il était devenu le centre de l’Univers autour duquel je gravitais.  

-C’est un peu comme ça que je me sentais quand j’ai rencontré Sora la première fois, répondit Yukari sur un ton ou l’on devinait le petit ricannement.  

-Cette nuit, j’ai eu beaucoup de difficulté à m’endormir parce que je ne faisais que penser à lui. Je ne voulais pas m’endormir parce que j’avais peur qu’à mon réveil je doive réaliser que j’avais rêvé...  

-Sora et moi étions incapables de cesser de nous regarder. Quand il est venu me parler, j’avais le coeur qui battait la chamade...  

-J’ai tellement peur d’avoir l’air ridicule si j’allais lui dévoiler mes sentiments pour lui!, répondit Makoto d’une voix étouffée.  

 

Sur un coup de tête, sans trop y réfléchir, j’entrai dans la salle à manger. Yukari était assise à côté de Makoto et lui entourait les épaules de son bras droit. Cette dernière avait enfoui son visage dans ses mains et en entendant le bruit de ma démarche claudiquante, elle se redressa. Il ne lui fallut qu’une fraction de seconde pour transformer sa surprise en un sourire qui parrut forcé, elle me salua, immitée par Yukari.  

 

-Tu es déjà réveillé?, me demanda cette dernière en se levant de sa chaise. Je pensais que tu dormirais plus longtemps que ça, il est à peine huit heures du matin!  

-Disons que le “bombardement” m’a réveillé... répondis-je en tentant de sourire.  

-Je ne comprends ce que tu veux dire..., demanda Yukari d’un air interrogateur.  

-Le bombardement de rayons de soleil, répondis-je en souriant. Je n’ai pas de rideaux dans mes fenêtres, quelque chose qu’il va falloir remédier au plus vite...  

 

Makoto baissa la tête d’un air gêné et elle me lança un regard bref et timide avant de se mettre à fixer ses mains en silence. La voilà qui recommençait son petit manège de timidité... Yukari s’approcha du frigo et elle me demanda:  

 

-Tu veux que je te prépare quelque chose?  

-Non merci, répliquai-je en fixant Makoto d’un regard scrutateur. Je ne mange jamais le matin, je n’ai pas faim.  

 

Un silence lourd que je trouvai oppressant s’installa dans la pièce et je ne trouvai rien à dire pour le rompre. Makoto et moi continuions d’échanger des brefs regards alors que Yukari s’affairait à remplir le lave-vaisselle.  

 

-Tu as bien dormi, au moins?, finit-elle par me demander.  

-Comme un loir, répondis-je en me tournant vers elle. Masahiko a joué le bon samaritain en m’aidant à gravir les dernières marches de l’escalier et en me soutenant pour m’aider à me rendre à ma chambre et Shion a même voulu me border... Bref, je suis traîté comme un roi ici!  

 

Yukari échappa un petit ricannement et elle dit:  

 

-Shion est tout un numéro quand elle a trop bu... On essaie de la surveiller et de la sermonner pour qu’elle se montre plus raisonnable, mais parfois, c’est peine perdue.  

-’Faut bien que jeunesse se passe, dis-je. J’ai déjà été jeune comme ça...  

-Tu n’es pas si vieux..., dit Yukari en riant.  

-Hé Ho! J’ai tout de même vingt-neuf ans!  

 

Autre silence durant lequel Makoto et moi échangeâmes un nouveau regard. Tantdis que je gardais un visage de marbre, m’éfforçant de rester inexpressif, elle m’adressa un timide sourire et elle esquissa un geste comme si elle voulait poser sa main sur la mienne, mais elle se ravisa lorsque Yukari revint s’asseoir avec nous.  

 

-Je voulais te féliciter une nouvelle fois pour le petit spectacle que tu nous as donné hier au Bistro, dit-elle comme si elle n’avait pas remarqué le geste de Makoto. Tu chantes très bien.  

-Merci encore, répondis-je avec un sourire forcé. Il faut dire que j’avais un excellent orchestre qui m’accompagnait.  

-Pour jouer, ils savaient jouer, dit Makoto dans un soupir, mais pour chanter, tu étais le meilleur.  

 

Je ne répondis pas. La jalousie pour ce prince charmant qui l’avait séduite me tuait: ce qui me torturait le plus, c’est que la veille, tout en elle, depuis sa façon de me regarder et de me parler, jusqu’à la façon qu’elle avait eu de s’accrocher à mon cou lorsque nous étions assis dans la wagonnette, semblait m’indiquer que j’avais une chance... Pourquoi avait-elle joué ce jeu alors, si c’était pour me jeter à la figure que je n’étais pas dans son coeur?  

 

Avec un mot d’excuse, je pris congé et je sortis à l’extérieur en prenant soin de mettre mes verres fumés. Je trouvai Shion et Sora qui se lançaient la balle, au fond de la cour arrière, à l’ombre de deux arbres. Je les observai quelques instants alors qu’ils échangeaient des blagues qui les faisaient bien rire, jusqu’à ce que Shion s’aperçoive de ma présence.  

Avec un petit sourire malicieux, elle me lança la balle en s’écriant:  

 

-Pense vite!  

 

D’un geste vif, je saisis la balle qui me pinça la paume de ma main et, un sourire narquois aux lèvres, je dis:  

 

-Tu ne pensais quand même pas que j’allais laisser passer ça?  

 

Sora me regarda, aussi impressionné que sa fille qui semblait prise au dépourvu et il dit, après avoir échappé un petit sifflement:  

 

-C’était tout un boulet que ma fille t’a lancé la et tu l’as attrappé à main nue?? Quel autre talent as-tu et qu’on ignore?  

-Avoir le coup de foudre pour les mauvaises personnes, répondis-je en m’approchant d’eux.  

-Hein?, demanda Shion en me regardant d’un air interrogateur.  

-Pense vite, répliquai-je.  

 

Sans autre avertissement, je laissai tomber ma canne et je lui lançai la balle de toutes mes forces. Entraîné par mon élan, je perdis l’équilibre et je tombai par terre, les deux mains sur le gazon chaud. Shion dût tendre le bras au-dessus d’elle pour l’attrapper de justesse parce que j’avais lancé un peu trop haut. Quand elle me vit par terre, elle fit un pas vers moi pour m’aider à me relever, mais je me redressai aussitôt, après m’être emparé de ma canne.  

 

-Il sait lancer, en tout cas!, dit Shion en regardant son père. Ouille ouille ouille! Ça pince!  

 

Elle avait enlevé son gant de base ball et elle secouait sa main. Sora échappa un petit ricannement et il dit:  

 

-Je vous laisse, je vais dans mon studio. J’ai encore quelques croquis à faire pour Mademoiselle Mori. À plus tard et toi Patrick, profite bien de ta semaine de congé parce que dans six jours, tu commences!  

-L’attente sera interminable, répondis en souriant.  

 

Sur ce, il entra à l’intérieur de la maison. Shion me m’invita à m’asseoir avec elle à l’ombre des arbres et je la suivis volontier. Me retrouver à l’extérieur me faisait le plus grand bien, d’autant plus que le terrain était complètement entouré d’une haute clôture qui préservait l’intimité familiale de la résidence. Avec un sourire, ramenant sa longue chevelure dans son dos, Shion me dit:  

 

-T’as tout les talents, toi!  

-Bof... répondis-je dans un haussement d’épaules. J’ai appris le piano quand j’allais à l’école, j’ai toujours été un sportif... avant cet accident, dois-je spécifier... et j’ai découvert le dessin lors de ma convalescence... Pour ce qui est du chant, j’ai fait la chorale à tous les ans à Noël dès l’âge de cinq ans jusqu’à mes dix-sept ans, c’est à dire jusqu’au jour ou j’ai réalisé que les cantiques n’étaient pas vraiment de mon genre...  

 

Ma remarque la fit rire, mais elle prit alors une expression gênée et elle dit:  

 

-Patrick, je tiens à m’excuser de mon comportement d’hier soir. Je ne sais pas ce qui m’a prise d’agir ainsi...  

-Bah! Tu voulais t’amuser, non? Il y avait toute une ambiance hier au Bistro, tu n’es pas la première qui vole quelques gorgées d’alcool dans le verre de son père...  

-Je ne parlais pas de ça... dit-elle, mal à l’aise.  

-Ah..., répondis-je en me souvenant de ce qui s’était passé la veille quand elle avait voulu me “border”....  

-Quand j’ai fait la bêtise de te déshabiller...  

 

J’eus un petit sourire et je lui dis:  

 

-Tu n’as pas à t’en faire, Shion. Tu ne m’as pas offencé.  

-N’empêche, je n’avais pas le droit de faire ça... J’étais ivre, mais ça n’excuse pas mon geste.  

-Shion, puisque je te dis que je ne t’en veux pas! Ce n’est pas grave. Il faut bien que je finisse par accepter que j’ai l’air de Mr Stitch...  

-Qui c'est celui-là?, me demanda-t-elle, intriguée.  

-C’est un personnage d’un film d’horreur, un véritable navet... Le titre anglais, c’est Mr Stitch... En français, ça s’appelle 88 visages. C’est un espèce de montre de Frankenstein constitué de quatre-vingt-huit personnes différentes... Trop long à expliquer.  

 

Après un bref silence, elle me demanda:  

 

-Qu’as-tu voulu dire tout à l’heure quand tu avais un talent pour avoir le coup de foudre pour les mauvaises personnes?  

 

J’hésitai un long moment avant de répondre. Je ne voulais pas resortir le discours du je-suis-trop-laid, mais tout s’y rapportait.  

 

-Parce que c’est vrai, répondis-je dans un haussement d’épaules. Il m’arrive de tomber amoureux d’une personne et cette personne ne partage pas les mêmes sentiments...  

-C’est arrivé souvent?  

-Beaucoup trop souvent à mon goût.  

-C’est arrivé récemment?  

-Tu es curieuse..., dis-je ne me forçant a sourire.  

-C’est ce qui est charmant chez moi!, répliqua-t-elle en ne faisant un clin d’oeil. Allez, dis-moi!  

 

Je soupirai, réalisant que je ne pourrais pas m’en sauver...  

 

-Eh oui... Ça m’est arrivé récemment.  

-Je le savais!, dit-elle, triomphante. Allez, c’est qui?  

-Tu es trop curieuse, Shion...  

-Non, allez dis-moi c’est qui??, dit-elle d’un ton insistant.  

-Je préfère taire son nom pour éviter de mettre cette personne dans l’embarras...  

-C’est quand même pas ma mère?, dit-elle, faussement indignée.  

-Non, Yukari-san est mariée et je ne veux pas faire affront à Maître Sora.  

-C’est moi?  

-Désolé, mauvaise réponse, répliquai-je en souriant.  

-Masahiko?, me demanda-t-elle d’un air malicieux.  

-NON!, m’empressai-je de répondre.  

-C’est Hiromi, alors?  

-C’est l’une des assistantes, mais je ne te le dirai pas.  

-Kazuko?, demanda-t-elle, incertaine. Elle est mariée, je te signale...  

-J’ai l’air de quelqu’un qui est attiré par un char d’assaut vêtu d’une robe?  

 

Shion pouffa de rire et, me scrutant du regard, elle finit par dire:  

 

-T’inquiète, je découvrirai bien qui c’est! Je te verrai bien avec Hiromi ou Makoto...  

 

Voyant que je ne disais toujours rien, les yeux fixés vers le ciel, elle appuya sa tête contre mon épaule et elle dit, d’un ton aguichant:  

 

-Si tu me le dis, je garderai le secret. Je pourrais peut-être t’aider, qui plus est!  

-Shion, je préfère ne rien dire... Cette personne semble être attirée par quelqu’un d’autre, de toute façon. Alors tu vois, je ne veux pas m’imposer dans la vie de qui que ce soit.  

-Tu es sérieux, ou c’est encore un stupide complexe à cause de ton apparence?  

-Un peu des deux...  

-Patrick, tu n’es pas laid!  

-Shion, tu es très gentille, mais sérieusement, si tu avais le choix entre un bel homme à la peau parfaite, au physique avantageux et à la démarche sûre, et quelqu’un comme moi, honnêtement, lequel choisirais-tu?  

 

Elle soupira et elle dit:  

 

-Tu penses que j’aurais honte si j’avais un petit ami comme toi?  

-Je ne parle pas de honte, mais disons que tu finirais par te lasser des regards...  

-Tu ne me connais pas encore... Moi je ne te vois pas comme un Mr Stitch, comme tu t’appelles, mais comme un survivant. Je serais fière si j’avais un survivant comme toi pour petit ami...  

 

Un survivant? Eh oui, j’ai survécu, mais à cette époque, j’aurais préféré être mort. Je n’en dit rien à Shion, de peur de devoir me faire sermonner à nouveau, et je me contentai de fixer le ciel d’un bleu clair.  

 

-Patrick?  

-Oui?  

-Pourquoi t’es aussi amer?  

-Je suis amer, moi?  

-Oui. Tu sais j’ai peut-être seulement dix-sept ans, mais disons que le mode de vie particulier que j’ai eu jusqu’à aujourd’hui m’a donné une maturité précosse qui m’a aidé à voir au-delà des apparences. Tu es gentil et attentionné avec les autres, tu sais divertir les gens autour de toi et je suis certain que cette année qu’on va passer en ta compagnie sera tout sauf ennuyeuse... Pourtant, tu sembles te détester, malgré tous tes talents. Tu es amer avec la vie... Envers toi...  

-Amer... Aigri... Choisis le terme que tu veux, c’est assuré qu’il me collera à la peau comme une sangsue. Si je n’avais pas été dans les parrages quand cette bombe a explosé...  

-Tu ne serais peut-être pas ici...  

-Masahiko me l’a déjà faite celle-là.  

-Peut-être devrais-tu commencer à y croire?  

-Peut-être...  

-Je ne te force pas à te confier à moi, mais si tu as envie de parler...  

-Je sais, je te remercie...  

 

Eh puis merde! Je poussai un long soupir, puis je décidai de tout lui dire.  

 

-Cette petite assistante m’est arrivée en pleine figure comme un coup de poing sur la gueule. Elle m’a ensorcellé et elle m’a volé mon coeur. Elle m’a semblé être attirée par moi, mais pauvre crétin que je suis! Je l’ai entendue parler avec ta mère ce matin dans la salle à manger... Elle parlait de ce bel homme qui l’a foudroyée de sa belle apparence... Je peux me l’imaginer, ce bellâtre: aucune cicatrices, la démarche sûre...  

-Peut-être que Makoto parlait de toi... Peut-être qu’elle ne les voit pas, tes cicatrices...  

-Peut-être... Hé! Comment t’as deviné que c’était d’elle que je parlais?  

-Il fallait être aveugle pour ne pas voir comment vous vous regardiez tout le temps hier... Je la connais depuis longtemps et je peux te dire que jamais je n’ai vu pareille expression sur son visage quand elle regardait quelqu’un.  

-La vie serait trop belle... Je n’oserais pas espèrer...  

-Justement, c’est peut-être pour ça que tu es aussi amer...  

-Tu ne sais pas ce que c’est que d’être aussi défiguré... J’ai été rejeté trop de fois pour m’accrocher à l’espoir que quelque part, il y aie une place pour moi dans le pays du bonheur...  

-C’est à toi de voir si tu veux vraiment une place dans ce “pays”...  

 

Sur ce, elle se leva et partit en direction de la maison. Tandis qu’elle s’éloignait, je me surpris à me demander si elle n’avait pas raison...  

 

Avais-je malgré moi tendance à m’apitoyer sur mon propre sort?  





Chapter 8 :: La Visite de Mr Tatsumi

Published: 26-11-09 - Last update: 26-11-09

Comments: Un épisode un peu plus léger, avec une petite surprise à la fin... 

C’était l’après-midi et je me trouvais au salon avec Yukari et Shion, leur montrant mes dessins, lorsqu’on claqua la porte d’entrée sans ménagement. Nous eûmes un sursaut et nous échangeâmes un regard intrigué.  

 

-Masahiko?, demanda Yukari, inquiète.  

 

Silence... Puis, le bruit d’un sac à dos qu’on jetait par terre.  

 

-Masahiko est absent, laissez un message après le foutu bip sonore!, répondit le jeune homme d’un ton irrité, alors qu’il s’apprêtait à gravir l’escalier.  

 

-Qu’est-ce qui se passe? Demanda Yukari. Pourquoi tu as l’air fâché?  

-Je reviens dans deux minutes!, répondit Masahiko d’un ton pressé, comme s’il cherchait à se sauver.  

 

Shion eut alors une expression malicieuse qui n’avait rien de rassurant et elle se précipita vers le vestibule d’ou on l’entendit pouffer de rire, puis Masahiko s’exclama:  

 

-Shion, laisse-moi tranquille!  

-Viens nous montrer combien tu es JOLIE!  

-Non! J’ai dit non!, répliqua Masahiko d’un ton rageur. SHION LÂCHE-MOI TOUT DE SUITE!!!  

 

Amusé, j’échangai un regard intrigué avec Yukari alors que retentissait un bruit de lutte, puis nous vîmes Shion apparaître dans l’embrasure de la porte. Elle poussa son cousin à deux mains dans le dos pendant que ce dernier essait désespèrément de mettre les freins, les deux talons plantés sur le sol. Ce fut peine perdue: quand il sut qu’il était exposé à nos regards, Masahiko cessa de résister et il laissa sa cousine le pousser jusque devant nous, l’air résigné. Je compris enfin pourquoi il refusait de se montrer à nous: il était vêtu d’une chemise sans manches rose, d’une jupe noire courte et portait des bas de nylon couleur chair. J’eus peine à reconnaitre mon ami tant le maquillage de qualité professionnel et la coiffure qu’il arborrait le changeaient de manière phénomenale. Il aurait pu être l’une des assistantes de son oncle! Apparament, il n’était pas enchanté d’être vu ainsi et c’est avec une profonde gêne qu’il me regarda avant de baisser les yeux, son visage rougissant.  

 

Yukari le regardait en hochant la tête, un petit sourire aux lèvres, alors que Shion, lui barrant la sortie et lui interdisant toute possibilité de fuite, me regarda, l’air satisfaite, en disant:  

 

-Patrick, je te présente Masami, ma cousine.  

-Tais-toi, Shion! Répliqua Masahiko, les dents serrées.  

-Ils ont fini par t’avoir, c’est ça? Demandai-je, souriant malgré moi.  

-Quelle clairvoyance!, répliqua Masahiko en venant s’asseoir à côté de moi.  

 

L’air contente d’elle, comme si elle tirait une grande fièreté de mettre son cousin dans un tel embarras, Shion, les mains croisées derrière son dos, dit d’un air taquin:  

 

-Alors, tu auras combien de scènes de baisers avec Ejima cette fois-ci?  

 

Masahiko adressa à sa cousine un regard noir, resta silencieux et croisa ses bras sur sa poitrine. Je passai une main dans mes longs cheveux et, incapable de détacher mon regard de mon ami, je dis:  

 

-Dois-je en conclure que finalement, il y aura beaucoup de scènes de baisers avec Ejima?  

-On ne peut rien te cacher... Répondit-il avec dédain. Après un court silence, il me demanda: Qu’est-ce que tu as à me regarder comme ça??  

-Oh rien, répondis-je dans un haussement d’épaules. Je te trouvre juste... jolie...  

 

Yukari ne put réprimer un petit ricannement qu’elle tenta tout de même de dissimuler derrière sa main et Masahiko sembla s’enfoncer davantage dans les coussins du sofa. L’air maussade, il maugréa:  

 

-Lee et Ejima m’ont coincé!  

-Et comment, je te prie? Demanda Shion, malicieuse.  

 

Masahiko hésita un long moment et il dit:  

 

-Encore et toujours cette stupide vidéo qu’ils ont fait de moi dans la douche quand ils ont cru que c’était toi qu’ils filmaient!  

-Pardon?, demanda Yukari en cessant de rire brusquement. De quoi parles-tu, Masahiko?  

 

Ce fut Shion qui raconta en quelques mots ce qui s’était passé: Apparament, elle avait tourné un film étudiant nommé The Night Leopardess et Lee avait voulu lui faire tourner une scène de nu, mais elle avait refusé. Après le dernier jours de tournage, elle avait voulu aller prendre une douche, mais elle avait décidé d’attendre et c’est Masahiko qui avait pris sa douche dans la salle de bain ou Lee et Ejima avaient installés des caméras, croyant qu’ils allaient avoir leur scène de nu avec Shion... Ils ont déchanté quand ils se sont rendu compte qu’ils avaient filmé Masahiko... Et ils s’étaient servi de cette cassette en guise de chantage pour persuader ce dernier de se joindre au Club Ciné de son Université.  

 

Si Yukari parassait scandalisée en entendant sa fille raconter cette histoire, de mon côté j’étais franchement en colère. Serrant les dents, je maugréai:  

 

-Si jamais je mets la main au collet de cet imbécile de Lee, je lui fait avaler sa stupide casquette de producteur à la noix! C’est illégal ce qu’il a tenté de faire!  

 

Masahiko me regarda avec appréhension et il dit:  

 

-Justement, tu pourrais avoir ta chance, mais ce serait mal avisé de lui faire bouffer sa stupide casquette de producteur à la noix, comme tu dis.  

-Et pourquoi, je te prie? Demandai-je, toujours révolté à l’idée qu’un universitaire avait tenté de filmer une mineure dans sa douche.  

-Parce que ce fameux film qu’il veut que je fasse a un personnage de Yakuza et il souhaiterait que tu endosses ce rôle...  

 

J’eus un mouvement de recul, incrédule à cette idée, et je regardai tour à tour Shion, Masahiko et Yukari, avant de dire:  

 

-Tu te moques de moi, j’espère?! Moi, jouer le rôle d’un Yakuza?! Il est timbré, ou quoi? Je suis même pas Japonnais!!  

-Non, je ne me moque pas de toi, répondit Masahiko avec un certain amusement dans la voix. Il était très sérieux. Il trouve que tu as la tête de l’emploi.  

-Je m’en vais lui en faire une tête au carré, oui!, m’exclamai-je avec véhémence. Il n’est pas question que je joue dans son film! Il n’a pas arrêté de me regarder de travers quand je suis arriver hier, et l’autre con d’Ejima avait l’air de penser que je venais de débarquer d’une autre planète!  

 

Yukari posa une main sur mon épaule, l’air inquiète à cause de ma réaction. Elle pensait peut-être que j’aillais retrouver Lee et Ejima pour leur règler leurs compte, idée qui me séduisait royalement il va sans dire!  

 

-Pas question! Dis-je d’un ton catégorique. Je ne suis pas venu ici pour jouer dans un film, je suis ici pour suivre un stage avec Maître Sora! Il n’est pas question que je revienne sur ma parole!  

 

On sonna à la porte et Shion s’empressa d’aller ouvrir. Masahiko posa une main sur mon épaule et il dit:  

 

-Personne ne t’oblige à participer, tu sais. Je n’étais pas d’accord non plus parce que je sais que tu as certains problèmes avec ton apparence, mais d’un autre côté, j’ai lu le script et je peux t’assurer que je ne verrais personne d’autre pour ce rôle.  

-C’est ça, je ne coûterais pas cher de maquillage, c’est ce que tu es en train de dire?  

-Non! Répondit Masahiko avec empressement. Pas du tout!  

 

Il avait parlé avec une drôle de voix, presque féminine... Lorsqu’il s’en aperût, il plaqua ses mains sur sa bouche et sa tante prit la relève:  

 

-Patrick , écoute: le fait que tu aies une chance de jouer dans un petit film étudiant ne signifie pas que tu manqueras à tes engagements envers Sora. Je suis certaine qu’il sera enchanté quand nous lui en parlerons.  

-On n’aura rien à lui parler parce qu’il est hors de question que je joue dans ce film! Répondis-je d’un ton sans réplique.  

Shion revint dans la pièce, un large sourire aux lèvres, et elle dit d’une voix enjouée:  

 

-Devinez qui vient nous rendre visite!  

 

Un jeune homme, vêtu d’un jean, d’un tee-shirt et d’une chemise (tout en noir) trop ample pour lui entra dans la pièce. Ses cheveux bruns clairs étaient longs et retombaient sur ses épaules et couvraient son visage. Les mains dans les poches, il avait ce petit air rebel et désinvolte qu’ont les adolescents qui entrent à l’âge adulte. En le regardant plus attentivement, je remarquai que les traits de son visage étaient délicats, presque féminins.  

Le jeune homme fut accompagné d’un homme plus âgé, un véritable géant. Costaud et large d’épaules, il portait un habit trois pièces blanc et une chemise noire. Ses cheveux étaient plus courts, noirs, et il portait une barbe de deux ou trois jours. Je remarquai qu’il avait une cicatrice au-dessus de l’oeil droit. Il avait la démarche sûre et arborait un air sévère, mais lorsqu’il entra dans la pièce, son visage s’éclaira d’un large sourire.  

 

Yukari se leva, apparament contente de les voir, et elle dit:  

 

-Mr Tatsumi! Kaoru! Quelle belle surprise!  

-Yukari, vous ètes toujours aussi jolie!, répondit le géant d’une voix profonde en donnant à Yukari un baise-main.  

-Salut, Tante Yukari, répondit le jeune homme d’une voix basse, comme s’il cherchait à rendre sa voix plus grave qu’elle ne l’était en réalité.  

-Kaoru, je suis très contente de te revoir! Dit Yukari en serrant le jeune homme dans ses bras.  

 

Se tournant vers moi, Mr Tatsumi me regarda d’un air intrigué et il dit:  

 

-Qui est ce jeune homme? C’est la première fois que je le vois ici, non?  

-Ouais, qui c’est ce type?, demanda Kaoru et entourant les épaules de Shion de son bras. Un autre clodau que vous avez ramassé dans la rue?  

-Je t’en foutrai, moi!, répliquai-je, piqué au vif.  

 

Déjà que je n’étais pas d’humeur à ce que me cherche des poux dans la tête, j’avais pas besoin qu’un jeune effronter vienne m’insulter par-dessus le marché!  

 

-Kaoru, dit Tatsumi en regardant son fils, sur un ton de reproche, surveille ton langage.  

 

Le géant s’approcha de moi et m’offrit une solide poignée de main en ajoutant:  

 

-Il faut pardonner à Kaoru son impétuosité, il a tendance à être familier avec tout le monde. À qui avons-nous l’honneur?  

-Il s’appelle Patrick Decker et il est ici pour un an, dit Masahiko en se levant. Il va suivre un stage avec mon oncle.  

 

Mr Tatumi eut alors une drôle de réaction en voyant Masahiko. Du coup, j’ai pensé qu’il serait troublé de voir ce dernier vêtu en fille, mais je me souvins qu’il avait été trés épris de “Masami”...  

Kaoru laissa échapper un petit sifflement en dévisageant Masahiko de la tête aux pieds et il dit, l’air badin:  

 

-Alors, tu y a finalement pris goût? Ça t’a été bénéfique de laisser tomber cette chipie de Asaoka!  

-De quoi tu parles?, demanda Masahiko avant de réaliser ce à quoi Kaoru faisait référence.  

 

Il sursauta et remarqua enfin que Mr Tatsumi le regardait avec un drôle de sourire. L’air profondément embarassé, Masahiko tenta de se faire discret et Mr Tatsumi sembla sortir de sa transe. Se râclant la gorge, il dit:  

 

-Kaoru est venu chercher ses dernier cartons, alors j’ai pensé profiter de cette occasion pour venir vous dire bonjour.  

-C’est très gentil de votre part, répondit Yukari en souriant. Shion, tu veux bien aider Kaoru?  

-Avec plaisir! Répondit cette dernière avant de sortir de la pièce, suivie par Kaoru.  

 

Mr Tatsumi s’assit sur fauteuil en face de moi et il me demanda:  

 

-Alors, tu viens d’ou?  

-De la province du Québec, au Canada, répondis-je.  

 

Tout d’un coup, je me sentis plus à l’aise avec lui. En dépit de son allure de dur à cuir, il m’était sympathique. Quelque chose émanait de lui, une certaine attitude, (ou une aura, appelez ça comme vous voulez), qui faisait peur à certains mais qui invitait à la confidence pour d’autres.  

 

-Tu as fait un long voyage pour venir ici, et tout ça pour suivre un stage de Mangaka?  

-Oui, répondis-je. J’ai pris conscience que je savais dessiner lors de ma convalescence.  

-Convalescence? De quoi tu parles? Tu as eu une maladie grave?  

-On peut dire ça, si vous considèrez comme une maladie une voiture piègée qui vous explose à la figure et qui laisse des traces sur votre visage et le reste de votre corps.  

 

Mr Tatsumi éclata de rire et il dit:  

 

-Tu es très drôle, toi! Il me plaît, ton copain! Ajouta-t-il en se tournant vers Masahiko.  

-Il gagne à être connu, répondit ce dernier, mal à l’aise devant le regard pénétrant que lui adressait le Yakuza.  

-En tout cas, il sait jouer et chanter, intervint Yukari avec une certaine fièreté, comme si elle cherchait à vanter mes quelques talents de musicien.  

-Ah? Je serais curieux d’entendre ça, dit Mr Tatsumi en se penchant vers moi, l’air intéressé. Tu joues de quel instrument?  

-Piano et guitare, mais le piano est mon instrument de prédilection, répondis-je avec un sourire. Hier, j’ai joué quelques chansons avec un groupe amateur et j’ai eu un certain succès.  

-Tu es trop modeste, Patrick, répliqua Masahiko avec un sourire. Tu as fait danser presque tout le monde quand tu chantais!  

-Il faut absolument que tu me montres ce que tu sais faire, dit Tatsumi, toujours penché vers moi, prenant soudain des airs d’homme d’affaires. Je connais quelques personnes dans le milieu qui pourraient t’aider à te faire connaitre. Tu n’as qu’un mot à dire et je m’occupe de tout! Je serai ton agent!  

 

J’eus un sursaut et, n’osant croire ce que je venais d’entendre, je dévisageait Tatsumi quelques secondes en silence avant de dire, un peu embarrassé:  

 

-C’est très généreux de votre part, Mr Tatsumi, mais je ne pense pas avoir l’étoffe d’une star de la chanson...  

-Ridicule! Répliqua le Yakuza d’un geste impatient de la main. Si les Wakanaé disent que tu as du talent pour la musique, alors je les crois sur parole!  

-Peut-être bien, mais... balbutiai-je, mal à l’aise.  

-Écoute, je possède un petit piano bar dans le quartier de Kabukicho. Voilà ce que je te propose: Tu viens y jouer quelques chansons, et si tout va bien, on discutera des procédure pour te faire rencontrer mes contacts...  

-Je ne suis pas ici pour enregistrer un album, Mr Tatsumi, m’empressai-je de dire en levant les mains à la hauteur de mes épaules.  

-C’est décidé!, répliqua-t-il d’un ton sans réplique, levant un doigt autoritaire devant moi. Tu viens chanter dans mon bar demain soir! Je m’occupe de préparer le terrain pour toi!  

-JE PEUX EN PLACER UNE OUI?!, m’exclamai-je avec une soudaine véhémence.  

 

Tatsumi, eut un mouvement de recul et Masahiko et Yukari me regardèrent, incrédules. Réalisant que je venait de gueuler sur un Yakuza, chose que je sentais que j’allais regretter, je m’empressai de dire, la voix chevrotante:  

 

-Mr Tatsumi, je m’excuse de vous avoir crié dessus, mais voilà: sachez que j’apprécie tout l’intérêt que vous me portez et je vous remercie de vouloir vous montrer aussi généreux avec moi, mais le fait est que je suis un homme d’honneur. Je suis convaincu que vous savez de quoi je parle quand je dis ça. J’ai pris des engagements avec Maître Sora et je ne peux pas me laisser distraire par une carrière de chanteur qui risque d’être éphémère... Je suis venu ici pour perfectionner ma technique de dessinateur, non pas pour faire carrière dans la chanson. Je vous remercie encore une fois, mais je me vois dans l’obligation de décliner respectueusement votre offre.  

 

Mr Tatsumi me dévisagea un long moment en silence, une expression dure sur le visage, puis il prit un air résigné et il dit:  

 

-J’imagine que si tu as pris d’autres engagements, je ne peux pas t’en détourner. Je respecte énomément les personnes qui tiennent parole et qui ont de l’honneur. Je ne te cache pas que je suis très déçu que tu refuses mon offre, mais j’apprécie ton honnêteté.  

-Je vous assure que je ne désire pas vous offenser...  

 

-Pas du tout!, répliqua Mr Tatsumi en riant, n’aie crainte, il en faut plus pour m’offenser! Sache cependant que mon invitation tient toujours! Je veux que tu viennes jouer dans mon bar demain soir!  

-C’est d’accord, dis-je en tendant la main.  

 

Il la serra à nouveau avec fermeté et il regarda sa montre.  

 

-Il est presque l’heure du souper..., dit-il. Il faudrait que Kaoru se dépèche de récupèrer ses cartons.  

 

-Je vais aller voir ce qu’ils fabriquent, dit Yukari en se dirigeant vers l’escalier, me laissant seul avec Mr Tatsumi qui regardait Masahiko avec un petit sourire en coin.  

 

Nous restâmes ainsi en silence pendant un long moment, puis le Yakuza dit, d’une voix étrangement douce:  

 

-Masami, tu m’as manqué, tu sais?  

 

Masahiko sursauta comme s’il avait été aspergé d’eau glacée et il regarda le Yakuza, l’air pris au dépourvu. En croyant à peine mes oreilles, je dévisageai le collosse qui soudainement se précipita à genoux, aux pieds de Masahiko et, prenant les mains de ce dernier dans les siennes, il dit:  

 

-Je t’invite à venir dîner avec moi ce soir! Je te prie d’accepter! Tu me manques, c’est plus fort que moi! Saki est sortie de ma vie pour de bon, Kaoru a trouvé sa place dans mon foyer et a une amie de coeur, il ne reste plus que moi qui suis seul! Masami, je t’aime, je veux passer le reste de ma vie avec toi!  





Chapter 9 :: LES SOUCIS DE SHION

Published: 07-12-09 - Last update: 07-12-09

Comments: un autre chapitre qui parle un peu plus des sentiments de Shion pour Masahiko, mais pas trop... et on apprend qui est la petite amie de Kaoru... un chapitre bien tranquile, mais qui vaut la peie d'être lu...


On peut dire une chose de Mr Tatsumi: il est fort!  

 

Comment a-t-il pu réussir à convaincre Masahiko d’accepter son invitation à aller dîner au restaurant avec lui, sous les traîts de Masami, c’est au-dessus de mes capacités de compréhension. Je n’avais pu détacher mon regard incrédule de ce spectacle particulier: un Yakuza format géant agenouillé devant un jeune homme habillé en fille (contre son gré), le suppliant de l’accompagner dans une soirée romantique dont l’issu était très prévisible...  

 

Kaoru parut amusé quand il revint au salon, accompagné de Shion, comme son père venait de quitter la résidence, accompagné de Masahiko, et que je leur racontai ce qui venait de se passer.  

 

-Mon père est tout un numéro!, avait ricanné Kaoru en se laissant tomber sur le sofa. Mais maintenant il y a un petit problême: j’étais venu avec mon père pour ramasser mes affaires... S’il est parti avec la voiture, je ne peux logiquement pas trimballer tout ça à pieds...  

-Tu n’as qu’à rester pour souper, avait dit Yukari en haussant les épaules.  

-Merci, mais y a quelque chose d’embêtant! J’avais rendez-vous avec ma petite amie...  

-Elle n’a qu’à venir te rejoindre. Plus on est de fous plus on rit!  

 

Cette Yukari était vraiment d’une générosité sans borne! Alors qu’elle s’en allait à la cuisine pour préparer le repas, je remarquai que Kaoru me dévisageait, un petit sourire aux lèvres, puis il finit par me demander:  

 

-Serais-tu par hasard un futur assistant de Sora?  

-Non, je suis ici pour un stage, pourquoi tu le demandes? Demandai-je, soupçonneux.  

-Parce que je t’imagine mal portant une robe et des talons hauts... Disons que le résultat est assez particulier avec Susumu et Kazu... Et avec la gueule que tu as...  

 

Je réagis comme si un dard m’avait piqué:  

 

-Qu’est-ce qu’elle a ma gueule? Elle te répugne?...  

-C’est pas ce que j’ai voulu dire... Je te juge pas...  

-Tu parles de ma gueule, mais la tienne? T’as l’air d’un gars, mais plus je te regarde, plus je trouve que tu as l’air d’une fille déguisée!  

 

Kaoru perdit son sourire désinvolte et il soupira rageusement. Se penchant vers moi, il dit:  

 

-Fais gaffe à ce que tu dis, quand même! Sora a été mon mentor durant mon cheminement pour devenir un homme, alors m’insulter c’est l’insulter lui!  

-J’ai vu juste, alors?  

 

Son regard froid et son silence furent éloquents. Si un couple pouvait échanger leurs rôles, alors pourquoi une fille ne pourrait-elle pas devenir un homme? Je fis un petit sourire narquois et je me tournai vers Shion. Elle se tenait devant la fenêtre du salon, silencieuse, les bras croisés sur sa poitrine, l’air soucieuse. Elle regardait à l’extérieur, tapant du pied, et semblait ne pas avoir remarqué le petit échange entre Kaoru et moi.  

 

-Shion?, dis-je. Quelque chose ne va pas?  

 

Elle ne me répondit pas, comme si elle ne m’avait pas entendu. Je répétai ma question, toujours pas de réponse. Kaoru et moi échangeâmes un regard intrigué, haussant les épaules, et je dis:  

 

-Ton père court depuis longtemps après Masahiko comme ca?  

-Je ne sais pas exactement. Ça fait un bail, en tout cas...  

-Et le fait que “Masami” soit un homme ne semble pas le déranger...  

-J’ai renoncé à essayer de comprendre la sexualité de mon père depuis longtemps, répondit Kaoru en ricannant. Si tu veux bien m’excuser, je vais téléphoner à ma copine.  

 

Il se leva et alla à la cuisine. J’en profitai pour me lever à mon tour et je m’approchai de Shion. Elle remarqua à peine ma présence lorsque je lui demandai:  

 

-Qu’est-ce qui ne va pas? Depuis que vous ètes redescendus tu n’as pas dit un mot et tu es restée plantée là comme un piquet.  

-Ce crétin se met toujours dans des situations embarassantes et c’est toujours moi qui finit par l’en sortir!, répondit-elle d’une voix étrangement étouffée.  

 

Sur ce, elle fit volte-face et quitta la pièce d’un pas raide, les poings serrés. Je la suivis du regard et elle bouscula Kaoru sans ménagement alors qu’il revenait au salon.  

 

-Hé!, s’exclama-t-il, outré. Regarde un peu ou tu marches!  

 

Il la regarda s’éloigner, puis il revint s’asseoir aur le sofa, l’air boudeur.  

 

-Un drôle de numéro cette fille, tu ne trouve pas?, dit Kaoru en levant les yeux vers moi.  

-Qu’est-ce que tu veux dire?, demandai-je en me rasseyant à ses côtés.  

-Elle pourrait avoir n’importe quel gars de son choix (et je connais même quelques filles qui se laisseraient tenter), mais elle en pince pour lui, et il est trop stupide pour voir l’évidence...  

-Tu pourrais être plus clair?, demandai-je tout en sachant ou il voulait en venir. De qui tu parles?  

 

Kaoru m’adressa un sourire moqueur et il dit:  

 

-Je te laisse le soin de le découvrir par toi-même. Dis, je m’excuse si je t’ai offencé, tout à l’heure. C’était pas mon intention.  

-Je sais bien, répondis-je, un peu gêné. J’ai pas voulu être hostile, mais depuis que je suis arrivé ici, j’ai été sans cesse dévisagé... Ma réaction tient plus de l’exaspération que de l’hostilité... Si tu savais comment je déteste mon visage...  

-Pourquoi? Tu as vu de quoi mon père a l’air, non?  

-Oui, mais il y a une petite différence: lui c’est un colosse, moi non. Jamais il ne me viendrait à l’idée de chercher des poux dans la tête de quelqu’un comme lui. En plus, il peut se tenir debout sans canne, ce que je ne peux faire trop longtemps.  

 

Après un court silence un peu gêné, il dit:  

 

-Changement de sujet: Shion m’a dit que tu étais musicien?  

-Tu ne vas quand même pas me demander de faire un disque toi aussi, soupirai-je.  

 

Kaoru éclata de rire et il dit:  

 

-Non, pourquoi?  

-Ton père a voulu être mon agent...  

-Ah, Tatsuya est comme ça. S’il t’a fait une telle offre, c’est qu’il t’aime bien. Considère-toi chanceux. Il n’est pas si gentil avec les étranger, d’habitude.  

-Il veut que j’aille jouer dans son bar demain soir. Est-ce que tu joues de la musique?  

-Oui, si on veut. C’est plutôt le chant qui m’intéresse. Je fais partie d’un groupe dont les membres sont tous aveugles, mais ils jouent magnifiquement!  

-On devrait jouer ensemble un de ces jours... On ne sait jamais... dis-je, sans grande conviction.  

 

Je me demandai alors combien d’autres personnes allaient me harceler avec mes “talents” de musicien... Il se passa une demie-heure durant laquelle Kaoru et moi parlâmes de nos vies respectives. Il m’expliqua comment il avait fait connaissance avec les Wakanaé, les raisons pour lesquelles il avait choisi Maître Sora comme mentor dans son cheminement pour devenir un homme, sa réconcilliation avec son père, comment il avait rencontré sa petite amie...  

Je lui parlai de mon accident, la chose la plus spectaculaire qui s’était passé dans ma vie. Je lui parlai aussi de mes déceptions amoureuses, de mes projets et de mes ambitions...  

 

Quand on sonna à la porte, Kaoru s’y précipita en courant. Quand il revint dans la pièce, il tenait par la taille une jeune femme à l’air timide, vêtue d’une robe rouge, ses longs cheveux noirs noués en queue de cheval. Elle se peletonnait contre lui et le regardait d’un air mielleux. Lorsqu’elle me vit, elle eut un sursaut et détourna son regard avec empressement, gênée. Je ne m’en formalisai pas et Kaoru fit les présentation:  

 

-Patrick, je te présente ma petite amie, Akané Fujisaki. Akané, je te présente Patrick Decker. Il va passer un an avec les Wakanaé pour un stage avec Sora-san.  

-Enchanté, dis-je en tendant la main vers la jeune femme.  

-Moi de même, répondit-elle sans me regarder en me serrant la main.  

-Akané est d’un naturel timide, mais une fois qu’elle aura fait ta connaissance, elle sera un peu plus ouverte..., dit Kaoru en souriant tendrement à sa petite amie.  

 

Le couple vint s’asseoir à côté de moi et, enlacés dans les bras l’un de l’autre, ils échangèrent de longs regards qui en disaient long... Et qui me faisaient atrocement envie... Pendant un instant, je me vis à la place de Kaoru, et Makoto occupait la place d’Akané. Discrètement, je m’éclipsai et j’allai rejoindre Yukari à la cuisine ou elle s’affairait à couper des légumes. En me voyant, elle me demanda:  

 

-Kaoru est sorti?  

-Non, sa petite amie est arrivée et j’ai subitement cessé d’exister, répondis-je dans un rire. Les amoureux sont seuls au monde, dit-on... Tu as besoin d’aide?  

-Non merci, je m’en tire très bien. Tu es notre invité et je m’en voudrais de te laisser faire mon travail à ma place.  

-Pas du tout, protestai-je. Ça m’aurait fait plaisir, mais je n’envahirai pas ton domaine.  

 

Je décidai de ne pas insister et je décidai d’aller rejoindre Shion qui sans doutes était montée à sa chambre. Je pris le temps de jeter un coup d’oeil en passant devant la porte du salon et je vis Kaoru et Akané étroitement enlacés dans les bras l’un de l’autre, regardant la télé. Je pris mon temps pour monter l’escalier et arrivé au bout, je pris quelque secondes pour reprendre mon souffle... L’idée de me remettre à faire du sport pour me remettre en forme effleura brièvement mon esprit tandis que je gravissais l’escalier menant à la chambre à coucher de Shion. La pièce était vaste et aérée, décorée de couleurs claires. Je trouvai Shion etendue sur son lit, serrant un oreiller dans ses bras. Sa respiration semblait saccadée, comme si elle sanglotait. Selon toute vraissemblance, elle ne m’avait pas entendu pénètrer dans la pièce parce qu’elle sursauta lorsque je lui parlai:  

 

-Shion? Quelle que chose ne va pas?  

 

Elle s’empressa d’essuyer ses larmes et elle jeta son oreiller derrière elle, l’air gênée. Je m’assis à ses côtés et elle me regarda, un faible sourire aux lèvres. En dépit des efforts qu’elle faisait pour le dissimuler, je pouvais voir la profondeur de son chagrin au fond de ses yeux.  

 

-Alors, pourquoi tu pleurais?, demandai-je timidement, conscient que je me mêlais de ce qui ne me regardait pas.  

-Je pleurais, moi?, répondit-elle d’un air faussement innocent. Je ne pleurais pas, j’avais seulement une poussière dans l’oeil.  

-À d’autres!, répliquai-je, un petit sourire en coin. Tu pleurais, je t’ai vue.  

 

Son faux sourire s’effaça lentement et son menton se mit à tembler. De nouvelles larmes roulèrent sur ses joues et elle détourna son regard, honteuse de se laisser aller devant moi. Je déposai ma canne par terre et je l’attirai vers moi. Je l’étreignis avec fermeté alors qu’elle laissait libre cours à ses pleurs, son visage enfoui dans mon cou, ses mains serrant le dos de mon tee-shirt. Il lui fallut plusieurs minutes pour se calmer et lorsque ses larmes cessèrent enfin, elle put enfin parler:  

 

-Je comprends ta douleur et ta peine quand tu dis que tu as eu le coup de foudre pour les mauvaises personnes! Ça m’est arrivé à moi aussi et je sais combien on peut souffrir quand on aime sans être aimé de la même façon en retour!  

 

Je caressai ses cheveux avec douceur et je la laissai reprendre son souffle avant de lui demander:  

 

-Qui est l’inconscient qui a laissé passer la chance d’avoir une petite amie aussi extraordinaire que toi?  

-Il s’appelait Mitsuhiro. Tu le connais.  

-J’en doute, je viens d’arriver...  

-Mitsuhiro était le nom masculin de Hiromi...  

 

J’eus un mouvement de recul, complètement pris au dépourvu par cette réponse, et voyant mon air ahuri, elle eut un pâle sourire avant de dire:  

 

-C’était avant qu’il devienne ce qu’il est aujourd’hui.  

 

Elle se défit de mon étreinte, mais resta pelotonnée contre moi, sa tête reposée contre mon épaule.  

 

-J’étais jeune à l’époque, au début de l’adolescence. Il était venu demander à mon père de le prendre sous son aile. Mon père a accepté et peu à peu, il s’est taillé une place dans mon coeur de jeune fille... Mon premier amour... Il était très gentil, très attentionné... J’ai cru qu’il partageait en silence les sentiments que j’éprouvais pour lui, mais j’ai eu la surprise de ma vie quand je l’ai vu, ce fameux soir de Noël... Comme les autres assistantes de mon père, qui étaient des hommes auparavant, il est devenu une femme.  

 

Elle fit une pause pendant laquelle mon cerveau marchait à vive allure. Les paroles de Kaoru prennaient leur sens, maintenant... Voilà pourquoi il disait mal m’imaginer en robe... Je n’avais jamais ressenti le besoin de me travestir, ni de changer de sexe. Or, tous les hommes qui étaient passés dans le studio de Maître Sora avaient “changé de sexe”. Je repoussai vivement cette éventualité et Shion poursuivit:  

 

-J’ignore pourquoi je me sens aussi mal quand Masahiko sort avec d’autres personnes et qu’il ne m’invite pas... Bien souvent, c’est pour des réunions du club ciné, mais quand c’est des occasions comme ce soir...  

-Est-ce que Masahiko est gay?, demandai-je.  

-Non, il a clairement spécifié qu’il ne l’était pas. Tu aurais dû le voir quand il est arrivé dans cette maison. Il a éprouvé beaucoup de difficulté à s’adapter au mode de vie de mes parents, mais il y est parvenu. Il a aussi clamé avec véhémence qu’il n’aimait pas se travestir, mais Lee est un gars entêté et un peu maître chanteur sur les bords, alors...  

-Mais ça ne t’empêche pas de le taquiner, non?  

-Ouais...  

-Shion, serais-tu amoureuse de Masahiko sans même t’en rendre compte? Ou est-ce parce que tu as peur de tes sentiments que tu refuses de te les avouer?  

 

Elle haussa les épaules et elle prit un mouchoir pour éponger ses larmes. Elle resta silencieuse quelques secondes avant de se tourner vers moi et me demander:  

 

-Est-ce que je peux compter sur ta discrétion?  

-Absolument. Tu as été assez gentille pour me laisser te confier mes petits soucis amoureux à propos d’une certaine personne, tu peux compter sur moi pour garder le silence. Je resterai muet comme une tombe.  

 

-Merci, me dit-elle en me serrant dans ses bras, puis elle déposa sur mes deux joues un baiser amical qui me fit chaud au coeur.  

 

Nous échangeâmes un sourire et un peu mal à l’aise, je lui demandai:  

 

-Que sais-tu de ce Tatsumi? À part le fait que ce soit un Yakuza...  

-C’est un drôle de bonhomme, en fait, répondit Shion avec un drôle de sourire. Parfois il peut être très charmant, d’autre fois il peut se montrer très effrayant. Il a essayé de changer Masahiko en Masami pour vrai, mais il n’est jamais parvenu à ses fins. Il a tenté par tous les moyens de faire en sorte que mon cousin devienne une femme, mais Masahiko a toujours été capable de résister... Jusqu’à maintenant...  

-Tu crois sérieusement que Masahiko pourrait succomber?  

-Tous les hommes qui sont passés ici ont soit quitté pour ne plus jamais revenir, soit ils sont restés et sont devenues des femmes... Peut-être que Masahiko est seulement plus difficile à “convertir”...  

-Je ne le pense pas, Shion. Masahiko n’a pas la mentalité d’une femme. Je sais reconnaitre ce genre de choses. Il restera tel qu’il est.  

 

Comme elle ne répondait pas, je l’étreignis à nouveau brièvement. Nous restâmes ainsi serrés l’un contre l’autre longtemps, plongés dans un silence de calme et de détente, et pour la première fois depuis très longtemps, je me sentis vraiment à ma place là ou j’étais...  





Chapter 10 :: Rencontre au Club Cinéma

Published: 15-07-10 - Last update: 15-07-10

Comments: Après une trop longue absence au cours de laquelle j'ai du consacrer toutes mes énergies à règler des problemes personnels, je suis de retour avec un nouveau chapitre de Family compo. Un chapitre plus léger, plus drôle, aucours duquel certains événement feront changer le fameux Lee (Director) son fusil d'épaule... Bonne lecture en esp`rant que vous retrouverez l'intérêt de me lire:)


J’avais l’intention de faire de sérieuses mises au points avec Lee, mais ce qui se passa ce jour-là dépassa mes espérances les plus folles...  

 

Tout a commencé après le souper de la veille, pendant que Masahiko était sorti (bien malgré lui) avec Mr Tatsumi. Nous venions de nous installer au salon afin de continuer la conversation que nous avion commencée lors du repas quand la porte d’entrée fut ouverte puis fermée sans ménagement. Ce fut un Masahiko apparament très contrarié qui apparut devant nous, toujours habillé en Masami, l’air plus mécontent que jamais. Sans prendre la peine de nous saluer, il jeta un regard mélangeant colère et exaspération sur Kaoru et il dit, d’un ton peu amical, tout en désignant la porte du pouce d’un geste sec:  

 

-Toi! Ton père t’attends dehors! Dépêche-toi de l’emmener très loin d’ici avant que je ne fasse un malheur!!  

 

Kaoru eut un petit sourire malicieux et, se levant, il nous salua tous, puis, accompagné d’Akané, il nous quitta sans adresser un regard à Masahiko. Ce dernier les regarda sortir puis, se tournant vers moi, il dit:  

 

-Il faut que je te parle. Donne-moi le temps de sortir de cet accoutrement ridicule et je te rejoins. Dans ta chambre.  

 

Se tournant vers sa cousine qui le regardait d’un étrange air moqueur, il ajouta:  

 

-Sans aucune oreilles indiscrètes, c’est clair?  

 

Shion leva les mains d’un air innocent, l’air de se demander ce dont son cousin pouvait bien vouloir sous-entendre, et Masahiko disparut dans un coup de vent. Maître Sora et Yukari-san échangèrent un regard intrigué pendant que Shion s’extirpait du sofa et qu’elle quittait la pièce en gambadant et en fredonnant. J'eus un haussement d’épaules je me levai à mon tour en disant:  

 

-Veuillez bien m’excuser, il apparait qu’on me demande de façon plutôt impérative...  

 

Maître Sora, qui cachait à peine son fou rire, dit:  

 

-Il n’y a pas de quoi s’excuser. Puisque Masahiko prend la peine d’être aussi impératif, il m’apparait impossible que tu puisses t’esquiver... Quand vous aurez fini, tu as envie qu’on se fasse quelques parties de domino?  

 

-J’en serai ravi!, répondis-je avec un sourire. A plus tard alors.  

 

Je quittai la pièce et arrivé au pied de l’escalier, j’entrepris la pénible tâche d’en gravir les marches, appuyé sur ma canne. Arrivé tout en haut, je fis une pause pour reprendre mon souffle. Je vis alors Makoto qui sortait de la salle de bain, sa brosse à dents à la main. Nous échangeâmes un bref regard (le sien était toujours empreint de timidité, le mien sans doute embarrassé, allez-savoir pourquoi), puis je lui tournai le dos pour me diriger vers ma chambre à coucher. Avant d’y entrer, je jetai à nouveau un regard vers elle et, àma grande surprise, elle me sourit en me faisant un petit salut de la main. Je lui rendis son sourire et répondis à son geste d’un hochement de tête. Appuyée contre le câdre de la porte, elle était déjà vêtue de sa chemisette de nuit et semblait prête à se mettre au lit, en dépit du fait qu’il n’était pas si tard...  

 

J’allais m’asseoir sur mon lit et pendant que j’attendais Masahiko, je regardai dehors. Le soleil s’était couché depuis environ une quinzaine de minutes, selon moi, et le ciel commençait à s’emplir d’étoiles. Je n’eus pas à attendre bien longtemps: Masahiko pénétra dans ma chambre à coucher d’un pas rageur en s’exclamant:  

 

-Ce taré de Yakuza veut m’épouser, bordel!! Il veut que je sois sa femme! Non mais tu te rends comptes?!?!  

-Gné??, fut tout ce que je trouvai a dire, incertain d’avoir bien entendu.  

 

Masahiko, qui avait commencé à faire les cents pas devant moi, s’arrêta net et me regarda comme si j’étais un demeuré. Il s’approcha de moi et pendant une seconde, j’eus l’absurde impression qu’il allait me peler le crâne à grands coups de ma propre canne. Il me saisit par les épaules et il me dit, en détachant chaque syllabes et me secouant légèrement:  

 

-Tatsumi vient de me demander de l’épouser. Il est prêt à payer pour toutes les opérations! Tu piges?!?!  

 

L’air gêné, je ne pus qu’émettre un ricannement niais et mon copain recommença à faire les cents pas devant moi, les mains dans ses poches. Habituellement, je suis plus rapide à la raisonnette, mais je fus tellement pris au dépourvu par ce que venait de me dire Masahiko que je ne trouvais rien d’intelligent à dire.  

 

-Désolé..., murmurai-je en m’efforçant de ne pas rire, légèrement embarassé.  

 

Masahiko se laissa tomber sur mon lit, découragé, et il dit:  

 

-J’ai cru comme un imbécile que Tatsumi avait réalisé que je n’éprouvais absolument aucun plaisir à me travestir, mais apparament, j’avais tout faux!  

-Que tu aies du plaisir ou non à te travestir, tu dois tout de même avouer que tu fais une fille tout à fait convaincante...  

 

Je venais de gaffer, inconsciemment. Masahiko serra les poings et il me dit, les dents serrées:  

 

-J’ai fait ça seulement parce qu’on m’a fait chanter! J’ai jamais voulu en faire une vocation, figure-toi! Tout ça c’est sorti de l’esprit tordu de Lee. Ejima s’en est servi comme tremplin pour sa carrière d’acteur! Et Shion... Elle s’est fait un malin plaisir de m’entraîner comme un toutou de cirque!  

 

-N’empêche, tu as l’air d’avoir un don naturel quand tu deviens Masami, dis-je, sans réfléchir.  

 

D’un geste vif, je plaquai mes mains sur ma bouche, mais le mal était déjà fait: Masahiko me regardait d’un oeil assassin et tout à coup je me sentis rétrécir à vue d’oeil. Afin d’échapper à la lourdeur de son regard, je détournai la tête. Lorsqu’il m’adressa à nouveau la parole, je perçus dans sa voix une pointe de malice:  

 

-N’oublie pas que tu as accepté de m’accompagner demain à la réunion du club ciné de mon école.  

-Je m’en souviens que trop bien, mais ne t’imagine pas que je vais me plier aux demande de Lee, il risque d’être déçu. D’autant plus que j’ai quelques points à lui mettre sur les “I” en ce qui concerne Shion.  

 

Masahiko sembla se détendre et il m’adressa un petit sourire. Il se leva et, avant de sortir de ma chambre, il dit:  

 

-On doit y être à la première heure demain matin... La journée risque d’être longue.  

-Ça dépend pour qui, dis-je.  

 

La-dessus il referma la porte derrière lui.  

 

* * * * *  

 

Et c’est ici que ça se corse...  

J’avais planifié un scénario très simple ou j’entrais dans le local du club ciné, repérais ce gros plein de soupe de Lee, l’isolais dans un coin sombre, rejetais son offre de jouer dans son film à la con et lui expliquais dans les moindres détails pourquoi il était bon pour sa santé qu’il renonçat à tenter de tourner autre une scène de nu avec Shion, chose qui semblait l’obsèder au plus haut point. Il avait affirmé à mon amie qu’il aurait fait ça artistiquement... Mon oeil!  

Mais pour une obscure raison que je ne m’explique pas, ça ne s’est pas passé comme prévu. Masahiko et moi sommes entrés dans le local du club ciné, ou une dizaine de personnes étaient déjà rassemblées et discutaient à voix basse. Au fond du local, alors que je cherchais Lee du regard, je repèrai Akané qui me salua énergiquement de la main avant de me faire signe de la rejoindre. Au cours du souper, elle s’était finalement débarassée de sa gêne à mon endroit et était devenue des plus sympa.  

 

-Ou est Lee?, demandai-je à Masahiko dans un murmure alors que nous nous approchions de Akané.  

 

-Je l’ignore, répondit-il dans un haussement d’épaule. Habituellement, il est toujours le premier arrivé étant donné qu’il est le chef de l’équipe. Son retard n’est pas habituel...  

 

J’affichai malgré moi une moue déconfite et Akané me scruta du regard avec attention avant de me demander:  

 

-Quelque chose ne va pas, Patrick-san?  

-Tout va bien, Akané, répondis-je en m’efforçant d’être un peu plus jovial. Je pensais que je verrais Lee et Ejima en entrant ici. J’aurais voulu les saluer correctement...  

 

Pendant que je parlais, je pouvais sentir tous les regards tournés vers moi, chose que je détestai immédiatement. Les membres du club me regardaient en chuchotant entre eux dans leur langue natale. Je regrettai de ne pas avoir laissé mes cheveux détachés, mais voulant suivre le conseil de Maître Sora j’avais décidé de laissé mon visage découvert. Se rendant compte qu’ils me fixaient des yeux, Masahiko les regarda sévèrement et leur dit, avec froideur:  

 

-Quoi? Vous n’avez jamais vu de Canadien avant? Arrêtez de le dévisager comme ça, vous êtes ridicules!  

 

Plusieurs étudiants se détournèrent de nous promptement, mais quelques uns attardèrent leurs regard sur moi. Je m’efforçai de leur sourire, mais j’avais davantage envie de leur foutre des claques que d’être amical. Akané posa une main sur mon épaule et elle dit d’un ton incitant à l’indulgence:  

 

-Ils ne sont pas méchants, mais un peu stupides parfois. Ils sont curieux de savoir ce qui s’est passé pour que tu sois balafré comme ça. C’est ce qu’ils se disent entre eux. La fille là-bas trouve ça dommage, parce qu’elle te trouve plutôt mignon.  

 

J’eus un sursaut d’étonnement et du regard, Akané désigna une jeune fille aux cheveux longs et noirs qui me regardait en me souriant timidement. Je la saluai en souriant, tâchant de calmer mon exaspération, et je regardai autour de moi, cherchant un endroit ou je pourrais me faire le plus discret possible. Nous n’eûmes pas à attendre longtemps avant de voir Lee et Ejima faire leur entrée dans la pièce, accompagnés d’un grand type maigrichon qui transportait une caméra à l’épaule. Lee, qui portait sa stupide casquette et ses lunettes fumées rondes, avait l’air étrangement soucieux lorsqu’il s’assit à son bureau, tout à l’avant de la classe. Étant l’un de leur plus vieux membre, Masahiko alla s’asseoir à ses côtés, de même que Ejima et le type à la caméra, alors que les autres étudiants s’asseoyaient à leurs pupitres. Je décidai d’aller au fond de la pièce et, en silence, je m’appuyai contre le mur, tout près de la fenêtre.  

Tout en étallant ses notes devant lui, Lee salua tout le monde et, ô cauchemar, il ajouta:  

 

-Vous avez certainement remarqué que nous avions un invité spécial parmis nous. Pour son bénéfice, afin qu’il nous comprenne, cette rencontre se déroulera en anglais, puisqu’il ne comprend pas un mot de notre langue. Il s’appelle Patrick Decker et il vient du Canada. Souhaitons-lui tous un chaleureux WELCOME.  

-Welcome, chantonna la classe sans grand enthousiasme.  

 

Je les saluai en m’incliant, appuyé sur ma canne, afin d’éviter une perte d’équilibre qu’il m’aurait fait embrasser involontairement le parquet, puis Lee s’adressa à ses ouialles. En fait, la réunion ce jour-là n’avait qu’un seul point à l’ordre du jour: l’annonce que le prochain film produit par l’équipe sénior du club ciné serait le dernier puisque qu’ils étaient en dernière année d’Université. Il y eut quelques larmes de la part des membres les plus jeunes, mais Lee ne leur prêta aucune attention. Il raconta ensuite les grandes lignes de l’histoire qui se résumait ainsi: ce devait être l’histoire d’une jeune fille (Masami) ayant quitté le millieu des Yakuza, dans lequel elle avait été élevée, qui se faisait poursuivre par un Yakuza sans pitié qui cherchait à la ramener dans sa méchante famille. Le petit ami de la fille (Ejima, sans doute) allait tout mettre en oeuvre pour la sauver afin d’éviter qu’elle ne retourne dans sa famille Yakuza... Pendant que Lee parlait, les membres du club notaient scrupuleusement tout ce qu’il disait.  

 

Durant tout ce temps, Ejima ne cessait de me jeter des coups d’oeils inquiet. Je fus étonné de constater que son regard était rempli de crainte, comme s’il redoutait que je pète un cable et assassine tout le monde dans la salle. Après une heure qui me sembla interminable, Lee m’adressa finalement la parole:  

 

-Patrick, aurais-tu la générosité de venir ici pour te présenter à nous, afin que nous sachions à qui nous aurons affaire?  

-Je ne pense pas qu’il aie envie de venir faire un discours devant les membres du club, Sempai, répliqua Masahiko, mal à l’aise. D’autant plus que je ne me souviens pas qu’il aie encore accepté de participer au film...  

 

Tous les membres du club ciné se tournèrent vers moi d’un même mouvement et me regardèrent avec insistance. J’haussai les épaules d’un air résigné et je m’avançai vers l’avant de la classe. Me tournant vers eux, je me râclai la gorge et, m’efforçant de m’exprimer dans le meilleur anglais possible, je dis, m’appuyant sur la table ou Lee, Ejima, Masahiko et le type à la caméra étaient assis:  

 

-Bonjour à tous. Je suis heureux d’être venu dans votre beau pays. Je loge présentement chez Masahiko Yanagiba dont la famille a eu la bonté de m’accueillir sous son toit. Je veux préciser que je suis ici en stage pour un an, non pas en cinématographie, mais pour devenir mangaka. J’ai une semaine libre afin de m’adapter à mon nouvel environnement pour ensuite m’immerger de façon intense dans mon stage. Je doute fort que j’aurai le temps de faire partie de votre équipe.  

 

Il y eut un léger murmure de déception dans la pièce et je continuai:  

 

-J’espère que j’aurai l’occasion de vous revoir au cours de mon année de stage, vous avez l’air tous très sympatiques et accueillants. Bonne journée à tous.  

 

Il y eut un bref applaudissement et Lee donna congé à son équipe. Lorsqu’il ne resta plus que Masahiko, Ejima, Lee et moi-même, ce dernier dit:  

 

-Tu peux t’asseoir, les chaises ne mordent pas ici.  

 

Je n’obtempèrai pas. Appuyé sur ma canne, je me contentai de le fixer du regard, conscient que Ejima murmurait quelque chose à l’oreille de Masahiko. A peine eut-il terminé que je lui dis, les yeux toujours fixés sur Lee:  

 

-Si tu as quelque chose à dire à mon sujet, Ejima, tu pourrais au moins avoir la politesse de parler assez fort pour que tout le monde t’entende.  

 

Ejima rougit quasi-instantanément. Gêné, il bredouilla:  

 

-Je... Je demandais juste à Giba comment tu t’étais fais ça...  

 

Il pointa son visage en dessinant les traits de mes cicatrices sur sa peau. J’eus soudain une idée et tout se mit en place à la vitesse de l’éclair. Je me forçai à sourire et je dis:  

 

-Eh bien, Masahiko pourrait te raconter toute mon histoire, ça me gêne pas. Il pourrait même te la raconter dehors, et nous laisser seuls, Lee et moi. J’ai besoin de lui parler seul à seul.  

-Je ne pense pas que ce soit une bonne idée, répliqua Masahiko d’un ton hésitant.  

-J’insiste, répondis-je, en étirant mon sourire.  

 

Masahiko n’était pas idiot: il devinait que derrière mon sourire artificiel, je bouillais de colère et de ressentiment envers Lee. Masahiko acquiessa quand même et il entraîna Ejima hors de la pièce en me jetant un regard méfiant. Lee, qui me tournait le dos, rassemblait les notes qu’il avait dispercées sur la surface de la table pendant la réunion et, quand la porte fut refermée, il se tourna vers moi et il me demanda:  

 

-Alors, pourquoi tu voulais me voir seul à seul?  

-Une ou deux choses à discuter avec toi, quelques petits points à mettre au clair, répondis-je calmement.  

-Tu as toute mon attention, répondit Lee en croisant ses mains sur son énorme ventre.  

 

Je m’approchai de lui, affichant un air calme et neutre, mais à l’intérieur de moi, la colère grondait. Je serrai la main sur le pommeau de ma canne et je pris une grande inspiration avant de dire:  

 

-D’après ce que m’a dit Masahiko, tu trouves que j’ai une tête de Yakuza?  

-Ton aspect physique et ta voix profonde apporteraient un réalisme saisissant au personnage que tu vas incarner dans mon film, répondit Lee comme si j’osais mettre sa parole en doute.  

-Tu as l’air assuré de ma participation...  

-Je ne vois pas pourquoi tu n’y participerais pas. Tu es tout désigné pour ce rôle.  

 

Je pris quelques secondes pour bien respirer, mais toute la colère que j’avais réussi à contenir en moi durant la réunion menaçait de faire irruption à tout moment. Aussi pour m’appaiser un peu les esprits, je dis tout en m’efforçant de rire:  

 

-Si tu trouves que j’ai l’aspect physique d’un Yakuza, je pourrais me pratiquer un peu afin d’en avoir également l’attitude et les agissements...  

-Biensur, ça rendra ta performance encore plus réaliste!, répondit Lee en me donnant une tape sur l’épaule, très enthousiasmé.  

 

A la seconde ou il me toucha, je ne pus me contenir. Laissant tomber ma canne par terre, je le saisis par le col de son chandail et je l’allongeai de force sur un pupitre avec plus de brusquerie que je ne l’aurais voulu. Appuyant l’un de mes avant-bras sur sa gorge, je posai ma main libre sur sa bouche et je murmurai d’une voix étrangement menaçante:  

 

-Tu vois, je commence ma pratique sur toi, Lee! J’ai quelques mises au point à faire avec toi et nous allons les passer en revues. Tu vas te contenter de fermer ta grande gueule et tu me laisseras parler, c’est clair?  

 

Il hocha la tête péniblement, me regardant avec de grands yeux horrifiés, laissant échapper un couinnement étouffé parfaitement ridicule. Je lui enlevai sa stupide casquette, libérant sa bouche, et je la jetai par terre. Pointant un index menaçant sur son nez, je poursuivis:  

 

-Primo, tu m’oublies pour ton satané film! J’ai pas envie de faire le mariole devant une caméra pour un étudiant qui se prend pour un grand producteur de film... Tu piges?  

-Ou...oui.... J’ai c... compris...  

-Secundo, la cassette que tu as fait de Masahiko alors qu’il prenait sa douche, tu l’as toujours?  

-Oui, mais...  

-T’en as fait des copies?  

-Non...  

-Tu dis la vérité ou tu me mens?, fis-je d’une voix plus dure.  

-Je jure devant Bouddha que je te dis la vérité!  

-Alors tu vas la lui donner! Ton petit chantage à la con c’est terminé. T’as compris?  

-A...A la premiPre heure demain je la lui donnerai... Mais sans cette cassette, il n’y aura plus aucun moyen de le convaincre de jouer Masami...  

-Ce sera à lui de décider!, grognai-je. Une dernière chose: j’ai appris que tu avais tenté de tourner une scène de nu avec Shion Wakanaé?  

-C’était pour le film... On aurait fait ça dans le respect, je te jure!  

-Est-ce que tu réalises, espèce de gros lard, que cette fille est encore mineure? Que toi, un universitaire, tu cherches à faire du nu avec une lycéenne, peut être considèré comme de la porno juvénile?? Ça n’a jamais traversé son esprit, ou alors tu cherchais seulement un fantasme pour satisfaire tes nuits en solitaire?  

-Ce n’était pas une scène de sexe, protesta Lee d’un air scandalisé, juste du nu... il n’y avait aucun mal la de–  

 

Je perdis contenance et sans réfléchir, je lui donnai une bonne paire de gifles d’une main tremblante avant d’ajouter:  

 

-Tu comprends pas que le simple fait d’avoir une photo d’adolescente nue dans ton ordinateur peut t’envoyer illico en taule? Pauvre crétin!  

 

Lee me regarda avec des yeux tellements ronds que j’ai cru un instant qu’ils allaient tomber de leurs orbites. Avec l’expression qui se peignait sur son gros visage rondouillard, je constatait que finalement il commençait à comprendre les conscéquences qu’il aurait pu encourir s’il était parvenu à ses fins. Il leva deux mains tremblantes à la hauteur de son visage et il bredouilla d’une vois tremblotante:  

 

-D’accord, j’ai compris... Je ne vais pas recommencer, je t’assure... tu veux bien me lâcher, maintenant?  

-Une dernière chose, Lee. Demain, toute la famille Wakanaé sera à la maison. Tu vas y aller et tu vas demander pardon à Shion devant tout le monde pour avoir tenté de la filmer en costume d’Eve.  

-Devant toute sa famille?!?! Mais son père va vouloir me crucifier!!  

-Si tu ne le fais pas, Lee, dis-je d’une vois drôlement détendue, Je me chargerai moi-même du travail! Tu as besoin que je te fasse un dessin? C'est tout désigné pour parfaire mon entraînement de Yakuza, tu ne penses pas?  

 

Il hocha nerveusement la tête et je le lâchai enfin. Je me penchai pour ramasser ma canne et, me relevant, je vis Lee qui, blanc comme un drap, s’asseyait à un pupitre, les mains tremblantes. Après avoir pris quelque grandes respirations, il leva les yeux vers moi et il dit:  

 

-J’ai beaucoup de défauts, mais je tiens mes promesses. Demain à la première heure, je remettrai la cassette à Giba et je présenterai mes excuses à Shion devant toute sa famille.  

-Parfait. Maintenant tu me permettras de prendre congé, j’ai des plans pour ce soir, alors je te laisse réfléchir à notre petite conversation. Bonne journée Lee.  

-Merci de ta visite, dit-il en inclinant la tête. Et merci de m’avoir fait comprendre que j’avais mal agi avec Shion-san et Giba. Sois assuré que ça ne se reproduira plus.  

 

Je fis mine de me diriger vers la porte, mais décidai plutôt de revenir sur mes pas et je lui tendis la main. Tout d’abord il hésita, craignant sans doute que j’allais lui foutre une autre paire de baffes, mais réalisant que ce n’était pas mon intention, il la serra vigoureusement en disant:  

 

-C’est dommage que tu ne veuilles pas faire partie du casting, tu aurais été très convaincant dans le rôle de ce Yakusa... T’es certain que tu ne veux pas reconsidèrer mon offre?  

 

Je ne pus m’empêcher d’esquisser un petit sourire en constatant qu’il était vraiment têtu comme une mûle...  

 

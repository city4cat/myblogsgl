
# FC实物集  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96820))  

FC里的某些事物在真实世界里的原型

------------------------  

## 白玉-车站  {#白玉-车站}  

叶子坐电车回奥多摩是在白玉站下车。站牌显示：东京都奥多摩町，白玉/Shiratama。可能对应改造前（2011年）的"白丸站/Shiromaru"(青梅线).

- 站牌：

![](img/14_247_6__.jpg) 
![](img/14_247_6__real.jpg)

>文字对比：

>> 白玉/Shiratama v.s. 白丸/Shiromaru
>> 
>> Hatomune v.s. Hatonosu
>> 
>> Okusama  v.s. Oku-tama

- 站台侧面:

![](img/14_260_0__.jpg)
![](img/14_260_0__real.jpg)

- 站台斜侧面:

![](img/13_096_4__.jpg)
![](img/14_213_5__..jpg)
![](img/14_259_1__.jpg)
![](img/13_096_4__14_213_5__14_259_1__real.jpg)

- 2011年之后的样子:

![](img/after_2011_00.jpg)

参考链接：

1. [这是一座你没见过的，候车室与站名一模一样的车站](https://zhuanlan.zhihu.com/p/61501414)。白丸站位于日本东京都西多摩郡奥多摩町。该站隶属于JR东日本青梅线，编号JC73。于2011年进行的改造中，车站候车室外观被改建为了与站名“白丸”非常契合的白色球体状。

2. [白丸駅 (東京都)](https://ja.wikipedia.org/wiki/白丸駅_(東京都))

3. [白丸駅コンパクト化 | aran.or.jp](http://www.aran.or.jp/works/4812.html)

4. [東京都で最も乗降客数が少ないのがここ！ – 珍鉄](http://www.chin-tetsu.com/article/3333)

5. [JR青梅線 白丸駅/奥多摩周辺/CHSいろいろサイト](https://www.chspmedia.com/ShiromaruSt_Area.html)


------------------------  


## 日式旅馆-叶子家  {#日式旅馆-叶子家}  

叶子家位于奥多摩。下面的这些实景和漫画有些差别，所以不能非常肯定是漫画里的原型。

### 御岳小桥

-

![](img/13_193_5__.jpg)
![](img/13_193_5__real.jpg)

-

![](img/13_099_3__.jpg)
![](img/13_099_3__real.jpg)

参考链接：

1. [多摩川に架かる吊り橋　杣の小橋を河原から見上げる](https://pixta.jp/photo/67270760)

2. [御岳渓谷　御岳小橋 ](https://pixta.jp/photo/54464880)


### 御岳溪谷（御岳渓谷[日] / Mitake Valley[英]）

推测叶子家在御岳小桥附近，而御岳小桥的位置在"御岳溪谷"。

- 交通图, ([链接](https://www.omekanko.gr.jp/wph8gq9w/wp-content/uploads/2019/09/442db3ee0591faae0c71ddab7566c3d2.pdf))：

![](img/mitake_valley_map_0.jpg)

- 海鸥地图, ([链接](https://cn.gullmap.com/jp))：

![](img/mitake_valley_map_1.jpg)

可以看出：

- 附近有：御嶽駅(车站-JR青梅線), 青梅街道, 御岳橋, 吉野街道, 玉堂美術館(Gyokudo Art Museum), 御岳小橋, 多摩川, 河鹿園(Kajikaen), 東峯園, Tohoen(稻农园)
    
- 地图显示御岳小桥无法通行，因为被水冲毁了([洪水で壊された御岳小橋-撮影时间:2020/06/20](https://pixta.jp/photo/66789075))：
    
![](img/13_193_5__real_broken.jpg)
    
- 旅馆门前的河流是"多摩川"
      
- 河鹿園一侧的河边多是红叶树，河鹿園对面的河边多是黄叶树。

- 此外，水从"御岳桥"流向"御岳小桥"，
[视频-御岳渓谷(Mitake Gorge)-bilibili](https://www.bilibili.com/video/av21563084)


### 视角从"御岳桥"到"御岳小桥" ：

![](img/kajikaen_bridge.jpg) [实物链接](https://pixta.jp/photo/36489791)

![](img/13_100_5__real_0.jpg) [实物链接](https://pixta.jp/photo/45937500)

![](img/13_100_5__real__kajikaen_bridge_small.jpg)  [实物链接](https://pixta.jp/photo/45937497)

![](img/kajikaen_bridge_small.jpg) [实物链接](https://pixta.jp/photo/45687604)：


### 河鹿园

叶子家旅馆的样子和大概位置，推测叶子家的旅馆可能是河鹿园：

-

![](img/13_100_5__.jpg)
![](img/13_100_5__real_1.jpg) [实物链接](https://pixta.jp/photo/45937487)。

-

![](img/13_130_2__.jpg)
![](img/13_145_1__.jpg)
![](img/13_152_6__.jpg)

下图能看出溪水的位置和院子入口：

![](img/13_189_2__.jpg)

入口处门的实物：

![](img/13_189_2__real.jpg)


叶子家的旅馆客房有：  竹之间（13_107_3），兰之间(13_127_0)，杉之间(13_127_1)，


### 叶子家附近的一处景

13_090，13_099，13_160，这三个画面应该是同一处景。视点可能是在御岳小橋，视角可能是朝向河流下游：

![](img/13_090_4__.jpg)
![](img/13_099_0__.jpg)
![](img/13_160_0__.jpg)

我猜这处景的实物是：

![](img/13_090_4__13_099_0__13_160_0__real_0.jpg) [实物链接](https://pixta.jp/photo/54816885)

![](img/13_090_4__13_099_0__13_160_0__real_1.jpg) [实物链接](https://pixta.jp/photo/54816886)


### 河鹿园的美食：

[山里会席料理旅館　河鹿園  （奥多摩御岳橋畔）](http://mokuzoken.com/S00088_01.php)

- 在叶子家旅馆喝的啤酒：Asahi Super Dry

![](img/13_148_5__.jpg)
![](img/13_148_5__real.jpg) [实物链接](https://it.wikipedia.org/wiki/Asahi_Breweries)

- 13_117，13_123，叶子家旅馆的美食：  
![](../fc_food/img/13_117_4__.jpg) 
![](../fc_food/img/13_123_5__.jpg)  



### 河鹿园其他信息

1. 御岳溪谷的河鹿园已申请并成为日本的文化遗产。

[新闻-2020年6月8日](https://www.asahi.com/articles/ASN677FLJN66UTIL00Y.html). "沿着东京青梅市的御岳溪谷，持续近100年的元料亭旅馆"河鹿园"被登记为国家注册有形文化财产（建筑物）...最古老的建筑是1925年（大正14年）左右的书店和主楼。 从奥多摩观光的休息所开始，在30年（昭和5年）左右建造了数寄屋座敷的"山鱼楼"、磨练原木吸引眼的"溪梅亭"、拥有优质客房的"枕流亭"、巧妙的创作作品"射山庄"。 建材主要是屋久杉，用平板玻璃窗和拉门的码头来表现"三保松原"等古朴的房间...然而，由于没有继任者，旅馆在三年前的春天停业了。 他把书本、鲜花和收集到的书画改成"博物馆"，在房间的地板之间装饰。"


------------------------  


## 多摩川浅間神社(Sengen Shrine)

叶子姓氏“浅冈”。家在奥多摩，据猜测是御岳渓谷，门前的河流是"多摩川"。此外，有[多摩川浅間神社(Sengen Shrine)](https://pixta.jp/photo/64779711)，在多摩川的下游，距离御岳渓谷直线距离约50km。不知道叶子的姓氏和浅間神社是否有关系。

![](img/sengen_shrine_map.jpg) 
![](img/sengen_shrine.jpg)


------------------------  

## 高知风景-室户岬(高知県室戸岬[日])

07_065，

![](img/07_065_3__.jpg) 
![](img/07_065_3__real.jpg) [实物链接](https://pixta.jp/photo/68730508)


------------------------  

## 高知风景-"龙河洞"

07_066，FC里是“龙牙洞”。实际指的应该是高知县"龙河洞"（香美市）：

![](img/07_067_3__.jpg) 
![](img/07_067_3__real.jpg) [实物链接](https://www.517japan.com/viewnews-77908.html)


------------------------  

## 高知风景-高知城

- 

![](img/07_004_1__.jpg) 
![](img/07_004_1__real.jpg) [实物链接](https://baike.baidu.com/pic/高知县/6618091)

![](img/07_008_3__.jpg) 
![](img/07_008_3__real.jpg)

新版FC第三卷封底的取景地照片应该也是它：   
![](img/fc-new-edition-3.jpg)

- 07_004，俯瞰高知：

![](img/07_004_0__.jpg)
![](img/07_004_0__real.jpg) [实物链接](https://baike.baidu.com/pic/高知县/6618091)


------------------------  

## 高知风景-多宝塔（日文：室戸岬にある霊場-最御崎寺(多宝塔))

07_065, FC里称为 神社/神庙(shrine)：

![](img/07_065_4__.jpg)
![](img/07_065_4__real.jpg) [实物链接](https://pixta.jp/photo/68729094)


------------------------  

## 高知风景-桂浜(Katsurahama)  {#高知风景-桂浜}  

14_276，大结局里雅彦表白紫苑的海滩应该是高知县高知市的"桂浜(桂滨)公园"里的桂浜海岸。

### 桂浜公园地图[5]：

![](img/Katsurahama_map.jpg)

### 高知机场 --> 桂浜水族館

用[地图](https://cn.gullmap.com)查询“高知机场”到“桂浜水族館(Katsurahama Aquarium)”的驾车路线(约16分钟)：

![](img/kaochi_airport__aquarium_00.jpg) 
![](img/kaochi_airport__aquarium_01.jpg) 
![](img/kaochi_airport__aquarium_02.jpg) 
![](img/kaochi_airport__aquarium_03.jpg) 
![](img/kaochi_airport__aquarium_04.jpg) 
![](img/kaochi_airport__aquarium_05.jpg) 
![](img/kaochi_airport__aquarium_06.jpg) 
![](img/kaochi_airport__aquarium_07.jpg) 

### 桂浜海岸(Katsurahama Beach)[2,3]

![](img/14_276_4__.jpg)  
![](img/14_276_4__real2.jpg)  

### 山上的亭子(Ryuogu Shrine)[5]：

![](img/14_278_4__.jpg) 
![](img/14_278_4__real.jpg)


### 坂本龙马铜像

同样在上图的小山上有坂本龙马铜像(Ryoma Sakamoto Bronze Statue)，新版FC第5卷封面的取景地就是它：  
![](img/fc-new-edition-5.jpg)


### 桂浜水族館(Katsurahama Aquarium)[3,5]:

![](img/14_279_0__.jpg) 
![](img/14_279_0__real_0.jpg)  

![](img/14_292_4__.jpg) 
![](img/14_292_4__real_1.jpg)  

### 背景里的大石头[3]：

![](img/14_287_4__.jpg) 
![](img/14_287_4__real.jpg)  

参考链接：  

1. [百度百科-高知县-桂滨](https://baike.baidu.com/item/高知县#7_1)  
2. [百度百科-高知县](https://baike.baidu.com/pic/高知县/6618091)  
3. [Bing图片-桂浜](https://cn.bing.com/images/search?q=桂浜)  
4. [日本四国旅游攻略：高知](https://jp.hjenglish.com/new/p440722/) "位于高知市浦户桂浜公园境内有一片濒临太平洋的桂浜海岸"。  
5. [桂浜公園．日本名海岸百選．坂本龍馬銅像](https://www.travalearth.com/post-31405926/)  
6. [高知县十大人气景点](https://www.517japan.com/viewnews-77908.html)  


------------------------  

## 高知特色料理-皿钵料理(さわちりょうり)(大凉盘)

14_273，下飞机后，紫苑提议在高知市吃过午饭再去，天黑前到就可以了。中文注释提到：杂锦锅是高知的特色料理。这可能对应于高知县参考资料里的“皿钵料理(さわちりょうり)(大凉盘)”[1,2,3,4]，理由是: 1.“凉盘“对应故事里的夏天;2.“大”对应紫苑说的“分量太大”。

![](img/kochi_sawachi_01.jpg) [2]

![](img/kochi_sawachi_02.jpg) 
![](img/kochi_sawachi_03.jpg) 
![](img/kochi_sawachi_04.jpg) [3]

![](img/kochi_sawachi_00.jpg) [4]

参考链接：

1. [百度百科-高知县](https://baike.baidu.com/item/高知县#9_1)  
2. [百度百科-皿钵料理](https://baike.baidu.com/item/皿钵料理)  
3. [皿鉢（さわち）- 土佐料理 司 高知本店](https://kazuoh.com/sawachi/)  
4. [皿鉢料理（さわちりょうり）とは？](https://medakasuisan.com/sea-food/post-3655/)  


------------------------  

## 高知风景 - 津吕山

07_064, 空户阿南海岸国定公园 津吕山(高冈园地)


------------------------  

## 高知风景-爷爷家

- 附近钓鱼:  
04_063，04_067，04_068，04_069

- 附近 - “四万十川”:  
03_162_,03_171

参考资料：  
1. [百科 - 四万十川](https://baike.baidu.com/item/四万十川/1770677)


------------------------  

- 高知空港
06_186_3

- 其他：
06_166_0
06_189_1


------------------------  

# 东京

------------------------  


## 若苗家的房屋  
![](img/02_060_0__.jpg)  
从门前的灌木丛和围墙猜测，可能是"杂司谷旧宣教师馆(McCaleb Old Missionary House / Zoshigaya Missionary House Museum)"(杂司谷1丁目25-5):  
![](img/02_060_0__real_map.jpg)  

旧宣教师馆大门口(大门朝北)：  
![](img/02_060_0__real_1-chome-25-5_label.jpg) 
![](img/02_060_0__real_1-chome-25-5_porch.jpg) 
![](img/02_060_0__real_1-chome-25-5_porch-2.jpg) 
![](img/02_060_0__real_1-chome-25-5_porch-3.jpg) 

旧宣教师馆后院(视角朝南):  
![](img/02_060_0__real_1-chome-25-5_yard_south.jpg) 

旧宣教师馆室内照片如下，透过窗子可以看到旧宣教师馆东西两侧的建筑(邻居?)：  
![](img/02_060_0__real_1-chome-25-5_inner_0.jpg) 
![](img/02_060_0__real_1-chome-25-5_inner_1.jpg) 
![](img/02_060_0__real_1-chome-25-5_inner_2.jpg) 
![](img/02_060_0__real_1-chome-25-5_inner_3.jpg) 
以上这些建筑的屋顶的风格类似FC里若苗家大门对面的邻居（只是类似，目前还没有找到实物）：  
![](img/09_064_2__.jpg) 
 

目前(2021年)旧宣教师馆北侧的邻居是一块空地；从google map上可以看到其2008年是如下的民居：  
![](img/1-chome-25-5_north0.jpg) 
![](img/1-chome-25-5_north1.jpg) 

旧宣教师馆南侧的邻居是“平井医院”：  
![](img/1-chome-25-5_sourth.jpg) 

旧宣教师馆到鬼子母神前站约720m(步行约8min):  
![](img/02_060_0__real_to_station.jpg)  


## 若苗家附近的建筑  
不知道是否能找到对应实物：  
![](img/09_028_4__.jpg) 
![](img/09_064_2__.jpg)  

附近的一个十字路口：  
![](img/09_182_0__.jpg) 
![](img/09_182_0__1.jpg)  

14_006，雅彦和紫苑回家路上的街景。左边的建筑有斜角：  
![](./img/14_006_5__.jpg)  


------------------------  

## 若苗家附近的公园-池袋公园  
仁科约雅彦6:00以女装在池袋公园见面(10_160_2)

10_160_6，10_161_1，10_161_2，10_161_4

现在池袋公园的喷泉变样了。


------------------------  

## 若苗家附近的一个公园

第5卷32话：  
![](img/05_088_2__.jpg) 
![](img/05_088_4__.jpg) 
![](img/05_088_6__.jpg) 
![](img/05_089_0__.jpg) 
![](img/05_089_4__.jpg) 
![](img/05_090_3__.jpg) 
![](img/05_090_6__.jpg) 
![](img/05_090_5__.jpg)  

第5卷33话：  
![](img//05_122_4__.jpg) 
![](img//05_123_5__.jpg) 
![](img//05_124_2__.jpg) 
![](img//05_127_5__.jpg) 
![](img//05_127_6__.jpg) 
![](img//05_128_2__.jpg) 
![](img//05_128_5__.jpg) 
![](img//05_130_4__.jpg) 
![](img//05_130_5__.jpg)  

我猜测这个公园是"杂司谷1丁目公园"; "F.COMPO応援隊"里考证[1]这个公园是南池袋第二公園。  
  
- 杂司谷1丁目公园  
    地图：  
    ![](img/02_060_0__real_map.jpg)  

    红框里的路灯一致：  
    ![](img/05_090_5__real_1-chome_park0.jpg) 
    ![](img/05_090_5__real_1-chome_park1.jpg) 

    公园里的卫生间与情节12_112_4对应(紫苑放学到家前去公共卫生间换装):  
    ![](img/12_112_4__.jpg)  
    但这个论据可信度不高，因为好像每个公园都有卫生间。  


- "F.COMPO応援隊"里考证[1]这个公园是"南池袋第二公園(Toshina Kuritsu Minaniikebukuro Daini Park)"   
    地址：東京都豊島区南池袋4-8-5。  
    ![](img/Daini_Park.jpg)  

    - 对应的实物-台阶：  
    ![](img/05_088_2__.jpg) 
    ![](img/Daini_Park.jpg)  

    - 对应的实物-背景里的楼：  
    ![](img/05_088_4__.jpg) 
    ![](img/05_088_4__Daini_Park_01.jpg) 
    ![](img/05_088_4__Daini_Park_03.jpg)  

    - 对应的实物-大树：  
    ![](img/05_088_4__.jpg) 
    ![](img/05_090_3__.jpg) 
    ![](img/05_122_4__.jpg) 
    ![](img/05_124_2__.jpg) 
    ![](img/05_127_6__.jpg) 
    ![](img/05_128_2__.jpg) 
    ![](img/05_128_5__.jpg) 
    ![](img/05_130_4__.jpg) 
    ![](img/05_130_5__.jpg)  
    ![](img/05_130_4__daini2.jpg)[2] 
    ![](img/05_088_4__Daini_Park_02.jpg) 
    ![](img/05_088_4__Daini_Park_03.jpg) 
    ![](img/05_088_4__Daini_Park_04.jpg) 
    ![](img/05_088_4__10252007256.jpg)[3]  

    - 对应的实物-树下的凳子：   
    ![](img/05_130_4__.jpg) 
    ![](img/05_130_4__daini2.jpg)[2]  

    - 对应的实物-树旁边的长椅子：  
    ![](img//05_122_4__.jpg) 
    ![](img//05_123_5__.jpg) 
    ![](img//05_127_5__.jpg) 
    ![](img/05_090_5__Daini_Park_05.jpg)  
    ![](img/05_088_4__10252007256.jpg)[3] 
    ![](img/05_088_4__Daini_Park_02.jpg) 
    
    - 对应的实物-地面上的拱形栏杆：   
    ![](img/05_090_5__.jpg) 
    ![](img/05_090_5__Daini_Park_05.jpg)  

    - 该实景的其他图片：  
    ![](img/Daini_Park_00.jpg)  

**参考资料**：  
[1]. [设定写真集1 - F.COMPO応援隊(整理备份)](../zz_www.biglobe.ne.jp_julianus/pictures/pictures1.md)  
[2]. https://www.city.toshima.lg.jp/340/shisetsu/koen/028.html  
[3]. https://amanaimages.com/info/infoRF.aspx?SearchKey=10252007256  


------------------------  

## 若苗家附近的公园-杂司谷2丁目公园  



------------------------  

## 若苗家附近-鬼子母神附近  {#若苗家附近-鬼子母神附近}  

### 若苗家附近-神社  {#若苗家附近-神社}  
![](img/08_163_0__.jpg)  
![](img/08_163_0__real_0.jpg)  
远景:  
![](img/08_163_0__real_1.jpg)  

公孙树(门形红色建筑为"武芳稻荷")：  
![](img/08_163_1__.jpg)  
![](img/08_163_1__real_0.jpg)  
![](img/08_163_1__real_1.jpg)  
![](img/08_163_1__real_2.jpg)  

### 若苗家附近-鬼子母神表参道/鬼子母神西参道  {#若苗家附近-鬼子母神表参道}  
![](img/kishimojin_map.jpg) 

![](img/02_155_4__.jpg) 
![](img/02_157_2__.jpg) 
![](img/03_022_2__.jpg) 
![](img/03_023_2__.jpg) 
![](img/08_165_0__.jpg)  

![](img/02_155_4__real_0.jpg) 
![](img/02_155_4__real_1.jpg) 
![](img/02_155_4__real_2.jpg)  

### 若苗家附近-鬼子母神附近的公园(Mimizuku)  {#若苗家附近-鬼子母神附近的公园}  

![](img/3chome-15-20_Mimizuku_0.jpg) 
![](img/3chome-15-20_Mimizuku_1.jpg)  


------------------------  

## 若苗家附近-天桥  
- 13_064，13_065，雅彦和紫苑在放学路上会经过这里。从两边的栏杆看，这应该是一座天桥：  
![](./img/13_064_5__.jpg)
![](./img/13_064_6__.jpg)
![](./img/13_065_4__.jpg)

![](img/01_046_4__.jpg)  

天桥的实物为杂司谷3丁目1-7-5(3-chome-1-5),3丁目1-7(3-chome-1-7):  

![](img/01_046_4__real.jpg) 

![](img/01_046_4__13_064_5__13_064_6__real.jpg)  

另， 右上角蓝框里的柱子可能是下图里的实物：  
![](img/13_064_5__.jpg) 
![](img/13_064_6__.jpg)  


------------------------  

## 若苗家附近-电车站  
若苗家电车站(鬼子母神前站?)处(2丁目-8-8)，铁轨方向正对着"太阳城60"的正面:  
![](../fc_details/img/01_003_1__.jpg)  
![](img/01_003_1__real.jpg)  

电车站十字路口：  
![](img/01_013_6__.jpg)  
![](img/01_013_6__real.jpg)  


------------------------  

## 若苗家附近-墓地(杂司谷1丁目39-6)  
![](img/1-chome-39-6.jpg) 
![](img/1-chome-39-6_1.jpg) 
![](img/1-chome-39-6_2.jpg) 


------------------------  

## 柳叶家的房屋


------------------------  

## 真朱家（熏，辰巳）

07_162_3，07_163_1，
附近有路牌(07_163_2)，看起来像"二丁目(或三丁目)"

07_163_3，07_163_5


------------------------  

## 江岛的住处


------------------------  

## 和子家  {#和子家}  
- 08_111, 08_121, 08_122. 和子家(102房间(08_111_2)), 及其附近:  
![](./img/08_111_1__.jpg)
![](./img/08_111_2__.jpg)
![](./img/08_121_5__.jpg)
![](./img/08_122_3__.jpg)

在1991年版的《东京爱情故事》里"关口里美"的住所类似和子的住处：

室外：  
![](../fc_details/img/08_111_1__.jpg) 
![](img/08_111_1__real_tokyo_love_story_1991_ep07.jpg) 

室内：  
![](img/08_111_3__.jpg) 
![](img/08_111_4__.jpg) 
![](img/08_121_6__.jpg) 
![](img/08_111_3__real_tokyo_love_story_1991_ep02.jpg) 
![](img/08_111_3__real_tokyo_love_story_1991_ep05.jpg) 
![](img/08_111_3__real_tokyo_love_story_1991_ep09.jpg) 


------------------------  

## 公园 - 影研社拍戏的公园

![](img/14_125_5__.jpg)  
![](img/14_230_2__.jpg)  
FC里这个公园的栏杆一直都是画的这样子，所以应该没有画错。  

以栏杆和喷泉为线索，我找到了两个公园有这样的喷泉和类似的栏杆：  头恩赐公园, 碑文谷公园。  
    - "头恩赐公园"（好像也称"头公园"）：  
![](img/14_125_5__14_230_2__real.jpg)   [实物链接](https://pixta.jp/photo/66069044)  
![](img/14_125_5__14_230_2__real2.jpg)   [实物链接](https://pixta.jp/photo/38511279)  

    Bilibili有一个[NHK关于井之头公园的纪录片](https://www.bilibili.com/video/BV1sA411n7Nw)，片子28:17处有更近似的栏杆:  
![](img/14_125_5__14_230_2__real4_nhk_inokashira_park.jpg)

    - 碑文谷公园：  
![](img/14_125_5__14_230_2__real3.jpg)   [实物链接](https://pixta.jp/photo/42603267)  
    碑文谷公园水边的栏杆差异比较大：[碑文谷公園](https://pixta.jp/photo/12954865)  

参考资料：  

1. [万维百科-井之头恩赐公园](https://wanweibaike.com/wiki-井之頭恩賜公園)。井之头恩赐公园(Inokashira Park)是位在东京都武蔵野市和三鹰市的都立公园。属于恩赐公园，统称"井之头恩赐公园"。  
2. 更多头恩赐公园的图片: [頭公園 東京](https://pixta.jp/tags/頭公園 東京), [頭恩賜公園 東京](https://pixta.jp/tags/頭恩賜公園 東京)  
3. 更多碑文谷公园的图片: [碑文谷公園](https://pixta.jp/tags/碑文谷公園)  


------------------------  

## 公园 - 熏和雅彦第一次见面

07_133_1, 07_133_2
07_134_0
07_136_3, 07_136_4
07_137_0
07_149_4,


------------------------  

## 高楼

### 新宿 高楼

![](img/shinjuku_buildings.jpg)  
![](img/shinjuku_buildings_2.jpg)  

住友大厦的横截面是六边形(或者说是三棱形)。  

实物链接：  
    - [新宿三井大厦](https://cn.tripadvisor.com/Attraction_Review-g14133673-d1373759-Reviews-Shinjuku_Mitsui_Building-Nishishinjuku_Shinjuku_Tokyo_Tokyo_Prefecture_Kanto.html)   
    - [东瀛归来(八)：都厅45层俯瞰东京](https://yjx1963.blog.sohu.com/219096621.html)   
    - [新宿三井大厦（Shinjuku Mitsui Building）](https://zh.wikipedia.org/zh-hans/新宿三井大厦)    
    - [新宿 住友大厦 （Shinjuku Sumitomo Building）](https://zh.wikipedia.org/wiki/新宿住友大厦)    
    - [新宿 中心大厦](https://zh.wikipedia.org/wiki/新宿中心大厦)    
    - [京王廣場大飯店（Keio Plaza Hotel）](https://zh.wikipedia.org/wiki/京王廣場大飯店)  
    - 更多图片: [新宿三井ビル](https://pixta.jp/tags/新宿三井ビル), [住友ビル](https://pixta.jp/tags/住友ビル)  


------------------------  

### 若苗家附近的高楼

- 这个建筑的出镜频率很高。（从01_003_1看，若苗家电车站(鬼子母神前站?)处，铁轨方向正对着建筑物的正面）  
![](../fc_details/img/01_003_1__.jpg) 
![](../fc_details/img/01_016_1__.jpg) 
![](../fc_details/img/01_154_1__.jpg) 
![](../fc_details/img/05_038_2__.jpg) 
![](../fc_details/img/05_088_0__.jpg) 
![](../fc_details/img/06_007_0__.jpg) 
![](../fc_details/img/06_010_0__.jpg) 
![](../fc_details/img/06_013_0__.jpg) 
![](../fc_details/img/06_087_3__.jpg) 
![](img/07_128_0__.jpg)  

FC里第一话开篇，这座楼正对着电车站(应该是若苗家附近的鬼子母神前站)的铁轨。从东京交通地图里可以看出，这段铁轨向南几乎正对着新宿的几个著名高楼(新宿三井大厦、住友大厦，等)。但漫画里铁轨正对的只有一座高楼、而不是几座高楼。所以，镜头里的铁轨应该是向北正对着这座高楼。  
这座高楼应该是"太阳城60(サンシャイン60/Sunshine 60 Tower/池袋大厦)"[1]，也被称为"SunShineCity"[2]:  
![](img/07_128_0__real_sunshine60.jpg)  
从东京交通地图里可以看出, 该楼位置符合上述推测：  
![](img/07_128_0__real_sunshine60_map0.jpg)，![](img/07_128_0__real_sunshine60_map1.jpg)  
太阳城60、新宿三井、鬼子母神前站的相对位置：  
![](img/07_128_0__real_sunshine60_map2.jpg)，

从杂司谷2丁目公园看：  
![](img/07_128_0__real_sunshine60_2chome_park0.jpg) 
![](img/07_128_0__real_sunshine60_2chome_park1.jpg) 
![](img/07_128_0__real_sunshine60_2chome_park2.jpg) 
![](img/07_128_0__real_sunshine60_2chome_park3.jpg) 
![](img/07_128_0__real_sunshine60_2chome_park4.jpg) 
![](img/07_128_0__real_sunshine60_2chome_park5.jpg) 


参考资料：  

1. [太阳城60-摩天档案馆](https://www.motianguan.com/wiki/太阳城60)。太阳城60是一座60层的多用途摩天大楼，位于东京都岛池袋市，毗邻太阳城综合体。在1978年竣工时，这座239.7米的建筑是亚洲最高的建筑，直到1985年被首尔的63大厦超越。太阳城60大楼最高的60层设有需收费参观的屋顶展望台，其高度为226.3米，是东京都的高层建筑展望台中最高的。除了展望台本身，到了周六、周日以及节庆假期，尚会开放天空平台供民众参观。
2. "JAPAN MAP" App里其名字为：SunShineCity。紧邻东池袋中央公园。


------------------------  

### 街道

第47话，熏冒名雅彦玩弄女生。雅彦、紫苑、江岛商量后决定，在假雅彦经常勾搭的那条街上(07_126_0)，由雅彦异装作诱饵。这条街的样子：07_125_1，07_125_2，07_125_4，07_126_0


------------------------  

### 东京-表参道(Omotesando)  

雅彦和叶子第一次圣诞夜约会时，这里应该是东京的表参道：  
![](img/05_098_1__.jpg)  
![](img/05_098_1__real_omotesando.jpg)  
(图片源自[东京爱情故事30年：恋爱教科书还藏着旅行宝典](https://www.latiaozixun.net/20210312/644146F9E8066EF3A5F7E7A139CB3C6C-1.html?li=BBOXPUT))  


------------------------  

## 日式旅馆-若苗家庭旅行

第2卷，若苗家庭旅行，去的日式旅馆“梁汤”（02_055）。尚未找到实物。


------------------------  

## 日式旅馆-江岛的一个打工地点

第4卷，江岛的一个打工地点“民宿渔火”（04_088_1）, 海滩附近有个岬角，是自杀圣地（04_086_2）。 可能是日本第一大自杀圣地：东寻坊。

04_098_0


------------------------  

## 日式旅馆-影研社暑假特训（第7卷）

暑假刚开始时（07_090_4）影研社循例举行暑期特别宿营，今年地点是：轻井泽附近的这个（混浴(07_090_1)）温泉旅馆（07_091_0）。


------------------------  

## 日式旅馆-紫苑毕业旅行（第11卷）

（万津屋旅馆）

![](img/11_160_3__.jpg)  
实际类似的旅馆：  
![](img/11_160_3__real_tokyo_love_story_1991_ep05.jpg)  
(图片源自《东京爱情故事》(1991)第五集)  


------------------------  

## 现代风格旅馆-空外出采风（卷10）


------------------------  

## 酒馆

06_148, 空回高知和宪司喝酒，在"猫又"局酒屋。乐亭（03_195_2）


------------------------  

### 紫的驾照信息

紫的驾照：

![](../fc_details/05_042_1__.jpg)

姓名：若苗 龙彦  
生日：昭和36年(1961年)2月x日  
本籍：东京都 三鹰市 深大寺 6-18-8  
住所：东京都 丰岛区 杂司谷 4-24-15  
交付：平成07年(1995年)02月11日 114622  
有效期：平成12年持证人的生日前  

英文：
Name：Wakanae Tatsuhiko
生日：D.O.B 24年2月x日
Born：Tokyo-Mitaka-Jindaiji-6-18-8 (注：英文版将"三鹰市"翻译为Marita, 应为错误)  
Address：Tokyo-Toshima(或Toyoshima)-Zoshigaya-4-24-15 (注：英文版将"杂司谷"翻译为Zoshijiya, 应为错误)  
Issued：11/02/1996 114622
Date of Expiry：11/02/2001


------------------------  

- **飲料-New Calorie Mate**  
    - 03_004_0, 雅彦喝的饮料: New Calorie Mate  
    
        ![](img/CALORIE-MATE-Energy-Drink.jpg) 
         
        CALORIE MATE能量饮料咖啡馆Au Lait。 Calorie Mate是一种营养均衡的食品，旨在为当今人们的忙碌生活提供饮食支持。 它可以在任何地方饮用，并提供人体所需的营养。 包含人体所需的蛋白质、脂肪、11种维生素和6种矿物质的良好平衡。 （饮料类型含有10种维生素和5种矿物质）
        
        生产商：Otsuka Japan  
        产地：日本  
        价格：48.5美元/200ml*6罐，(约8美元/罐)  
        
        参考资料：
        [Calorie Mate Energy Drink Cafe Au Lait 200mlx6 cans-Made in Japan](https://www.takaski.com/product/calorie-mate-energy-drink-cafe-au-lait-200ml-x-6-cans-made-in-japan/)

------------------------  

- **旅館-万津屋**  
紫苑高中毕业旅行时下榻的旅馆:伊万津(Imazu)车站(11_159_1)附近的万津屋旅馆(11_160)。  

------------------------  

- **伊万里**  

    06——035，辰巳设陷阱，让雅彦打破"伊万里的古董"花瓶（其实是赝品）。  
    
    注：位于佐贺县东松蒲半岛，历史悠久，陶瓷制品尤其闻名。


------------------------  

- **飲料-啤酒**  

    07_178_6。熏家里的啤酒是：Heinebn Premium Beer All Malt 100% . 可能指的是Heineken
    
    12_067_0，成人礼当天凌晨，雅彦和江岛在雅彦卧室里喝的各种酒。


------------------------  

- **飲料-咖啡**  
    - 12_148，Blue Thunder, UGG Coffee

        ![](img/12_148_3__.jpg)
        
        网上可以查到Klatch Coffee的Blue Thunder咖啡。但我想这里暗指的是不是：UCC(悠诗诗)的Blue Mountain（蓝山）咖啡？
        
        UCC咖啡是以定点精心培养、种植的咖啡豆为原料，由日本UCC上岛咖啡株式会社生产、销售的世界著名品牌咖啡。UCC在牙买加生产世界顶级的Blue Mountain（蓝山）咖啡。
        
        参考链接： 
        
        [百度百科-UCC咖啡](https://baike.baidu.com/item/UCC咖啡/)
        

    - 12_173_1，12_173_2， 盲人乐队喝的饮料。


------------------------  

- **其他**

    - 12_161，地铁浜名湖出站口：
    
        ![](img/12_161_1__.jpg)
        
        浜名湖（Lake Hamana），中文也称为：滨名湖.
        
        参考链接： 
        
        [浜名湖/滨名湖](https://baike.baidu.com/item/浜名湖/10158026)
        
    
    - 12_163，忠犬八公像：      
        ![](img/12_163_7__.jpg)
        ![](img/12_163_7__cn.jpg)
        ![](img/12_163_7__en.jpg)
        
        忠犬八公像，是日本著名的碰头地标志物，位于日本涩谷车站检票口外。  
        
        参考链接：  
        [日本涩谷车站 忠犬八公像](https://m-news.artron.net/news/20110826/n183223.html)  


    - 第9卷/58话"网上情人"里，若苗空工作室的电脑：  
        ![](img/09_055_7__.jpg) ![](img/09_033_3__.jpg)  
        ![](img/09_032_1__.jpg) 
        ![](img/09_037_2__.jpg) 
        ![](img/09_055_5__.jpg) 
        ![](img/09_056_0__.jpg) 
        ![](img/09_056_1__.jpg)  

        应该是1997年产的Apple Power Macintosh 8600或9600：  
        ![](img/09_055_7__real_pm8600.jpg) 
        ![](img/09_055_7__real_pm9600.jpg)  

        这两款电脑的更多信息：  
        [Apple PM 8600](http://www.applemuseum.dk/apple_museum/1997/histo_powermac8600.html)  
        [Apple PM 9600](http://www.applemuseum.dk/apple_museum/1997/histo_powermac9600.html)  

--------------------------------------------------

- 07_157。熏常喝马爹利路易13. 顶级酒，价格约2W RMB。  
![](./img/07_157_1__cn.jpg)  
![](./img/07_157_1__real_Martell.Louis.XIII.jpg)  


- 08_149。叶子看的杂志"non-non"很可能指的是日本时尚杂志"non-no"。 "non-no"是集英社发行的月刊女性杂志。  
![](./img/08_149_0__.jpg)


- 12_032，（紫苑高中毕业履行。三个男生要把雅美灌醉）他们除了喝啤酒之外，还喝烈酒。  
![](../fc_food/img/12_032_3__.jpg) 
![](../fc_food/img/12_032_4__.jpg)  
应该是日本“一甲单一麦芽威士忌-余市12年”(Nikka Single Malt Whisky Yoichi 12 Year Old)，700~750ml，售价约650美元，2016年获得全球威士忌大奖（World Whiskies Awards）的金奖：  
![](./img/12_032_3__nikka-yoichi-12yo-old-bottling_2.jpg)  
参考链接：   
1. [Nikka Yoichi 12 Year Old Single Malt Japanese Whisky (750ml)](https://www.nicks.com.au/nikka-yoichi-12-year-old-single-malt-japanese-whisky-750ml)  
2. [Yoichi 余市](https://www.wine-world.com/winery/yoichi)  


- 12_148，Blue Thunder, UGG Coffee  
![](./img/12_148_3__.jpg)  
网上可以查到Klatch Coffee的Blue Thunder咖啡。但我想这里暗指的是不是：UCC(悠诗诗)的Blue Mountain（蓝山）咖啡？  
UCC咖啡是以定点精心培养、种植的咖啡豆为原料，由日本UCC上岛咖啡株式会社生产、销售的世界著名品牌咖啡。UCC在牙买加生产世界顶级的Blue Mountain（蓝山）咖啡。  
参考链接：  [百度百科-UCC咖啡](https://baike.baidu.com/item/UCC咖啡/)  



- 12_159，映研社的录像机：VHS，*ietor, HR-SD2000。  
![](./img/12_159_2__.jpg)  




- 12_161，地铁浜名湖出站口：  
![](./img/12_161_1__.jpg)  
浜名湖（Lake Hamana），中文也称为：滨名湖.  

参考链接：  [浜名湖/滨名湖](https://baike.baidu.com/item/浜名湖/10158026)  


- 12_163，忠犬八公像：  
![](./img/12_163_7__.jpg) 
![](./img/12_163_7__cn.jpg) 
![](./img/12_163_7__en.jpg)  
忠犬八公像，是日本著名的碰头地标志物，位于日本涩谷车站检票口外。  

参考链接： [日本涩谷车站 忠犬八公像](https://m-news.artron.net/news/20110826/n183223.html)



- 14_018, 14_021，14_022，影研社迎新聚会(仁科)。他们喝的酒：  
![](../fc_food/img/14_018_0__.jpg) 
![](../fc_food/img/14_021_6__.jpg)  
紫苑和江岛拼酒时喝的酒是：  
![](./img/14_022_5__.jpg) 
![](./img/14_022_5__real_Four.Roses.Black.Label.Fine.Old.Bourbon.jpg)  

Four Roses Black Label, Fine Old Bourbon  
![](./img/14_022_0__.jpg) 
![](./img/14_022_0__real_Austin.Nichols.Wild.Turkey.Straight.Bourbon.jpg)  

Austin Nichols Wild Turkey Straight Bourbon  

参考链接：  
1. [Four Roses Black Label 75cl](https://www.kabukiwhisky.com/besides-japan/bourbon-whsiky/category-166/black-pre2002/four-roses-mr-9-2-90255/)  
2. [Austin Nichols Wild Turkey Kentucky Straight Bourbon Whiskey](https://www.oldspiritscompany.com/products/austin-nichols-wild-turkey-kentucky-straight-bourbon-whiskey-bottled-1995-43-4-75cl)  



- 14_206, 雅彦和叶子吵架分手的地方, 新宿地铁站外。  
![](img/14_206_3__.jpg)  
![](img/14_206_4__.jpg)  
实景（图片源自[《北条司研究序说》](http://chiakik.la.coocan.jp/htkj/ch/chwords-places.htm)）：  
![](img/higasiguchi-2.jpg) 
![](img/higasiguchi-3.jpg) 
![](img/higasiguchi-4.jpg) 

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
    - 如有错误，请指出，非常感谢！  


# FC的画风-面部-眼睛(侧面)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96895))  


- **如何描述眼睛**  
侧面眼睛的比例：  
    - 成年：  
        ![](../fc_drawing_style__face/side_img/Drawing.The.Head.hands.p077.female.scaled.jpg) 
        ![](../fc_drawing_style__face/side_img/Drawing.The.Head.hands.p043.male.jpg)  
        

- **FC里眼睛的一些特点**:  
有时角色的眼部会偏上或偏下，我猜，可能是因为摄像机仰视或俯视。  
例如，紫的眼睛(侧面)有符合比例的：  
    ![](../fc_drawing_style__face/side_img/face_side_yukari_basis.png) 
    ![](../fc_drawing_style__face/side_img/face_side_yukari_basis_mark.jpg)  

也有眼部偏上：  
    ![](img/face_side_yukari_basis_down.jpg) 
    ![](img/face_side_yukari_basis_down_mark.jpg)  

还有眼部偏下：  
    ![](img/face_side_yukari_basis_top.jpg) 
    ![](img/face_side_yukari_basis_top_mark.jpg)  


- 07_168。雅美的眼睛和睫毛的这个画法在FC里不常见：把眼睛画成球面，一部分睫毛画在球面内侧(红色)。（FC里常见的画法是图里熏的眼睛和睫毛的样子）  
![](./img/07_168_0__.jpg)  
另一处：（07_175）  
![](./img/07_175_2__.jpg)  


------

    

**参考资料**：  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

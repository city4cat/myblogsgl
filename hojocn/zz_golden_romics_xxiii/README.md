
source
https://aprile2018.romics.it/en/ospiti_23ma_edizione

https://aprile2018.romics.it/en/ospiti/tsukasa-hojo-golden-romics-xxiii-edition

![](https://aprile2018.romics.it/sites/default/files/_rnz3662_0.jpg)  
([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96826))  

# Tsukasa Hojo Golden Romics of the XXIII edition
第23届Golden Romics之北条司

Famous creator of the manga series Cat’s Eye, City Hunter, and Angel Heart, Hojo has been passionate about comics since childhood thanks to his relationship with a friend who aspired to become a professional cartoonist. After his debut in 1980 as a professional mangaka on Shonen Jump with the piece Ore wa Otoko da!, he began a close collaboration with Nobuhiko Horie that would lead him to the publication of Cat's Eye in 1981. The piece, focusing on the stories of three able and agile thieves chasing down works of art, immediately conquered the public and brought him immediate popularity, both for the themes addressed and for the, then unique, choice to present female characters as protagonists in a market where the female figure was relegated to a secondary role. The success was huge: in 1983 Tokyo Movie Shinsha would go on to create an anime that would be broadcasted in Italy in 1985, as well as in France, Spain, Germany, Brazil, China and the Philippines. In 1985 he debuted the investigative series City Hunter on the Shonen Jump. Its success at home was even greater than that of Cat’s Eye and definitively established Hojo as the leading artist of Japanese comics. In 1987 the Sunrise studio created an anime series of City Hunter which was exported to dozens of countries. In 2001, based on the consolidated success of City Hunter, Hojo created Angel Heart, a spin-off of the series set in a parallel future. Angel Heart also resulted in an anime series by TMS Entertainment and broadcasted in Japan from 2005 to 2006, though it is currently unpublished in Italy.  
《猫眼》、《城市猎人》、《天使之心》等系列漫画的著名作者，由于与一位立志成为职业漫画家的朋友的关系，北条从小就热衷于漫画。1980年，他在《少年Jump》上以作品《Ore wa Otoko da！》作为职业漫画家出道，之后，他与堀江信彦开始密切合作，并于1981年出版了《猫眼》。这部作品聚焦于三个能干敏捷的盗贼追回艺术品的故事。该漫画立即征服了公众，并很快为他带来了人气，这既是因为作品所涉及的主题，也是因为当时独特的选择，即在女性形象被贬为次要角色的市场中，将女性角色作为主角。取得了巨大的成功：1983年东京电影Shinsha社将继续创作动画，1985年在意大利以及法国、西班牙、德国、巴西、中国和菲律宾播出。1985年，他在《少年Jump》上首次推出了侦探系列《城市猎人》。它在(日本)国内的成功甚至超过了《猫眼》，并最终确立了北条在日本漫画界首屈一指的地位。1987年，Sunrise工作室创作了《城市猎人》系列动画，并出口到几十个国家。2001年，在巩固《城市猎人》成功的基础上，北条创作了以平行世界为背景的系列衍生作品《天使心》。《天使心》也因此被TMS娱乐公司制作成动画系列，并于2005年至2006年在日本播出，不过目前在意大利尚未出版。





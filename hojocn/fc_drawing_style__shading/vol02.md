
# FC的画风-着色(shading) 


目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  

----------------------

## Vol02

----------------------------------------------------------
<a name="ch008"></a>  
### Vol02	CH008	page003		母亲的回忆  	Mother's Gift  {#ch008}  		

- 02_013_5, 侧面眼、鼻的shading：  
![](../fc_drawing_style__face_side2/img/02_013_5__mod.jpg)  

----------------------------------------------------------
<a name="ch009"></a>  
### Vol02	CH009	page031		家庭旅行!！  	Family Trip!  {#ch009}  		

- 02_035_2，回忆画面和紫苑的头发有过渡，头发着浅色。  
![](img/02_035_2__mod.jpg)  


- 02_050_1，02_050_2，02_054_0，按照剧情，车内应该无光或亮度很低。但这里按照有光照来处理。车窗外为黑暗，三个镜头，用不同的方式表现车窗外的黑暗效果。  
![](../fc_drawing_style__gesture/img/02_050_1__.jpg) 
![](../fc_drawing_style__gesture/img/02_051_2__mod.jpg) 
![](../fc_drawing_style__gesture/img/02_054_0__mod.jpg)  


- 02_054_1, 强光：  
![](img/02_054_1__mod.jpg)  

----------------------------------------------------------
<a name="ch010"></a>  
### Vol02	CH010	page059		不请自来的助手  	The Stubborn Assistant  {#ch010}  		  

----------------------------------------------------------
<a name="ch011"></a>  
### Vol02	CH011	page087		仰慕的人  	The One I Admire  {#ch011}  		  

----------------------------------------------------------
<a name="ch012"></a>  
### Vol02	CH012	page115		紫苑的不道德交际!？  	Shion Courtesan!?  {#ch012}  		

----------------------------------------------------------
<a name="ch013"></a>  
### Vol02	CH013	page141		银幕上的紫苑  	The Girl On The Screen  {#ch013}  		

- 02_159_5,02_159_6, 暗室里的荧光效果：  
![](img/02_159_5__.jpg) 
![](img/02_159_6__.jpg) 

----------------------------------------------------------
<a name="ch014"></a>  
### Vol02	CH014	page169		充满回忆的同学会  	A Day To Remember Another Life  {#ch014}  		

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


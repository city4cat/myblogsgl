
# FC的画风-着色(shading) 

目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  


----------------------

## Vol06

---------------------------------------
<a name="ch036"></a>  
### Vol06	CH036	page003		雅彦的独立作战  	Masahiko's Plan For Independence  {#ch036}  

- 06_018_2, 回忆里用的是漫画里的画面，但画面有些粗糙。同时，人物未着色：    
    ![](img/06_018_2__.jpg)  

----------------------------------------------------------
<a name="ch037"></a>  
### Vol06	CH037	page031		危险的兼职  	A Dangerous Job  {#ch037}  		  

----------------------------------------------------------
<a name="ch038"></a>  
### Vol06	CH038	page057		纯情葵  	The Feelings Of Aoï  {#ch038}  		  



- 06_064。1)黑色衣服的上色方式，为了表现转身的速度。2)用高光表示褶皱。  
    ![](./img/06_064_5__.jpg) 
    ![](./img/06_064_5__crop0.jpg) 
    ![](./img/06_064_5__crop1.jpg) 
    ![](./img/06_064_5__crop2.jpg) 
    ![](./img/06_064_5__crop3.jpg) 

- 06_077_0，衣服的光泽：  
    ![](img/06_077_0__.jpg) 
    ![](img/06_077_0__crop0.jpg) 
    ![](img/06_077_0__crop1.jpg) 
    ![](img/06_077_0__crop2.jpg) 
    ![](img/06_077_0__crop3.jpg) 

----------------------------------------------------------
<a name="ch039"></a>  
### Vol06	CH039	page085		Family-若苗  	My Family The Wakanaes  {#ch039}  		  

- 06_095_4，听说雅彦要搬走独立住，紫哭红了眼睛：  
    ![](img/06_095_4__crop0.jpg)  

- 06_134_2, 09_130_2， 手贴玻璃的效果。  
    ![](img/06_134_2__.jpg) 
    ![](img/09_130_2__.jpg) 

----------------------------------------------------------
<a name="ch040"></a>  
### Vol06	CH040	page111		憧憬的婚纱  	Wanted  {#ch040}  		  

- 06_142。影研社里喜欢雅美的那个女孩茜。她经常暗中观察雅彦，包括在学会、饭堂和校园（06_143_6）。这个镜头里，路人和背景用的是虚线。漫画里虚线不太明显，我手动加了虚线，并进一步简化背景，做个比对。  
    ![](./img/06_142_5__.jpg)
    

- 06_153。阴影处表现出了酒屋里踏踏米的凸凹不平。  
    ![](./img/06_153_1__crop0.jpg)

----------------------------------------------------------
<a name="ch042"></a>  
### Vol06	CH042	page165		秘密赴东京  	The Secret Trip  {#ch042}  		  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



# FC的画风-着色(shading) 

目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  


----------------------

## Vol13

------------------------------------
<a name="ch085"></a>  
### Vol13	CH085	page003		失乐园  	Lost Paradise  {#ch085}  		  

- 13_003，13_006_1。路人着蓝色。  
![](img/13_003_0__.jpg) 
![](img/13_003_0__crop.jpg) 
![](img/13_006_1__.jpg) 
![](img/13_006_1__crop0.jpg) 

- 13_003_0, 1)面部阴影的明暗有三个明显的层次；2）头发高光为黄褐色。  
![](img/13_003_0__.jpg) 
![](img/13_003_0__crop0.jpg) 
![](img/13_003_0__crop1.jpg) 


- 13_004_0, 13_006_5, 熏面色偏黄。可能是因为作者设定ta为男生的肤色--一般来说，男生肤色比女生稍暗。  
![](img/13_004_0__crop.jpg) 
![](img/13_006_5__.jpg)  

- 13_006_1, 1)黄色和紫色衣服有一层淡淡的白色，有些衣服材质的确有这样的效果；2）臀部和腿部，有些区域加了黑色阴影；3）地面的影子的着色方式；  
![](img/13_006_1__.jpg) 
![](img/13_006_1__crop1.jpg) 
![](img/13_006_1__crop2.jpg) 
![](img/13_006_1__crop3.jpg) 
![](img/13_006_1__crop4.jpg) 


- 13_006_3，熏正在街头演奏，听到雅彦喊ta，很惊讶。这里表示惊讶的情绪，画面着蓝色。  
![](img/13_006_3__crop.jpg)  

- 13_007_3, 眼镜上的高光.  
![](img/13_007_3__crop.jpg)  

- 13_008_0，部分地砖有反光。  
![](img/13_008_0__.jpg) 
![](img/13_008_0__crop.jpg)  

- 13_008，雅彦、紫苑的头部和背景之间有巨大的空隙：  
![](./img/13_008_6__.jpg)  


- 13_010_1，面部排线阴影。  
![](img/13_010_1__crop.jpg) 

- 13_010_2,面部排线阴影。  
![](img/13_010_2__crop.jpg) 

- 13_011_0, 脚随着音乐打节拍。1）鞋下的阴影；2）脚周围的阴影（围成一个类似圆形的亮区域）；    
![](img/13_011_0__crop.jpg) 

- 13_014，吉他背带的纹理很细腻：  
![](img/13_014_3__crop0.jpg)  

- 13_015_0, 墙角处的阴影。  
![](img/13_015_0__.jpg) 
![](img/13_015_0__crop.jpg) 

- 13_016_3，黑暗环境的一种画法：1）大区域（地面）加淡阴影；2）只有部分物体加黑色阴影；3)书包没加黑色阴影（[疑问]为什么?）, 头发加高光;4）左下角栏杆的不同面有不同的着色。5）树干着色逼真。    
![](img/13_016_3__.jpg) 
![](img/13_016_3__crop.jpg)  
该场景的另两个镜头(13_024_2，13_026_0)：  
![](img/13_024_2__s.jpg) 
![](img/13_026_0__s.jpg)  

- 13_017_0, 暗环境下，近处和远处的人物脚底依然加了淡淡的阴影。  
![](img/13_017_0__s.jpg) 
![](img/13_017_0__crop0.jpg) 
![](img/13_017_0__crop1.jpg) 

- 13_017_5, 1）腿部投下的阴影，用简单的竖线表示；2）暗环境下，背景灌木丛的着色。  
![](img/13_017_5__s.jpg) 
![](img/13_017_5__crop0.jpg) 
![](img/13_017_5__crop1.jpg) 

- 13_018_3，除去眼睛和嘴，整个画面加了很深的阴影。1）面部阴影排线；2）汗滴有反射和折射；    
![](img/13_018_3__s.jpg) 
![](img/13_018_3__crop0.jpg) 
![](img/13_018_3__crop1.jpg) 

- 13_018_4, 装乐器的箱子。  
![](img/13_018_4__s.jpg) 
![](img/13_018_4__crop0.jpg) 
![](img/13_018_4__crop1.jpg) 

- 13_019_0, 吉他的着色。  
![](img/13_019_0__crop.jpg) 

- 

- 13_019_4, 面部着色。1）口腔内的着色比以往更逼真；2）眉头的排线阴影（皱眉）；3）眼部和脸侧面的阴影排线。  
![](img/13_019_4__s.jpg) 
![](img/13_019_4__crop0.jpg) 
![](img/13_019_4__crop1.jpg) 
![](img/13_019_4__crop2.jpg) 

- 13_022_3, 1)眼部阴影排线；2）鼻子的形状更写实，用排线阴影；3）嘴部的着色更写实；4）口腔内的牙齿；  
![](img/13_022_3__s.jpg) 
![](img/13_022_3__crop0.jpg) 
![](img/13_022_3__crop1.jpg) 
![](img/13_022_3__crop2.jpg) 

- 13_023_4，面部着色：1）大面积淡阴影（网点？）；2）用排线增加阴影的暗度。  
![](img/13_023_4__.jpg) 

- 13_025_2，1）整个画面使用渐变阴影(网点?)；2）近景在底面用黑色阴影。  
![](img/13_025_2__s.jpg) 

- 13_026_0, 1）近景地砖逼真（有透视）；2）背景树木逼真；  
![](img/13_026_0__s.jpg) 
![](img/13_026_0__crop0.jpg) 
![](img/13_026_0__crop1.jpg) 

- 13_026_5, 1)脚部多个方向的阴影（因为有多个路灯）；2）雅彦左腿有排线阴影，且表现出腿部圆柱形曲面；紫苑右腿黑色阴影；3）远处的窗框上有屋内灯洒下的光;4)近处若苗空工作室的窗框上也有亮光，[疑问]这是屋内灯洒下的光吗？  
![](img/13_026_5__s.jpg) 
![](img/13_026_5__crop0.jpg) 
![](img/13_026_5__crop1.jpg) 
![](img/13_026_5__crop2.jpg) 

- 13_028_3, 身体在地面的投影的不同画法。1）紫苑和雅彦的影子；2）若苗空的影子；3）辰巳的影子。辰巳背部的影子边缘模糊，他腿部的影子边缘较清晰。这符合实际，因为他这个姿势的背部距离地面较远、腿部距离地面较近。  
![](img/13_028_3__s.jpg) 
![](img/13_028_3__crop0.jpg) 
![](img/13_028_3__crop1.jpg) 
![](img/13_028_3__crop2.jpg) 

- 13_029_3，面部阴影+排线阴影。  
![](img/13_029_3__crop.jpg) 

- 13_029_4，1）吉他不同部为颜色不同；2）琴弦用黑、白各着色一半，有立体感；3）沙发凹槽处的密集的排线阴影；4）因为在暗环境，所以物体无高光，且整个画面有淡阴影。  
![](img/13_029_4__crop.jpg) 

- 13_029_5, 1）吉他不同部为颜色不同；2）琴弦用黑、白各着色一半，有立体感；3）沙发凹槽处的密集的排线阴影；4）因为在暗环境，所以物体无高光，且整个画面有淡阴影;5)墙角处加暗。   
![](img/13_029_5__s.jpg) 
![](img/13_029_5__crop0.jpg) 
![](img/13_029_5__crop1.jpg) 

----------------------------------------------------------
<a name="ch086"></a>  
### Vol13	CH086	page031		父母心  	Parental Feelings  {#ch086}  		  

- 13_032_0，熏梦中的画面，高对比度。  
![](img/13_032_0__s.jpg) 

- 13_032_3, 画面较亮。1）头发着色三个层次：白、灰、黑；2）脖子下方的竖线阴影的边缘较整齐，甚至有部分阴影边缘线；3）汗珠的画法--长汗珠的线条有一个断点，应该表示折射光。  
![](img/13_032_3__s.jpg) 
![](img/13_032_3__crop0.jpg) 
![](img/13_032_3__crop1.jpg) 
![](img/13_032_3__crop2.jpg) 

- 13_033_1, 鼓掌。手心用网点着色。  
![](img/13_033_1__crop.jpg) 

- 13_034_0，用手砸门。手指处的排线可能表示阴影，也可能表示速度。    
![](img/13_034_0__s.jpg) 
![](img/13_034_0__crop.jpg) 

- 13_034_3，1）透过百叶窗的光线；2）床单上的亮色；3）球体的阴影排线；4）人物未加环境光照，(所以显得明亮).    
![](img/13_034_3__s.jpg) 
![](img/13_034_3__crop.jpg) 

- 13_036_6, 电线杆着色逼真。    
![](img/13_036_6__s.jpg) 
![](img/13_036_6__crop.jpg) 

- 13_038_0，物体在路面的影子（剧情里是早晨），影子边缘清晰。  
![](img/13_036_6__s.jpg) 

- 13_039_6, 熏被老师找去谈话。1）日光灯的亮度不大；2）楼廊加了阴影，但两侧窗玻璃无阴影（为什么？[疑问]）  
![](img/13_039_6__s.jpg) 
![](img/13_039_6__crop.jpg) 

- 13_040_2，熏被老师找去谈话。背景黑板的渐变阴影。  
![](img/13_040_2__s.jpg) 
![](img/13_040_2__crop.jpg) 

- 13_041_4，熏开教师门出去。门外走廊对面的窗子和玻璃简化为轮廓。  
![](img/13_041_4__s.jpg) 

- 13_041_6, 整个画面加辉光效果。  
![](img/13_041_6__s.jpg) 
![](img/13_041_6__crop.jpg) 

- 13_042_0, 整个画面加辉光效果。  
![](img/13_042_0__s.jpg) 
![](img/13_042_0__crop.jpg) 

- 13_042_2, 1)杆子上的着色；2）胳膊在衣服上的投影，阴影排线且渐变；3）墙上被撕破的广告纸，未着色。  
![](img/13_042_2__s.jpg) 
![](img/13_042_2__crop0.jpg) 
![](img/13_042_2__crop1.jpg) 
![](img/13_042_2__crop2.jpg) 

- 13_044_4, 背景黑衣和前景人物间的过渡。紫苑脸部和背景有较大空隙，且用的交错线条。    
![](img/13_044_4__s.jpg) 
![](img/13_044_4__crop.jpg) 


- 13_045_2, 回忆画面里，1）背景镜子、墙角有着色；2）前景衣服有纹理，只有少数阴影。整体感觉是背景的着色更细致。为什么会这样？[疑问]  
![](img/13_045_2__s.jpg) 
![](img/13_045_2__crop0.jpg) 
![](img/13_045_2__crop1.jpg) 

- 13_053_6，面部排线阴影。  
![](img/13_053_6__crop.jpg) 

- 13_054_0, 熏坐在屋顶，猛捶屋顶。1）线条模糊表示速度和力度；2）手和屋顶接触的位置未用模糊；3）黑点应该表示击起的尘土。  
![](img/13_054_0__s.jpg) 
![](img/13_054_0__crop.jpg) 

- 13_054_1, 13_054_4, 同一个影子的不同画法。  
![](img/13_054_1__s.jpg) 
![](img/13_054_1__crop.jpg)  
![](img/13_054_4__s.jpg) 
![](img/13_054_4__crop.jpg) 


- 13_054_3, 若苗空的裤子透过玻璃的效果。  
![](img/13_054_3__s.jpg) 
![](img/13_054_3__crop.jpg) 

- 13_054_5, 左一影子有一处尖角（左一红框所示），它是左二里的屋檐的影子（左二红框所示）。作者画的很仔细！。    
![](img/13_054_5__s_mod.jpg) 
![](img/13_054_4__s_mod.jpg) 


- 13_055，裤子上的皱纹非常赞！  
![](./img/13_055_1__.jpg)  

- 13_057_0，面部排线阴影。  
![](img/13_057_0__crop.jpg) 

----------------------------------------------------------
<a name="ch087"></a>  
### Vol13	CH087	page059		孩子要独立  	Cutting The Umbilical Cord  {#ch087}  		  

- 13_059_0, 1）交叉排线阴影，且有渐变；2）石块磨损的棱角很逼真。[疑问]为什么磨损的棱角画得这么逼真？     
![](img/13_059_0__s.jpg) 
![](img/13_059_0__crop0.jpg) 
![](img/13_059_0__crop1.jpg) 
![](img/13_059_0__crop2.jpg) 
![](img/13_059_0__crop3.jpg) 


- 13_061, 人物和背景之间有巨大的空白：  
![](./img/13_061_1__.jpg)  

- 13_063_0, 1）角色加阴影；2）地面影子为网点+排线；3）背景加了竖线阴影；4）门上的阴影；5）台阶处的阴影。台阶处的排线阴影未必位于角落，但它们位于画面的边缘，就加了排线阴影。[todo]去掉画面边缘的（部分）排线阴影，对比一下。   
![](img/13_063_0__s.jpg) 
![](img/13_063_0__crop0.jpg) 
![](img/13_063_0__crop1.jpg) 


- 13_070_0, 辰巳前面的一个路人女子(背包)，她的头发细节度较高。  
![](./img/13_070_0__crop.jpg)  

- 13_070_6，辰巳和空之间背后的那个路人的头发细节度较高：  
![](./img/13_070_6__.jpg)

- 13_071, 13_075, 13_076. 空和辰巳挤在人群里，紫苑和雅彦看到他们后挤了过去：  
![](../fc_sequence/img/13_071_7__.jpg)  
人群没使用简化的画法，所以如果没有紫苑和雅彦的对话框的话，不太容易找到他们。  

- 13_072_3，若苗空眼部特写。1）面部侧面大面积阴影排线；2）眼下方画有眼睑；3）从上下眼皮的曲线看出，眼睛角膜未凸起。  
![](img/13_072_3__crop.jpg) 

- 13_074，汗滴的精细画法：  
![](img/13_074_5__crop.jpg)  

- 13_076_0, 1）线条粗细程度： 熏 > 左侧前景人物 > 中间主要人物 > 右侧背景人物；2）左侧前景人物细节度高；3）路人加阴影，突出主要人物；4）辰巳眼仁无着色；5）辰巳腋下的阴影；    
![](img/13_076_0__s.jpg) 
![](img/13_076_0__crop0.jpg) 
![](img/13_076_0__crop1.jpg) 
![](img/13_076_0__crop2.jpg) 
![](img/13_076_0__crop3.jpg) 

- 13_076_1，熏的眼部特写。  
![](img/13_076_1__s.jpg) 

- 13_077_1, 辰巳的面部特写。整体加了渐变的特效。  
![](img/13_077_1__s.jpg)  
汗珠逼真。1）汗珠边缘线有1～2个断点；2）暗处的汗珠着色细节多，亮处的汗珠几乎无着色；2）暗处的汗珠有明显的折射；  
![](img/13_077_1__crop0.jpg) 
![](img/13_077_1__crop1.jpg) 
![](img/13_077_1__crop2.jpg) 

- 13_077_4, 辰巳怒，紧握拳。 1）袖口处的排线阴影，体现出圆柱形曲面；2）胳膊在身体上的投影为全黑；3）手背关节处的阴影；4）手部下方的排线阴影，有渐变；   
![](img/13_077_4__s.jpg) 
![](img/13_077_4__crop0.jpg) 
![](img/13_077_4__crop1.jpg) 
![](img/13_077_4__crop2.jpg) 

- 13_079_3， 1）背景路人简化为轮廓；2）聚焦线条为灰色、虚线；  
![](img/13_079_3__s.jpg) 

- 13_082_1, 熏的眼部特写。  
![](img/13_082_1__crop.jpg) 

- 13_084_0，1）路人简化为灰色轮廓，且下方有渐变；2）路人的背后有更淡的阴影，且从左到右有渐变。  
![](img/13_084_0__s.jpg) 

----------------------------------------------------------
<a name="ch088"></a>  
### Vol13	CH088	page087		似近亦远的东京  	Tokyo, So Near And Yet So Far  {#ch088}  		  

- 13_087_0, 原图为彩图，电话的着色。  
![](img/13_087_0__crop.jpg) 

- 13_088_0, 1)地板的纹理；2）地板纹理在明暗区域的对比；3）皮肤的着色，有明显的斑点；  
![](img/13_088_0__s.jpg) 
![](img/13_088_0__crop0.jpg) 
![](img/13_088_0__crop1.jpg) 
![](img/13_088_0__crop2.jpg) 

- 13_091, 右下角两路人秃头？可能这是头发的画法--但只是忘记画了上半部分的黑发。  
![](./img/13_091_5__.jpg)  

- 13_092，人物和背景间有巨大的空白，而且背景较简单：  
![](./img/13_092_5__.jpg)  

- 13_094，脖子处的阴影无边框：  
![](./img/13_094_6__.jpg)  
对比12_033_1（脖子处的阴影有边框）：  
![](./img/12_033_1__.jpg)  

- 13_095_6, 列车飞驰的效果。  
![](img/13_095_6__s.jpg) 
![](img/13_095_6__crop.jpg) 

- 13_098_0, 下电车后，去叶子家的路上。1）从近到远的树；2）树荫投影在车和路面上；3）电缆逼真；4）近处的景物逼真；  
![](img/13_098_0__s.jpg) 
![](img/13_098_0__crop0.jpg) 
![](img/13_098_0__crop1.jpg) 
![](img/13_098_0__crop2.jpg) 
![](img/13_098_0__crop3.jpg) 
![](img/13_098_0__crop4.jpg) 
![](img/13_098_0__crop5.jpg) 
![](img/13_098_0__crop6.jpg) 

- 13_098_5, 1)车窗外景物飞驰；2）车内物体加阴影和高光；3）座椅明暗交界处用深色着色，可理解为物体的边缘线。    
![](img/13_098_5__s.jpg) 
![](img/13_098_5__crop.jpg) 

- 13_101_4, 背景的柱子有渐变，用白线表边缘（或高光）。   
![](img/13_101_4__s.jpg) 
![](img/13_101_4__crop.jpg) 

- 13_104_6, 1)人物脚下的阴影；2）背景台阶的阴影；  
![](img/13_104_6__s.jpg) 
![](img/13_104_6__crop0.jpg) 
![](img/13_104_6__crop1.jpg) 

- 13_108_0, 1)人物脚下的阴影可能是倒影；2）背景木质结构有倒影；  
![](img/13_108_0__s.jpg) 
![](img/13_108_0__crop0.jpg) 
![](img/13_108_0__crop1.jpg) 

- 13_111_1，暗环境下的一种画法。  
![](img/13_111_1__s.jpg) 

- 13_111_4, 暗环境下的一种画法。1)人物明亮，未加环境着色；2）人物和背景黑色的过渡（排线）；3）前景草木的着色；  
![](img/13_111_4__s.jpg) 
![](img/13_111_4__crop.jpg) 

- 13_112_0，暗环境下的一种画法。1）靠近灯火的山脉更暗。2）近景黑色树木之间的过渡（排线）；3）近景树木着黑色，稍远处树木着灰色；   
![](img/13_112_0__s.jpg) 
![](img/13_112_0__crop0.jpg) 
![](img/13_112_0__crop1.jpg) 

- 13_113_1, 暗环境下的一种画法。1)左下角的树干；2）右下角的近景草木；3）远景树木的两个层次，用排线着色过渡；4）前景人物用高对比度着色；  
![](img/13_113_1__s.jpg) 
![](img/13_113_1__crop0.jpg) 
![](img/13_113_1__crop1.jpg) 
![](img/13_113_1__crop2.jpg) 
![](img/13_113_1__crop3.jpg) 

----------------------------------------------------------
<a name="ch089"></a>  
### Vol13	CH089	page115		母与女  	Mother And Daughter  {#ch089}  		  


- 13_116_2, 暗环境下的一种画法。1.近景草木着灰色；2.中景到背景草木的过渡；3.手电筒光芒后的物体；4.中景树木的边缘稍有亮色；  
![](img/13_116_2__s.jpg) 
1.![](img/13_116_2__crop0.jpg) 
2.![](img/13_116_2__crop1.jpg) 
3.![](img/13_116_2__crop2.jpg) 
4.![](img/13_116_2__crop3.jpg) 

<a name="13_116_5"></a>  
- 13_116_5, 光照逼真。1）屋内的灯光透过窗子洒在木质结构上；2）灯光直接洒在木质结构上。这些高光照亮了木制结构的位置、形状、凹凸不平的表面纹理。    
![](img/13_116_5__s.jpg) 
1.![](img/13_116_5__crop0.jpg) 
2.![](img/13_116_5__crop1.jpg) 

- 13_117_0, 1.角色脚下的着色线条不是竖线，而是斜线，所以应该是灯光投下的影子，而不是倒影; 2,3,4有倒影；4.墙角处的地砖有亮晶晶的效果。  
![](img/13_117_0__s.jpg) 
1.![](img/13_117_0__crop0.jpg) 
2.![](img/13_117_0__crop1.jpg) 
3.![](img/13_117_0__crop2.jpg) 
4.![](img/13_117_0__crop3.jpg) 

- 13_123_6，门上的人影。  
![](img/13_123_6__s.jpg) 

- 13_124_1, 背景是走廊里的窗子，简化为轮廓，且有渐变。  
![](img/13_124_1__s.jpg) 

- 13_124_2, 1)整个画面有自上而下渐变的阴影；2）角色脚下有倒影。  
![](img/13_124_2__s.jpg) 

- 13_126_3，1)近景的草用淡色，且只有轮廓；2）中景的植被着色逼真；3）远景的建筑简化为轮廓，且融入背景；  
![](img/13_126_3__s.jpg) 
1.![](img/13_126_3__crop0.jpg) 
2.![](img/13_126_3__crop1.jpg) 
3.![](img/13_126_3__crop2.jpg) 

- 13_126_4, 1)近景的植被只在边缘着色；2）窗户处的栅栏着色逼真；3）图片边缘的窗户下有竖线阴影；4）角色手部的阴影；  
![](img/13_126_4__s.jpg) 
1.![](img/13_126_4__crop0.jpg) 
2.![](img/13_126_4__crop1.jpg) 
3.![](img/13_126_4__crop2.jpg) 
4.![](img/13_126_4__crop3.jpg) 

- 13_128_0, 小叶子从门缝中看见忙碌的母亲。1）门框无实线；2）门用排线阴影，可能是景深(DOF)效果，或辉光(glow)效果；  
![](img/13_128_0__s.jpg)， 
![](img/13_128_0__crop.jpg) 

- 13_128_1,满头大汗的画法。    
![](img/13_128_1__crop.jpg) 

- 13_128_2, 1)图片边缘处的门框上有排线阴影；2）门框有一条边线加了阴影(红箭头所示)；   
![](img/13_128_2__s.jpg)， 

- 13_131, 鬓角处的头发是镂空的：  
![](./img/13_131_1__.jpg)

- 13_134_7, 1）背景中的阴影，有渐变；2）织物的褶皱和阴影逼真；  
![](img/13_134_7__s.jpg) 
1.![](img/13_134_7__crop0.jpg) 
2.![](img/13_134_7__crop1.jpg) 

- 13_135_0, 1)灯光照亮角色脸部和衣服；2）角色加了环境光（暗色阴影）；3）近处房梁的着色，表现了其木制纹理；4）房梁从近到远的着色渐变；  
![](img/13_135_0__s.jpg) 
1.![](img/13_135_0__crop0.jpg) 
2.![](img/13_135_0__crop1.jpg) 
3.![](img/13_135_0__crop2.jpg) 
4.![](img/13_135_0__crop3.jpg) 

- 13_135_5, 1）远景角色没有和环境融为一体，反而突出地画为黑色；2）远景角色后背用阴影排线，表明低头；3）香烟的烟雾突出地画为白色；4）房顶处加了竖线阴影；    
![](img/13_135_5__s.jpg) 
![](img/13_135_5__crop0.jpg) 

- 13_136_5, 1）房顶处加了竖线阴影； 2）角色未加环境光；3）茶几下的阴影；  
![](img/13_136_5__s.jpg) 
![](img/13_136_5__crop0.jpg) 
![](img/13_136_5__crop1.jpg) 

- 13_138_0, 茶几下的阴影。剧中并没有开灯，所以这里是专门把环境变亮。   
![](img/13_138_0__s.jpg) 
![](img/13_138_0__crop.jpg) 

----------------------------------------------------------
<a name="ch090"></a>  
### Vol13	CH090	page141		冲突  	Clash  {#ch090}  		  

- 13_142，阴影非常细腻。（也有可能是原画有网点效果，但扫描成电子版时丢失了网点效果）  
![](./img/13_142_2__.jpg) 


- 13_145_2。 暗环境。1）木制结构的着色；2）背景植物的着色；3）茶几底下有阴影；4）人物画为黑色；  
![](img/13_145_2__s.jpg) 
![](img/13_145_2__crop0.jpg) 
![](img/13_145_2__crop1.jpg) 

- 13_146_0，虽然剧情是暗环境，但人物画为亮色；  
![](img/13_146_0__s.jpg) 

- 13_147, 比起叶子的衣服，她手里的饮料画得细致：不仅细节度高，而且有阴影和高光。  
![](./img/13_147_6__.jpg)  
关于制作流程，我猜，这个饮料瓶会不会是“把已经画好的图复制粘贴上去”的？  

- 13_153_6，早晨阳光明媚的效果。角落里加阴影；  
![](img/13_153_6__s.jpg) 

<a name="13_156_1"></a>  
#### 13_156_1 {#13_156_1}  
- 13_156_1, 水面波光粼粼的效果逼真。石头下方有零星的阴影，可能是折射或反射。仔细看的话，感觉石块和水没有融为一体。对比[13_189_1](#13_189_1)(左三)，似乎水色更深，与石头更能融为一体。    
![](img/13_156_1__s.jpg) 
![](img/13_156_1__crop0.jpg) 
![](img/13_189_1__crop.jpg) 

- 13_157, 远景的树比中景的石头画得细致：  
![](../fc_sequence/img/13_157_5__.jpg)  


- 13_159_0, 山的一种画法。   
![](img/13_159_0__s.jpg) 
![](img/13_159_0__crop.jpg) 


- 13_160, 风景的墨色很重：  
![](./img/13_160_0__.jpg) 


- 13_162_4, 眼部、嘴部着色；  
![](img/13_162_4__s.jpg) 
![](img/13_162_4__crop0.jpg) 
![](img/13_162_4__crop1.jpg) 

- 13_163_1, 能感受到画面里"草被风吹动产生的波浪"，[疑问]这种感觉是怎么画出来的呢？  
![](img/13_163_1__s.jpg) 


- 13_165_3, 远处的树木；  
![](img/13_165_3__s.jpg) 
![](img/13_165_3__crop.jpg) 

- 13_167_5, 1）袖口处的阴影，阴影边缘较清晰；2）衣缝的阴影；  
![](img/13_167_5__s.jpg) 
1.![](img/13_167_5__crop0.jpg) 
2.![](img/13_167_5__crop1.jpg)， 
  ![](img/13_167_5__crop2.jpg) 

----------------------------------------------------------
<a name="ch091"></a>  
### Vol13	CH091	page169		只做一天老板娘  	Okami For A Day  {#ch091}  	

- 13_170_3，1）织物的褶皱用淡色阴影，少数有阴影排线；2）用阴影表示嘴唇厚度；3）下颌处的阴影；4）画面边缘有排线阴影；5）人物画了鼻翼。    
![](img/13_170_3__s.jpg) 
![](img/13_170_3__crop.jpg) 

- 13_179_2, 1）浴室地面的水；2）阴影也用的是竖排线；3）玻璃窗外的树木；  
![](img/13_179_2__s.jpg) 
![](img/13_179_2__crop0.jpg) 
![](img/13_179_2__crop1.jpg) 
![](img/13_179_2__crop2.jpg) 
![](img/13_179_2__crop3.jpg) 
![](img/13_179_2__crop4.jpg) 

- 13_182_3，用一些浅色线条表示风。  
![](img/13_182_3__crop.jpg) 

- 13_183_3，茶杯茶壶的着色；  
![](img/13_183_3__crop.jpg) 

- 13_184_5，竹子的画法特别。从剧情推测，这是从屋内透过窗帘或纱窗看到的效果。  
![](img/13_184_5__crop.jpg) 

- 13_185_5，一种面部着色。  
![](img/13_185_5__s.jpg) 

- 13_186，左边窗户和右边窗帘都有半透明效果：  
![](./img/13_186_0__.jpg)

- 13_188_0，逆光的面部着色。  
![](img/13_188_0__s.jpg) 

<a name="13_189_1"></a>  
#### 13_189_1 {#13_189_1}
- 13_189_1，水面波光粼粼的效果。 对比[13_156_1](#13_156_1)（左二）.   
![](img/13_189_1__crop.jpg) 
![](img/13_156_1__crop0.jpg) 

- 13_190_4, 1)玻璃上有阴影；2）红框内是否是倒影？3）窗外景物飞驰；    
![](img/13_190_4__s.jpg) 
![](img/13_190_4__crop.jpg) 

- 13_191_4，景物穿过路灯的光芒。  
![](img/13_191_4__s.jpg) 
![](img/13_191_4__crop.jpg) 

- 13_193_4, 面部着色；  
![](img/13_193_4__s.jpg) 
![](img/13_193_4__crop.jpg) 




--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



# FC的画风-着色(shading) 

目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  


----------------------

## Vol14

- 14_000a_0, 1）衣袖透光；2）皮肤有衣服的勒痕；3）勒痕处有相应的着色--亮色或暗色:  
![](img/14_000a_0__.jpg), 
![](img/14_000a_0__crop0.jpg), 
![](img/14_000a_0__crop1.jpg), 
![](img/14_000a_0__crop2.jpg), 

--------------------------------------
<a name="ch092"></a>  
### Vol14	CH092	page003		恐怖的迎新联欢会  	Scary Meeting  {#ch092}  		  

- 14_003_0, 1)人物加了淡淡的肤色；2）颈部的阴影。（顺便说，人物线条粗细相间，很流畅！）   
![](img/14_003_0__.jpg), 
![](img/14_003_0__crop0.jpg), 
![](img/14_003_0__crop1.jpg), 


- 14_005_5，背景用褐色渐变。人物只是平面的着色，未表现形状:  
![](img/14_005_5__.jpg)  

- 14_006_5，1）画面整体加褐色。是晚霞的效果吗？2）树叶用蓝色。这很特别。   
![](img/14_006_5__.jpg)  

- 14_006_6，1）背景整体加褐色。是晚霞的效果吗？2）角色未加环境色；3）树木的画法；  
![](img/14_006_6__.jpg)  

- 14_007_3，背景里夕阳的着色效果:  
![](img/14_007_3__.jpg) 
![](img/14_007_3__crop.jpg)  

- 14_013_1，前景物体（舞台上的设备）着色逼真:  
![](img/14_013_1__.jpg) 
![](img/14_013_1__crop.jpg)  

- 14_017_6，面部整体加网点阴影，侧面加排线阴影:  
![](img/14_017_6__.jpg) 
![](img/14_017_6__crop.jpg)  

- 14_019_1, 帽沿在脸部投下的阴影:  
![](img/14_019_1__.jpg)  

- 14_019_2, 屋顶着色细节度高:  
![](img/14_019_2__.jpg) 
![](img/14_019_2__crop.jpg)  

- 14_019_4，人物和背景衣服之间用排线过渡:  
![](img/14_019_4__.jpg) 
![](img/14_019_4__crop.jpg)  

- 14_020_1，面部阴影(排线)，帽沿投下的阴影(渐变网点):  
![](img/14_020_1__crop.jpg)  

- 14_020_2，面部整体阴影（网点），面部阴影（排线），因醉酒而脸红（稀疏排线）:  
![](img/14_020_2__.jpg)  

- 14_021_2, 衣服上的交叉排线阴影:  
![](img/14_021_2__.jpg) 
![](img/14_021_2__crop.jpg)  

- 14_021_0, 14_021_4, 对侧的眼部被简化为阴影:  
![](img/14_021_0__.jpg) 
![](img/14_021_0__crop0.jpg)  
![](img/14_021_4__.jpg) 
![](img/14_021_4__crop0.jpg)  

<a name="14_022_0"></a>  
- 14_022_0, 盛有酒精的容器后面的物体的着色：  
![](img/14_022_0__.jpg)， 
![](img/14_022_0__crop0.jpg)，  
此外，物体透过盛酒精的容器后没有折射效果吗？   
![](img/14_022_0__crop0.jpg)， 
![](img/14_022_0__crop1.jpg)， 

- 14_024_4，面部排线阴影+网点阴影:  
![](img/14_024_4__.jpg)  

- 14_026_3，首先，单就表情来说，角色眉毛上移，表示困倦或轻蔑。其次，脸颊、额头两处阴影。分别去掉这两处阴影，对比后发现，这些阴影很传神。  
![](img/14_026_3__crop.jpg) 
![](img/14_026_3__crop_mod0.jpg) 
![](img/14_026_3__crop_mod1.jpg) 
![](img/14_026_3__crop_mod2.jpg) 
 

- 14_026_7, 鞋尖处的排线着色，[疑问]这表示高光吗？   
![](img/14_026_7__.jpg) 
![](img/14_026_7__crop.jpg)  
 

- 14_029_4，背景的夜景简化为灯光的轮廓:  
![](img/14_029_4__.jpg) 
![](img/14_029_4__crop.jpg)  


----------------------------------------------------------
<a name="ch093"></a>  
### Vol14	CH093	page031		再会  	Reunion  {#ch093}  		  

- 14_031_0，面部的阴影，锁骨处的阴影，手部的阴影:  
![](img/14_031_0__.jpg) 
![](img/14_031_0__crop0.jpg) 
![](img/14_031_0__crop1.jpg) 

题外话：该图灯光自下而上，一般会让人感到恐怖或怪异，[疑问]但这张图并没有恐怖或怪异感，为什么？[疑问]如果面部不显示全部(如下图)，则会有种怪异感，为什么？  
![](img/14_031_0__crop0_mod.gif)  


- 14_034_4, 树干上的斑驳阴影；背景植被的斜线纹理；近景植被简化为黑色轮廓和白色轮廓。  
![](img/14_034_4__.jpg) 
![](img/14_034_4__crop0.jpg) 
![](img/14_034_4__crop1.jpg)  

- 14_035_1，树干上的斑驳阴影；树叶的画法；地面上，只在角色附近的阴影处画草（因为其他区域太明亮，所以不画草）:  
![](img/14_035_1__.jpg) 
![](img/14_035_1__crop0.jpg) 
![](img/14_035_1__crop1.jpg) 
 

- 14_039_0，逆光效果。1）背景人物和门都有辉光；2）背景人物衣服有渐变模糊的过渡；3）膝盖处的阴影；4）前景人物脸部的排线阴影；  
![](img/14_039_0__.jpg) 
![](img/14_039_0__crop0.jpg) 
![](img/14_039_0__crop1.jpg) 
![](img/14_039_0__crop2.jpg) 
![](img/14_039_0__crop3.jpg) 
 

- 14_039_3, 快速推人。肩膀、胳膊处的排线表示速度快:  
![](img/14_039_3__.jpg) 
![](img/14_039_3__crop.jpg) 
 

- 14_040_0,面部阴影：网点+排线。用排线衬托出鼻翼:  
![](img/14_040_0__.jpg) 
![](img/14_040_0__crop.jpg)  

- 14_040_5，背带上的纹理:  
![](img/14_040_5__.jpg) 
![](img/14_040_5__crop.jpg)  

- 14_040，14_041，江岛的黑色衣服，两种不同的着色：  
![](./img/14_040_3__.jpg) 
![](./img/14_041_2__.jpg)  

- 14_041_1，表示迅速转身的排线和特效;鞋带的着色；  
![](img/14_041_1__.jpg) 
![](img/14_041_1__crop0.jpg) 
![](img/14_041_1__crop1.jpg)  

- 14_041_5, 背景窗户简化为轮廓；用排线衬托出下嘴唇；耳朵下方的排线阴影有点特别:  
![](img/14_041_5__.jpg) 
![](img/14_041_5__crop.jpg) 
 

- 14_041_6,夕阳。1)云的着色是一块一块的，有点像油画里粗糙的笔触；2）近景树叶简化为黑白相间的轮廓:  
![](img/14_041_6__.jpg) 
![](img/14_041_6__crop0.jpg) 
![](img/14_041_6__crop1.jpg)  

- 14_042_6, 1)背景虚化；2）胳膊下方有渐变的排线阴影:  
![](img/14_042_6__.jpg) 
![](img/14_042_6__crop0.jpg) 
![](img/14_042_6__crop1.jpg) 

- 14_043_0, 14_045_4, 帽沿在面部的影子:  
![](img/14_043_0__.jpg) 
![](img/14_043_0__crop0.jpg)  
![](img/14_045_4__.jpg) 
![](img/14_045_4__crop0.jpg)  

- 14_043_2，前景人物整体面部加阴影，眼部的排线阴影:  
![](img/14_043_2__.jpg)  

<a name="14_043_3"></a> 
- 14_043_3，14_067_5， 吸管在水里没有折射效果。  
![](img/14_043_3__.jpg) 
![](img/14_067_5__.jpg)  

- 14_043_5, 近景盆栽的着色（渐变阴影）；烟灰缸的着色；近景墙砖和花的网点阴影:  
![](img/14_043_5__.jpg) 
![](img/14_043_5__crop0.jpg) 
![](img/14_043_5__crop1.jpg)  

- 14_044_4，面部整体的网点阴影，侧面脸颊的排线阴影:  
![](img/14_044_4__.jpg)  

- 14_046_5，1）因为托腮，所以嘴角和下嘴唇略微变形；2）灌木丛被路灯照亮的效果:  
![](img/14_046_5__.jpg) 
![](img/14_046_5__crop0.jpg) 
![](img/14_046_5__crop1.jpg) 

- 14_050, 衣服被泼湿、粘在身上的效果；  
![](./img/14_050_3__.jpg)  

- 14_050_4, 扫把和刷子的画法:  
![](img/14_050_4__.jpg) 
![](img/14_050_4__crop0.jpg) 
![](img/14_050_4__crop1.jpg) 

- 14_052_7, 1)近景树叶用交叉排线着色；2）近景树叶简化为黑白轮廓。右上角为黑色轮廓包围白色轮廓，左下角为黑白轮廓相间；3）中景的植被有斜线纹理，可能表示投下的阴影；4）中景的植被有渐变的淡色阴影；5）背景植被有层次感远：从黑/灰过渡为白色，再过渡为黑色，再过渡为灰色；  
![](img/14_052_7__.jpg) 
![](img/14_052_7__crop0.jpg) 
![](img/14_052_7__crop1.jpg) 
![](img/14_052_7__crop2.jpg) 
![](img/14_052_7__crop3.jpg) 
![](img/14_052_7__crop4.jpg) 
![](img/14_052_7__crop5.jpg) 
![](img/14_052_7__crop6.jpg) 

- 14_055_0, 衣服领子处的阴影过渡不平滑，有种卡通效果:  
![](img/14_055_0__.jpg) 
![](img/14_055_0__crop.jpg) 
 

----------------------------------------------------------
<a name="ch094"></a>  
### Vol14	CH094	page057		浅葱到访  	Asagi's Visit  {#ch094}  		  

- 14_057_0，1）近景角色衣服边缘的高光；2）角色耳朵后方的阴影排线；3）耳朵的着色；4）中景角色的面部阴影(网点)；5)背景有不同层次；6）背景窗户简化为轮廓；7)小腹处的褶皱和排线阴影。（B.T.W.盐谷/紫苑的耳屏很小）  
![](img/14_057_0__.jpg) 
![](img/14_057_0__crop0.jpg) 
![](img/14_057_0__crop1.jpg) 
![](img/14_057_0__crop2.jpg) 
![](img/14_057_0__crop3.jpg) 
![](img/14_057_0__crop4.jpg) 
![](img/14_057_0__crop5.jpg) 
 
<a name="14_058_0"></a>  
#### 14_058_0     
- 14_058_0, 为什么屋檐下有亮光？可能是被反射的阳光照亮了。首先，由墙面上的影子，可以推测出阳光的大致方向。然后，如右图所示(蓝色代表在物体背面)，阳光经过2次反射后再进入眼睛(相机):  
![](img/14_058_0__crop.jpg) 
![](img/14_058_0__mod_crop.jpg) 

- 14_058_1,着色逼真:  
![](img/14_058_1__.jpg)  

- 14_059_4，背景虚化:  
![](img/14_059_4__.jpg)  

- 14_060_1，着色逼真:  
![](img/14_060_1__.jpg)  

- 14_061_1，1)雅彦在毯子上的影子深,且模糊；2）紫苑在地板上的影子浅，且有门玻璃的倒影；3）书桌背面黑色，物体在书桌侧面的投影为浅色；4）屋顶几乎无着色:  
![](img/14_061_1__.jpg) 
![](img/14_061_1__crop0.jpg) 
![](img/14_061_1__crop1.jpg) 

- 14_061_5，眼部阴影排线:  
![](img/14_061_5__.jpg)  

- 14_067_0，1）墙、墙裙、门颜色不同；2）门的不同侧面的颜色不同；3）门在地面的投影有深浅，横向影子深、纵向影子浅;4)植被的着色:  
![](img/14_067_0__.jpg) 
![](img/14_067_0__crop0.jpg) 
![](img/14_067_0__crop1.jpg) 
![](img/14_067_0__crop2.jpg)  

- 14_068_3, 1）玻璃的全反射，有辉光；2）玻璃反射树影；3）树影为暗色，表现室外阳光强烈；  
![](img/14_068_3__.jpg) 
![](img/14_068_3__crop.jpg)  

- 14_068_7， 1）茶几表面的反射；2）茶几表面有倒影；3）杯子的水痕:  
![](img/14_068_7__.jpg)  

- 14_070, 裤子上的阴影：  
![](./img/14_070_5__.jpg)  

- 14_076_1, 1）衣服缝隙里有阴影排线，沿缝隙的方向排线；2）角落里有阴影排线和网点阴影:  
![](img/14_076_1__.jpg) 
![](img/14_076_1__crop0.jpg) 
![](img/14_076_1__crop1.jpg) 

- 14_080_4, 1）屋檐角落的着色逼真，表面凹凸不平，且有渐变。[疑问]不象是排线或网点，难道是纯手工画的吗？2）玻璃透射的效果；3）屋檐在窗框上的投影；4）窗栓逼真；5）窗户底座、屋檐的不同侧面的颜色不同；6)屋檐最外层有不同的着色（颜色深且有纹理）；    
![](img/14_080_4__.jpg) 
![](img/14_080_4__crop0.jpg) 
![](img/14_080_4__crop1.jpg) 
![](img/14_080_4__crop2.jpg) 
![](img/14_080_4__crop3.jpg) 
 

- 14_081_5, 1)玻璃上的影子；2）角落处的密集排线阴影；3）呼出的哈气投射墙壁边线和空调边线，如下图右红框所示；  
![](img/14_081_5__.jpg) 
![](img/14_081_5__crop_mod.jpg) 
 

----------------------------------------------------------
<a name="ch095"></a>  
### Vol14	CH095	page083		浅葱突击行动  	Asagi's Plan  {#ch095}  		  

- 14_083_0,1)紫苑脸部着色深浅分为3个层次；2）身体在胳膊上的投影有渐变；3）栏杆接近顶部边缘处有亮线；4）牛仔裤上有排线高光；5）牛仔裤裤兜处的纹理和褶皱逼真:  
![](img/14_083_0__.jpg) 
![](img/14_083_0__crop0.jpg) 
![](img/14_083_0__crop1.jpg) 
![](img/14_083_0__crop2.jpg) 
![](img/14_083_0__crop3.jpg) 
 

- 14_085_4, 1)屋檐角落用排线阴影+网点阴影；2）屋檐最外层无着色，对比14_080_4（右图）:  
![](img/14_085_4__.jpg) 
![](img/14_085_4__crop.jpg) 
![](img/14_080_4__crop3.jpg) 

- 14_086_3, 1)衣橱门上的锯齿状阴影；2）脚底阴影简单；3）垃圾桶侧面的排线阴影；4）屋顶角落处有少许排线阴影:  
![](img/14_086_3__.jpg) 
![](img/14_086_3__crop0.jpg) 
![](img/14_086_3__crop1.jpg) 
![](img/14_086_3__crop2.jpg)

- 14_092_1, 1)浅葱右腿的排线阴影未表现出腿的曲面；2）台阶处只有墙脚处有阴影(网点):  
![](img/14_092_1__.jpg) 
![](img/14_092_1__crop0.jpg) 
![](img/14_092_1__crop1.jpg) 

- 14_094_1,若苗空的愤怒。1)眼球底部加排线阴影；2）鼻翼处的排线阴影似乎衬托出鼻翼；3）脸侧面的排线阴影；4）汗滴线短，着色较细致；5）整个面部加网点阴影  :  
![](img/14_094_1__.jpg) 
![](img/14_094_1__crop0.jpg) 
![](img/14_094_1__crop1.jpg) 
![](img/14_094_1__crop2.jpg) 

- 14_094_3, 若苗空怒吼。1)深色排线表示声音大；2）门处有浅色喷射状着色；3）屋檐处有一大块亮斑,[疑问]其原因是否和[14_058_0](#14_058_0)里的分析一致？5)屋顶有振动线条；    
![](img/14_094_3__.jpg) 
![](img/14_094_3__crop0.jpg) 
![](img/14_094_3__crop1.jpg)  

- 14_097_1， 面部侧面有排线阴影，边缘有高亮:  
![](img/14_097_1__.jpg)  

- 14_099_1, 1)浅葱衣角内侧的排线阴影；2）盐谷胳膊下方的渐变排线阴影，且有边缘清晰；3）背景树木的着色，有斜亮线([疑问]是否表示阳光的方向？)；4）站台边缘的地砖可能是凸起、也可能是缝隙:  
![](img/14_099_1__.jpg) 
![](img/14_099_1__crop0.jpg) 
![](img/14_099_1__crop1.jpg) 
![](img/14_099_1__crop2.jpg) 
![](img/14_099_1__crop3.jpg) 
![](img/14_099_1__crop4.jpg) 

- 14_100_0，1)背景虚化;2)车内乘客轮廓有亮线;3)站台边缘的地砖可能是凸起、也可能是缝隙;  
![](img/14_100_0__.jpg) 
![](img/14_100_0__crop0.jpg) 

- 14_100_2,用线条表示车速。但线条表示的车速方向和剧情里的车速方向相反:  
![](img/14_100_2__.jpg)  

- 14_100_3, 用线条表示车速。但线条表示的车速方向和剧情里的车速方向相反:  
![](img/14_100_3__.jpg) 
![](img/14_100_3__crop.jpg) 
 

- 14_102_0, 鞋底的排线阴影:  
![](img/14_102_0__.jpg) 
![](img/14_102_0__crop.jpg)  

- 14_103_0，1)领子处的纹理和肩部的纹理不连续，这更符合实际。但FC里绝大多数情况将整个衬衫简化为一块连续的纹理，例如[14_138_6](#14_138_6)。2）面部边缘有辉光；3）面部整体加阴影，脸颊侧面有排线阴影；4）眼角膜凸起；5）鼻翼处的排线和鼻侧处的排线方向不一致，这衬托出了鼻翼；6）汗珠的尾迹不平滑；7）耳垂处的排线阴影方向大致延续了脸颊侧面的排线方向。:  
![](img/14_103_0__.jpg) 
![](img/14_103_0__crop0.jpg) 
![](img/14_103_0__crop1.jpg) 
![](img/14_103_0__crop2.jpg) 
![](img/14_103_0__crop3.jpg) 


----------------------------------------------------------
<a name="ch096"></a>  
### Vol14	CH096	page109		哪方面较好呢？  	It Doesn't Matter?!  {#ch096}  		  

- 14_109_3,14_109_4,14_109_6，铁丝网虚实相间:  
![](img/14_109_3__.jpg) 
![](img/14_109_3__crop0.jpg)  
![](img/14_109_4__.jpg) 
![](img/14_109_4__crop0.jpg)  
![](img/14_109_6__.jpg) 
![](img/14_109_6__crop0.jpg) 
![](img/14_109_6__crop1.jpg)  

- 14_118_0, 1)面部、颈部大面积排线阴影；2）眼球底部有阴影排线；2）鼻翼处的排线衬托出鼻翼；  
![](img/14_118_0__.jpg) 
![](img/14_118_0__crop0.jpg) 
![](img/14_118_0__crop1.jpg)  
14_100_0和14_118_0的背景相同，但后者机车阴影、地面阴影的着色更重。此外，还发现至少有以下差异(下图第一行为14_100_0，第二行为14_118_0)。（B.T.W.[疑问]由此能否推测出作者的某些漫画制作流程？）    
![](img/14_100_0__crop1.jpg) 
![](img/14_100_0__crop2.jpg)  
![](img/14_118_0__crop2.jpg) 
![](img/14_118_0__crop3.jpg)  

- 14_118_2, 1)二人在地面上的影子（着色粗糙、有渐变）；2）地面凸起的影子；  
![](img/14_118_2__.jpg) 
![](img/14_118_2__crop0.jpg)  

- 14_124_5, 1)喷泉的画法；2）背景的树木的着色:  
![](img/14_124_5__.jpg) 
![](img/14_124_5__crop0.jpg) 
![](img/14_124_5__crop1.jpg)  

- 14_126_0, 1)面部阴影；2）鼻侧的阴影；3）眼皮处的排线阴影；  
![](img/14_126_0__.jpg) 
![](img/14_126_0__crop.jpg)  

- 14_126_1, 1)铁丝网和背景物体间画为实线；2）铁丝网边缘和前景的楼之间画为虚线；2）窗玻璃的着色；3）右上角树叶的着色，较逼真；3）左下角草的画法；  
![](img/14_126_1__.jpg) 
![](img/14_126_1__crop0.jpg) 
![](img/14_126_1__crop1.jpg) 
![](img/14_126_1__crop2.jpg) 
![](img/14_126_1__crop3.jpg) 

- 14_127_2，1）铁丝网着色逼真；2）砖墙的着色；3）右上角树叶的着色；4）灌木丛的着色（明、灰、暗三层）；5）上衣在裙子上的投影、裙子在腿部的投影，都反映出相应的曲面而不是平面；6）地面的影子用排线阴影，排线方向同跑步方向，影子一侧边缘清晰；  
![](img/14_127_2__.jpg) 
![](img/14_127_2__crop0.jpg) 
![](img/14_127_2__crop1.jpg) 
![](img/14_127_2__crop2.jpg) 
![](img/14_127_2__crop3.jpg) 
![](img/14_127_2__crop4.jpg) 
![](img/14_127_2__crop5.jpg) 
![](img/14_127_2__crop6.jpg) 

- 14_128_0, 1）斑驳树影，投在地面和石头上，人身上没有；2）头部在地面的影子为排线，且有影子边线；3）树叶在自身的投影为排线；4）浅葱腿部在地面上的投影为排线，方向大致沿腿的方向；5）浅葱腿部在背包上的投影为淡色网点；    
![](img/14_128_0__.jpg) 
![](img/14_128_0__crop0.jpg) 
![](img/14_128_0__crop1.jpg) 
![](img/14_128_0__crop2.jpg) 
![](img/14_128_0__crop3.jpg) 

- 14_128_2，1）右左背景逐渐简化、淡化；2）树叶在自身的投影为排线  
![](img/14_128_2__.jpg)  

- 14_129_0, 1)浅葱颈部阴影的(大部分)边缘清晰；2）绝大部分背景树木简化为轮廓，但其着色有深浅之分；3）左侧背景树木的粗树干隐约有着色:  
![](img/14_129_0__.jpg) 
![](img/14_129_0__crop0.jpg) 
![](img/14_129_0__crop1.jpg) 
![](img/14_129_0__crop2.jpg) 

- 14_129_1, 1)前景树叶简化为轮廓，白色；2）背景近处树叶着色为暗、灰；3）远处树木着色为白、灰、黑；  
![](img/14_129_1__.jpg) 
![](img/14_129_1__crop0.jpg) 
![](img/14_129_1__crop1.jpg) 
![](img/14_129_1__crop2.jpg) 

- 14_129_3, 整个画面左暗右亮。1）树干边缘有亮边；2）背景近处的树叶右边更亮；3）背景远处的树木有白、灰、暗三层着色，其中白色区域大，所以显得明亮；4）木制栏杆的纹理有白、灰、暗三层着色；  
![](img/14_129_3__.jpg) 
![](img/14_129_3__crop0.jpg) 
![](img/14_129_3__crop1.jpg) 
![](img/14_129_3__crop2.jpg) 
 

- 14_129_5，1）近景树叶逼真；2）树干横向排线阴影，且边缘有亮边；3）近景灌木丛顶部边缘有亮色；4）湖水的着色从远到近由暗变亮:  
![](img/14_129_5__.jpg) 
![](img/14_129_5__crop0.jpg) 
![](img/14_129_5__crop1.jpg) 
![](img/14_129_5__crop2.jpg) 
![](img/14_129_5__crop3.jpg)  
下图左一里浅色叶子的效果可能源于左二照片。照片里亮色的叶子是其正面被阳光照亮、相机从其背面拍摄。   
![](img/14_129_5__crop0.jpg) 
![](img/leaf_0.jpg) 

- 14_130_6, 1)喷泉喷水的一种画法；2）远景树干着灰色、树木间隙着黑色；3)近景树叶着黑色，角落处渐变为亮色（[疑问]是排线吗？）；4）夕阳的效果:  
![](img/14_130_6__.jpg) 
![](img/14_130_6__crop0.jpg) 
![](img/14_130_6__crop1.jpg) 
 

- 14_132_4, 1)地面、石头上的斑驳树影；2）左侧树叶着色相对简单（多数未画叶脉）；3）右侧树叶着色细节相对高，叶脉多为排线阴影；4）地面的人影为灰色网点，和脚近的位置加排线阴影；5）栏杆上靠近人体的位置颜色深；6）石块的着色，大面积白和灰，少量黑色；  
![](img/14_132_4__.jpg) 
![](img/14_132_4__crop0.jpg) 
![](img/14_132_4__crop1.jpg) 
![](img/14_132_4__crop2.jpg) 
![](img/14_132_4__crop3.jpg) 
![](img/14_132_4__crop4.jpg) 
![](img/14_132_4__crop5.jpg)  
下图左一里叶子表面只有部分着色，这种效果可能源于左二左三照片。   
![](img/14_132_4__crop2.jpg) 
![](img/leaf_1.jpg) 
![](img/leaf_2.jpg) 


- 14_133_6, 1)"挤眼睛"的一种画法（这里的"挤眼睛"表情没有皱纹）；2）耳朵后下方的排线阴影；3）锁骨和脖筋的线条象草稿线条，这很特别；4）背带边缘的排线阴影；  
![](img/14_133_6__.jpg) 
![](img/14_133_6__crop0.jpg) 
![](img/14_133_6__crop1.jpg) 
![](img/14_133_6__crop2.jpg) 
![](img/14_133_6__crop3.jpg) 
 

- 14_134_0, 1)树干、树枝的阴影为密集的排线，树叶用排线绘制；2）身体在背包上的投影用排线。以上提到的这些排线方向基本一致。3）浅葱左腿涂黑；4）栏杆上边缘有亮线；  
![](img/14_134_0__.jpg) 
![](img/14_134_0__crop0.jpg) 
![](img/14_134_0__crop1.jpg) 
![](img/14_134_0__crop2.jpg)  

- 14_134_1, 1)树干、树枝的阴影为密集的斜排线，树叶用斜排线绘制；2）紫苑的影子为横向排线，且投影到石头上；3）路人简化为轮廓；4）背景的紫苑、树木、路人显得层次分明；  
![](img/14_134_1__.jpg) 
![](img/14_134_1__crop0.jpg) 
![](img/14_134_1__crop1.jpg)  

- 14_134_3，1）远处树木的底色为灰，树叶在树干上的投影为深灰，树间隙为黑，有层次感；2）路人简化为轮廓；3）近景树干有横向排线阴影，右侧边缘有亮边；4）角色的裙子用灰色网点和排线阴影，排线的方向或许表现了裙子的曲面；  
![](img/14_134_3__.jpg) 
![](img/14_134_3__crop0.jpg) 
![](img/14_134_3__crop1.jpg) 
![](img/14_134_3__crop2.jpg) 
![](img/14_134_3__crop3.jpg)  

- 14_134_5, 1）近景灌木高对比度着色，可能是表现阳光强烈；2）树干上的斑驳树影，着色至少有4个层次；3）近处树叶的着色为密集排线，可能表示阳光照射的方向，且树叶有深浅的层次感；4）远处树影在地面和灌木丛上的投影；5）更远处的树木有灰色排线阴影；    
![](img/14_134_5__.jpg) 
![](img/14_134_5__crop0.jpg) 
![](img/14_134_5__crop1.jpg) 
![](img/14_134_5__crop2.jpg) 
![](img/14_134_5__crop3.jpg) 
![](img/14_134_5__crop4.jpg) 
 

- 14_135_3，1）镜头仰视时树叶的着色；2）角色裙子小腹下方的排线阴影，有一条左上至右下的斜线表示阴影边缘；   
![](img/14_135_3__.jpg) 
![](img/14_135_3__crop0.jpg) 
![](img/14_135_3__crop1.jpg)  


----------------------------------------------------------
<a name="ch097"></a>  
### Vol14	CH097	page137		一天的情人  	Couple For A Night  {#ch097}  		  

- 14_137_0, 1）锁骨处的排线阴影；2）胸前衣服上的着色，有点复杂；3）衣服上的排线阴影和褶皱；4）裤子两侧有网点阴影；5）右胳膊的排线阴影；6）左手的着色:  
![](img/14_137_0__.jpg) 
![](img/14_137_0__crop0.jpg) 
![](img/14_137_0__crop1.jpg) 
![](img/14_137_0__crop2.jpg) 
![](img/14_137_0__crop3.jpg) 
![](img/14_137_0__crop4.jpg)  
![](img/14_137_0__crop5.jpg) 

<a name="14_138_6"></a>  
#### 14_138_6  
- 14_138_6, 1)角色的影子、窗子的影子为排线阴影；2）窗户的屋内省略为网点和竖线阴影:  
![](img/14_138_6__.jpg) 
![](img/14_138_6__crop0.jpg) 
![](img/14_138_6__crop1.jpg)  

- 14_139_1, 1)人影为网点；2）窗户的影子。[疑问]红框内是窗框的影子？ 3）屋檐、墙壁、屋顶的着色相间隔；4）窗玻璃后面物体的透射，用了虚线；5）窗玻璃角落处有阴影；  
![](img/14_139_1__.jpg) 
![](img/14_139_1__crop0.jpg) 
![](img/14_139_1__crop1.jpg) 
![](img/14_139_1__crop2.jpg) 
![](img/14_139_1__crop3.jpg)  

- 14_139_5, 雅彦整体加了粗糙的网点阴影:  
![](img/14_139_5__.jpg) 
![](img/14_139_5__crop0.jpg)  

- 14_140_5, 摩天轮中心处的辐条省略未画。（B.T.W.我感觉摩天轮支架的透视和轮的透视不一致）:  
![](img/14_140_5__.jpg) 
![](img/14_140_5__crop0.jpg) 
![](img/14_140_5__crop1.jpg) 

- 14_141_1, 1）底座和脚附近的排线阴影很深，和周围的白色对比强烈。[疑问]是否应该加上灰色网点阴影？这是否能推测处作者的漫画制作流程？    
![](img/14_141_1__.jpg) 
![](img/14_141_1__crop0.jpg) 
![](img/14_141_1__crop1.jpg) 

- 14_143_5，路人的头发的画法：好像是只有线条、没有涂色：  
![](./img/14_143_5__.jpg)  

- 14_146_6, 1)眼角膜未凸起；2）面部的网点阴影、排线阴影；  
![](img/14_146_6__.jpg) 
![](img/14_146_6__crop0.jpg) 

- 14_147_2, 过山车设施着色逼真:  
![](img/14_147_2__.jpg) 
![](img/14_147_2__crop0.jpg) 
 

- 14_148_1，[疑问]左侧大面积的圆形亮斑是什么？  
![](img/14_148_1__.jpg)  

- 14_149_4， 1）灯泡的边线用虚线，表示明亮；2）为灯加了光晕；  
![](img/14_149_4__.jpg) 
![](img/14_149_4__crop0.jpg) 
 

- 14_153_5, 虽然为暗环境，但只全局加了灰色网点:  
![](img/14_153_5__.jpg)  

- 14_155_4, 1)腿部阴影排线；2）胳膊投影的排线，有影子边线；3）远处人物简化为交错排线；4）更远处的黑色背景用排线过渡；  
![](img/14_155_4__.jpg) 
![](img/14_155_4__crop0.jpg) 
![](img/14_155_4__crop1.jpg) 
![](img/14_155_4__crop2.jpg) 
![](img/14_155_4__crop3.jpg) 

- 14_156_1, 1）速度线条之间有灰色网点；2）近处的灰色网点渐变到远处的黑色；  
![](img/14_156_1__.jpg) 
![](img/14_156_1__crop0.jpg) 

- 14_156_7, 实际为暗环境。近处白色渐变到远处的黑色:  
![](img/14_156_7__.jpg) 
![](img/14_156_7__crop0.jpg) 

- 14_159_1，实际为无光环境。用黑色渐变网点、白色影子表示:  
![](img/14_159_1__.jpg)  

----------------------------------------------------------
<a name="ch098"></a>  
### Vol14	CH098	page163		雅彦的剧本  	Masahiko's Scenario  {#ch098}  		  

- 14_166_0，喷泉喷水的一种画法:  
![](img/14_166_0__.jpg)  

- 14_166_1, 1)近处树叶用密集斜排线，且有深浅不同；2）树干上有斑驳树荫，树干边缘有亮边；3）右侧近处树叶简化为黑色轮廓，内部用灰色，加有斜排线。这种斜排线有阳光照射的方向感；4）远处树木简化为白色、灰色轮廓，树木间隙着黑色；5）近处灌木丛的着色有白、灰、黑三个层次；6）为保持神秘感，人物头部肩部涂黑；  
![](img/14_166_1__.jpg) 
![](img/14_166_1__crop0.jpg) 
![](img/14_166_1__crop1.jpg) 
![](img/14_166_1__crop2.jpg) 
![](img/14_166_1__crop3.jpg) 
![](img/14_166_1__crop4.jpg)  

- 14_166_5，1）背景近处的树叶着灰色网点，加斜排线阴影；2）再远处的树叶简化为边缘线；3）再远处的树木只有轮廓，没有边缘线:  
![](img/14_166_5__.jpg)  

- 14_167_2, 1）树干上有斑驳树荫;2)近处树叶有灰色和黑色，加斜阴影排线；3）近处灌木丛的着色有白、灰、黑三个层次；4）远处树木的着色有白、灰、黑三个层次；5）再远处的树木简化为白、灰两个层次，没有边缘线；  
![](img/14_167_2__.jpg) 
![](img/14_167_2__crop0.jpg) 
![](img/14_167_2__crop1.jpg) 
![](img/14_167_2__crop2.jpg) 
![](img/14_167_2__crop3.jpg)  

- 14_168_0, 1）地面、石头上的斑驳树荫；2）灌木丛、树干上的斑驳树荫；3）远处树干颜色浅；4）角色影子用排线阴影；  
![](img/14_168_0__.jpg) 
![](img/14_168_0__crop0.jpg) 
![](img/14_168_0__crop1.jpg) 
![](img/14_168_0__crop2.jpg) 
![](img/14_168_0__crop3.jpg)  

- 14_170_3, 1)树叶高对比度，表示阳光强烈；2）有些窗框用虚线，表示反射的阳光强烈；3）有些窗玻璃用渐变网点；  
![](img/14_170_3__.jpg) 
![](img/14_170_3__crop0.jpg) 
![](img/14_170_3__crop1.jpg) 
![](img/14_170_3__crop2.jpg) 

- 14_172_4, 1)黑色裤子用灰色高光；2）凳子边缘用排线；3）近景角色面部加阴影，面部侧面加排线；  
![](img/14_172_4__.jpg) 
![](img/14_172_4__crop0.jpg) 
![](img/14_172_4__crop1.jpg) 
![](img/14_172_4__crop2.jpg)  

- 14_175_0，背景窗户简化为轮廓，且有渐变:  
![](img/14_175_0__.jpg)  

- 14_176_3, 1)头发的高光有白和灰；2）面部阴影；3）颈部阴影；4）前景人物左亮右暗；  
![](img/14_176_3__.jpg) 
![](img/14_176_3__crop0.jpg) 
![](img/14_176_3__crop1.jpg) 
![](img/14_176_3__crop2.jpg) 

- 14_180_0，1）近景树木由树根到树梢逐渐边暗；2）树干上的排线阴影、灰色网点，树荫斑驳；3）近处的不同树枝用不同方向的排线；4）近处的树叶用白色轮廓、黑色轮廓表示层次；5）远处树枝为黑色；6）远处树叶为黑色轮廓、白色轮廓；7）近处石凳的不同侧面着色深浅不同，其阴影为灰色网点；8）舞台穹顶的粗糙网点渐变；  
![](img/14_180_0__.jpg) 
![](img/14_180_0__crop0.jpg) 
![](img/14_180_0__crop1.jpg) 
![](img/14_180_0__crop2.jpg) 
![](img/14_180_0__crop3.jpg) 
![](img/14_180_0__crop4.jpg) 
![](img/14_180_0__crop5.jpg) 
![](img/14_180_0__crop6.jpg) 

<a name="14_184_0"></a>  
#### 14_184_0  
- 14_184_0, 画面较细腻。1）整个画面的排线方向统一，能表现出光线的方向，同时有较粗的一缕缕阳光线；2）地面树荫斑驳；3）**角色身上有斑驳树影**，树干、角色在地面的影子为较细腻的黑色网点；4）树干上有斑驳树影，着色有白灰黑三层次，边缘有亮线；5）从近景树叶上可以清晰地看出排线；6）中景树叶逼真，排线深浅不同；7）远景树木的着色有白灰黑三层次；  
![](img/14_184_0__.jpg) 
![](img/14_184_0__crop0.jpg) 
![](img/14_184_0__crop1.jpg) 
![](img/14_184_0__crop2.jpg) 
![](img/14_184_0__crop3.jpg) 
![](img/14_184_0__crop4.jpg) 
![](img/14_184_0__crop5.jpg) 
![](img/14_184_0__crop6.jpg)  

- 14_184_1，**角色面部有斑驳树影**（网点），且深浅不同:  
![](img/14_184_1__.jpg)  

- 14_184_4, 1）树干、角色在地面的影子，[疑问]这是网点阴影还是密集的排线阴影？2)近景人物身上有斑驳树影，白色噪点疏密不同。其余特点同[14_184_0](#14_184_0);     
![](img/14_184_4__.jpg) 
![](img/14_184_4__crop0.jpg) 
![](img/14_184_4__crop1.jpg) 
![](img/14_184_4__crop2.jpg) 

- 14_185_2, 1)人物身上无斑驳树影；2）背景树木的排线方向统一；3）背景近处树木着色深；4）背景远处树木着色浅、简化为轮廓，从上到下渐变为灰色排线，再渐变为白色；  
![](img/14_185_2__.jpg) 
![](img/14_185_2__crop0.jpg) 
![](img/14_185_2__crop1.jpg) 

- 14_185_4, 1)近景树叶背面着深色；2）地面、人物身上树影斑驳，且人物身上的树影反映出身体的形状(曲面)；3）灌木、树根、角色身体在地面的影子的方向一致(下图绿线)，但这个方向和一缕缕的阳光方向不一致；4）灌木、树根自身的投影（下图蓝框）的排线方向（下图红色）和和一缕缕的阳光方向基本一致；  
![](img/14_185_4__.jpg) 
![](img/14_185_4__crop0.jpg) 
![](img/14_185_4__crop1.jpg) 
![](img/14_185_4__crop2.jpg) 
![](img/14_185_4__crop3_mod.jpg) 
![](img/14_185_4__crop4_mod.jpg) 

- 14_186_5, 1)远处的栏杆简化为黑白高对比度；2）远处角色在地面的阴影象是色块（[疑问]这是什么着色方式？）；3）远处角色的上半身是排线阴影，排线方向和一缕缕光线方向基本一致。其余特点同[14_184_0](#14_184_0)；    
![](img/14_186_5__.jpg) 
![](img/14_186_5__crop0.jpg) 
![](img/14_186_5__crop1.jpg) 
![](img/14_186_5__crop2.jpg) 
![](img/14_186_5__crop3.jpg) 
![](img/14_186_5__crop4.jpg) 
![](img/14_186_5__crop5.jpg) 
![](img/14_186_5__crop6.jpg) 

- 14_187_0, 1)人物身上有斑驳树影；2）颈部排线阴影的方向和一缕缕阳光的方向不一致；3）背景近处树叶着白色，画主叶脉；4）背景稍远处基本色为灰网点，树干斜排线阴影；5）背景再远处的树叶用排线着色；6）背景再远处的树木；  
![](img/14_187_0__.jpg) 
![](img/14_187_0__crop0.jpg) 
![](img/14_187_0__crop1.jpg) 
![](img/14_187_0__crop2.jpg) 
![](img/14_187_0__crop3.jpg) 
![](img/14_187_0__crop4.jpg) 
![](img/14_187_0__crop5.jpg) 
![](img/14_187_0__crop6.jpg) 

- 14_187_1, 导演大喊“开始”的效果结合阳光强烈的效果:  
![](img/14_187_1__.jpg) 
![](img/14_187_1__crop0.jpg) 
![](img/14_187_1__crop1.jpg) 
![](img/14_187_1__crop2.jpg) 

- 14_187_3, 镜头里景物的着色。1）人物身上无斑驳树影；2）树木从近到远的着色；  
![](img/14_187_3__.jpg) 
![](img/14_187_3__crop0.jpg) 
![](img/14_187_3__crop1.jpg) 
![](img/14_187_3__crop2.jpg) 
![](img/14_187_3__crop3.jpg) 

----------------------------------------------------------
<a name="ch099"></a>  
### Vol14	CH099	page189		改变剧情  	Change Of Course  {#ch099}  		  

- 14_190_2，电话着色逼真:  
![](img/14_190_2__.jpg)  

- 14_195_4, 14_196_5, 草木高对比度:  
![](img/14_195_4__.jpg) 
![](img/14_195_4__crop0.jpg) 
![](img/14_195_4__crop1.jpg)  
![](img/14_196_5__.jpg) 
![](img/14_196_5__crop0.jpg)  

- 14_196_3, 1)毛巾的排线阴影；2）毛巾的边缘为虚线，表现其毛茸茸的质感；  
![](img/14_196_3__.jpg) 
![](img/14_196_3__crop0.jpg) 
![](img/14_196_3__crop1.jpg) 

- 14_196_4, 1)毛巾的排线阴影；2)毛巾的自身投影；3）面部着色；4）颈部排线阴影；  
![](img/14_196_4__.jpg) 
![](img/14_196_4__crop0.jpg) 
![](img/14_196_4__crop1.jpg) 
![](img/14_196_4__crop2.jpg) 
![](img/14_196_4__crop3.jpg) 
![](img/14_196_4__crop4.jpg) 
 

- 14_199_1, 1)前景树叶曝光度高；2）人物影子为灰网点+排线；3）背景植被有白色高光；  
![](img/14_199_1__.jpg) 
![](img/14_199_1__crop0.jpg) 
![](img/14_199_1__crop1.jpg) 
![](img/14_199_1__crop2.jpg) 

- 14_199_6, 1)杆子底部的背景为白色，所以杆左边缘线为黑线；2）杆子中部背景为暗色，所以杆此处左边缘线为白线；3）杆子在矮墙上的影子，上边缘处影子的位移表现处此处的墙边缘的凸起；4）背景植被从近到远，暗色区域逐渐减少；  
![](img/14_199_6__.jpg) 
![](img/14_199_6__crop0.jpg) 
![](img/14_199_6__crop1.jpg) 
![](img/14_199_6__crop2.jpg) 
![](img/14_199_6__crop3.jpg) 
![](img/14_199_6__crop4.jpg) 

- 14_200_0, 1）眼部的着色；2）耳朵后方的阴影排线方向大约与耳廓边缘垂直；3）背景近处树木为黑白高对比度；4）背景远处树木为黑灰白三层次；5）背景最远处的树木简化为灰色轮廓，树间隙为白色；  
![](img/14_200_0__.jpg) 
![](img/14_200_0__crop0.jpg) 
![](img/14_200_0__crop1.jpg) 
![](img/14_200_0__crop2.jpg) 
![](img/14_200_0__crop3.jpg) 

- 14_200_2, 1)长椅上的人影；2）背景植被的画法（排线线条粗且边缘不规则，应该画笔有点特别）；  
![](img/14_200_2__.jpg) 
![](img/14_200_2__crop0.jpg) 
![](img/14_200_2__crop1.jpg) 
 

- 14_200_5, 1)近景树叶整体加灰网点，有辉光效果([疑问]为什么？)，且线条粗细有层次感；2）背景灌木丛的灰色网点，自右下到左上逐渐减少；3）远处的树叶简化为黑色轮廓；  
![](img/14_200_5__.jpg) 
![](img/14_200_5__crop0.jpg) 
![](img/14_200_5__crop1.jpg) 
![](img/14_200_5__crop2.jpg)  

- 14_201_0, 背景树叶没有边线，斜排线着色:  
![](img/14_201_0__.jpg) 
![](img/14_201_0__crop0.jpg)  

- 14_201_5, 1)紫苑黑裤子上的高光为灰色；2）两腿的高光差异大，说明两腿的姿势不同（腿姿势见[14_202_0](#14_202_0), 可见作者画得准确！）；3）长椅金属架的着色，从下到上逐渐明亮；4）背景树干灰色排线，深浅不一；5）背景树叶着灰色和白色，远处更白；  
![](img/14_201_5__.jpg) 
![](img/14_201_5__crop0.jpg) 
![](img/14_201_5__crop1.jpg) 
![](img/14_201_5__crop2.jpg) 
![](img/14_201_5__crop3.jpg) 
![](img/14_201_5__crop4.jpg) 

<a name="14_202_0"></a>  
#### 14_202_0  
- 14_202_0, 1)垃圾桶的网很逼真；2）背包底部的着色较逼真，象是油画的笔触；3）近处背景树木；4）远处有一处树木，其画法可能是在最后步骤添加了深色的排线阴影；  
![](img/14_202_0__.jpg) 
![](img/14_202_0__crop0.jpg) 
![](img/14_202_0__crop1.jpg) 
![](img/14_202_0__crop2.jpg) 
![](img/14_202_0__crop3.jpg) 

- 14_202_2, 1）背景的树叶曝光度大；2）背景的树叶的影子用密集斜排线；  
![](img/14_202_2__.jpg) 
![](img/14_202_2__crop0.jpg)  

- 14_204_1, 1)背景近处树木细节度稍高；2）近处灌木简化；3）远处树木简化为无边线；4）最远的树木简化为灰轮廓，树间隙为白色；  
![](img/14_204_1__.jpg) 
![](img/14_204_1__crop0.jpg) 
![](img/14_204_1__crop1.jpg) 
![](img/14_204_1__crop2.jpg) 

- 14_204_3, 1)背景树木为黑白高对比度，表现阳光强烈；2）建筑表面的渐变着色；3）上方和下方的阳光方向不平行，暗示太阳的位置；  
![](img/14_204_3__.jpg) 
![](img/14_204_3__crop0.jpg) 
![](img/14_204_3__crop1.jpg) 

- 14_204_4, 背景树木的画法。（B.T.W.紫苑肩膀的连线和长椅不平行，考虑到后续紫苑挎包离开，这里可能她有动作）:  
![](img/14_204_4__.jpg) 
![](img/14_204_4__crop0.jpg) 
![](img/14_204_4__crop1.jpg)  

- 14_204_7, 1)紫苑胳膊在上衣(浅色)投下的影子，阴影排线且有渐变；2)紫苑胳膊在裤子(深色)投下的影子画为黑色；3）臀部的交错排线阴影；4）黑色裤子在臀部有大面积高光；5）背景树木的着色；  
![](img/14_204_7__.jpg) 
![](img/14_204_7__crop0.jpg) 
![](img/14_204_7__crop1.jpg) 
![](img/14_204_7__crop2.jpg) 
![](img/14_204_7__crop3.jpg) 

- 14_205_2, 1）左下角近景树叶着色为灰、黑，灰色似乎有渐变；2）长椅最顶部的缝隙颜色浅；3）右上角近景树叶着色黑，灰色应该是背景；4）杆子影子投在灌木丛上；  
![](img/14_205_2__.jpg) 
![](img/14_205_2__crop0.jpg) 
![](img/14_205_2__crop1.jpg) 
![](img/14_205_2__crop2.jpg) 
![](img/14_205_2__crop3.jpg) 

- 14_208，背景人物有点秃顶：  
![](./img/14_208_0__.jpg)  

- 14_209_0, 1)近景树叶逼真；2）背景路人腿窝有两点，表示凹陷；3）叶子腿部的排线阴影未反映出腿部的曲面；4）人物在地面的影子为排线阴影；  
![](img/14_209_0__.jpg) 
![](img/14_209_0__crop0.jpg) 
![](img/14_209_0__crop1.jpg) 
![](img/14_209_0__crop2.jpg) 
![](img/14_209_0__crop3.jpg) 
![](img/14_209_0__crop4.jpg) 


----------------------------------------------------------
<a name="ch100"></a>  
### Vol14	CH100	page215		浅葱的反击  	Asagi's Retaliation  {#ch100}  		  

- 14_221_3, 物体透过玻璃窗的效果:  
![](img/14_221_3__.jpg) 
![](img/14_221_3__crop0.jpg) 
![](img/14_221_3__crop1.jpg) 

- 14_221_4, 1)玻璃上的反射效果；2）屋顶板条透过玻璃窗的效果；3）窗框的边缘线和两条缝隙线；4）人物面部侧面的排线阴影；  
![](img/14_221_4__.jpg) 
![](img/14_221_4__crop0.jpg) 
![](img/14_221_4__crop1.jpg) 
![](img/14_221_4__crop2.jpg) 
![](img/14_221_4__crop3.jpg) 

- 14_222_2, 1)灯光透过玻璃门照在地面；2）人影在墙壁的投影；3）暗环境下墙壁的边线未画出，只有不同侧面的不同着色；  
![](img/14_222_2__.jpg) 
![](img/14_222_2__crop0.jpg) 
![](img/14_222_2__crop1.jpg) 
![](img/14_222_2__crop2.jpg) 
![](img/14_222_2__crop3.jpg) 

- 14_222_3, 1)面部整体网点阴影，脸侧面的排线阴影；2）红框内的排线可能表示对侧的眉毛；  
![](img/14_222_3__.jpg) 
![](img/14_222_3__crop0.jpg) 
![](img/14_222_3__crop1.jpg) 

- 14_224_1, 屋内椅子、窗帘透过玻璃的效果:  
![](img/14_224_1__.jpg) 
![](img/14_224_1__crop0.jpg) 

- 14_226_2, 面部阴影排线:  
![](img/14_226_2__.jpg) 
![](img/14_226_2__crop0.jpg) 
![](img/14_226_2__crop1.jpg)  

- 14_227_3, 1）暗环境下，墙面的边线模糊或未画出；2）窗帘透过灯光的效果；3）窗帘透过玻璃的效果；  
![](img/14_227_3__.jpg) 
![](img/14_227_3__crop0.jpg) 

<a name="14_228_0"></a>
#### 14_228_0  
- 14_228_0, 1)若苗紫脚下的阴影排线呈辐射状，表示光线的方向；2）灯光有一个圆形的光照区域；3）窗帘在玻璃上的反射；4）玻璃的着色；  
![](img/14_228_0__.jpg) 
![](img/14_228_0__crop0.jpg) 
![](img/14_228_0__crop1.jpg) 
![](img/14_228_0__crop2.jpg) 

- 14_228_5, 1)沙发处的排线阴影;2)暗环境下角色在沙发上的影子；3）角色头发加了高光。其他特点同[14_228_0](#14_228_0):  
![](img/14_228_5__.jpg) 
![](img/14_228_5__crop0.jpg)  

- 14_230_1, 1）面部排线阴影；2）右侧对话框两侧的阴影不同；  
![](img/14_230_1__.jpg) 
![](img/14_230_1__crop0.jpg) 
 
- 14_231_4, 1）背景近景树叶为黑色；2）背景稍远树叶为黑色+灰色斜排线；3）背景更远处的树干简化为黑白高对比度着色；4）人物腋下的阴影；  
![](img/14_231_4__.jpg) 
![](img/14_231_4__crop0.jpg) 
![](img/14_231_4__crop1.jpg) 
![](img/14_231_4__crop2.jpg) 
![](img/14_231_4__crop3.jpg) 
![](img/14_231_4__crop4.jpg) 

- 14_231_5，人物面部的斑驳树影:  
![](img/14_231_5__.jpg)  

- 14_232_5, 1)近景树叶为黑色+斜排线；2）远处树木用大面积渐变灰网点；3）近景树干上的灰色排线；  
![](img/14_232_5__.jpg) 
![](img/14_232_5__crop0.jpg) 
![](img/14_232_5__crop1.jpg) 
![](img/14_232_5__crop2.jpg) 
 

- 14_234_2, 鼻子另一侧的着色:  
![](img/14_234_2__.jpg) 
![](img/14_234_2__crop0.jpg) 

- 14_234_3, 面部的排线阴影:  
![](img/14_234_3__.jpg) 
![](img/14_234_3__crop0.jpg)  

- 14_237_2, 1)近景黑色树叶加白色排线，高对比度，表示阳光强烈；2）人物衣服上的斑驳树影为黑色，高对比度；3）胳膊在衣服上的影子为渐变排线阴影, 且有边缘线；   
![](img/14_237_2__.jpg) 
![](img/14_237_2__crop0.jpg) 
![](img/14_237_2__crop1.jpg) 
 

- 14_237_3, 1）背景树木着色为网点+排线；2）背景树木简化为排线，且无边线；  
![](img/14_237_3__.jpg) 
![](img/14_237_3__crop0.jpg) 
![](img/14_237_3__crop1.jpg) 

- 14_239_1,14_239_2, 一缕缕阳光多数位于角色背景:  
![](img/14_239_1__.jpg) 
![](img/14_239_1__crop0.jpg)  
![](img/14_239_2__.jpg) 
![](img/14_239_2__crop0.jpg) 
![](img/14_239_2__crop1.jpg) 

- 14_239_4, 1）人物衣服上的斑驳树影; 2）胳膊在衣服上的影子为渐变排线阴影, 且有边缘线；   
![](img/14_239_4__.jpg) 
![](img/14_239_4__crop0.jpg)  

- 14_239_6，人物面部的斑驳树影:  
![](img/14_239_6__.jpg)  

- 14_241_7, 1)背景树干的着色有点特别；2）左上角这一块整齐的排线（[疑问]这是什么效果？）；3）背景树叶的着色；  
![](img/14_241_7__.jpg) 
![](img/14_241_7__crop0.jpg) 


----------------------------------------------------------
<a name="ch101"></a>  
### Vol14	CH101	page243		奥多摩的分手  	Split At Okutama  {#ch101}  		  

- 14_243_0, 1)人物面部的斑驳树影；2）上衣的着色表现出了布料的质感；3）裤子的深灰网点有渐变，斑驳树影和面部的不同；  
![](img/14_243_0__.jpg) 
![](img/14_243_0__crop0.jpg) 
![](img/14_243_0__crop1.jpg) 

- 14_244_2, 1)头部在玻璃上的阴影；2）窗外的树木有排线表示速度；3）窗外的建筑几乎无排线，最远处的山无排线；  
![](img/14_244_2__.jpg) 
![](img/14_244_2__crop0.jpg) 
![](img/14_244_2__crop1.jpg) 
![](img/14_244_2__crop2.jpg)  

- 14_246_2, 1)脸颊侧面的排线阴影；2）鼻子处的着色；  
![](img/14_246_2__.jpg) 
![](img/14_246_2__crop0.jpg) 
![](img/14_246_2__crop1.jpg) 
 

- 14_246_3, 树木的着色:  
![](img/14_246_3__.jpg) 
![](img/14_246_3__crop0.jpg) 
![](img/14_246_3__crop1.jpg) 
![](img/14_246_3__crop2.jpg) 

- 14_247_5, 远处的树木有白色的斜排线；  
![](img/14_247_5__.jpg) 
![](img/14_247_5__crop0.jpg) 
 

- 14_247_7， 1)车厢内地板有倒影;2)鞋带着色逼真；  
![](img/14_247_7__.jpg) 
![](img/14_247_7__crop0.jpg)  

- 14_249_0, 1)腿部的阴影从黑色过渡到灰色，用排线过渡；2）背景竖线阴影；  
![](img/14_249_0__.jpg) 
![](img/14_249_0__crop0.jpg) 
![](img/14_249_0__crop1.jpg) 

- 14_251_3, 1)颈部的排线阴影；2）嘴唇的排线着色；3）鼻子的排线阴影；  
![](img/14_251_3__.jpg) 
![](img/14_251_3__crop0.jpg) 
![](img/14_251_3__crop1.jpg) 
![](img/14_251_3__crop2.jpg) 

- 14_251_5， 14_251_6，桌子上手的影子用的是浅灰网点阴影:  
![](img/14_251_5__.jpg) 
![](img/14_251_5__crop0.jpg)  
![](img/14_251_6__.jpg) 
![](img/14_251_6__crop0.jpg)  

- 14_253_0, 河面波光粼粼:  
![](img/14_253_0__.jpg) 
![](img/14_253_0__crop0.jpg) 
![](img/14_253_0__crop1.jpg)  

- 14_253_1, 1)远处的树叶；2）远处的树干，树干间隙涂黑；3）远处的斑驳树荫和一缕缕光线；4）近处河水波光粼粼；5）人物和背景有很大的间隙，间隙处的一缕缕光线明显:  
![](img/14_253_1__.jpg) 
![](img/14_253_1__crop0.jpg) 
![](img/14_253_1__crop1.jpg) 
![](img/14_253_1__crop2.jpg) 
![](img/14_253_1__crop3.jpg) 
![](img/14_253_1__crop4.jpg) 

- 14_253_5, 背景里的树影斑驳，树叶为黑色轮廓、白色轮廓、灰色轮廓:  
![](img/14_253_5__.jpg) 
![](img/14_253_5__crop0.jpg) 
![](img/14_253_5__crop1.jpg) 

- 14_255_0, 河水的倒影；倒影的着色简化；水面的着色。  
![](img/14_255_0__.jpg) 
![](img/14_255_0__crop0.jpg) 
![](img/14_255_0__crop1.jpg) 
![](img/14_255_0__crop2.jpg) 
![](img/14_255_0__crop3.jpg) 

- 14_255_1，桥着色逼真:  
![](img/14_255_1__.jpg) 
![](img/14_255_1__crop0.jpg) 

- 14_255_4, 1)近处河岸有渐变网点阴影；2）河水波光粼粼的效果有辉光；3）河对岸植被的着色；3）河对岸植被的顶部的叶子有灰色白色的排线，这能表现出阳光照射的方向；  
![](img/14_255_4__.jpg) 
![](img/14_255_4__crop0.jpg) 
![](img/14_255_4__crop1.jpg) 
![](img/14_255_4__crop2.jpg) 
![](img/14_255_4__crop3.jpg) 

- 14_255_5, 1)背景植被被简化；2）左侧的背景颜色比右侧的浅:  
![](img/14_255_5__.jpg) 
![](img/14_255_5__crop0.jpg) 
![](img/14_255_5__crop1.jpg)  

- 14_256_3, 1)近景石头的着色；2）河水波光粼粼，有些区域有辉光效果；3）对岸的石头；4）远处的树荫；5）远处的树干；6）远处的树叶，有灰色或白色斜排线；7）远处的桥融入了环境；  
![](img/14_256_3__.jpg) 
![](img/14_256_3__crop0.jpg) 
![](img/14_256_3__crop1.jpg) 
![](img/14_256_3__crop2.jpg) 
![](img/14_256_3__crop3.jpg) 
![](img/14_256_3__crop4.jpg) 
![](img/14_256_3__crop5.jpg) 

- 14_256_4，脸颊侧面的阴影排线:  
![](img/14_256_4__.jpg)  

- 14_257_1, 1)雅美胳膊在衣服上的投影和以往不同，从上到下逐渐变深；2）叶子锁骨处的着色，左边排线，右边似乎是草稿式的线条；3）背景简化为灰色轮廓，向下渐变为白色；  
![](img/14_257_1__.jpg) 
![](img/14_257_1__crop0.jpg) 
![](img/14_257_1__crop1.jpg) 
![](img/14_257_1__crop2.jpg) 

- 14_260_0, 背景基色为深灰。1）护栏的高光，地面排线阴影；2）右侧地面排线阴影；3）近景树干着黑色，背景深灰；4）路灯在地面的光照；5）电线的着色；  
![](img/14_260_0__.jpg) 
![](img/14_260_0__crop0.jpg) 
![](img/14_260_0__crop1.jpg) 
![](img/14_260_0__crop2.jpg) 
![](img/14_260_0__crop3.jpg) 
![](img/14_260_0__crop4.jpg) 

- 14_260_6, 1)面部加部分网点阴影；2）面颊侧面的排线阴影；  
![](img/14_260_6__.jpg) 
![](img/14_260_6__crop0.jpg) 

- 14_261_6, 1)路面的渐变网点；2）路灯的光晕；3）背景植被的大面积黑色排线；4）背景植被为黑色、灰色轮廓；  
![](img/14_261_6__.jpg) 
![](img/14_261_6__crop0.jpg) 
![](img/14_261_6__crop1.jpg) 
![](img/14_261_6__crop2.jpg) 
![](img/14_261_6__crop3.jpg) 

- 14_263_1, 1)背景排线表示强光照射的效果；2）面部网点阴影；3）衣服上的着色的层次--白、浅灰、深灰或排线、黑；  
![](img/14_263_1__.jpg) 
![](img/14_263_1__crop0.jpg) 
![](img/14_263_1__crop1.jpg) 
![](img/14_263_1__crop2.jpg) 

- 14_263_3, 1)环境在车表面的反射，着色有渐变；2）强光照射下的背景植被--网点渐变、树枝高对比度:  
![](img/14_263_3__.jpg) 
![](img/14_263_3__crop0.jpg) 
![](img/14_263_3__crop1.jpg) 

- 14_264_4, 1）护栏柱子的着色，柱子的影子有半影区；2）护栏的着色，反射灯光；3）车灯的光晕，地面反射车灯灯光；4）部分背景植被被车灯照亮；5）远处植被的着色；6）远处到更远处植被的层次；  
![](img/14_264_4__.jpg) 
![](img/14_264_4__crop0.jpg) 
![](img/14_264_4__crop1.jpg) 
![](img/14_264_4__crop2.jpg) 
![](img/14_264_4__crop3.jpg) 
![](img/14_264_4__crop4.jpg) 
![](img/14_264_4__crop5.jpg) 

- 14_264_5，1）面部侧面阴影排线；2）对侧眼睛省略为阴影；  
![](img/14_264_5__.jpg)  

- 14_265_4, 车内被车灯反射光照亮。1)面部排线阴影；2）手臂和方向盘的着色；3）车门内侧的排线和高光；4）车座的着色；5）雅彦背部有少量排线；6）雅彦在车窗玻璃上的影子或反射（白色）；7）雅彦手指处有排线；8）雅彦胳膊在车门内侧的投影（渐变排线）；  
![](img/14_265_4__.jpg) 
![](img/14_265_4__crop0.jpg) 
![](img/14_265_4__crop1.jpg) 
![](img/14_265_4__crop2.jpg) 
![](img/14_265_4__crop3.jpg) 
![](img/14_265_4__crop4.jpg) 
![](img/14_265_4__crop5.jpg) 
![](img/14_265_4__crop6.jpg) 

- 14_266_4, 1)车玻璃底色为白，人物在车窗玻璃上的影子或反射为灰色渐变；2）车座的着色；  
![](img/14_266_4__.jpg) 
![](img/14_266_4__crop0.jpg) 
![](img/14_266_4__crop1.jpg) 

----------------------------------------------------------
<a name="ch102"></a>  
### Vol14	CH102	page269		家庭(最终话)  	Last Day - End  {#ch102}  		  

- 14_271_0, 着色逼真:  
![](img/14_271_0__.jpg) 
![](img/14_271_0__crop0.jpg) 

- 14_272_6, 1)下颌和颈部的着色；2）腋下的着色；  
![](img/14_272_6__.jpg) 
![](img/14_272_6__crop0.jpg) 
![](img/14_272_6__crop1.jpg) 

- 14_274_2, 1）面部侧面的排线阴影；2）口腔内部的着色；  
![](img/14_274_2__.jpg) 
![](img/14_274_2__crop0.jpg)  

- 14_276_4, 角色腿部的排线阴影:  
![](img/14_276_4__.jpg) 
![](img/14_276_4__crop0.jpg)  

- 14_277_7, 1)眼部排线阴影；2）面部侧面的排线阴影；  
![](img/14_277_7__.jpg) 
![](img/14_277_7__crop0.jpg) 
![](img/14_277_7__crop1.jpg) 

- 14_280_1, 沙子(和水?)溅起:  
![](img/14_280_1__.jpg) 
![](img/14_280_1__crop0.jpg)  

- 14_283_0, 1)面部侧面排线阴影；2）耳朵后面的排线阴影；3）鼻翼处的排线阴影；4）海面波光粼粼:  
![](img/14_283_0__.jpg) 
![](img/14_283_0__crop0.jpg) 
![](img/14_283_0__crop1.jpg) 
![](img/14_283_0__crop2.jpg) 

- 14_283_1, 1）眼部的排线阴影；2）眼球上边缘附近的灰网点阴影；3）眼球上边缘的排线阴影:  
![](img/14_283_1__.jpg) 
![](img/14_283_1__crop0.jpg) 
 

- 14_283_3, 1)外眼角处的排线阴影；2）眼球上边缘附近的灰网点阴影；3）眼球上边缘的排线阴影；4）靠近鼻子处的排线阴影；5）面部侧面有排线阴影；  
![](img/14_283_3__.jpg) 
![](img/14_283_3__crop0.jpg) 
![](img/14_283_3__crop1.jpg) 
![](img/14_283_3__crop2.jpg) 

- 14_283_4, 面部背向光源的细腻着色。 1)鼻梁没有边线，用排线阴影和网点衬托出边线；2）颈部的排线阴影；3）嘴部的排线阴影；4）下嘴唇边线未画出，或者是用排线衬托出下嘴唇边线；5)眼部的着色，眼球上边缘附近的灰网点阴影；   
![](img/14_283_4__.jpg) 
![](img/14_283_4__crop0.jpg) 
![](img/14_283_4__crop1.jpg) 
![](img/14_283_4__crop2.jpg) 
![](img/14_283_4__crop3.jpg) 

- 14_283_5, 面部朝向光源的细腻着色。 1)鼻梁没有边线，用排线阴影和网点衬托出边线；2）颈部的排线阴影；3）嘴部的排线阴影；4)眼部的着色，眼球上边缘附近的灰网点阴影；  
![](img/14_283_5__.jpg) 
![](img/14_283_5__crop0.jpg) 
![](img/14_283_5__crop1.jpg) 
![](img/14_283_5__crop2.jpg) 
![](img/14_283_5__crop3.jpg) 

- 14_284_0, 1)紫苑腋窝处的着色；2）雅彦颈部和衣服上的排线阴影；3）雅彦衣服的渐变排线阴影；4）胳膊在衣服上的影子；5）衣服在胳膊上的排线阴影，表现出了胳膊的曲面;6)云朵逼真；  
![](img/14_284_0__.jpg) 
![](img/14_284_0__crop0.jpg) 
![](img/14_284_0__crop1.jpg) 
![](img/14_284_0__crop2.jpg) 
![](img/14_284_0__crop3.jpg) 
![](img/14_284_0__crop4.jpg) 
![](img/14_284_0__crop5.jpg) 
![](img/14_284_0__crop6.jpg) 
![](img/14_284_0__crop7.jpg) 
![](img/14_284_0__crop8.jpg) 

- 14_287_0，1）臀部裤子的褶皱；2）紫苑在地面的影子是排线阴影；3）海滩被浪冲刷的效果；4）海浪的着色，由近到远颜色变深；  
![](img/14_287_0__.jpg) 
![](img/14_287_0__crop0.jpg) 
![](img/14_287_0__crop1.jpg) 
![](img/14_287_0__crop2.jpg)  

- 14_287_3，面部有辉光效果。1）鬓角处排线阴影+灰网点阴影，有辉光效果；2）鼻子处的排线阴影；3）耳朵后面的排线方向几乎与耳廓边线平行；4）颈部的排线阴影+灰网点阴影，有辉光效果；  
![](img/14_287_3__.jpg) 
![](img/14_287_3__crop0.jpg) 
![](img/14_287_3__crop1.jpg) 
![](img/14_287_3__crop2.jpg) 
![](img/14_287_3__crop3.jpg) 

- 14_289_0, 1)衣服在胳膊上的排线阴影，排线方向体现出胳膊的曲面；2）胳膊肘的阴影；3）胳膊在衣服上的排线阴影，阴影边线粗细不一；  
![](img/14_289_0__.jpg) 
![](img/14_289_0__crop0.jpg) 
![](img/14_289_0__crop1.jpg)  

- 14_292_4, 1)远景人物的着色；2）远景树木简化为黑白高对比度，部分有黑色排线；  
![](img/14_292_4__.jpg) 
![](img/14_292_4__crop0.jpg) 
![](img/14_292_4__crop1.jpg) 
![](img/14_292_4__crop2.jpg) 

- 14_295_0，1）百叶窗的扇页边线虚实结合，透过一屡屡光线；2）宪司头发用排线；3）紫苑腿部的排线；4）宪司衣服在胳膊上的排线阴影、胳膊在衣服上的排线阴影；5）画面边缘的人物着色简化为排线；  
![](img/14_295_0__.jpg) 
![](img/14_295_0__crop0.jpg) 
![](img/14_295_0__crop1.jpg) 
![](img/14_295_0__crop2.jpg) 
![](img/14_295_0__crop3.jpg) 
![](img/14_295_0__crop4.jpg) 
![](img/14_295_0__crop5.jpg) 
![](img/14_295_0__crop6.jpg) 




--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


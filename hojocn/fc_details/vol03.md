# FC的一些细节

目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  

## Vol03

------------------------------------------------
<a name="ch015"></a>  
### Vol03	CH015	page003		女星的诞生!？  	A Star Is Born!?  {#ch015}  		

- 03_004_0, 雅彦喝的饮料: New Calorie Mate 
 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_things_in_real_world/img/CALORIE-MATE-Energy-Drink.jpg) 

CALORIE MATE能量饮料咖啡馆Au Lait。 Calorie Mate是一种营养均衡的食品，旨在为当今人们的忙碌生活提供饮食支持。 它可以在任何地方饮用，并提供人体所需的营养。 包含人体所需的蛋白质、脂肪、11种维生素和6种矿物质的良好平衡。 （饮料类型含有10种维生素和5种矿物质）

生产商：Otsuka Japan  
产地：日本  
价格：48.5美元/200ml*6罐，(约8美元/罐)  

参考资料：
[Calorie Mate Energy Drink Cafe Au Lait 200mlx6 cans-Made in Japan](https://www.takaski.com/product/calorie-mate-energy-drink-cafe-au-lait-200ml-x-6-cans-made-in-japan/)


- 03_004_1. 关于雅彦的一个格子衫，漫画里是这样的:

![](./03_004_1__.jpg)

我在想原型会不会是Burberry花纹格子衫:

![](./burberry-shirt.jpg)


- 03_004_4，03_005_0。在学校餐厅吃饭时，见雅彦喝饮料时翘起小拇指。江岛发现后，盯着雅彦看了一会儿说：“哎呀！你的容貌...挺像个女子呢。唔，刚开学时，见你总是一副阴沉惶恐的样子...当时真察觉不到，你那么像女孩。可是，现在就不同了...神态变得好温和、和蔼...”

这里间接提到搬进若苗家一段时间后，雅彦变得不那么阴沉惶恐了。


- 03_005_2，江岛和雅彦开玩笑，雅彦顺势肉麻了一把。依雅彦的性格，说这种肉麻的话很少见。


- 03_005_5，03_005_6，导演正为下一部电影演员不足的问题而烦恼，希望雅彦能来帮忙。雅彦很谦卑地说：“啊...如果不嫌弃我的话...”，导演立刻说：“好，就这样，男人不能食言的啊！！”

江岛在一旁着急：“前辈，你舍我不用，只挑柳叶吗？”


- 03_006. 导演从来都是墨镜，终于在这里露了一次眼：

![](./03_006__.jpg)


- 03_006，03_007。  
紫苑："扮女人？哦！反串女人拍电影？很有趣呢❤"  
雅彦："才不有趣啊！怎可以做这种羞家的事情！？那个前辈，是因为偷拍不到你的裸照，怀恨在心！！所以才故意要我。。。"  
紫苑："但是，我觉得那角色很适合你"  
雅彦："是吧！反正我的样子像女人。"  
紫苑："即使不像，我家可有现成的扮女人的专家呀！一定能把你装扮成明艳照人的女人。你可能不相信！！交给我们把扮的话，你一点也不会感到羞家的。"  
（可以看出，雅彦心里很不想反串，但没有立刻拒绝导演。可能是因为他的性格，也可能因为自己是新生，不敢当面抗拒学长。后续(03_025_1)，紫要打扮雅彦时，雅彦说：“不要，其实我已准备拒绝他的...哗!”）

（接着，从雅彦和紫苑在卧室里的戏转到空工作室，通过提到叶子来转接场景：）  
雅彦："。。。你怎么了？我觉得你完全不像在安慰我。。。"  
紫苑："因为这不是我的责任，你快去找个女朋友安慰你吧！"  (注：紫苑还不知道叶子已经主动做雅彦的女友了。紫苑这里打趣，是因为大家都知道上次叶子为了见到空而假意喜欢雅彦。)
雅彦："女朋友？"    
紫苑："浅冈小姐。。。今天来了"    
（然后镜头切至空的工作室）

- 03_007

![](./03_007_4__.jpg)

来看看空的工作室里有什么。顺时针，左上角开始。空的工作台的右上角，那个矮胖的可能是闹钟。闹钟旁边三个瓶子不知道是什么。右边的柜子上可能是画笔和墨水。几个瓶子不知道是什么。笔筒里不知道是哪些笔。再右边是复印机和两箱纸。再右边是一个闲置的工作台，不知道是给谁用的（后续浩美网恋时，这里放置的是电脑）。闲置的工作台的右边是一个书架，这里只显示出一个角。

来看助手们的书桌。左上角起，顺时针分别是真琴，和子，叶子，浩美:

         - 真琴正在用直尺。右手边桌角处可能是手帕（空和助手工作时常用手帕垫在手下，可能是防止手上的汗或墨渍沾到画纸上）。真琴的台灯上挂着一个东西，像是手机？左手边的一堆杂物上面是纸，纸上放的是云尺曲线板。
         - 和子胳膊上带着袖筒，可能是防止胳膊上出汗沾到画纸上（有人穿短袖，可能气温不低），手下可能是手帕。面前是纸巾。纸巾旁边的瓶瓶罐罐不知道是什么。再旁边的,竖着一叠纸，上面画着000，不知道是什么。
         - 和子对面是叶子，叶子右手边有手帕。
         - 浩美左手边可能是带纹理的画纸（可能是网点纸。比如，用来给雅彦的格子衫添加格子花纹？）右手边可能是斑点纸和刻刀？
         - 台灯上有便签纸。然后各种瓶瓶罐罐不知道是做什么用的。

结合工作室的其他的图看一下：
![](./03_008_1__.jpg)
![](./03_008_5__.jpg)
![](./01_052_0__.jpg)
![](./01_187_2__.jpg)
![](./01_188_0__.jpg)
![](./01_191_0__.jpg)
![](./01_202_1__.jpg)
![](./05_073_3__.jpg)

老北实际的工作台是这样的：

[0](./hojo_studio_0.jpg)

[1](./hojo_studio_1.jpg)

[2](./hojo_studio_2.jpg)

[3](./hojo_studio_3.jpg)

[4](./hojo_studio_4.jpg)

注意旁边那个白胖矮的钟表。


- 03_007_5， 表示“在屋内显示门外有人敲门“的方法。


- 03_008_1, 中英文不一致：  
中文版：她！浅冈叶子，从那次扰攘事件后，经常到家里来。。。空舅父工作繁重之时，她间中（注：偶尔）也会来帮忙的。

英文版：Yoko Asaoka comes from time to time to give a hand... when Sora falls behind in his work(当空进度落后时，浅冈叶子经常来帮忙)...



- 03_008, 第15话，空说叶子画的不错，所以让她作临时助手。说明叶子的还是有潜力的。作漫画家也许是适合她的.

- 03_008_5. 真琴：“嗯，直至去年夏天。因某些原因，那人正停职中。”

03_009_1, 空：“他（注：横田）应该在春天时便回来...”。（注：说明FC的剧情时间是春天）

- 03_009_1，03_009_3，03_010_0，空一连串点烟、抽烟的动作。03_010_0空的表情，我猜，是因为他完全和横田进失去联系。（03_013_2横田进说，之所以回来前没有告诉空，是为了想吓大家一跳）


- 03_009_5，03_009_6，03_009_7.叶子对雅彦说：“说起来，高中时，有一个比我们高两届的前辈，亦叫横田吧！与我属同一美术部，漫画画的不错的...”  
雅彦：“呀！是那个一点也看不出会绘画的横田前辈吗！？外形帅得很呢。像空舅父在漫画中绘画的硬汉子...他亦是空舅父的拥趸呢”  
叶子：对，对。有传他毕业之后当了某个著名漫画家的助手...  

通过提到横田进，进而引出他出场。  


- 03_011_6. 浩美：“告诉你也不要紧吧！进仔停职的原因是他要去美国...”这是门铃响起，浩美的话被打断，给读者制造一个悬疑。


- 03_012_2，雅彦去开门，横田进看到雅彦后，认出了他。因为横田进曾经解救过被流氓敲诈的雅彦（03_019_5），所以认识。


- 03_013_2，横田进说，之所以从美国回来前没有告诉空，是为了想吓大家一跳。他直接从成田（注：应该是指成田国际机场）回来。


- 03_14, 这个路人的背影，衣服和头发都很像薰。可是，薰在第7卷才出现。这是第3卷里的图。（这类似叶子在第10话出场，而在第9话02_035_0露了背影）：        
![](./03_014_4__.jpg)  


- 03_14, 店牌上写“COFFEE HOUSE Hitomi since 1985”, 《猫眼》里瞳的英译为Hitomi，同时1985年发表《猫眼》大结局，同年《猫眼》动画版完结   
![](./03_014_4__.jpg)


- 03_014_5，横田进、叶子、雅彦三人在咖啡厅小聚，很合理，因为是高中校友，且横田之前解救过被流氓敲诈的雅彦（03_019_5）。

叶子和雅彦回来的路上，经过的林荫大道（03_022_2）




- 03_016_0, 叶子、雅彦和横田在餐厅里聊天。雅彦和叶子（坐在桌子的下方位）面对横田进很紧张。背景里有其他三个餐桌，其中两个能显示出桌子下方位的客人（小孩）。有趣的是这些小孩子也都低着头、看起来像是犯了错、很紧张。

- 03_016_4, 雅彦参加过一次空的派对，叶子没有参加过。



03_017_2, 空的派对每周都举行。

03_018_1, 横田进回忆他参加空的派对，有其他三位助手。所以，我猜，很可能其他三位助手在横田进之前就来到空的工作室了。同时，03_021_2，横田进说“留下来的只有阿真、阿浩、和子三人”也支持这个猜测。

- 03_021_2，横田进：“老师那儿，一直都有不少人慕名而来想当助手的...但大多数人都熬不过一天”


- 03_016_6，雅彦的回忆天体之夜的画面，画面有变形（变窄）。


- 03_022_4, 雅彦已经搬到若苗家2个月了。

- 03_023_0，03_023_2，听了横田进的话后，雅彦很气馁。从咖啡店出来后，叶子看到雅彦无精打彩，便说：“你无须太介意前辈说的话啊！你住在那儿已两个月了，但完全没有迹象嘛！！而且--有这么一个好的女朋友在！！”，“对了，有我的存在，仍会变成跨性别者的话，我可要丧失自信心呢！”

注：03_085_1，叶子两次看到雅彦让她心痛的行为（异装、拍吻戏）后，思索和雅彦的关系：“为什么会这么生气？是普通朋友吧，但却又...” 。疑问：为什么两人在恋爱但仍是普通朋友？（因为没有表白么？）


- 03_024_4，听紫苑说雅彦要反串，紫很兴奋地要打扮雅彦：我已经磨拳擦掌呢❤。遭到雅彦拒绝后，又说（03_025_1）："别急！试试效果再说不迟！！"。雅彦继续拒绝，紫又说（03_025_2）："你这人真顽固，只不过扮扮女人而已"。打扮完后，紫苑和雅彦在照镜子，旁边的紫闭着眼睛--对自己的成果很有信心(03_027_0)  

这些对紫的描写很生动。


- 03_026, 雅美的特写占了整幅页面。

----------------------------------------------------------
<a name="ch016"></a>  
### Vol03	CH016	page031		扮女人的测试  	Transvestite Apprentice  {#ch016}  		

- 03_033_0, 给雅彦打扮的这个女孩应该是影研社的成员。以前也多次出现过：
02_134_3，02_148_0，02_152_0，02_155_2


- 03_035_1, 雅彦拒绝反串，提到自己18岁。

- 03_035_5，导演要雅彦反串一周、体验生活，自比《窈窕淑女》（My Fair Lady）里的希金斯教授(Prof. Higgins)（03_036_1）。雅彦拒绝，导演以录影带威胁之。


- 03_037_3, 雅彦：“做出这种事...万一我的(反串)'才能'被赏识便惨了”。后续（03_042_3）又说：“我的女性造型，真的那么厉害...？应该高兴？还是悲伤呢？”


- 03_037_6，路人的衬衫上印有“Daicon”字样。

Daicon Film是GAINAX的前身，曾制作1981年第20届日本科幻大会（Daicon III）、1983年第22届日本科幻大会（Daicon IV）的开幕短片。

参考链接：
[GAINAX](https://baike.baidu.com/item/GAINAX/1800415)


- 03_041_1，叶子来到若苗家，看到紫苑和雅美出门，便问侯午安。说明此时是中午左右。
同时，叶子没有认出雅彦，以为她是紫苑的同学，便说：“很漂亮的女孩呢！近来高中生的质素可真高呢！！”


- 03_043，紫苑和雅彦都喜欢称呼叶子为“浅冈”(叶子的姓氏)，而不是叶子的名。这说明什么？难道是彼此间不够亲密吗？

![](./03_043__.jpg)


- 从出版和重新印刷日期来看，FC约三个月重印一次。

![](./fc.press.date.jpg)


- 03_043, 03_064, ”人多热闹“这个台词经常出现

![](./03_043_064__the_more_the_merrier.jpg)


- 03_044_4，03_044_5，03_044_6. 说悄悄话，用的是"想"对话框.

- 03_045_2，雅美、紫苑、叶子三人逛街。街上有商标：Koda、AOKI。。。



03_045_4，紫苑和叶子看泳装，雅美在旁边很害羞。

03_046_1, 在街边拍照。有字样：RASH，XXX俱乐部。不知道这个Rash和北条司的作品《Rash》有没有联系。感觉应该有些关系，因为logo和《Rash》封面的字样一模一样。

03_047_3，叶子疯抢春季促销的衣服。

03_048_5，叶子：“女人一疯狂购物，就会忘记时间的了。”


- 03_046. 老北客串路人看美女。

![](./03_046_4__.jpg)



- 03_050_1, 叶子独自一人生活，是在说服了父母之后。英文版里用的也是“parents”，说明这时她父亲还健在。

![](./03_028__.jpg)


- 03_050，叶子住处有点乱（但后续多为整洁）。屋里有一些绘画相关的用品。（FC里紫苑的住处就很整洁，只有一次非常乱，那是在拍影研社的电影期间）

![](./03_050_053_.jpg)


- 03_053_1, 叶子住处的楼不是直角？

- 03_053_4, 雅彦对叶子住处的印象：“看她的家局情况，女人亦相当不整洁，跟男人也差不多...原来是同一类型的人，我可放心了。”

- 03_053, 03_054, 03_057, 这几张图，怎么看都觉得叶子的身高比雅美(雅彦)高一点点。两人都是平底鞋。  
![](./03_053_054_057__.jpg)  
后续[11_085_0](./img/11_085_0__.jpg)进一步证实了这一点。  

----------------------------------------------------------
<a name="ch017"></a>  
### Vol03	CH017	page059		追击  	Repetition  {#ch017}  		

- 03_062_3, 紫苑端下午茶咖啡到空工作室，空说：“真是少有啊，紫苑端咖啡来呢。通常浅冈在的话总是雅彦端来的”。这么推测的话，下午茶时间雅彦已经放学到家了？叶子也是放学之后来空的工作室？

- 03_063_3，03_063_6，叶子：“空老师，我有话跟你说！！”，下一个叶子的镜头，空：“辞...辞职?”，这里省略了叶子提出辞职的那句话。

- 03_064_0，叶子在美大上学。



- 两条叙事线并进：1.雅彦在影研社拍戏、回到家。2.紫苑来到叶子住处，解释雅彦的异装是为了拍戏；并带叶子回到若苗家。

- 03_065_3, 学校教学楼窗玻璃上有天空云的影子。


- 03_068_2, 03_068_3. 叶子生气地从空工作室辞职，出来遇见因拍戏而异装的雅彦。雅彦喊住叶子(03_068_1)，想解释; 结果叶子讨厌地说“变态！”(03_068_4)。03_068_2, 03_068_3这两个镜头人物几乎静止、且无对白；我猜，这是为了用对比的方式来突出03_068_4里人物的情绪，有点类似"暴风雨前的宁静"。

- 03_072_6，放学后，雅彦和江岛在雅彦的卧室。这时太阳和阴影的方向。是否有助于推测若苗家的朝向？

- 03_072, 03_077. 叶子的住处。从走廊处的竖直的管道推测，很可能一层有6户。

![](./03_072__yoko_house.jpg)

从后续（11_024）看，一层有8户：
![](./11_024_0__.jpg)

叶子的屋子可能是最靠边的那间。



- 03_073, 江岛面前的盘子是烟灰缸？还是盛水果(葡萄？)的盘子？雅彦床下面的盒子上印的好像是“IQXY”，不知道是什么。

![](./03_073_0__.jpg)


- 03_073, 江岛说“我的志愿时当演员呀！！”。他为了演戏出名，可以说是很拼了。我不懂日文，所以只对比中英译文，英文翻译的比较欢乐：“就算是亲牛屁眼，我也愿意”。

![](./03_073_4__kiss_a_cow.jpg)


- 03_075，03_076。在叶子的住处，人们是不穿拖鞋的：

![](./03_075_076__yoko_house.jpg)

02_136，02_156，江岛的住处，人们也是不穿拖鞋的：

![](./02_136_156__ejima_house.jpg)

01_004，01_012，柳叶家也是：

![](./01_004_012__yanajiba_house.jpg)

但在若苗家里人们总是穿着拖鞋。


- 叶子发现雅彦扮女装的事情后非常生气。紫苑打算告诉叶子真相（雅彦是为了影研社的演戏）(03_063)，但叶子没有给紫苑机会就打算从空的工作室辞职。紫苑劝雅彦去挽回叶子。雅彦说(03_069)：“没关系，我现在灾难重重，一浪接着一浪，脑袋一片空白，什么都想不出来...”。紫苑无奈，同时也觉得自己对这个关系破裂也有责任(03_069)，于是她去叶子家里道歉并说明真相(03_072)。然后叶子随紫苑回到若苗家见雅彦。
在回若苗家的路上：

![](./03_078__.jpg)


- 03_074, 雅彦的衣服上应该是“CITY HUNTER”的字样

![](./03_074_0__.jpg)

03_073_2, 也有疑似“CITY HUNTER”的字样。

03_069， 雅彦的衣服上的字是不是“F.compo”的字样？

![](./03_069_1__.jpg)


- 03_075_4. 紫苑向叶子道歉：“令到你误会了，我也有少许...不，一半的责任...”, 从“少许”改为“一半”说明紫苑道歉有诚意。

- 03_076_4. 见叶子有些犹豫，紫苑说：“本来平日思想进步，很快就能恢复心情的人...为什么对雅彦这件事总要这么执着？”。这里说到叶子的特点：思想进步，很快就能恢复心情.

之前也有提到叶子的这个特点。叶子第一次来若苗家离开时，雅彦(02_112_2)："浅冈...依...依旧没变呢...这么快就能恢复心情。。。"  


- 03_076, 03_077。紫苑劝说叶子的过程中，总觉得两处蓝色框里的表情思绪万千。是不是紫苑有点吃醋、她这时已经有点喜欢雅彦了？

![](./03_076_077__.jpg)


- 03_078_0, 电车站有“子母神前”的字样，应该是"鬼子母神前"站。应该是若苗家附近的车站。





- 03_081, 这个门把手向下倾斜，说明门外有人拧门把手（这是叶子在看到雅彦和江岛kiss后，默默关上房门退出的镜头）。

![](./03_081_6__.jpg)


- 03_082, 雅彦发现叶子看到自己和江岛kiss后昏厥。用没有画眼仁来表现这种状态。我不太理解的是，为什么嘴角要画一滴口水？

![](./03_082_2__.jpg)



- 03_084, 叶子出门时比较仓促，所以桌子上的水杯没有收拾。回来后，水杯还是原样(对比，03_075, 03_076, 03_077. 03_077应该是少画了叶子的水杯)。

![](./03_084_2__cup.jpg)


- 03_085. 雅彦和江岛实拍时，嘴上没有贴胶带。好惨。（即使贴了也很惨～）

    [03_085](./03_085_2__.jpg)
    
    04_133， 雅彦说这个戏是嘴上贴了胶带的。
    [04_133](./04_133_1__.jpg)


- 正如俊夫作为猫眼的男主，不仅收获了小瞳的香吻，竟然还有来生泪的香吻；雅彦作为FC的男主，不仅收获了紫苑的、叶子的、甚至是浅葱的香吻：

        - 紫苑之吻：

![](./romance/05_138_0__.jpg)
![](./romance/05_138_1__.jpg)
![](./romance/05_138_2__.jpg)

          闯迷宫后：

![](./romance/14_161_3__.jpg)

        - 叶子之吻：

![](./02_112_5__yoko_kiss.jpg)
![](./05_157_1__.jpg)
![](./img/06_017_1__.jpg)

        - 浅葱之吻：
        
![](./img/14_235_0__.jpg)


而且，作者还赐给了他：

        - 江岛之吻（[03_081](./03_081_2__ejima_kiss.jpg)）、

        - 熏之吻（[07_138](./07_138_3__.jpg)，[07_173](./img/07_173_5__.jpg) ，07_175_1）、

        - 文哉之吻（[12_040](./12_040_1__.jpg)）

真是忍不住要纪念一下这些浪(心)漫(痛)的时刻啊，哈哈。

（紫苑："雅彦，老北果然没有让身为男主的你失望啊！哈哈..."，雅彦："(苦笑)......你负责貌美如花，我负责插科打诨啊～"）
    

- 03_085_0, 03_085_1, （前后两周，叶子两次看到雅彦让她心痛的行为（异装、拍吻戏）。两人的关系降至冰点）叶子在思索和雅彦的关系：“我究竟做什么了...为什么会这么生气？是普通朋友吧，但却又...” 。所以叶子不认为两人是男女朋友关系。

后续（04_132_4）,叶子看过雅彦的戏后找到雅彦。雅彦觉得叶子想要分手，但心里又说：“哦，不！我们从来没有拍拖过呀！！”。所以雅彦也认为两人不是男女朋友关系。


[疑问]：为什么叶子这里说她和雅彦是"普通朋友"？因为在我看来，之前有两次叶子表达了两人之间非普通朋友：     

1. 叶子第一次来若苗家（为了见空而谎称喜欢雅彦），离开时叶子对雅彦说(02_113_0)："因为我想再见到你！！再见。"  

2. 03_023_0，03_023_2，听了横田进说"能待在若苗家的人都会改变"的话后，雅彦很气馁。从咖啡店出来后，叶子安慰雅彦不比太介意：“而且--有这么一个好的女朋友在！！”


- 每一话结束时，最后一页一般会放一张本话里的某个图。作者选这张图的依据是什么呢？[疑问]  


----------------------------------------------------------
<a name="ch018"></a>  
### Vol03	CH018	page087		意想不到的助手  	An Unexpected Collaborator  {#ch018}  		

- 03_090. 这是早晨紫苑送雅彦去车站，如果这条路和若苗家门前的路同一方向的话，那么从影子的方向推测，若苗家大门应是朝南。

![](./03_090_5__.jpg)

（这里自行车的影子是不是不该是实心的？）


- 03_091. 影研社一行人去新宿华舞伎町拍戏。右侧的是mini？是獠的坐骑？

![](./03_091_3__.jpg)


- 03_097. 紫苑想去看雅彦拍戏，路遇横田进。当得知拍戏地点是新宿歌舞伎町后，横田进担心会出事，于是两人同去。在地铁上，右边的这个路人画得很详细，感觉可能是北条司作品里的一个人物，但不知道是谁。

![](./03_097_3__.jpg)

- 03_096_2, 
导演闹肚子，雅美：那么不舒服，明天再拍不更好吗？
摄影师：他决定了的事，没人能阻止他的！！

03_109_5，
导演：“不，这是十分必要的一场戏，即使赔上了性命也要拍的！！”。
江岛&雅美：“他决定了的事，真的怎么也不能改变。。。”

说明导演对拍戏很执着。

----------------------------------------------------------
<a name="ch019"></a>  
### Vol03	CH019	page113		辰已，纯情的爱…!?  	Tatsumi's Unwavering Love  {#ch019}  		  

- 03_114, 雅美的信息：
三围：89:65:87, 身高：168.5cm，体重：48.2kg（即雅彦的身高/体重）


- 03_115. 左边两人也是影研社的人，表情很有趣。

![](./03_115_4__.jpg)


- 03_129_5, 辰巳在学校门口等雅美。雅彦从校门口逃脱。天下大雨。雅彦在浴室泡澡，想：“正常的人...都该走了。。。虽然我曾试过等了四小时...但这么大的雨...”

"曾试过等了四小时"应该指的是（第1卷6话，雅彦代替紫苑去赴约齐藤玲子）01_176_5，雅彦：“要我一直等下去吗？已经等了4个小时了啊！！”

----------------------------------------------------------
<a name="ch020"></a>  
### Vol03	CH020	page141		阔别了18年的妹妹  	A Little Sister Not Seen For Over 18 Years  {#ch020}  

- 03_142_2，雅彦独自在家（空和紫外出购物（03_149_3）），接到顺子的电话。空本名"菊地春香"，家住高知，36岁（03_143_7）。

- 03_142_3, 顺子在日向前站（日文是"目白站"）的百佳咖啡店。雅彦去那里接她。


- 03_145。“破格”指人物冲破边框。这在FC很常见。这个有趣的是雅彦只有眉毛破格了。

![](./03_145_2__.jpg)


- 03_154_5, (空的妹妹菊第顺子来访, 告诉空自己要结婚(03_153_0))7月20日是顺子过文定的日子(03_154_5), 顺子希望空也参加(03_154_6)。雅彦那时放暑假(03_157_3), 空的家乡有“四万十川”。

- 03_156_5，顺子：“做父母的当然挂念自己的女儿。。。所以他（父亲）才派我来作代表”。事实上，空的父亲并不同意：03_173_2, 只是顺子想让空参加她的过文定仪式，空的父亲并不同意。



- 03_160_1, （紫劝空回去参加顺子过文定，顺便看看父母）紫说：“老公你太不懂珍惜了。父母尚在，能相见而不去见。。。”。推测，紫的父母可能已经去世了。

----------------------------------------------------------
<a name="ch021"></a>  
### Vol03	CH021	page169		父与"女"!  	The Father And The "daughter"!  {#ch021}  		

- 03_178_2，紫和空的母亲初次见面

- 03_183_4，浴室的墙上挂着牌子，上面有文字“田中举(?)建”

- 03_184, 03_185, 03_186, 紫和空的母亲谈话，很感人。


- 03_187. 空的父亲在空的高中学校任职(当年任训导主任（03_188_1），后来任校长(03_154_2_，前年(1995)已退休(03_156_2))。
![](./03_187_3__.jpg)


- 03_189，宪司：“本来春香一直想往市外不用穿校服的学校念书。。。可是遭到父亲的反对，被逼在父亲现任职的高中就读。他父亲的想法是希望最起码让她穿着女生校服。。。做回女孩模样”

- 03_194_1，宪司联系到的村子里空的朋友：阿浩，内藤，祐二。其中那个女性（婚后）改姓室井。


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


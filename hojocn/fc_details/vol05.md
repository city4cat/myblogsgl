# FC的一些细节

目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  

## Vol05

-------------------------------------------
<a name="ch029"></a>  
### Vol05	CH029	page003		扮女人比赛  	Crossdressing Competition  {#ch029}  		  

- 05_004_5. 江岛说紫苑的高中是名门私立女子高中

- 05_025. 江岛全名卓也江岛

![](./05_025_0__.jpg)


- 05_028. 紫说出与紫苑同校。

![](./05_028_5__.jpg)


----------------------------------------------------------
<a name="ch030"></a>  
### Vol05	CH030	page031		亡命驾驶  	Maniac At The Wheel  {#ch030}  		  

- 05_034, 05_042. 紫说已经15年没开车了： 

![](./05_034_6__.jpg)

紫的驾照：

![](./05_042_1__.jpg)

姓名：若苗 龙彦  
生日：昭和36年(1961年)2月x日  
本籍：东京都 三鹰市 深大寺 6-18-8  
住所：东京都 丰岛区 杂司谷 4-24-15  
交付：平成07年(1995年)02月11日 114622  
有效期：平成12年持证人的生日前  

英文：
Name：Wakanae Tatsuhiko
生日：D.O.B 24年2月x日
Born：Tokyo-Mitaka-Jindaiji-6-18-8 (注：英文版将"三鹰市"翻译为Marita, 应为错误)  
Address：Tokyo-Toshima(或Toyoshima)-Zoshigaya-4-24-15 (注：英文版将"杂司谷"翻译为Zoshijiya, 应为错误)  
Issued：11/02/1996 114622
Date of Expiry：11/02/2001

那么驾照距今15+年;同时，驾照上的住址是现在的若苗家的住址（参见下面的06_112），所以，若苗一家人在这里住了15+年。

(06_112_0)信封上部有邮编:171-0032. 这应该是若苗家的邮编吧？


- 05_036_5，紫开车出门是左转  



- 05_040_6, 05_041_1，紫在千叶的（西）船桥警察署  


- 05_055，空说“我回来了”，而且穿西服。他从哪里回来？

- 05_056，紫开180迈，但她感觉只有60迈。开进了中央高速路，经过了山梨县的大月。

- 05_057_2，雅彦的衬衫有"IMPAL*"字样


----------------------------------------------------------
<a name="ch031"></a>  
### Vol05	CH031	page059		你喜欢妇科吗？  	Gynaecology? You're Kidding Me!  {#ch031}  		  

- 05_060. 空的助手们早晨被闹钟吵醒。但为什么闹钟是1:00呢？[疑问]  

![](./05_060_1__.jpg)


- 05_060. 助手们的房间。

![](./05_060_4__.jpg)

- 05_061. 浩美和真琴有胡子茬。06_009也有。

- 05_070_6, 空：“...我...一直以来..从来不脱稿这一点，是我自豪的事情”。我猜，这也是作者北条司的自豪吧。05_081_6，紫：“（画漫画）这是一般男人也熬不住的工作呀...”，说明漫画家的辛苦。

- 05_076_2，空倒下昏迷后被送往“山岡产妇人科医院”，后转到“X崎综合医院”治疗阑尾炎（05_078_5，05_080_0）。

----------------------------------------------------------
<a name="ch032"></a>  
### Vol05	CH032	page086		各人的平安夜  	A Xmas Like The Others  {#ch032}  		  

- 05_087 ~ 05_090. 雅彦放学回家路上经历的地点。

![](./05_087_0__.jpg)
![](./05_088_0__.jpg)
![](./05_088_2__.jpg)
![](./05_088_4__.jpg)
![](./05_090_3__.jpg)
![](./05_090_5__.jpg)
![](./05_090_6__.jpg)

- 05_087_1, 叶子约雅彦过平安夜。



- 05_088. 圣诞夜前一天，紫苑被一男生表白。这个男生在上学途中经常看到紫苑。

![](./05_088_4__.jpg)

- 05_091_3. 空不在，紫、雅彦、紫苑三人在和室吃晚饭。往年的平安夜都是空和助手们开派对。

《北条司拾遗集.画业35周年纪念》里(146页)有访谈：
----家庭剧的话，您对日常事物的考究如何呢？我觉得雅彦经常轮换着穿DKNY呀Calvin Klein的T恤，这一点很棒（笑）
北条： 我有让他轮换着穿西装。也有衣柜（笑）。角色很多，虽然辛苦，但我很注重他们各自的喜好、季节感。但是，虽说是便装，比如运动裤啦，不太美的东西我就不想画（笑）。还有，若苗家的餐桌。夏天在餐厅吃饭，但冬天就会在隔壁的和室围着被炉吃。


- 05_096_3，紫要去医院和空共度圣诞夜，带的又红饭和甜品、香槟、装饰用的花环、圣诞树和七彩爆竹，还有...  

- 05_097_2,  圣诞夜，雅彦和叶子约会在猫眼咖啡厅。这是网友MaggieDeric发现的[这个帖子的17楼](https://tieba.baidu.com/p/6869409371)  
![](./05_097_2__mod.jpg)  


- 05_102. 圣诞夜，紫苑和助手们庆祝，喝多了。英文版的翻译表现了说话不清楚的感觉。

![](./05_102-en.jpg)

- 05_104, 05_105. 圣诞夜助手们狂欢后从若苗家出来去车站。能不能看出来他们出大门后是向左转还是向右转？

![](./05_104_2__.jpg)
![](./05_105_0__.jpg)

----------------------------------------------------------
<a name="ch033"></a>  
### Vol05	CH033	page113		紫苑的初恋  	Shion's First Love  {#ch033}  		  

- 05_122_6, 圣诞夜，叶子走后，雅彦和紫苑出来散步。雅彦递给紫苑热饮。  
紫苑：Thanks！虽然没有白色圣诞，但星空下的圣诞也不错吧～～～

注：下一个圣诞夜(11_087_0)就是白色圣诞

- 05_124  
紫苑：我的初恋发生在中一那年...相当迟吧...  
雅彦：呀，嗯。。那么，对方是男的？  
紫苑：这个当然嘛！！  


- 05_126。光浩（浩美）刚来时的助手们:已经有和子。真琴的位置是一个不认识的人，横田进的位置看不清是谁。

![](./05_126_0__.jpg)

- 05_127_6, 这个公园是光浩去若苗家的必经之路。

- 05_127, 05_130. 紫苑回忆初恋。镜头这样进入回忆：

![](./05_127.jpg)

再从回忆里拉回来：

![](./05_130.jpg)

- 05_134,
紫苑：告诉你，我对于跨性别者呢...！！（注：这里紫苑想说什么？）  
紫苑：你虽然曾被强迫扮女人，又被男人苦缠过...有过这种的危机...  
雅彦：因...因为...  
紫苑：唔...你没有变女人，也没有逃掉。一直都是喜欢女人...能好好地做个男人...这真的令我很惊讶呀！！如果...你一直都是喜欢女人。继续做个堂堂男子的话...可能你会令我改变对男人的看法...可能..你就是把我改变过来的人。把我...改变好吗...？


- 05_138，圣诞夜雅彦和紫苑在若苗家门口的路灯处接吻了。
05_139，紫苑好像瞌睡了，雅彦喊：“喂...喂！别在这儿...醒来呀！你又想装睡了吧！？”。然后叶子出现（手扶着墙壁）（从05_143_0来看，叶子是从若苗家大门口出来），我猜，叶子回家路上半路返回若苗家，敲一楼的门没人开。然后听到大门口雅彦在喊叫，所以叶子赶到大门口处（然后便看到雅彦抱着瞌睡的紫苑）

后续第39话(06_100_5，06_100_6)，叶子：“我...我曾经妒忌紫苑...从平安夜那一晚开始...那一晚，我感到你和她之间，像是发生了什么事情似的..所以我怀疑你...喜欢了紫苑...”（叶子的这些话说明她没有看到雅彦和紫苑的亲吻(05_139, 05_142)）


----------------------------------------------------------
<a name="ch034"></a>  
### Vol05	CH034	page141		叶子的求爱!！  	Yoko Makes Her Move!!  {#ch034}  		  

- 05_155，叶子想和雅彦亲吻，雅彦眼前出现紫苑的幻想，便惊讶着推开叶子。叶子伤心：“你真的不喜欢...”，雅彦："不是！！因为是初次接吻！！",然后雅彦想到刚才和紫苑的吻，意识到自己说错了。这说明，雅彦和紫苑刚才在路灯下的吻是雅彦的初吻。


- 05_165，05_166，对于圣诞夜的事，雅彦觉得：我陷于罪恶感和宿醉的双重折磨下...虽然说是喝醉酒，一晚之中与两个女子（？）...对于自己的荒唐...我感到我越来越讨厌自己了。但是，却又反而有一种很奇妙的感觉...像优越感般......留在心里某个地方...唉，我真是...  


- 05_166, 圣诞夜，紫苑说起初恋，后来和雅彦亲密。第二天，紫苑说昨晚的事“我完全即不起来呢”。实际上，紫苑是记得的，理由：第一，从接下来她紧张和有些尴尬的表情可以推测出来。

![](./img/05_166_4_5__cn.jpg)

第二，紫苑后来说(14_047)浅葱是自己的初恋，雅彦反问：“你不时说过...你的初中初恋是中一那年吗？”, 紫苑：“那是我女性时的初恋”（14_048）：

![](./img/14_048_2__cn.jpg)

参考链接：
[紫苑酒后"失忆"是一个谎言-非常家庭吧](https://tieba.baidu.com/p/6104097285)

----------------------------------------------------------
<a name="ch035"></a>  
### Vol05	CH035	page169		拜祭母亲  	At Mother's Graveside  {#ch035}  	

<a name="推测雅彦妈妈的忌日"></a>  
#### 推测雅彦妈妈的忌日：  {#推测雅彦妈妈的忌日}  

05_174_3. 圣诞节过后的第35话，可能是1998年1月。因为34话是圣诞节、36话是寒假开始（1月6日左右），所以本话35话提到的11日应该是1月11日。

11日是妈妈的忌日（05_173_6），是假期（05_174_3）。现实中1998年1月11日是周日。符合！


- 05_172_6, 雅彦内心独白：  

雅彦：每年，母亲的死忌将至之前，我差不多都会做同样的一个梦(05_170_2)...梦境内...是妈妈去世后的第二年...我和爸爸去拜祭她的情形(05_173_5)----

雅彦：令我印象深刻的是....爸爸那悲伤的背影和那漂亮的女孩成了强烈的对比----(05_172_6)  （这句内心独白，我觉得非常有文艺气息，给我的感觉很好！）

雅彦：我猜那可能就是我的初恋。


- 05_177_3，年轻时妈妈和若苗龙彦(紫)的合影（紫被裁去）。





- 05_183_5, 紫去探望病重的雅彦妈妈。雅彦妈妈说：小雅彦和紫小时候很像。

- 05_195_5，雅彦母亲所在的墓地的一块墓碑背面："九年六月二十八日X  方年五十二岁"。这几个字很清晰，会不会有特别的含义？[疑问]  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


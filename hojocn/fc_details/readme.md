

# FC的一些细节  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96736))  

- 说明：  
    - "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  
    - 类似03_143这样的字符串表示"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似03_143_7这样的字符串表示"卷-页-分格(panel)/分镜"编号。xx_yyy_z表示第xx卷、第yyy页、第z分格(panel)/分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  

----------------------------------------------------------

## 目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  

----------------------------------------------------------

## 背景资料  

### 日本小学、中学、大学的学期  

日本小学/初中?学期:  
第一学期：4月～7月  
暑假  
第二学期：8月～12月  
寒假  
第三学期：次年1月～3月中旬  
春假（3月下旬～4月初）  


日本高中三学年：  
上半学期：4月 ~ 7月21日前后  
暑假  
下半学期：8月24日前后 ~ 次年3月  

日本高中二学年：  
第一学期：4月～9月  
第二学期：10月～次年3月  


日本的大学的学期：  
上半学期： 4月 - 7月末/8月初  
暑假  
下半学期1：  10月 - 圣诞节  
寒假  
下半学期2：次年1月6日(约) - 次年1月底  
春假：次年2月 - 次年3月底  

参考资料： [日本高中学期制度](https://zhuanlan.zhihu.com/p/37253508)  

----------------------------------------------------------
## 一些链接  

[推测雅彦的生日](./vol04.md#推测雅彦的生日)  
[推测雅彦妈妈的忌日](./vol05.md#推测雅彦妈妈的忌日)  
[推测和子的年龄](./vol08.md#推测和子的年龄)  
[紫苑的手机](./vol11.md#紫苑的手机)  
[紫苑的手机2](./vol14.md#紫苑的手机)  
[雅彦的手机](./vol11.md#雅彦的手机)  
[浅葱的手机](./vol14.md#浅葱的手机)  
[关于叶子和雅彦分手的一个猜测](./vol14.md#关于叶子和雅彦分手的一个猜测)  
[FC时间线](../fc_information/fc_timeline.md)  

----------------------------------------------------------

## 一些可能的错误

以下这些可能是错误，也可能不是错误。之所以指出这些错误，不是想说明作品不好；反倒是想说明：  
1)看漫画时往往觉察不到这些错误。所以这些错误并不影响读者的阅读。  
2)漫画的质量不会因为这些错误而受到影响。所以，漫画创作者不必顾虑某些错误而限制了创作。  

- 第6话,01_159_7紫苑的衣服是这样的，胳膊前臂处是净色纹理。  
(我很奇怪这衣服的设计，或许是内衣是净色长袖，外衣是短袖？所以前臂的净色实际上是内衣？)  
![](./01_159_7__.jpg)  
01_162_0变成了这样，应该是个bug吧:  
![](./01_162_0__.jpg)  

- 02_017。餐桌两侧的椅子靠背是不一样的。空和紫的椅子靠背是直的，紫苑(和雅彦)的椅子靠背是折的。  
![](./02_017_1__.jpg)  
以为是画错了，看到02_019时发现是有意为之。厉害了！  
![](./02_019_4__.jpg)  

- 02_066, 叶子第一次出现，扑向雅彦的右肩膀，02_067变成了左肩膀。  
![](./02_066_6__.jpg) 
![](./02_067_3__.jpg)  

- 03_118, 3_122. 影片里辰巳是正面：  
![](./03_122_1__.jpg)  
但拍摄时，从相机和辰巳以及其他演员的位置来看，拍的不是辰巳的正面(很可能是背面或者左侧面)：  
![](./03_118_4__.jpg)  

- 03_180, 可以透过右边的桌腿看到雅彦的腿部，桌腿变得透明了。  
![](./03_180_5__.jpg)  

- 06_028, 06_029. 辰巳给雅彦介绍兼职。从咖啡厅里出来之后江岛的衣服变了  
![](img/06_028_0__.jpg) 
![](img/06_029_0__.jpg)  

- 06_055， 06_061。雅彦衣服上的"FC"前后不一致。  
![](./img/06_055_2__.jpg) 
![](./img/06_061_0__.jpg) 

- 06_155，06_156。茜把亚美带到酒店，06_155_4、06_155_7两个镜头是右手拿啤酒，06_156_0换成左手拿啤酒。(当然，这不是没有可能。但是，06_155_7,06_156_0这两个紧邻的镜头这么切换，让我觉得有点突然。)  
![](./img/06_155_4__.jpg) 
![](./img/06_155_7__.jpg) 
![](./img/06_156_0__.jpg)  

- 06_170，06_171。门口墙上的灯，大小不一致（和砖墙的位置作比对）。  
![](./img/06_170_6__.jpg) 
![](./img/06_171_2__.jpg) 
![](./img/06_171_5__.jpg)  

- 08_017。紫少画了泪痣。  
![](./img/08_017_4__.jpg) 
![](./img/08_017_5__.jpg)  

- 08_144_4, 上课时，叶子的同学在讨论男女亲密之事，叶子听到了觉得尴尬。08_144_4分镜的最左边的女同学的背带是不交叉的。后续08_146_0，08_146_2里应该也是这个女同学，但她的背带是交叉的。  

- 10_071。 1th是不是应该是1st？  
![](./img/10_071_5__.jpg)  

- 10_145。若苗家门前的电线杆应该离门很近，但这里画的距离很远：  
![](./img/10_145_0__.jpg)  

- 10_160_2，雅彦的黑色体恤上应该有白色字样，像10_158_2那样。  

- 10_184_3，10_186_2，紫的连体毛衣背后从背部到臀部应该有竖向的花纹，像10_179_2那样。  

- 10_193。紫苑左胳膊距离左肩膀太远：  
![](./img/10_193_3__.jpg)  

- 10_196，右肩膀的位置有些别扭：  
![](./img/10_196_5__.jpg)  

- 11_078，地面的雨滴圈圈大小太一致了  
![](./img/11_078_0__.jpg)  

- 12_112，二楼走廊里忘记画了一扇挡板  
![](./img/12_112_0__.jpg)  

- 12_138，仔细看，我觉得江岛和背后的书架之间好像有一定的距离----没有直接躺靠在书架上。是我自己的错觉？  
![](./img/12_138_1__.jpg)  

- 13_008，紫苑身体的方向不是竖立的，和其他人的不一样（当然也可能是她此时惊讶、身体前倾）：  
![](./img/13_008_0__.jpg)  

- 13_059，紫苑的脚画错了：  
![](./img/13_059_0__.jpg)  

- 13_174，"地板的透视"和"左侧一排门的透视"好像不一致：  
![](./img/13_174_6__.jpg)  

- 14_004_4，雅彦的体恤上没有文字。14_005_2, 雅彦的的体恤上有文字。  

- 14_014, 导演和摄像师被冷落。导演酒喝完了，要酒，服务生懒得搭理他，直接扔酒瓶过来让他自己倒酒喝。我觉得，平酒瓶砸在头上，头肯定会震动一下。但这里没有震动的效果：  
![](./img/14_014_0_2__cn.jpg)  

- 14_047， 14_048，紫苑窗前的台灯的位置不一致：  
![](./img/14_047_4__.jpg) 
![](./img/14_048_0__.jpg)  

- 14_048，14_049，第一个镜头紫苑的衣服上没有CH，后两个镜头里有：  
![](./img/14_048_6__.jpg) 
![](../fc_dress/img/14_049_1__.jpg) 
![](./img/14_049_4__.jpg)  

- 14_058，紫的位置像是站在了洗碗池里：  
![](./img/14_058_2__.jpg)  

- 14_251，紫苑左手托腮的姿势有点怪：  
![](../fc_drawing_style__gesture/img/14_251_0__.jpg)  

- 14_269, 雅彦的衬衫忘记加格子花纹了。本话（102话）雅彦穿的是格子衫：  
![](./img/14_269_3__.jpg)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处




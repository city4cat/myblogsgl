# FC的一些细节

目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  

## Vol01

- 01_00a_0, FC第一卷封面，雅彦的衣服上可能是"Family"字样：  
![](./img/01_00a_0__mod.jpg)  



- 在[20周年访谈](../zz_anniversary_20th/anni_20th_interview.md)里，北条司说:“最让我觉得不可思议的是雅彦。为什么他会那么内向呢？再让他象样一点不也挺好吗？其实在最初的预告中，他的形象明明比现在要不良得多。可现在那种感觉已经半点也不剩了（笑)”。  
下面这张图(FC第1卷第1页)，雅彦的表情有些调皮，会不会和上面说的“不良”有些联系?  
![](./img/01_001_0__.jpg)  


----------------------------------------------------------
<a name="ch001"></a>  
### Vol01	CH001	page003		"家庭"的初体验  	First "family" Life Experience  {#ch001}		  


- 01_003，FC开篇，主题是家庭（雅彦怀念家庭）。雅彦（受紫的邀请）去若苗家的路上，遇见了一家三口，这勾起了雅彦的怀念。雅彦："妈妈在我小二的时候，已经过了身，父亲因为工作，经常不在家。。。我时常一个人在家，简直像是处于无依无靠的境况。一个人观看电视剧集，当中出现的场面在我来说，是个憧憬。。。是实现不到的梦想。。。家庭。。。"  
![](./img/01_003_2_5__cn.jpg)  

FC结尾主题也是家庭。


- 01_003, 

下面这幅图，左侧有铁丝网和树木：  
![](./img/01_003_1__.jpg) 

下面这幅图，雅彦背景的道路右侧有铁丝网、左侧有树木：  
![](./img/01_003_3__.jpg) 

结合这两幅图，推测此时雅彦的位置在铁路的左侧。


- 01_003. 
FC新版第一卷封底的照片

![](./img/fc-0.jpg)

是不是这个图？

![](./img/01_003_3__.jpg)


- 01_003～01_013。  
01_003～01_013是回忆镜头，其中01_003～01_006是彩页，镜头没有边框；01_007～01_013是黑白页，镜头有边框。

在叙述方式上，回忆镜头和实际镜头穿插进行。


- 01_003, 01_004, 雅彦的背景介绍：

雅彦妈妈在他小二时去世（01_003），爸爸在一个月前因交通事故去世(01_004)。  
![](./img/01_003_2__cn.jpg)  
![](./img/01_004_0__cn.jpg)  

(注：关于01_003_2里的日文“小二”，感谢"知更鸟"和"多蒙卡殊"帮忙确认)


雅彦高中毕业，即将上大学。雅彦(01_004_3)："下个月----4月----就是大学入学的日子..."  所以现在是3月（因为当天是紫苑16岁生日（01_020）所以是3月19日）。  
![](./img/01_004_3__cn.jpg) 

所以，3月19日是上半学期开学前。  


目前，父亲留给雅彦的遗产（仅仅父亲去世一个月后）已经快花完了，雅彦在担心生活费，正在看招聘信息找工作：  
![](./img/01_004_1__cn.jpg) 
![](./img/01_004_1__en.jpg)   

01_004_2, 中英文不一致：    
![](./img/01_004_2__.jpg)  
中文，雅彦：“怎么没有轻松一点的兼职...!!”  （注：从中文翻译可以隐约感觉到雅彦可能有些懒散。同时，屋里很乱，可能是雅彦受亲人去世的影响心情低落，也可能是他本来就比较懒散）  
英文，雅彦：“Not one interesting job(都是无聊的工作)...”  


- 01_003, 中英文不一致：

![](./img/01_003_2__.jpg)  
![](./img/01_003_2__cn.jpg)  
中文，雅彦：妈妈在我小二的时候已经过身了....  
英文，雅彦：My mother died while I was still attending kindergarten(妈妈在我上幼儿园的时候就去世了)...

（注：关于kindergarten的解释（Longman）：  （美）a school or class for children aged 5;（英）a school for children aged 2 to 5.）  

假设4岁上幼儿园、6岁上小学的话。如果中文翻译正确的话，则雅彦7岁时妈妈去世。如果英文翻译正确的话，则雅彦5岁时妈妈去世。


- 01_004_2, 左下角的蓝色桌子，后来应该是被搬到了若苗家，放在了雅彦的卧室。

![](./img/01_004_2__.jpg)  


- 01_003，01_005， 有两个画面都标注了本话的标题：  
![](./img/01_003_1__.jpg) 
![](./img/01_005_0__.jpg)   


- 01_006, 由门牌"柳叶"介绍了雅彦的姓氏, 由紫的打招呼介绍了雅彦的名字：

![](./img/01_006_0_1__cn.jpg)  

- 01_007, 紫自我介绍：“我就是他的妻子，若苗紫”

![](./img/01_007_4__.jpg)  


- 01_008，紫来找雅彦的原因是想收养雅彦，且会供雅彦上大学(01_008_2，01_008_3)：    

中文，雅彦：那个舅父两夫妇的若苗家（妈妈的姓氏）----知道父亲去世的消息，所以想收养我，舅母就是为此事，来我家拜访的。  

英文，雅彦：To go and live with the Wakanae(去和若苗家一起住)... My aunt came to make this proposition, knowing my father had just passed away(知道父亲去世后，舅母前来这么建议). 


- 01_008， 雅彦回忆了紫来访的目的：为了收养他，并供他上大学。面对紫的提议，雅彦说(01_009_1)“的确是很好的建议，但这么突然。。。”，心里觉得(01_009_2)"只觉得事情来的太美好了"。他内心独白：若苗家要收养自己、且“更说会供我念什么大学的...尽是吸引人的好话...”(01_008_2)，从这句话可见雅彦不太相信紫说的这些话。（疑问：这能否反映出雅彦内心或者性格上的一些特点？比如，不轻易相信人、不相信天上掉馅饼）

参考资料：  [关于日本的成人收养](http://www.hojocn.com/bbs/viewthread.php?tid=96804)  
        或 [关于日本的成人收养](https://mp.weixin.qq.com/s/2i_3DqSKoNibdaOa8nBycQ)  


- 01_009, 01_012, 紫邀请雅彦今天来若苗家参加紫苑的16岁生日（01_020）聚会：

![](./img/01_009_3_4__cn.jpg)

若苗家的地址：丰岛区杂司谷4丁目24-15，电话:xxxx-5xx1

![](./img/01_012_6__.jpg)
![](./img/01_012_6__en.jpg)

关于若苗家住址的原型, 可参考[FC实物集](../fc_things_in_real_world/readme.md)  


- 01_003～01_009。开篇，"雅彦怀念家庭、紫来访"这段戏让我感觉很压抑。不清楚作者是怎么达到这种效果的，我猜，是不是有这些原因：

    - 路人一家三口温馨的画面，作为反衬  
    - 雅彦丧失双亲，还要为生计而找工作
    - 紫提出了好的机会（收养、供上大学），雅彦却不相信这是真的，也不敢接受。
    - 多个俯视镜头：  
![](./img/01_004_2__.jpg) 
![](./img/01_008_4__cn.jpg) 
![](./img/01_009_2__cn.jpg)  
    - 其他镜头：  
![](./img/01_009_5_8__cn.jpg)    


- 01_008_3，在骑车去若苗家的路上，雅彦想："知道父亲去世的消息，所以想把我收养，舅母就是为此事，来我家拜访的。更说会供我念什么大学的。。尽是吸引人的好说话。。。。。。但是，只觉得事情来得太美好了(01_009_2)。。。"

这说明雅彦不太信任若苗紫，不相信这样的好事会发生在自己身上。后续(01_017_0), 借紫苑的玩笑说出雅彦内心的疑惑："(若苗家给雅彦的)提议这样完美，一定有什么阴谋在内".


- 01_010, 紫对雅彦说："以前只见过一次，我结婚时跟你父母打过招呼。。。当时你年纪太小，所以记不起来吧！"  
![](./img/01_010_4__cn.jpg)  

这和后续剧情不符：雅彦母亲病重时，紫曾去医院探望（05_181），当时见到了年幼的雅彦。


- 01_011, 中英文不一致(感觉中英文翻译意思完全相反)：

![](./img/01_011_4__.jpg)
![](./img/01_011_4__cn.jpg)  
![](./img/01_011_4__en.jpg)  

紫从雅彦家走后，雅彦看着母亲的照片。

中文，雅彦：“其实。。。也不是，太相似。。。不相似的。。。嗯！！”  
英文，雅彦：“It's incredible...such resemblance... Unbelievable!!”（无法想象。。。真是太像了。。。难以置信！！）

根据后续(01_012_3)中文“不单只是。。。脸容。。。总觉得那气质。。。那温柔的感觉，跟妈妈她----”，我猜，上述中文翻译错了。


- 01_013_5，雅彦决定去若苗家看看，骑上单车后远去。01_013_4是远去的背影，01_013_5是若苗家附近的交通灯。01_013_5特别之处是其大小突出出来一部分(如下图蓝色边框所示)占据了01_013_4的一块区域。  
![](./img/01_013_mod.jpg)  


- 01_014，紫出场之后，紧接着是紫苑出场。

雅彦到电车站时，天快黑了：  
![](./img/01_013_4__.jpg)  
![](./img/01_014_0__.jpg)  

雅彦家在三鹰（Mitaka）:  
![](./img/01_015_3__cn.jpg)

[地图](https://cn.gullmap.com)显示从三鹰市到杂司谷(若苗家01_012_6)约16.2km，骑行约55min。由此推测雅彦出发时是下午。  


从这图猜测，雅彦是从"若苗家出大门右手边"的方向骑车来的。  
![](./img/01_018_4__.jpg)  
进而猜测，雅彦和紫苑邂逅的那个电车站（01_013_4__）大致在"若苗家出大门右手边"的方向：  
![](./img/01_013_4__.jpg)   


沿"若苗家出大门右手边"的方向骑往若苗家，沿路的景象：   
![](./img/01_048_6__.jpg) 
![](./img/01_016_0__.jpg)  
![](./img/01_016_4__.jpg)  
![](./img/01_017_3__.jpg)  
![](./img/01_017_6__.jpg)  


- 若苗家的门牌：  
![](./img/01_019_0__.jpg)  

- 紫苑16岁生日：  
![](./img/01_020_4__cn.jpg)  
从下面的照片里看到当天是1997年3月19日：  
![](./img/02_000a__mod.jpg)  








- 01_032, 紫能抱起紫苑（01_032_2）、背起雅彦（01_203_0），说明有力气：  

01_032_2，雅彦到若苗家参加紫苑的生日聚会时，紫苑为了揭露空和紫的秘密，故意向紫身上泼水。紫生气地抱起紫苑（送到屋里）。此时雅彦有点吃惊。

01_203_0，紫临时代替浩美去夜总会，空让雅彦去照看紫。最后雅彦喝醉后，被紫背回家。



----------------------------------------------------------
<a name="ch002"></a>  
### Vol01	CH002	page043		天体之夜  	A Torrid Night  {#ch002}  		  


- 01_045, 发现若苗家的秘密后，雅彦昏倒。第二天雅彦在若苗家醒来后，忘记了昨晚发生的意外。雅彦说“我在这里，日后麻烦大家照顾了”，这应该是接受了紫的收养：    
![](./img/01_045_1__cn.jpg)  


- 01_048_1, 雅彦决定搬去若苗家住，一边骑车一边想入非非。差点和一辆mini撞上。应该是獠的mini吧。  

- 01_055，雅彦弄脏了空的原稿，空发火拍桌子：  
![](./img/01_055_3__.jpg)  


- 01_055_4, 空的原画稿纸大小和空的手掌大小相比：纸长约为3倍手掌长; 纸宽约为3倍手掌宽。  
假设空的手掌大小为一般成年男性的手掌大小（约190mm x 110mm）,则纸大小约为（540mm x 330mm）。由此猜测，空的原画纸张大小为4K（543mm x 390mm）。


- 01_060, 第一次来若苗家后的下一周，雅彦搬来若苗家住：   
![](./img/01_060_4__cn.jpg)  

- 01_061，中英文不一致：

雅彦记不得那天晚上发生的事了，雅彦问紫苑：“我做了什么傻事吗？”

中文，紫苑问：“我想知道究竟有什么事？”  
英文，紫苑问：“You really want to know？” (你真想知道？)

后续，紫苑带雅彦去参加空和助手的庆祝晚会。从此可推测，英文翻译是对的。




- 01_066，真琴的服饰：外套是短袖，里面套的是长袖格子杉。（我以前一直误以为真琴穿的是袖筒...）  
![](./img/01_066_2__.jpg)  






- 01_071, 雅彦在歌厅里想起了之前那天晚上发生的事。这里天空有颗流星：  
![](./img/01_071_2__cn.jpg) 


----------------------------------------------------------
<a name="ch003"></a>  
### Vol01	CH003	page073		温暖的饭  	A Hot Meal  {#ch003}  		

- 01_078, 搬到若苗家的第二天，雅彦已经开始打开箱子整理东西了。图里：1可能是吹风机；2不确定是不是落地灯； 3和4像是同一种，但不知道是什么；5是书本；6、7、9、10可能是纸屑；8像是长棍，不知道是什么。    
![](./img/01_078_1__mod.jpg)


- 01_079_2, 雅彦不太接受若苗家的秘密，离开并来到原先的家。但“已不能会去了。。。又没有钱，房子又已经有人租用了。。。”


- 01_079_5，01_079_6，空内疚天体之夜吓到了雅彦，因为紫苑一直急于向雅彦展示若苗家的秘密，所以空要找紫苑谈话(01_077_0)。后续，空(01_079_5，01_079_6)欲找紫苑谈话，却找不到她。空生气得青筋爆起：  
![](./img/01_080_0__.jpg)  

01_080_1 紫的面部加了阴影：  
![](./img/01_080_1__cn.jpg)

空提议今天为雅彦开庆祝会（01_080_7）并亲自下厨，这里提到空的厨艺不好（01_081_2）。这里借空和紫的谈论雅彦，把情节切换至紫苑和雅彦这条线。


- 01_081, 柳叶家附近的便利店：Friend Market。 一份盒饭625日元（约40RMB）。  
![](./img/01_081_3__.jpg)  
雅彦讨厌把凉了的饭再加热(01_081_7__)，觉得难吃。  


- 01_082, 01_083, 这两页很悲伤。  
01_082, 雅彦："冷饭盒味道更好。。。因为已经习惯了。。。但是，从和是开始的呢！？令我养成吃冷饭习惯的。。。是因为----"。接下来是过渡到回忆的镜头：  
![](./img/01_082_2__.jpg)  
回忆里，父亲的镜头没有显示表情，这可能是因为父子交流少、记忆淡漠；也可能是为了渲染父亲的冷漠：  
![](./img/01_082_4__.jpg) 
![](./img/01_082_5__.jpg) 
![](./img/01_083_0__.jpg) 

父亲的台词也有些冷漠，和小雅彦的热情放形成反差，制造悲伤的情绪：  
雅彦："呀！已冷掉了呢！爸爸"(01_082_3)  
爸爸："什么？雅彦仍未吃晚饭吗？"(01_082_4)  
雅彦："嗯，我想跟爸爸一起吃。"(01_082_4)  
爸爸："爸爸说过要晚点回来，叫你趁热吃的吧？"(01_082_5)  
雅彦："。。。嗯。。。。"  
雅彦："爸爸从今天起要经常加班。。。。会在公司吃过饭才回来，你也不用煮二人分量的了。。。"(01_083_0)  
雅彦："。。。嗯。"(01_083_0)  
雅彦：变成只一个人吃饭时，便觉得煮一人分量的饭太麻烦了。。。不知不觉就时常光顾便利店的饭盒。。。  
![](./img/01_083_0_1__en.jpg)  
这两个镜头形成对比：道具(雅彦的服饰、背景里的日历、桌上的盒饭、饮料、调料)的差异暗示岁月流逝、物是人非；父亲在镜头里消失，可能指父爱缺失。仔细看还能发现，这两个镜头里雅彦头部和背景窗户的相对位置有差异，说明雅彦长高了。    

接下来(01_083)回忆母亲。这里先回忆父亲、后回忆母亲；这和母亲先去世、父亲后去世的人物顺序不一致。我猜，可能是一般人对母亲的感情更深，所以把回忆母亲放在后面，利于进一步制造悲伤的情绪。  
雅彦：家里的电饭煲便变得没用了。。。后来，更只成为厨房中的饰品。  
雅彦：妈妈在的时候，也吃过用它烧出来的熟饭。。。现在。。。已记不起来了。。。  
![](./img/01_083_2_4__en.jpg)  
这里，用电饭煲衔接镜头序列。  
    - 前两个镜头(01_083_2,01_083_3)：电饭煲渐渐远离镜头，可能在暗示亲情越来越远。  
    - 后两个镜头(01_083_4,01_083_5)：雅彦动作类似，所以是借他的动作来衔接镜头。因为提到母亲、并站在电饭煲的视角，所以可能是在用电饭煲代指母亲；最后的镜头里电饭煲消失，指母亲的离世。雅彦手部的动作变得松懈，表示雅彦对母亲的离世感到悲伤而无奈。独白“现在已记不起来了”可能是指记不得母亲的样子了  


- 01_85_2, 01_85_3， 紫苑提到有独立生活的打算。

紫苑：“不是跟你一起！！我想一个人生活，作为一个独立女性，要与家庭分开！！”，

紫苑：“因为，父母一定会说我人年轻。。。不能这样。其实上高校时已经想独立的了。"（注：这里日文的“高校”指"高中"）

我觉得，这可能有一定的可信度，而不单单是为了打探雅彦离开若苗家的原因。

01_098_4, 雅彦被有温度的白米饭感动(内心独白)："我一边吃着饭，一边呆呆的在沉思--紫苑她真的那么想离家出走吗？"。我猜，既然作者借雅彦的口这么说，说明紫苑并不是想独立生活。


- 01_88_4, 雅彦和紫苑来到的酒店是：Hotel ikoi 憩

后续，紫苑在影研社拍戏，雅彦跟踪，发现紫苑出现在"Hotel 憩"(02_132_0)门口. 



----------------------------------------------------------
<a name="ch004"></a>  
### Vol01	CH004	page099		开学礼的条件  	Entry Ceremony  {#ch004}  		  


- 01_108_4，01_108_5，江岛在开学典礼上和雅彦攀谈。  



- 01_109，空和紫异装后，两人身高差很多。因为空本来就比紫高，现在空又穿了高跟鞋。  

01_110_2, 边框注释：本来就高个子，穿上高跟鞋更有1.9米呢！   

同时，01_113_3、01_121_5这两个画面里可以看到空的高跟鞋，估计鞋跟有3cm~5cm。  
所以，可以推测出空的身高可能大概是185cm ～ 187cm。  


- 01_114_3，若苗夫妇的苦恼：我们如何装扮，阿空始终像男人，阿紫也始终看来似个女人。  


- 01_115_2, 01_118_1, 镜头里的墨镜男子，是老北出镜吗？  

01_115_2，这个男子要拿空和紫的异装搞个大新闻，说："错失了这精彩特稿的画，就败坏新闻部的声誉了！！", 可见他是武蔵野产业大学新闻部的(骨干?)成员。 01_115_4，他说：“摄影部也要帮手打气啊”。这里提到了大学的新闻部和摄影部，是指的新闻社团和摄影社团吗？ 北条司喜爱摄影，和这有无联系？  



- 01_121, 紫苑衣服上的字是不是“Mission Impossible”?(第04话是1996年9月，电影《Mission Impossible》是1996年5月上映的)  
![](./img/01_121_0__crop.jpg)


- 01_122，  
![](./img/01_122_5__.jpg)
![](./img/01_122_5__cn.jpg)
![](./img/01_122_5__en.jpg)

英文版，空：“It's the parent's role to realise the children's wishes. Even if they're imossible!!”  
这里提到了impossible，不知道和01_121_0紫苑衣服上的“Mission Impossible”有没有联系？

- 01_123_1。边框、若苗空头发、对话框的遮挡关系。  
![](img/01_123_1__.jpg)  



----------------------------------------------------------
<a name="ch005"></a>  
### Vol01	CH005	page125		棒球的回忆  	The Glove Of Memories  {#ch005}  		


- 01_127_0, 01_127_1,   
紫：“阿空他。。。一直希望有个自己的儿子。与自己的儿子玩棒球是他的梦想。有了紫苑的时候，便预定是男孩。。。出世前已预先买了棒球手套回来。”  

可见，空喜欢棒球，且希望有个男孩子；棒球手套是紫苑出生前买的。

这是第1卷第5话，此外FC里第9卷59话也是与棒球有关的故事（棒球手松下敏史）。


- 01_135, 第5话“棒球的回忆”，开始产生"紫苑到底是男是女"这个问题。紫苑的衣服上有"BOY"字样, 有故意迷惑雅彦(和读者）的可能。  

3岁儿童节时是女装；5岁儿童节是男装；幼稚园时是男装。小学入学时是女装；小学四年级时是男装；小学六年级时是女装；  
紫苑：（01_146_3）“但上中学时，团团转的变换性别，太麻烦了。现今的世界，我相信女人比较有利。。。所以至今一直都是做着女生。”，  

紫苑：（01_147_2）“我像男人吗？是女人！！起码现在是。。。啊❤暂时只能这样说，将来如何，不知道呢！”    
所以，这里紫苑明确告诉雅彦她是女生。至于未来不确定，大概是指类似光浩变浩美的可能。


关于"紫苑的是男是女"这个话题，我一直以为这仅仅是作者设置的噱头而已，我以为其目的是为了迷惑雅彦以便二人关系不要发展得太快。但后来，我看到《北条司拾遗集.画业35周年纪念》里(147页)有访谈：  
----最后，非常家庭以后情节会任何展开呢？  
北条： 会有紫苑的性别问题，该如何处理呢（笑）。或许不会明确地画出来（笑）。雅彦和紫苑是第二代性别倒错夫妇。剧情本身没有没有大事件发生，也没什么计划，一边看角色的动态一边展开情节，老实说，未来的事一点也没考虑。只是，这是一部不同的作品，有着常年一直保留的素材，它以后会变成什么样子呢，经请大家期待（笑）。

从这个访谈看，FC里"紫苑的是男是女"这个梗不仅仅是个噱头，作者真的有考虑过"紫苑是男生"的这种结局。  


- 01_144, 紫苑所在的小学：“x测小学校”。 书包旁边的小袋子上的文字应该是"SHION"(紫苑的英文名)

![](./img/01_144_5__.jpg)




- 01_148, 01_149. 若苗家出门左转的场景。

![](../fc_sequence/img/01_148_149_wakanae_house_turn_left.jpg)

01_148, 01_151. 若苗家出门右转的场景。（雅彦学校的方向）

![](./img/01_148_151_wakanae_house_turn_right.jpg)

03_144, 若苗家出门右转,去车站(附近的一个咖啡厅去接顺子（空的妹妹）)

![](./img/03_144_0__.jpg)

----------------------------------------------------------
<a name="ch006"></a>  
### Vol01	CH006	page153		在远处遥望的初恋  	A First Discrete Love  {#ch006}  		



- 01_156_6，齐藤玲子的住址：札幌市北区北三十二条 一七第三xxxx(日文)803

齐藤玲子于x月x日(01_156_2的邮戳看不清)寄出信件给紫苑，说紫苑是她的初恋情人，没有吐露就转校了。下周要到东京毕业旅行，想见紫苑。（01_157）

紫苑说那是7年前的事（01_158_3）。所以推算，紫苑现在高一或高二。


- 01_158_7，  
紫苑：“初恋是女孩子最重要的回忆啊！随便把这回忆粉碎，才令她伤心啊！！”

----------------------------------------------------------
<a name="ch007"></a>  
### Vol01	CH007	page179		助一臂之力  	Just A Little Service  {#ch007}  		



- 01_157, 03_069. 雅彦喜欢听音乐。有CD和耳机。

![](./01_157__03_069__masahiko_music.jpg)

北条司大学期间酷爱音乐（参见<<黑白漫画文化.墨("城市猎人"北条司)>>第7页，“孜孜不倦的勇者”一文)，所以，北条司是否把这个爱好映射给了雅彦。另，紫苑的房间里好像没有见过和音乐相关的东东。紫苑不爱音乐吗？


- 01_162，前景人物的边缘线很粗，不知道这是表示什么效果。（为了表现画面的层次感？）  
![](../fc_sequence/img/01_162_6__.jpg)  
14_027_2__也有类似的镜头：  
![](../fc_sequence/img/14_027_2__.jpg)


- 01_169（第六话第二十页），这个建筑像是个桥拱的桥
 
![](./01_169_0__.jpg)

01_046（第二话第四页），雅彦路过的这个桥。这两处会是同一个景点吗？

![](./01_046_4__.jpg)


- 01_169（第六话18页），暗恋紫苑的那个女孩子(齐藤玲子)回忆小时候紫苑在学校打棒球，右下角的背影应该是空

![](./01_169_4__.jpg)

19页显示果然是空。这些是齐藤玲子记忆里的画面

![](./01_170_0__.jpg)






- 01_176, 为了不被雅彦打扰，紫苑带玲子去游乐场玩时把电话关机了。和玲子分别时，紫苑想起来雅彦，于是打开电话。然后听到雅彦在电话里咆哮。这里，蓝框里的镜头是什么意思？[疑问]：

![](./img/01_176_2_5__mod.jpg)  
![](./img/01_176_2_5__cn.jpg)

- 01_184_4，01_185_0


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


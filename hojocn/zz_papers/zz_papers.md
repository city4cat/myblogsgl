
## 与北条司相关的论文  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96846))    
一些论文里提到了北条司。这里搜集这些论文，节选其中与北条司相关的段落，并翻译为中文。  




-----------------------
**[La représentation de la nature dans les mangas (2) : à la rencontre de la nature dans la ville dessinée japonaise : pistes introductives (I)](https://labojrsd.hypotheses.org/1779)**  
(漫画中自然的表现(2)：在日本漫画城遇见自然：入门轨迹(I) )  
par Bénédicte Tratnjek · 31/03/2014

Dans la ville-centre (et tout particulièrement dans le centre tokyoïte, très fortement représenté dans le manga), la nature en ville apparaît principalement comme « assignée » à certains espaces qui lui sont dédiés. Alors que l’action de City Hunter et de sa suite alternative Angel Heart (de Tsukasa Hojo) situe Ryo Saeba (le personnage principal, plus connu en France sous le nom de Nicky Larson, du nom de la traduction de l’anime dans les années 1990) dans les espaces de la criminalité tokyoïte (et donc dans les territoires de « l’ombre », de la nuit et de l’informel, tous situés dans les espaces du Tokyo « hyperdense »), sa rencontre avec Kaori (Laura dans la traduction de l’anime en français) va le confronter aux territoires de l’« ordinarité » et le ramener ponctuellement dans les « territoires du jour » (que le personnage confronte aux « territoires de la nuit », auxquels il appartient par son métier de « nettoyeur », c’est-à-dire de tueur à gages). C’est sa rencontre avec Kaori et avec les jeunes femmes qu’il va protéger tout au long des tomes qui le confronte aux espaces de la nature en ville : dès lors qu’il accepte de les protéger, il « sort » des territoires de la criminalité, et les rejoint temporairement dans les territoires de l’« ordinarité ». Au fil des tomes, Ryo Saeba se trouve donc régulièrement confronté à des promenades dans les parcs et jardins urbains de Tokyo : avec lui, le lecteur découvre ses balades, leur rôle dans les promenades en couple, leur place esthétique dans la ville. Les parcs urbains apparaissent, dans ces parcours dessinés, comme des espaces dédiés aux loisirs et à la fête. Ce sont des espaces appropriés pour des usages récréatifs, mais aussi, par les fêtes, pour célébrer la sacralité de la nature (dans le billet suivant, on abordera plus en détail cette nature en ville par les différents éléments de « décor » urbain, dont l’arbre et le parc urbain).  
(在城市中心（尤其是东京的中心，在漫画中非常有代表性），城市的自然界主要表现为“分配”给某些专用于它的空间。虽然《城市猎人》及续集《天使之心》（由北条司创作）将冴羽獠（主角，在法国更广为人知的名字是Nicky Larson，源于1990年代动漫的翻译）放置在东京犯罪的空间中（因此，在“影子”，夜晚和非正式场合，都位于“东京超密集区”的空间中），他与香的会面（在动漫的法语译本为Laura）将与他的领地面对面。 “普通性”，并按时带回“白天的领土”（角色与“夜晚的领土”相对，他通过“清洁工”的职业而属于该职业，也就是说，是雇佣杀手的职业） 。这是他与香以及与年轻女子的会面，他将在整个与大自然面对的空间中保护她们：一旦他同意保护她们，他就“离开”犯罪领土，并加入其中。他们暂时在“普通性”的领土上。因此，在整本书中，冴羽獠经常会在东京的城市公园和花园中遇到散步：与他一起，读者会发现他的散步，它们在情侣散步中的作用以及在城市中的审美地位。在这些设计路线中，城市公园似乎是休闲和庆祝活动的专用空间。它们是休闲娱乐的合适空间，但也可以通过节日来庆祝自然的神圣性（在下一篇文章中，我们将通过城市“装饰”的各种元素（包括树木）更详细地讨论城市中的自然。和城市公园）。 )  

![](img/City_Hunter_Hojo.jpg)  
La première planche de City Hunter : le parc urbain  
comme territoire de récréativité des habitants « ordinaires »  
Source : Tsukasa Hojo, 1985, City Hunter, tome 1, planche 1.  
(城市猎人的第一话：城市公园  
作为“普通”居民的休闲区  
资料来源：北条司，1985年，《城市猎人》，第1卷，第1页。 )  

......

Sous un rayon de soleil (de Tsukasa Hojo) est une œuvre emblématique de cette représentation de la nature dans le périurbain japonais. En effet, Tsukasa Hojo (plus connu pour des titres comme Cat’s eye, City Hunter ou encore Family Compo) narre l’histoire d’une jeune fille, Sarah Nishikujo, qui tient avec son père (dont le character design rappelle un personnage particulièrement apprécié des lecteurs dans City Hunter : Umibôzu ou « l’éléphant » – surnommé Mammouth dans la traduction française de l’anime) un magasin de fleurs itinérant. L’histoire commence lorsque Sarah et son père viennent s’installer dans la ville périurbaine où vit Tatsuya Kitazaki, un jeune garçon qui s’apprête à scier un styrax, qu’il considère comme responsable de la chute de sa petite sœur, Satsuki. A partir de cette rencontre, Tatsuya va, aux côtés de Sarah, découvrir le langage des plantes et voir, à travers la ville, la place de la nature (et tout particulièrement des arbres et des fleurs) dans les territoires de son quotidien. Quand Sarah partira, Tatsuya et tous ceux qui ont côtoyé la jeune fille auront appris à regarder et aimer la nature, et à respecter sa place dans la ville.  
(《阳光少女》（北条司）是日本近郊地区这种代表自然的标志性作品。的确，北条司（以《猫眼》、《城市猎人》或《非常家庭》等作品而闻名）讲述了一个年轻女孩西九条纱罗的故事，她与父亲保持着联系（其角色设计让人回想起《城市猎人》中一位特别受赞赏的角色读者： 海怪或“大象”-在动漫的法语翻译中被称为猛玛象），是一间旅行的花店。故事开始于纱罗和她的父亲搬到北崎达也居住的郊区城镇，一个年轻男孩正要砍樱花树，他认为这是导致妹妹Satsuki病倒的罪魁祸首。在这次会议上，达也将与纱罗一起探索植物的语言，并在整个城市中看到她日常生活区域中的自然风光（尤其是树木和花朵）。纱罗离开后，达也和所有认识这个年轻女孩的人们将学会看待和热爱自然，并尊重她在城市中的地位。 )  

![](img/Sous_un_rayon_de_soleil_Hojo.jpg)  
Sarah défendant le styrax contre Tatsuya  
Source : Tsukasa Hojo, 1994, Sous un rayon de soleil, © Tonkam  
(réédition avec planches couleurs Ki-oon, 2013), tome 1.  
(纱罗为达也捍卫针锋相对  
资料来源：北条司，1994年，阳光少女，©Tonkam  
（用Ki-oon彩色印版重新发行，2013年），第1卷。 )  

Si dans le manga Sous un rayon de soleil Hojo fait de la nature en ville le thème de sa narration (tout comme Taniguchi le fait dans ses mangas prenant scène dans la ville japonaise), bien d’autres mangas confrontent le lecteur aux espaces de la nature dans le périurbain japonais. De Card Captor Sakura (sutdio Clamp) où l’on suit la jeune Sakura, élève de primaire dotée de pouvoirs, dans ses territoires du quotidien entre l’espace domestique, l’espace scolaire et les espaces récréatifs du périurbain, à la vie du lycéen Ranma Saotome dans Ranma 1/2 (de Rumiko Takahashi) qui s’installe avec son père dans la famille d’Akane Tendô en plein espace périurbain et qui confronte le lecteur à la forte présence de l’eau urbaine (voir dans le billet suivant la partie sur la rivière dans la ville dessinée), la nature périurbaine dessinée est aussi présente quand la nature, l’environnement et l’écologie ne sont pas des thèmes centraux du manga. Elle s’ancre alors dans les territoires du quotidien, et s’offre davantage au lecteur attentif de cet espace-scène. Mais, elle reste très présente, et donne à voir (tout comme dans la représentation de la ville-centre) l’usage des habitants et usagers « ordinaires » de cette nature en ville. Elle n’apparaît aux personnages qui ne sont pas confrontés directement à sa présence davantage un espace-décor de leurs quotidiennetés qu’un paysage. Loin de la dimension paysagère et esthétique dans laquelle nous entraîne Jirô Taniguchi ou Tsukasa Hojo dans ses œuvres courtes telles que Sous un rayon de soleil ou Le temps des cerisiers, la majorité des mangas représentent la nature dans la ville dessinée (que ce soit dans la ville-centre ou dans les espaces périurbains) comme un espace-scène du quotidien, rappelant ainsi sa forte présence mais aussi son « ordinarité ».  
(如果在漫画《阳光少女》中，北条把叙事的主题变成城市中的自然（就像谷口在日本的漫画中所作的漫画一样），那么许多其他漫画在自然环境中与读者面对面。日本城市周边地区。从Card Captor Sakura（sutdio Clamp）那里，我们跟随年轻的Sakura，这是一个拥有力量的小学生，她在家庭空间，学校空间和郊区的娱乐空间之间的日常领地，一直到城市生活。高中学生乱马 Saotome（Rumiko Takahashi）位于乱马1/2，他与父亲定居在郊区附近的Akane Tendô的家庭中，并以强大的城市用水与读者面对面（参见文章）在自然界，环境和生态环境不是漫画的中心主题时，也将出现城市周边自然界。然后将其锚定在日常生活领域中，并为这个舞台空间的细心读者提供更多帮助。但是，它仍然非常存在，并显示（如代表城市中心）城市中这种性质的居民和“普通”用户的使用。对于不直接面对它的角色来说，它看起来更像是日常生活中的空间装饰而不是风景。漫画中的漫画绝大部分都代表着绘制的城市中的大自然（无论是在市中心）或在城市周边空间）作为日常场景空间，因此让人联想到它的强大存在和“平凡”。 )


-----------------------
**[La représentation de la nature dans les mangas (3) : La végétation et l’arbre dans la ville japonaise dessinée : La nature dans la ville (II)](https://labojrsd.hypotheses.org/1801)**  
(漫画中自然的表现（3）：绘制的日本城市中的植被和树木：城市中的自然（II） )  
par Bénédicte Tratnjek · Publié 01/04/2014 · Mis à jour 01/04/2014

L’arbre est particulièrement présent dans la ville dessinée du manga. L’arbre traverse, par exemple, l’œuvre de Tsukasa Hōjō, depuis Le temps des cerisiers ou Sous un rayon de soleil [4] où l’héroïne est la fille d’un marchand de fleurs qui communique avec l’âme des arbres [5], jusqu’au célèbre City Hunter (plus célèbre en France sous le nom du titre de l’anime Nicky Larson) et sa suite alternative Angel Heart qui, pourtant, prennent place davantage dans les quartiers « chauds » de Tokyo, mais où l’on voit quelques scènes romantiques réunissant Ryo (Nicky dans l’anime) et Kaori (Laura). L’arbre polarise ainsi des espaces-refuges, où les personnages vivent des moments festifs ou des moments romantiques. Dans le manga, la poétique de l’arbre dans la ville est rythmée par les saisons et les floraisons, tout autant que par une présence « ordinaire » dans les territoires du quotidien. De plus, l’arbre est souvent associé à des esprits divins, qui permettent la mise en scène de rites de passage (particulièrement présent dans le scénario des shōnen où l’on suit très souvent le parcours initiatique d’un jeune adolescent vers l’âge adulte) et sont associés à la vie quotidienne de nombreux personnages, qu’ils habitent l’espace rural ou l’espace urbain. Ainsi, par la représentation de l’arbre dans la ville, le manga met souvent en scène une nature qui participe des territoires du quotidien (même pour les plus bétonnés et les plus marqués par la verticalité d’entre eux). Par l’arbre, le manga dessine non seulement des espaces-scènes (des « scènes de théâtres », des décors sur lesquels se déroule l’action), mais bien plus encore des « paysages du quotidien ». Le lecteur se voit confronté, par la présence de l’arbre dans la ville japonaise dessinée, à la dimension sensible de l’espace urbain, marqué par un environnement familier pour les personnages, où la nature et la culture s’entremêlent sans être pensées comme des éléments contradictoires.  
(该树在漫画漫画城中尤为突出。自从樱桃树时期或在阳光下[4]，女主人公就是与树的灵魂进行交流的花商的女儿[5]以来，树贯穿了北条司的作品。 到著名的《城市猎人》（在法国以Nicky Larson的名字更出名）及其续集《天使心》（Angel Heart）的拍摄，该片更多地发生在东京的“热”地区，但我们看到了一些浪漫的场景将獠（动漫中的Nicky）和香（Laura）融合在一起。因此，树使庇护所空间两极分化，在角色处，人物经历了喜庆的时刻或浪漫的时刻。在漫画中，城市的树木的诗意因季节和花朵以及在日常生活中的“普通”存在而被打断。此外，这棵树通常与神灵相关联，从而允许通过仪式（特别是在shōnen场景中，我们经常跟随年轻的青少年走向成年的旅程）并与神灵相关联。许多角色的日常生活，无论他们居住在农村还是城市空间。因此，通过在城市中树的表示，漫画经常表现出一种参与日常领域的自然（即使对于最具体和最垂直的地区而言）。通过树，漫画不仅绘制了场景空间（“剧院场景”，在其上进行动作的场景），而且绘制了更多的“日常风景”。在绘制的日本城市中，树木的存在使读者面对的是城市空间的敏感维度，其特征是人物所熟悉的环境，自然和文化相互融合，就像相互矛盾的元素一样。 )  

......

Pourtant, le manga ne donne pas seulement voir une grande harmonie entre la société et l’arbre : dans de nombreuses scènes de manga, les arbres, et tout particulièrement des arbres centenaires, sont menacés de la destruction. C’est le point de départ de Sous un rayon de soleil de Tsukasa Hōjō qu’on a abordé dans le billet précédent [8]. Pour ne pas reprendre le même exemple, on prendra ici le cas d’un autre manga de Tsukasa Hōjō, Angel Heart (la suite alternative de City Hunter). L’intérêt de cet exemple provient du fait que Angel Heart est un manga sombre, centré sur les territoires de la criminalité tokyoïte (et même de la criminalité internationale telle qu’elle s’inscrit dans certains espaces de Tokyo, avec la forte présence d’une puissante organisation criminelle taïwanaise, la Zheng Dao Hui). La nature en ville n’est ni un thème, ni au cœur des préoccupations des personnages (contrairement à l’exemple de Sous un rayon de soleil où l’on suit l’histoire d’une jeune fleuriste). Pourtant, Angel Heart laisse une très grande place aux sentiments des personnages et aux drames qu’ils vivent (bien plus que dans City Hunter où les personnages secondaires contactant Ryo Saeba pour être protégés ou vengés vont être entraînés le plus souvent dans des situations cocasses, dans Angel Heart, Hōjō dessine principalement des récits bouleversants).  
(然而，漫画不仅显示出社会与树木之间的巨大和谐：在许多漫画场景中，树木，尤其是老树受到破坏的威胁。这是我们在上一篇文章中讨论的北条司的《阳光少女》的起点。不重复相同的示例，在这里，我们以另一部北条司漫画《Angel Heart》（《城市猎人》的续集）为例。这个例子的有趣之处来自这样一个事实，即“天使心”是一种黑社会漫画，集中在东京犯罪（甚至在东京的某些地区发生的国际犯罪）的领土上，并且有强大的台湾犯罪组织，正道会）。城市中的自然既不是主题，也不是角色关注的核心（不像《阳光少女》那样，我们遵循的是年轻的花店的故事）。但是，《天使心》为角色和他们所居住的戏剧留下了很大的空间（比《城市猎人》要多得多，在《城市猎人》中，与冴羽獠联系以被保护或报仇的次要角色通常会被引到有趣的情况下，在《天使心》里，北条主要画动人的故事）。 )  

Lorsque les personnages principaux rencontrent Yasuyuki Oikawa, ils se retrouvent confrontés à l’histoire d’un habitant d’un quartier de l’hypercentre tokyoïte, dont l’habitat est menacé par un grand projet immobilier. Derrière ce micro-quartier, on aperçoit un paysage marqué par la verticalité des grands immeubles. Mais, au milieu de ce bâti dense, reste une parcelle de terrains sur laquelle le lecteur découvre un chantier. Par la pression financières et les menaces physiques des promoteurs immobiliers, les autres habitants du micro-quartier ont dû abandonner leur maison. Mais Yasuyuki est resté, et lutte contre les menaces du promoteur immobilier et des yakuzas auxquels ce dernier s’est associé pour effrayer les habitants. Les deux arbres au pied de sa maison sont le symbole à la fois de son ancrage territorial et de ses souvenirs familiaux. A l’enracinement de l’arbre, répondent l’enracinement de Yasuyuki et son identité territoriale (« pour lui, ces cerisiers sont des souvenirs de ses parents défunts », chapitre 5, tome 1, saison 2). Le conflit d’aménagement entre la société de développement urbain Shintoshi-Kaihatsu KK et Yasuyuki oppose deux manières de percevoir l’habiter urbain : la densification et la verticalité du promoteur immobilier se confronte à la résistance de Yasuyuki. Le cerisier est le symbole de la difficile coexistence entre la résistance (celle d’un habiter marqué par une très forte présence de la nature en ville) et les besoins d’aménagement qui sont ainsi représentés par la destruction de cette nature aménagée. Les deux types d’acception de ce qu’est l’habiter urbain se trouvent confrontés dans cette représentation de la nature en ville, au cœur d’un conflit d’usages. Par ce récit, Tsukasa Hōjō livre au lecteur sa propre représentation sur la place « juste » et « légitime » de la nature en ville, en amenant le lecteur à prendre parti pour la lutte anti-construction de Yasuyuki.  
(当主要人物与大川靖行会面时，他们发现自己遇到了一个东京内城区郊区居民的故事，该郊区的居住环境受到大型房地产项目的威胁。在这个小区的后面，我们可以看到以大型建筑物的垂直度为标志的景观。但是，在这个密集的框架中间，仍然是一块土地，读者可以在上面发现建筑工地。由于资金压力和房地产开发商的实际威胁，小区的其他居民不得不放弃了房屋。但及川康幸留下来，与开发商和黑社会的威胁作斗争，后者与黑社会联手恐吓当地人。他家脚下的两棵树既象征着他的领地，也代表着他的家人。对于树的生根，对康幸的生根和他的领土身份作出回应（“对他来说，这些樱桃树是他已故父母的记忆”，第2季，第1卷，第5章）。城市发展公司Shintoshi-Kaihatsu KK和Yasuyuki之间的计划冲突反对两种感知城市生活的方式：房地产开发商的密度和垂直度面临Yasuyuki的阻力。樱桃树象征着抵抗（在城市中以自然非常强烈的存在为特征的居民的抵抗）和发展需求之间的艰难共存，而这种发展需求则以这种被管理的自然的破坏为代表。在使用冲突的核心部分，这种对城市中自然的表征面临两种对城市生活意义的理解。通过这个故事，北条司通过引导读者支持及川康幸的反建筑斗争，向读者传达了他对城市中“公正”和“合法”自然场所的自己代表。 )  

![](img/Angel_Heart_tome2.jpg)  
Glass Heart rencontrant un homme dans ses rêves, au milieu des arbres  
Source : Tsukasa Hōjō, 2004, Angel Heart, tome 2, © Panini Comics.  
(Glass Heart在梦中的树林里遇见一个男人  
资料来源：北条司，2004年，《天使之心》，第2卷，©Panini Comics。 )  


-----------------------
**[Série « Café géo Ville et BD » (5) : La ville et la guerre dans la bande dessinée](https://labojrsd.hypotheses.org/1548)**  
(系列“地理城市与BD咖啡厅”（5）：漫画中的城市与战争 )  
par Bénédicte Tratnjek · Publié 15/03/2014 · Mis à jour 24/06/2014

Qu’il s’agisse des comics ou des mangas, de nombreuses bandes dessinées explorent la ville comme un territoire de violences, mais aussi comme l’espace où émergent des héros urbains. Les superhéros des comics, ou Ryo Saeba qui, dans Angel Heart (la suite alternative de City Hunter) déclare : « On n’est pas dans la jungle ici ! La ville a sa propre façon de combaaaattre ! » (Tsukasa Hōjō, Angel Heart, tome 2 saison 1, Générations Comics, p. 214). Bénédicte Tratnjek propose donc de poursuivre sur des représentations plus ancrées sur les espaces du « réel » autour de la figure de la ville en guerre. C’est à la fois en tant que lectrice de bandes dessinées et par ses recherches en doctorat qu’elle a abordé la BD par l’approche spatiale : cette réflexion sur la représentation de la ville en guerre dans la bande dessinée provient donc d’un questionnement méthodologique sur le poids de ses propres imaginaires dans la manière dont elle pense son objet d’études, la ville en guerre.  
(无论是漫画还是漫画，许多漫画都将这座城市视为暴力领地，同时也将其视为城市英雄崛起的空间。漫画的超级英雄，也就是冴羽獠，他在《天使心》（《城市猎人》的另一部续集）中说：“我们不在这里的丛林中！这个城市有自己的战斗方式！ »（北条司，《天使之心》，第1季第2卷，《Générations漫画》，第214页）。因此，本尼迪克特·特拉尼耶克（Bénédicte Tratnjek）建议继续在战争中围绕城市形象的“真实”空间中固定代表。既是漫画的读者，又是她的博士研究，她通过空间方法来研究漫画：因此，对战争中的城市在漫画中的表现的反思来自对自己想象力的方法论问题的质疑。她考虑自己的学习目标，战争中的城市。 )  

![](img/Angel_Heart-332x500.jpg)  
La ville et le combat « urbain »  
dans City Hunter / Angel Heart  
Source : Tsukasa Hōjō, Angel Heart, tome 2, saison 1, p. 114.  
(城市与“城市”斗争    
在《城市猎人》/《天使心》  
资料来源：北条司，《天使心》，第1季，第2卷，第1页。 114。 )  


-----------------------
**[Série “Café géo Ville et BD” (3) : La ville dans les mangas (suite) : une forte représentation de l’espace urbain comme laboratoire de la coprésence](https://labojrsd.hypotheses.org/1490)**  
(系列“地理城市和漫画咖啡馆”（3）：漫画中的城市（续）：作为共存实验室的城市空间的有力代表)  
par Bénédicte Tratnjek · Publié 11/03/2014 · Mis à jour 15/03/2014  

Bénédicte Tratnjek souligne que, par-delà la dichotomie urbaphilie/urbaphobie qui représente des rapports extrêmes à l’urbain, par une géographie de l’émotion, la ville dessinée des mangas met surtout en scène des paysages urbains qui permettent au lecteur « occidental » de se plonger dans un habiter qu’il n’attend pas nécessairement dès lors qu’il se représente, dans son imaginaire spatial, la ville japonaise contemporaine, et tout particulièrement la mégapole tokyoïte. Dans les manuels scolaires comme dans les photographies de touristes partagées sur Internet par des réseaux sociaux, Tokyo apparaît comme la ville de la densité et de la verticalité. Si des mangas prennent effectivement scène dans l’hypercentre tokyoïte (comme par exemple City Hunter), un grand nombre d’entre eux nous « dépaysent » et représentent une autre partie de l’agglomération tokyoïte, celle des périphéries. Les mangas permettent alors de confronter le lecteur « occidental » à son propre imaginaire spatial, par la représentation de toutes les formes de l’urbain qui sont présentes dans cette agglomération. Le Tokyo périurbain est particulièrement présent dans deux sous-genres, le shōnen et le shōjo, qui sont de grands succès en librairie (non seulement par le nombre de titres proposés, mais aussi par le succès des ventes de certains titres). Dans ces deux sous-genres, la fiction est souvent élaborée par des rites initiatiques que traverse le jeune adolescent (généralement un jeune garçon dans les shōnen et une jeune fille dans les shōjo) qui grandit au fur et à mesure de ces épreuves. Pour une grande partie d’entre eux, ces adolescents vivent dans le Tokyo périurbain qui nous est donné à voir comme un espace de vie « ordinaire », loin de l’agitation de l’hypercentre. Les paysages urbains du manga Ranma 1/2 (Rumiko Takahashi) témoignent, à titre d’exemples, de cette « ordinarité » de paysages que nous n’associons pas, dans notre imaginaire spatial, à l’agglomération tokyoïte : loin de la densité et de la verticalité des espaces de l’hyper-centre, le lecteur découvre une vie de quartier, où les mobilités reposent sur la proximité, où les paysages sont marqués par l’habitat « traditionnel » ou sur de petits immeubles. Une plongée dans les espaces domestiques est souvent représentée dans les mangas : dans Ranma 1/2, on découvre autant le quartier (l’action se déroule à Nerima, dans l’arrondissement de Tokyo) que l’espace domestique (le dojo familial des Tendô, où s’installent Ranma et son père Genma Saotome). Le lecteur n’est plus, alors, dans le Tokyo de la verticalité, mais découvre différents espaces urbains. De plus, les différentes formes d’espaces domestiques sont données à voir à travers les mangas : de l’immeuble haut de l’hypercentre dans lequel vivent Ryo et Kaori les héros de City Hunter au dojo familial des Tendô dans Ranma 1/2, en passant par la pension de famille des héros de Maison Ikkoku (Rumiko Takahashi, plus connu en France sous le nom traduit pour la version animée : Juliette, je t’aime) ou la clinique Kurosaki, clinique médicale privée tenue par le père d’Ichigo, héros de Bleach, l’habitat est montré dans sa diversité, et la ville « réelle » est représentée par la multiplicité des formes urbaines japonaises. Si certains mangas sont fortement situés dans l’espace (Ryo et Kaori, dans City Hunter, vivent à Shinjuku, un quartier « chaud » de Tokyo, dont l’animation, la fête et les « dessous » donnent corps aux enquêtes et aux vengeances pour lesquels est embauché Ryo, le « nettoyeur » : l’appartenance territoriale des personnages à ce quartier sera même renforcée dans la suite alternative de ce manga, Angel Heart, du même auteur Tsukasa Hōjō, dans lequel il fait de la défunte Kaori l’« âme » du quartier) et placent l’action dans des hauts-lieux de l’identité territoriale tokyoïte (dans City Hunter, par exemple, le lecteur se balade de haut-lieu en haut-lieu, depuis la gare de Shinjuku et le célèbre immeuble My City – Hōjō allant jusqu’à scénariser dans Angel Heart le récent changement d’enseigne de ce centre commercial ; la porte principale du quartier des plaisirs Kabuki Chô , ses bars à hôtesses et ses « love hôtels » ; l’hôtel de police de Tokyo ; la statue du chien Hachiko en face de la gare de Shibuya, la tour de l’immeuble Shibuya 109…), les actions se déroulant dans les périphéries tokyoïtes sont moins souvent situées, ou placées dans des villes imaginaires (comme Karakura pour Bleach). C’est bien un rapport à l’identité territoriale d’une part, et à l’habiter urbain d’autre part que l’on peut lire dans ces géographies subjectives qui mettent en scène la ville japonaise contemporaine.  
(Bénédicte Tratnjek强调，除了代表城市与城市之间极端关系的urbaphilia / urbaphobia二分法之外，通过情感地理学，这座城市被漫画所吸引，主要表现出城市景观，使“西方”读者能够沉浸在他确实居住的居民中当他用空间想象力想象当代日本城市，尤其是东京大都市时，并不一定期望。在学校书本以及通过社交网络在网上共享的游客照片中，东京都显示为密度和垂直度的城市。尽管漫画确实在东京的超级中心（例如《城市猎人》）中占据着中心位置，但其中许多使我们“迷失了方向”，并代表了东京都会区的另一部分，即郊区。然后，漫画通过呈现这种集聚中存在的所有城市形式，使他有可能以自己的空间想象力与“西方”读者面对面。东京周边城市特别存在于两个子类型中，即shōnen和shōjo，它们在书店中很受欢迎（不仅是提供的书名数量，而且还包括某些书名的成功销售）。在这两个子流派中，小说往往是通过青少年的入学仪式（通常是少年中的小男孩和少年中的小女孩）经历的入会仪式来详细阐述的，随着这些试验的进行，这些少年会长大。在很大程度上，这些少年住在东京郊区，我们认为这是一个“普通”的生活空间，远离超级中心的喧嚣。漫画《乱马1/2》（Rumiko Takahashi）的城市景观证明了这种“普通性”景观，在我们的空间想象中，我们不认为它们与东京都会区相关：远离密度和密度。垂直于超中心空间，读者会发现附近的生活，其流动性是基于邻近性的，其景观以“传统”房屋或小型建筑物为标志。漫画通常代表跳入家庭空间：在《乱马1/2》中，我们发现了邻居（动作发生在东京地区的Nerima）和家庭空间（Tendô的道场家族，乱马和她的父亲Genma Saotome定居）。读者不再是垂直东京，而是发现了不同的城市空间。此外，漫画中还展示了各种形式的家居空间：从超大建筑物的高层建筑（獠和香，城市猎人的主角居住的地方）到《乱马1/2》的Tendô家族道场，都经过登机Maison Ikkoku的英雄之屋（高桥留美子（Rumiko Takahashi，在法国更名，动画名称为Juliette，je t'aime））或黑崎诊所，这是由死神的父亲一护的父亲经营的私人医疗诊所，栖息地以多样性呈现，而“真实”城市则以日本城市形态的多样性为代表。如果某些漫画牢固地位于太空中（城市猎人的獠和香居住在东京“红色”区新宿，其动画，舞会和“下层”则为调查和报复提供了实质性内容，而“清洁工”獠则为此进行了调查和报复。“已被雇用：同一位作者北条司在这部漫画的续集《Angel Heart》中甚至强化了角色在该地区的领土归属，在其中他将已故的香称为该社区的“灵魂”）并将动作放置在东京领土地标的高处（例如，在城市猎人中，读者从高处走到高处，从新宿站和著名的MyCity-Hōjō大楼走到该购物中心最近的品牌变化；歌舞伎町游乐区的大门，女主人酒吧和“情趣酒店”；东京警察； 涩谷车站前面的忠犬八公雕像，涩谷109大厦的塔楼de de Shibuya ...），在东京郊外发生的活动很少出现，或者放置在虚构的城市中（例如，Karakura for Bleach）。一方面，这确实与领土身份有关，另一方面与城市居民息息相关，我们可以在这些主观地理位置上阅读这些内容，以展示当代日本城市。 )  

![](img/MyCity_AngelHeart_Hojo.jpg)  
Le changement d’enseigne de l’immeuble « My City » (dans la page suivante, le lecteur découvre la nouvelle enseigne « Lumine Est »), lieu emblématique de Tokyo et du manga City Hunter  
Source : Tsukasa Hōjō, Angel Heart.  
(更改“MyCity”建筑的标志（在下一页中，读者会发现新的“Lumine Est”标志），这是东京和城市猎人漫画的象征地  
资料来源：北条司，《天使心》。 )


-----------------------
**[Série « Café géo Ville et BD » (1) : La ville, la géographie et la bande dessinée](https://labojrsd.hypotheses.org/1298)**   
(“地理城市与BD咖啡厅”系列（一）：城市，地理和漫画 )  
par Bénédicte Tratnjek · Publié 15/02/2014 · Mis à jour 10/03/2014 

Les liens entre mobilités urbaines et bande dessinée sont un autre moyen d’appréhender la BD comme laboratoire de l’urbain. Les bandes dessinées proposent ainsi au lecteur des géographies subjectives : il suit les personnages dans leurs déplacements, qu’ils soient des migrations [18] ou des mobilités quotidiennes, qu’ils soient « ordinaires » ou pénibles. Dans de nombreux mangas, le dessin représente le train comme structurant l’espace : les lignes des trains et métros tokyoïtes sont ainsi des figures récurrentes du paysage urbain tel qu’il est représenté dans le manga, reflétant des pratiques urbaines quotidiennes « cohérentes » avec les déplacements des habitants. « A Tôkyô, deux réseaux se rencontrent : l’éventail des lignes régionales et nationales vient buter sur les principales gares de la Yamanote, qui délimite le contour du centre et le territoire du métro. Ces gares, lieux de passage obligés, se doublent depuis bien longtemps d’une dimension commerciale. Non loin des trains, se dressent ainsi de grands magasins sous l’enseigne de la compagnies des chemins de fer – Keio, Hankyû, Odakyû, Seibu, Tôbu ou Tôkyû » [19]. Rien de tel que le célèbre City Hunter (le célèbre manga de Tsukasa Hōjō, plus connu en France sous le titre de Nicky Larson du nom de l’adaptation en dessin animé tel qu’il a été traduit en France et diffusé par le Club Dorothée) pour rappeler la forte présence des gares dans le manga. Dans Le choix d’Ivana (Tito), le lecteur suit les parcours « ordinaires » d’une jeune femme, Ivana, dans le Sarajevo qui découvre l’arrestation de Radovan Karadžić. Le touriste qui visiterait Sarajevo prendrait le célèbre tramway (qui a même donné son nom au titre d’un récit de voyage dessiné de Jacques Ferrandez : Les tramways de Sarajevo. Voyage en Bosnie-Herzégovine) avec un goût d’exotisme. Mais le lecteur du Choix d’Ivana découvre une autre manière de pratiquer et de vivre cette mobilité : loin de la découverte teintée d’un imaginaire touristique dans cette ville-symbole, le lecteur découvre la ville de Sarajevo tel qu’elle est vécue dans son « ordinarité ». Dans le tramway, moyen de transport « ordinaire » (c’est-à-dire inscrit dans les quotidiennetés) pour Ivana, la jeune femme se retrouve confrontée au poids des événements. Le tramway, présent sur la couverture de cette bande dessinée, témoigne tout d’abord des liens entre territoire et identité dans cette ville dessinée : il permet au lecteur de se situer dans l’espace. Le tramway de Sarajevo est, en effet, un symbole identitaire pour cette ville. Mais, il est aussi un espace pratiqué dans les quotidiennetés d’Ivana. Et c’est dans cet espace qu’elle se retrouve rattrapée par sa propre histoire, confrontée à des inconnus lisant un journal ou discutant de l’arrestation de Radovan Karadžić. Les mobilités de la ville dessinée appréhendent notre propre confrontation à la foule, à l’anonymat, à notre propre relation à l’espace.  
城市机动性与漫画之间的联系是将漫画理解为城市实验室的另一种方式。这样，漫画为读者提供了主观地理条件：无论人物是迁徙[18]还是日常活动，无论人物是“普通”人物还是痛苦人物，他都遵循人物的动作。在许多漫画中，该图代表火车是在构造空间：东京火车和地铁的线条重复出现在漫画中的城市景观图，反映了与居民运动“一致”的日常城市实践。 “在东京，两个网络相遇：区域和国道的范围与Yamanote的主要车站相撞，从而界定了市中心的轮廓和地铁的领土。这些站点是必经的通道，长期以来一直与商业规模相结合。距火车不远，因此在铁路公司（庆应义Han，阪急，小田急，西武，通武或通济[19]）的旗帜下站着多家大型商店。没有什么能回想起著名的《城市猎人》（由北条司创作的漫画，在法国改编并由俱乐部Dorothée播出的动画片改编名中，在法国更名为Nicky Larson） 。在《伊凡娜的选择》（铁托版）中，读者跟随一名年轻女子伊凡娜在萨拉热窝的“平凡”旅程，发现了拉多万·卡拉季奇的被捕者。参观萨拉热窝的游客将乘坐著名的电车（在雅克·费兰德斯（Jacques Ferrandez）绘制的游记中更名：萨拉热窝电车。前往波斯尼亚和黑塞哥维那），品尝异国情调。但是，《伊凡娜的抉择》的读者发现了另一种实践和体验这种流动性的方式：与在这个象征性城市中想象的游客想象相去甚远的是，读者发现了萨拉热窝市，因为它生活在其“平凡”中。在有轨电车上，这名年轻女子发现伊万娜（Ivana）是“普通”的运输工具（也就是说，是日报的一部分），她发现自己面对着重重的事件。在漫画封面上的电车首先证明了这座绘制的城市的领土与身份之间的联系：它允许读者将自己置于太空中。实际上，萨拉热窝电车是这座城市的身份象征。但是，这也是伊凡娜（Ivana）日常生活中实践的空间。在这个空间中，她发现自己陷入了自己的故事，面对陌生人阅读报纸或讨论逮捕拉多万·卡拉季奇的故事。被抽中的城市的机动性使我们与人群的对抗，匿名与我们与太空的关系成为了现实。  


-----------------------
**Japan’s growing cultural power. The example of manga in France**  
(日本日益强大的文化力量--以法国漫画为例)  
Jean-Marie Bouissou  

This survey produced a clear view of the most successful series among the French students and people in work manga fans in 2005. The most interesting result is probably the fact that the fans mix up all the genres. The three superstars – equally popular among and young men and women – are Nana (a shôjo manga by Yazawa Aï), 20 th Century Boys (a seinen manga by Urasawa Naoki) and Great Teacher Onizuka (a shônen manga by Fujisawa Toru) in that order – preceding a mixed bunch of shônen manga for teenagers (One Piece by Oda Eiichirô, Dragon Ball by Toriyama Akira, and Naruto by Kishimoto Masashi) and for more grown-up audience (City Hunter by Hojo Tsukasa), shôjo manga (Fruits Basket by Takaya Natsuki), and seinen manga (Monster, by Urasawa Naoki City Hunter (Hojo Tsukasa), plus a lonely piece of cyberpunk science fiction (Gunnm by Kishiro Yukito). Thus it seems that the genre – and even age – categories are not very significant for the purpose of analyzing “how manga has succeeded in France”. As a consequence, I feel entitled here to analyze “manga” as a whole.  
(这项调查清楚地显示了2005年法国学生和在职漫画爱好者中最成功的系列。最有趣的结果可能是粉丝们混合了所有类型的事实。三位超级巨星在男女中同样受欢迎，分别是娜娜（矢泽爱信的漫画漫画），二十世纪少年（浦泽直树的漫画漫画）和大冢鬼冢（藤泽彻的漫画漫画）。订购–在青少年混合漫画束中（小田荣一郎创作的作品，鸟山明晃的七龙珠和岸本昌史创作的火影忍者）和成年观众（北条鹤司创作的《城市猎人》），少年漫画（水果篮） Takaya Natsuki）和seinen manga（Monster，浦泽直树市猎人（Hojo Tsukasa）所著，再加上一段寂寞的网络朋克科幻小说（Kishiro Yukito所著的Gunm）。对于分析“漫画在法国如何成功”非常重要，因此，我有资格在这里对“漫画”进行整体分析。 )  
......   
ANNEX I.  附件一  
THE MOST LIKED SERIES AMONG STUDENTS AND PEOPLE IN WORK FRENCH MANGA FANS(工作法语漫画中学生和人们中最喜欢的系列)  
The fifteen most popular series among student boys (in that order)  
学生男孩中最受欢迎的十五个系列（按顺序排列）

20th Century Boys (Urasawa Naoki), 20世纪男孩（浦泽直树）  
GTO (Fujisawa Toru),（GTO（藤泽彻）），  
One Piece (Oda Eiichirô), （海贼王（织田荣一郎））   
Dragon Ball (Toriyama Akira), （龙珠（鸟山明））  
Monster (Urasawa Naoki),怪物（浦泽直树）  
Gunnm (Kishiro Yukito),Gunnm（岸希幸人）  
Akira (Otomo Katsuhiro),彰（大友克洋）  
Hunter x Hunter (Togashi Yoshihiro),猎人x猎人（富义义博）  
Planètes (Yukimura Makoto),刨冰（雪村诚）  
City Hunter (Hojo Tsukasa),城市猎人（北条司）  
Nana (Yazawa Ai),娜娜（矢泽爱）  
Evangelion (Sadamoto Yoshizuki) 新世纪福音战士（贞本义月）  
Berserk (Miura Kentarô),狂暴（MiuraKentarô）  
Slam Dunk (Inoue Takahiko),灌篮高手（井上雄彦）  
Naruto (Kishimoto Masashi)火影忍者（岸本昌史）  


-----------------------
FromDrawingToComics__UNI_Gustincic_Iva_2010  

-----------------------
**Pensées du végétal dans Sur les chemins noirs de Sylvain Tesson et L’orme du Caucase et L’Homme qui marche de Jirô Taniguchi**  
(Sylvain Tesson和L'orme du Caucase以及JirôTaniguchi的L'Homme qui marche在《在黑色的道路上》中关于植物的想法 )  
Fabiola Obamé, Université de Bretagne  Occidentale, France   

Les humains sont mis en cause dans la répartition d’un partage territorial inégal. Leur subjectivité joue un rôle important dans la fabrique des paysages car elle morcèle la nature et tente de la dompter pour n’en garder qu’une version policée qui coïncide avec les projets esthétiques de nos villes. Aussi, parce que le regard est sollicité en première instance, l’environnement devient le résultat d’un état d’âme qui exprime en réalité la projection des sentiments. Dans le Tome 1 du manga Sous un rayon de soleil de Tsukasa Hojo, on constate que les sentiments humains affectent les rapports aux végétaux. Sara Nichikujo, fille du jardinier parlant aux plantes, va devoir convaincre un jeune garçon de l’innocence d’un arbre qu’il accuse d’être responsable de l’infirmité de sa petite sœur. Alors que le garçon se montre décidé à abattre l’arbre « fautif » par vengeance, c’est Sara qui va intercéder en faveur de l’être végétal. À la fois porte-parole de l’humain et du non-humain, elle prend en charge la difficile tâche d’apprendre à l’humanité à communiquer avec la nature. Sara mène un dur combat pour stopper l’abattage de l’arbre et pour « réparer les cœurs malades qui ne savent plus aimer la nature » (Tsukasa 25). Ce conte insiste sur les ressentis que peuvent avoir les fleurs et les arbres. Il montre que, bien souvent, les émotions circulant entre l’humain et l’arbre s’épanouissent à l’unisson et déterminent les rapports que l’on entretient avec les végétaux. Ainsi, le monde extérieur tel qu’il se déploie résulterait du paysage intérieur et d’une vision interne qui se construit à partir du regard qu’on pose sur le monde.  
(人类牵涉到不平等的领土划分的分布。他们的主观性在造景中起着重要作用，因为它划分自然并试图驯服自然，以仅保留与我们城市的美学项目相吻合的优美版本。同样，因为凝视是在第一时间被调用的，所以环境成为实际上表达情感投射的心态的结果。在北条司的漫画《阳光少女》第1卷中，我们看到人的情感影响着与植物的关系。园丁的女儿西九条纱罗与植物聊天，她必须说服一个小男孩，他指责这是造成妹妹的身体虚弱的原因。当男孩表现出决心砍倒“有毛病的”树报仇时，纱罗将代表植物为自己辩护。无论是人类还是非人类的代言人，她都承担着教育人类与自然沟通的艰巨任务。纱罗进行了艰苦的斗争，以阻止砍伐树木并“修补不再知道如何爱自然的病心”（北条司 25）。这个故事强调了花草树木可以拥有的感觉。它表明，在人类和树木之间循环传播的情感常常并存，并决定了我们与植物之间的关系。因此，外部世界的发展将源于内部景观和内部愿景，内部愿景是根据我们对世界的凝视而构建的。)  


-----------------------
**The Diffusion of Foreign Cultural Products: The Case Analysis of Japanese Comics (Manga) Market in the US**  
(外国文化产品的扩散：以美国日本漫画（Manga）市场为例)  
Takeshi Matsui, Spring 2009,   

Most important among localization adaptations was “flipping.” As mentioned previously, manga is read right to left, but it was clearly unusual for American readers. So, in order to read left to right, Viz reversed pages horizontally. This “flipping” technique was Shogakukan’s idea and Viz was the first publisher to release flipped manga in the US. But the problem was that some of the manga creators objected to “flipping” of their original works (Horibuchi and Iiboshi 2006: 66-67). Contrary to the American comic industry, manga artists have at least part of the copyright to their work. So, objection by artists to flipping was a major obstacle to acquiring good titles such as City Hunter by Tsukasa Hojo, which was one of the most popular titles in Japan at that time and was finally released by another publisher in 2002. Furthermore, the length of the flipped manga was similar to that of American comics, 32 pages and pamphlet-style, as manga was intended to be distributed to specialty comic stores, the main distribution channel of mainstream American comics.  
(漫画作品本地化(翻译成本国语言)里最重要的是“翻转”。如前所述，漫画是从右到左阅读的，但是对于美国读者来说，这显然是少见的。因此，为了从左到右阅读，Viz水平翻转了漫画页面。这种“翻转”技术是Shogakukan的想法，Viz是第一家在美国发行"翻转"漫画的发行商。但是问题在于，一些漫画家反对“翻转”他们的作品（Horibuchi和Iiboshi 2006：66-67）。与美国漫画业相反，(日本)漫画家的作品至少拥有部分版权。因此，漫画家反对“翻转”是引进优秀漫画作品的主要障碍，例如北条司的《城市猎人》（City Hunter是当时日本最受欢迎的作品之一，并于2002年最终由另一家出版商发行）。翻转后的漫画与美国漫画相似，共32页，采用小册子形式，因为漫画原本打算分发给专业漫画商店，而漫画商店是美国主流漫画的主要发行渠道。 )   


-----------------------  
[The Dissemination of Japanese Manga in China](https://lup.lub.lu.se/student-papers/search/publication/1327615)    
日本漫画在中国的传播  
Yang Wang  

......  
In his book Visual Communication: Images with Message 36 , Lester intends to give ‘a method for analyzing visual messages regardless of the medium of presentation’ 37 . After thoroughly discussed the mechanism and the ethnic of what we see, he started to introduce and analysis the media through which we see by employ Six Perspectives for Analyzing Any Image.  
Lester在《视觉传播：带有信息的图像》一书中提供"一种分析视觉信息的方法，无论其表现形式如何"37。在充分讨论了我们所看到的机制和种族之后，他开始介绍和分析我们所看到的媒体，采用六个角度来分析任何图像。  
......  
According to Lester, the six perspectives are:  
Personal: a gut reaction to the work based on Subjective opinions.  
Historical: a determination of the importance of the work based on the medium’s time line.  
Technical: the relationship between light, the recording medium used to produce the work, and the presentation of the work.  
Ethical: the moral and ethical responsibilities that the producer, the subject, and the viewer have the work.  
Cultural: an analysis of the symbols used in the work that convey meaning within a particular society at a particular time.  
Critical: the issues that transcend a particular image and shape a reasoned personal reaction.  
根据Lester的说法，这六个角度是：  
个人：基于主观意见的对作品的直觉反应。  
历史：基于媒介的时间线对作品的重要性的判断。  
技术：光线、用于制作作品的记录媒介和作品的呈现之间的关系。  
伦理：制作者、拍摄对象和观看者对作品的道德和伦理责任。  
文化：对作品中使用的符号进行分析，这些符号在特定时期的特定社会中传递着意义。  
批判性：超越特定图像的问题，形成合理的个人反应。  
......  
Personal Perspective  
个人角度  
Different individuals must have diversified experiences and interests with their reading. But for many teenagers in the 1990s, reading manga can be an exciting adventure to approach taboo subjects, for instance, to understand the secrets of human body. After 1978 when China restored the education system, students began to go to school again. The old education system established in the 1950s have yet to be changed, in which ‘to promote and channel human resource for Socialism’ is the prime goal for education. Surrounded by such a solemn and depressive social atmosphere in which abstinent ethos was inherited from both traditional culture and command economy, issues on gender and sex were extremely sensitive within the society. Teachers and parents seldom talk about the sex issue with students and children as if this part of human being does not exist. Gender education has been generally ignored. Among the young generation born after the 80s, many had no idea about the opposite gender even until 18 years old. After two decades, however, sex is no more an ignorant topic. Sometimes it is even unbearable to see that the issue has been horribly abused on the Internet as well as in other mass media.  
不同的人在阅读方面肯定有不同的经验和兴趣。但对于90年代的许多青少年来说，阅读漫画可以是一次令人兴奋的冒险，可以接触禁忌的话题，例如，了解人体的秘密。1978年后，中国恢复了教育系统，学生们又开始上学了。1950年代建立的旧教育制度尚未改变，其中"促进和引导社会主义的人力资源"是教育的首要目标。在这种庄严压抑的社会氛围中，传统文化和指令性经济的禁欲精神被继承下来，性别和性的问题在社会上极为敏感。教师和家长很少与学生和儿童谈论性问题，仿佛人类的这一部分并不存在。性别教育被普遍忽视。在80年代以后出生的年轻一代中，许多人甚至在18岁之前都对异性没有概念。然而，20年后，性不再是一个无知的话题。有时候，看到这个问题在互联网以及其他大众媒体上被可怕地滥用，甚至让人无法忍受。  

Figure 4.1 Man and woman in 北条司’s manga City Hunter  
图4.1 北条司漫画城市猎人中的男子和女子  

In the early 90s, however, the knowledge of gender and sex is still scarce in the public resource. When a small unknown publishing company 40 had a bunch of prestigious Japanese manga book published in 1992, it soon had its name remembered by Chinese manga fans. Besides a whole new world of interesting stories in manga books, boys and girls in their growing up ages have, for the first time, encountered a heap of nude and curvaceous human body which they could never see from their education. Since aestheticism is one of the favorite qualities in Japanese culture, the portions of form and structure in manga books are usually exaggerated such as the big eyes, the flowing hair and long slim legs in order to intensify the beauty of human body. It is very likely that from the human bodies that were beautifully drawn in manga books, many boys and girls were enlightened with the beauty of gender in the early 90s. Figure 4.1 is one of such manga works going very popular at that time. The author had his women characters drawn in a very realistic way as if they are taken from photographs. Followed by the cognizance of gender, the desire for self-identity as being a human individual in opposed to collectivism emphasized in school is also emerging.  
然而，在90年代初，公共资源中关于性别和性的知识仍很少。当一个不知名的小出版公司在1992年有一堆著名的日本漫画书出版时，它的名字很快就被中国的漫画迷记住了。除了在漫画书中看到一个全新的有趣的故事世界外，处于成长阶段的男孩和女孩也第一次接触到他们在教育中从未见过的一堆裸体和曲线优美的人体。由于唯美主义是日本文化中最受欢迎的品质之一，漫画书中的形式和结构部分通常被夸大，如大眼睛、飘逸的头发和纤细的长腿，以加强人体的美感。很有可能，从漫画书中描绘的人体美中，许多男孩和女孩在90年代初得到了性别之美的启蒙。图4.1是当时非常流行的此类漫画作品之一。作者把他的女性角色画得非常逼真，就像从照片上拍摄的一样。随着对性别的认识，对作为人类个体的自我认同的渴望也开始出现，与学校强调的集体主义相反。  


-----------------------  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



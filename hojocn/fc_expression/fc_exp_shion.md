"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


# FC表情包  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96861))  

----------------------------------------------------------
--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  


- Wakanae Shion:  
    - admire     
![](./img/06_072_0__.jpg)  

    - adore  
![](./img/05_006_4__mod.jpg) 
![](./img/05_006_3__mod.jpg) 

    - apprec  
![](./img/06_134_2__.jpg) 

    - amuse  
![](./img/08_004_4__mod_shion.jpg)     
![](./img/06_118_6__.jpg) 
![](./img/07_051_3__.jpg) 
08_011_4__

    - anger  
![](./img/07_006_2__mod0.jpg)     
![](./img/07_006_2__mod1.jpg) 

    - anxiety  
              
    - awe  
![](./img/11_005_3__mod.jpg) 

    - awkward  
![](./img/07_069_0__.jpg) 
06_129_4__

    - bored  
![](./img/07_050_5__.jpg) 
![](./img/08_005_5__mod.jpg) 
09_021_1  

    - calm  
![](./img/08_004_1__mod_shion1.jpg) 
![](./img/08_004_1__mod_shion2.jpg) 
![](./img/08_004_1__mod_shion3.jpg) 
![](./img/08_004_1__mod_shion4.jpg) 
![](./img/08_004_1__mod_shion5.jpg) 
![](./img/08_004_1__mod_shion6.jpg) 
![](./img/08_004_1__mod_shion7.jpg) 

    - confused  
05_044_0__

    - crave  
              
    - disgust  
![](./img/06_133_6__.jpg) 

    - pain  
![](./img/06_118_2__.jpg) 

    - entranced  

    - excited  
![](./img/07_004_1__mod.jpg)     
![](./img/06_133_6__.jpg) 
![](./img/07_051_3__.jpg) 

    - fear/horror  

    - interest  
![](./img/06_133_4__.jpg) 

    - joy  
![](./img/06_115_7__.jpg) 
![](./img/06_133_6__.jpg) 
![](./img/07_066_3__.jpg) 
02_172_5__
07_043_0__

    - nostalgia  
![](./img/07_066_3__.jpg) 
09_036_4  

    - relief  

    - romance  

    - sad  

    - satisfy  
![](./img/07_066_3__.jpg) 
![](./img/11_006_1__mod.jpg) 

    - sexual  

    - surprise  
![](./img/11_005_3__mod.jpg)      
![](./img/11_006_3__mod.jpg) 
![](./img/07_006_3__mod.jpg)    
![](./img/11_006_6__mod.jpg)  
![](./img/07_004_1__mod.jpg)    
![](./img/06_143_4__.jpg) 
![](./img/07_064_4__.jpg) 
05_045_0__
11_036_4__

    - others  
![](./img/12_005_0__mod.jpg) 
![](./img/12_004_4__mod1.jpg) 
![](./img/12_004_4__mod2.jpg) 
![](./img/06_072_0__.jpg) 
![](./img/06_118_0__.jpg) 
![](./img/07_050_5__.jpg) 
![](./img/11_122_3__.jpg) 


- 11_149，若苗紫反对紫苑高中毕业旅行，紫苑对此一脸不屑：  
![](./img/11_149_0__cn.jpg)

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处




([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96832))  

[zz] Anime News Network上与北条司相关的新闻  

https://www.animenewsnetwork.com/encyclopedia/people.php?id=3546


-----------------------------------------------
**獠为什么不避开香的大锤**

source: https://www.animenewsnetwork.com/interest/2019-02-02/city-hunter-creator-answers-age-long-question-why-doesnt-ryo-just-dodge-kaori-hammer/.142842

**City Hunter Creator Answers Age-Long Question: 'Why Doesn't Ryō Just Dodge Kaori's Hammer?'**  
**城市猎人作者回答了一个长久以来的问题："为什么獠不避开香的大锤？"**

posted on 2019-02-02 15:45 EST by Kim Morrissy

In anticipation of the City Hunter: Shinjuku Private Eyes film's upcoming release in Japan, the March issue of Monthly Comic Zenon will publish 30 fan questions answered by the original City Hunter manga creator Tsukasa Hojo, one for each year since the manga's original publication.
为了期待《 City Hunter：新宿私家侦探》这部电影即将在日本上映，3月发行的月刊漫画Zenon将发布由City Hunter漫画的原创者北条司所解答的30个粉丝的问题，漫画发行以来30年所以一个问题对应一年。


Hojo's Twitter account released the answer to one of the questions ahead of time: "Why doesn't Ryō just dodge Kaori's hammer?" Hojo wrote: "It's not that he can't dodge it, but more like he won't. Perhaps he thinks that his actions deserve a punchline, or maybe somewhere in his heart he wants Kaori to stop him."
北条的Twitter帐户之前发布了一个问题的答案：“獠为什么不躲开香的锤子？”北条写道：“这并不是说他不能躲避，而更像是他不会去躲避。也许他认为自己的举动应该配上一个搞笑镜头，或者也许是在心里他想让香制止他。”

原twitter链接：
https://twitter.com/hojo_official/status/1088735964132864000


The tweet was first published on January 25, and has since garnered over 6,000 retweets. Judging by the responses, the answer didn't really surprise too many people. Despite his playboy tendencies, Ryō's feelings for Kaori were never in doubt.
该推文于1月25日首次发布，至今已获得6,000多个转发。从回答来看，答案并没有真的让太多人感到惊讶。尽管有花花公子的倾向，但獠对香的感受是毋庸置疑的。


Hojo also revealed what kind of alcohol Ryō drinks: he's apparently a big fan of Wild Turkey.
北条还透露了獠喝的酒：他显然是Wild Turkey的忠实粉丝。


City Hunter follows Ryō Saeba, a sniper and a private eye ("sweeper") based in Tokyo's Shinjuku Ward with unrivaled marksmanship and an over-the-top obsession for the opposite sex. He serves as a bodyguard and other duties for his clients. The manga inspired four television anime series and several anime and live-action films. City Hunter: Shinjuku Private Eyes will move the setting to present-day Shinjuku, and will open in Japan on February 8.
城市猎人讲的是东京新宿区的狙击手和私人侦探冴羽獠，以无与伦比的枪法和对异性的过度痴迷。他为客户担任保镖和其他职责。漫画衍生了四个电视动画系列以及几部动画和真人电影。 《城市猎人：新宿私家侦探》剧情设置为今天的新宿，并将于2月8日在日本上映。

-----------------------------------------------
**AH的官方英文版的相关报道(2015-12-13)：**  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96835))    

https://www.animenewsnetwork.com ... lable-online/.96454

Tsukasa Hojo's Angel Heart, Free English Version Available OnlineDec 13th 2015
                              The English version of “Angel Heart”, the follow-up to Tsukasa Hojo's legendary series “City Hunter”, will be readable for free online at “SMAC! Web Magazine”.


Kichijoji, Tokyo, JAPAN - December 11th, 2015

SMAC! Web Magazine has announced that they will be releasing an English version of Angel Heart by Tsukasa Hojo, a manga artist well known for works such as “Cat's Eye” and “City Hunter”. Angel Heart, a follow-up to the City Hunter series, began serialization 15 years ago, and is still running in the Monthly Comic Zenon magazine. This will be the first time that this work is made officially available in English.

SMAC! Web Magazine is the official website of SILENT MANGA AUDITION, the largest international manga award in the world. One section of this website, “ZENON International”, is dedicated to translating and publishing manga from the Japanese manga magazine Comic Zenon, based on the votes of its readers. Angel Heart is the most recent series to be selected. No registration is required to read manga, and everything is completely free.

Other works available on ZENON International include “Ikusa no Ko”, a manga by Tetsuo Hara, famous for his “Fist of the North Star” series, “Nobo & Her?” by Molico Ross, a past winner of the domestic version of Silent Manga Audition, and “Arte” by Kei Ohkubo, a story about a young girl aiming to become an artist.

The SMAC! Editorial Team, in co-operation with Monthly Comic Zenon, aims to release more titles in the near future. Public votes will be held on the website, and titles that receive many votes will be given top priority.

For more information, visit the official website “SMAC! Web magazine”
http://www.manga-audition.com










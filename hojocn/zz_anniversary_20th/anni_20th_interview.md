(摘自[《北条司漫画家20周年纪念》](https://www.maofly.com/manga/20700.html))  
[hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96931)  

## 与北条司的谈话(HOJO TSUKASA interview)  
([source](https://www.maofly.com/manga/20700/157209_83.html); 
images: 
[image83](https://mao.mhtupian.com/uploads/img/11295/114848/083.jpg), 
[image84](https://mao.mhtupian.com/uploads/img/11295/114848/084.jpg), 
[image85](https://mao.mhtupian.com/uploads/img/11295/114848/085.jpg), 
[image86](https://mao.mhtupian.com/uploads/img/11295/114848/086.jpg), 
[image87](https://mao.mhtupian.com/uploads/img/11295/114848/087.jpg), 
[image88](https://mao.mhtupian.com/uploads/img/11295/114848/088.jpg), 
[image89](https://mao.mhtupian.com/uploads/img/11295/114848/089.jpg), 
)

文字版：  

**20年的光阴转瞬即逝**  

----恭喜您进入漫画家生活的第20周年，此外，也祝您的连载《非常常庭》顺利完成。  

多谢，不过，已经有20年了吗？总觉得好像才不过一转眼的功夫......（笑）  


----回顾您这20年的创作生涯，我们有一个问题想要向您请教一下。您作为职业漫画家出道应该是在大学时代吧？那么在这之前您进行过什么样的漫画活动呢？  

最开始画有情节的漫画的时候，我好像还只是个小学1年级的学生吧？当时非常流行一个叫做《超人Q》，采用了大量特技摄影的节目。我每天在学校时都热衷于和朋友们讨论里而的各种怪兽。结果说着说着，我一时高兴就随口说，“其实有个叫《超人x》的漫画也很厉害呢，我下次临摹给你们看！”当然了，这个漫画在实际中并不存在。因为没办法改口，我只好自己在本子上用铅笔画了个故事拿到了学校去心这应该就算是我的第一次漫画创作吧？（笑）  


----您有没有什么因为漫画而交到的朋友呢？  

有啊。小学的时候，我们还只是把彼此的涂鸦一样的铅笔画互相展示给对方看而已，等到了初中以后，我的朋友里而有个人始相当认真地去画漫画。我看到他那个样子觉得很有意思，于是自己也开始在本子上涂涂画画。到了高中的时候，这个朋友原本和我分别进了不同的地方。结果没想到他竟然来找我说，“我组织了一个‘漫画俱乐部’，你要不要参加？”，正好我当时也闲着没，所以就加人了进去。我们那个供乐部一共有6个成员，还男2女。然后这6个人分别拿来自己喜欢的角色一起创作共同的漫画。那种共同创作真的很好玩呢。因为大家的口味都不一样，所以有人拿来的角色象老式连环画里的人物，也有人的角色象通少女漫画的主人公（笑），结果这些乱七八槽、互不搭调的角色全都要挤到同一画面里，一起来完成一个故事，结果你也可想而知啦（笑)。这种创作不但让我温习了人物的描绘，而且也第一次让我尝到了正式构思情节，用钢笔把故事画出来的感觉。所以在从高二到大一的期间，我一直都有持续画这样的故事。高中二，三年级的时候，我好像只画过两个故事呢，这两个全都是科幻感觉的东西。不过，那时候我还完全没有想过投稿啦，或是做职业漫画家之类的事情。  


**为了拿奖金而获得了手冢赏的准入选**  

一一听说您在大学的时候，还参加过电影和动画的制作是吗？  

是有做过，那可相当要命呢（笑)。当时我明明是想从事影像或电影美术之类的工作啊，为什么最后却成了漫画专业户呢（笑）？不过，我大学里的学长有制作包括诗、插画和漫画在内的同人志，我因为也加人在了里面，所以当时多多少少也画了些漫画。  


----这就是您出道的契机吗？  

那倒不是。我出道是在大学3年级的时候，那时我有个和同人志完全无关的朋友说他把作品寄去参加“手冢奖”，我听说之后心想，“既然能有钱拿的话，我也去寄一个试试吧。“。我当时真的是抱着很无所谓的心态，结果没想到居然一下子就能获得准人选。当20万的奖金拿到手里的时候，我可真是吓了一大跳呢。  


----那部作品就是《宇宙天使》吧？  

老实说，当时我和同一研究室的同伴有制作一个未完成的动画，我这个作品的灵感就是来源自那里面的角色。那年的大学暑假，我因为还要兼职帮别人画设计图，所以是以每天一页的速度，花了一个月左右的时间完成了这部作品。而且当时根本没有什么框架，完全是画到哪里算哪里。  


----您在获得准入选的通知后又怎么样了呢？  

那是在我大学3年级的12月吧。少年跳跃通知我说，“这边要举行手冢赏的颁奖仪式，你到东京来吧。”，因为他们说旅费由他们出，所以我就想，“那就去吧。”（笑)。结果到了那边一看，居然有个那么大的宴会，甚至还跑出来一个自称“责编”的人，说什么，“让我们以新的漫画为目标吧！”后来过了年之后，这位责编又为了和我商量画短篇的事情而来到了九州。  

即使如此，当时我本人还是没有想过自己要做个职业的漫画家。因为当时正为大学的毕业设计而头疼，所以倒是有	
想过“就算找不到工作，至少暂时还可以凭漫画混口饭吃。”。那时我的心情就是这么简单而已。  


----不过，《猫眼三姐妹》发表后，您立刻就成为了连载作家呢。  

在我正式进入毕业制作之前，曾经画过3个短篇故事。其中有2个被杂志刊登了出来。然后过了一段时间，就在我	
的毕业制作也有了眉目，可以松一口气的时候，突然就来了个电报。哦，对了，当时我还没有电话，所以要联络只能用电报（笑)。那个电报就是催促我构思情节。  

正好我当时在和朋友为了庆祝毕业制作完成而去喝酒时聊到了一些有趣的情节，于是我就活用那个灵感，创作出了《猫眼三姐妹》的原型。没想到的是，这个作品在杂志上刊登之后反应居然特别好，于是责编就叫我继续画续篇，我因此在那之后又照他的吩咐画了两个故事。结果那个责编也没有告诉我一声，就把《猫眼三姐妹》拿到了迕载会议上去讨论。结果就是，“我们已经决定把这个故事转为连载，房子也给你找好了，到东京来吧。”（笑)。那时正好是我大学毕业那年的夏天。  


**画漫画的手法是在《猫眼三姐妹》的连载中学到的**  


----对于初次连载，您是干劲十足呢？还是紧张不安？  

我来到东京以后，8月开始作画，9月起开始在杂志连载......“不过因为一直抓不住感觉，而画技又差劲。所以我当时一直在想，“反正象我这种大外行画的漫画一定很快就会玩完心”（笑)。不过居然还是继续了下去。到了这种时候，我开始觉得自己再这么混下去可不行，得好好地认真干了。不过我越是认真画，感觉到的障碍就越多。比如画人物的话要描绘出各种角度的不同，街景，背景、服装、装饰之类的也尽是以洫从没有画过的东西。总之是吃尽了苦头。  

原本我的画风是连环画的惑觉，现在一下子要改成漫画的风格，也着实让我头疼了不少。《描眼三姐妹》的连载本身对我来说就是一个练习的过程。我就是通过这个连我，而将漫画的各种技法一个个地学到了手里。不过呢，没有时间啦、忙到昏天黑地啦，睡不了觉啦......周刊的连载可真的够辛呢。  


----大概是从什么时候起，您能从画《猫眼三姐妹》中找到了乐趣呢？  

大溉是在开始连载那年的年末。因为已经抓住了感觉，所以开始觉得有意思了吧？或者该说是在糊里糊涂开始连载后的一年左右的时间。


----大约是1982年左右吧。少年跳跃本身因为江口寿史的活跃和鸟山明的加入，转入了活用设计灵感的新时代。是这样吗？  

你说的没错。我自己在最开始的时候，也光是为了建立自己漫画的形态就已经拼了老命。但后来不知道是不是感觉到了游刃有余，所以我画着画着开始觉得，说不定画漫画比拍电影还要有趣呢。你想，从编剧、导演、美术，乃至于主角，不全都能完全按照自己的意思决定吗？我就是感觉到了这种乐趣。但是呢，问题当然还是有的。因为我对影像的向往过于强烈，所以我无论如何还是想让我的作品成为“动起来的”有趣的漫画。  


----所谓的“动起来”，就是指类似于三姐妹从大厦上飞身而下之类的感觉吗？  

没错没错〈笑）。这和《城市猎人》也有相通的地方。  


**我从来没打算做一个动作漫画作家**  


----《城市猎人》是因为获得了读者喜爱奖，才从短篇发展为了连载吧？  

对，不过我也不是就真的那么青睐动作戏。只是因为《猫眼三姐妹》都是女性唱主角，所以想这次也该画个男主人公了，于是就创作出了寒羽良这个角色。然后呢，我还想将1性感 2爽朗 3可爱这三个在《猫眼三姐妹》中分别分散在三姐妹身上的特质集中到同一个女孩子（香）的身上。说老实话，因为将三姐妹的性格特色划分得过于清楚，有时候真的让我觉得非常的拘束哦（笑)。  


----您是说《猫眼三姐妹》和《城市猎人》并不是动作戏吗？  

我自己可从来没打算把它们画成动作戏的（笑)。虽然也经常有人说，“北条司的招牌戏就是动作和美女”，但《猫眼三姐妹》也好，《城市猎人》也好，他们会有这样的感觉是不是还是受动画的影响比较多啊？因为是在《少年跳跃》这样的少年杂志上画东西，所以我倒是确实有想过“动作的感觉多一些比较好吧“。但是，就拿《城市猎人》来说好了，在它里面，重要的绝对不是所发生的事件，而是男女之间心意的交流。问题的中心在于人类的心，而所发生的事件只是解决这一问题的线索而已。所以呢，我本人其实并不怎么喜欢推理小说之类的东西。  

不管多么曲折复杂的故事，我看完之后，也就只有“哦，原来这样啊。”的感觉。我本身的感性，也在更傾向于恋爱啦，人情世故之类的部分。大家不也都知道吗，自从《城市猎人》之后，我的作品可都是老实规矩的故事哦（笑)。  


----您指的是《天使的礼物》、《樱花盛开时》、《阳光少女》......以及在《城市猎人》连载时穿插发表的短篇系列吧？  

说心里话，在《城市猎人》连载完毕的时候，我的精神体力都已经接近界限。无论是在工作上还是人际关系上都遇到了瓶颈，而且对作家和编辑间的关系惑到了疲劳。结果这种精神压力反而成为了我创作短篇的动力。这其中《阳光少女》的诞生，主要原因之一就是想彻底排除美女和动作（笑）。  

因为我心里也有怀疑过，“孩了们真的就想看这种东西吗？”......结果就是，“能和树木对话的少女”的产生。说实话，在我画《城市猎人》的时候，就已经开始为自然的东西而倾倒，对花呀、树呀之类的东西常之着迷。那时的我拼命地收集自然界的写真集，还跑到高原地区去旅行。《城市猎人》舞台不是新宿吗？也许是因为这么多年都尽画些新宿的街道啦、人厦啦之类杀风景的东西，而产生出的抵触心理在作怪吧。不过，可惜这部作品并不受读者欢迎《阳光少女》的第一回在以者调查中排第4位。第4位可是不行的，这算是我常年在《少年跳跃》连载所积累下来的直觉吧。也就是说．第3位就已经是分界线了。不过这些我当时也并不在乎，因为原本画这个作品的时候就知道它不会长命了。	


**在周刊连载中耗费10年，令我想描绘自然**  


----全彩的短篇《SPLASH！》是在《超级跳跃》上连载的吧？对于这部作品您有什么想法吗？  

它在《超级跳跃》上倒是始终都是最受欢迎的第一位，算是没有白费力气。不过当时我正忙于《城市猎人》的连载，现实情况也不容许我就那样直接转到青年杂志上进行长期创作。不过从技术上而来说的话，这种全彩的感觉在后来的《幸福的人》上倒是被继承了下来。  


----在那之后，您好像是为了恢复风格一样而开始连载《假小子勇希》对吗？  

《阳光少女》的时候，各位编辑大人容忍了我的任性。而且正好当时一直照顾我的责编也升为了主编，我心想至少要稍微回报人家一下吧，所以就留在了《少年跳跃》。不过，这部完全遵循责编意向的作品，在勉勉强强画着的途中就开始觉得不行了。其实我在连载开始之前就想过，“照这个样子的话这个连载长不了的”。  


----您认为《假小子勇希》失败的地方在哪里？  
主人公勇希其实有精神坚强这一点就足够了，没有必要还去设计地会拳法之类的武力部分。她都已经这样了，还安排她接受监狱里的犯罪专家的指点去拯救社会，这不就成了单纯的暴力女超人了吗？还有她那个师傅新田的治疗法，我可不觉得那种东西就能让社会变好。自从这个人物出现后我就觉得这个作品完了。  


----此外，我们还向请教一下关于战争系列的部分。《晴空的尽头--少年们的战场》、《少年们的夏天‘Melody Jenny》和《American Dream》,不管是这三个中的哪一个，感觉上您都画得非常顺畅啊。  

我从很久以前起就一直想尝试一下战争这个主題。因此在《假小子勇希》的连载结束的那年新年去和总编喝酒的时候，就谈到了要做战后50年企画这个话题。实际上读者的反应还算是过得去。三个故事中，只有《少年们的夏天》是我自己的原创，其他两个都有原作在先。我以前基本上没怎么尝试过有原作的作品，所以最开始的时候迟迟不能进人状况。不过，最后真正画起来的时候，不管是哪个都画得相当顺畅，算是相当愉快的经验吧。对了，《American Dream》还是我唯一的棒球漫画呢（笑)。等战争系列结束才后，我就提出了《非常家庭》的构思。


**从少年杂志到青年杂志，依然不能摆脱约束**  


----那么说您最初还是打算在《少年跳跃》上连载《非常家庭》啦。  

不过在编辑会议上就被彻底驳回了哦。什么绝对不能画人妖啦，这是变态漫画之类的，好像被说的挺难听的样子。就算当时在《少年跳跃》上通过的话，画出来大概也不是现在这个样子了。  

就在这时，《ALLMAN》这本新杂志创刊了。它应该还算是没有染上任何色彩的杂志吧。虽然我还不敢说，“我来为它定出色调来！”，但不能否认，它的出现确实让我跃跃欲试。因此，我就转去了《ALLMAN》。  

不过，就算是青年杂志，也还是有它的限制啊。首先，就不能涉及到差别对待的描写。若苗家在周围人看起来，应当是非常异端的家庭吧？可是呢，这里却存在超越普通家庭的温暖交流。这就是它的核心所在。在《非常家庭》里，对于若苗家抱有偏见的邻居，也就是所谓的世俗的眼光，完全都没有出现。这就是因为对于这些描写是有限制的。但是，作为一个故事来说，我觉得这一点非常之不自然。就算是转到了青年杂志，好像和少年杂志的差别也不是很大的样子。至少我觉得和在《少年跳跃》的时候相比就没有太大变化。  


----在《非常家庭》中，您自己觉得最挑战的部分是什么地方？  

归根结底的话，应该还是人与人之间的交流，我想画的其实就是家人的关系和持殊环境中的家庭而已。还有，因为无法影像化（笑），也不能改成小说，因为缺乏动作，改编成动画或是电视剧也一定没有意思。所以，这部作品绝对只有用漫画才能表现出来。这就是《非常家庭》吧。不过，在角色设定上我也有失败的地方。


---失败的地方？  

紫虽然是人妖，但内心却是女人，这一点我就没能充分地表现出来。从杂志的角度出发，会选择紫苑而不是紫作为主角也是没有办法的事情。但是呢，对于紫苑这个人物，照我原来的设定，应该是个没有胸部的女孩，而且以男孩子的形象登场的。结果最后变成了又丰满又性感的美女......我之所以后来让“薰”这个角色出场，其实就是为了一洗这口怨气。我原本所设想的紫苑应该就是薰那个样子。还有，我原本也想好好描写一下那些助手们为什么都是人妖的，结果到了这种时候，编辑的反应就非常不情愿。  


----在《非常家庭》中有很多非常有个性的角色，请问您自己印象最深的是哪个角色？  

最让我觉得不可思议的是雅彦。为什么他会那么内向呢？再让他象样一点不也挺好吗？其实在最初的预告中，他的形象明明比现在要不良得多。可现在那种感觉已经半点也不剩了（笑)。说实话，在和紫苑的对比中，他究竟会被改变到什么程度，我一直都掌握不好分寸。好像直到最后一回我都没能切实抓住那种感觉。  


----在最终回里，雅彦成长了一些吗？  

一点点面已（笑)。不过因为紫苑的性别也始终没有交待清楚，所以难免给人突然结束的印象。  


----对于这些部分，您有创作续篇的打算吗？  
目前我还不是很清楚。比起这个来，我现在更想多投注些精力在新的工作上面。


**从现在起，新的北条司即将诞生**  


----您说的新工作是指......  

这件事情我从去年年底就已经开始筹备了我打算创建一个聚集热爱漫画的人，只为了漫画而存在的新公司"CORE MIXED”。我们将在这个新公司里所企画的作品提交给出版社，然后将赚到的钱全部用于漫画事业。我就是想创立一个这种宗旨的公司。原本这个计划应该还不是那么着急，但是正好出版社和新杂志创刊的事情碰到了一起，所以我们的活动突然就得正式运作起来了。因此，虽然很对不住《非常家庭》的读者，但是由于实际生活上的原因，我还是不能不进人新作品的创作阶段。


----您打算在新杂志上刊登新连载吗？  

没错，这本杂志预定期年5月创刊。因为是好久没有从事过的周刊连载，所以我自己多少也有些不安，不知道身体还撑不撑得下去。不过我相信还是不会让各位读者失望的。关于新连载的内容，现在在网络上据说已经是谣言满天飞了，但是具体要画什么现在还是秘密。不过我至少可以透露一点，“不会是《城市猎人》的！”（笑）。今后，我希望自己不只是作为漫画家，同时也作为"CORE MIXED”的董事之一，和编辑上的顾问，继续从事我所喜爱的漫画事业。


----对于您已经跨进20周年的漫画事业，这将又是一个新的挑战吧？  

原本我之所以进人漫画这个世界，主要是出于偶然，所以也没有抱有什么太大的抱负和理想。然后，我也没有经过太多的磨练，就掌握了工作的做法，多多少少达到了一定的成就，多多少少耗费了一定的体力。曾经有一个时期，我也想过就这样马马虎虎的也可以了。不过，我还是感到了憋闷......所谓的马马虎虎就可以，其实只是自己为自己找的借口而已。当我发觉到这一点的时候，就构思出了"CORE MIXED”的计划。现在最令我心动的，与其说是自己的作品，还不如说是新人的育成。我目前最大的乐趣就是，如何才能培育成可以成为未来漫画界接班人的人才。不知为什么，做了20年的漫画家，我好像还是第一次尝到了积极主动出击的感觉。	








**Links:**  

- https://ocr.space/  
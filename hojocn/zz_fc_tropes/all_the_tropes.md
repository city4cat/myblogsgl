
source:  
https://allthetropes.fandom.com/wiki/Family_Compo?oldid=373262

## Family Compo 里的转义词语（allthetropes版） ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96825))   

Revision as of 02:23, 22 December 2019 by GlitteringFlowers  
2019年12月22日02:23 修订，GlitteringFlowers

![](https://vignette.wikia.nocookie.net/allthetropes/images/9/94/201807070015444897.jpg)


Ordinary college student Masahiko goes to live with his aunt and uncle, Yukari and Sora Wakanae after the deasth of his mom. While excited about meeting his relatives for the first time, he is surprised to discover that his lovely aunt Yukari was born as a man, and his handsome uncle Sora is really female-bodied. Their plucky daughter, Shion, is revealed to be... whichever gender she feels like (Though for the time being she's sticking with female).  
普通大学生雅彦在母亲去世后与舅母若苗紫和舅舅若苗空住在一起。 当他很高兴第一次见到他的亲戚时，他惊讶地发现自己可爱的舅母紫实际上是一个男人，而他英俊的舅舅空则是女人。他们勇敢的女儿紫苑被揭露是……她所喜欢的性别（尽管暂时仍在坚持做女生）。

However, given Masahiko's current fate: living alone in a fetid apartment that he can't afford and with no family to return to: he decides to stay anyways. Eventually, Masahiko's desire for a real home coincide with with the Wakanaes' surprising generosity to anyone who is willing to accept their family at face value, and he quickly becomes Happily Adopted into the household.  
然而，考虑到雅彦的当前命运：独自生活在一个他负担不起的令人厌恶的公寓里，无家可归。他仍然决定留下。 最终，雅彦渴望一个真正的家，这与若苗家出人意料的对任何人的慷慨相吻合--只要这人愿意接受他们家庭，然后雅彦很快就成了幸福的被领养者。

By the mangaka Tsukasa Hojo, Family Compo is a Slice of Life manga that, perhaps, has a more realistic than average depiction of transsexuality, and at the same time has a remarkably positive tone with tremendous emotional poignancy.  
由漫画家北条司创作的《非常家庭》是一部生活片段漫画，或许，该作品比变性人的一般描写更真实。同时又具有非常积极的基调和极大的情感上的刺痛。

Available to read here.  
可在此处阅读。

The entire manga, albeit in Chinese, is available here. Click on the chapter you want and use Google Translate to find the “read” link if you don’t want to download.   
完整的漫画（尽管是中文版）可在此处找到。单击您想要看的章节，如果您不想下载，请使用Google翻译查找“read”链接。

# Contains examples of: 

- Ambiguously Gay: A bunch of young hooligans question the sexuality of one of their members when he says he wants to have sex with Kaoru.  
含糊不清的同性恋：一群年轻的流氓质疑他们的一个成员的性取向，当他说他想和薰发生性关系。

- Arranged Marriage: Yoriko. It works out for her, though. She gets together with Kenji.  
包办婚姻：顺子。 最终还是搞定了，她和宪司在一起了

- Attractive Bent Gender: Sora and Yukari Wakanae, Shion, Masahiko, arguably Kaoru
有吸引力的变弯：若苗空和若苗紫，紫苑，雅彦，大概还有熏

    - Ironically, Sora and Yukari (well, definitely Sora) look terrible when they dress as their birth sex.  
     讽刺的是，空和紫（好吧，主要是空）当他们按天生的性别打扮的话，看起来很糟糕。

- Author Avatar: Not the author, but the character Sora Wakanae has Sora Harukaze, who's idolized by people like Yoko. And Harukaze isn't secretly a woman.  
作者替身/笔名：不是作者，而是若苗空这个角色有笔名春风空，这是叶子等人的偶像。而且春风并不是私下里的女人。

- Beauty, Brains, and Brawn: All three in Shion.  
美丽、智力、体力：紫苑集这三项于一身

- Betty and Veronica: Asoko and Shion, respectively, with Masahiko as Archie  
Betty和Veronica：分别对应浅葱和紫苑。雅彦对应Archie。  
（注： Betty和Veronica是小说《Betty和Veronica》里的人物，两女生是好朋友，友谊经受了各种考验。Veronica曾亲吻过 Archie。[参考链接 - Betty and Veronica](https://riverdale.fandom.com/wiki/Betty_and_Veronica)）


- Berserk Button: Don't screw up Sora's work. And imply that he's a woman. And, in turn, talk about the beach.  
狂暴按钮: 别把空的工作搞砸了。不要暗示他是个女人。还有，不要谈论海滩。

- Bifauxnen: Kaoru  
男装大佬： 熏

    - Almost Sora, but he doesn't exactly come off as a Bishonen.  
    基本上就是空。但他扮女装大佬不太成功

    - Shion too, assuming she really ISN'T a boy.  
    紫苑也是(男装大佬)，假设她真的不是男生

- Cast Full Of Crossdressers: Everyone wears outfits of both genders at one point or another.  
到处都是异装：特别是异装。每个人都在此时或彼时穿着男性和女性的服装。

    - Except Kaoru, and Tatsumi, and Saki for that matter.  
    这件事上，熏、辰巳、早纪例外。

- Bitch in Sheep's Clothing: Saki  
披着羊皮的贱人：早纪

- Black Comedy Rape: The film club gets raped by the transvestites at a bath.  
黑色喜剧爆菊：影研社的人在澡堂里被变性者爆菊（注：应该是温泉旅馆里的澡堂）

- Boobs of Steel: Sora, a muscular guy who gets into fist-fights and knows Kendo, hasn't had his substantial breasts surgically removed. As a result, he cannot go to the beach, much to his chagrin.  
钢铁之胸。空，一个强壮的家伙，他习惯了用拳头打架，并懂剑道。没有实质性的乳房切除手术。因此，他不能去海边，为此他很懊恼。

    - It's a mystery how he managed to grow so tall and muscular, and his feet so big, without the aid of hormones. He was manly even in high school.    
      这是个谜，他是如何在没有激素的帮助下，成功地长出这么高的个子和肌肉，以及这么大的脚。他在高中的时候已经很有男人味了。

    - Also true for Yukari, who isn't a strong as Sora, but stronger than she looks, because she's a man.   
      紫也是如此，她虽然不像空那样强壮，但她比外表更强壮，因为她是一个男人。

- Buxom Is Better: Shion, who has breasts too big to be a transvestite, that is, if they are natural.  
丰满更好：紫苑，胸部挺拔以至于不太可能是异装，如果它们是真的话。

    - Must get it from her, er, father, heh.   
      一定是继承自她的...爸爸的基因...嗯

- Calling the Old Man Out: Sora to his father, Kaoru to Tatsumi. Neither are biologically men.  
挑战父亲：空对他的父亲；熏对辰巳。都不是天生的男性。

- Can't Hold His Liquor: Sora passes out after a single shot. Would make sense in the context of his sex, but it's probably just because he's Sora.  
酒量不行。空喝一杯酒就倒，这在他的性别背景下说得通，但也可能只是因为他是空。

    - And Shion, if she's a he.   
      紫苑也是，如果她是男生的话

- Don't You Dare Pity Me!: In volume 5, when Sora has appendicits, he insists that his period isn't an excuse to finish his work late. Even when he collapses and is dragged to the hospital, he resists seeing a gynaecologist because he's a man.  
你敢可怜我吗!：在第五卷中，当空得阑尾炎时，他坚持认为他的月经并不是拖延工作的借口。即使他倒下了、被拖到医院，他也因为自己是个男人而拒绝看妇科医生。

- Double Standard: Sora seems to get a lot more attention than Yusaki. Sort of makes sense, because Yusaki’s more feminine and quieter.  
双重标准：空似乎比紫更引人注目。这有点道理，因为紫更女性化、更安静。

- Dragged Into Drag: Masahiko, initially for the Film Club's movie... then it becomes a theme.  
拖累：雅彦最初为演影研社的电影... 然后这成为一个主题。

- Drives Like Crazy: Much like another Yukari, Yukari Wakanae. She hasn't been behind the wheel in several years  
飙车：更像是另一个紫，若苗紫。她已经很多年没开车了。

- Dropped a Bridget On Him: Yukari and Sora's assistants on Masahiko.  
女装大佬被揭穿时的尴尬：对于雅彦来说，这种尴尬发对应于紫和空的助手们。  
（注： [Dropped a Bridget On Him 的解释](https://tvtropes.org/pmwiki/pmwiki.php/Main/DroppedABridgetOnHim)）

- Face Fault: They happen frequently.
惊掉下巴：经常出现这样的镜头

- Flanderization: Tatsumi becomes a less and less serious character, despite attempting to rape and enslave "Masami".  
Flanders化: Tatsumi变得越来越不严厉，尽管试图OOXX并奴役雅美。  
（注：[Flanderization](https://tvtropes.org/pmwiki/pmwiki.php/Main/Flanderization)）

- Girl-On-Girl Is Hot: Kaoru attempts to rape Shion, but Shion breaks out of Kaoru's hold and starts molesting her. And, yeah.  
热火的女女戏：薰试图OOXX紫苑，但紫苑挣脱了，并开始反制熏。 然后，耶。

   - Kaoru was getting into it too. She could have broken out earlier, she only chose to when somebody came in. Especially when you consider how strong she can punch.  
   熏有点开始享受其中了。他本可以早点挣脱的，可只是后来有人进来的时候他才这么做了。特别是，你想想他一拳头也很大力气的。

       - Then again, Shion's got a strong throwing arm and hitting arm (for a girl....), so she's probably technically more skilled, and generally smarter. Also she might be a guy.  
       话又说回来，紫苑的手臂在投掷和击球时都很强（对于女孩来说...），所以她可能在技术上更熟练，而且通常更聪明。 当然，她也可能是一个男人。

- Gratuitous French  
无缘无故的法语

- Guy-On-Guy Is Hot: Averted by Asoaka, who is disgusted when she thinks she sees Masahiko and Tatsuya kissing.  
火热的男男戏：当浅冈认为她看到雅彦和卓也(江岛)接吻时，她很反感，并背过脸去。

- Good Parents: Despite how freaked out Masahiko is initially by his new parents, they turn out to be positive role models who do their best to treat himself and Shion with the love and respect they deserve.    
好父母。尽管雅彦最初被他的新父母吓坏了，但他们是积极的榜样。对于雅彦和紫苑，空和紫尽力用两个孩子所需要的爱和尊重去对待他们。

- Happily Married: Sora and Yukari  
幸福婚姻：空和紫

- Have I Mentioned That I Am Heterosexual Today: Masahiko primarily, but most of the characters invoke this trope at one point or another, which is odd considering the manga's focus on transsexuality and alternative gender expression. It's justified at least with Sora and Yukari, who are clearly monogamous and straight, but are far more comfortable with switched gender roles.  
我有没有提到我今天是异性恋。主要是雅彦，而不是本文这里和那里所提到的大多数角色。考虑到漫画的重点是在变性和其他性别表达方式，所以这就显得很奇怪。至少在空和紫身上是合理的，他们显然是一夫一妻制、直男直女，只不过调换性别后这么认为才合适。

- Insistent Terminology: Sora is a man. Sora is Shion's father.  
顽固的术语  

    - However, Yukari recognizes that no matter what she does, she is still biologically a man. Which is why she gets so emotional over having a child.  
      然而，紫认识到无论她做什么，她仍然是生物学上的男性。这也是为什么她会因为要孩子而变得如此情绪化。

- Insufferable Genius: Shion is very smart (and hot, as a girl), but at times will create messes out of fun, especially for Masahiko, and will say she doesn't feel responsible.  
令人难以忍受的天才：紫苑非常聪明（作为一个女孩，也很性感），但有时会因为好玩而恶作剧，尤其是对雅彦，而且她说她概不负责。

- I Want to Be a Real Man: Kaoru.  
我想成为男子汉：熏

- Jerkass: Kaoru and Shion have their moments.  
混蛋：薰和紫苑有他们的时刻。

- Jerk with a Heart of Gold: Kaoru only acts tough a lot of the time. See how she reacts to finding out her Tatsumi really is her father.  
混蛋与金子般的心：薰很多时候行为强硬。当她得知辰巳真的是她的父亲后，你看她的反应。

- Karaoke Bar: At one is where Masahiko learns the family secret, since he forgot it due to being drunk.  
卡拉Ok酒吧：是雅彦得知若苗家秘密的地方，因为他之前喝醉而忘记了。

- Kissing Cousins: Masahiko and Shion. However multiple times Masahiko has mentioned he wanted them to be Like Brother and Sister, he's constantly referred to as Shion's brother, and Shion's parents do act as his parents.. So it seems like adopted Brother-Sister Incest.  
表兄妹之吻。雅彦和紫苑。然而雅彦多次提到他希望他们像兄妹一样，他一直被称为紫苑的哥哥，而紫苑的父母也确实在言行上像是他的父母一样......。所以好像是养兄妹乱伦。

- Lethal Chef: Sora, and apparently even more so Shion.  
致命的厨师:空。显然紫苑更是如此.

- Love-Obstructing Parents: Sora and Yukari play this role for Masahiko, at times  
阻碍爱情发展的父母：空和紫有时为雅彦扮演这个角色。  
（注：[Love-Obstructing Parents](https://tvtropes.org/pmwiki/pmwiki.php/Main/LoveObstructingParents)）

- Manipulative Bitch: Saki.  
操纵型的贱人：早纪

- Masculine Girl, Feminine Boy: Sora and Yukari invert this trope, but for practical reasons since Yukari is the guy but far better at being a domestic than Sora on her best day, and Sora is rather adept at being a bread winner and father figure despite being a woman.  
男性化的女孩,女性化的男孩:空和紫反转了这个比喻, 但由于实际上，因为虽然紫是男的，但在家务事上远好于空；空虽然是女的，但在赚钱养家和父亲这个角色上做的很好。

- Mister Seahorse: Sora sees himself as a man, but is technically Shion's mother. He initially refused, because men don’t give birth (and also a thing about his job), but agreed when he saw how saddened Yukari was by her inability to conceive. His pride is greatly damaged when he's reminded of this.  
海马先生：空视自己为男人，但从技术上讲是紫苑的生物学母亲。起初他拒绝，因为男人不生孩子（还有关于他的工作的事情），但是当他看到紫由于生物学上的原因无法生育而痛苦时，他同意了。虽然他最终很高兴能生育一个孩子，但当他想起这个时，他的自尊心就大大受损了。

- Momma's Boy: Masahiko. Definitely justified.  
听话的孩子：雅彦，绝对没错。

- Muscles Are Meaningless: Shion seems to be abnormally strong for herself. Or maybe she's just that technically skilled and athletic.  
肌肉毫无意义：紫苑本身似乎身体素质异常的好。也许她只是技术娴熟和运动能力强。

    - Same goes for Yusaki, even if she is really a man.  
    紫也是如此，即使她真的是一个男人。

- Moment Killer: Too many to count  
煞风景：出现次数不计其数

- Ordinary High School Student: Masahiko, though technically it's college.  
普通高校学生：雅彦，虽然实际上是大学

- Overly Long Gag: Shion's Ambiguous Gender.  
过长的口水战： 紫苑模棱两可的性别

    - Not really. In Volume 10, Yukari refers to her as a "she", just after she's born. I doubt that they of all people would force a baby boy to be assigned to a different gender, I guess it's possible since they're a reverse couple.  
    不见得。在第10卷中，紫在她出生后将她称为"她"。 我觉得不可能他们会强迫给男婴不同的性别；我觉得有可能，因为他们是一个反转夫妇。

        - It may just be a translation convention. It's possible in Japanese to speak about someone in the third person without ever revealing their gender. The scanlaters may have just found it too awkward in English and just defaulted it to her chosen gender.   
        这可能只是一个翻译惯例。 在日语中，可以在没有透露性别的情况下以第三人称谈论某人。 译者可能发现它在英语中太尴尬了，然后就将其默认为她选择的性别。

- Pettanko: Masahiko notes that Kaoru has small breasts.  
飞机场/平胸：雅彦说薰的胸部有点小。  
（注：[Pettanko](https://tvtropes.org/pmwiki/pmwiki.php/UsefulNotes/Pettanko) )

- Psycho Lesbian: We get some of that from Shion in volume 8.  
心理上的女同：从紫苑在第8卷里的表现，我们能猜测出一些。

- Rated "M" for Manly: Sora, in a more toned down version. There are several moments where either the author or Masahiko asks if Sora really is a woman.  
评为 "M "的男人味。一个更低调的版本的空。有好几次作者或雅彦问空是不是真的是女人。

- Rebellious Spirit: Shion, and even more so Kaoru.  
叛逆的精神：紫苑，薰更是如此。

- Recursive Crossdressing: Masahiko asks Sora and Yukari to come to his college entrance ceremony dressed as "what they really are". He swiftly regrets this, as they look so awkward that everyone assumes they're crossdressers.   
递进式变装。雅彦要求空和紫来参加他的大学入学典礼 按"他们真正的性别"着装。他很快就后悔了，因为他们看起来很尴尬，大家都认为他们是异装者。

- Schoolgirl Lesbians: They're college age, but Masahiko Masami picks up a fangirl in addition to all the drooling guys. Naturally, she thinks Masami crossdresses as a man.  
女学生女同性恋者：他们已经上大学了，但是雅美除了被人意淫外还有一个女粉丝。 自然，她认为雅美异装为男人。

- Sequential Artist: Sora Wakanae  
连载漫画家：若苗空

- Smoking Is Cool: Sora, Kaoru, Tatsumi sort of Susumu, and Saki.  
吸烟很酷：空，熏，辰巳，横田有一点酷，早纪

- Spit Take: Sora and his father do spit takes.  
吐槽：空和父亲会吐槽。

- Stupid Sexy Flanders: Normally caused by Masahiko or Yukari. And possibly Shion.  
愚蠢性感的Flanders。通常是由雅彦或紫引起的。也可能是紫苑。

- The Mind Is a Plaything of the Body: Totally averted/ inverted (The Body Is A Plaything Of The Mind). Shion's neck even becomes manlier just from dressing as a man.  
心灵是身体的玩物：完全避开/反转（身体是心灵的玩物）。紫苑的脖子甚至因为穿成男装而更男性化了。

- Transsexual: Sora and Yukari Wakanae, Kaoru "half-assedly", and Sora's assistants: Susumu (no "lance and balls"), Hiromi ( Shion's first love no operation, has long blond hair), Kazuko (Judo champion), and Mako (wears glasses).  
变性人。若苗空、若苗紫、"半身不遂 "的薰、空的助手们。横田进（没有"jj和蛋蛋"）、浩美（紫苑的初恋，没有做过手术，金发长发）、和子（柔道冠军）、真琴（戴眼镜）。

    - Susumu's jealous that Sora has periods and she doesn't, but looks so manly that she passes better, er, "crossdressing".   
    横田进嫉妒空有例假而她没有，但是看起来如此男子气概，以至于她异装得更好。

- Triang Relations: Variant 3: See Betty and Veronica above   
三角恋：变体3：见上文Betty and Veronica。

- Unintentional Period Piece: The manga just reeks of The Nineties Japan, the most obvious indicators being the clothing and the lack of DVDs.  
无意中的时代片。这部漫画散发着九十年代日本的气息，最明显的指标就是服装和没有DVD。

- Unwanted Harem: Masahiko just keeps collecting suitors. For his female persona, naturally.  
不受欢迎的后宫。雅彦一直追求者不断，自然是因为他的女性形象。

- Unsettling Gender Reveal: Sora and Yukari, and the assistants. And Kaoru.  
性别暴露后令人不安：空和紫以及助手，还有熏

    - And sort of toward Mori, who realizes that she thinks of Sora as a man after he sleeps in the same room as her. After the event, she insists that she still sees him as a woman, but starts working him harsher.  
对森也有一点，在空和她睡在同一个房间后，她才发现自己把空当成了男人。事件发生后，她坚持认为自己还是把他当做女人，但在工作上开始对他更加严厉。
    
    - It happens to Sora far more frequently than Yukari: Twice in front of Masahiko and almost a third time at Christmas, in front of Yoko, to the husband of a stranger, to a gynaecologist, and to Saki.  
这种事发生在空身上的频率远比紫高：两次在雅彦面前，第三次可以认为是在圣诞节时，在叶子面前，对陌生人的丈夫，对妇科医生，对早纪。

        - Four of those times he was drunk.   
        这四次空都喝醉了
        
- Wannabe Gangster: Kaoru. She's "half-assed".  
想当黑帮老大：熏。她有点"半途而废"。

- Wholesome Crossdresser: Masahiko is a good guy in a dress.  
健康的女装大佬。雅彦是个穿裙子的好男人。

- Will They or Won't They?: Masahiko and Shion  
他们到底会不会结婚？：雅彦和紫苑

- Yamato Nadeshiko: Yukari  
大和抚子：紫

- Yakuza: Tatsumi  
黑社会：辰巳

- Younger Than They Look: Kaoru is strangely tall for a girl, taller than both Shion and Masahiko, but is the youngest one of the group, having just turned 17 at the time of introduction.  
比他们看起来更年轻：对一个女孩来说很奇怪，薰高的离谱，比紫苑和雅彦都要高，但她是这群人中年龄最小的，出场时刚满17岁。 

Retrieved from "https://allthetropes.fandom.com/wiki/Family_Compo?oldid=373262"





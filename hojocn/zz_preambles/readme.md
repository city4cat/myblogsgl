# 北条司作品卷首语 ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96857))

中文里所说的单行本每卷的"卷首语"，在HTKJ里被称为"作者的评论"，英文为"Review"。  


[「Cat's Eye」卷首语](./ce_preambles.md)  
[「City Hunter」卷首语](./ch_preambles.md)  
[「Rash!!」卷首语](./rash_review.md)  
[「阳光少女」卷首语](./sg_preambles.md)  
[短篇集卷首语](./ss_preambles.md)  
[「F.COMPO」卷首语](./fc_preambles.md)  
[「Angel Heart」卷首语](./ah_preambles.md)
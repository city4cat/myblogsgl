《こもれ陽の下で･･･》是北条司的作品，该作品的其他名称有: Komorebi No Moto De... / Under the Dapple Shade / Beneath the Dappled Shade / 阳光少女(大陆) / 艷陽少女(港)


**艷陽少女 卷首语**  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96857))  


(摘自《艷陽少女》香港中文版(玉皇朝))  


- Vol01  
有一天，我在散步途中發現了一棵樹，它開滿了白花，是棵百里飄香的野茉莉樹。  
在初夏的陽光映照下，花兒格外亮麗可愛，使我不禁駐足欣賞。回想過去的日子，原來我是曾經見過那棵野茉莉樹的。唸初中的三年間，我每天都是沿這條路上學的，但當時的我竟然對眼前的美好的事物完全無動於衷...  
----我之所以寫這部作品，是希望讓那些跟我小時候一樣的人，能多些留意身邊美好的事物。  


- Vol02  
仲夏的某一天，我玩得興高采烈，連跟媽媽的約會也忘記了。忽然記起來的時候，已經是黃昏時分，四周染上了一片說不上時粉紅還是紫藍的奇異色調，熟悉的街道仿佛變成了陌生的異域。那一瞬間，仿似會永恆地持續下去一樣...在那疏離的感覺之中，我呆呆地站着，連媽媽怒容滿面的樣子也忘記了，心裏只想着----這個世界多美好啊......  
----我希望在「小小大冒險」中，能夠描繪出那種氣氛...結果，也不錯吧！？



- Vol03  
我相信很多人都對櫻花抱有特別的感情。這可能是因為櫻花盛放的四月天，時逢畢業、入學、升學等無數的分離與邂逅，皆同時在這個時候發生。櫻花一下子燦爛地開滿枝頭，然後又轟轟烈烈地全部凋落。看着花開花落，人也容易被惹得傷春悲秋起來！不知道這部以櫻花季節裏的聚散為題材之漫畫，可有惹起你的無限感慨？













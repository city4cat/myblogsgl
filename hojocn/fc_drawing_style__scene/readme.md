# FC的画风-场景

----------------------------------------------------------
### 烟雾（吸烟）  

细腻：  
![](img/smoke_smoking/03_059_0__.jpg) 
![](img/smoke_smoking/09_165_5__.jpg) 
![](img/smoke_smoking/09_169_2__.jpg) 

普通：  
![](img/smoke_smoking/06_023_4__.jpg) 

简化：  
![](img/smoke_smoking/12_138_1__.jpg) 

粗犷：  
![](img/smoke_smoking/08_033_0__.jpg) 

其他：  
![](img/smoke_smoking/09_169_5__.jpg) 
![](img/smoke_smoking/10_018_6__.jpg) 
![](img/smoke_smoking/10_020_5__.jpg) 

### 下雨  
![](img/03_132_3__.jpg) 

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处





F.COMPOに期待すること  
我对F.COMPO的期望  
　

　ここのところ、作品そのものに対してというよりもその枝葉の部分をつっつくことを続けていたので、一度原点に戻って自分の考えを整理するという意味も込めて、私がF.COMPOに魅力を感じる理由、そしてF.COMPOに対して持つ期待といったものを書いていきます。（先日掲示板に書き込んだ内容に少々訂正を加えたものです）
　由于我一直在F.COMPO的枝节末梢里挑刺，而不是作品本身，所以我想写一写我觉得F.COMPO有吸引力的原因和我对F.COMPO的期望，具有回到起点和整理思路的意义。 这是我前几天写在公告板上的内容的一个略微修改版本。  

　さて、「F.COMPO」の話のどこに、自分が一番惹かれているのか。これは何回か言ってきていることですが、「家族の暖かさ」というものを感じられる点です。  
　现在，"F.COMPO "的故事中哪一部分最吸引我？ 我已经说过好几次了，但这是我能感受到 "家庭的温暖 "的事实。  

　「物語」となると、どうしても非日常を描く方向に進みがちな漫画というものにあって、これはありふれているようで逆に新鮮なものである、ということは他の皆さんも何度か触れられていることです。  
　在一个倾向于描绘不寻常的 "故事 "的漫画中，这是一个看似普通的东西，但恰恰相反，它是一个新鲜的东西。  

　現在「家庭」といったものは、その昔の有り様から、大きく変化して来ています。「家制度」はもはや崩壊しつつありますし、「家庭」にまつわる問題が多くなっていることもまた指摘されてきています。それが「モノ」に目標をおいてきた戦後の経済成長の負の側面であることは、さほど異論のないところでしょう。簡単にいえば、世のお父さん達が家庭よりも仕事を優先してきたということです。家庭のことは妻に任せて仕事に逃げ込んでいた、というパターンも多いことでしょう（ここでそのことの是非を論じるつもりはありません。念のため）。とにかくここで言いたいことは、「家庭」を正しく機能させるようにすることは、現代の最も重要な問題の一つであるということです。  
　今天，"家庭 "已经与过去的方式发生了很大的变化。 “家庭系统"正在崩溃，也有人指出，有许多问题与 "家庭 "有关。 很难说这是战后经济增长的一个消极方面，因为战后经济增长的重点是 "物"。 简单地说，世界上的父亲们一直把工作放在家庭之前。 我不是想在这里争论这个问题的优劣，只是想确认一下）。 总之，我在这里想说的是，让 "家庭 "正常运作是我们这个时代最重要的问题之一。  

　私が何気なく立ち読みしたALLMANでF.COMPOを発見、単行本を買い漁って読み終えた時に感じたことは、「これを読むことによって、もっと幸せになることができる家庭がたくさんあるんじゃないだろうか」ということでした。なんだか突然大層なことを考えているように思えますが、これには理由があります。  
　我是在一个随手浏览的Allman中发现F.COMPO的，当我买了这本书并读完后，我感到的是，有很多家庭可以通过阅读这本书而变得更加幸福。 突然想起来似乎是件大事，但这是有原因的。  

　日常の生活の中で、毎日顔を合わせる相手に対して感謝の気持ちをあらわすというのは、案外難しいものです。特にそれが家族であればなおさら、「いて当たり前」だからです。そこで「父の日」「母の日」などがありますが、サプライズパーティーが好きなアメリカ人ならともかく、照れ屋の日本人は、それでもきっちりと感謝の感情を表現することを避けがちです。  
　在我们的日常生活中，出乎意料地难以向我们每天见到的人表达感激之情。 特别是如果是一个家庭成员，那是因为我们把他们视为理所当然。 这就是为什么我们有 "父亲节 "和 "母亲节"，但美国人喜欢搞惊喜派对，而害羞的日本人往往避免表达他们的感激之情。  

　しかしこれは個々人の気持ちの持ち方の問題です。ちょっと日頃「ありがとう」と言おう、と気をつけるだけで、その効果は決して小さいものではないでしょう。電車で席を譲ったとき、相手がさも譲られるのが当たり前のような顔をしながら何の言葉もなく座るのと、ちょっと微笑んで「どうも有り難うございます」と一声かけてから座る。たったそれだけで、受ける印象と言うのは大きく違うものです。同様に、家族も人間同士の関わりあいですから、潤滑油としての言葉が持つ力は大きいだろうと言うことです。
　然而，这是每个人的感受问题。 只要每天注意说 "谢谢你"，就会产生积极的影响。 当你在火车上让出你的座位时，对方可能一言不发地坐下，同时看起来好像他们让出座位是很自然的事，或者他们可能在坐下之前微微一笑，说 "非常感谢你"。 仅仅通过这样做，你得到的印象就非常不同。 同样地，家庭是人与人之间的关系，所以语言作为润滑剂的力量是巨大的。  

　もちろん潤滑油は言葉には限りません。むしろ言葉の方が、気持ちの表現方法の一つなわけですから、当然重要なのは気持ちそのものの方です。そしてその気持ちの変化をもたらすきっかけに、F.COMPOという作品はなり得るのではないか。私が思ったのはこういうことだったのです。  
　当然，润滑剂并不限于文字。 事实上，语言是表达感情的方式之一，所以自然而然地，感情本身才是最重要的。 而我认为F.COMPO可能是一个触发器，使这些感觉发生变化。 这就是我所想的。  

　F.COMPOに描かれていること。それは様々な要素があり、一言で括ることはできません。しかしその作品全体を通じて流れているもの、それは「人を思う心」です。その形は雅彦と紫苑の関係であったり、空、紫夫婦の関係であったり、そして若苗家の全員が互いに相手を気づかいあう、という形であったりします。しかしその中身は共通のものであり、それは「相手を思いやる気持ち」と言ってもいいかもしれません。  
　F.COMPO中所描述的内容。 它有各种要素，不能用一个词来概括。 但贯穿整个作品的是 "为他人着想的心"。 这可以是雅彦和紫苑之间的关系、空和紫之间的关系，或者是若苗家所有成员相互关心的方式。 然而，他们都有一个共同点，那就是对他人的关爱之情。  

　そしてF.COMPOが我々の心を動かすという点ではもう一つ大事なことがあります。それは雅彦の境遇です。彼の両親は、既に亡くなっています。それだけでなく「天涯孤独」ということですから、他に身寄りもないと推測されます。特に母親を小二でなくしている彼には、母親との思い出がほとんどないのです。そしてその家族の思い出がない雅彦が、初めて家族と言うものの暖かさを「当たり前でないもの」として受け取っていく。このことは、ごく普通に家族がいる私に「私は今それだけで充分恵まれているのではないか」と言う、漠然と暮らしていたのでは決して持つことのない視座と感謝の気持ち、そして気の持ち方を変える少しのきっかけを与えてくれます。  
　还有一件重要的事情使F.COMPO打动了我们的心。 这是雅彦的情况。 他的父母已经死了。 不仅如此，他还 "一生中最孤独的人"，因此可以认为他没有其他亲属。 特别是由于他在二年级时失去了母亲，他对母亲的记忆很少。 而对家庭没有记忆的雅彦，第一次接受家庭的温暖是 "不自然的东西"。 这让我这个拥有正常家庭的人有机会说："我有足够的福气，"并有一种视角和感激之情，如果我一直糊里糊涂地生活，就不会有这种感觉，并改变我的思维方式。  

　作者には別にそこまでの意図はないのかもしれません。ただ作者が考える「家族というもののエッセンス」といったものを描きたいのかもしれません（私はその可能性が強いと思っています。作者の家族構成と若苗家の家族構成が同じと言うことは、作中彼等を理想の家庭にすることはあっても、逆はないでしょうから）。どのみち、それは私にはわからないことです。要は我々がどう受け取るかであり、私の受け取り方もその一つであると言うことでしょう。  
　作者可能没有这样的意图。 他可能只是想描绘他认为的家庭的本质（我认为这种可能性很大，因为作者的家庭结构和若苗家的家庭结构是一样的，这将使他们成为一个理想的家庭，而不是相反）。 无论如何，这是我不知道的事情。 关键是我们如何接受它，而我接受它的方式是其中之一。    

　そしてその受け取り方故に、私はこれからのF.COMPOに期待してしまうし、作者には是非この物語をよりよい方向へ導いていって欲しいのです。  
　也正因为如此，我对F.COMPO的未来寄予厚望，我希望作者能将这个故事引向更好的方向。    

　私がここまで書いてきたことがF.COMPOのすべてを表しているとは思いません。F.COMPOにはいろいろな魅力があり、ここで書いたことはその魅力のうちの一つに焦点を当てたに過ぎないからです。つまり、それぞれの人がそれぞれの読み方をして、その魅力を感じればいいのです。様々な可能性があって然るべきですから。  
　我不认为到目前为止我所写的内容代表了F.COMPO的全部，因为F.COMPO有许多魅力，而我在这里所写的只是侧重于其中的一个。 换句话说，每个人都应该以自己的方式阅读它，感受它的魅力。 因为应该有各种各样的可能性。    

　そのためにも、一人でも多くの人にF.COMPOを楽しみ、その魅力を感じ取ってほしいです。  
　出于这个原因，我希望尽可能多的人享受F.COMPO，感受它的魅力。  

ハル（00/02/01）  
哈尔 (00/02/01)  
　
	トップページへ戻る
Copyright 1996-2003 Rix All rights reserved.
Privacy Policy & Site Info.


# 关于「Cat's Eye 40周年纪念原画展」的一些信息([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96949))


目录：  

- [1. 数据来源](#Sources)  
- [2. 展览总览](#Overview)  
- [3. 部分展品的细节](#Details)： 
    [0](./details0.md), 
    [1](./details1.md), 
    [2](./details2.md), 
    [3](./details3.md), 
    [总结](./details3.md#SummaryOfDetails), 
- [4. 买家实拍](#BuyersPhoto)  
- [5. 观后感](#Review)  


<a name="Sources"></a>  
## 1. 数据来源  

- edition-88.com上的消息：  
    - [「Cat's♥Eye」40周年纪念原画展 ～并向City Hunter～ 展示内容公開](https://edition-88.com/blogs/blog/catseye40th-exhibition)，及其
      [中译文](../zz_edition-88.com/catseye40th-exhibition_cn.md)  
    - [「Cat's♥Eye」40周年纪念原画展 Special Interview](https://edition-88.com/blogs/blog/catseye40th-interview), 及其
      [官方中文版](https://weibo.com/ttarticle/p/show?id=2309404767977643049406)  
    - [「Cat's♥Eye」相关商品](https://edition-88.com/pages/catseye-goods)，及其
      [中译文](../zz_edition-88.com/catseye40th-exhibition_cn.md)  
    - [「City Hunter」相关商品](https://edition-88.com/pages/cityhunter-goods)，及其
      [中译文](../zz_edition-88.com/catseye40th-exhibition_cn.md)  
    - [Arts千代田场展会报道（官方）](https://edition-88.com/blogs/blog/catseye40th-report)，及其
      [官方中文](https://weibo.com/ttarticle/p/show?id=2309404772366403371739) 或 
      [中译文](../zz_edition-88.com/catseye40th-report(2022-05-21).md)  
    - [Arts千代田场展会报道（非官方）](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)  
    - [博多场展会报道（官方）](https://edition-88.com/blogs/blog/catseye40th-report02)
     （[转载](../zz_edition-88.com/catseye40th-report02(2022-11-28).md)）, 及其 
      [官方中文](https://weibo.com/ttarticle/p/show?id=2309404841217266680050) 
     （[转载](../zz_weibo.cn/catseye40th-report02(2022-11-29).md)） 

- Twitter上的相关tag（已转载至[Twitter上与北条司及其作品相关的信息 - hojocn](http://www.hojocn.com/bbs/viewthread.php?tid=96791)）:  
    - [キャッツアイ40周年記念原画展 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年記念原画展)  
    - [キャッツアイ40周年原画展 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年原画展)  
    - [キャッツアイ40周年 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年)  
    - [キャッツアイ - Twitter](https://twitter.com/hashtag/キャッツアイ)  
    - [CatsEye - Twitter](https://twitter.com/hashtag/CatsEye)  

- COAMIX官方微博： [猫眼三姐妹-城市猎人_官方 - Weibo](https://weibo.com/u/7734122862)  （可能对应[twitter账户](https://twitter.com/cityhunter100t)）   

- [北条司原画展 by MK SCRATCH |note.com](https://note.com/9451380790/n/n038e2c10f5ed), 及其
  [中译文](./zz_essay01.md)    

- [商品信息](./goods_info.md)  

- 预备资料：  
    - [关于纸张和印刷技术的资料](./extra_info.md)  
    - [关于官网展览和展品的一些说明](./about_edition88.md)  


-------------------  

<a name="Overview"></a>  
## 2. 展览总览  

我猜，展览可分为几个板块：  

- 开场  
    - 北条司为该画展绘制的三姐妹画像；  
    - 北条司&堀江信彦的寄语、作者简介；  
    - 作者现场作画（小瞳半边像）；  
    - 作品的背景资料、画材等；  
- Cat's Eye  
  注：因信息不足，所以无法确定以下子板块在展览中的先后顺序。
    - 噂のキャッツアイ 颯爽参上！  
     （传说中的Cat'sEye登场! ）  
    - 魅惑の怪盗キャッツ♥アイ  
     （迷人的怪盗Cat'sEye）  
    - 喫茶店キャッツアイ 來生姉妹と俊夫の日常風景  
     （Cat'sEye咖啡屋 來生姐妹和俊夫的日常生活）  
    - 泥棒紳士ねずみと 犬鳴署キャッツ特搜班  
     （小偷绅士老鼠和犬鳴署猫眼特搜班）  
    - キャッツアイに隱された 秘密と目的とは！？  
      （Cat'sEye背后的秘密和目的！？）  
    - 恋ふたたび  
     （再恋爱一次）  
    - まだまだキャッツアイ 来生姉妹は永遠に不滅です  
      （Cat'sEye 永远的来生姐妹）  
    - Cat's Eye插画
- City Hunter  
    - City Hunter -XYZ-的全部原画
    - City Hunter插画
- 重绘的《Space Angel》    
- 商品区  

将[Arts千代田场展会报道（官方）](https://edition-88.com/blogs/blog/catseye40th-report)、[Arts千代田场展会报道（非官方）](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)、[博多场展会报道（官方）](https://edition-88.com/blogs/blog/catseye40th-report02)中的图片集中如下。因搜集的资料不全，所以以下内容或顺序难免有误。欢迎指出错误，谢谢!  
此外，我猜，千叶田场的观看顺序是从右至左，博多场的观看顺序是从左至右。  

**2.0 开场**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_4441_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_4441_4800x4800.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3139_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3139_4800x4800.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0985_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0985_4800x4800.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3097_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3097_4800x4800.jpg) 

[<img title="5备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04018_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04018_4800x4800.jpg) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04025_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04025_4800x4800.jpg) 
[<img title="7备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3098_e781449e-7090-4fda-bccb-bbdcc355cbb4_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3098_e781449e-7090-4fda-bccb-bbdcc355cbb4_4800x4800.jpg) 

[<img title="8备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3109_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3109_4800x4800.jpg) 
[<img title="9备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04156_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04156_4800x4800.jpg) 
[<img title="10备份缩略图(点击查看原图)" height="100" src="img/thumb/ecac7082gy1h262fmgrp6j20ku0v9n99.jpg"/> 
](https://wx2.sinaimg.cn/large/ecac7082gy1h262fmgrp6j20ku0v9n99.jpg) 
[<img title="11备份缩略图(点击查看原图)" height="100" src="img/thumb/ecac7082gy1h262fnkmopj20ku0v9k3q.jpg"/> 
](https://wx2.sinaimg.cn/large/ecac7082gy1h262fnkmopj20ku0v9k3q.jpg) 
[<img title="12备份缩略图(点击查看原图)" height="100" src="img/thumb/ecac7082gy1h262foj6chj20ku0v9k3i.jpg"/> 
](https://wx4.sinaimg.cn/large/ecac7082gy1h262foj6chj20ku0v9k3i.jpg)  

[<img title="13备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3100_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3100_4800x4800.jpg) 
[<img title="14备份缩略图(点击查看原图)" height="100" src="img/forward_hojo.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821ulme5j20ku0v9dis.jpg) 
[<img title="15备份缩略图(点击查看原图)" height="100" src="img/forward_nobu.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821unvnvj20ku0v9q6d.jpg) 

[<img title="16备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04154_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04154_4800x4800.jpg) 
[<img title="17备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3099_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3099_4800x4800.jpg) 
[<img title="18备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_4800x4800.jpg) 

[<img title="19备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04046_990d0745-ccb2-42fc-8aa4-d65bd95932ac_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04046_990d0745-ccb2-42fc-8aa4-d65bd95932ac_4800x4800.jpg) 
[<img title="20备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_4800x4800.jpg) 
[<img title="21备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0258_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0258_4800x4800.jpg) 
[<img title="22备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3107_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3107_4800x4800.jpg) 

[<img title="23备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04035_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04035_4800x4800.jpg) 
[<img title="24备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04037_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04037_4800x4800.jpg) 
[<img title="25备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3106_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3106_4800x4800.jpg) 
[<img title="26备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVkAO8UYAEC577.jpg"/> 
](https://pbs.twimg.com/media/FiVkAO8UYAEC577?format=jpg&name=large) 
[<img title="27备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04041_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04041_4800x4800.jpg) 
[<img title="28备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04040_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04040_4800x4800.jpg) 
[<img title="29备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3105_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3105_4800x4800.jpg) 
[<img title="30备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h282269hgwj20ku0dw0tg.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h282269hgwj20ku0dw0tg.jpg) 
[<img title="31备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822683pej20ku0dwjry.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2822683pej20ku0dwjry.jpg) 

[<img title="32备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04157_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04157_4800x4800.jpg) 
[<img title="33备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04198_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04198_4800x4800.jpg) 

**2.1 噂のキャッツアイ 颯爽参上！**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz5wdbVIAYqQcp.jpg"/> 
](https://pbs.twimg.com/media/FSz5wdbVIAYqQcp?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/Fik7bZDakAAPHEV.jpg"/> 
](https://pbs.twimg.com/media/Fik7bZDakAAPHEV?format=jpg&name=large) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04031_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04031_4800x4800.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h282265aaoj20ku0v9abc.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h282265aaoj20ku0v9abc.jpg) 
[<img title="5备份缩略图(点击查看原图)" height="100" src="img/thumb/FTfDPQGaAAA4UuV.jpg"/> 
](https://pbs.twimg.com/media/FTfDPQGaAAA4UuV?format=jpg&name=large) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28226hinpj20ku0dwdg2.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h28226hinpj20ku0dwdg2.jpg) 
[<img title="7备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnvggQUAAAxJsE.jpg"/> 
](https://pbs.twimg.com/media/FSnvggQUAAAxJsE?format=jpg&name=large) 
[<img title="8备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h26vgk1w80j30p00irmyq.jpg"/> 
](https://wx2.sinaimg.cn/wap720/008rpAF8gy1h26vgk1w80j30p00irmyq.jpg) 
[<img title="9备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04051_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04051_4800x4800.jpg) 

**2.2 泥棒紳士ねずみと 犬鳴署キャッツ特搜班**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz6ihCVEAAWEEU.jpg"/> 
](https://pbs.twimg.com/media/FSz6ihCVEAAWEEU?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822istdcj20ku0dwaaq.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h2822istdcj20ku0dwaaq.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822ix3aaj20ku0dw74o.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2822ix3aaj20ku0dw74o.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_4800x4800.jpg) 

**2.3 魅惑の怪盗キャッツ♥アイ**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz6jIKVEAAzlvj.jpg"/> 
](https://pbs.twimg.com/media/FSz6jIKVEAAzlvj?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04098_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04098_4800x4800.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822ix751j20ku0dw3zb.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2822ix751j20ku0dw3zb.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04066_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04066_4800x4800.jpg) 
[<img title="5备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822iy7ktj20ku0dwjrp.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2822iy7ktj20ku0dwjrp.jpg) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822iyk5oj20ku0dwweq.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2822iyk5oj20ku0dwweq.jpg) 
[<img title="7备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822j3nhij20ku0dwwel.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2822j3nhij20ku0dwwel.jpg) 
[<img title="8备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822u7ov7j20ku0dwgm7.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h2822u7ov7j20ku0dwgm7.jpg) 
[<img title="9备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822u7a2uj20ku0dwq3e.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2822u7a2uj20ku0dwq3e.jpg) 

**2.4 喫茶店キャッツアイ 來生姉妹と俊夫の日常風景**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz5w1XVIAEkC41.jpg"/> 
](https://pbs.twimg.com/media/FSz5w1XVIAEkC41?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/Fii29j-VEAES2Kw.jpg"/> 
](https://pbs.twimg.com/media/Fii29j-VEAES2Kw?format=jpg&name=large) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_1024x1024.jpg?v=1669576824"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_4800x4800.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2821uemwbj20ku0dwjrw.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821uemwbj20ku0dwjrw.jpg) 
[<img title="5备份缩略图(点击查看原图)" height="100" src="img/thumb/40a836fegy1h26hl448rvj20ku0dw74r.jpg"/> 
](https://wx3.sinaimg.cn/wap720/40a836fegy1h26hl448rvj20ku0dw74r.jpg) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="img/thumb/FTeAmkUVIAAxATI.jpg"/> 
](https://pbs.twimg.com/media/FTeAmkUVIAAxATI?format=jpg&name=large)  
[<img title="7备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVivbkUYAEUefo.jpg"/> 
](https://pbs.twimg.com/media/FiVivbkUYAEUefo?format=jpg&name=large) 
[<img title="8备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVivb-VUAA-O9P.jpg"/> 
](https://pbs.twimg.com/media/FiVivb-VUAA-O9P?format=jpg&name=large) 

**2.5 キャッツアイに隱された 秘密と目的とは！？**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz6jfFUcAA4YJs.jpg"/> 
](https://pbs.twimg.com/media/FSz6jfFUcAA4YJs?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822ud5paj20ku0dwmyd.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2822ud5paj20ku0dwmyd.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822iv4e0j20ku0dwmxy.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2822iv4e0j20ku0dwmxy.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822itou6j20ku0dwwfd.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2822itou6j20ku0dwwfd.jpg) 

**2.6 恋ふたたび**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSn9vNHVsAEzkBJ.jpg"/> 
](https://pbs.twimg.com/media/FSn9vNHVsAEzkBJ?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822u9qb0j20ku0dw3yx.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2822u9qb0j20ku0dw3yx.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/40a836fegy1h26hl1nip8j20ku0dwt93.jpg"/> 
](https://wx3.sinaimg.cn/wap720/40a836fegy1h26hl1nip8j20ku0dwt93.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/Fiiy50AVIAEz_KV.jpg"/> 
](https://pbs.twimg.com/media/Fiiy50AVIAEz_KV?format=jpg&name=large) 
[<img title="5备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04196_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04196_4800x4800.jpg) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822ua9x1j20ku0dwdgl.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h2822ua9x1j20ku0dwdgl.jpg) 
[<img title="7备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822ua5r1j20ku0dw0te.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2822ua5r1j20ku0dw0te.jpg) 
[<img title="8备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822uabgoj20ku0dwaas.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2822uabgoj20ku0dwaas.jpg) 
[<img title="9备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822ua5r1j20ku0dw0te.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2822ua5r1j20ku0dw0te.jpg) 
[<img title="10备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822ul3yrj20ku0dwmxy.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h2822ul3yrj20ku0dwmxy.jpg) 
[<img title="11备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_4800x4800.jpg) 
[<img title="12备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_4800x4800.jpg) 


**2.7 まだまだキャッツアイ 来生姉妹は永遠に不滅です**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz88QpVsAAXuIk.jpg"/> 
](https://pbs.twimg.com/media/FSz88QpVsAAXuIk?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/FSn9vNJVEAALOyl.jpg"/> 
](https://pbs.twimg.com/media/FSn9vNJVEAALOyl?format=jpg&name=large) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2822upjugj20ku0dwgmk.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2822upjugj20ku0dwgmk.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04198_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04198_4800x4800.jpg) 
[<img title="5备份缩略图(点击查看原图)" height="100" src="img/thumb/40a836fegy1h26hl3k6ppj20ku0dwdgf.jpg"/> 
](https://wx4.sinaimg.cn/wap720/40a836fegy1h26hl3k6ppj20ku0dwdgf.jpg) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_4800x4800.jpg) 

**2.8 Cat's Eye板块结束**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823ouwltj20ku0dwgmt.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2823ouwltj20ku0dwgmt.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog01_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog01_4800x4800.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0986_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0986_4800x4800.jpg) 

**2.9 CityHunter -XYZ-**  
因该版块搜集到的图片多、能连起来，所以将这些图片按千代田场展的顺序连起来。阅读顺序从右至左。  
[<img title="24备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823owsb3j20ku0dw3zl.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h2823owsb3j20ku0dw3zl.jpg) 
[<img title="23备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh7TSueagAEI2FC.jpg"/> 
](https://pbs.twimg.com/media/Fh7TSueagAEI2FC?format=jpg&name=large) 
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823ouw41j20ku0dwt91.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2823ouw41j20ku0dwt91.jpg) 
[<img title="22备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3127_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3127_4800x4800.jpg) 
[<img title="21备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh7LmxDaAAAuIo-.jpg"/> 
](https://pbs.twimg.com/media/Fh7LmxDaAAAuIo-?format=jpg&name=large) 
[<img title="20备份缩略图(点击查看原图)" height="100" src="img/thumb/FizuCBfVEAIWeHG.jpg"/> 
](https://pbs.twimg.com/media/FizuCBfVEAIWeHG?format=jpg&name=large) 
[<img title="19备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh6155IUAAAkay0.jpg"/> 
](https://pbs.twimg.com/media/Fh6155IUAAAkay0?format=jpg&name=large) 
[<img title="18备份缩略图(点击查看原图)" height="100" src="img/thumb/FTfDPpqaUAAcA1g.jpg"/> 
](https://pbs.twimg.com/media/FTfDPpqaUAAcA1g?format=jpg&name=large) 
[<img title="17备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_4800x4800.jpg) 
[<img title="16备份缩略图(点击查看原图)" height="100" src="img/thumb/FierNtGVsAEp5UR.jpg"/> 
](https://pbs.twimg.com/media/FierNtGVsAEp5UR?format=jpg&name=large) 
[<img title="15备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823oxeduj20ku0dwaak.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2823oxeduj20ku0dwaak.jpg) 
[<img title="14备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3131_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3131_4800x4800.jpg) 
[<img title="13备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04140_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04140_4800x4800.jpg) 
[<img title="12备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823p85bvj20ku0dw74t.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2823p85bvj20ku0dw74t.jpg) 
[<img title="11备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04123_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04123_4800x4800.jpg) 
[<img title="10备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h26vgklq4zj30iw0e63yy.jpg"/> 
](https://wx3.sinaimg.cn/wap720/008rpAF8gy1h26vgklq4zj30iw0e63yy.jpg) 
[<img title="9备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823ow2ibj20ku0dwmxv.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2823ow2ibj20ku0dwmxv.jpg) 
[<img title="8备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823ouyh1j20ku0dwwfb.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2823ouyh1j20ku0dwwfb.jpg) 
[<img title="7备份缩略图(点击查看原图)" height="100" src="img/thumb/FSncDfXVUAAVQ4h.jpg"/> 
](https://pbs.twimg.com/media/FSncDfXVUAAVQ4h?format=jpg&name=large) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823othgrj20ku0dw755.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2823othgrj20ku0dw755.jpg) 
[<img title="5备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823ov30sj20ku0dwt9l.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h2823ov30sj20ku0dwt9l.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_4800x4800.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04194_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04194_4800x4800.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/FSt-vhracAAnHKr.jpg"/> 
](https://pbs.twimg.com/media/FSt-vhracAAnHKr?format=jpg&name=large) 
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FTPeF_cUsAAKX5u.jpg"/> 
](https://pbs.twimg.com/media/FTPeF_cUsAAKX5u?format=jpg&name=large) 

**2.10 CityHunter板块结束**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823owsb3j20ku0dw3zl.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h2823owsb3j20ku0dw3zl.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog01_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog01_4800x4800.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0986_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0986_4800x4800.jpg) 

**2.11 拍照区**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04052_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04052_4800x4800.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3118_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3118_4800x4800.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/rectangle_large_type_2_3be2c217e4a3c55ee30becb69ece4400.jpg"/> 
](https://assets.st-note.com/production/uploads/images/92016131/rectangle_large_type_2_3be2c217e4a3c55ee30becb69ece4400.jpeg) 

**2.12 重绘的《Space Angel》**  
(以下新浪的图片源自[猫眼40周年记念原画展 by鸟山明保存会|weibo.com](https://weibo.com/ttarticle/p/show?id=2309404769077813183265) )  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823vsca0j20ku0v9gop.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2823vsca0j20ku0v9gop.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2821ugft2j20ku0dw3z8.jpg"/> 
](https://wx3.sinaimg.cn/large/bcb839c9ly1h2821ugft2j20ku0dw3z8.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04138_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04138_4800x4800.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_1024x1024.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_4800x4800.jpg) 


**2.13 商品区**  
[<img title="1备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog02_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog02_4800x4800.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_4800x4800.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0996_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0996_4800x4800.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_4800x4800.jpg) 

[<img title="5备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0994_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0994_4800x4800.jpg) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04180_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04180_4800x4800.jpg) 
[<img title="7备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog03_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog03_4800x4800.jpg) 
[<img title="8备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28245h8zjj20ku0dwwgn.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h28245h8zjj20ku0dwwgn.jpg) 
[<img title="9备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28245izarj20ku0dwmz8.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h28245izarj20ku0dwmz8.jpg) 
[<img title="10备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28245nhxbj20ku0dwjtj.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h28245nhxbj20ku0dwjtj.jpg) 
[<img title="11备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04176_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04176_4800x4800.jpg) 
[<img title="12备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h282459o49j20ku0dw0ur.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h282459o49j20ku0dw0ur.jpg) 
[<img title="13备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0989_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0989_4800x4800.jpg) 
[<img title="14备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823vx5txj20ku0dw0um.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2823vx5txj20ku0dw0um.jpg) 
[<img title="15备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_1004_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_1004_4800x4800.jpg) 
[<img title="16备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823vvpq1j20ku0v9adw.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2823vvpq1j20ku0v9adw.jpg) 

[<img title="17备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04175_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04175_4800x4800.jpg) 
[<img title="18备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28245amsuj20ku0v9q69.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h28245amsuj20ku0v9q69.jpg) 
[<img title="19备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28245h4y0j20ku0dwwg2.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h28245h4y0j20ku0dwwg2.jpg) 
[<img title="20备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0993_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0993_4800x4800.jpg) 
[<img title="21备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28245fkvcj20ku0dwwhf.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h28245fkvcj20ku0dwwhf.jpg) 
[<img title="22备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2824je7a3j20ku0dwq58.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2824je7a3j20ku0dwq58.jpg) 
[<img title="23备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2824jg7z9j20ku0dwmyw.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2824jg7z9j20ku0dwmyw.jpg) 

[<img title="24备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823vx5vmj20ku0dw75s.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2823vx5vmj20ku0dw75s.jpg) 
[<img title="25备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823w1gonj20ku0dwjsw.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2823w1gonj20ku0dwjsw.jpg) 
[<img title="26备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823w4cb8j20ku0v941i.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2823w4cb8j20ku0v941i.jpg) 
[<img title="27备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823w6tmaj20ku0dw75z.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2823w6tmaj20ku0dw75z.jpg) 

[<img title="28备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823w24i1j20ku0dwtaa.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h2823w24i1j20ku0dwtaa.jpg) 
[<img title="29备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h282457p0nj20ku0dwta2.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h282457p0nj20ku0dwta2.jpg) 
[<img title="30备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28245cqxvj20ku0dwt9n.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h28245cqxvj20ku0dwt9n.jpg) 

-------------------  

<a name="Details"></a>  
## 3. 部分展品的细节  
因内容多，故分为多个文件： 
    [0](./details0.md), 
    [1](./details1.md), 
    [2](./details2.md), 
    [3](./details3.md), 
    [总结](./details3.md#SummaryOfDetails), 

-------------------  

<a name="BuyersPhoto"></a>  
## 4. 买家实拍
以下图片源于网上买家的照片，侵则删。  

[买家实拍 - Twitter](https://twitter.com/N7Obo2E2S3a3Xw4/status/1525620162791059456)  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXWDwUEAImiKJ.jpg"/> 
](https://pbs.twimg.com/media/FSwXWDwUEAImiKJ?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXWDyVsAAsLJy.jpg"/> 
](https://pbs.twimg.com/media/FSwXWDyVsAAsLJy?format=jpg&name=large) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXWDwUAAA-awQ.jpg"/> 
](https://pbs.twimg.com/media/FSwXWDwUAAA-awQ?format=jpg&name=large) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXgnGUYAAbQFB.jpg"/> 
](https://pbs.twimg.com/media/FSwXgnGUYAAbQFB?format=jpg&name=large) 
[<img title="5备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXgndVsAAGqkg.jpg"/> 
](https://pbs.twimg.com/media/FSwXgndVsAAGqkg?format=jpg&name=large) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXgnvUsAAzO4h.jpg"/> 
](https://pbs.twimg.com/media/FSwXgnvUsAAzO4h?format=jpg&name=large) 
[<img title="7备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXgn9VsAAEmea.jpg"/> 
](https://pbs.twimg.com/media/FSwXgn9VsAAEmea?format=jpg&name=large) 
[<img title="8备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXkukUUAAXJzz.jpg"/> 
](https://pbs.twimg.com/media/FSwXkukUUAAXJzz?format=jpg&name=large) 
[<img title="9备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwXkuyVsAAwAL0.jpg"/> 
](https://pbs.twimg.com/media/FSwXkuyVsAAwAL0?format=jpg&name=large) 

[买家实拍 - weibo](https://weibo.com/2163554012/M3aTv1He9)  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/80f536dcly1h5muni2ppoj20u0140aee.jpg"/> 
](https://wx2.sinaimg.cn/large/80f536dcly1h5muni2ppoj20u0140aee.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/80f536dcly1h5munigh17j20u0140q4o.jpg"/> 
](https://wx2.sinaimg.cn/large/80f536dcly1h5munigh17j20u0140q4o.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/80f536dcly1h5munjalpqj21400u0qan.jpg"/> 
](https://wx4.sinaimg.cn/large/80f536dcly1h5munjalpqj21400u0qan.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/80f536dcly1h5munjqutrj21400u00x4.jpg"/> 
](https://wx4.sinaimg.cn/large/80f536dcly1h5munjqutrj21400u00x4.jpg) 

[买家实拍 - weibo](https://weibo.com/7477017531/M3Kb4qaPi)  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/008a0NOXgy1h5r6id65uwj30zk0qon0g.jpg"/> 
](https://wx4.sinaimg.cn/large/008a0NOXgy1h5r6id65uwj30zk0qon0g.jpg) 

[买家实拍 - weibo](https://weibo.com/2175179624/M6V6Q7hdK)  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/81a69b68ly1h6fbrj3kgkj20u0140ag7.jpg"/> 
](https://wx3.sinaimg.cn/large/81a69b68ly1h6fbrj3kgkj20u0140ag7.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/81a69b68ly1h6fbrjd9sej20u0140abm.jpg"/> 
](https://wx4.sinaimg.cn/large/81a69b68ly1h6fbrjd9sej20u0140abm.jpg) 

[买家实拍 - weibo](https://weibo.com/1453640924/MgytVAMEb)  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/56a4ccdcgy1h8gl5exfc2j22c0340b2b.jpg"/> 
](https://wx2.sinaimg.cn/large/56a4ccdcgy1h8gl5exfc2j22c0340b2b.jpg) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/56a4ccdcgy1h8gl5kecrqj228s28snpe.jpg"/> 
](https://wx1.sinaimg.cn/large/56a4ccdcgy1h8gl5kecrqj228s28snpe.jpg) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/56a4ccdcgy1h8gl5mjys8j22w2253b2b.jpg"/> 
](https://wx3.sinaimg.cn/large/56a4ccdcgy1h8gl5mjys8j22w2253b2b.jpg) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/56a4ccdcgy1h8gl5oqod0j22c0340hdv.jpg"/> 
](https://wx1.sinaimg.cn/large/56a4ccdcgy1h8gl5oqod0j22c0340hdv.jpg) 
[<img title="5备份缩略图(点击查看原图)" height="100" src="img/thumb/56a4ccdcgy1h8gl5rscspj22c03401l0.jpg"/> 
](https://wx4.sinaimg.cn/large/56a4ccdcgy1h8gl5rscspj22c03401l0.jpg) 
[<img title="6备份缩略图(点击查看原图)" height="100" src="img/thumb/56a4ccdcgy1h8gl5ujis3j22c03401l0.jpg"/> 
](https://wx2.sinaimg.cn/large/56a4ccdcgy1h8gl5ujis3j22c03401l0.jpg) 
[<img title="7备份缩略图(点击查看原图)" height="100" src="img/thumb/56a4ccdcgy1h8gl5cy2p4j22c02c0u0y.jpg"/> 
](https://wx3.sinaimg.cn/large/56a4ccdcgy1h8gl5cy2p4j22c02c0u0y.jpg) 


-------------------  

<a name="Review"></a>  
## 5. 观后感  
[北条司-原画展  by MK SCRATCH](./zz_essay01.md)  

-------------------  

**Links:**  

- https://ocr.space/  
- https://o-oo.net.cn/japaneseocr/  
- https://www.qiuziti.com/tool_dariyu.html  
- https://o-oo.net.cn/keyboard/  
- https://www.deepl.com/  
- https://translate.google.cn

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  

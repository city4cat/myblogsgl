
# About EDITION88, Printing method, PAPER FOR ART PRINTS, and etc ...

## [Japanese version](https://edition-88.com/pages/about-us)

### EDITION88の版画 (EDITION88印刷品)
EDITION88はアートギャラリーであり、版画工房です。  
EDITION88是一个艺术画廊和印刷工坊。  

販売されている版画は、全て自社で制作しています。  
所有出售的印刷品都是在内部制作的。  

版画は商品の価値を証明する為に、２つの決まりがあります。  
印刷品证明产品的价值有两条规则。  

一つは、「アーティストの直筆サイン入り」、もしくは「落款」が押されていること。  
首先，必须有 "艺术家的签名 "或盖有 "签名"。  

二つ目は、限定数が設定され、「エディションナンバー」が記載されていることです。  
其次，印刷品的数量必须是有限的，而且必须标有 "版号"。  

### 全て直筆サインもしくは落款入り (所有签名或题词的作品)
世の中には複製、高級印刷物と呼ばれているものが多く存在しますが、版画には必ず、直筆サインもしくは落款が入ります。 
世界上有许多复制品和精美的印刷品，但印刷品总是有个人签名或题词。  
 
なぜなら版画は、昔から存在する「絵画」の表現方法の一つであり、アーティストの承認の元、アーティストと版画工房（私たち）が協力して制作するものだからです。  
这是因为版画是 "绘画 "的一种表现形式，已经存在了很长时间，是艺术家和印刷厂（我们）之间的合作，并得到艺术家的认可。   

アーティストと共に、出来上がった版画を入念にチェックします。  
与艺术家一起，对完成的印刷品进行仔细检查。  

十分に納得できる仕上がりであることを確認した上で、版画一枚一枚に直筆サインもしくは落款が入ります。  
在确认工艺令人满意后，每幅印刷品都会有个人签名或题词。  


### エディションナンバー（版本编号）
版画は必ず限定数が決められています。  
印刷品的数量总是有限的。  

8/100という記載があれば、分母の100が限定枚数になります。分子の数字は一枚一枚の版画に振られる数で、つまり、8/100は世の中に一枚しか存在しません。  
如果印刷品上标有8/100，分母100就是有限的印刷品数量。 分子中的数字是分配给每个印刷品的数字，也就是说，世界上只有一个8/100。  

この表記はエディションナンバーと言われており、版画には必ず記載されます。  
这个符号被称为版本编号，并且总是在印刷品上标注。 

エディションナンバーこそが本物の価値、希少性を証明するものになります。  
版本号是真实性和稀有性的证明。  


### アーティストとの共同作業（与艺术家合作）
アーティストが生み出すアートやデザインを、「版画」の印刷技法を用いて、忠実に再現していくことが私たち版画工房の仕事です。  
我们在印刷车间的工作是忠实地再现艺术家使用 "版画 "这一印刷技术所创造的艺术和设计。  

そのために私たちは、アーティストと話し合いを重ねながら、アーティストが表現したいものを正確に理解した上で、それを版画という表現に落とし込んでいきます。  
为了达到这个目的，我们与艺术家讨论，了解他或她到底想表达什么，然后将其付诸于版画的表达。  

例えば、版画に使われる紙の質感、インクの微妙なラインの表現など、幾度にもわたる修正を重ねながら、表現したいものに近づけていく制作プロセスは、まさにアーティストと共同作業です。  
例如，版画用纸的质地、墨水的微妙线条，以及通过无数次修改使版画更接近于它们所要表达的内容的制作过程，都是真正与艺术家合作的过程。   


## [English version](https://edition88.com/pages/about-us)

### What is EDITION88？

We curate and produce everything from exhibits to art prints, working mainly with Japanese creators such as manga artists, animators, and illustrators.
我们策划和制作从展览到艺术印刷品的一切，主要与日本创作者合作，如漫画家、动画师和插画家。  

Our art prints are based on their original creations, all of which are developed in close collaboration with the artist.  
我们的艺术版画是基于他们的原创作品，所有这些作品都是与艺术家紧密合作开发的。  

To create an art print, a complex process with numerous steps is required. Only those that pass our rigorous inspections are made available and are hand-signed by the artist or have the artist’s seal.  
要创作一幅艺术印刷品，需要一个复杂的过程，有许多步骤。只有那些通过我们严格检查的作品才会被提供，并由艺术家亲笔签名或盖有艺术家的印章。   

### COLLABORATION WITH ARTISTS（与艺术家协作）

Our printing studio’s job is to accurately replicate the artist’s art or design using a special method for art prints. We discuss with artists extensively to fully understand what they wanted to explore and express through their creations, and reflect their intent into the print. We also talk details, such as the texture of the paper, delicate lines drawn by subtle ink, and so on. The process of discussions and numerous modifications to bring the art print closer to the artist’s vision is truly a collaborative effort.   
我们印刷工作室的工作是利用艺术印刷品的特殊方法准确地复制艺术家的艺术或设计。我们与艺术家广泛讨论，充分了解他们想通过创作探索和表达什么，并将他们的意图反映到印刷品中。我们还谈论一些细节，如纸张的质地、微妙的墨水所绘制的精致线条等等。为使艺术印刷品更接近艺术家的设想而进行的讨论和多次修改的过程是真正的合作努力。  


### Printing method（印刷方法）

To articulate a diverse range of color and texture, we use giclée (water-based pigments) and UV printers with UV curing ink as needed.  
为了阐述多样化的色彩和质地，我们根据需要使用giclée（水基颜料）和带有紫外线固化墨水的UV打印机。  
(译注：详见[Giclée](./extra_info.md#Giclée)。Giclée中译名有：[艺术微喷、数位版画、数码艺术微喷](https://www.bing.com/dict/search?q=giclée&FORM=HDRSC6)   )  

Giclée is capable of recreating a broad range of clear colors through its seven-color lineup: cyan, magenta, yellow, black (CMYK), orange, green, and gray. With a UV printer, in addition to the seven giclée colors, we can also use UV curing ink in white and a clear, glossy varnish, which are perfect for giving depth. Another advantage of the UV printer is that it can present matière (uneven physical texture such as brushstrokes) or the glistening touch of paint or pencil. These aspects enable it to faithfully replicate the three-dimensional finish of the original or explore a new expression just for the print version. We also create prints as mixed media, using the combination of giclée and UV printer.  
Giclée能够通过其七色阵容再现广泛的清晰色彩：青色（cyan）、品红（magenta）、黄色、黑色（CMYK）、橙色、绿色和灰色。使用UV打印机，除了七种胶印颜色外，我们还可以使用白色的UV固化墨水和透明的光泽清漆，这些都是赋予深度的完美选择。UV打印机的另一个优势是，它可以呈现matière（不均匀的物理纹理，如笔触）或者绘画颜料或铅笔的闪亮触感。这些方面使它能够忠实地复制原作的三维抛光，或只为印刷版而探索一种新的表达方式。我们还以混合媒体的方式创作版画，使用giclée和UV打印机的组合。    

### PAPER FOR ART PRINTS（艺术印刷品用纸）

To create prints that satisfy the original artist, the paper that the art print is printed on needs to meet many conditions, such as the matière of the surface, wear resistance to printing pressure, fibers that hold up even when submerged in water, and ink compatibility. Below are some of the papers we often use.  
为了创作出令原作者满意的印刷品，艺术印刷品所使用的纸张需要满足许多条件，如表面的matière、对印刷压力的耐磨性、即使浸泡在水中也能保持的纤维、以及墨水的兼容性。以下是我们经常使用的一些纸张。  

・Bamboo-based Japanese Paper (washi)  
竹制日本纸(和纸)  (译注： 详见[Washi](./extra_info.md#Washi)。)  

Papermaking or washi making is a long-established, traditional technology in Japan. As such, we work with a highly qualified hand-made washi studio in Tokushima prefecture to prepare a unique bamboo-based paper just for us. Bamboo-based washi is soft and finely textured, similar to silk, and allows for intricate printed designs.  
造纸或制作washi是日本一项历史悠久的传统技术。因此，我们与Tokushima县一家高素质的手工和纸工作室合作，为我们准备了一种独特的竹制纸。竹制washi质地柔软，纹理细腻，与丝绸相似，可以进行复杂的印刷设计。   

・Kakita, an Acid-free Paper  
Kakita，一种无酸纸  

The Kakita brand is named after the Kakita River that runs near its factory. Only neutral agents are used in the development of acid-free print paper; the paper is highly light-resistant, allowing the work to be exhibited or stored for an extended time without being affected.  
Kakita品牌是以其工厂附近的Kakita河命名的。在开发无酸印刷纸时，只使用了中性制剂；这种纸具有很强的耐光性，使作品可以长期展出或储存而不受影响。   


### SIGNED BY HAND OR STAMPED WITH THE ARTIST’S SEAL（亲笔签名或盖上艺术家的印章）

All of our art prints are either signed by the artist or have their seal. Printing is an art form that has been around for a long time, created as a joint effort by the artist and the print studio like us, under the artist’s approval. The signature or seal captures our cooperative relationship. We carefully check the finished print together with the artist; once the artist is fully satisfied with the finished prints, they sign by hand or apply their seal on each approved print.  
我们所有的艺术印刷品都有艺术家的签名或盖有他们的印章。印刷是一种由来已久的艺术形式，是由艺术家和像我们这样的印刷工作室在艺术家的批准下共同创作的。签名或印章体现了我们的合作关系。我们与艺术家一起仔细检查完成的印刷品；一旦艺术家对完成的印刷品完全满意，他们就会在每件批准的印刷品上亲笔签名或盖上他们的印章。  

Of note, in Japan, seal impressions are deemed to have the same authority as signatures. Japanese drawings or calligraphy pieces often carry the artist’s seal as proof that the artist created the work. Even today, official documents such as contracts are often “sealed” instead of signed.  
值得注意的是，在日本，印章印记被认为具有与签名相同的权威。日本的图画或书法作品往往带有艺术家的印章，以证明艺术家创作了该作品。即使在今天，像合同这样的官方文件也经常是 "盖章 "而不是签名。   

### edition number(版本编号）

An edition number is proof of an art print’s authenticity and verification of its rarity. All authentic prints carry an edition number.  
版本编号是艺术印刷品的真实性的证明，也是对其稀有性的验证。所有真实的印刷品都有一个版本编号。 

Generally, prints have a limited run. If the number on a print says 8/100, it means only 100 prints were made. The number before the slash is the number assigned sequentially to each print, meaning that there is only one 8/100 print in the world.  
一般来说，印刷品的发行量是有限的。如果印刷品上的数字是8/100，这意味着只生产了100张。斜线前面的数字是按顺序分配给每张印刷品的数字，这意味着世界上只有一张8/100印刷品。   

### CERTIFICATE OF AUTHENTICITY(鉴定证书)

All of our art prints have a Certificate of Authenticity as proof that the print is genuine.  
我们所有的艺术印刷品都有一份鉴定证书，以证明印刷品的真实性。   



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  

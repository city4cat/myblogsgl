部分展品的细节目录： 
    [0](./details0.md), 
    [1](./details1.md), 
    [2](./details2.md), 
    [3](./details3.md), 
    [总结](./details3.md#SummaryOfDetails), 

## 「Cat's Eye 40周年纪念原画展」部分展品的细节2  

「Cat's Eye 40周年纪念原画展」以下简称“画展”。  



---  

## 原画（1984年）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiU_av1aEAAXU-D.jpg"/> 
](https://pbs.twimg.com/media/FiU_av1aEAAXU-D?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/NakedbrunchT/status/1595751384389156865)    

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![](img/not_given.jpg) 
![](img/illustrations_48_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiU_av1aEAAXU-D.jpg"/> 
](https://pbs.twimg.com/media/FiU_av1aEAAXU-D?format=jpg&name=large) 

细节：  

- 虹膜为蓝色。  

- 面部细节。B(下图左一), C(下图左二)。    
![](img/illustrations_48_crop0.jpg) 
![](img/FiU_av1aEAAXU-D_crop0.jpg) 



---  

## 原画（1984年）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS4WJD6UAAA_BaG.jpg"/> 
](https://pbs.twimg.com/media/FS4WJD6UAAA_BaG?format=jpg&name=medium)  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/S_Rouge/status/1526181671548391424)      

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/illustrations_48-2_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS4WJD6UAAA_BaG.jpg"/> 
](https://pbs.twimg.com/media/FS4WJD6UAAA_BaG?format=jpg&name=medium) 

细节：  

- 面部细节。B(下图左一), C(下图左二)。   
![](img/illustrations_48-2_crop0.jpg) 
![](img/FS4WJD6UAAA_BaG_crop0.jpg) 


---  

## 原画  (1984-08)  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM7u9KaMAUCOOh.jpg"/> 
](https://pbs.twimg.com/media/FTM7u9KaMAUCOOh?format=jpg&name=large)  

Cat's Eye 18卷版第11卷封面。   
Cat's Eye 18卷版第11卷108页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)；18卷版第11卷封面；18卷版第11卷第108页。    
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527630390835683329)   

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/illustrations_51_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM7u9KaMAUCOOh.jpg"/> 
](https://pbs.twimg.com/media/FTM7u9KaMAUCOOh?format=jpg&name=large) 

细节：  

- 手部的对比。对比B(下图左一)、C(下图左二)。  
![](img/illustrations_51_crop0.jpg) 
![](img/FTM7u9KaMAUCOOh_crop0.jpg) 

---  

## 复制原稿/2 (B2) /Cat's♥Eye  (1984-08?)
![](img/B2_fukuseigenko_02_thumb.jpg)  
Cat's Eye 18卷版"最后的决战"第63页。

[日文商品链接 複製原稿/2 (B2) /キャッツ♥アイ](https://edition-88.com/products/catseye-duplicateｍanuscript2-b2)及描述：  
“尺寸：B2（长728mmx宽515mm），  
胶印，  
高档纸 “   

[英文商品链接 无]()   

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: Cat's Eye 18卷版"最后的决战"第63页；  
C: [现场实拍 - Twitter](https://twitter.com/momotarou9/status/1527115706772312064)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/B2_fukuseigenko_02_thumb.jpg) 
![](img/ce_18_063_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTFnosdUAAAjpO3.jpg"/> 
](https://pbs.twimg.com/media/FTFnosdUAAAjpO3?format=jpg&name=large) 

细节：   

- 原稿边缘出现了2种图标：  
    - (图标1)左侧空白处竖向上部、下部各有一个如下图标。  
        ![](img/B2_fukuseigenko_02_mark1.jpg)  
        我猜，这两个图标之间的距离表示印刷时不能被裁掉的内容(下图左一)。例如：  
        - 上部的该图标齐于该图中对话框内文字的上边沿。如果印刷时上部剪裁时越过了该图标，则对话框内文字会显示不完整。  
        - 下部的该图标裁减的内容与B(下图左二)中的下边沿一致。  
        ![](img/B2_fukuseigenko_02_mark2_function.jpg) 
        ![](img/ce_18_063_thumb200.jpg)  
    - (图标2) 下部空白处一个如下的图标。暂时不知道该图标的作用是什么[疑问]。    
        ![](img/B2_fukuseigenko_02_mark2.jpg)  

- A中左上角的笔迹：  
    - 1)外边缘不整齐。我猜，可能因为作者预料到(印刷时)这里一定会被剪裁掉;  
    - 2)有铅笔的水平、竖直直线。我猜，这是作者打草稿时的笔迹；  
    ![](img/B2_fukuseigenko_02_crop0.jpg)   

- 对话框：  
    - (下图左一)对话框黑色边线内侧有白颜料涂抹的痕迹。不知道这是为什么[疑问]；  
    - (下图左二)对白文字是打印后贴上的；    
    ![](img/B2_fukuseigenko_02_crop1.jpg) 
    ![](img/B2_fukuseigenko_02_crop2.jpg)   

- 画面左上角，关于聚焦线条和黑色叶子：  
    - A(下图左一)能看到黑色叶子上的聚焦线条的痕迹。（但无法确定是先画的黑色叶子，还是先画的聚焦线条[疑问]）；  
    - B(下图左二)看不到上述细节；    
    ![](img/B2_fukuseigenko_02_crop3.jpg) 
    ![](img/ce_18_063_crop3.jpg)   

- A(下图左一)中蓝线标记处有白色涂抹的痕迹。蓝线处似乎有拼接的痕迹(下图左二、左三)。我猜，白色涂抹是为了掩盖拼接的痕迹。  
![](img/B2_fukuseigenko_02_crop4.jpg) 
![](img/B2_fukuseigenko_02_crop7.jpg) 
![](img/B2_fukuseigenko_02_crop8.jpg)  
    A(下图左一列)与B(下图左二列)的白色涂抹痕迹处的对比：  
    - 小瞳的头发尖被截断：  
    ![](img/B2_fukuseigenko_02_crop5.jpg) 
    ![](img/ce_18_063_crop5.jpg)   
    - 白色涂抹处露出的黑色：  
    ![](img/B2_fukuseigenko_02_crop6.jpg) 
    ![](img/ce_18_063_crop6.jpg)   

- A(下图左一列)头发的线条和墨色有深浅之分，B(下图左二列)中看不到该区别。  
    - 墨色浅。例如：头发轮廓内部的着色（似乎是用粗线条软笔(毛笔？)）；  
    - 墨色重。例如：头发边沿的线条、细发丝。  
    ![](img/B2_fukuseigenko_02_crop9.jpg)  ![](img/ce_18_063_crop9.jpg)   
    ![](img/B2_fukuseigenko_02_crop18.jpg) ![](img/ce_18_063_crop18.jpg)   

- A(下图左一列)中能看到网点纸作为面部阴影贴在画上，B(下图左二列)中看不到这个瑕疵。所以，制作流程应该是先画头发线条、头发着色，然后贴上网点阴影作为面部阴影。    
    ![](img/B2_fukuseigenko_02_crop10.jpg) ![](img/ce_18_063_crop10.jpg)  
    ![](img/B2_fukuseigenko_02_crop15.jpg) ![](img/ce_18_063_crop15.jpg)  

- A(下图左一)中(泪的右腿)用墨色着色，能区分出与左腿(网点阴影)的边界。B(下图左二)中无法区分两腿的边界。  
![](img/B2_fukuseigenko_02_crop14.jpg) 
![](img/ce_18_063_crop14.jpg)  

- A(下图左一)中能看出：网点纸上，除了黑点之外，网点纸本身有一定的灰度。B(下图左二)中只能看到网点纸上的黑点，而看不到网点纸本身的灰度。  
![](img/B2_fukuseigenko_02_crop16.jpg) 
![](img/ce_18_063_crop16.jpg)  

- 背景树木。A(下图左一)能看出笔触和黑色的层次，B(下图左二)中没有这些效果。    
![](img/B2_fukuseigenko_02_crop17.jpg) 
![](img/ce_18_063_crop17.jpg)  

- A(下图左一列)和B(下图左二列)似乎有不一致的地方：  
    - A腹部的网点阴影过渡突兀，反而B的腹部的网点阴影过渡均匀：  
![](img/B2_fukuseigenko_02_crop11.jpg) ![](img/ce_18_063_crop11.jpg)  
    - 背部的网点阴影：  
![](img/B2_fukuseigenko_02_crop12.jpg) ![](img/ce_18_063_crop11.jpg)  
    - 臀部边缘有明显的白色边线，但B中几乎看不到：  
![](img/B2_fukuseigenko_02_crop13.jpg) ![](img/ce_18_063_crop13.jpg)  



---  

## 原画(约1984-08)  
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVcY69VUAAX-G7.jpg"/> 
](https://pbs.twimg.com/media/FiVcY69VUAAX-G7?format=jpg&name=large)  
Cat's Eye 18卷版第18卷“永远的牵绊”第160页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;  
B: 18卷版第18卷“永远的牵绊”第160页。  
C: [现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595783242321256448)      

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_18_160_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVcY62UYAAyqAN.jpg"/> 
](https://pbs.twimg.com/media/FiVcY62UYAAyqAN?format=jpg&name=large) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVcY69VUAAX-G7.jpg"/> 
](https://pbs.twimg.com/media/FiVcY69VUAAX-G7?format=jpg&name=large) 

细节：  

- C的评论：  
"道具の進化と言えばそれまでだが、ペンのような道具の使い熟しとともに、ある程度のシステム管理も漫画家自身がやらなければならなくなっている。"  
(这是一种工具的演变，但伴随着熟练使用笔等工具，漫画家们自己现在也必须在一定程度上管理这个系统。)  

- C中有3个如下图标。[疑问]这是什么意思？  
![](img/B2_fukuseigenko_02_mark1.jpg)  

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  

---  

## 原画(约1984-08)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVcY7kUAAA_LJI.jpg"/> 
](https://pbs.twimg.com/media/FiVcY7kUAAA_LJI?format=jpg&name=large)  
Cat's Eye 18卷版第18卷“永远的牵绊”第161页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;  
B: 18卷版第18卷“永远的牵绊”第161页。  
C:[现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595783242321256448)  
D:[现场实拍 - Twitter](https://twitter.com/hojo_official/status/1525002971020308481)  
E:[现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595792977065955328)  

A(下图左一)，B(下图左二), C(下图左三)， D(下图左四)， E(下图左五)：  
![](img/not_given.jpg) 
![](img/ce_18_161_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVcY7kUAAA_LJI.jpg"/> 
](https://pbs.twimg.com/media/FiVcY7kUAAA_LJI?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FSni7OzUEAAlyZ5.jpg"/>
](https://pbs.twimg.com/media/FSni7OzUEAAlyZ5?format=jpg&name=4096x4096) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVk1OsUoAUcCk-.jpg"/> 
](https://pbs.twimg.com/media/FiVk1OsUoAUcCk-?format=jpg&name=large)  

细节：  

- C的评论：  
"道具の進化と言えばそれまでだが、ペンのような道具の使い熟しとともに、ある程度のシステム管理も漫画家自身がやらなければならなくなっている。"  
(这是一种工具的演变，但伴随着熟练使用笔等工具，漫画家们自己现在也必须在一定程度上管理这个系统。)

- C中(下图左二)可看出皮鞋上的高光是用白色颜料画在网点纸上。对比B(下图左一)。    
![](img/ce_18_161_crop0.jpg) 
![](img/FSni7OzUEAAlyZ5_crop0.jpg)  

- 云朵的效果逼真。B(下图左一)对比C(下图左二)。    
![](img/ce_18_161_crop0.jpg) 
![](img/FSni7OzUEAAlyZ5_crop1.jpg)  

- 交错排线光滑、有力。B(下图左一)对比C(下图左二)。    
![](img/ce_18_161_crop2.jpg) 
![](img/FSni7OzUEAAlyZ5_crop2.jpg)  

- 交错排线的起始笔触几乎重合(下图左三红线、粉线)、整齐(下图左三蓝线)。B(下图左一)对比C(下图左二)。    
![](img/ce_18_161_crop3.jpg) 
![](img/FSni7OzUEAAlyZ5_crop3.jpg) 
![](img/FSni7OzUEAAlyZ5_crop3_fig.jpg)  

- C中下部作者的画。这是CE连载时的最后一话时作者的心情。[疑问]其中的手写文字是：...（待识别）。[疑问]左侧的三个人物是谁？[疑问]最左侧的人物像是女性，她是谁？        
![](img/FSni7OzUEAAlyZ5_crop4.jpg) 
![](img/FSni7OzUEAAlyZ5_crop5.jpg)  


---  

## 复制原稿/3 (B2) /Cat's♥Eye  (1984-09.19)

![](img/B2_fukuseigenko_03_thumb.jpg)  
18卷版第18卷"再一次恋爱之卷"第162页「Collection No.87」  

[商品链接 複製原稿/3 (B2) /キャッツ♥アイ](https://edition-88.com/products/catseye-duplicateｍanuscript3-b2)及描述：  
"尺寸：B2（长728mmx宽515mm），  
胶印，  
高档纸"  

[英文版商品链接 无]()  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第18卷"再一次恋爱之卷"第162页「Collection No.87」；  
C: [现场实拍 - Twitter](https://twitter.com/33Aquamarine3/status/1597176242033549313) 

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/B2_fukuseigenko_03_thumb.jpg) 
![](img/ce_18_162_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FipPUBcaYAAldfv.jpg"/> 
](https://pbs.twimg.com/media/FipPUBcaYAAldfv?format=jpg&name=large) 

细节：   

- A下部有大字号的蓝色Cat's Eye, B中左上角有小字号的Cat's Eye。     

- 爱、瞳、泪最后的造型。  
![](img/B2_fukuseigenko_03_crop1.jpg) 
![](img/B2_fukuseigenko_03_crop2.jpg) 
![](img/B2_fukuseigenko_03_crop3.jpg)  

- A(下图左一)左眼有涂抹白色颜料，这在降低gamma值后B(下图左二)更容易看出。对比B(下图左三)：  
![](img/B2_fukuseigenko_03_crop0.jpg) 
![](img/B2_fukuseigenko_03_crop0_gamma.jpg) 
![](img/ce_18_162_crop0.jpg)     
  
- A(下图左一)交叉排线表示黑色衣服的高光。对比B(下图左二)：  
![](img/B2_fukuseigenko_03_crop4.jpg) 
![](img/ce_18_162_crop4.jpg)     

-  A(下图左一)中泪的衣服边缘处有白线，B(下图左二)中没有：  
![](img/B2_fukuseigenko_03_crop5.jpg) ![](img/ce_18_162_crop5.jpg)    
![](img/B2_fukuseigenko_03_crop6.jpg) ![](img/ce_18_162_crop6.jpg)    

- A(下图左一)中泪的裤子的颜色渐变更均匀。对比B(下图左二)：  
![](img/B2_fukuseigenko_03_crop7.jpg) ![](img/ce_18_162_crop7.jpg)  

- 浅谷的刘海直接用浅墨色画出。    
![](img/B2_fukuseigenko_03_crop8.jpg) ![](img/ce_18_162_crop8.jpg)  

- A(下图左一)能看出木崎的脸颊的线条不够平滑(如红箭头所示)，这在B(下图左二)中因为尺寸不够大而看不出。    
![](img/B2_fukuseigenko_03_crop9.jpg)  ![](img/ce_18_162_crop9.jpg)   

- 科长衣服的纹理是手工画的，而不是用网点纸。  
![](img/B2_fukuseigenko_03_crop10.jpg)  ![](img/ce_18_162_crop10.jpg)   

- A(下图左一列)浅谷和老鼠的脸颊处有白色涂抹的痕迹，这在降低gamma值后B(下图左二列)更容易看出。  
![](img/B2_fukuseigenko_03_crop11.jpg) 
![](img/B2_fukuseigenko_03_crop11_gamma.jpg)  
![](img/B2_fukuseigenko_03_crop12.jpg) 
![](img/B2_fukuseigenko_03_crop12_gamma.jpg)  


---  


## 複製原稿11 (B4) /Cat's♥Eye  （推测为1984年底～1985年初）  
![](img/hukuseigenkou1_B4-11-eg_thumb.jpg)  
完全版第18卷第158話“再一次恋爱之卷”的扉页（第？页) 。  
18卷版第18卷第130话“再一次恋爱之卷”的扉页（第164页)。  

“再一次恋爱”发表于週刊少年Jump 1985年6号。  

[日文商品链接 无]()      
[英文商品链接Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第18卷"再一次恋爱之卷"第164页「Collection No.87」；  
C: [商品实拍 - Twitter](https://twitter.com/aogiriakirabook/status/1526918441596522499) 

A(下图左一)，B（下图左二）， C（下图左三）：  
![](img/hukuseigenkou1_B4-11-eg_thumb.jpg) 
![](img/ce_18_164_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTC0MhJUAAAtpUb.jpg"/> 
](https://pbs.twimg.com/media/FTC0MhJUAAAtpUb?format=jpg&name=large) 

细节：   

- A的左上、右上、右下角分别有一个如下图标。  
    ![](img/B2_fukuseigenko_02_mark1.jpg)  

- A的上下左右四个角分别有如下草稿痕迹。  
![](img/hukuseigenkou1_B4-11-eg_crop0.jpg)   

- A黑色背景边缘粗犷的笔触(下图左一)。  
![](img/hukuseigenkou1_B4-11-eg_crop1.jpg)  

- A黑色背景墨色有涂抹的痕迹(下图左一、左二)。B的该位置为文字（下图左三）  
![](img/hukuseigenkou1_B4-11-eg_crop2.jpg) 
![](img/hukuseigenkou1_B4-11-eg_crop3.jpg) 
![](img/ce_18_164_crop2.jpg)  

- A(下图左一)比B（下图左二）喷洒了更多的白色颜料。       
![](img/hukuseigenkou1_B4-11-eg_crop4.jpg) 
![](img/ce_18_164_crop4.jpg)  

- A(下图左一)的网点阴影的痕迹，喷洒的白色墨点(下图左一红箭头)正好有一半位于网点纸上。对比B（下图左二）。       
![](img/hukuseigenkou1_B4-11-eg_crop5.jpg) 
![](img/ce_18_164_crop5.jpg)  

- A(下图左一列)头发着色有深有浅，呈层次感；且笔迹光滑、有力！对比B（下图左二列）。       
![](img/hukuseigenkou1_B4-11-eg_crop6.jpg) ![](img/ce_18_164_crop6.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop8.jpg) ![](img/ce_18_164_crop8.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop9.jpg) ![](img/ce_18_164_crop9.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop10.jpg) ![](img/ce_18_164_crop10.jpg)  

- A(下图左一)喷洒的白色墨点在白色背景的衬托下变成了银灰色。对比B（下图左二）。       
![](img/hukuseigenkou1_B4-11-eg_crop7.jpg) 
![](img/ce_18_164_crop7.jpg)  

- A(下图左一列)头发轮廓边缘的白色发丝是最后加上的。这在降低gamma值后(下图左二列)能更清晰地看出笔触。对比B（下图左三列）。    
![](img/hukuseigenkou1_B4-11-eg_crop11.jpg) 
![](img/hukuseigenkou1_B4-11-eg_crop11_gamma.jpg) 
![](img/ce_18_164_crop11.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop12.jpg) 
![](img/hukuseigenkou1_B4-11-eg_crop12_gamma.jpg) 
![](img/ce_18_164_crop12.jpg)  

- A(下图左一列)眼部的细节。对比B（下图左二列）。   
![](img/hukuseigenkou1_B4-11-eg_crop13.jpg) 
![](img/ce_18_164_crop13.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop14.jpg) 
![](img/ce_18_164_crop14.jpg)  

- A(下图左一列)嘴部的细节。下嘴唇上的高光是在网点纸上擦出的。对比B（下图左二列）。   
![](img/hukuseigenkou1_B4-11-eg_crop15.jpg) 
![](img/ce_18_164_crop15.jpg)  

- A(下图左一列)围巾轮廓边缘的毛发是用白色颜料画的。对比B（下图左二列）。   
![](img/hukuseigenkou1_B4-11-eg_crop16.jpg) 
![](img/ce_18_164_crop16.jpg)  

- A中多处黑色区域(下图左一)似乎有文字的笔迹：   
![](img/ce_18_164_crop17.jpg)  
这些笔迹(下图左一列)疑似“北条”、“手”字样，增加图片的exposure值后更容易看到这些笔迹(下图左二列)：  
![](img/hukuseigenkou1_B4-11-eg_crop17.jpg) 
![](img/hukuseigenkou1_B4-11-eg_crop17_exposure.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop18.jpg) 
![](img/hukuseigenkou1_B4-11-eg_crop18_exposure.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop19.jpg) 
![](img/hukuseigenkou1_B4-11-eg_crop19_exposure.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop20.jpg) 
![](img/hukuseigenkou1_B4-11-eg_crop20_exposure.jpg)  

- A(下图左一列)俊夫领带处的高光是交叉排线。对比B（下图左二列）。   
![](img/hukuseigenkou1_B4-11-eg_crop21.jpg) 
![](img/ce_18_164_crop21.jpg)  

- A(下图左一列)俊夫裤子上的高光全部是贴上的。对比B（下图左二列）。   
![](img/hukuseigenkou1_B4-11-eg_crop22.jpg) 
![](img/ce_18_164_crop22.jpg)  

- A(下图左一列)俊夫鞋上的着色有深有浅，且线条简练！对比B（下图左二列）。  
![](img/hukuseigenkou1_B4-11-eg_crop23.jpg) 
![](img/ce_18_164_crop23.jpg)  
![](img/hukuseigenkou1_B4-11-eg_crop24.jpg) 
![](img/ce_18_164_crop24.jpg)  

- A(下图左一、左二)鞋的着色的特点是：线条简练、留有大面积空白。    
![](img/hukuseigenkou1_B4-11-eg_crop23.jpg) 
![](img/hukuseigenkou1_B4-11-eg_crop24.jpg)  
这种着色特点在时装设计绘画中常见, 如下图（源自：《David Downton Portraits Of The World's Most Stylish Women》, David Downton, 2015；《Fashion Illustration Art: How to Draw Fun Fabulous Figures, Trends and Styles》, Jennifer Lilya, 2014）。  
![](img/David_Downton0.jpg) 
![](img/David_Downton1.jpg) 
![](img/Fashion_Illustration_Art0.jpg) 
![](img/Fashion_Illustration_Art1.jpg) 
![](img/Fashion_Illustration_Art2.jpg) 
![](img/Fashion_Illustration_Art3.jpg) 


---  

## 原画 (推测为1984年底～1985年初)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9ps0YWYAIvFCY.jpg"/> 
](https://pbs.twimg.com/media/FS9ps0YWYAIvFCY?format=jpg&name=large)  
Cat's Eye 18卷版第18卷“再一次恋爱”165页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源： 第18卷“再一次恋爱”165页,   
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526555056224583683)     

A(下图左一)，B(下图左二), C(下图左三、左四)：  
![](img/not_given.jpg) 
![](img/ce_18_165_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9ps0YWYAIvFCY.jpg"/> 
](https://pbs.twimg.com/media/FS9ps0YWYAIvFCY?format=jpg&name=large) 
[<img title="C分镜备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9pswTWYAEtgN2.jpg"/> 
](https://pbs.twimg.com/media/FS9pswTWYAEtgN2?format=jpg&name=large) 

细节：  

- C中有如下图标。[疑问]这些标记是什么意思？    
![](img/B2_fukuseigenko_02_mark2.jpg) 
![](img/B2_fukuseigenko_02_mark1.jpg)  

- C中(下图左二)雕像轮廓处用白颜料勾线，目的是将前景与背景人物分离。白色勾线在烟斗处不连续。对比B中(下图左一)的效果。    
![](img/ce_18_165_crop0.jpg) 
![](img/FS9ps0YWYAIvFCY_crop0.jpg) 

- C中头发涂黑区域在画面边缘处不规则。  
![](img/FS9ps0YWYAIvFCY_crop1.jpg) 

- C中有一个对话框内有疑似草稿的线条。[疑问]这线条的功能是什么？      
![](img/FS9ps0YWYAIvFCY_crop2.jpg) 


---  

## 原画 (推测为1984年底～1985年初)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9puKsXoAMiWC6.jpg"/> 
](https://pbs.twimg.com/media/FS9puKsXoAMiWC6?format=jpg&name=large)  
Cat's Eye 18卷版第18卷“再一次恋爱”166页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源： 第18卷“再一次恋爱”166页,   
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526555056224583683)     

A(下图左一)，B(下图左二), C(下图左三、左四)：    
![](img/not_given.jpg) 
![](img/ce_18_166_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9puKsXoAMiWC6.jpg"/> 
](https://pbs.twimg.com/media/FS9puKsXoAMiWC6?format=jpg&name=large) 
[<img title="C分镜备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9puF3WUAMolxq.jpg"/> 
](https://pbs.twimg.com/media/FS9puF3WUAMolxq?format=jpg&name=large) 

细节：

- 网点阴影。对比B(下图左一)、C(下图左二)。  
![](img/ce_18_166_crop0.jpg) 
![](img/FS9puKsXoAMiWC6_crop0.jpg) 

- C中panel边框之间有白色涂抹的痕迹。  
![](img/FS9puKsXoAMiWC6_crop1.jpg) 
![](img/FS9puKsXoAMiWC6_crop2.jpg) 
![](img/FS9puKsXoAMiWC6_crop3.jpg) 


---  

## 原画 (推测为1984年底～1985年初)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9puWbX0AApizW.jpg"/> 
](https://pbs.twimg.com/media/FS9puWbX0AApizW?format=jpg&name=large)  
Cat's Eye 18卷版第18卷“再一次恋爱”167页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源： 第18卷“再一次恋爱”167页,   
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526555056224583683)     

A(下图左一)，B(下图左二), C(下图左三、左四)：    
![](img/not_given.jpg) 
![](img/ce_18_167_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9puWbX0AApizW.jpg"/> 
](https://pbs.twimg.com/media/FS9puWbX0AApizW?format=jpg&name=large) 
[<img title="C分镜备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9puPmX0AUtSdu.jpg"/> 
](https://pbs.twimg.com/media/FS9puPmX0AUtSdu?format=jpg&name=large) 

细节：  

- 贴上的文字上面又贴上了一列文字。     
![](img/FS9puWbX0AApizW_crop0.jpg) 

- 灯光是用白色颜料画的。B(下图左一)对比C(下图左二)。    
![](img/ce_18_167_crop1.jpg) 
![](img/FS9puWbX0AApizW_crop1.jpg) 

- 云的着色用了多层网点纸。B(下图左一)对比C(下图左二)。  
![](img/ce_18_167_crop2.jpg) 
![](img/FS9puWbX0AApizW_crop2.jpg) 

- C中的白色有涂抹的痕迹。我猜，这是车灯灯火通明的效果。B(下图左一)对比C(下图左二)。  
![](img/ce_18_167_crop3.jpg) 
![](img/FS9puWbX0AApizW_crop3.jpg) 

- C中右下角有手写文字：  
![](img/FS9puWbX0AApizW_crop4.jpg) 

- 路灯弯曲处的线条不平行。    
![](img/FS9puWbX0AApizW_crop5.jpg) 



---  

## 原画  （推测为1984年底～1985年初）
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTGutg6UsAUN7OY.jpg"/> 
](https://pbs.twimg.com/media/FTGutg6UsAUN7OY?format=jpg&name=large)   
Cat's Eye 18卷版第18卷“再一次恋爱”196～197页。  

“再一次恋爱”发表于週刊少年Jump 1985年6号。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 18卷版第18卷“再一次恋爱”196～197页。  
C: [现场实拍 - Twitter](https://twitter.com/Rosen_Lizard_/status/1527193851596656640)  
D：[现场实拍 - Twitter](https://twitter.com/N7Obo2E2S3a3Xw4/status/1525415306138595329)  

A(下图左一)，B(下图左二), C(下图左三、左四)：  
![](img/not_given.jpg) 
![](img/ce_18_196_197_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTGutg6UsAUN7OY.jpg"/> 
](https://pbs.twimg.com/media/FTGutg6UsAUN7OY?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FStdHpvVIAAjLVF.jpg"/> 
](https://pbs.twimg.com/media/FStdHpvVIAAjLVF?format=jpg&name=large) 


细节：   

- C中对话框文字的剪纸的颜色不同。  
![](img/FTGutg6UsAUN7OY_crop1.jpg)  

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  



---  

## 複製原稿12 (B4) /Cat's♥Eye  (推测为1984年底～1985年初)  
![](img/hukuseigenkou12_B4_thumb.jpg)  
完全版第18卷第158話“再一次恋爱之卷”第?页  
18卷版第18卷第130话"再一次恋爱之卷"第198页。    

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript12-b4)及描述：  
“「完全版第18卷第158话 再一次恋爱」的复制原稿。”  
  
[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第18卷第130话"再一次恋爱之卷"第198页；  

A(下图左一)，B（下图左二）：  
![](img/hukuseigenkou12_B4_thumb.jpg) 
![](img/ce_18_198_thumb.jpg)   

细节：   

- A(下图左一)泪的腿部阴影有蓝色的线条。对比B(下图左二)。     
![](img/hukuseigenkou12_B4_crop0.jpg) 
![](img/ce_18_198_crop0.jpg)   

- A(下图左一)瞳的头发的着色。对比B(下图左二)。     
![](img/hukuseigenkou12_B4_crop1.jpg) 
![](img/ce_18_198_crop1.jpg)   

- A(下图左一)黑色的领带上覆盖了网点阴影。对比B(下图左二)。     
![](img/hukuseigenkou12_B4_crop2.jpg) 
![](img/ce_18_198_crop2.jpg)   

- A(下图左一)裤子上黑色高光的画法。对比B(下图左二)。     
![](img/hukuseigenkou12_B4_crop3.jpg) 
![](img/ce_18_198_crop3.jpg)   
高光处的墨色浅，旁边整块黑色区域的墨色重。我猜，这不是一次画的，很可能不是一个人画的--高光处的墨色由一个人画，旁边整块黑色区域的墨色由另一个人画。  

- A中泪的眼部阴影的网点纸痕迹明显。  

- 泪的眼部细节。鼻梁处(下图左二)的线很细，且不是一条线。    
![](img/hukuseigenkou12_B4_crop4.jpg) 
![](img/hukuseigenkou12_B4_crop5.jpg)   

- A(下图左一)嘴部的细节。嘴角的痣画为小凸起，而不是一个黑点。对比B(下图左二)。     
![](img/hukuseigenkou12_B4_crop6.jpg) 
![](img/ce_18_198_crop6.jpg)   

- A(下图左一)线条流畅，但这是两条线，而不是一条。对比B(下图左二)。    
![](img/hukuseigenkou12_B4_crop7.jpg) 
![](img/ce_18_198_crop7.jpg)   


---  

## 原画 (推测为1984年底～1985年初)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBMoQbaMAEYlGs.jpg"/> 
](https://pbs.twimg.com/media/FTBMoQbaMAEYlGs?format=jpg&name=large)  
18卷版第18卷“再一次恋爱”第204-205页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 18卷版第18卷“再一次恋爱”第204-205页。  
C: [现场实拍 - Twitter](https://twitter.com/eijikita/status/1526804554452389888)    
D[现场实拍1 - Twitter](https://twitter.com/chasadamaru7/status/1528255772182089729)  

A(下图左一)，B(下图左二), C(下图左三)， D（下图左四）：  
![](img/not_given.jpg) 
![](img/ce_18_204_205_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBMoQbaMAEYlGs.jpg"/> 
](https://pbs.twimg.com/media/FTBMoQbaMAEYlGs?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTV0hLYagAEsK0M.jpg"/> 
](https://pbs.twimg.com/media/FTV0hLYagAEsK0M?format=jpg&name=large) 

细节：   

- 白色浪花使用白色颜料。对比B(下图左一)，C(下图左二)，C降低gamma值(下图左三)。  
![](img/ce_18_204_205_crop0.jpg) 
![](img/FTBMoQbaMAEYlGs_crop0.jpg) 
![](img/FTBMoQbaMAEYlGs_crop0_gamma.jpg) 

- C中这个镜头里的确是有戒指的。即，原画中这个镜头里是有戒指的。  
![](img/FTBMoQbaMAEYlGs_crop1.jpg) 

- D中(下图左二)能看到头发墨色不均匀。对比B(下图左一)。   
![](img/ce_18_204_205_crop2.jpg) 
![](img/FTV0hLYagAEsK0M_crop2.jpg) 

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  



---  

## 复制原稿/4 (B2) /Cat's♥Eye  (推测为1984年底～1985年初)
![](img/B2_fukuseigenko_04_thumb.jpg)  
CE（18卷版） 18卷 再一次恋爱之卷第206-207页。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript4-b2)及描述：  
“CAT'S♥EYE 最終話、最后的对页是用高清的胶印重制的。   
尺寸：B2（长728mmx宽515mm），胶印，高档纸”  

[英文版商品链接 无]()  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第18卷第130话"再一次恋爱之卷"第206页；  
C: [现场实拍 - Twitter](https://twitter.com/dasuke05/status/1526046389607624704)  
D: [现场实拍 - Twitter](https://twitter.com/Draichi_/status/1528338113302790145)  
E: [现场实拍 - Twitter](https://twitter.com/MituruSouda/status/1527972554526568448)  
F: [现场实拍 - Twitter](https://twitter.com/33Aquamarine3/status/1597176242033549313)  
G: [现场实拍 - Twitter](https://twitter.com/eijikita/status/1526804554452389888)  

A(下图左一)，B（下图左二）, C（下图左三）, D（下图左四）, E（下图左五）, F（下图左六）, G（下图左七）：  
![](img/B2_fukuseigenko_04_thumb.jpg) 
![](img/ce_18_206_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2bGXhVUAAkdxc.jpg"/> 
](https://pbs.twimg.com/media/FS2bGXhVUAAkdxc?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTW_aEuaMAA4B66.jpg"/> 
](https://pbs.twimg.com/media/FTW_aEuaMAA4B66?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FTRy2zFaMAIhtIq.jpg"/> 
](https://pbs.twimg.com/media/FTRy2zFaMAIhtIq?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/FipPUn5aEAApWqZ.jpg"/> 
](https://pbs.twimg.com/media/FipPUn5aEAApWqZ?format=jpg&name=large) 
[<img title="G备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBMoQbaMAEYlGs.jpg"/> 
](https://pbs.twimg.com/media/FTBMoQbaMAEYlGs?format=jpg&name=large) 

细节：   

- A中出现了如下标记：  
![](img/B2_fukuseigenko_02_mark1.jpg) ![](img/B2_fukuseigenko_02_mark2.jpg)  
[疑问]红色标记是什么意思？  

- A中背景黑色山脉的墨色不均匀，有反光的效果。但不影响B中的效果。  

- A(下图左一)中领带似乎有反光。对比B(下图左二):  
![](img/B2_fukuseigenko_04_crop0.jpg) 
![](img/ce_18_206_crop0.jpg)   

- A(下图左一)中白色竖线，应该是左右页的分隔:  
![](img/B2_fukuseigenko_04_crop1.jpg)   



---  

## 官网报道中的图片02 (1984-12)  

![](img/cat_s-eye_tenji_96_02_thumb.jpg)  
Cat's Eye 18卷版第13卷封面。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 18卷版第13卷封面；  

A(下图左一)，B（下图左二）：  
![](img/cat_s-eye_tenji_96_02_thumb.jpg) 
![](img/ce_13_000_thumb.jpg)  

细节：   

- 金发猫眼的虹膜似乎是绿色或蓝色：  
![](img/cat_s-eye_tenji_96_02_crop0.jpg)  

- 衣服褶皱的着色逼真。  
![](img/cat_s-eye_tenji_96_02_crop1.jpg)  

- 角色在柱子上有倒影。  
![](img/cat_s-eye_tenji_96_02_crop2.jpg)  


---  

## 「Cat's♥Eye」 版画4 (1985)
![](img/hanga_cat04_thumb.jpg)  
Jump Comics 1985年 第18卷封面的版画。   

[商品链接](https://edition-88.com/products/catseye-hanga4)及描述：  
“这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  
技法：giclée  
版画用中性紙”  

[商品链接 Cat's Eye, Art Print #4](https://edition88.com/products/catseye-hanga4)及描述：  
“国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），以精致地表现色彩和纹理。  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  
国际版在美术纸上采用混合媒体（Giclée和UV）”  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 北条司イラスト集(北条司Illustrations)；  
C: [现场实拍 - Twitter](https://twitter.com/Ryo_tatuya_shin/status/1528314778774835202) 
D: [现场实拍 - Twitter](https://twitter.com/33Aquamarine3/status/1597176242033549313) 

A(下图左一)，B（下图左二）,C(下图左三)、D（下图左四）：  
![](img/hanga_cat04_thumb.jpg) 
![](img/illustrations_039_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTWqFclacAAoVA6.jpg"/> 
](https://pbs.twimg.com/media/FTWqFclacAAoVA6?format=jpg&name=4096x4096) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FipPUUuaAAAFmhv.jpg"/> 
](https://pbs.twimg.com/media/FipPUUuaAAAFmhv?format=jpg&name=4096x4096) 

细节：   

- A(下图左一)中背景玻璃窗边框几乎是直线，其在B（下图左二）中呈波浪线：  
![](img/hanga_cat04_crop0.jpg) 
![](img/illustrations_039_crop0.jpg)   

- A(下图左一)中头发的细节，对比B（下图左二）中的效果：  
![](img/hanga_cat04_crop1.jpg) 
![](img/illustrations_039_crop1.jpg)   

- A(下图左一)中头发的着色的凹凸不平，对比B（下图左二）中的效果：  
![](img/hanga_cat04_crop2.jpg) 
![](img/illustrations_039_crop1.jpg)   

- A(下图左一)中衣服的着色的凹凸不平，对比B（下图左二）中的效果：  
![](img/hanga_cat04_crop6.jpg) 
![](img/illustrations_039_crop6.jpg)   

- A中瞳(下图左一)、俊夫、泪的侧脸下颌骨末端处涂白色颜料，这在降低gamma值后(下图左一)更容易看出。对比B（下图左二）中的效果：  
![](img/hanga_cat04_crop3.jpg) 
![](img/hanga_cat04_crop3_gamma.jpg) 
![](img/illustrations_039_crop1.jpg)   

- A(下图左一)中衣服褶皱的着色，对比B（下图左二）中的效果：  
![](img/hanga_cat04_crop5.jpg) 
![](img/illustrations_039_crop5.jpg)   

- A(下图左一)中泪左胳膊的线条的一侧(红色箭头所示)比另一侧不平滑。对比B（下图左二）中的效果：  
![](img/hanga_cat04_crop7.jpg) 
![](img/illustrations_039_crop7.jpg)   

- A中泪的虹膜为蓝色：  
![](img/hanga_cat04_crop4.jpg)  

- C中(下图左一)展示了很有趣的一点：俊夫头部周围有一个圆形的拼接痕迹。A、B均没有该痕迹。C降低gamma值(下图左二)后容易看出：    
![](img/FTWqFclacAAoVA6_crop2.jpg) 
![](img/FTWqFclacAAoVA6_crop2_gamma.jpg)  
A(下图左一列)对比C(下图左二列)中的细节：   
![](img/hanga_cat04_crop8.jpg) ![](img/FTWqFclacAAoVA6_crop8.jpg)  
![](img/hanga_cat04_crop9.jpg) ![](img/FTWqFclacAAoVA6_crop9.jpg)  
![](img/hanga_cat04_crop10.jpg) ![](img/FTWqFclacAAoVA6_crop10.jpg)  



---  

## 「Cat's♥Eye」 版画3 (1985)
![](img/hanga_cat03_thumb.jpg)  
Cat's Eye 18卷版第14巻封面。    

[商品链接](https://edition-88.com/products/catseye-hanga3)及描述：  
“Cat'sEye 1985年第14巻封面的版画。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  
技法：giclée  
版画用中性紙”  

[商品链接 Cat's Eye, Art Print #3](https://edition88.com/products/catseye-hanga3)及描述：  
“国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），以精致地表现色彩和纹理。  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  
国际版在美术纸上采用混合媒体（Giclée和UV）”  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 北条司イラスト集(北条司Illustrations)；  

A(下图左一)，B（下图左二）：  
![](img/hanga_cat03_thumb.jpg) 
![](img/ce_14_000_thumb.jpg)    

细节：   

注：图中阳光透过云朵形成的条带光线被称为["Crepuscular Rays"](https://en.wikipedia.org/wiki/Crepuscular_rays)或["God Rays"](https://ownyourweather.com/crepuscular-rays/)。  

- A中Crepuscular Rays的下边线清晰、有颗粒感，上边线模糊。我猜，上色时采用条带遮挡颜料喷洒。      
![](img/hanga_cat03_crop0.jpg)  

- 眼部、嘴部的细节。虹膜为褐色。  
![](img/hanga_cat03_crop1.jpg)  
![](img/hanga_cat03_crop4.jpg)  

- A(下图左一)中头发的着色细节。对比B（下图左二）：  
![](img/hanga_cat03_crop2.jpg) 
![](img/ce_14_000_crop2.jpg)   

- A(下图左一)中的颜料白点，官网展示的A的局部侧视图显示了其凸起效果（下图左二）。  
![](img/hanga_cat03_crop3.jpg) 
![](img/hanga_cat03_crop3_2.jpg)   

- A(下图左一)中的颈部的阴影、衣服的阴影似乎不是黑色，二是褐色。  
![](img/hanga_cat03_crop5.jpg) 

- A(下图左一列)中胳膊、右腿、左腿透过衣服的效果，其颜色不同。对比B（下图左二列）：  
![](img/hanga_cat03_crop6.jpg) ![](img/ce_14_000_crop6.jpg)   
![](img/hanga_cat03_crop7.jpg) ![](img/ce_14_000_crop7.jpg)   
![](img/hanga_cat03_crop8.jpg) ![](img/ce_14_000_crop8.jpg)   

- A(下图左一)中衣服色彩绚丽。对比B（下图左二）：  
![](img/hanga_cat03_crop9.jpg) 
![](img/ce_14_000_crop9.jpg)   

- 角色姿态的线条(Action Lines)流畅。  
![](img/hanga_cat03_thumb.jpg) 
![](img/hanga_cat03_fl3.jpg) 
![](img/hanga_cat03_fl2.jpg) 
![](img/hanga_cat03_fl.jpg) 


---  

## 原画（作品创作年月不详）  

[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTcltLYaIAEODKc.jpg"/> 
](https://pbs.twimg.com/media/FTcltLYaIAEODKc?format=jpg&name=medium)  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。  
C: [现场实拍 - Twitter](https://twitter.com/garnetlynx777/status/1528732065419894784)     
D: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527630390835683329)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![](img/not_given.jpg) 
![](img/illustrations_47_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTcltLYaIAEODKc.jpg"/> 
](https://pbs.twimg.com/media/FTcltLYaIAEODKc?format=jpg&name=medium) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM7u8uakAAMwsS.jpg"/> 
](https://pbs.twimg.com/media/FTM7u8uakAAMwsS?format=jpg&name=large) 

细节：  

- 黄绿配色是一个不错的选择。  

- 用白色竖线虚化背景人物。对比B(下图左一)，C(下图左二)，D(下图左三)。  
![](img/illustrations_47_crop0.jpg) 
![](img/FTcltLYaIAEODKc_crop0.jpg) 
![](img/FTM7u8uakAAMwsS_crop0.jpg) 

- D中能清晰地看出画作表面的凹凸不平。   
![](img/FTM7u8uakAAMwsS_crop1.jpg) 

- 绿色裙子的着色。对比B(下图左一)，C(下图左二)，D(下图左三)。  
![](img/illustrations_47_crop2.jpg) 
![](img/FTcltLYaIAEODKc_crop2.jpg) 
![](img/FTM7u8uakAAMwsS_crop2.jpg) 

- D中绿色边线粗细不均。  
![](img/FTM7u8uakAAMwsS_crop3.jpg)  

- D中能看到拇指处有淡淡的褶皱。    
![](img/FTM7u8uakAAMwsS_crop4.jpg)  

- D中能看到浅色线条（下图红箭头所示）。[疑问]这表示褶皱的高光吗？  
![](img/FTM7u8uakAAMwsS_crop5.jpg)  

- D中能看到有些褶皱的墨色深（下图蓝箭头所示），有些褶皱的墨色浅（下图红箭头所示）。我猜，浅色褶皱是在裙子着色绿色之前画上的；深色褶皱是在裙子着色绿色之后画上的。      
![](img/FTM7u8uakAAMwsS_crop6.jpg)  



---  

## 原画  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM-9m7agAAWrQa.jpg"/> 
](https://pbs.twimg.com/media/FTM-9m7agAAWrQa?format=jpg&name=large)  
18卷版第？卷第？页。[疑问]不知道这是哪张图。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 18卷版第7卷26-27页。  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527633943000121344) 

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM-9m7agAAWrQa.jpg"/> 
](https://pbs.twimg.com/media/FTM-9m7agAAWrQa?format=jpg&name=large) 


细节：  

- C中(下图左一)能看到枪上的网点纸。这在降低gamma值后(下图左二)更容易看出。    
![](img/FTM-9m7agAAWrQa_crop0.jpg) 
![](img/FTM-9m7agAAWrQa_crop0_gamma.jpg) 


---  

## 原画  

[现场实拍 - Twitter](https://twitter.com/ichigomilk016/status/1594933107211534336)  

[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiJXNCvakAESZct.jpg"/> 
](https://pbs.twimg.com/media/FiJXNCvakAESZct?format=jpg&name=large) 

[疑问]不知道这是哪张图。  

细节：  

- 文字可能为"イ......!! ツツア"(译文：...（待完成）)  

---  

## 原画  
[现场实拍 - Twitter](https://twitter.com/oMJDmiknIQ1VtRE/status/1526037998558924800)  

[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2TcX0UcAAMGO5.jpg"/> 
](https://pbs.twimg.com/media/FS2TcX0UcAAMGO5?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2TcX5VsAE5oWN.jpg"/> 
](https://pbs.twimg.com/media/FS2TcX5VsAE5oWN?format=jpg&name=large)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2TcX2UcAAJYHf.jpg"/> 
](https://pbs.twimg.com/media/FS2TcX2UcAAJYHf?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2TcX_UUAU69uY.jpg"/> 
](https://pbs.twimg.com/media/FS2TcX_UUAU69uY?format=jpg&name=large) 



---  

## Cat's Eye板块结束的留影处  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVXJAvVIAEwLji.jpg"/> 
](https://pbs.twimg.com/media/FiVXJAvVIAEwLji?format=jpg&name=large) 

- Twitter上的[现场实拍](https://twitter.com/tohnomiyuki00/status/1595777478211305472)的拍照以及评论：  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVXGmkUAAIIAQw.jpg"/> 
](https://pbs.twimg.com/media/FiVXGmkUAAIIAQw?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVXJAwUAAAIqHP.jpg"/> 
](https://pbs.twimg.com/media/FiVXJAwUAAAIqHP?format=jpg&name=large)  
"一部を除き、撮影?SNSアップOKという太っ腹な展覧会。昭和な看板が否が応でも懐かしさを倍増する。   
(除了一些例外情况，摄影和社交网络上传在这个慷慨的展览中是允许的。无论你喜欢与否，昭和时代的招牌让人倍感怀旧。)"  
关于昭和风格，[解析昭和复古设计风格的设计秘密](https://zhuanlan.zhihu.com/p/359504079)中提到：  
“昭和设计之所以能给人留下深刻印象，是由于使用了特别的配色和字体，同时与叠加的印刷特点相结合，最终形成了独特的氛围感 ...... 昭和设计的配色，在整体上偏向使用让人感觉深沉与浓厚的颜色，在细部用色上偏向使用红色、橙色、黄色等令人兴奋的颜色。即便是出现蓝色、绿色这样的冷色，也会尽量挑选带有暖色调的蓝绿色，让画面更偏暖。另外，颜色的饱和度和明度都非常高，这样在作为海报等宣传的时候，会使设计内容既抢眼又干净 ...... 昭和时期的海报等宣传物料在字体的选择上，不仅较多地使用粗体，文字间隔也比较宽。这个时期的主要使用两种字体：一种是明朝体（相当于宋体），一种是哥特体。  
[<img title="(点击查看原图)" height="100" src="https://pic3.zhimg.com/80/v2-f04b233ca102d1deb3bb6cffc3043f52_720w.webp"/>](https://pic3.zhimg.com/80/v2-f04b233ca102d1deb3bb6cffc3043f52_720w.webp)
”  
从上述评论里得知，该招牌具有昭和风格是指其字体为明朝体（相当于宋体）、颜色为红色。    

---  

## 原画 （创作时间未知）  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTcluQYakAAAH8j.jpg"/> 
](https://pbs.twimg.com/media/FTcluQYakAAAH8j?format=jpg&name=medium)  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/garnetlynx777/status/1528732082763341824)         

A(下图左一)，B(下图左二)， C（下图左三）：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTcluQYakAAAH8j.jpg"/> 
](https://pbs.twimg.com/media/FTcluQYakAAAH8j?format=jpg&name=medium) 

细节：   

- C中领子的质感。  
![](img/FTcluQYakAAAH8j_crop0.jpg) 
![](img/FTcluQYakAAAH8j_crop1.jpg) 


---  

## 原画（1985年第18号)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2f12da9fdj30e60iwt9s.jpg"/> 
](https://wx1.sinaimg.cn/large/008rpAF8gy1h2f12da9fdj30e60iwt9s.jpg)  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见处出：北条司イラスト集(北条司Illustrations)。  
C：[官方blog - weibo](https://m.weibo.cn/detail/4771262742659434)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/illustrations_18_36_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2f12da9fdj30e60iwt9s.jpg"/> 
](https://wx1.sinaimg.cn/large/008rpAF8gy1h2f12da9fdj30e60iwt9s.jpg) 


细节：  

- C中的评论：      
“和泪姐一模一样构图的阿香  
虽然很像…但是有点微妙的不同…  
北条司老师说‘为什么要向泪致敬呢…？不记得了（笑）’ ”  


---  

## 原画 （1985年第48号）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBGZ4QaIAAjLNi.jpg"/> 
](https://pbs.twimg.com/media/FTBGZ4QaIAAjLNi?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/megenna_968/status/1526797754839998464)          

A(下图左一)，B(下图左二)， C（下图左三）：  
![](img/not_given.jpg) 
![](img/illustration_37_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBGZ4QaIAAjLNi.jpg"/> 
](https://pbs.twimg.com/media/FTBGZ4QaIAAjLNi?format=jpg&name=large) 

细节：   

- 头发的细节。B(下图左一列)对比C(下图左二列)。  
![](img/illustration_37_crop0.jpg) ![](img/FTBGZ4QaIAAjLNi_crop0.jpg)  
![](img/illustration_37_crop1.jpg) ![](img/FTBGZ4QaIAAjLNi_crop1.jpg) 

- 蓝色衣服上的高光。B(下图左一)对比C(下图左二)。  
![](img/illustration_37_crop2.jpg) ![](img/FTBGZ4QaIAAjLNi_crop2.jpg)  

- 红色衣服上的阴影。B(下图左一)对比C(下图左二)。  
![](img/illustration_37_crop3.jpg) ![](img/FTBGZ4QaIAAjLNi_crop3.jpg)  
![](img/illustration_37_crop4.jpg) ![](img/FTBGZ4QaIAAjLNi_crop4.jpg) 




---  

## 原画（1986-07）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6DG3aMAANC8k.jpg"/> 
](https://pbs.twimg.com/media/FTM6DG3aMAANC8k?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527628538807209984)        

A(下图左一)，B(下图左二)， C（下图左三）：  
![](img/not_given.jpg) 
![](img/illustration_38_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6DG3aMAANC8k.jpg"/> 
](https://pbs.twimg.com/media/FTM6DG3aMAANC8k?format=jpg&name=large) 

细节：   

- C中能看到着色的颗粒感。B(下图左一)对比C(下图左二)。  
![](img/illustration_38_crop0.jpg) 
![](img/FTM6DG3aMAANC8k_crop0.jpg) 

- 腰带的高光的笔触。B(下图左一)对比C(下图左二)。  
![](img/illustration_38_crop1.jpg) 
![](img/FTM6DG3aMAANC8k_crop1.jpg) 


---  

## 原画(1986年第35号)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6tnZacAAWunX.jpg"/> 
](https://pbs.twimg.com/media/FTM6tnZacAAWunX?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍1 - Twitter](https://twitter.com/yae_ch3/status/1527629263616495616)  
D: [现场实拍2 - Twitter](https://twitter.com/TOKYO_GENSO/status/1525013304812244996)          

A(下图左一)，B(下图左二)， C（下图左三）, D（下图左四）：  
![](img/not_given.jpg) 
![](img/illustrations_29_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6tnZacAAWunX.jpg"/> 
](https://pbs.twimg.com/media/FTM6tnZacAAWunX?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnvggWUUAA2GXm.jpg"/> 
](https://pbs.twimg.com/media/FSnvggWUUAA2GXm?format=jpg&name=large) 

细节：  

- D中的细节。枪械的质感(下图左一)、头发的细节(下图左二)、眉间的高光(下图左三)：    
![](img/FSnvggWUUAA2GXm_crop0.jpg) 
![](img/FSnvggWUUAA2GXm_crop1.jpg) 
![](img/FSnvggWUUAA2GXm_crop2.jpg)  
胸前衣服的着色：  
![](img/FSnvggWUUAA2GXm_crop3.jpg) 
![](img/FSnvggWUUAA2GXm_crop4.jpg) 
![](img/FSnvggWUUAA2GXm_crop5.jpg) 

- 枪托的着色：  
![](img/FSnvggWUUAA2GXm_crop6.jpg) 

- 手臂的高光。B(下图左一)对比D(下图左二)。  
![](img/illustrations_29_crop6.jpg) 
![](img/FSnvggWUUAA2GXm_crop7.jpg) 


---  

## 「CityHunter」 版画4 (1987-01)  
![](img/hanga_city04_thumb.jpg)  

[日文版商品链接](https://edition-88.com/products/cityhunter-hanga4)及描述：  
“来自版画工坊的评论：  
夕阳从红色到深蓝色的渐变很美，而夜空中的绿色建筑也很醒目。 绿色是主角轮廓中使用的重要颜色，所以颜色的搭配很谨慎。   
为了衬托出星空的美丽，在整个作品上喷了一层薄薄的珍珠漆，使其具有立体的感觉。   
技法：giclée  
版画用中性紙”  

[英文版商品链接](https://edition88.com/products/cityhunter-hanga4)及描述：  
“国际版在美术纸采用混合媒体（Giclée和UV）”  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: CityHunter完全版X；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527634946239889408)  
D： [现场实拍 - Twitter](https://twitter.com/garnetlynx777/status/1528732082763341824)  

A(下图左一)，B（下图左二）, C（下图左三）， D（下图左四）：  
![](img/hanga_city04_thumb.jpg) 
![](img/CH-X_044_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM_4SxaMAAnvzr.jpg"/> 
](https://pbs.twimg.com/media/FTM_4SxaMAAnvzr?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTclt7QakAAvSPj.jpg"/> 
](https://pbs.twimg.com/media/FTclt7QakAAvSPj?format=jpg&name=large) 

细节：   

- 官方给出A的侧视局部图很漂亮。不知道其中的金色亮点（下图左三）是什么？[疑问]：  
![](img/hanga_city04_crop0.jpg) 
![](img/hanga_city04_crop0-1.jpg) 
![](img/hanga_city04_crop0-2.jpg)   

- 以前看这幅图时没注意到夕阳的渐变。这次看商品描述才发现到这图里的夕阳的确很美！A(下图左一列)对比B（下图左二列）：    
![](img/hanga_city04_crop1.jpg) ![](img/CH-X_044_crop1.jpg)  
![](img/hanga_city04_crop2.jpg) ![](img/CH-X_044_crop2.jpg)  
![](img/hanga_city04_crop3.jpg) ![](img/CH-X_044_crop3.jpg)  
![](img/hanga_city04_crop4.jpg) ![](img/CH-X_044_crop4.jpg)  
![](img/hanga_city04_crop5.jpg) ![](img/CH-X_044_crop5.jpg)  


- A(下图左一列)的烟雾的笔触。远看烟雾很逼真（下图左二列）：  
![](img/hanga_city04_crop6.jpg) ![](img/CH-X_044_crop6.jpg)  

-  A(下图左一)的斑驳的窗户。对比B（下图左二）：  
![](img/hanga_city04_crop7.jpg) ![](img/CH-X_044_crop7.jpg)  

-  A(下图左一)的衣服的褶皱。对比B（下图左二）：  
![](img/hanga_city04_crop8.jpg) ![](img/CH-X_044_crop8.jpg)  


---  

## 原画 (1987年第11号)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTX0UpYakAEMzK4.jpg"/> 
](https://pbs.twimg.com/media/FTX0UpYakAEMzK4?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/MituruSouda/status/1528396349758390272)        

A(下图左一)，B(下图左二)， C（下图左三）：  
![](img/not_given.jpg) 
![](img/illustrations_30_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTX0UpYakAEMzK4.jpg"/> 
](https://pbs.twimg.com/media/FTX0UpYakAEMzK4?format=jpg&name=large) 

细节：  

- 大面积黑色区域的着色不均匀。B(下图左一)对比C(下图左二)。  
![](img/illustrations_30_crop0.jpg) 
![](img/FTX0UpYakAEMzK4_crop0.jpg)  

- C中的细节。枪械(下图左一左二)、袖口的装饰(下图左三)。  
![](img/FTX0UpYakAEMzK4_crop1.jpg) 
![](img/FTX0UpYakAEMzK4_crop2.jpg) 
![](img/FTX0UpYakAEMzK4_crop3.jpg) 


---  

## 「CityHunter」 版画1 (1987-06)
![](img/hanga_cty01_thumb.jpg)  

[日文版商品链接](https://edition-88.com/products/cityhunter-hanga1)  及描述：  
“週刊少年Jump 1997特别版SUMMER SPECIAL/ 卷首海报插画的版画。  
CityHunter的每张版画上都喷涂了珍珠漆颗粒。  
技法：giclée  
纸张：版画用中性纸”  

[英文版商品链接 City Hunter, Art Print #1](https://edition88.com/products/cityhunter-hanga1)及描述：  
“国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）对色彩和纹理进行精致的表达。  
每幅CityHunter的印刷品都是手工喷上珠光颜料。  
艺术版画有一种随观众的视角变化而变化的亮度，显示出深度。  
国际版用的是fine art纸上的混合媒体（Giclée和UV）”   

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 北条司Illustrations  
C: [现场实拍 - Twitter](https://twitter.com/yukimitsu/status/1525452263841755136) 


A(下图左一)，B（下图左二），C（下图左三）：  
![](img/hanga_cty01_thumb.jpg) 
![](img/illustrations_11_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSt-v-macAIKbRP.jpg"/> 
](https://pbs.twimg.com/media/FSt-v-macAIKbRP?format=jpg&name=medium) 

细节：   

- A(下图左一)的云应该是用喷枪制作的。远处看（下图左二）效果逼真。  
![](img/hanga_cty01_crop0.jpg) 
![](img/illustrations_11_crop0.jpg)    

- A(下图左一列)的水珠的画法。远处看（下图左二列）效果逼真。  
![](img/hanga_cty01_crop1.jpg) ![](img/illustrations_11_crop1.jpg)    
![](img/hanga_cty01_crop2.jpg) ![](img/illustrations_11_crop2.jpg)    

- A(下图左一)的海浪的画法。远处看（下图左二）效果逼真。  
![](img/hanga_cty01_crop3.jpg) ![](img/illustrations_11_crop3.jpg)    


---  

## 原画 (1988年第11号)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6tnZaIAEU0-L.jpg"/> 
](https://pbs.twimg.com/media/FTM6tnZaIAEU0-L?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527629263616495616)        

A(下图左一)，B(下图左二)， C（下图左三）：  
![](img/not_given.jpg) 
![](img/illustrations_26_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6tnZaIAEU0-L.jpg"/> 
](https://pbs.twimg.com/media/FTM6tnZaIAEU0-L?format=jpg&name=large) 

细节：   

- C中头发的细节。  
![](img/FTM6tnZaIAEU0-L_crop0.jpg)  

- C中眼部的细节。白眼球的上沿有灰色阴影。    
![](img/FTM6tnZaIAEU0-L_crop1.jpg) 

- 衣服的着色。B(下图左一)对比C(下图左二)。  
![](img/illustrations_26_crop2.jpg) 
![](img/FTM6tnZaIAEU0-L_crop2.jpg) 


---  

## 原画 （1989年第3、4合并号）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSsZuuMVEAA1EyW.jpg"/> 
](https://pbs.twimg.com/media/FSsZuuMVEAA1EyW?format=jpg&name=large)  
1989年新年3、4合并号。  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/unlimited_artx/status/1525341201641000960)  
D: [现场实拍 - Twitter](https://twitter.com/chisa_ryoyuki/status/1593905991363399680)         

A(下图左一)，B(下图左二)， C（下图左三）, D（下图左四）：  
![](img/not_given.jpg) 
![](img/illustration_p21-2_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSsZuuMVEAA1EyW.jpg"/> 
](https://pbs.twimg.com/media/FSsZuuMVEAA1EyW?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh6w2P-VEAAi80h.jpg"/> 
](https://pbs.twimg.com/media/Fh6w2P-VEAAi80h?format=jpg&name=large) 

细节：   

- 衣服上的着色细节。B(下图左一)对比C(下图左二)。  
![](img/illustration_p21-2_crop0.jpg) 
![](img/FSsZuuMVEAA1EyW_crop0.jpg) 


---  

## 原画 (1989年第30号)  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwWrEQaMAAOfyg.jpg"/> 
](https://pbs.twimg.com/media/FSwWrEQaMAAOfyg?format=jpg&name=medium) 

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/yukimitsu/status/1525619305723076608)         

A(下图左一)，B(下图左二)， C（下图左三）：  
![](img/not_given.jpg) 
![](img/illustration_p18-2_thumb.jpg) 
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwWrEQaMAAOfyg.jpg"/> 
](https://pbs.twimg.com/media/FSwWrEQaMAAOfyg?format=jpg&name=medium) 

细节：   

- 面部的细节。B(下图左一)对比C(下图左二)。  
![](img/illustration_p18-2_crop0.jpg) 
![](img/FSwWrEQaMAAOfyg_crop0.jpg) 

- 衣服上着色的细节。B(下图左一)对比C(下图左二)。  
![](img/illustration_p18-2_crop1.jpg) 
![](img/FSwWrEQaMAAOfyg_crop1.jpg) 


---  

## 原画 (1989年第36号)  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwWrEQaMAAOfyg.jpg"/> 
](https://pbs.twimg.com/media/FSwWrEQaMAAOfyg?format=jpg&name=medium) 

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/yukimitsu/status/1525619305723076608)         

A(下图左一)，B(下图左二)， C（下图左三）：  
![](img/not_given.jpg) 
![](img/illustration_p18_thumb.jpg) 
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwWrEQaMAAOfyg.jpg"/> 
](https://pbs.twimg.com/media/FSwWrEQaMAAOfyg?format=jpg&name=medium) 

细节：   

注：C的分辨率不够大，无法比较细节。  




---  

## 「CityHunter」 版画2 (1989-10)
![](img/hanga_cty02_thumb.jpg)  

[商品链接](https://edition-88.com/products/cityhunter-hanga2)及描述：  
“1989年周刊少年Jump第50期扉页的版画。  
Cat'sEye的版画使用白色墨水，每一张都有手工上色。   
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  
技法：giclée  
版画用中性紙”  

[商品链接](https://edition88.com/products/cityhunter-hanga2)及描述：  
“国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），精致地表现了里色彩和纹理。   
每幅CityHunter都是手工喷上珠光颜料。   
艺术版画有一种亮度，随着观众的视角变化而变化，显示出深度。  
国际版用的是美术纸上的混合媒体（Giclée和UV）"    

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 北条司Illustrations  
C: [现场实拍 - Twitter](https://twitter.com/dasuke05/status/1526046389607624704)  
D: [现场实拍 - Twitter](https://twitter.com/N7Obo2E2S3a3Xw4/status/1525415306138595329)  
E: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1531559103906344962)  
F: [现场实拍 - Twitter](https://twitter.com/berrypafe/status/1596872896932384768)  

A(下图左一)，B（下图左二）, C（下图左三）, D（下图左四）, E（下图左五）, F（下图左六）：  
![](img/hanga_cty02_thumb.jpg) 
![](img/illustrations_14_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2bGeMUYAElz2i.jpg"/> 
](https://pbs.twimg.com/media/FS2bGeMUYAElz2i?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FStdHpvUEAArF7g.jpg"/> 
](https://pbs.twimg.com/media/FStdHpvUEAArF7g?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FUEw4f9aIAAscLV.jpg"/> 
](https://pbs.twimg.com/media/FUEw4f9aIAAscLV?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/Fik7bZHaMAAfPjz.jpg"/> 
](https://pbs.twimg.com/media/Fik7bZHaMAAfPjz?format=jpg&name=large) 

细节：   

- A的底部是渐变，B的底部是不规则的涂抹。  

- 衣服的褶皱的笔触(下图左一)，远看效果（下图左二）逼真。  
![](img/hanga_cty02_crop0.jpg) 
![](img/hanga_cty02_crop1.jpg)   

- A(下图左一)眼部、嘴部的细节：  
![](img/hanga_cty02_crop2.jpg)  
![](img/hanga_cty02_crop3.jpg)  

- A(下图左一)中的白点中央有凸起，这在降低gamma之后（下图左二）更容易看出。  
![](img/hanga_cty02_crop4.jpg) 
![](img/hanga_cty02_crop4_gamma.jpg)  

- A(下图左一)中红色衣服处有疑似"杂质"的星星点点的痕迹，但其他颜色区域没有这种痕迹。不知道为什么会有这种痕迹[疑问]。  
![](img/hanga_cty02_crop5.jpg)   

- A(下图左一)中笔触。B(下图左二)中的效果。    
![](img/hanga_cty02_crop6.jpg) 
![](img/illustrations_14_crop6.jpg)   

- A(下图左一)中高楼窗内灯火的笔触。远看效果(下图左二)逼真。    
![](img/hanga_cty02_crop7.jpg) 
![](img/illustrations_14_crop6.jpg)   


---  

## 官网报道中的图片14 (1990-02?)  
![](img/cityhunter_tenji_96_01_thumb.jpg)  
1990年16号扉页。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 北条司イラスト集(北条司Illustrations)；  
C: CITYHUNTER-完全版-Y;  
D: [现场实拍 - Twitter](https://twitter.com/Draichi_/status/1528338113302790145)  
E: [现场实拍 - Twitter](https://twitter.com/cityhunter100t/status/1528293522415054848)  
F: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527630390835683329)  
G: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527628538807209984)  

A(下图左一)，B(下图左二)，C(下图左三)，D(下图左四), E(下图左五), F(下图左六), G(下图左七)：  
![](img/cityhunter_tenji_96_01_thumb.jpg) 
![](img/illustration_044_thumb.jpg) 
![](img/chce_034-35_thumb.jpg) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTW_ZEjagAAaGdT.jpg"/> 
](https://pbs.twimg.com/media/FTW_ZEjagAAaGdT?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FTWW2x3VIAADIMz.jpg"/> 
](https://pbs.twimg.com/media/FTWW2x3VIAADIMz?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM7vA9aMAQWvkD.jpg"/> 
](https://pbs.twimg.com/media/FTM7vA9aMAQWvkD?format=jpg&name=large) 
[<img title="G备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6DGoaMAA71Nz.jpg"/> 
](https://pbs.twimg.com/media/FTM6DGoaMAA71Nz?format=jpg&name=large) 

细节：   

- A的图片显示范围比B、C大，在右下角显示出签名和时间戳。  

- [官方微博](https://m.weibo.cn/detail/4772071005487793)的评论：“北条司老师的彩色插图色彩丰富，猫眼插图是以水彩为主，而城市猎人的绘制上导入了喷枪！这种令人印象深刻的烟，是先用喷枪喷上白色，再用湿纸巾擦拭描绘而成的”。  

- 头发的细节。A(下图左一)，B，C(下图左二)，D(下图左三)，E(下图左四)：      
![](img/cityhunter_tenji_96_01_crop0.jpg) 
![](img/chce_034-35_crop0.jpg) 
![](img/FTW_ZEjagAAaGdT_crop0.jpg) 
![](img/FTWW2x3VIAADIMz_crop0.jpg) 

- 眼部的细节。A(下图左一)，B，C(下图左二)，D(下图左三)，E(下图左四)：      
![](img/cityhunter_tenji_96_01_crop0.jpg) 
![](img/chce_034-35_crop1.jpg) 
![](img/FTW_ZEjagAAaGdT_crop1.jpg) 
![](img/FTWW2x3VIAADIMz_crop1.jpg) 

- 嘴部的细节。A(下图左一)，B，C(下图左二)，D(下图左三)，E(下图左四)：      
![](img/cityhunter_tenji_96_01_crop0.jpg) 
![](img/chce_034-35_crop2.jpg) 
![](img/FTW_ZEjagAAaGdT_crop2.jpg) 
![](img/FTWW2x3VIAADIMz_crop2.jpg) 

- 烟雾的细节。A(下图左一)，B，C(下图左二)，D(下图左三)，E(下图左四)：      
![](img/cityhunter_tenji_96_01_crop3.jpg) 
![](img/chce_034-35_crop3.jpg) 
![](img/FTW_ZEjagAAaGdT_crop3.jpg) 
![](img/FTWW2x3VIAADIMz_crop3.jpg)  
[官方微博](https://m.weibo.cn/detail/4772071005487793)对烟雾的评论：“北条司老师的彩色插图色彩丰富，猫眼插图是以水彩为主，而城市猎人的绘制上导入了喷枪！这种令人印象深刻的烟，是先用喷枪喷上白色，再用湿纸巾擦拭描绘而成的”。  

- 裤子着色的细节。A(下图左一)，B，C(下图左二)，D(下图左三)，E(下图左四)：      
![](img/cityhunter_tenji_96_01_crop4.jpg) 
![](img/chce_034-35_crop4.jpg) 
![](img/FTW_ZEjagAAaGdT_crop4.jpg) 
![](img/FTWW2x3VIAADIMz_crop4.jpg)  


- 背景人物的细节。A(下图左一)，B，C(下图左二)，D(下图左三)，E(下图左四),F(下图左五)：      
![](img/cityhunter_tenji_96_01_crop5.jpg) 
![](img/chce_034-35_crop5.jpg) 
![](img/FTW_ZEjagAAaGdT_crop5.jpg) 
![](img/FTWW2x3VIAADIMz_crop5.jpg) 
![](img/FTM7vA9aMAQWvkD_crop5.jpg)  

- 头发的细节。A(下图左一)，B，C(下图左二)，D(下图左三)，E(下图左四)，F(下图左五)，G(下图左六)：      
![](img/cityhunter_tenji_96_01_crop6.jpg) 
![](img/chce_034-35_crop7.jpg) 
![](img/FTW_ZEjagAAaGdT_crop7.jpg) 
![](img/FTWW2x3VIAADIMz_crop7.jpg) 
![](img/FTM7vA9aMAQWvkD_crop7.jpg) 
![](img/FTM6DGoaMAA71Nz_crop7.jpg) 

- 眼部的细节。A(下图左一)，B，C(下图左二)，D(下图左三)，E(下图左四)，F(下图左五)，G(下图左六)：      
![](img/cityhunter_tenji_96_01_crop6.jpg) 
![](img/chce_034-35_crop6.jpg) 
![](img/FTW_ZEjagAAaGdT_crop6.jpg) 
![](img/FTWW2x3VIAADIMz_crop6.jpg) 
![](img/FTM7vA9aMAQWvkD_crop6.jpg) 
![](img/FTM6DGoaMAA71Nz_crop6.jpg) 

- 白色发丝的细节。F(下图左1)，G(下图左2)，G降低gamma后(下图左3)能看到白颜料堆积的痕迹：  
![](img/FTM7vA9aMAQWvkD_crop8.jpg) 
![](img/FTM6DGoaMAA71Nz_crop8.jpg) 
![](img/FTM6DGoaMAA71Nz_crop8_gamma.jpg) 

- G中的细节。下图是发丝与高光。下图左三中的发丝有飞白的效果：    
![](img/FTM6DGoaMAA71Nz_crop14.jpg) 
![](img/FTM6DGoaMAA71Nz_crop15.jpg) 
![](img/FTM6DGoaMAA71Nz_crop16.jpg) 
![](img/FTM6DGoaMAA71Nz_crop17.jpg)  
伤疤(下图左1、左2)、衣服上的高光(下图左3-5)、衣服边缘的破损(下图左6)：  
![](img/FTM6DGoaMAA71Nz_crop9.jpg) 
![](img/FTM6DGoaMAA71Nz_crop10.jpg) 
![](img/FTM6DGoaMAA71Nz_crop11.jpg) 
![](img/FTM6DGoaMAA71Nz_crop12.jpg) 
![](img/FTM6DGoaMAA71Nz_crop13.jpg) 
![](img/FTM6DGoaMAA71Nz_crop21.jpg) 

- G中的细节。下图是脸颊和嘴部的细节，线条粗与细、线条边缘粗糙与光滑、墨色深与浅有不同。      
![](img/FTM6DGoaMAA71Nz_crop18.jpg) 
![](img/FTM6DGoaMAA71Nz_crop19.jpg) 
![](img/FTM6DGoaMAA71Nz_crop20.jpg) 
![](img/FTM6DGoaMAA71Nz_crop22.jpg) 



---  

## 高級Art Print 1 (A4)/Cat's♥Eye (1990-12)  
![](img/artprint1_A4_01_thumb.jpg)  

[日文版商品链接 高級アートプリント1 (A4)/キャッツ♥アイ](https://edition-88.com/products/catseye-artprint1-a4)  

[英文版商品链接 Cat's Eye, Art Prints #1 (A4 size / 10 sheets per set)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：    
“Mermaid纸”   

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司美女写真馆》中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/tenchi0094/status/1526155281696034822)  
D: [现场实拍 - Twitter](https://twitter.com/megenna_968/status/1526793131857166336)  
E: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1531558128936173568)  
F: [现场实拍 - Twitter](https://twitter.com/SASAME828/status/1594564365856157696)  

A(下图左一)，B（下图左二）, C（下图左三）, D（下图左四）, E（下图左五）：  
![](img/artprint1_A4_01_thumb.jpg) 
![](img/illustrations_106_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS3-IrKUsAEoCKO.jpg"/> 
](https://pbs.twimg.com/media/FS3-IrKUsAEoCKO?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBCMnWaMAE07CG.jpg"/> 
](https://pbs.twimg.com/media/FTBCMnWaMAE07CG?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FUEv_1TaIAAz3zG.jpg"/> 
](https://pbs.twimg.com/media/FUEv_1TaIAAz3zG?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/FiEH08XacAADjKc.jpg"/> 
](https://pbs.twimg.com/media/FiEH08XacAADjKc?format=jpg&name=large) 

细节：  
- A背景右侧更红（或紫），使得背景左、右的色彩对比更明显。  
- A上部、底部显示范围更多。左下角有作者的签名和时间戳。B因下部显示范围少，而未显示签名和时间戳。    
![](img/artprint1_A4_01_crop5.jpg)  
- 分辨率约3500x4000  
- 日文版商品页面有局部图片，其分辨率更大：  
![](img/artprint1_A4_02_crop0.jpg)  
- 可能因为整体的红色更明显，所以肤色的阴影更明显。  
- 小瞳腿部的蓝色阴影更明显。  
- 画面更细腻、过渡更均匀、噪点更少。（可能是因为图片分辨率足够大）  
![](img/artprint1_A4_01_crop0.jpg) 
![](img/artprint1_A4_01_crop1.jpg) 
![](img/artprint1_A4_01_crop2.jpg)  
- 能看出高光的笔触：  
![](img/artprint1_A4_01_crop3.jpg) 
![](img/artprint1_A4_01_crop4.jpg) 
![](img/artprint1_A4_01_crop6.jpg)  

- 现场实拍的C、D、E背景均是大面积的红色。为什么？[疑问]。    



---  

## 「CityHunter」 版画3 (1991)
![](img/hanga_cty03_thumb.jpg)  

[商品链接](https://edition-88.com/products/cityhunter-hanga3)及描述：  
“北条司 ILLUSTRATIONS 1991 附录的海报版画。   
技法：giclée  
版画用中性紙 ” 

[商品链接 Art Print #3](https://edition88.com/products/cityhunter-hanga3)及描述：  
“国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），精致地表现了里色彩和纹理。  
每幅CityHunter都是手工喷上珠光颜料。   
艺术版画有一种亮度，随着观众的视角变化而变化，显示出深度。  
国际版用的是美术纸上的混合媒体（Giclée和UV）”  

签名位于图片右上角。  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 北条司Illustrations  
C: [现场实拍 - Twitter](https://twitter.com/SASAME828/status/1594564365856157696) 

A(下图左一)，B（下图左二）， C（下图左三）：  
![](img/hanga_cty03_thumb.jpg) 
![](img/illustrations_02_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiEH1efacAIpY-z.jpg"/> 
](https://pbs.twimg.com/media/FiEH1efacAIpY-z?format=jpg&name=large) 

细节：   

- A(下图左一列)头发大部分的高光为留白，而不是涂白色颜料。对比B（下图左二列）：  
![](img/hanga_cty03_crop4.jpg) ![](img/illustrations_02_crop4.jpg)  
![](img/hanga_cty03_crop5.jpg) ![](img/illustrations_02_crop5.jpg)  

- A(下图左一列)眼部、嘴部的细节。对比B（下图左二列）：  
![](img/hanga_cty03_crop2.jpg) ![](img/illustrations_02_crop2.jpg)  
![](img/hanga_cty03_crop3.jpg) ![](img/illustrations_02_crop2.jpg)  
![](img/hanga_cty03_crop0.jpg) ![](img/illustrations_02_crop0.jpg)  
![](img/hanga_cty03_crop1.jpg) ![](img/illustrations_02_crop0.jpg)  

- 烟雾逼真。A(下图左一)对比B（下图左二）：    
![](img/hanga_cty03_crop6.jpg) ![](img/illustrations_02_crop6.jpg)  

- 降低gamma后可看到A中大多数高光处要么留白、要么只涂了淡淡的白色颜料(下图左一)。对比B（下图左二）：    
![](img/hanga_cty03_crop7.jpg) ![](img/illustrations_02_crop7.jpg)  

- 腿部线条很流畅。  
![](img/hanga_cty03_crop8.jpg) ![](img/hanga_cty03_crop9.jpg) 

- A(下图左一列)中金属质感的着色。对比B（下图左二列）：    
![](img/hanga_cty03_crop10.jpg) ![](img/illustrations_02_crop10.jpg)  
![](img/hanga_cty03_crop11.jpg) ![](img/illustrations_02_crop11.jpg)  

- A(下图左一列)中高光的笔触。B（下图左二列）中效果逼真：    
![](img/hanga_cty03_crop12.jpg) ![](img/illustrations_02_crop12.jpg)  
![](img/hanga_cty03_crop13.jpg) ![](img/illustrations_02_crop12.jpg)  

- A(下图左一)中笔触。B(下图左二)中的效果。    
![](img/hanga_cty03_crop14.jpg) ![](img/illustrations_02_crop14.jpg)   

- A(下图左一)中高楼窗内灯火的笔触。远看效果(下图左二)逼真。    
![](img/hanga_cty03_crop15.jpg) ![](img/illustrations_02_crop14.jpg)   

- A(下图左一(带少许水印))中枪的着色逼真。  
![](img/hanga_cty03_crop16.jpg)  

- A(下图左一)中衣服的着色笔触。远看效果(下图左二)逼真。    
![](img/hanga_cty03_crop17.jpg) ![](img/illustrations_02_crop17.jpg)   



---  

## 原画(1991)  
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh561BraUAEL2qt.jpg"/> 
](https://pbs.twimg.com/media/Fh561BraUAEL2qt?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：未知。    
C: [现场实拍 - Twitter](https://twitter.com/megenna_968/status/1526797754839998464)  
D: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527637602140327937)  
E: [现场实拍 - Twitter](https://twitter.com/chisa_ryoyuki/status/1593905991363399680)  
F: [现场实拍 - Twitter](https://twitter.com/ShikiYggdrasill/status/1593846378484363264)            

A(下图左一)，B(下图左二)， C(下图左3)、D(下图左4)、E(下图左5)、F(下图左6)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBGZ4VaIAATAIj.jpg"/> 
](https://pbs.twimg.com/media/FTBGZ4VaIAATAIj?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNCTC6acAIo8Jb.jpg"/> 
](https://pbs.twimg.com/media/FTNCTC6acAIo8Jb?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh6w2PpUYAAzQfp.jpg"/> 
](https://pbs.twimg.com/media/Fh6w2PpUYAAzQfp?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh561BraUAEL2qt.jpg"/> 
](https://pbs.twimg.com/media/Fh561BraUAEL2qt?format=jpg&name=large) 

细节：   

- E中的细节。头发(下图左一)、眼部(下图左二)。     
![](img/Fh6w2PpUYAAzQfp_crop0.jpg) 
![](img/Fh6w2PpUYAAzQfp_crop2.jpg) 

- E中的衣服褶皱逼真。  
![](img/Fh6w2PpUYAAzQfp_crop1.jpg)  


---  

## 官网报道中的图片09 (1991年第15号)
![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_4_thumb.jpg)    

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 常见来源(无)；  
C: [现场实拍 - Twitter](https://twitter.com/yukimitsu/status/1525452263841755136)  
D: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527628538807209984) 

A(下图左一)，B(下图左二), C(下图左三), D（下图左四）：  
![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_4_thumb.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSt-vv9aUAAZ6-p.jpg"/> 
](https://pbs.twimg.com/media/FSt-vv9aUAAZ6-p?format=jpg&name=large ) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6DIoaUAAfDHT.jpg"/> 
](https://pbs.twimg.com/media/FTM6DIoaUAAfDHT?format=jpg&name=large) 

细节：   

- D中能看到头发高光的笔触：    
![](img/FTM6DIoaUAAfDHT_crop0.jpg) ![](img/FTM6DIoaUAAfDHT_crop7.jpg)   

- D中头发的细节。  
![](img/FTM6DIoaUAAfDHT_crop1.jpg) 
![](img/FTM6DIoaUAAfDHT_crop2.jpg) 


- D中眼部的细节。  
![](img/FTM6DIoaUAAfDHT_crop3.jpg)  
![](img/FTM6DIoaUAAfDHT_crop4.jpg) 

- D中嘴部的细节。  
![](img/FTM6DIoaUAAfDHT_crop5.jpg)  
![](img/FTM6DIoaUAAfDHT_crop6.jpg) 

<a name="CompareHairInCEAndCH"></a>  

- D中头发的光泽：  
![](img/FTM6DIoaUAAfDHT_crop7.jpg)  
对比CityHunter和Cat'sEye里的头发着色。CH(上图)里头发高光细节比CE里有些图(下图左四、左五)少，但CH中头发高光的最终效果更好。  
![](img/ce_02_000_crop3.jpg) 
![](img/hanga_cat05_crop3.jpg) ![](img/hanga_cat05_crop4.jpg) 
![](img/hanga_cat07_crop16.jpg) 
![](img/hukuseigenkou3_B4_crop3.jpg)   





--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  

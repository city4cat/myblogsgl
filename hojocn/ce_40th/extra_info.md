目录：  

- [Paper](#Paper)  
    - [マーメイド紙 / 'Marmaid' paper](#Marmaid)  
    - [上質紙 / WOODFREE PAPER](#Woodfree)  
    - [Fine Art Paper / 美术纸](#FineArt)  
    - [和纸 / Washi](#Washi)  
    - [クソト紙 / Kent Paper](#Kent)  
- [Print](#Print)  
    - [Offset Printing / 胶印](#Offset)  
    - [Giclée](#Giclée)  


# Paper  


<a name="Marmaid"></a>  
## 1. マーメイド紙 / 'Marmaid' paper  
(source: https://takeo.co.jp/en/finder/main/mermaid.html  )

A feltmark is the surface structure imparted to paper at the paper mill by using felt with a woven pattern that transfers to the surface of the paper.  
毛毡印是在造纸厂通过使用带有编织图案的毛毡转移到纸的表面而赋予纸张的表面结构。  

MERMAID, which was designed and launched in 1956 under the editorial supervision of designer Hiromu Hara (1903-1986) is a feltmarked fine paper produced by machine in Japan.  
MERMAID是在设计师Hiromu Hara（1903-1986）的编辑监督下于1956年设计并推出的，是一种在日本用机器生产的有毛毡标志的高级纸张。  


The resemblance of the feltmark to gentle waves inspired the paper’s name, and it was launched as MERMAID RIPPLE, recalling the gentle waves of a sea where mermaids live. Then, in 1960, it became known simply as MERMAID.  
毡标与轻柔的海浪相似，启发了报纸的名称，它以MERMAID RIPPLE的名义发行，让人想起美人鱼生活的大海中的轻柔海浪。然后，在1960年，它被简单地称为MERMAID。  

MERMAID has a rich variety of color and weights, selected in 1999 under the editorial supervision of graphic designer Ikko Tanaka (1930-2002). It is widely used for painting and as design paper, and is widely appreciated as a paper for use in bookbinding.  
MERMAID拥有丰富的色彩和重量，1999年在平面设计师Ikko Tanaka（1930-2002）的编辑监督下选定。它被广泛用于绘画和作为设计用纸，并作为装订用纸而受到广泛赞赏。  

MERMAID can be manufactured without using aluminium sulphate, a chemical that is one factor in the degradation of paper. This option is available with some of the most popular papers in the range. Use of high-quality pulp and dyes makes the colors fade-resistant, and the paper resists creasing well. These characteristics make MERMAID suitable for long term storage or preservation.  
MERMAID可以在不使用硫酸铝的情况下生产，而硫酸铝是导致纸张降解的一个因素。该系列中的一些最受欢迎的纸张可以选择这种方式。使用高质量的纸浆和染料使颜色具有抗褪色性，而且纸张的抗折痕能力很强。这些特点使MERMAID适合长期储存或保存。  

To suit diverse uses, it is available in a number of specialized specifications, including a pearlized gloss (KIRABIKI M-115), versions for watercolor painting (DRAWING MERMAID), and inkjet papers.  
为了适应不同的用途，它有许多专门的规格，包括珍珠光泽（KIRABIKI M-115），用于水彩画的版本（DRAWING MERMAID），以及喷墨纸。  


<a name="Woodfree"></a>  
## 2. 上質紙 / WOODFREE PAPER
source: https://ja.wikipedia.org/wiki/上質紙, https://en.wikipedia.org/wiki/Wood-free_paper  

上質紙（じょうしつし）は、化学パルプ配合率が100%の洋紙を指す。主に印刷用に使用される紙で、非塗工印刷用紙の上級印刷紙に分類される。「WOODFREE PAPER」とも言う。  
高档纸是指化学纸浆含量为100%的纸张。 它主要用于印刷，被列为非涂布印刷纸类别中的高级印刷纸。 也被称为"不含木材的纸"。  

### 製法
基本的な製法はコート紙の原紙製法と同一。ただし、コート紙原紙がコート層の塗布を想定しているのに対して、上質紙は塗布することなく製品になるため、原料パルプの配合比率などが異なる。  
基本的制造过程与铜版纸原纸的制造过程相同。 然而，铜版纸原纸是为涂布层而设计的，而上質紙则是在没有涂布层的情况下制成的，因此，其原料纸浆比例等也不同。  

### 特徴
表裏ともコートされておらず、表面はパルプが露出している。このためインクのにじみが多く発生するため、レジンなどのインク吸収剤を添加することが多い。表面は平滑であるが、コート層がないためにコート紙に比べると平滑性は劣る。ただし、パルプ比率が高いため、同一米坪であればコート紙よりも紙腰・断裂強度・引張強度・不透明度とも優れている。  
正面和背面都没有涂层，让纸浆暴露在表面。 这就造成了大量的油墨渗出，所以通常会添加树脂等油墨吸收剂。 表面是光滑的，但由于没有涂布层，使其不如铜版纸光滑。 然而，由于其较高的纸浆比率，在相同的基础重量下，它在纸张强度、撕裂强度、拉伸强度和不透明度方面优于铜版纸。  

### 原料
主に木材パルプを使用するが、昨今は再生上質紙として古紙パルプを使用するもの、あるいは森林保護の観点からバガス・藁・葦・竹などの非木材パルプを使用するものも出回っている。  
它主要由木浆制成，但最近也有一些再生上質紙使用再生纸浆，或从保护森林的角度出发，使用蔗渣、稻草、芦苇和竹子等非木材纸浆。   

### 用途
主な用途は雑誌・チラシ・事務用伝票・メモ帳・ノート・原稿用紙などの印刷用途である。厚手のものは郵便葉書や名刺などにも用いられる。また、断裁してパッケージを施したものはコピー用紙として使用される。ただしコピー用紙の場合、複数枚を同時にコピー機に送り込んでしまう重送を避けるため、あるいはコピー機の内部で詰まるジャミングを避けるために、静電気の発生を抑えることが必要である。また、昨今は家庭用インクジェットプリンターの普及に伴い、インクジェット適性を付加したコピー用紙もある（詳細はコピー用紙を参照のこと）。  
其主要用途是印刷杂志、传单、办公单据、记事本、笔记本和手稿纸。 厚片也被用于邮政明信片和名片。 那些经过切割和包装的纸张被用作复印纸。 然而，在复印纸的情况下，有必要抑制静电的产生，以避免大量进纸，即几张纸同时被送入复印机，或卡纸，即纸张被卡在复印机内。 此外，随着最近家用喷墨打印机的普及，有一些复印纸增加了喷墨适用性（详见复印纸）。   

Wood-free paper is not as susceptible to yellowing as paper containing mechanical pulp.  
非木材纸张不像含有机械浆的纸张那样容易变黄。  


<a name="FineArt"></a>  
## 3. Fine Art Paper / [美术纸](https://cn.bing.com/dict/search?q=fine+art+paper&FORM=HDRSC6)
source: https://pisnak.com/fineartpaper-and-photopaper/

### What Is Fine Art Paper? (什么是美术纸？)    
Art prints and fine art prints are typically made with a type of fine art paper. This type of paper is very durable and is usually made with archival, acid-free cotton (or a cotton blend).  
艺术印刷品和美术印刷品通常是用一种美术纸制作的。这种类型的纸非常耐用，通常是用存档的无酸棉（或混合棉）制成。  

Fine art paper is usually matte and thick and can be smooth or lightly textured.  
美术纸通常是哑光、厚，可以是光滑的或轻度纹理的。  

Fine art paper is a type of paper that is specifically designed for use with artworks. It is usually made from high quality materials and has a lightly textured surface to allow for easy printing and mounting. Fine art paper is available in a variety of sizes and thicknesses, depending on the needs of the artist.  
美术纸是一种专门为艺术作品设计的纸张。它通常由高质量的材料制成，表面有轻微的纹理，便于印刷和安装。美术纸有各种尺寸和厚度，取决于艺术家的需要。  

### Conclusion: What Paper Is Best for Art Printing? （ 结论：什么纸最适合艺术印刷？） 
Art prints and fine art prints are typically made with a type of fine art paper. This type of paper is very durable and is usually made with archival, acid-free cotton (or a cotton blend).  
艺术印刷品和美术印刷品通常是用一种美术纸制作的。这种类型的纸非常耐用，通常是用存档的无酸棉（或混合棉）制成。  

Photographic prints are made of photo paper. This type of paper is resin-coated and light-sensitive.  
摄影印刷品是由相纸制成的。这种类型的纸是树脂涂层的，对光线敏感。  

Fine art paper is usually matte, thick, and lightly textured. Photo paper is glossier and typically thinner and smoother than fine art paper.  
美术纸通常是哑光、厚，有轻微的纹理。相纸更有光泽，通常比美术纸更薄、更光滑。  

Photo paper also helps in giving the print a high contrast look (amazing highlights and shadows) compared to the more muted look of fine art paper.  
照片纸还有助于使印刷品具有高对比度的外观（惊人的高光和阴影），相比之下，美术纸的外观更加柔和。  

However, compared to other papers used (like photo paper), galleries usually prefer fine art paper because it tends to last longer with less fading, cracking, and yellowing.  
然而，与其他纸张（如照片纸）相比，画廊通常更喜欢美术纸，因为它的寿命更长，褪色、开裂和变黄的情况更少。  

Fine art paper can have a lifespan of 100+ years.  
美术纸的寿命可以达到100年以上。 


<a name="Washi"></a>  
## 4. 和纸 / Washi  {#Washi}  
### [和紙 - Wikipedia](https://zh.wikipedia.org/wiki/和紙)

和紙是日本以传统技艺生产的一种纸之统称，用来和西方传入的洋纸相区分。和纸通常由雁皮、三椏或纸桑的纤维制成，但也可用竹子、麻、稻秆和麦秆制作。
2014年11月26日，聯合國教科文組織（UNESCO）審核通過將「和紙 日本手漉和紙技術」列入無形文化遺產。

#### 特点
日本纸的特点是比洋纸的纤维长，质地虽薄，但更坚韧，且寿命更长，手感也很好。但因为产量低，所以价格高。
利用其能保存上多年以及强韧柔软的特性，被用于日本画、木刻等许多特殊的用途。同时也作为部分工艺品的材料、家具的材料等使用。江户时代在日本大批量生产，除了门窗隔扇外，还用于和服和寝具等。

### [Washi - Wikipedia](https://en.wikipedia.org/wiki/Washi)  

Washi (和紙) is traditional Japanese paper. The term is used to describe paper that uses local fiber, processed by hand and made in the traditional manner. Washi is made using fibers from the inner bark of the gampi tree, the mitsumata shrub (Edgeworthia chrysantha), or the paper mulberry (kōzo) bush.[1] As a Japanese craft, it is registered as a UNESCO intangible cultural heritage.  
和纸（Washi）是传统的日本纸。该术语用于描述使用当地纤维的纸张，由手工加工并以传统方式制作。和纸是用甘比树、Mitsumata灌木（Edgeworthia chrysantha）或纸桑（kōzo）灌木的内侧树皮的纤维制成的。 [1] 作为一种日本工艺，它被注册为联合国教科文组织非物质文化遗产。   

Washi is generally tougher than ordinary paper made from wood pulp, and is used in many traditional arts. Origami, Shodō, and Ukiyo-e were all produced using washi. Washi was also used to make various everyday goods like clothes, household goods, and toys, as well as vestments and ritual objects for Shinto priests and statues of Buddha. It was even used to make wreaths that were given to winners in the 1998 Winter Paralympics. Washi is also used to repair historically valuable cultural properties, paintings, and books at museums and libraries around the world, such as the Louvre and the Vatican Museums, because of its thinness, pliability, durability over 1000 years because of its low impurities, and high workability to remove it cleanly with moisture.[3][4][5] 
Washi通常比由木浆制成的普通纸更坚韧，并被用于许多传统艺术中。折纸、寿堂和浮世绘都是用Washi制作的。Washi还被用来制作各种日常用品，如衣服、家庭用品和玩具，以及神道士的法衣和祭祀用品和佛像。它甚至被用来制作1998年冬季残奥会上送给获奖者的花环。在世界各地的博物馆和图书馆，如卢浮宫和梵蒂冈博物馆，Washi还被用来修复具有历史价值的文化财产、绘画和书籍，因为它很薄，很柔韧，由于杂质少，耐用性超过1000年，而且可操作性强，可以用水分清除干净。  

#### Manufacture  
Washi is produced in a way similar to that of ordinary paper, but relies heavily on manual methods. It involves a long and intricate process that is often undertaken in the cold weather of winter, as pure, cold running water is essential to the production of washi. Cold inhibits bacteria, preventing the decomposition of the fibres. Cold also makes the fibres contract, producing a crisp feel to the paper. It is traditionally the winter work of farmers, a task that supplemented a farmer's income.  
Washi的生产方式与普通纸相似，但主要依靠手工方法。它涉及一个漫长而复杂的过程，通常在冬季的寒冷天气下进行，因为纯净的冷水对Washi的生产至关重要。寒冷能抑制细菌，防止纤维的分解。寒冷也使纤维收缩，使纸张产生清脆的感觉。传统上，Washi是农民的冬季工作，是补充农民收入的一项任务。  

Paper mulberry is the most commonly used fiber in making Japanese paper. The mulberry branches are boiled and stripped of their outer bark, and then dried. The fibers are then boiled with lye to remove the starch, fat and tannin, and then placed in running water to remove the spent lye. The fibers are then bleached (either with chemicals or naturally, by placing it in a protected area of a stream) and any remaining impurities in the fibers are picked out by hand. The product is laid on a rock or board and beaten.  
纸桑是制作日本纸最常用的纤维。桑树枝被煮沸并剥去外皮，然后被晒干。然后用碱液煮沸纤维，以去除淀粉、脂肪和丹宁，然后放在流水中，以去除废碱液。然后，纤维被漂白（用化学品或自然漂白，通过将其置于溪流的保护区域），纤维中的任何剩余杂质被手工拣出。产品被铺在石头或木板上并被打碎。   

Wet balls of pulp are mixed in a vat with water and a formation aid to help keep the long fibers spread evenly. This is traditionally neri, which is a mucilaginous material made from the roots of the tororo aoi plant, or PEO, polyethylene oxide. One of two traditional methods of paper making (nagashi-zuki or tame-zuki) is employed. In both methods, pulp is scooped onto a screen and shaken to spread the fibers evenly. Nagashi-zuki (which uses neri in the vat) produces a thinner paper, while tame-zuki (which does not use neri) produces a thicker paper.  
湿的纸浆球在大桶中与水和成型助剂混合，以帮助保持长纤维的均匀分布。这就是传统上的neri，它是一种从tororo aoi植物的根部制成的粘性材料，或PEO，聚氧化乙烯。两种传统的造纸方法（nagashi-zuki或tame-zuki）中的一种被采用。在这两种方法中，纸浆被舀到一个筛子上，然后摇晃以使纤维均匀地分布。Nagashi-zuki（在大桶中使用neri）生产的纸张较薄，而tame-zuki（不使用neri）生产的纸张较厚。   

#### Types  
With enough processing, almost any grass or tree can be made into a washi. Gampi, mitsumata, and paper mulberry are three popular sources.[1]  
经过足够的加工，几乎任何草或树都可以做成washi。Gampi、Mitsumata和Paper mulberry是三种流行的来源。   

- Ganpishi (雁皮紙) – In ancient times, it was called Hishi (斐紙). Ganpishi has a smooth, shiny surface and is used for books and crafts.  
在古代，它被称为Hishi（斐纸）。雁皮纸的表面光滑、有光泽，用于制作书籍和工艺品。   

- Kōzogami (楮紙) – Kōzogami is made from paper mulberry and is the most widely made type of washi. It has a toughness closer to cloth than to ordinary paper and does not weaken significantly when treated to be water-resistant.  
楮紙是由桑树纸制成的，是最广泛的washi类型。它的韧性更接近于布，而不是普通的纸，并且在经过防水处理后不会明显减弱。  

- Mitsumatagami (三椏紙) – Mitsumatagami has an ivory-colored, fine surface and is used for shodō as well as printing. It was used to print paper money in the Meiji period.  
三椏紙有一个象牙色的精细表面，被用来做shodō以及印刷。明治时期，它被用来印刷纸币。 


<a name="Kent"></a>  
## 5. クソト紙 / Kent Paper  {#Kent}  

### 5.1 资料来源1：https://kamiconsal.jp/kentpapernani/
#### ケント紙の用途 製図用紙や名刺に使われる理由（肯特纸的用途 为什么它被用来做绘图纸和名片）
ケント紙の用途が何かというと多くは製図用紙です。少なくとも自分が大学生だった昭和の終わり頃はそうでした。今では信じられないかもしれませんが当時パソコンで図面を描くCADはまだなかったんですね。  
肯特纸的许多用途是用于绘图纸。 至少在我还是一名大学生的时候，昭和末期的情况是这样的。 现在可能很难相信，但在那个时候，还没有在电脑上绘图的CAD系统。  

図面を引くといったらT定規とかドラフターという道具を使って、鉛筆でケント紙に図面を描くものと相場は決まっていました。鉛筆で書いて間違えたら消しゴムで消してという作業をするわけですから、製図用の紙は表面が滑らかで消しゴムで消しても毛羽立たないような強い表面強度を持つ紙でなければ使えない。その用途にマッチしたのがケント紙だったということです。  
说到画画，通常是用铅笔在肯特纸上用T型尺或绘图仪画的。 因为你必须用铅笔写字，用橡皮擦除错误，所以画纸必须有光滑的表面和强大的表面强度，这样即使用橡皮擦除也不会蓬乱。 肯特纸是这一目的的良好匹配。  

ただし最近は美術系など特殊な用途は別として、製図はみなパソコンに置き換わってしまいました。CADを使えば線の太さがどうこうとか言われることもないし書き損じた時の修正も簡単ですし、データなら図面の管理も簡単ですから  
然而，最近，除了美术等特殊应用外，所有制图都已被计算机取代。 如果你使用CAD，你不必担心线条的粗细，很容易纠正错误，如果是数据，很容易管理图纸。  

ケント紙の出番は減っているんだろうと思います。漫画やイラストなんかでさえも紙に描くよりペンタブを使って描く人が増えていますからケント紙が使われるシーンは減っているんでしょう。  
我认为肯特纸的使用正在减少。 使用肯特纸的场景可能正在减少，因为使用钢笔标签作画的人越来越多，而不是在纸上作画，即使是漫画和插图。  

製図用紙以外では名刺用途も多かったと思います。自分の感覚では名刺はケント紙かなと。紙質がしっかりしていて折れ曲がるようなことはなくて、下品なピカピカしたような白ではなくて上品でナチュラルな白。特徴はありませんが初対面の挨拶で名刺交換する時には大手企業にとっては最適だったと思います。それに昔は凸版印刷が多かったので印刷する時に表面強度が強くなければ紙ムケしてしまって使えなかったんですね。そういう意味でもケント紙は都合が良かったんだろうと思います。  
除了画纸之外，我认为名片也有很多用途。 我自己的感觉是，名片是由肯特纸制成的。 纸张质量牢固，不会弯曲，而且不是庸俗的闪亮的白色，而是优雅的自然白色。 它没有任何功能，但我认为它非常适合大公司在第一次见面时交换名片和打招呼。 此外，过去经常使用凸版印刷，所以如果印刷时表面强度不够，纸张太软，就无法使用。 从这个意义上说，肯特纸也可能很方便。  

ただ、名刺についても最近は事情が変わっているようです。事情が大きく変わったのは個人ベースで印刷できるインクジェット印刷の登場でしょう。ケント紙ではなくて名刺用インクジェット用紙を使って自分でカラー印刷をする。そうすれば自分でスマホ撮影した写真を自由に入れることも出来ますし、急ぐ時にわざわざ名刺印刷を頼む必要もないし数枚からでも作ることが出来る。実際自分の手元にある名刺を見たところ、企業の名刺はケント紙が多いんですが個人の名刺のほとんどはインクジェット用紙かコート紙になってました。こういうのも時代の流れというかIT化の影響ということなんでしょう。  
然而，名片的情况最近似乎有了变化。 情况的最大变化是喷墨打印的出现，它可以在个人基础上进行打印。 你可以用喷墨纸代替肯特纸制作名片，并自己用彩色打印。 这样，你可以自由地插入你自己用智能手机拍摄的照片，当你匆忙时，你不必要求印制名片，而且你可以制作少量的卡片。 事实上，当我查看我手头的名片时，我发现大多数企业名片是用肯特纸制作的，但大多数个人名片是用喷墨纸或铜版纸制作的。 我想这也是时代的潮流，或者说是信息技术的影响。  

なお、ケント紙の印刷についてはこちらに記事を書きましたので参考にして下さい。（参考記事）⇒ケント紙の印刷。インクジェットやレーザーでも使えるのか？  
关于在肯特纸上印刷的更多信息，请参考我在这里写的文章。（参考記事）⇒ケント紙の印刷。インクジェットやレーザーでも使えるのか？  


#### ケント紙の種類と厚さ（肯特纸的类型和厚度）  
ケント紙の種類は様々です。管理人が調べたところではこんな感じ。  
銘柄  
    KMKケント  
    バロンケント  
    BBケント  
    ホワイトピーチケント  
    クリームケント紙  
    MSケント紙  
肯特纸有多种类型。 以下是我发现的情况。    
品牌名称  
    KMK肯特  
    Baron肯特  
    BB肯特  
    白桃肯特  
    奶油肯特纸  
    MS肯特纸  

また、色調、米坪、紙厚はたとえばMSケント紙ならばこんな感じ。  
色調  
    ホワイト  
    ナチュラル  
    クリーム  
此外，以MS Kent纸为例，其色调、基重和纸张厚度如下。  
色调  
    白  
    自然  
    奶油  

米坪、厚さ、kg連量（四六判換算）の関係  
基重、厚度和公斤连续重量之间的关系（以方格为例）  

    104.7g/㎡(0.11mm)<90>  
    127.9g/㎡(0.15mm)<110>  
    157g/㎡(0.17mm)<135>  
    209.4g/㎡(0.24mm)<180>  
    261g/㎡(0.3mm)<224.5>  

基本的に厚紙になります。  
基本上是厚纸。  


#### ケント紙の品質設計は何がどうなっているのか（肯特纸业的质量设计是什么？)  
ここからは元製紙会社社員としての本音をお話させて下さい。自分はケント紙の製造に関わったことはないので、ケント紙はの品質設計は何がどのようになっているのかを調べてみました。  
让我首先告诉你我作为一个前造纸公司雇员的真实想法。 我从未参与过肯特纸的生产，所以我调查了肯特纸的质量设计是怎样的。  

まずケント紙は白い上質紙ですからパルプ配合は晒クラフトパルプ100%が基本です。製図に使われるので黒点などの夾雑物はNGです。最近はエコということで白色度の高い古紙パルプを使用した再生紙もありますが、その場合は夾雑物は我慢しないといけないですね。  
首先，肯特纸是白色细纸，所以纸浆含量基本上是100%漂白牛皮纸浆。 由于它是用来绘画的，所以不允许有黑点和其他异物。 最近，有一种由回收纸浆制成的再生纸，因为它是环保的，所以白度很高，但在这种情况下，你必须忍受异物。  

酸性紙か中性紙かというと上質紙で保存が必要な紙なので中性紙が多い。そしてあの表面性をどうやって出しているのかですが、どうも「ゼラチン」を表面に塗工することがあるようです。  
说到酸性或中性纸，中性纸更常见，因为它是细纸，需要保存。 那你是如何得到这种表面性的呢？显然，'明胶'有时是涂在表面的。  

実は非塗工紙と呼ばれる紙の表面というのは何も塗っていないように見えますが、印刷で表面強度が必要な紙には「表面サイズ」と呼ばれる薬品を塗っています。  
事实上，被称为无涂层纸的纸张表面看起来就像没有涂层一样，但需要表面强度以进行印刷的纸张则涂有一种称为 "表面尺寸 "的化学物质。  

これは塗工紙の塗料とはまた別で、印刷用紙の多くは「デンプン」を表面強度対策やにじみ防止のために塗っています。あの食べ物のデンプンです。たとえばご飯粒を潰して糊に出来ますが、そういうものを塗っているということです。実際に自分が関わった時は「コーンスターチ」つまりトウモロコシ澱粉がよく使われていました。紙に塗ったデンプンが乾けばパリッとして表面強度も強くなるんですね。  
这与铜版纸的涂层不同，大多数印刷纸都涂有 "淀粉"，以防止表面强度和模糊不清。 它就是那个食物淀粉。 例如，米粒可以被捣碎制成胶水，这就是用来涂抹纸张的东西。 事实上，在我参与工作时，经常使用 "玉米淀粉"，或玉米淀粉。 当涂在纸上的淀粉干了以后，它就会变得很脆，表面强度也会变强。  

同じように表面強度を強くしたいケント紙にはゼラチンが使われているのだろうと思います。ゼラチンは古くから接着剤としても使われていますから使いやすいのでしょう。  
我想明胶可能是用于肯特纸，在那里你希望表面强度同样强大。 明胶作为粘合剂已经使用了很长时间，所以它必须易于使用。  



### 5.2 资料来源2：https://kotobank.jp/word/ケント紙-60964  
イギリスのケント地方で初めて製造された画用紙。純白で紙質は硬く、おもに製図用にする。わが国では名刺、カレンダーにも用いる。一般には主原料として化学パルプを用いるが、高級品は、綿ぼろを精製した綿パルプを用いて抄造し、表面サイズ（膠(にかわ)状の液を塗布する）を施して粗面に仕上げる。  
纸张最早产于英格兰的肯特地区。 它是纯白色的，很硬，主要用于绘画。 在日本，它还被用于制作名片和日历。 一般来说，化学纸浆被用作主要原料，但高档产品是由精制棉浆和表面尺寸（涂有类似胶水的液体）制成的，使其表面粗糙。  

 純白で紙面が緻密(ちみつ)な高級製図用紙。名刺用紙にも使われる。英国のケント地方で初めて作られたのでこの名がある。化学パルプを原料とし，粗面に仕上げるのが普通であるが，高級品は木綿ぼろを原料とする。  
高质量的画纸，表面纯白、致密。 也用于名片纸。 因为它最早是在英国的肯特地区生产的，所以被命名为肯特酒。 通常由化学纸浆制成，表面粗糙，但高端产品是由棉布制成。  

高級紙の一種。色調は純白で，紙質は硬く締っている。イギリスのケント地方で初めてつくられたのでこの名がある。化学パルプでつくるが，ごく高級品は木綿など天然繊維を加えることもある。図画，製図用のほか，名刺などにも用いる。   
高级纸的一种。 色调为纯白色，纸张坚硬而紧密。 因为它最早产于英格兰的肯特地区而得名。 它是由化学纸浆制成的，但有时会在非常高级的产品中加入天然纤维，如棉花。 用于绘图和制图，以及制作名片。  

〘名〙 (ケントは Kent 原産地であるイギリスの地名から) 洋紙の一種。純白の上質紙でやや粗面。ふつうはケミカルパルプが原料。図画・製図用紙、名刺用紙などに用いられる。ケント。〔造本と印刷（1948）〕  
(Kent来自英国的一个地名，即Kent的发源地)一种西方的纸。 纯白色的细纸，表面略显粗糙。 通常由化学纸浆制成。 用于绘画/绘图纸、名片纸等。 肯特。 [书籍装订和印刷（1948）]。   

純白で緻密な上質紙。綿ぼろ・化学パルプなどで作られ、製図・絵画・印刷などに用いる。英国のケント州で初めて製造された。   
纯白、致密、优质的纸张。 由棉布、化学纸浆等制成，用于绘图、绘画和印刷。 首次生产于英国肯特郡。  

化学パルプを100％原料とした，純白で固く締まった上質の図画用紙。比較的厚手でインキがにじまず，消しゴムで消しても毛羽立たないが，鉛筆で書きやすいようないくぶんざらついた面をもつ。イギリスのケントKent地方ではじめて作られたので，この名称が日本で用いられている。主用途は製図用であるが，名刺やカレンダーにも使用される。【臼田 誠人】   
一种纯白、坚硬、高质量的绘图纸，由100%的化学浆制成。 它比较厚，墨水不会渗出，用橡皮擦拭时也不会蓬松，但表面略微粗糙，便于用铅笔书写。 这个名字在日本被使用，因为它最早是在英格兰的肯特地区生产的。 主要用于绘画，但也用于名片和日历。 宇田诚。  


### 5.3 资料来源3：https://ja.wikipedia.org/wiki/画用紙
ケント紙は平滑な画用紙で、製図やイラストレーション、名刺等に使用される。硬筆やインクとの相性が良く、にじみにくさと吸収性を持つ。ケント紙の名はかつてイギリスのケント州から輸入されていたことに由来するが、日本でのみ通用する呼称である[9]。国産品は1918年に日本海軍用の海図用紙として三菱製紙で製造されたものを始まりとする。海外での類似紙種にはブリストルボード（英語版）がある。  
肯特纸是一种光滑的绘图纸，用于绘图、插图和名片。 它与硬笔和墨水兼容，并具有良好的渗漏和吸收性能。 肯特纸的名字来自于它曾经从英国的肯特郡进口，但这个名字只在日本被接受[9]。 第一个国内产品是由三菱造纸厂在1918年生产的，作为日本海军的海图纸。 海外类似的纸张类型包括Bristol Board。

### 5.4 资料来源4：http://zokeifile.musabi.ac.jp/ケント紙/
ケント紙は、イギリスのケント地方にその名が由来する紙で、一般には化学パルプ100%を原料としています。平滑性が高く、適度な弾力性と厚みを持っており、鉛筆やペン、水性絵具など、多くの筆記具や画材と相性がよく、消しゴムを使った時に紙面の毛羽立ちが少ないのが特長です。ただし、水分によって伸縮しますから、水性絵具などの画材を用いる場合は、水張りをしてから使用すると良いでしょう。また、ケント紙の表裏を判別するのは難しく、手で触れたときに若干滑らかに感じる面が表側になります。  
肯特纸的名字来自英国的肯特地区，一般由100%的化学纸浆制成。 它具有高度的平滑性、适度的弹性和厚度，与许多书写和绘画材料兼容，包括铅笔、钢笔和水性涂料，其特点是使用橡皮时纸面不那么模糊。 然而，它随水分膨胀和收缩，所以在使用水基涂料和其他绘画材料时，最好在使用前用水拉伸纸张。 也很难区分肯特纸的正面和背面；摸起来稍微光滑的一面就是正面。

ケント紙は、たいへん幅広い用途を持った紙です。製図や建築パースをはじめ、グラフィックデザインやイラストレーションなどの平面構成、モデル製作のような立体構成にも使うことができます。またディスプレイやパッケージのデザイン用として、有色のケント紙を利用することもできます。  
肯特纸有非常广泛的用途。 它可用于绘图、建筑透视、平面设计和插图等二维合成，以及模型制作等三维合成。 彩色肯特纸也可用于展示和包装设计。  

ケント紙は、サイズや厚さ（1平方メートルあたりの紙一枚の重量）によって様々な種類の商品があります。  
- KMKケント（サイズ：四六判・A本判・10～30mのロール形状、厚さ：150g・200g・250ｇ）は、製図用インクとも相性がよく、製図や建築パースで使われることの多い中性紙です。  
- またバロンケント（サイズ：四六判、厚さ：150g・200g・250ｇ）は、印刷用としても多く利用されています。  
- BBケント（サイズ：B本判、厚さ：175ｇ）は細目と荒目があり、コットンを原料としていて、表面にある程度の強度を持っている中性紙です。にじみが少ないため、緻密な描画に使うことができます。  
- ホワイトピーチケント（サイズ：四六判、厚さ：150g・200g・250ｇ）は比較的安価で、絵画やデザインの練習用としても広く利用できる中性紙です。他に、目の疲労を軽減するために、通常より白色度を抑えたものや、クリーム色のものがあります。ハガキと同じサイズのものや、パッド（紙の一端を糊付けしてつづったもの）になっているものもあります。  
肯特纸有各种尺寸和厚度（每平方米纸张的重量）。  
- KMK肯特纸（尺寸：46x，A型，10~30m卷，厚度：150克，200克，250克）与绘图墨水兼容，通常用于绘图和建筑透视。 它是一种中性纸，经常用于绘图和建筑透视。  
- Baron Kent（尺寸：46x，厚度：150克、200克、250克）也被广泛用于印刷，而  
- BB Kent（尺寸：B型，厚度：175克）是一种中性纸，质地细腻或粗糙，由棉花制成，具有一定的表面强度。 由于它的低透光性，它可以用于详细的图纸。   
- 白桃Kent（尺寸：46x，厚度：150g、200g和250g）是一种相对便宜的中性纸，可广泛用于绘画和设计实践。  
其他类型的产品有低于常规的白度，以减少眼睛疲劳，或者是奶油色。 有的与明信片大小相同，有的装在垫子里（纸的一端粘在一起并装订）。

ケント紙は画材店などで購入することができます。  
肯特纸可以从艺术用品商店购买。  

※描画例（写真）は、用紙の特性や表現の可能性を示すためのテストサンプルであり、特定の描画材の使用を薦めているものではありません。（一般的には適していないとされる描画材もあえて使用しています。）  
*绘画实例（如图）是测试样本，用于展示纸张的特性及其表现的可能性，并不构成使用任何特定绘画材料的建议。 (一般认为不合适的绘画材料也敢于使用)。  





<a name="Print"></a>  
# Print


<a name="Offset"></a>  
## 1. Offset Printing / 胶印  
source: https://en.wikipedia.org/wiki/Offset_printing, https://zh.wikipedia.org/wiki/胶印

胶印（英語：Offset printing，又名柯式印刷）是广泛使用的印刷技术，是先把上墨的图像转移到橡皮布上，然后再转移到印刷材料表面的一种印刷方法。胶印是平版印刷的一种，也是基于水墨相斥的原理的，胶印技术可以避免印刷表面的水与油墨一起传递到印刷材料的表面上。

胶印的优点如下：

- 图像质量高——比凸版印刷更加清晰、明锐，因为橡皮布能够与印刷材料表面的纹理很好地接触。
- 除了平滑的纸张外，还可以使用范围广泛的印刷材料，例如木头、织物、金属、皮革、较粗糙的纸张等。
- 印版的制作快速、简便。
- 印版耐印率比直接平版印刷更高——因为印版和印刷表面之间没有直接接触。

<a name="Giclée"></a>  
## 2. [Giclée - Wikipedia](https://en.wikipedia.org/wiki/Giclée)    {#Giclée}  

Giclée (/ʒiːˈkleɪ/ zhee-KLAY) is a neologism, ultimately derived from the French word gicleur, coined in 1991 by printmaker Jack Duganne for fine art digital prints made using inkjet printers. The name was originally applied to fine art prints created on a modified Iris printer in a process invented in the late 1980s. It has since been used widely to mean any fine-art printing, usually archival, printed by inkjet. It is often used by artists, galleries, and print shops for their high quality printing, but is also used generically for art printing of any quality.   
Giclée（/ʒiːˈkleɪ/ zhee-KLAY）是一个新名词，最终来自法语单词gicleur，由版画家Jack Duganne在1991年创造，用于由喷墨打印机制作的fine art数字印刷品。[1] 这个名字最初应用于fine art印刷品，后者由一个改良的Iris打印机制作于1980年代末发明的流程中。此后，它被广泛用于指任何由喷墨打印的fine art印刷品，通常是存档的。它经常被艺术家、画廊和印刷厂用于其高质量的印刷，但也被泛指任何质量的艺术印刷。  

### Origins  
The word giclée was adopted by Jack Duganne around 1990. He was a printmaker working at Nash Editions. He wanted a name for the new type of prints they were producing on a modified Iris printer, a large-format, high-resolution industrial prepress proofing inkjet printer on which the paper receiving the ink is attached to a rotating drum. The printer was adapted for fine-art printing. Duganne wanted a word that would differentiate such prints from regular commercial Iris prints then used as proofs in the commercial printing industry. Giclée is based on the French word gicleur, the French technical term for a jet or a nozzle, and the associated verb gicler (to squirt out). Une giclée (noun) means a spurt of some liquid.[3][4][5] The French verb form gicler means to spray, spout, or squirt. Duganne settled on the noun giclée.[3][6][4] 
giclée这个词是由Jack Duganne在1990年左右采用的。他是一位在Nash Editions工作的版画家。他想为他们在改良的Iris打印机上生产的新型版画取一个名字，Iris打印机是一种大尺寸、高分辨率的工业印前打样喷墨打印机，接受墨水的纸张被固定在一个旋转的滚筒上。这台打印机被改装为fine-art印刷。Duganne希望有一个词能将这种印刷品与当时作为商业印刷业样张的普通商业Iris印刷品区分开来。Giclée是基于法语单词gicleur，法语技术术语是指喷射或喷嘴，以及相关动词gicler（喷出）。Une giclée（名词）意味着某种液体的喷出。法语动词形式gicler意味着喷射、喷出或喷出。Duganne决定采用名词giclée。  

### Current usage  
In addition to its original association with Iris prints, the word giclée has come to be loosely associated with other types of inkjet printing including processes that use dyes or fade-resistant, archival inks (pigment-based), and archival substrates primarily produced on Canon, Epson, HP and other large-format printers.[7] These printers use the CMYK  (Cyan, Magenta, Yellow and Black) color process as a base with additional color cartridges for smoother gradient transitions (such as light magenta, light cyan, light and very light gray), up to 12 different inks in top model printers (orange, green, violet (Epson); red, green, blue (HP)) to achieve larger color gamut.[8] A wide variety of substrates on which an image can be printed with such inks are available, including various textures and finishes such as matte photo paper, watercolor paper, cotton canvas, pre-coated canvas, or textured vinyl.  
除了最初与Iris版画的联系外，giclée这个词还与其他类型的喷墨打印有松散的联系，包括使用染料或抗褪色的档案墨水（基于颜料）的工艺，以及主要由Canon, Epson, HP和其他大型打印机生产的档案基材。 [7] 这些打印机使用CMYK(Cyan, Magenta, Yellow and Black) 色彩工艺作为基础，并配有额外的彩色墨盒以实现更平滑的渐变过渡（如 light magenta, light cyan, light and very light gray），在顶级型号的打印机中最多有12种不同的墨水（橙、绿、紫（Epson）；红、绿、蓝（HP））以实现更大的色域。 [8] 可以用这种墨水打印图像的基材种类繁多，包括各种质地和表面处理，如亚光相纸、水彩纸、棉布、预涂帆布或有纹理的乙烯基。   

### Applications  
Artists generally use inkjet printing to make reproductions of their original two-dimensional artwork, photographs, or computer-generated art. Professionally produced inkjet prints are much more expensive on a per-print basis than the four-color offset lithography process traditionally used for such reproductions. A large-format inkjet print can cost more than ten times that of a four-color offset litho print of the same image in a run of 1,000, not including scanning and color correction. Four-color offset lithographic presses have the disadvantage of the full job having to be set up and produced all at once in a mass edition. With inkjet printing the artist does not have to pay for the expensive printing plate setup or the marketing and storage needed for large four-color offset print runs. This allows the artist to follow a just-in-time business model in which inkjet printing can be an economical option, since art can be printed and sold individually in accordance with demand. Inkjet printing has the added advantage of allowing artists to take total control of the production of their images, including the final color correction and the substrates being used. As a result, numerous individual artists own and operate their own printers.  
艺术家通常使用喷墨打印来复制他们的原始二维艺术作品、照片或计算机生成的艺术。专业生产的喷墨打印比传统上用于此类复制的四色平版印刷工艺的单件价格要高得多。大幅喷墨打印的成本可能是相同图像的四色平版印刷的十倍以上，不包括扫描和色彩校正。四色胶印平版印刷有一个缺点，那就是整个工作必须在大规模的版本中一次性设置和生产。有了喷墨印刷，艺术家就不必为昂贵的印版设置或大型四色胶印所需的营销和存储支付费用。这使艺术家能够遵循一种just-in-time的商业模式，在这种模式下，喷墨印刷可以成为一种经济的选择，因为艺术品可以根据需求单独印刷和销售。喷墨印刷还有一个好处，就是允许艺术家完全控制其图像的生产，包括最后的颜色校正和使用的基材。因此，许多个人艺术家拥有并操作他们自己的打印机。  

### Giclée in Inkjet printing  
（以下内容摘自[Inkjet printing - Wikipedia](https://en.wikipedia.org/wiki/Inkjet_printing)  ）  
...  
The highest-quality inkjet prints are often called "giclée" prints, to distinguish them from less-durable and lower-cost prints. However, the use of the term is no guarantee of quality, and the inks and paper used must be carefully investigated before an archivist can rely on their long-term durability.   
最高质量的喷墨打印作品通常被称为 "giclée "打印作品，以区别于耐用性较差、成本较低的打印作品。然而，使用这个术语并不能保证质量，在档案管理员可以信赖其长期耐用性之前，必须仔细调查所使用的墨水和纸张。   
...  
Inkjet printers have traditionally produced better quality output than color laser printers when printing photographic material. Both technologies have improved dramatically over time, although the highest-quality giclée prints favored by artists use what is essentially a high-quality specialized type of inkjet printer.  
在打印摄影材料时，喷墨打印机的输出质量传统上比彩色激光打印机更好。随着时间的推移，这两种技术都有了很大的改进，尽管艺术家们喜欢的最高质量的giclée版画使用的基本上是高质量的专门类型的喷墨打印机。   
...  
Another specialty application for inkjets is producing prepress color proofs for printing jobs created digitally. Such printers are designed to give accurate color rendition of how the final image will look (a "proof") when the job is finally produced on a large volume press such as a four-colour offset lithography press. An example is an Iris printer, whose output is what the French term giclée was coined for.   
喷墨机的另一个特殊应用是为以数字方式创建的印刷作业生产印前颜色样张。这类打印机的设计目的是，当作业最终在大批量印刷机（如四色胶印机）上生产时，对最终图像的外观进行准确的色彩渲染（"样张"）。一个例子是Iris打印机，它的输出就是法国术语giclée的由来。  



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  





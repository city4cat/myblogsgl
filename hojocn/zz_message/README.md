
source:  
https://www.manga-audition.com/message-from-tsukasa-hojo

# Message from Tsukasa Hojo

Taiyo Nakashima, 21/08/2013, 1 min read

Meet the judges : Here we have a message from another judge Manga master, Tsukasa Hojo, for all Silent Manga Audition applicants!  
评委见面会 : 在这里，我们有请另一位评委--漫画大师北条司--为所有默白漫画大赛的报名者致辞!

    As in movies, a simple change of camera angle in mangas can make a scene fun or sad. Even with the same facial expression, it is possible to convey different emotions. I hope everyone will enjoy the “power of performance” that is the core of manga as they work on their projects.  
    和电影一样，在漫画中，只要改变一下镜头角度，就能让场景变得有趣或伤感。即使是同样的面部表情，也可以传达出不同的情绪。希望大家在创作作品时，也能品位"表演的魅力"，它正是漫画的核心。

Once dubbed “The man who draws most beautiful woman”, Mr. Hojo was throughly impressed with the works from first year submissions!  
曾经被誉为"画出最美女性的男人 "的北条老师，对第一年投稿的作品印象非常深刻!

Let’s see if you can impress him further this year!  
让我们看看今年的你能否进一步打动他吧!

注：
《Master Shots Vol3》, Christopher Kenworthy, 第9章专门讲相机高度。

([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96796))  

[CH] 关于CH结尾里的致谢名单
CH结尾处有一个致谢名单。这里是英文版和法语版：


中文版图片里的人名 / 法文版图片里的(英文)人名（下文括号里为日本常见姓氏的中英对应[1][2]） 之 对应：

井上 雄彦  / Yoshihiko Inoue(井上)
梅沢(ze2) 勇人 / Hayato Umesawa
中村 隆夫  / Takao(高尾/高雄) Nakamura(中村)
新潟(xi4) 进  / Susumu(进) Nigata,     注："新潟"在法语版和英语版FC里被译为"Niigata", 笔误？（英文版FC被认为是从法语版翻译的）
畠(tian2)岡 光浩  / Mitsuhiro(光浩) Hataoka
北条 美津子  / Mitsuko Hojo(北条)
柳川 喜弘  /  Yoshihiro(吉弘) Yanagawa(柳川/梁川)
横田 和人  / Kazuto Yokota(橫田)
(其他) ET
垣内 克彦  / Katsuhiko Kakiuchi(垣內/柿內)
佐佐木 尚  / Nao Sasaki(佐佐木),     注：Nao(名尾), Sho(莊/向/尚)。
堀江 信彦  / Nobuhiko Horie(堀江)




- 井上雄彦和梅泽春人是北条司的高徒。梅泽春人曾以梅泽勇人名义创作[3], 应该对应上面的：梅沢 勇人。


- FC里空的助手是：
	Niigata新潟 Kazuko和子 (本名: Niigata新潟 Kazuto和人)
	Yokota横田 Susumu进
	Nakamura中村 Hiromi浩美  (本名: Nakamura中村 Mitsuhiro光浩)
	Yamazaki山崎 Makoto真琴 
所以，FC里的助手：
	Niigata新潟 Kazuko和子 (本名: Niigata新潟 Kazuto和人)
	Yokota横田 Susumu进
是由这两个实际人名组合而来的：
	新潟   进  / Susumu(进) Nigata,     注：Niigata(新潟)
	横田 和人  / Kazuto Yokota(橫田)

FC的助手：
	Nakamura中村 Hiromi浩美  (本名: Nakamura中村 Mitsuhiro光浩)
是由这两个实际人名组合而来的：
	中村 隆夫  / Takao(高尾/高雄) Nakamura(中村)
	畠岡 光浩  / Mitsuhiro(光浩) Hataoka

以上这些真实人名很可能都是男性。FC里空说只招男性助手，可能是北条司的真实写照。
另，井上雄彦、梅泽春人、柳川喜弘都是漫画家或业余漫画家。我做一个非常没有根据的猜测：北条司可能考虑到，都在同一个漫画圈，如果用他们的名字来构成FC里的助手的名字，会不会对他们会有影响？所以没有使用他们的名字来构成FC里的助手的名字。


- 基于找到的这些关系，从图里看，"其他(ET)"之前的那些人名会不会都是北条司的助手？


- 北条 美津子。很多人都猜测这是北条司的夫人。(更有人猜测短篇《城市猎人-XYZ》里的“清水美津子”可能也源自她[4])。北条美津子名字前后的那些人名都间接和北条司的助手有些联系。把北条美津子放在这里是否意味着她也做过北条司的助手呢？
另，"美津子"对应"Mitsuko",而"Mitsuko"也对应于"光子"[6]。猫眼里的"浅谷光子(Mitsuko Asatani)"人设有些类似CH里的野上伢子[6]，神谷真人是獠的原型。猫眼里浅谷光子和神谷真人(后来确实建立恋爱关系[6]）的关系有些类似于CH里伢子和獠。


- 柳川 喜弘[5]。
于1964年6月30日出生于鹿儿岛县。在《周刊少年Jump》组织的第37届“手冢大奖赛”（1989年）中，他获得了第三名，他去了东京，在那里他成为了北条司的主要助手(原文：“il monte à Tokyo où il devient notamment pour Tsukasa Hojo”，不确定这么翻译是否准确)...多年来，只在周末画漫画。

参考链接[7]里明确提到 柳川喜弘 是 北条司 的助手：  
 Among his former assistants there were INOUE Takehiko, UMEZAWA Haruto & YANAGAWA Yoshihiro.  (译：他(北条司)之前的助手有 井上雄彦，梅泽春人，柳川喜弘。）


- 垣内 克彦。不知道是谁

- 佐佐木 尚。不知道是谁

- 堀江 信彦。北条司的编辑&伯乐



##### 參考链接：

1. [Table of Common Japanese Surnames](http://htmfiles.englishhome.org/Japsurnames/Japsurnames.htm)
2. [日本常用姓氏表收集](https://www.douban.com/note/353116593/)
3. [梅泽春人](https://zh.wikipedia.org/zh-hans/%E6%A2%85%E6%B3%BD%E6%98%A5%E4%BA%BA)
4. [城市猎人完全版鉴赏——带你回顾这部充满男性荷尔蒙的漫画](https://www.jianshu.com/p/3776fa5c6582)
5. [柳川 喜弘](https://www.manga-news.com/index.php/auteur/YANAGAWA-Yoshihiro)
6. [浅谷光子](http://www.gerenjianli.com/Mingren/34/so23kp53a8.html)
7. [北条司 - MangaUpdates](https://www.mangaupdates.com/authors.html?id=350)

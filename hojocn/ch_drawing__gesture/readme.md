

- 32_123_5. 下图这种英雄式的站姿是正常人的姿态吗？  
![](img/ch_32_123_5_mod.jpg)  
因为资料[1]里285页提到一种非正常的膝盖会导致类似这种姿态，如下：  
![](img/Clinical.Gait.Analysis.Theory.and.Practice.p285.jpg)  
Gait abnormality: Genu recurvatum.  
Signs: Knee hyperextension during stance.   
Cause: Weak quadriceps, plantarflexion contracture (excessive plantarflexor–knee extensor couple) 



## 参考资料  
[1]. Clinical Gait Analysis: Theory and Practice. 2006. Christopher Kirtley.  
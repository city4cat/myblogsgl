

更多信息：  
https://ddnavi.com/person/6316  
https://ddnavi.com/person_tag/北条司/  

（借助在线翻译：[Google Translate](https://translate.google.cn)，[DeepL Translate](https://www.deepl.com/translator) ）


-------  
**Books**:  
-------  

## **小説 エンジェル・ハート ~消えた心臓~ (ZENON NOVELS)**  
小说《天使心》~《失踪的心脏》~（ZENON NOVELS）  
![](img/51Wvy8AvieL._SX350_BO1,204,203,200_.jpg)  
https://ddnavi.com/book/4198644691/   
作家    仲野ワタリ     北条司  
出版社    徳間書店  
発売日    2017-08-19  
ISBN    9784198644697  
[Amazon - Japan](http://www.amazon.co.jp/exec/obidos/ASIN/4198644691/dddev02-22/)  
伝言板に書かれた新たなるXYZ。その依頼内容は、“5年前に強奪されたあの心臓を探して欲しい"というものだったーー。  
累計2,400万部突破のメガヒット作「エンジェル・ハート」が待望の小説化!  
カバーイラストは北条司による描き下ろし!  
あのキャラクターたちが、小説版だけの完全オリジナルストーリーで躍動する!  
在留言板上写了一个新的XYZ，请求的内容是 "我想让你寻找那颗五年前被抢劫的心脏。  
总销量超过2400万册的大热作品《天使心》，现在是一部期待已久的小说。  
封面插图是由北条司新绘制的!  
书中的人物在一个完全原创的故事中进行，只为小说版！  


## **エンジェル・ハート ドラマ化記念 Sp (ゼノンセレクション)**  
《天使之心》戏剧化纪念版 Sp（ZENON选集）  
https://ddnavi.com/book/490524644X/  
作家    北条司  
出版社    コアミックス  
発売日    2015-10-01  
ISBN    9784905246442  
[amazonで購入する](https://www.amazon.co.jp/dp/490524644X?tag=dddev02-22&linkCode=ogi&th=1&psc=1)


## **漫画が語る戦争 焦土の鎮魂歌 (小学館クリエイティブ単行本)**  
漫画讲述的战争：焦土的安魂曲（小学馆创意书）  
![](img/51ZWgOcyCQL.jpg)  
https://ddnavi.com/book/4778032578/  
作家    手塚治虫     北条司     中沢啓治     ちばてつや  
出版社    小学館  
発売日    2013-07-25  
ISBN    9784778032579  


## **戦争の傷あと (漫画家たちの戦争)**  
战争的伤痕（漫画家的战争)  
![](img/51U-qvE6qiL._SL500_.__SX334__.jpg)  
https://ddnavi.com/book/4323064039/  
作家    藤子・F・不二雄     樹村みのり     手塚治虫     北見けんいち     今日マチ子     巴 里夫     西岸良平     北条司     滝田ゆう  
出版社    金の星社  
発売日    2013-04-04  
ISBN    9784323064031  
[amazonで購入する](https://www.amazon.co.jp/dp/4323064039?tag=dddev02-22&linkCode=ogi&th=1&psc=1)
金の星社 創業95周年記念出版  
シリーズ 漫画家たちの戦争(全6巻)  

原爆、子ども、銃後等のテーマ毎に戦争漫画を収載。  
手塚治虫、ちばてつや、赤塚不二夫、水木しげる等の巨匠から、『社長 島耕作』の弘兼憲史、  
『シティハンター』の北条司など第一線の作家、気鋭の若手まで内容も年代も幅広く収録。
“こち亀"の秋元治の作品など出版社や掲載誌の枠を超えて収載した奇跡的なシリーズです。
今こそ漫画で平和と戦争について考えてみませんか。  
「ドラえもん」が伝える戦争/  
「シティハンター」北条司、「釣りバカ日誌」北見けんいちが描く銃後――  
【収録作品】  
●藤子・F・不二雄『ドラえもん ぞうとおじさん』  
●樹村みのり『雨の中のさけび』  
●手塚治虫『すきっ腹のブルース』  
●北見けんいち『焼けあとの元気くん』  
●今日マチ子『cocoon 暗闇とペン先』  
●巴里夫『愛と炎・東京大空襲』(原作・さわさかえ)  
●西岸良平『三丁目の夕日 台風の夜』  
●北条司『少年たちのいた夏』  
●滝田ゆう『寺島町奇譚 日和下駄』  
金の星社 95周年纪念出版物  
漫画家之战系列（6卷)  
包含原子弹爆炸、儿童和枪击后遗症等主题的战争漫画。  
来自手冢治虫、千叶哲也、赤冢不二雄、水木茂、"志摩小作社长 "的广金健士、《城市猎人》北条司等大师，以及新晋的年轻作家。  
这是一个神奇的系列，包括 "Kochikame "名家秋元修的作品，以及其他超越出版商和杂志界限的作品。  
为什么你现在不通过漫画来思考和平与战争呢？  
《哆啦A梦》所传达的战争  
《城市猎人》的作者是北条司，而「釣りバカ日誌」的作者是北见健一。  
收录作品：  
。。。
北条司《少年たちのいた夏》。



## **北条司クロニクル~叙情詩の軌跡~ (ゼノンコミックス エコゼノン)**   
 北条司纪事~ 抒情诗的轨迹~ (Zenon Comics Eco Zenon)  
![](img/61yiUiO4mL.jpg)  
https://ddnavi.com/book/4199800077/  
作家    北条司  
出版社    徳間書店  
発売日    2011-03-09  
ISBN    9784199800078   
[amazonで購入する](https://www.amazon.co.jp/dp/4199800077?tag=dddev02-22&linkCode=ogi&th=1&psc=1)


## **CITY HUNTER SPECIAL 2 (JUMP jBOOKS)**  
![](img/31NBVRX43FL.jpg)  
https://ddnavi.com/book/4087030822/  
作家    岸間 信明     北条司  
出版社    集英社  
発売日    1999-04-02  
ISBN    9784087030822  
[amazonで購入する](https://www.amazon.co.jp/dp/4087030822?tag=dddev02-22&linkCode=ogi&th=1&psc=1)


## **CITY HUNTER 2 (JUMP jBOOKS)**  
![](img/31GWZ2BX06L.jpg)  
https://ddnavi.com/book/408703058X/ 
作家    稲葉 稔     北条司  
出版社    集英社  
発売日    1997-04-24  
ISBN    9784087030587   
[amazonで購入する](https://www.amazon.co.jp/dp/408703058X?tag=dddev02-22&linkCode=ogi&th=1&psc=1)


## **キャッツ・アイ (JUMP jBOOKS)**    
![](img/31W39QWBPNL._SL500_.__SX334__.jpg)  
https://ddnavi.com/book/4087030555/  
作家    高屋敷 英夫     北条司  
出版社    集英社  
発売日    1996-12-13  
ISBN    9784087030556  
[amazonで購入する](https://www.amazon.co.jp/dp/4087030555?tag=dddev02-22&linkCode=ogi&th=1&psc=1)
怪盗キャッツアイの今回の標的は絵画「冬の少女」。しかし手に入れたそれは偽せ物だった! 真相をつきとめるため、瞳たちは一路、北海道へと向かうが…。不朽の人気コミックノベライズ。



## **Magzines**:  

https://ddnavi.com/davinci/backnumber/281/  
    ダ・ヴィンチ2017年9月号, 特集2 『シティーハンター』～『エンジェル・ハート』完結！冴羽獠が見守った32年間  
![](https://ddnavi.com/wp-content/uploads/2017/08/862926b223eec49bc11a4d3c3d2bbcad.jpg)  

https://ddnavi.com/davinci/backnumber/274/  
    ダ・ヴィンチ2017年2月号, 相思相愛の激レア対談! 北条 司×清水依与吏  
![](https://ddnavi.com/wp-content/uploads/2017/01/33f5bc29324eb01f83d8c3f5dbf6fcb9.jpg)  

https://ddnavi.com/davinci/backnumber/252/  
    ダ・ヴィンチ2015年4月号, davinci pick up 『ANGEL HEART』北条司  
![](https://ddnavi.com/wp-content/uploads/2015/03/4d0c314f51b1fb45fb35c51ad87f3e70.jpg)  

https://ddnavi.com/davinci/backnumber/118/  
    ダ・ヴィンチ2004年2月号, コミック ダ・ヴィンチ：北条司 その尽きない魅力  

https://ddnavi.com/davinci/backnumber/86/  
    ダ・ヴィンチ2001年6月号, コミック ダ・ヴィンチ：21世紀を彩る、注目の新雑誌！  





--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



source:  
https://ddnavi.com/interview/403939/a/

更多信息：  
https://ddnavi.com/person/6316  
https://ddnavi.com/person_tag/北条司/  

（借助在线翻译：[Google Translate](https://translate.google.cn)，[DeepL Translate](https://www.deepl.com/translator) ）

![]()  
トップ
インタビュー・対談
「北条司先生を前に感極まって泣いたのもいい思い出です」『シティーハンター』転生モノを描いた作者の意外な素顔

**「北条司先生を前に感極まって泣いたのもいい思い出です」『シティーハンター』転生モノを描いた作者の意外な素顔**  
"在北条司先生面前流泪是一段美好的回忆"，这是绘制《城市猎人》的作者意想不到的真面目

 公開日：2017/10/1

![](https://ddnavi.com/wp-content/uploads/2017/10/kyoukaraCITY-HUNTER_1.jpg)  
[月刊コミックゼノン 2017年 11 月号 [雑誌]](https://www.amazon.co.jp/dp/B074BPZBMJ?tag=dddev02-22&linkCode=ogi&th=1&psc=1)  

出版社：
    徳間書店
発売日：
    2017/09/25

Amazonで購入
BOOK☆WALKERで購入


　今年7月に、月刊コミックゼノン2017年9月号（ノース・スターズ・ピクチャーズ）から連載を始めた『今日からCITY HUNTER』。『シティーハンター』を“参考書”に、「40歳のヒロインが転生したら憧れの『シティーハンター』の世界にきちゃった!?」というナナメ上の設定を描き、ネットではSNSを中心に話題騒然。現在、9月25日に発売された同誌2017年11月号で第3話が掲載中と、順調に連載を続けている。  
　今年7月，《今日起城市猎人》开始在《月刊漫画Zenon》（North Stars Pictures）2017年9月号上连载。 该系列以《城市猎人》为 "参考"，描述了一个40岁的女主角在《城市猎人》的世界中转世。 在互联网上，特别是在SNS上，这是一个热门话题。 目前，第三集正刊登在9月25日上市的2017年11月号杂志上，该系列杂志正在顺利地继续。  

　本作を描いているのは『侍父』や『ロボこん‼』『3年B組一八先生』で知られる錦ソクラ氏。本記事では同氏に新連載『今日からCITY HUNTER』が実現した背景を直撃。インタビューから見えてきたのは、著作のイメージとは異なる誠実な人となりと、原作『シティーハンター』への熱い想いだった。  
　本作品由西木宗仓绘制，他以《侍父》、《机器人-Kon‼》和《3年B班一八老师》闻名。 在这篇文章中，我们采访了他关于新系列 "今日起城市猎人"的背景。 采访中出现的是一个与他的作品形象不同的真诚的个性，以及他对《城市猎人》原作的热情。

■「畏れ多くてとても描ける気がしません」  
■我不认为我可以画它，因为我对它充满敬畏。  

――『今日からCITY HUNTER』が連載されるまでの経緯は？  
--《今日起城市猎人》是如何被连载的？ 

錦ソクラ（以下、ソクラ）：コミックゼノンの編集の方に声をかけていただき、直接お会いして企画内容を伺ったのですが、それを初めて聞いた時は、正直、「自分には無理だ！」と思いました。  
錦ソクラ（以下简记为ソクラ）：《Comic Zenon》的编辑找到了我，并亲自与他见面，听取了关于这个项目的介绍，但当我第一次听到这个项目时，说实话，我想，"我不能这样做！"。  

　なにしろ北条先生は巨匠中の巨匠、雲の上の存在です。その代表作のひとつである『シティーハンター』は、誰もが知っていて熱心なファンも多い。そんな愛されている作品を、自分が汚すことになってしまったらと思うと、畏れ多くてとても描ける気がしませんでした。  
  毕竟，北条先生是大师中的大师，是站在世界之巅的存在。 他的代表作之一《城市猎人》人人皆知，有许多忠实的粉丝。 我很害怕我最终会玷污这样一部受人喜爱的作品，以至于我觉得我不能画它。  

　その気持ちを担当さんに正直に伝えて、そうしないためにはどうすべきか、何が大切か、よく話し合いながら作品の方向性を探り、少しずつ形にしていきました。  
　我诚实地告诉负责人我的感受，我们讨论了我应该如何防止这种情况发生，以及什么对我来说是重要的，同时探讨了工作的方向，并逐渐使其成形。 

――新連載が決まった時の心境はいかがでしたか？  
--当你决定开始一个新系列时，你的感觉如何？   

ソクラ：自分でも驚きました。不安ばかりで自信は全くありませんでしたが、やると決めたからには腹を括ってこの作品に全力で取り組もうと思いました。北条先生にもお会いして、温かい励ましのお言葉をかけていただき、感極まって涙をこぼしたのもいい思い出です（笑）。北条先生に、そして『シティーハンター』のファンの皆様に、できるだけ失礼のないように、そして少しでも喜んでもらえるように頑張ろうと思いました。  
ソクラ：我自己也很惊讶。 我当时充满了焦虑，完全没有信心，但现在我决定要做了，我决定把我的心和灵魂投入到这项工作中，全力以赴。 我见到了北条老师，他给了我热情的鼓励，我感慨万分，泪流满面。 我想尽我所能，对北条老师和所有"城市猎人"的粉丝尽可能地不客气，并尽可能地让他们高兴。 

――錦ソクラ先生が『シティーハンター』で一番好きなポイントは？  
--西木 《城市猎人》中你最喜欢的一点是什么，佐仓老师？ 

ソクラ：好きなポイントはたくさんあるのですが、やはり獠というキャラクターの魅力でしょうか。おちゃらけていて女好きですが、決める時はきっちり決める、これぞまさにかっこいい大人の男ですね。  
ソクラ：我喜欢的地方很多，但我不得不说獠这个角色的魅力。 他很笨拙，喜欢女人，但当他必须做出决定时，他就会做出决定。  

　また、小さい頃に読んだ時はあまり意識しなかったんですが、今読み返してみるとキャラクターの心の動きや関係性の変化がものすごく丁寧に描かれていることに驚きます。香も当時の少年読者からは全く不人気だったと言われていますが、今見るとすごく魅力的で真っ当にヒロインしている。普段強がっている一方で、びっくりしたときにナチュラルに獠に抱きついてたり、すごくかわいい。獠のおふざけっぷりも、周囲の人間のためにあえてそう見せている節があって、単純なスケベではない、思慮深いスケベだと大人になってからようやく気づきました（笑）。  
　另外，我小的时候读这本书时并没有多想，但现在回过头来再读，我对人物的情感和关系的变化如此仔细感到惊讶。 据说，当时香并不受少年读者的欢迎，但现在看她，她非常有吸引力，是个直率的女主角。虽然她通常很坚强，但当她感到惊讶时，她会自然地拥抱獠，这真的很可爱。直到我成为一个成年人，我才意识到他不仅仅是一个简单的猥琐的人，他是一个有思想的猥琐的人。(笑)  

　香に対しても、恋愛感情が深まらないようにわざと怒らせている。ハンマーをくらうのはその後ろめたさの表れでもあると。だから作中でマリィーに「香のハンマーをかわせないなんておかしい」と突っ込まれるシーンとかは本当にさりげない一言なんですが、今見ると結構ドキッとしますね。  
　他还故意让香生气，这样她就不会对他产生浪漫的感觉。 他被人用锤子敲打的事实是他犯错的表现。 这就是为什么电影中玛丽对他说："你无法抗拒香的锤子，这很有趣。"那是一个非常随意的评论，但当我现在看时，它是相当惊险的。   

　お互い意識しているのにそれを素直に伝えることができない二人が、パートナーとして心が通い合い、少しずつ気持ちを伝え合っていく……というドラマが根底に流れているところも、『シティーハンター』の大きな魅力のひとつだと思います。  
　我认为《城市猎人》的主要魅力之一是其背后的戏剧性，即两个人意识到对方，但不能诚实地表达，但作为搭档，他们逐渐了解对方，并逐渐向对方传达自己的感情。

――特に好きなエピソードやシーン、キャラクターなどがございましたら教えてください。  
--请告诉我们你特别喜欢的任何剧集、场景或人物。  

ソクラ：獠が海坊主、ミック、海原などのメインキャラクターと対峙するシリアスなエピソードは、どれもかっこよくて何度読んでも痺れます。あと、『都会のシンデレラ!!』などの、獠と香の二人の関係性の変化が描かれるエピソードも大好きです。  
ソクラ：獠面对主要人物的严肃情节，如海坊主、米克和海原，都是很酷的，一遍又一遍地阅读，让人沉浸其中。 另外，"城市中的灰姑娘！！"，和其他显示獠和香之间关系变化的情节。 


■ファン視点で原作を別の角度から描きたい  
我想从粉丝的角度出发，从不同的角度描绘原作。  

――北条先生らしい作画、『シティーハンター』の世界観を守るうえで、気を付けていることはありますか？  
--在以北条老师的风格作画和保留《城市猎人》的世界时，有什么是你所牢记的吗？   

ソクラ：『シティーハンター』の世界は北条先生の作品として完成されているので、僕はひたすら、北条先生の作品のノリを少しでも再現することを目指して描いています。そのためにも、尊敬の意を込めて絵柄をできるだけご本人に寄せることを意識していますが、北条先生は絵が上手すぎるので真似しようとするだけで精一杯です！   
北条老师的作品已经完成了《城市猎人》的世界观，所以我只是以尽可能模仿他的作品。 为了做到这一点，出于对北条老师的尊重，我尽量画得接近他的设计，但他画得很好，我只能模仿他！"。   

　また、作画だけでなく、細かいセリフ回しやギャグのテンポ、シリアスなシーンとのメリハリなどの、『シティーハンター』特有のノリや雰囲気を意識して、そのニュアンスを出せるように努力しています。  
　除了绘画，我还意识到"城市猎人"独特的氛围和气氛，如详细的对话、插科打诨的节奏、以及严肃场景的干脆利落，我努力将这些细微差别表现出来。 

――北条先生の作画で、上手く近づけることが難しい一番のポイントは？  
--北条老师的画中最难模仿的一点是什么？  

ソクラ：北条先生の絵柄は時期によってかなり変化しているので、単純にパーツだけ真似れば良いというわけではなく、髪の毛や服のシワに至るまで線の一本一本に気を配らなければ全体の雰囲気が近づけられないところがとても難しいです。  
北条老师的设计根据每年的时间变化很大。仅仅复制零件是不够的。你必须注意每一条线，从头发到衣服上的皱纹，以获得相同的整体氛围。 

　北条先生の作画はとにかく丁寧で誠実で、僕は基本的に雑でいい加減なので、大変苦労しています。日々勉強させてもらっています。  
　北条老师的画很仔细、很真诚，而我的画基本上是乱七八糟的、很懒散，所以对我来说是很费劲的。 我每天都在学习越来越多的东西。 

――今後、『今日からCITY HUNTER』で描きたいことがございましたら、ぜひお聞かせください！  
--如果你想在《今日起 城市猎人》中画出什么，请告诉我们!  

ソクラ：名エピソードの数々を、いちファンの視点で少しだけ違った角度から追体験できるような、そんな作品にしたいと思っています。  
我想创作一部作品，让你从一个稍微不同的角度，从一个粉丝的角度来重温许多著名的剧集。   

『シティーハンター』という作品の魅力を、よりたくさんの方に伝えられればと思います。これからもどうぞよろしくお願いいたします。  
我希望向尽可能多的人传达《城市猎人》的吸引力。 谢谢你的持续支持。  

（C）錦ソクラ/NSP 2017  
この記事で紹介した書籍ほか  




--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


